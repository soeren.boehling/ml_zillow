{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "643f3868-1417-4d68-b976-e65c46e428d0",
   "metadata": {},
   "source": [
    "# Group Project Zillow\n",
    "*Predicting housing prices*\n",
    "\n",
    "**Group members:**\n",
    "- Hamish Eckstein (51059)\n",
    "- Marc Seeger (51262)\n",
    "- Ricardo Leon (52490)\n",
    "- Paul Bork (50868)\n",
    "- Sören Böhling (48694)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f787ccf0-a934-42fb-93ef-32efc0733422",
   "metadata": {},
   "source": [
    "## 1. Business problem to machine learning problem"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9a476819-4200-4831-b6b3-9a7bf9c8c7d0",
   "metadata": {},
   "source": [
    "### 1.1. Do you need machine learning?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68d7dad8-5850-44d2-9f80-72493b13fead",
   "metadata": {},
   "source": [
    "Machine learning should be implemented when a traditional rule based approach to determining a target value is no longer feasible. For the housing dataset we are exploring there are 58 features. The complex rules required to predict our target value and large relatively unstructured nature of our data means machine learning is essential to tackle our business problem."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "301e7018-bc62-452e-ab22-8ec90674f5ae",
   "metadata": {},
   "source": [
    "### 1.2. Can you formulate your problem clearly?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81cfdac1-e3b5-4d3e-8bfe-06b95329b3b8",
   "metadata": {},
   "source": [
    "Our company seeks to utilise a data driven approach for predicting house prices based upon a large variety of potential input features and the associated weights of those input features.\n",
    "\n",
    "Two potential business models: (Which do you prefer?)\n",
    "\n",
    "Leverage real estate arbitrage opportunities that arise when sellers in the market under-price their house relative to the model we produce.\n",
    "\n",
    "Develop a platform to increase house price transparency. (similar to Autotrader car pricing platform)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26986e28-4e85-4723-943a-84547cd13eca",
   "metadata": {},
   "source": [
    "### 1.3. Do you have sufficient data?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fc07bd13-5203-4904-8da3-1bffd16164f2",
   "metadata": {},
   "source": [
    "We have almost 6 million labelled data points for analysis from 2016 and 2017. This should be sufficient for predictions, however the nature of the housing market means predictions may be suited to predicting prices in the near future, not longterm."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1c50e6df-aa44-47cd-81bb-d805f1169b9b",
   "metadata": {},
   "source": [
    "### 1.4. Does your problem have a regular pattern?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0d7951e1-ea68-4379-aff1-260901b915af",
   "metadata": {},
   "source": [
    "There does appear to be regular patterns that present themselves within our dataset. There seems to be relationships between size of property, location, number of bathrooms and other features and the relative house price."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aa80a221-e6d2-4267-9c1f-e01d926fb1c9",
   "metadata": {},
   "source": [
    "### 1.5. Can you find meaningful representations of your data?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "276e0e45-2658-43bb-970f-7fc551d821f9",
   "metadata": {},
   "source": [
    "The categorical data we have can be transformed into numerical data within our machine learning pipeline. This can be combined with the existing numerical data we have to form our features vector."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a1ab478a-d55b-458b-a291-3bfb4855461f",
   "metadata": {},
   "source": [
    "### 1.6. How do you define success?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b431a0fb-0492-4697-8953-d93ab1795745",
   "metadata": {},
   "source": [
    "The ability of our model to accurately predict house prices and generalise well to unseen data."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10060557-442c-4de2-8fe9-950a598c0ccc",
   "metadata": {},
   "source": [
    "### Notes about the Dataset:\n",
    "\n",
    "**Proporties_2016 and properties_2017 Datasets:**\n",
    "\n",
    "- Deleted columns with more than 1 mio missing data\n",
    "- Deleted entries/rows without longitude/latitude\n",
    "- The deleted rows sometimes don't drop but fill in new data\n",
    "\n",
    "**test_2017 and train_2016_v2 Datasets:**\n",
    "\n",
    "- What is logerror describing ?\n",
    "- Only 90k+ entries \n",
    "- Merge with properties datasets and delete rest of rows ?\n",
    "\n",
    "### Main Project:\n",
    "\n",
    "**Which house is going to sell? How much is the sold price of a house?**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "e9935806-ac4d-4b0e-94b6-f9fc3e595a15",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import seaborn as sns\n",
    "import matplotlib.pyplot as plt\n",
    "import pandas as pd\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "8d54c1aa-0c56-4300-8cda-a1f3997e8e38",
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "C:\\Users\\sboeh\\anaconda3\\lib\\site-packages\\IPython\\core\\interactiveshell.py:3165: DtypeWarning: Columns (22,32,34,49,55) have mixed types.Specify dtype option on import or set low_memory=False.\n",
      "  has_raised = await self.run_ast_nodes(code_ast.body, cell_name,\n",
      "C:\\Users\\sboeh\\anaconda3\\lib\\site-packages\\IPython\\core\\interactiveshell.py:3165: DtypeWarning: Columns (49) have mixed types.Specify dtype option on import or set low_memory=False.\n",
      "  has_raised = await self.run_ast_nodes(code_ast.body, cell_name,\n"
     ]
    }
   ],
   "source": [
    "#Import the datasets\n",
    "data_2016 = pd.read_csv('all/properties_2016.csv')\n",
    "data_2017 = pd.read_csv('all/properties_2017.csv')\n",
    "data_test_2017 = pd.read_csv('all/test_2017.csv')\n",
    "data_train_2016 = pd.read_csv('all/train_2016_v2.csv')\n",
    "df_dictonary = pd.read_excel('all/zillow_data_dictionary.xlsx')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "5e60ca91-1b72-461c-b84a-5d149e01ea5b",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Feature</th>\n",
       "      <th>Description</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>'airconditioningtypeid'</td>\n",
       "      <td>Type of cooling system present in the home (i...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>'architecturalstyletypeid'</td>\n",
       "      <td>Architectural style of the home (i.e. ranch, ...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>'basementsqft'</td>\n",
       "      <td>Finished living area below or partially below...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>'bathroomcnt'</td>\n",
       "      <td>Number of bathrooms in home including fractio...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>'bedroomcnt'</td>\n",
       "      <td>Number of bedrooms in home</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>5</th>\n",
       "      <td>'buildingqualitytypeid'</td>\n",
       "      <td>Overall assessment of condition of the buildi...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>6</th>\n",
       "      <td>'buildingclasstypeid'</td>\n",
       "      <td>The building framing type (steel frame, wood f...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>7</th>\n",
       "      <td>'calculatedbathnbr'</td>\n",
       "      <td>Number of bathrooms in home including fractio...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>8</th>\n",
       "      <td>'decktypeid'</td>\n",
       "      <td>Type of deck (if any) present on parcel</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>9</th>\n",
       "      <td>'threequarterbathnbr'</td>\n",
       "      <td>Number of 3/4 bathrooms in house (shower + si...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10</th>\n",
       "      <td>'finishedfloor1squarefeet'</td>\n",
       "      <td>Size of the finished living area on the first...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>11</th>\n",
       "      <td>'calculatedfinishedsquarefeet'</td>\n",
       "      <td>Calculated total finished living area of the ...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>12</th>\n",
       "      <td>'finishedsquarefeet6'</td>\n",
       "      <td>Base unfinished and finished area</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>13</th>\n",
       "      <td>'finishedsquarefeet12'</td>\n",
       "      <td>Finished living area</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>14</th>\n",
       "      <td>'finishedsquarefeet13'</td>\n",
       "      <td>Perimeter  living area</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>15</th>\n",
       "      <td>'finishedsquarefeet15'</td>\n",
       "      <td>Total area</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>16</th>\n",
       "      <td>'finishedsquarefeet50'</td>\n",
       "      <td>Size of the finished living area on the first...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17</th>\n",
       "      <td>'fips'</td>\n",
       "      <td>Federal Information Processing Standard code ...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>18</th>\n",
       "      <td>'fireplacecnt'</td>\n",
       "      <td>Number of fireplaces in a home (if any)</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>19</th>\n",
       "      <td>'fireplaceflag'</td>\n",
       "      <td>Is a fireplace present in this home</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>20</th>\n",
       "      <td>'fullbathcnt'</td>\n",
       "      <td>Number of full bathrooms (sink, shower + bath...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>21</th>\n",
       "      <td>'garagecarcnt'</td>\n",
       "      <td>Total number of garages on the lot including ...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>22</th>\n",
       "      <td>'garagetotalsqft'</td>\n",
       "      <td>Total number of square feet of all garages on...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>23</th>\n",
       "      <td>'hashottuborspa'</td>\n",
       "      <td>Does the home have a hot tub or spa</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>24</th>\n",
       "      <td>'heatingorsystemtypeid'</td>\n",
       "      <td>Type of home heating system</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>25</th>\n",
       "      <td>'latitude'</td>\n",
       "      <td>Latitude of the middle of the parcel multipli...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>26</th>\n",
       "      <td>'longitude'</td>\n",
       "      <td>Longitude of the middle of the parcel multipl...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>27</th>\n",
       "      <td>'lotsizesquarefeet'</td>\n",
       "      <td>Area of the lot in square feet</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>28</th>\n",
       "      <td>'numberofstories'</td>\n",
       "      <td>Number of stories or levels the home has</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>29</th>\n",
       "      <td>'parcelid'</td>\n",
       "      <td>Unique identifier for parcels (lots)</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>30</th>\n",
       "      <td>'poolcnt'</td>\n",
       "      <td>Number of pools on the lot (if any)</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>31</th>\n",
       "      <td>'poolsizesum'</td>\n",
       "      <td>Total square footage of all pools on property</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>32</th>\n",
       "      <td>'pooltypeid10'</td>\n",
       "      <td>Spa or Hot Tub</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>33</th>\n",
       "      <td>'pooltypeid2'</td>\n",
       "      <td>Pool with Spa/Hot Tub</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>34</th>\n",
       "      <td>'pooltypeid7'</td>\n",
       "      <td>Pool without hot tub</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>35</th>\n",
       "      <td>'propertycountylandusecode'</td>\n",
       "      <td>County land use code i.e. it's zoning at the ...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>36</th>\n",
       "      <td>'propertylandusetypeid'</td>\n",
       "      <td>Type of land use the property is zoned for</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>37</th>\n",
       "      <td>'propertyzoningdesc'</td>\n",
       "      <td>Description of the allowed land uses (zoning)...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>38</th>\n",
       "      <td>'rawcensustractandblock'</td>\n",
       "      <td>Census tract and block ID combined - also con...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>39</th>\n",
       "      <td>'censustractandblock'</td>\n",
       "      <td>Census tract and block ID combined - also con...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>40</th>\n",
       "      <td>'regionidcounty'</td>\n",
       "      <td>County in which the property is located</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>41</th>\n",
       "      <td>'regionidcity'</td>\n",
       "      <td>City in which the property is located (if any)</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>42</th>\n",
       "      <td>'regionidzip'</td>\n",
       "      <td>Zip code in which the property is located</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>43</th>\n",
       "      <td>'regionidneighborhood'</td>\n",
       "      <td>Neighborhood in which the property is located</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>44</th>\n",
       "      <td>'roomcnt'</td>\n",
       "      <td>Total number of rooms in the principal residence</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>45</th>\n",
       "      <td>'storytypeid'</td>\n",
       "      <td>Type of floors in a multi-story house (i.e. b...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>46</th>\n",
       "      <td>'typeconstructiontypeid'</td>\n",
       "      <td>What type of construction material was used t...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>47</th>\n",
       "      <td>'unitcnt'</td>\n",
       "      <td>Number of units the structure is built into (...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>48</th>\n",
       "      <td>'yardbuildingsqft17'</td>\n",
       "      <td>Patio in  yard</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>49</th>\n",
       "      <td>'yardbuildingsqft26'</td>\n",
       "      <td>Storage shed/building in yard</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>50</th>\n",
       "      <td>'yearbuilt'</td>\n",
       "      <td>The Year the principal residence was built</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>51</th>\n",
       "      <td>'taxvaluedollarcnt'</td>\n",
       "      <td>The total tax assessed value of the parcel</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>52</th>\n",
       "      <td>'structuretaxvaluedollarcnt'</td>\n",
       "      <td>The assessed value of the built structure on t...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>53</th>\n",
       "      <td>'landtaxvaluedollarcnt'</td>\n",
       "      <td>The assessed value of the land area of the parcel</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>54</th>\n",
       "      <td>'taxamount'</td>\n",
       "      <td>The total property tax assessed for that asses...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>55</th>\n",
       "      <td>'assessmentyear'</td>\n",
       "      <td>The year of the property tax assessment</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>56</th>\n",
       "      <td>'taxdelinquencyflag'</td>\n",
       "      <td>Property taxes for this parcel are past due as...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>57</th>\n",
       "      <td>'taxdelinquencyyear'</td>\n",
       "      <td>Year for which the unpaid propert taxes were due</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                           Feature  \\\n",
       "0          'airconditioningtypeid'   \n",
       "1       'architecturalstyletypeid'   \n",
       "2                   'basementsqft'   \n",
       "3                    'bathroomcnt'   \n",
       "4                     'bedroomcnt'   \n",
       "5          'buildingqualitytypeid'   \n",
       "6            'buildingclasstypeid'   \n",
       "7              'calculatedbathnbr'   \n",
       "8                     'decktypeid'   \n",
       "9            'threequarterbathnbr'   \n",
       "10      'finishedfloor1squarefeet'   \n",
       "11  'calculatedfinishedsquarefeet'   \n",
       "12           'finishedsquarefeet6'   \n",
       "13          'finishedsquarefeet12'   \n",
       "14          'finishedsquarefeet13'   \n",
       "15          'finishedsquarefeet15'   \n",
       "16          'finishedsquarefeet50'   \n",
       "17                          'fips'   \n",
       "18                  'fireplacecnt'   \n",
       "19                 'fireplaceflag'   \n",
       "20                   'fullbathcnt'   \n",
       "21                  'garagecarcnt'   \n",
       "22               'garagetotalsqft'   \n",
       "23                'hashottuborspa'   \n",
       "24         'heatingorsystemtypeid'   \n",
       "25                      'latitude'   \n",
       "26                     'longitude'   \n",
       "27             'lotsizesquarefeet'   \n",
       "28               'numberofstories'   \n",
       "29                      'parcelid'   \n",
       "30                       'poolcnt'   \n",
       "31                   'poolsizesum'   \n",
       "32                  'pooltypeid10'   \n",
       "33                   'pooltypeid2'   \n",
       "34                   'pooltypeid7'   \n",
       "35     'propertycountylandusecode'   \n",
       "36         'propertylandusetypeid'   \n",
       "37            'propertyzoningdesc'   \n",
       "38        'rawcensustractandblock'   \n",
       "39           'censustractandblock'   \n",
       "40                'regionidcounty'   \n",
       "41                  'regionidcity'   \n",
       "42                   'regionidzip'   \n",
       "43          'regionidneighborhood'   \n",
       "44                       'roomcnt'   \n",
       "45                   'storytypeid'   \n",
       "46        'typeconstructiontypeid'   \n",
       "47                       'unitcnt'   \n",
       "48            'yardbuildingsqft17'   \n",
       "49            'yardbuildingsqft26'   \n",
       "50                     'yearbuilt'   \n",
       "51             'taxvaluedollarcnt'   \n",
       "52    'structuretaxvaluedollarcnt'   \n",
       "53         'landtaxvaluedollarcnt'   \n",
       "54                     'taxamount'   \n",
       "55                'assessmentyear'   \n",
       "56            'taxdelinquencyflag'   \n",
       "57            'taxdelinquencyyear'   \n",
       "\n",
       "                                          Description  \n",
       "0    Type of cooling system present in the home (i...  \n",
       "1    Architectural style of the home (i.e. ranch, ...  \n",
       "2    Finished living area below or partially below...  \n",
       "3    Number of bathrooms in home including fractio...  \n",
       "4                         Number of bedrooms in home   \n",
       "5    Overall assessment of condition of the buildi...  \n",
       "6   The building framing type (steel frame, wood f...  \n",
       "7    Number of bathrooms in home including fractio...  \n",
       "8             Type of deck (if any) present on parcel  \n",
       "9    Number of 3/4 bathrooms in house (shower + si...  \n",
       "10   Size of the finished living area on the first...  \n",
       "11   Calculated total finished living area of the ...  \n",
       "12                  Base unfinished and finished area  \n",
       "13                               Finished living area  \n",
       "14                             Perimeter  living area  \n",
       "15                                         Total area  \n",
       "16   Size of the finished living area on the first...  \n",
       "17   Federal Information Processing Standard code ...  \n",
       "18            Number of fireplaces in a home (if any)  \n",
       "19               Is a fireplace present in this home   \n",
       "20   Number of full bathrooms (sink, shower + bath...  \n",
       "21   Total number of garages on the lot including ...  \n",
       "22   Total number of square feet of all garages on...  \n",
       "23                Does the home have a hot tub or spa  \n",
       "24                        Type of home heating system  \n",
       "25   Latitude of the middle of the parcel multipli...  \n",
       "26   Longitude of the middle of the parcel multipl...  \n",
       "27                     Area of the lot in square feet  \n",
       "28           Number of stories or levels the home has  \n",
       "29              Unique identifier for parcels (lots)   \n",
       "30                Number of pools on the lot (if any)  \n",
       "31      Total square footage of all pools on property  \n",
       "32                                     Spa or Hot Tub  \n",
       "33                              Pool with Spa/Hot Tub  \n",
       "34                               Pool without hot tub  \n",
       "35   County land use code i.e. it's zoning at the ...  \n",
       "36         Type of land use the property is zoned for  \n",
       "37   Description of the allowed land uses (zoning)...  \n",
       "38   Census tract and block ID combined - also con...  \n",
       "39   Census tract and block ID combined - also con...  \n",
       "40            County in which the property is located  \n",
       "41     City in which the property is located (if any)  \n",
       "42          Zip code in which the property is located  \n",
       "43      Neighborhood in which the property is located  \n",
       "44   Total number of rooms in the principal residence  \n",
       "45   Type of floors in a multi-story house (i.e. b...  \n",
       "46   What type of construction material was used t...  \n",
       "47   Number of units the structure is built into (...  \n",
       "48                                     Patio in  yard  \n",
       "49                      Storage shed/building in yard  \n",
       "50        The Year the principal residence was built   \n",
       "51         The total tax assessed value of the parcel  \n",
       "52  The assessed value of the built structure on t...  \n",
       "53  The assessed value of the land area of the parcel  \n",
       "54  The total property tax assessed for that asses...  \n",
       "55           The year of the property tax assessment   \n",
       "56  Property taxes for this parcel are past due as...  \n",
       "57  Year for which the unpaid propert taxes were due   "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "display(df_dictonary)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6b17f356-e80f-4c06-acf3-d8ed8ed9e313",
   "metadata": {},
   "source": [
    "## 2. Explorative data analysis for generating features"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "e594e5f9-9e56-4066-ae20-69dad29c4173",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create one dataset\n",
    "df = data_2016.append(data_2017)\n",
    "df.set_index('parcelid', inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "76a0370f-9483-46b2-bcce-c87d06b7b6bb",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Explore Dataset\n",
    "df_eda = df.copy()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "c701d27e-31c3-4278-8ee7-e8ee6dcde94c",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Index(['airconditioningtypeid', 'architecturalstyletypeid', 'basementsqft',\n",
       "       'bathroomcnt', 'bedroomcnt', 'buildingclasstypeid',\n",
       "       'buildingqualitytypeid', 'calculatedbathnbr', 'decktypeid',\n",
       "       'finishedfloor1squarefeet', 'calculatedfinishedsquarefeet',\n",
       "       'finishedsquarefeet12', 'finishedsquarefeet13', 'finishedsquarefeet15',\n",
       "       'finishedsquarefeet50', 'finishedsquarefeet6', 'fips', 'fireplacecnt',\n",
       "       'fullbathcnt', 'garagecarcnt', 'garagetotalsqft', 'hashottuborspa',\n",
       "       'heatingorsystemtypeid', 'latitude', 'longitude', 'lotsizesquarefeet',\n",
       "       'poolcnt', 'poolsizesum', 'pooltypeid10', 'pooltypeid2', 'pooltypeid7',\n",
       "       'propertycountylandusecode', 'propertylandusetypeid',\n",
       "       'propertyzoningdesc', 'rawcensustractandblock', 'regionidcity',\n",
       "       'regionidcounty', 'regionidneighborhood', 'regionidzip', 'roomcnt',\n",
       "       'storytypeid', 'threequarterbathnbr', 'typeconstructiontypeid',\n",
       "       'unitcnt', 'yardbuildingsqft17', 'yardbuildingsqft26', 'yearbuilt',\n",
       "       'numberofstories', 'fireplaceflag', 'structuretaxvaluedollarcnt',\n",
       "       'taxvaluedollarcnt', 'assessmentyear', 'landtaxvaluedollarcnt',\n",
       "       'taxamount', 'taxdelinquencyflag', 'taxdelinquencyyear',\n",
       "       'censustractandblock'],\n",
       "      dtype='object')"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Explore columns of dataset\n",
    "df_eda.columns "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "92b120cf-d7dd-45f7-9cb5-a8abcd8b8fa0",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>airconditioningtypeid</th>\n",
       "      <th>architecturalstyletypeid</th>\n",
       "      <th>basementsqft</th>\n",
       "      <th>bathroomcnt</th>\n",
       "      <th>bedroomcnt</th>\n",
       "      <th>buildingclasstypeid</th>\n",
       "      <th>buildingqualitytypeid</th>\n",
       "      <th>calculatedbathnbr</th>\n",
       "      <th>decktypeid</th>\n",
       "      <th>finishedfloor1squarefeet</th>\n",
       "      <th>...</th>\n",
       "      <th>yardbuildingsqft26</th>\n",
       "      <th>yearbuilt</th>\n",
       "      <th>numberofstories</th>\n",
       "      <th>structuretaxvaluedollarcnt</th>\n",
       "      <th>taxvaluedollarcnt</th>\n",
       "      <th>assessmentyear</th>\n",
       "      <th>landtaxvaluedollarcnt</th>\n",
       "      <th>taxamount</th>\n",
       "      <th>taxdelinquencyyear</th>\n",
       "      <th>censustractandblock</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>count</th>\n",
       "      <td>1.626881e+06</td>\n",
       "      <td>12122.000000</td>\n",
       "      <td>3255.000000</td>\n",
       "      <td>5.956015e+06</td>\n",
       "      <td>5.956039e+06</td>\n",
       "      <td>25360.000000</td>\n",
       "      <td>3.879883e+06</td>\n",
       "      <td>5.724366e+06</td>\n",
       "      <td>34475.0</td>\n",
       "      <td>406475.000000</td>\n",
       "      <td>...</td>\n",
       "      <td>5293.000000</td>\n",
       "      <td>5.862673e+06</td>\n",
       "      <td>1.367745e+06</td>\n",
       "      <td>5.868988e+06</td>\n",
       "      <td>5.893618e+06</td>\n",
       "      <td>5.956062e+06</td>\n",
       "      <td>5.842775e+06</td>\n",
       "      <td>5.916432e+06</td>\n",
       "      <td>112981.000000</td>\n",
       "      <td>5.820323e+06</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>mean</th>\n",
       "      <td>1.938462e+00</td>\n",
       "      <td>7.202607</td>\n",
       "      <td>647.051920</td>\n",
       "      <td>2.212674e+00</td>\n",
       "      <td>3.091159e+00</td>\n",
       "      <td>3.730363</td>\n",
       "      <td>6.032990e+00</td>\n",
       "      <td>2.301549e+00</td>\n",
       "      <td>66.0</td>\n",
       "      <td>1380.204340</td>\n",
       "      <td>...</td>\n",
       "      <td>278.331003</td>\n",
       "      <td>1.964350e+03</td>\n",
       "      <td>1.401209e+00</td>\n",
       "      <td>1.745185e+05</td>\n",
       "      <td>4.320197e+05</td>\n",
       "      <td>2.015500e+03</td>\n",
       "      <td>2.604776e+05</td>\n",
       "      <td>5.393300e+03</td>\n",
       "      <td>13.892053</td>\n",
       "      <td>6.048433e+13</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>std</th>\n",
       "      <td>3.154574e+00</td>\n",
       "      <td>2.436190</td>\n",
       "      <td>538.707521</td>\n",
       "      <td>1.078008e+00</td>\n",
       "      <td>1.273714e+00</td>\n",
       "      <td>0.501043</td>\n",
       "      <td>1.785322e+00</td>\n",
       "      <td>1.001996e+00</td>\n",
       "      <td>0.0</td>\n",
       "      <td>633.647526</td>\n",
       "      <td>...</td>\n",
       "      <td>369.723014</td>\n",
       "      <td>2.354339e+01</td>\n",
       "      <td>5.390660e-01</td>\n",
       "      <td>4.320903e+05</td>\n",
       "      <td>7.728019e+05</td>\n",
       "      <td>5.018861e-01</td>\n",
       "      <td>4.663194e+05</td>\n",
       "      <td>9.432917e+03</td>\n",
       "      <td>2.571590</td>\n",
       "      <td>3.249084e+11</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>min</th>\n",
       "      <td>1.000000e+00</td>\n",
       "      <td>2.000000</td>\n",
       "      <td>20.000000</td>\n",
       "      <td>0.000000e+00</td>\n",
       "      <td>0.000000e+00</td>\n",
       "      <td>1.000000</td>\n",
       "      <td>1.000000e+00</td>\n",
       "      <td>1.000000e+00</td>\n",
       "      <td>66.0</td>\n",
       "      <td>1.000000</td>\n",
       "      <td>...</td>\n",
       "      <td>10.000000</td>\n",
       "      <td>1.801000e+03</td>\n",
       "      <td>1.000000e+00</td>\n",
       "      <td>1.000000e+00</td>\n",
       "      <td>1.000000e+00</td>\n",
       "      <td>2.000000e+03</td>\n",
       "      <td>1.000000e+00</td>\n",
       "      <td>2.400000e-01</td>\n",
       "      <td>0.000000</td>\n",
       "      <td>-1.000000e+00</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>25%</th>\n",
       "      <td>1.000000e+00</td>\n",
       "      <td>7.000000</td>\n",
       "      <td>272.000000</td>\n",
       "      <td>2.000000e+00</td>\n",
       "      <td>2.000000e+00</td>\n",
       "      <td>3.000000</td>\n",
       "      <td>4.000000e+00</td>\n",
       "      <td>2.000000e+00</td>\n",
       "      <td>66.0</td>\n",
       "      <td>1012.000000</td>\n",
       "      <td>...</td>\n",
       "      <td>96.000000</td>\n",
       "      <td>1.950000e+03</td>\n",
       "      <td>1.000000e+00</td>\n",
       "      <td>7.617375e+04</td>\n",
       "      <td>1.838422e+05</td>\n",
       "      <td>2.015000e+03</td>\n",
       "      <td>7.726000e+04</td>\n",
       "      <td>2.464850e+03</td>\n",
       "      <td>14.000000</td>\n",
       "      <td>6.037400e+13</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>50%</th>\n",
       "      <td>1.000000e+00</td>\n",
       "      <td>7.000000</td>\n",
       "      <td>535.000000</td>\n",
       "      <td>2.000000e+00</td>\n",
       "      <td>3.000000e+00</td>\n",
       "      <td>4.000000</td>\n",
       "      <td>7.000000e+00</td>\n",
       "      <td>2.000000e+00</td>\n",
       "      <td>66.0</td>\n",
       "      <td>1282.000000</td>\n",
       "      <td>...</td>\n",
       "      <td>168.000000</td>\n",
       "      <td>1.963000e+03</td>\n",
       "      <td>1.000000e+00</td>\n",
       "      <td>1.248200e+05</td>\n",
       "      <td>3.133830e+05</td>\n",
       "      <td>2.016000e+03</td>\n",
       "      <td>1.717350e+05</td>\n",
       "      <td>3.999580e+03</td>\n",
       "      <td>14.000000</td>\n",
       "      <td>6.037572e+13</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>75%</th>\n",
       "      <td>1.000000e+00</td>\n",
       "      <td>7.000000</td>\n",
       "      <td>847.500000</td>\n",
       "      <td>3.000000e+00</td>\n",
       "      <td>4.000000e+00</td>\n",
       "      <td>4.000000</td>\n",
       "      <td>7.000000e+00</td>\n",
       "      <td>3.000000e+00</td>\n",
       "      <td>66.0</td>\n",
       "      <td>1615.000000</td>\n",
       "      <td>...</td>\n",
       "      <td>320.000000</td>\n",
       "      <td>1.981000e+03</td>\n",
       "      <td>2.000000e+00</td>\n",
       "      <td>2.004450e+05</td>\n",
       "      <td>5.010000e+05</td>\n",
       "      <td>2.016000e+03</td>\n",
       "      <td>3.163500e+05</td>\n",
       "      <td>6.215350e+03</td>\n",
       "      <td>15.000000</td>\n",
       "      <td>6.059042e+13</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>max</th>\n",
       "      <td>1.300000e+01</td>\n",
       "      <td>27.000000</td>\n",
       "      <td>8516.000000</td>\n",
       "      <td>3.200000e+01</td>\n",
       "      <td>2.500000e+01</td>\n",
       "      <td>5.000000</td>\n",
       "      <td>1.200000e+01</td>\n",
       "      <td>3.200000e+01</td>\n",
       "      <td>66.0</td>\n",
       "      <td>31303.000000</td>\n",
       "      <td>...</td>\n",
       "      <td>6141.000000</td>\n",
       "      <td>2.016000e+03</td>\n",
       "      <td>4.100000e+01</td>\n",
       "      <td>2.553212e+08</td>\n",
       "      <td>3.196225e+08</td>\n",
       "      <td>2.016000e+03</td>\n",
       "      <td>9.401108e+07</td>\n",
       "      <td>3.823176e+06</td>\n",
       "      <td>99.000000</td>\n",
       "      <td>4.830301e+14</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "<p>8 rows × 52 columns</p>\n",
       "</div>"
      ],
      "text/plain": [
       "       airconditioningtypeid  architecturalstyletypeid  basementsqft  \\\n",
       "count           1.626881e+06              12122.000000   3255.000000   \n",
       "mean            1.938462e+00                  7.202607    647.051920   \n",
       "std             3.154574e+00                  2.436190    538.707521   \n",
       "min             1.000000e+00                  2.000000     20.000000   \n",
       "25%             1.000000e+00                  7.000000    272.000000   \n",
       "50%             1.000000e+00                  7.000000    535.000000   \n",
       "75%             1.000000e+00                  7.000000    847.500000   \n",
       "max             1.300000e+01                 27.000000   8516.000000   \n",
       "\n",
       "        bathroomcnt    bedroomcnt  buildingclasstypeid  buildingqualitytypeid  \\\n",
       "count  5.956015e+06  5.956039e+06         25360.000000           3.879883e+06   \n",
       "mean   2.212674e+00  3.091159e+00             3.730363           6.032990e+00   \n",
       "std    1.078008e+00  1.273714e+00             0.501043           1.785322e+00   \n",
       "min    0.000000e+00  0.000000e+00             1.000000           1.000000e+00   \n",
       "25%    2.000000e+00  2.000000e+00             3.000000           4.000000e+00   \n",
       "50%    2.000000e+00  3.000000e+00             4.000000           7.000000e+00   \n",
       "75%    3.000000e+00  4.000000e+00             4.000000           7.000000e+00   \n",
       "max    3.200000e+01  2.500000e+01             5.000000           1.200000e+01   \n",
       "\n",
       "       calculatedbathnbr  decktypeid  finishedfloor1squarefeet  ...  \\\n",
       "count       5.724366e+06     34475.0             406475.000000  ...   \n",
       "mean        2.301549e+00        66.0               1380.204340  ...   \n",
       "std         1.001996e+00         0.0                633.647526  ...   \n",
       "min         1.000000e+00        66.0                  1.000000  ...   \n",
       "25%         2.000000e+00        66.0               1012.000000  ...   \n",
       "50%         2.000000e+00        66.0               1282.000000  ...   \n",
       "75%         3.000000e+00        66.0               1615.000000  ...   \n",
       "max         3.200000e+01        66.0              31303.000000  ...   \n",
       "\n",
       "       yardbuildingsqft26     yearbuilt  numberofstories  \\\n",
       "count         5293.000000  5.862673e+06     1.367745e+06   \n",
       "mean           278.331003  1.964350e+03     1.401209e+00   \n",
       "std            369.723014  2.354339e+01     5.390660e-01   \n",
       "min             10.000000  1.801000e+03     1.000000e+00   \n",
       "25%             96.000000  1.950000e+03     1.000000e+00   \n",
       "50%            168.000000  1.963000e+03     1.000000e+00   \n",
       "75%            320.000000  1.981000e+03     2.000000e+00   \n",
       "max           6141.000000  2.016000e+03     4.100000e+01   \n",
       "\n",
       "       structuretaxvaluedollarcnt  taxvaluedollarcnt  assessmentyear  \\\n",
       "count                5.868988e+06       5.893618e+06    5.956062e+06   \n",
       "mean                 1.745185e+05       4.320197e+05    2.015500e+03   \n",
       "std                  4.320903e+05       7.728019e+05    5.018861e-01   \n",
       "min                  1.000000e+00       1.000000e+00    2.000000e+03   \n",
       "25%                  7.617375e+04       1.838422e+05    2.015000e+03   \n",
       "50%                  1.248200e+05       3.133830e+05    2.016000e+03   \n",
       "75%                  2.004450e+05       5.010000e+05    2.016000e+03   \n",
       "max                  2.553212e+08       3.196225e+08    2.016000e+03   \n",
       "\n",
       "       landtaxvaluedollarcnt     taxamount  taxdelinquencyyear  \\\n",
       "count           5.842775e+06  5.916432e+06       112981.000000   \n",
       "mean            2.604776e+05  5.393300e+03           13.892053   \n",
       "std             4.663194e+05  9.432917e+03            2.571590   \n",
       "min             1.000000e+00  2.400000e-01            0.000000   \n",
       "25%             7.726000e+04  2.464850e+03           14.000000   \n",
       "50%             1.717350e+05  3.999580e+03           14.000000   \n",
       "75%             3.163500e+05  6.215350e+03           15.000000   \n",
       "max             9.401108e+07  3.823176e+06           99.000000   \n",
       "\n",
       "       censustractandblock  \n",
       "count         5.820323e+06  \n",
       "mean          6.048433e+13  \n",
       "std           3.249084e+11  \n",
       "min          -1.000000e+00  \n",
       "25%           6.037400e+13  \n",
       "50%           6.037572e+13  \n",
       "75%           6.059042e+13  \n",
       "max           4.830301e+14  \n",
       "\n",
       "[8 rows x 52 columns]"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Describtion of dataset\n",
    "df_eda.describe()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "e4430b23-7e58-45eb-bacd-589b2c8b593e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'pandas.core.frame.DataFrame'>\n",
      "Int64Index: 5970434 entries, 10754147 to 12766630\n",
      "Data columns (total 57 columns):\n",
      " #   Column                        Dtype  \n",
      "---  ------                        -----  \n",
      " 0   airconditioningtypeid         float64\n",
      " 1   architecturalstyletypeid      float64\n",
      " 2   basementsqft                  float64\n",
      " 3   bathroomcnt                   float64\n",
      " 4   bedroomcnt                    float64\n",
      " 5   buildingclasstypeid           float64\n",
      " 6   buildingqualitytypeid         float64\n",
      " 7   calculatedbathnbr             float64\n",
      " 8   decktypeid                    float64\n",
      " 9   finishedfloor1squarefeet      float64\n",
      " 10  calculatedfinishedsquarefeet  float64\n",
      " 11  finishedsquarefeet12          float64\n",
      " 12  finishedsquarefeet13          float64\n",
      " 13  finishedsquarefeet15          float64\n",
      " 14  finishedsquarefeet50          float64\n",
      " 15  finishedsquarefeet6           float64\n",
      " 16  fips                          float64\n",
      " 17  fireplacecnt                  float64\n",
      " 18  fullbathcnt                   float64\n",
      " 19  garagecarcnt                  float64\n",
      " 20  garagetotalsqft               float64\n",
      " 21  hashottuborspa                object \n",
      " 22  heatingorsystemtypeid         float64\n",
      " 23  latitude                      float64\n",
      " 24  longitude                     float64\n",
      " 25  lotsizesquarefeet             float64\n",
      " 26  poolcnt                       float64\n",
      " 27  poolsizesum                   float64\n",
      " 28  pooltypeid10                  float64\n",
      " 29  pooltypeid2                   float64\n",
      " 30  pooltypeid7                   float64\n",
      " 31  propertycountylandusecode     object \n",
      " 32  propertylandusetypeid         float64\n",
      " 33  propertyzoningdesc            object \n",
      " 34  rawcensustractandblock        float64\n",
      " 35  regionidcity                  float64\n",
      " 36  regionidcounty                float64\n",
      " 37  regionidneighborhood          float64\n",
      " 38  regionidzip                   float64\n",
      " 39  roomcnt                       float64\n",
      " 40  storytypeid                   float64\n",
      " 41  threequarterbathnbr           float64\n",
      " 42  typeconstructiontypeid        float64\n",
      " 43  unitcnt                       float64\n",
      " 44  yardbuildingsqft17            float64\n",
      " 45  yardbuildingsqft26            float64\n",
      " 46  yearbuilt                     float64\n",
      " 47  numberofstories               float64\n",
      " 48  fireplaceflag                 object \n",
      " 49  structuretaxvaluedollarcnt    float64\n",
      " 50  taxvaluedollarcnt             float64\n",
      " 51  assessmentyear                float64\n",
      " 52  landtaxvaluedollarcnt         float64\n",
      " 53  taxamount                     float64\n",
      " 54  taxdelinquencyflag            object \n",
      " 55  taxdelinquencyyear            float64\n",
      " 56  censustractandblock           float64\n",
      "dtypes: float64(52), object(5)\n",
      "memory usage: 2.6+ GB\n"
     ]
    }
   ],
   "source": [
    "# Exploring datatypes of dataset\n",
    "df_eda.info()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "e62f68d1-1e45-4dcb-8c5e-b754921abb39",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Text(0.5, 1.0, 'Available Information by Feature')"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAA0QAAARuCAYAAADziffwAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAADzk0lEQVR4nOzde7zmY73/8dfbYA6GkcjGliUNchxmUYRGyc4eoYwklaHdbJ2UtjS/2gmdRtopyWEUEybJqTA7lLPJYdYc1wzS3hm7KCUZxiDG+/fH91q5Lfc6zMxac6+Z+/18PNZjfe/r+Lm+a3Z7fVzX97tkm4iIiIiIiGa0RqMDiIiIiIiIaJQkRBERERER0bSSEEVERERERNNKQhQREREREU0rCVFERERERDStJEQREREREdG0khBFRETDSfqFpKPK9XhJd/ay38mSLummfqGk/foqzm7meY+k30taLGmX/p5vWUlaIGlMP4x7q6R/6+txIyJWpiREERGxzMovwn+TNLgvxrN9gO0f9cVYfUHSGEl/WIYu3wI+aXu47dn9FVdvSJoi6au1Zba3t31rg0LqkaQWSS4JZcfX3D4ac82+ijMiVk9JiCIiYplIagH2Bgwc1NhoBowtgAXL01HSoD6OZVW2fkkqh9veuZGBqJLfkyKaQP4PPSIiltWHgbuBKUDHMbfBkp6UtENHI0kbSXpW0uskvUbSdZL+UnaWrpP0zzVtuzx6Jem75TjaU5JmStq7U5Mhki6T9LSkWZLq/iItaQ1JEyX9r6S/SvqppA16s+AS31ckTS/z3Chpw7LuxcAgYK6k/y3t31T6PFmOqx1UM9YUSedI+m9JzwD7lqN9n5M0T9Izkn4oaeNylPBpSb+S9JqaMS6X9CdJiyTdLmn7Uj4BOBI4seyyXFvK/3F0sMT8HUmPlq/vdOz0deyMSfoPSX+W9EdJR/dwe7aSdG+J5ecd91TSNEmf6nQf50k6pDf3vKbPtpJ+KekJSb+R9L6aurGSZpd/G7+XdHJN19vL9yfLvdhDnY5Ydt5FKj+zr0maDiwB3tDd/BGxekhCFBERy+rDwNTy9S+SNrb9PHAVcERNu/cBt9n+M9X/v7mQaifl9cCzwFm9nG8GMArYAPgxcLmkITX1BwOX19T/TNJadcY5DjgEeBuwKfA34Pu9jAHgA8DRwOuAtYETbD9ve3ip39n2VmXua4EbS9tPAVMlbdNprK8B6wIdz0sdCrwT2Bp4N/AL4AvAhlT377ia/r8ARpbxZ1H9LLA9uVx/s+yyvLvOOr4IvIXqnu4M7A78Z039PwEjgM2AjwDfr03G6vgwcAzVPX0ROLOU/wj4YEejkqhuBvx3N2O9gqR1gF9S/VxfR/Xv6+yOBBB4psy/PjAW+FhNwrVP+d6x63RXL6f9EDCB6mfzlx7mj4jVQBKiiIjoNUl7USU1P7U9E/hfql/uofqlsTYh+kApw/ZfbV9pe4ntp6mSgbf1Zk7bl5T+L9r+L2AwUJtczLR9he0XgG8DQ6h+4e/s34Ev2v5DSeBOBsap98+YXGj7QdvPAj+lSijqeQswHJhk+++2bwau45X35ue2p9t+yfZzpex7th+z/QhwB3CP7dkl1quBf7yswfYFtp+uWcfOkkb0ch1HAqfa/rPtvwCnUCUBHV4o9S/Y/m9gMa+8351dbHu+7WeALwHvU3UM8OfASEkjS7sPAZfZ/ns3Yz1edtWelHQCcCCw0PaF5ec/C7gSGFfuw62228t9nAdcSi//XXVjiu0Ftl8E3tXd/BGxekhCFBERy+Io4Ebbj5fPPy5lADcDQyW9WdIWVAnD1QCShkk6T9LDkp6iOs60vnrx/Ew5vnV/OZL1JNXuxYY1TX7fcWH7JeAPVLsVnW0BXN3xCzdwP7AU2Lh3S+dPNddLqJKeejYFfl9i6fAw1e7Iq2Ku8VjN9bN1Pg+H6pkjSZNUHf17ClhY2tTek+5sWuKpja32fv21JAMdulsrvHItDwNrARuWZO2nwAdVPYtzBHBxD7FtaHv98vUtqp/Zm2uSpCepErp/Aij/1m5RdRRzEXAsvb8PvVlPt/NHxOohb16JiIhekTSU6hjcIEkdycFgqsRmZ9tzJf2U6hffx4Drym4QwH9Q7TK82fafJI0CZgPqYc69gc8D7wAW2H5J0t869du8pv0awD8Dj9YZ7vfAMbanL8u6l8OjwOaS1qhJil4PPFjTxisw/geojgnuR5UMjaA6/tdxT3oa+1Fe+RKI11P/fvXW5jXXr6faYepImH9ElQTdCSxZhmNrHX5PdezynV3U/5jq6OUBtp+T9B1eTojq3YdngGE1n+slNrX9epo/IlYD2SGKiIjeOoRqR2U7qt2fUcCbqI53fbi0+TFwONV/Rf9xTd91qXY5niwP3X+5l3OuS/Vcyl+ANSWdBKzXqc1oSe8tR98+AzxP9dKHzs4FvlZ2rzpe+nBwL+NYFvdQ/eJ9oqS1VP39n3cDP+mj8delWuNfqX65/3qn+seAN3TT/1LgP8v6NwROArr8W0698EFJ20kaBpwKXGF7KUBJgF4C/oued4fquQ7YWtKHyr1cS9Jukt5U6tcFnijJ0O68fHwTqn8zL/HKezEH2EfS68sRw/+3gvNHxGogCVFERPTWUVTP0fyf7T91fFH9F/ojJa1puyMZ2JTqwf8O3wGGUu0c3A1c38s5byjjPEh1HOs5Xn3c7OdUSdjfqJ5TeW95nqiz7wLXADdKerrE8eZextFr5RmZg4ADqNZ7NvBh2w/00RQXUd2LR4D7eHXy90Ngu3LE62d1+n8VaAPmAe1UL2X4ap12vXUx1RsH/0T1/NZxneovAnZkOZKussO4P/B+ql2sPwGnUe1MAnwcOLX8PE+iOqLX0XcJ1bNq08u9eIvtXwKXUa19JlXCsyLzR8RqQPaK7NpHREREdE3Sh4EJtvdqdCwREfVkhygiIiL6RTlG93FgcqNjiYjoShKiiIiI6HOS/oXqOZ7HeOXzZBERA0qOzEVERERERNPKDlFERERERDStJEQREREREdG08odZo6E23HBDt7S0NDqMiIiIiFiNzZw583HbG9WrS0IUDdXS0kJbW1ujw4iIiIiI1Zikh7uqy5G5iIiIiIhoWkmIIiIiIiKiaeXIXDRU+yOLaJk4rdFhREREREQ/WjhpbKND6FJ2iCIiIiIiomklIYpuSTpO0v2S/iZpYqPjiYiIiIjoSzkyFz35OHCA7YcaHUhERERERF/LDlF0SdK5wBuAayQdL+msUj5F0rmS7pD0oKQDS/n2ku6VNEfSPEkjGxl/RERERERPkhBFl2wfCzwK7Av8rVN1C/A2YCxwrqQhwLHAd22PAlqBP9QbV9IESW2S2pYuWdRP0UdERERE9CwJUSyvn9p+yfZvgd8B2wJ3AV+Q9HlgC9vP1utoe7LtVtutg4aNWIkhR0RERES8UhKiWF7u/Nn2j4GDgGeBGyS9feWHFRERERHRe0mIYnkdJmkNSVtRPWf0G0lvAH5n+0zgGmCnhkYYEREREdGDvGUultdvgNuAjYFjbT8n6XDgg5JeAP4EnNrIACMiIiIieiK788mniO5JmgJcZ/uKFR2rtbXVbW1tKx5UREREREQXJM203VqvLkfmIiIiIiKiaeXIXCwz2+MbHUNERERERF/IDlFERERERDStJEQREREREdG0khBFRERERETTSkIUERERERFNKwlRREREREQ0rSREERERERHRtPLa7Wio9kcW0TJxWqPDiIiIiIh+tHDS2EaH0KXsEEVERERERNNKQtRAkk6VtN9y9j1I0sQu6havWGTLFMcXVtZcERERERF9LQlRH1Flme6n7ZNs/2p55rN9je1Jy9O3jyUhioiIiIhVVhKiFSCpRdL9ks4GZgFfkjRD0jxJp9S0+5KkByT9UtKlkk4o5VMkjSvX75A0W1K7pAskDS7lCyWdImlWqdu2lI+XdFa53lLSXWXur3SK8cTSb66kSaVslKS7S5xXS3pNKb9VUmu53lDSwpq5rpJ0vaTfSvpmKZ8EDJU0R9JUSV+R9Omaub8m6bj+uPcREREREX0hCdGK2wa4CPg8sBmwOzAKGC1pn5JgHArsArwXaO08gKQhwBTgcNs7Ur3s4mM1TR63vStwDnBCnRi+C5xjezfgTzXjHgAcArzZ9s7AN0vVRcDnbe8EtANf7sU6RwGHAzsCh0va3PZE4Fnbo2wfCfwQOKrMvQbwfmBqnfVOkNQmqW3pkkW9mDoiIiIion8kIVpxD9u+G9i/fM2m2i3aFhgJ7AX83Paztp8Grq0zxjbAQ7YfLJ9/BOxTU39V+T4TaKnT/63ApeX64pry/YALbS8BsP2EpBHA+rZv62Kurtxke5Ht54D7gC06N7C9EPirpF0o98L2X+u0m2y71XbroGEjejF1RERERET/yGu3V9wz5buAb9g+r7ZS0vG9GEM91D9fvi+l65+Zuxi3XnlXXuTlJHlIFzH0FMcPgPHAPwEXLMPcERERERErXXaI+s4NwDGShgNI2kzS64A7gXdLGlLq6r2E/QGgRdIby+cPAbfVadeV6VTH0wCOrCm/scQ0rMS0ge1FwN8k7V1nroXA6HI9rpdzvyBprZrPVwPvAnajuicREREREQNWdoj6iO0bJb0JuEsSwGLgg7ZnSLoGmAs8DLQBizr1fU7S0cDlktYEZgDnLsP0nwZ+XF5ocGXNuNdLGgW0Sfo78N9Ub4U7Cji3JEq/A44uXb4F/FTSh4Cbezn3ZGCepFm2j7T9d0m3AE/aXroMa4iIiIiIWOlkL8uJqlgekobbXlwSkNuBCbZnNTqu/lBepjALOMz2b3tq39ra6ra2tv4PLCIiIiKalqSZtl/1cjPIkbmVZbKkOVSJwpWrcTK0HfA/VC9g6DEZioiIiIhotByZWwlsf6DRMawMtu8D3tDoOCIiIiIieis7RBERERER0bSSEEVERERERNNKQhQREREREU0rCVFERERERDStJEQREREREdG0khBFRERERETTSkIUERERERFNK3+HKBqq/ZFFtEyc1ugwIiIiIqIfLZw0ttEhdCk7RKsRSYt7qF9f0sdrPm8q6YpyPUrSvy7HnCdLOmHZo42IiIiIaLwkRM1lfeAfCZHtR22PKx9HAcucEEVERERErMqSEK2GJA2XdJOkWZLaJR1cqiYBW0maI+l0SS2S5ktaGzgVOLzUHd5556e0aynXX5T0G0m/ArapabOVpOslzZR0h6RtV96qIyIiIiKWXZ4hWj09B7zH9lOSNgTulnQNMBHYwfYogI4Ex/bfJZ0EtNr+ZKk7ud7AkkYD7wd2ofr3MwuYWaonA8fa/q2kNwNnA2+vM8YEYALAoPU26ov1RkREREQslyREqycBX5e0D/ASsBmwcR+NvTdwte0lACXRQtJwYE/gckkdbQfXG8D2ZKrkicGbjHQfxRURERERscySEK2ejgQ2AkbbfkHSQmDIMo7xIq88Ulnbv14SswbwZMfuU0RERETEqiDPEK2eRgB/LsnQvsAWpfxpYN0u+nSuWwjsCiBpV2DLUn478B5JQyWtC7wbwPZTwEOSDit9JGnnvltSRERERETfS0K0epoKtEpqo9otegDA9l+B6eUFCad36nMLsF3HSxWAK4ENJM0BPgY8WMaYBVwGzClt7qgZ40jgI5LmAguAg4mIiIiIGMBk5xGOaJzW1la3tbU1OoyIiIiIWI1Jmmm7tV5ddogiIiIiIqJpJSGKiIiIiIimlYQoIiIiIiKaVhKiiIiIiIhoWkmIIiIiIiKiaSUhioiIiIiIppWEKCIiIiIimlYSooiIiIiIaFpJiCIiIiIiommt2egAorm1P7KIlonTGh1GRERERPSjhZPGNjqELmWHqAeSxkjasxftpkga10dz3iqptS/GKuO1SPpAX43Xaexfd1HeZ/cjIiIiIqK/rBYJkaRB/TTumsAYoMeEaIBrAfolIbK9qt+biIiIiGhiAz4hKrsbD0j6kaR5kq6QNEzSQkknSboTOEzSEZLaJc2XdFpN/8WS/kvSLEk3SdqolG8l6XpJMyXdIWnbUj5F0rcl3QJcBhwLHC9pjqS9JT0kaa3Sdr0Sx1qdYj5J0owSy2RJKuW3SjpN0r2SHpS0dykfKuknZX2XAUNr46+5HidpSrk+rIw/V9LtpWyQpNPL3PMk/XvpOgnYu6zh+LLeUTXjTpe0k6STJV0s6WZJv5X00Zo2n6sZ95TO8alylqT7JE0DXrd8P/GIiIiIiJVnwCdExTbAZNs7AU8BHy/lz9neC7gdOA14OzAK2E3SIaXNOsAs27sCtwFfLuWTgU/ZHg2cAJxdM9/WwH62DwXOBc6wPcr2HcCtQMchyPcDV9p+oVO8Z9nezfYOVMnNgTV1a9reHfhMTSwfA5aU9X0NGN2Le3IS8C+2dwYOKmUfARbZ3g3YDfiopC2BicAdZQ1nAD8AxgNI2hoYbHteGWOnsr49gJMkbSppf2AksDvV/R0taZ9O8byH6ue0I/BRutlVkzRBUpuktqVLFvViqRERERER/WNVSYh+b3t6ub4E2KtcX1a+7wbcavsvtl8EpgIdv7C/VNPuEmAvScOpfmG/XNIc4Dxgk5r5Lre9tItYfgAcXa6PBi6s02ZfSfdIaqdK0ravqbuqfJ9JdZSNEuslACUxmUfPpgNTyi5Ox5HB/YEPlzXdA7yWKpHp7HLgwLKzdQwwpabu57aftf04cAtVErR/+ZoNzAK2rTPuPsCltpfafhS4uavAbU+23Wq7ddCwEb1YakRERERE/1hV3jLnLj4/U75rGcdaA3jS9qgu2jzTRTm2p5djfG8DBtmeX1svaQjVblOr7d9LOhkYUtPk+fJ9Ka+8/53XWK/8H+PYPlbSm6l2c+aUI3Ci2vW6oVNMYzqtYYmkXwIHA+8Dal/gUO9eC/iG7fO6iLGnNUREREREDEiryg7R6yXtUa6PAO7sVH8P8DZJG5YXLBxBdTwOqjV2vO3sA8Cdtp8CHpJ0GPzj+Zedu5j7aWDdTmUXAZdSf3eoI2l5vOxE9eZNa7cDR5ZYdqA6ttbhMUlvkrQG1bE0SrutbN9j+yTgcWBz4AbgYzXPOG0taZ0u1vAD4Exghu0nasoPljRE0mupXigxo4x7TFkPkjaT1PkZoduB95fnmDYB9u3FuiMiIiIiGmpVSYjuB46SNA/YADinttL2H4H/R3XEay7VM0M/L9XPANtLmkl1fO3UUn4k8BFJc4EFVLsl9VwLvKfjpQqlbCrwGqqk6BVsPwmcD7QDP6NKKHpyDjC8rO9E4N6auonAdVRH0P5YU366ykskqJKRuVRJzn3ArFJ+HtUu1DzgxfIChuNLnDOpnsfqnNTdC0wD7ga+YvtR2zcCPwbuKscAr+DVCdbVwG/Lus/h5YQ0IiIiImLAkj2wTzlJagGuKy8oWJ7+i20P7+OYxgEH2/5QX467MknalOoFEdvafqmUnQwstv2tlRVHa2ur29raVtZ0EREREdGEJM20XffvfK4qzxANGJK+BxwA/GujY1lekj5M9Ta7z3YkQxERERERzWjA7xDF6i07RBERERHR37rbIVpVniGKiIiIiIjoc0mIIiIiIiKiaSUhioiIiIiIppWEKCIiIiIimlYSooiIiIiIaFpJiCIiIiIiomklIYqIiIiIiKaVP8waDdX+yCJaJk5rdBgRERER0Y8WThrb6BC6lB2ifiDpM5KG9dFYLZI+0BdjlfHGSLquXJ8s6YQu5py/nOMvlLThisYZEREREbEyDKiESJUBFdNy+gxQNyGSNGgZx2oB+iwhioiIiIiIlzU8+Si7EfdLOhuYBfxQUpukBZJOKW12l3RVuT5Y0rOS1pY0RNLvSvkbJf1K0lxJsyRtVco/J2mGpHk143XMeX6Z50ZJQ0vdcZLuK+1/UspesZMiaX4ZYx1J08qc8yUdLuk4YFPgFkm3lPaLJZ0q6R5gD0knlZjmS5osSd2sYRKwt6Q5ko4v895R6mdJ2rP0HSPpVklXSHpA0tSacd9Vyu4E3tvpR7CzpJsl/VbSR+v8fIZIulBSu6TZkvYt5YMkfauUz5P0qU79hkq6vt6YEREREREDxUB5hmgb4GjbH5e0ge0nyk7KTZJ2okqUdilt9wbmA7tRxX9PKZ8KTLJ9taQhwBqS9gdGArsDAq6RtA/wf6X8CNsflfRT4FDgEmAisKXt5yWt30Pc7wIetT0WQNII24skfRbY1/bjpd06wHzbJ5V299k+tVxfDBwIXFtvDSWeE2wfWNoPA95p+zlJI4FLgdYyzy7A9sCjwHTgrZLagPOBtwP/A1zWaQ07AW8pMc6W1PmBnk8A2N5R0rbAjZK2Bo4GtgR2sf2ipA1q+gwHfgJcZPuizjdN0gRgAsCg9Tbq7v5GRERERPSrhu8QFQ/bvrtcv0/SLGA21S/329l+EfgfSW+iSm6+DexDlRzdIWldYDPbVwPYfs72EmD/8jWbKqnalioRAnjI9pxyPZPqaBrAPGCqpA8CL/YQdzuwn6TTJO1te1EX7ZYCV9Z83lfSPZLaqRKV7btZQ2drAeeXvpcD29XU3Wv7D7ZfAuaUNW1b1vpb26ZK+mr93PazJXm7her+1toLuLjE9ADwMLA1sB9wbvnZYPuJ2jGBC+slQ6XtZNuttlsHDRtRr0lERERExEoxUBKiZwAkbQmcALzD9k7ANGBIaXMHcADwAvArql/U9wJup9r9qUfAN2yPKl9vtP3DUvd8TbulvLxbNhb4PjAamClpTarEqPZeDQGw/WBp1w58Q9JJXcTxnO2lZY1DgLOBcbZ3pNq9GdLNGjo7HngM2JlqZ2jtmrqu1uRuxutc1/lzd/e2q3GnAwd0HNmLiIiIiBioBkpC1GE9quRokaSNqRKgDrdTvazgLtt/AV5LtfuxwPZTwB8kHQIgaXA5WnYDcIyk4aV8M0mv62pyVS902Nz2LcCJwPpUx78WAruWNrtSHRVD0qbAEtuXAN/qaAM8DazbxTQdCd7jJa5xAN2sofNYI4A/ll2gDwE9vaThAWDL8jwSwBGd6g8uzwm9FhgDzOhUfztwZIlpa+D1wG+AG4FjS8JIpyNzJwF/pUr8IiIiIiIGrAGVENmeS3W8bQFwAdVOQ4d7gI2pfkGH6mjbvHIMDKrk4DhJ84BfA/9k+0bgx8Bd5YjZFXSdqECVXFxS2s4GzrD9JNVxtw0kzQE+BjxY2u8I3FvKvwh8tZRPBn7R8VKFTmt8kmpXqB34Ga9MQF61hrLOF8uLFo6nSjKOknQ31dG1Z7pZD7afo3peZ1p5qcLDnZrcS7UTdzfwFduPdqo/GxhU7sllwHjbzwM/oHoWa56kubz6TXifAYZI+mZ38UVERERENJJeziciVr7W1la3tbU1OoyIiIiIWI1Jmmm7tV7dgNohioiIiIiIWJmSEEVERERERNNKQhQREREREU0rCVFERERERDStJEQREREREdG0khBFRERERETTSkIUERERERFNKwlRREREREQ0rSREERERERHRtNZsdADR3NofWUTLxGmNDiMiIiIi+tHCSWMbHUKXskO0ipC0uB/GPEjSxHJ9iKTtlmOMWyW19nVsERERERErQxKiJmb7GtuTysdDgGVOiCIiIiIiVmVJiFYxqpwuab6kdkmHl/IxZbfmCkkPSJoqSaXuX0vZnZLOlHRdKR8v6SxJewIHAadLmiNpq9qdH0kbSlpYrodK+omkeZIuA4bWxLa/pLskzZJ0uaThK/fuREREREQsmzxDtOp5LzAK2BnYEJgh6fZStwuwPfAoMB14q6Q24DxgH9sPSbq084C2fy3pGuA621cAlFyqno8BS2zvJGknYFZpvyHwn8B+tp+R9Hngs8CpfbDmiIiIiIh+kYRo1bMXcKntpcBjkm4DdgOeAu61/QcASXOAFmAx8DvbD5X+lwITVmD+fYAzAWzPkzSvlL+F6sjd9JJMrQ3cVW8ASRM6Yhi03kYrEEpERERExIpJQrTq6XLrBni+5nop1c+3u/bdeZGXj1QO6VTnLuL6pe0jehrY9mRgMsDgTUbWGysiIiIiYqXIM0SrntuBwyUNkrQR1Y7Nvd20fwB4g6SW8vnwLto9Daxb83khMLpcj+s0/5EAknYAdirld1Md0XtjqRsmaeveLCgiIiIiolGSEK16rgbmAXOBm4ETbf+pq8a2nwU+Dlwv6U7gMWBRnaY/AT4nabakrYBvAR+T9GuqZ5U6nAMML0flTqQkY7b/AowHLi11dwPbrshCIyIiIiL6m+ycWFrdSRpue3F569z3gd/aPqPRcQG0tra6ra2t0WFERERExGpM0kzbdf92ZnaImsNHy0sWFgAjqN46FxERERHR9PJShSZQdoMGxI5QRERERMRAkh2iiIiIiIhoWkmIIiIiIiKiaSUhioiIiIiIppWEKCIiIiIimlYSooiIiIiIaFpJiCIiIiIiomklIYqIiIiIiKaVv0MUDdX+yCJaJk5rdBgRERER0Y8WThrb6BC6lB2iJiZpvKRNGx1HRERERESjJCFqbuOBfk2IJGUXMiIiIiIGrCREvSDpZ5JmSlogaYKkQZKmSJovqV3S8aXdcZLukzRP0k9K2TqSLpA0Q9JsSQeX8u0l3StpTmk/srSdJmluGfvw0nahpK9LuktSm6RdJd0g6X8lHVsT5+fKPPMknVLKWiTdL+n8Ev+NkoZKGge0AlNLDGMlXV0z1jslXVWu9y9zz5J0uaThpfykMt98SZMlqZTfWuK9Dfj0SvgRRUREREQsl/zX+945xvYTkoYCM4CZwGa2dwCQtH5pNxHY0vbzNWVfBG62fUwpu1fSr4Bjge/anippbWAQ8K/Ao7bHlnFH1MTwe9t7SDoDmAK8FRgCLADOlbQ/MBLYHRBwjaR9gP8r5UfY/qiknwKH2r5E0ieBE2y3lWTmvyRtZPsvwNHAhZI2BP4T2M/2M5I+D3wWOBU4y/apJdaLgQOBa0u869t+2wrd9YiIiIiIfpYdot45TtJc4G5gc2Bt4A2SvifpXcBTpd08qh2XDwIvlrL9gYmS5gC3UiUxrwfuAr5QEowtbD8LtAP7STpN0t62F9XEcE353g7cY/vpkrg8VxKt/cvXbGAWsC1VIgTwkO055Xom0NJ5gbYNXAx8sIy3B/AL4C3AdsD0soajgC1Kt30l3SOpHXg7sH3NkJd1dTPLLlubpLalSxZ11SwiIiIiot9lh6gHksYA+wF72F4i6VZgMLAz8C/AJ4D3AccAY4F9gIOAL0nanmq35lDbv+k09P2S7il9bpD0b7ZvljSaaqfoG5Ju7NiBAZ4v31+que74vGaZ5xu2z+sUf0un9kuBoV0s90KqHZ7ngMttv1h2jn5p+4hO4w4BzgZabf9e0slUyV6HZ7qYA9uTgckAgzcZ6a7aRURERET0t+wQ9WwE8LeSDG1LtWOyIbCG7SuBLwG7SloD2Nz2LcCJwPrAcOAG4FM1z9fsUr6/Afid7TOpdn92Km98W2L7EuBbwK7LEOcNwDE1z/dsJul1PfR5Gli344PtR4FHqY7ITSnFdwNvlfTGMu4wSVvzcvLzeJlz3DLEGhERERExIGSHqGfXA8dKmgf8hipB2Ay4tSRBAP+P6hmgS8pzPwLOsP2kpK8A3wHmlaRoIdWzNodTHU97AfgT1TM5uwGnS3oJeAH4WG+DtH2jpDcBd5XcazHwQaodoa5MoXr+6FmqHbBnganARrbvK+P+RdJ44FJJg0u//7T9oKTzqY7wLaR6tioiIiIiYpWi6tGRiIqks4DZtn+4MuZrbW11W1vbypgqIiIiIpqUpJm2W+vVZYco/kHSTKpnf/6j0bFERERERKwMSYjiH2yPbnQMERERERErU16qEBERERERTSsJUURERERENK0kRBERERER0bSSEEVERERERNNKQhQREREREU0rCVFERERERDStJEQREREREdG08neIoqHaH1lEy8RpjQ4jIiIiIvrRwkljGx1Cl7JD1CCSWiTNX9l9VxZJX2h0DBERERERPUlCtBqRNJB2/JIQRURERMSAN5B+gW5Ga0r6EbAL8CDwYeBNwLeB4cDjwHjbf5Q0GrgAWALc2TGApPHAWGAIsI6kcaXdG0rbCbbnSdqgi/KTgS2BTYCtgc8CbwEOAB4B3m37BUm7Ad8F1gGeB94BHAocBAwDtgKutn2ipEnAUElzgAW2j+zrGxcRERER0ReyQ9RY2wCTbe8EPAV8AvgeMM52RwL0tdL2QuA423vUGWcP4CjbbwdOAWaXMb8AXFTadFUOVTIzFjgYuAS4xfaOwLPAWElrA5cBn7a9M7BfqQMYBRwO7AgcLmlz2xOBZ22PSjIUEREREQNZdoga6/e2p5frS6gSlR2AX0oCGAT8UdIIYH3bt5W2F1Pt4HT4pe0nyvVeVDs32L5Z0mtL/67KAX5RdoHay5zXl/J2oIUqcfuj7Rml/1MAJcabbC8qn+8DtgB+392iJU0AJgAMWm+j3tyniIiIiIh+kYSosdzp89NUR8xesQskaf06bWs9U9u8i3m6KofqCBy2X5L0gu2O8peo/o2om/mfr7leSi/+TdmeDEwGGLzJyO7WFRERERHRr3JkrrFeL6kj+TkCuBvYqKNM0lqStrf9JLBI0l6lbXfH0G7vqJc0Bni87Oh0Vd4bDwCblueIkLRuL17g8IKktXo5fkREREREQ2SHqLHuB46SdB7wW6rnh24AzizH2dYEvgMsAI4GLpC0pLTpysnAhZLmUb084ageyntk+++SDge+J2ko1fND+/XQbTIwT9KsPEcUEREREQOVXj4dFbHytba2uq2trdFhRERERMRqTNJM26316nJkLiIiIiIimlYSooiIiIiIaFpJiCIiIiIiomklIYqIiIiIiKaVhCgiIiIiIppWEqKIiIiIiGhaSYgiIiIiIqJpJSGKiIiIiIimlYQoIiIiIiKa1pqNDiCaW/sji2iZOK3RYUREREREP1o4aWyjQ+hSdohWEkktkuYvQ/vxkjat+bxQ0ob9E13fkrS+pI83Oo6IiIiIiJ4kIRq4xgOb9tSolqSBsuO3PpCEKCIiIiIGvCREK9eakn4kaZ6kKyQNk3SSpBmS5kuarMo4oBWYKmmOpKGl/6ckzZLULmlbAEknl343AhdJ2kLSTWWOmyS9vrTrqnyKpHMk3SLpd5LeJukCSfdLmtIRuKR3lbnnSrqpZu4LJN1a+h5Xmk8Ctiqxn75S7mxERERExHJIQrRybQNMtr0T8BTVLspZtnezvQMwFDjQ9hVAG3Ck7VG2ny39H7e9K3AOcELNuKOBg21/ADgLuKjMMRU4s7TpqhzgNcDbgeOBa4EzgO2BHSWNkrQRcD5wqO2dgcNq+m4L/AuwO/BlSWsBE4H/LbF/boXuWEREREREP0pCtHL93vb0cn0JsBewr6R7JLVTJSXbd9P/qvJ9JtBSU35NTdK0B/Djcn1xmaO7coBrbRtoBx6z3W77JWBBmectwO22HwKw/URN32m2n7f9OPBnYONu4gdA0gRJbZLali5Z1FPziIiIiIh+k4Ro5XKdz2cD42zvSLULM6Sb/s+X70t55RsCn1mGOeuVd4z7Us11x+c1AXUzTm37znHVn9iebLvVduugYSN6ah4RERER0W+SEK1cr5e0R7k+ArizXD8uaTgwrqbt08C6yzHHr4H3l+sja+boqrw37gLeJmlLAEkb9NB+eWOPiIiIiFipBspbyZrF/cBRks4Dfkv1LNBrqI6qLQRm1LSdApwr6Vmq4269dRxwgaTPAX8Bju6hvEe2/yJpAnCVpDWojsa9s5v2f5U0vbxm/Bd5jigiIiIiBipVj45ENEZra6vb2toaHUZERERErMYkzbTdWq8uR+YiIiIiIqJpJSGKiIiIiIimlYQoIiIiIiKaVhKiiIiIiIhoWkmIIiIiIiKiaSUhioiIiIiIppWEKCIiIiIimlYSooiIiIiIaFpJiCIiIiIiommt2egAorm1P7KIlonTGh1GRERERPSjhZPGNjqELmWHKPqcpPGSNm10HBERERERPUlCtBpRZSD8TMcDSYgiIiIiYsAbCL88xwqQ1CLpfklnA7OAH0qaL6ld0uGljSSdXqd8jKTbJP1U0oOSJkk6UtK9pd1Wpd3Gkq6WNLd87Vkz7/mSFki6UdJQSeOAVmCqpDmShjbq3kRERERE9CQJ0ephG+Ai4KvAPwM7A/sBp0vaBHgvMKpOOaXs08COwIeArW3vDvwA+FRpcyZwm+2dgV2BBaV8JPB929sDTwKH2r4CaAOOtD3K9rP9teiIiIiIiBWVhGj18LDtu4G9gEttL7X9GHAbsFs35QAzbP/R9vPA/wI3lvJ2oKVcvx04B6CMsaiUP2R7TrmeWdO+W5ImSGqT1LZ0yaKeO0RERERE9JMkRKuHZ8p3dVHfVTnA8zXXL9V8fome30JY23dpL9oDYHuy7VbbrYOGjehNl4iIiIiIfpGEaPVyO3C4pEGSNgL2Ae7tpry3bgI+BlDGWK+H9k8D6y5z9BERERERK1kSotXL1cA8YC5wM3Ci7T91U95bnwb2ldROdTRu+x7aTwHOzUsVIiIiImKgk+1GxxBNbPAmI73JUd9pdBgRERER0Y8a/YdZJc203VqvrlfPfET0lx03G0HbAP7LxRERERGxesuRuYiIiIiIaFpJiCIiIiIiomklIYqIiIiIiKaVhCgiIiIiIppWEqKIiIiIiGhaSYgiIiIiIqJpJSGKiIiIiIimlYQoIiIiIiKaVv4wazRU+yOLaJk4rdFhREREREQ/WjhpbKND6NIqu0MkaYykPVeVOCRNkTSuj+a8VVJrX4y1oiS1SJrf6DgiIiIiIpZHvyZEkgb107hrAmOAhidEDJw4IiIiIiJiGS13QlR2Bh6Q9CNJ8yRdIWmYpIWSTpJ0J3CYpCMktUuaL+m0mv6LJf2XpFmSbpK0USnfStL1kmZKukPStqV8iqRvS7oFuAw4Fjhe0hxJe0t6SNJape16JY61JL1R0q8kzS1zbaXK6SWmdkmHl35jJF1XE+NZksaX64WSTiljtEvaVlJLb+PodO9OkjSjzD9Zkkr5rZJOk3SvpAcl7V3Kh0r6SbnPlwFDa+9jzfU4SVPK9WFl/LmSbi9lg8q6Z5Sx/r2m74llXXMlTSployTdXdpeLek1pXx0aXcX8ImaMbocPyIiIiJiIFrRHaJtgMm2dwKeAj5eyp+zvRdwO3Aa8HZgFLCbpENKm3WAWbZ3BW4DvlzKJwOfsj0aOAE4u2a+rYH9bB8KnAucYXuU7TuAW4GOw4nvB660/QIwFfi+7Z2pdnL+CLy3xLMzsB9wuqRNerHex0u85wAn2F64DHHUOsv2brZ3oEpuDqypW9P27sBnau7Jx4Al5T5/DRjdi1hPAv6lrPugUvYRYJHt3YDdgI9K2lLSAcAhwJtL+2+W9hcBny/zttfEcyFwnO09Os1Zd/xexBoRERER0RArmhD93vb0cn0JsFe5vqx83w241fZfbL9IlZzsU+peqml3CbCXpOFUScvlkuYA5wG1icrltpd2EcsPgKPL9dHAhZLWBTazfTWA7edsLylxXmp7qe3HqBKy3Xqx3qvK95lAS2/jqNNmX0n3SGqnSha372GOfajuEbbnAfN6Eet0YIqkjwIdRxf3Bz5c7u09wGuBkVRJ4YXl3mD7CUkjgPVt31b6/gjYp075xTVzdjX+K0iaIKlNUtvSJYt6sZSIiIiIiP6xom+ZcxefnynftYxjrQE8aXtUF22e6aIc29NVHeN7GzDI9nxJ63XRvKu4XuSVSeKQTvXPl+9L6eLe1YvjFRNLQ6h2vVpt/17SyZ3m6WqOzve6Xvk/xrF9rKQ3U+1WzZE0imrdn7J9Q6eY3tXN+J2pm7Z1x39VwPZkqp1ABm8ysrfzRkRERET0uRXdIXq9pI5jU0cAd3aqvwd4m6QNVb1g4Qiq3ZiOuTveuvYB4E7bTwEPSToMoDzrs3MXcz8NrNup7CLgUsquTBnvDx3H9CQNljSM6ijf4eWZl42odmDuBR4GtivtRgDv6MU96DGOTjqSlsfLjlhv3jx3O3BkWcMOwE41dY9JepOkNYD3dBRK2sr2PbZPAh4HNgduAD5W84zT1pLWAW4Ejin3Bkkb2F4E/K3jOSbgQ8Bttp8EFknq2A08siaWrsaPiIiIiBiQVjQhuh84StI8YAOqZ2v+wfYfgf8H3ALMpXpm6Oel+hlge0kzqY6NnVrKjwQ+ImkusAA4uIu5rwXe0/Eyg1I2FXgNVTLS4UPAcSXGXwP/BFxNdexsLnAzcKLtP9n+PfDTUjcVmN2Le9DbODruyZPA+VTP5PwMmNGLOc4Bhpc1nEiVvHWYCFxX1vHHmvLTy0sS5lMlVHOpjvPdB8wq5edRPbN0PXAN0FaOu51QxjiqjDOP6pmrjp/R0cD3y0sVnq2Zs+74vVhfRERERERDyF6+E0uq3rB2XXkxwPL0X2x7+HJN3vWY44CDbX+oL8ddVeNYFQzeZKQ3Oeo7jQ4jIiIiIvpRo/8wq6SZtuv+Hc/V5r/eS/oecADwr4lj1bHjZiNoG8B/uTgiIiIiVm/LnRCVV04v1+5Q6d+nu0O2P9WX4y2vgRJHRERERET0bEWfIYqIiIiIiFhlJSGKiIiIiIimlYQoIiIiIiKaVhKiiIiIiIhoWkmIIiIiIiKiaSUhioiIiIiIppWEKCIiIiIimtZq84dZY9XU/sgiWiZOa3QYEREREdFPFk4a2+gQupUdooiIiIiIaFpJiFYTkk6VtN9y9j1I0sQu6hb30PfXyzNnRERERMRAkCNzA5AkAbL9Um/72D5peeezfQ1wzXL23XN5542IiIiIaLTsEA0Qklok3S/pbGAW8CVJMyTNk3RKTbsvSXpA0i8lXSrphFI+RdK4cv0OSbMltUu6QNLgUr5Q0imSZpW6bUv5eElnlestJd1V5v5KzbynSppTvh6RdGEpX1y+j5F0u6SrJd0n6VxJ+fcVEREREQNafmEdWLYBLgI+D2wG7A6MAkZL2kdSK3AosAvwXqC18wCShgBTgMNt70i1C/ixmiaP294VOAc4oU4M3wXOsb0b8KeOQtsn2R4FvA34K3BWnb67A/8B7AhsVWJ8FUkTJLVJalu6ZFHdGxERERERsTIkIRpYHrZ9N7B/+ZpNtVu0LTAS2Av4ue1nbT8NXFtnjG2Ah2w/WD7/CNinpv6q8n0m0FKn/1uBS8v1xbUV5SjfVOAM2zPr9L3X9u9sLy1j7FVvkbYn22613Tpo2Ih6TSIiIiIiVoo8QzSwPFO+C/iG7fNqKyUd34sx1EP98+X7Urr++buL8pOBP9i+sJf9uhonIiIiImJAyA7RwHQDcIyk4QCSNpP0OuBO4N2ShpS6ei91fwBokfTG8vlDwG3LMPd04P3l+siOQkkHAu8Ejuum7+7lGaQ1gMNLvBERERERA1YSogHI9o3Aj4G7JLUDVwDr2p5B9Ta4uVRH39qARZ36PgccDVxe+r4EnLsM038a+ISkGUDtebb/ADYF7i0vVji1Tt+7gEnAfOAh4OplmDciIiIiYqWTnVNNqxJJw20vljQMuB2YYHvWAIhrDHCC7QOXpV9ra6vb2tr6JaaIiIiICABJM22/6oVkkGeIVkWTJW0HDAF+NBCSoYiIiIiIVVUSolWM7Q80OoZ6bN8K3NrgMCIiIiIilkmeIYqIiIiIiKaVhCgiIiIiIppWEqKIiIiIiGhaSYgiIiIiIqJpJSGKiIiIiIimlYQoIiIiIiKaVl67HQ3V/sgiWiZOa3QYEREREdGHFk4a2+gQei07RBERERER0bSSEA0gktaX9PFGx7GiJI2XtGmj44iIiIiI6EkSooFlfWCVT4iA8UASooiIiIgY8JIQDSyTgK0kzZF0hqSbJM2S1C7pYABJu0maJ2mIpHUkLZC0g6ThXbRvkfSApB9Imi9pqqT9JE2X9FtJu5d2G0j6WRn7bkk7lfKTJZ3QEWAZo6V83S/p/BLDjZKGShoHtAJTyzqGrvS7GBERERHRS3mpwsAyEdjB9ihJawLDbD8laUPgbknX2J4h6Rrgq8BQ4BLb80v793RuX8Z9I3AYMAGYAXwA2As4CPgCcAhwCjDb9iGS3g5cBIzqId6RwBG2Pyrpp8Chti+R9EngBNtt9TpJmlBiYdB6Gy37XYqIiIiI6CNJiAYuAV+XtA/wErAZsDHwJ+BUqsTmOeC4HtoDPGS7HUDSAuAm25bUDrSUNnsBhwLYvlnSayWN6CHGh2zPKdcza8bqlu3JwGSAwZuMdG/6RERERET0hyREA9eRwEbAaNsvSFoIDCl1GwDDgbVK2TM9tH++ZtyXaj6/xMv/BlQnBgMv8sqjlUNqrmvHXUq1YxURERERscrIM0QDy9PAuuV6BPDnktzsC2xR024y8CVgKnBaL9r3xu1USRWSxgCP234KWAjsWsp3BbZcxnVERERERAxY2SEaQGz/tbzsYD7VkbhtJbUBc4AHACR9GHjR9o8lDQJ+XZ75mQpc27n9MjgZuFDSPGAJcFQpvxL4sKQ5JaYHezHWFOBcSc8Ce9h+dhljiYiIiIhYKWTnEY5onNbWVre11X33QkREREREn5A003ZrvbocmYuIiIiIiKaVhCgiIiIiIppWEqKIiIiIiGhaSYgiIiIiIqJpJSGKiIiIiIimlYQoIiIiIiKaVhKiiIiIiIhoWkmIIiIiIiKiaa3Z6ACiubU/soiWidMaHUZERERE9IGFk8Y2OoRllh2iiIiIiIhoWkmICknrS/r4SpqrRdL8PhprjKTremgzXtJZ5fpkSSf0xdzdzPcZScP6c46IiIiIiL6QhOhl6wMrJSFalUhanmOVnwGSEEVERETEgJeE6GWTgK0kzZF0hqSbJM2S1C7pYABJu0maJ2mIpHUkLZC0g6TLJP1rx0CSpkg6tOwE3VHGmSVpz86T1u7elM/XSRpTrveXdFfpe7mk4aX8XZIekHQn8N6avhtI+lmJ8W5JO3W3YEkflTRD0lxJV3bs6pT4vy3pFuA0SW+U9KvSbpakrcrO1K2SriixTFXlOGBT4JbSPyIiIiJiwMpLFV42EdjB9qiyKzLM9lOSNgTulnSN7RmSrgG+CgwFLrE9X9JPgMOB/5a0NvAO4GOAgHfafk7SSOBSoLU3wZR5/xPYz/Yzkj4PfFbSN4HzgbcD/wNcVtPtFGC27UMkvR24CBjVzTRX2T6/zPdV4CPA90rd1mXupZLuASbZvlrSEKpEenNgF2B74FFgOvBW22dK+iywr+3Hu1jbBGACwKD1NurN7YiIiIiI6BdJiOoT8HVJ+wAvAZsBGwN/Ak4FZgDPAceV9r8AzpQ0GHgXcLvtZyWNAM6SNApYSpVk9NZbgO2A6ZIA1gbuArYFHrL9WwBJl1CSC2Av4FAA2zdLem2JoSs7lERofWA4cENN3eUlGVoX2Mz21WXc58q8APfa/kP5PAdoAe7saWG2JwOTAQZvMtI9tY+IiIiI6C9JiOo7EtgIGG37BUkLgSGlbgOq5GGtUvZM2QG6FfgXqp2iS0vb44HHgJ2pdlWeqzPXi7zy6GLHPAJ+afuI2sYlueoqiVCdsu4SjinAIbbnShoPjKmpe6abMTs8X3O9lPx7ioiIiIhVTJ4hetnTwLrlegTw55IM7QtsUdNuMvAlYCpwWk35T4Cjgb15eadlBPBH2y8BHwIG1Zl3ITBK0hqSNgd2L+V3A2+V9EYAScMkbQ08AGwpaavSrjZhup0qmaM8h/S47ae6WfO6wB8lrdXRr7PS/w+SDinjDu7FG+Rq72VERERExICVhKiw/Veq42nzqZ67aZXURpUoPAAg6cPAi7Z/TPUSht3KszoANwL7AL+y/fdSdjZwlKS7qY7Ldey61JoOPAS0A98CZpV4/gKMBy6VNI8qQdq2HFmbAEwrL1V4uGask0vc80p8R/Ww7C8B9wC/7FhjFz4EHFfG/TXwTz2MOxn4RV6qEBEREREDnew8whGN09ra6ra2tkaHERERERGrMUkzbdd9uVl2iCIiIiIiomklIYqIiIiIiKaVhCgiIiIiIppWEqKIiIiIiGhaSYgiIiIiIqJpJSGKiIiIiIimlYQoIiIiIiKaVhKiiIiIiIhoWms2OoBobu2PLKJl4rRGhxERERERy2nhpLGNDmGFZIcoIiIiIiKaVp8mRJLGSzprOftOkTSuF+NvuozjtkiaX/P5UknzJB0v6VRJ+/XQ/yBJE3uIabnWXPqPkXTd8vbvL7X3aTn6jpG0Z3/EFRERERHRl1a1I3PjgfnAo8vTWdI/AXva3qK3fWxfA1yzPPMNZJLWtP1iF3XLfJ86GQMsBn69nP0jIiIiIlaKXu0QSfpw2S2YK+liSe+WdI+k2ZJ+JWnjOn02lnR16TNX0p51dmtOkHRynb4nSZohab6kyaqMA1qBqZLmSBoqabSk2yTNlHSDpE1K/9FlzruAT9QMfSPwutJ/79pdKUkLJZ0iaZakdknblvJ/7ABJOqzENFfS7TXjbirpekm/lfTNmnXsL+muMublkoaX8ndJekDSncB7a9q/rcQ2p9zbdcvaz5J0n6Rpkv67U8wblutWSbeW690l/bqM8WtJ29Ss5XJJ1wI3SlpH0gXlXs+WdHAX92mrsr6Zku6ouTcbSbqy9J8h6a2SWoBjgeM7+nf7jysiIiIiooF6TIgkbQ98EXi77Z2BTwN3Am+xvQvwE+DEOl3PBG4rfXYFFixDXGfZ3s32DsBQ4EDbVwBtwJG2RwEvAt8DxtkeDVwAfK30vxA4zvYencY9CPhf26Ns31Fn3sdt7wqcA5xQp/4k4F/Kmg6qKR8FHA7sCBwuafOSqPwnsF8Zsw34rKQhwPnAu4G9gX+qGecE4BNlfXsDzwLvAbYpY38U6M1RtAeAfcrP5yTg6zV1ewBH2X471c/1Ztu7AfsCp0tah1ffp8nAp8p9PgE4u4z1XeCM0v9Q4Ae2FwLnlvK691nSBEltktqWLlnUi+VERERERPSP3hyZeztwhe3HAWw/IWlH4LKyI7M28FAX/T5c+iwFFkl6TS/j2lfSicAwYAOqZOraTm22AXYAfikJYBDwR0kjgPVt31baXQwc0Mt5ryrfZ1Kzc1NjOjBF0k9r2gLcZHsRgKT7gC2A9YHtgOklvrWBu4BtgYds/7a0vwSYUDP+tyVNBa6y/QdJ+wCXlnv4qKSbe7GOEcCPJI0EDKxVU/dL20+U6/2BgyR1JH9DgNdTJWKU+IZTJWGXl3UADC7f9wO2qylfT9K6PQVnezJVksXgTUa6F+uJiIiIiOgXvUmIRPVLda3vAd+2fY2kMcDJvZzvRV65KzXkVZNVOyhnA622f6/qSN2r2pW4FnTeBZK0fp14e+v58n0pde6N7WMlvRkYC8yRNKpTv9q+oko+jugU36iu4rM9SdI04F+Bu/XyCx+6Wk/t/ay9R18BbrH9nnKE7daaumdqwwEOtf2bTjG21HxcA3iy7Fp1tgawh+1nawtrEqSIiIiIiAGtN88Q3QS8T9JrASRtQLUD8UipP6qbfh8rfQZJWg94jOrZlNdKGgwcWKdfxy/2j5fdido3zz0NdOxA/AbYSNIeZY61JG1v+0mq3ai9Srsje7HGXpG0le17bJ8EPA5s3k3zu4G3Snpj6TtM0tZUx9m2lLRVafePhKmM3277NKojdtsCtwPvL/dwE6qjbR0WAqPL9aE15bU/n/HdxHgD8CmVDEbSLp0b2H4KeEjSYaWNJO1cqm8EPlkT/6hyWftzioiIiIgYsHpMiGwvoHo25zZJc4FvU+0IXS7pDqrEoJ5PUx19a6c6gra97ReAU4F7gOuokoPO8z1J9YxNO/AzYEZN9RTgXElzqI7IjQNOK3HN4eXna44Gvq/qpQqv2L1YQaereuHCfKpEZW5XDW3/hSoZuVTSPKoEaVvbz1EdkZum6qUKD9d0+4zKSxtK3L8ArgZ+S3U/zgFuq2l/CvDd8nNYWlP+TeAbkqZT3aeufIXqON28sqavdNHuSOAjJa4FQMfLF44DWlW9cOM+qpcpQHW88T15qUJEREREDHSy8wjHqkTSFOC68pKJVV5ra6vb2toaHUZERERErMYkzbTdWq+uT/8wa0RERERExKpkVfvDrE3P9vhGxxARERERsbrIDlFERERERDStJEQREREREdG0khBFRERERETTSkIUERERERFNKwlRREREREQ0rSREERERERHRtPLa7Wio9kcW0TJxWqPDiIiIiIjltHDS2EaHsEKyQxQREREREU1rpSVEkj4jaVgfjveFvhqrjDde0qZ9OWYP890qqbWPxlooacMe2iwu31skze+LebuZa4ykPftzjoiIiIiIvrAyd4g+A9RNiCQNWo7xljkhktTdEcHxwEpLiFYVy/mzGQMkIYqIiIiIAa9fEiJJ60iaJmmupPmSvkyVbNwi6ZbSZrGkUyXdA+xRu8shqVXSreV6uKQLJbVLmifpUEmTgKGS5kia2nnXQ9IJkk4u17dK+rqk24BPSxot6TZJMyXdIGkTSeOAVmBqGXOopJMkzSjxT1ZlzVI2poz9DUlfk3SApJ/WzD9G0rXl+hxJbZIWSDqli/u1uOZ6nKQp5XojSVeWOWdIemspf62kGyXNlnQeoJr+ny0xz5f0mR5+Ti2S7pA0q3ztWRP/LZJ+DLRLGiTpWzU/g0+VdgslnVL6tkvaVlILcCxwfLmXe3cXQ0REREREI/XXSxXeBTxqeyyApBHA0cC+th8vbdYB5ts+qbTpaqwvAYts71javcb2lZI+aXtUKWvpIZ71bb9N0lrAbcDBtv8i6XDga7aPkfRJ4ATbbWXMs2yfWq4vBg60fa2k8cAVko4r63wz8BJwnqR1bD8DHA5cVub+ou0nyk7LTZJ2sj2vNzcR+C5whu07Jb0euAF4E/Bl4E7bp0oaC0wocY6mus9vpkqS7pF0m+3ZXYz/Z+Cdtp+TNBK4lCoxBNgd2MH2Q5I+BmwJ7GL7RUkb1IzxuO1dJX283L9/k3QusNj2t+pNKmlCR8yD1tuol7ciIiIiIqLv9VdC1A58S9JpwHW276iT8CwFruzFWPsB7+/4YPtvyxFPR3KyDbAD8MsSzyDgj1302VfSiVTH/DYAFgDX2l5QEqRrgT1s/x1A0vXAuyVdAYwFTizjvK8kAGsCmwDbAb1NiPYDtqu5d+tJWhfYB3gvgO1pkjruyV7A1SUpQ9JVwN5AVwnRWsBZkkZR/Ty2rqm71/ZDNXGca/vFMucTNe2uKt9ndsTUE9uTgckAgzcZ6d70iYiIiIjoD/2SENl+sOxW/CvwDUk31mn2nO2lNZ9f5OUjfENqygX09Etzbd/O/QGeqRlrge09uhtM0hDgbKDV9u/L8bvaMXcEngQ2rim7DPgE8AQww/bTkrYETgB2s/23chSuc2zwyvXV1q9BlXQ92ym+zn3+UdXduuo4HngM2LnM9VxN3TM11939DJ4v35eS17hHRERExCqmv54h2hRYYvsS4FvArsDTwLrddFsIjC7Xh9aU3wh8smbs15TLF8oROKh+qX9debZmMHBgF3P8BthI0h5lrLUkbV/qauPrSEoelzQcGFcz/3uB11Lt0pwpaf1SdWtZ50d5eUdqParEYpGkjYEDuojrMUlvkrQG8J5u1j6qXN4OHFnKDgBeU1N+iKRhktYpY93RxZwAI4A/2n4J+BDVjlk9NwLHqryUotORuXp6+llHRERERAwI/fWWuR2BeyXNAb4IfJXqiNQvVF6qUMcpwHcl3UG129Dhq8BryksC5gL7lvLJwDxJU22/AJwK3ANcBzxQb4JyvG0ccFoZaw4vvw1tCnBuifl54Hyqo38/A2YAqHrpwyTgI7YfBM6ies6Hstt1HVXSc10pm0t1XG0BcAEwvYu1Tyx9buaVR/iOA1rLiwzuo3pZQce92kfSLGB/4P/KfLPKOu4t9+IH3Tw/BNUu2FGS7qY6LvdMF+1+UOaYV+7bB7oZE6rjhO/JSxUiIiIiYqCTnUc4onFaW1vd1tbW6DAiIiIiYjUmaabtun8DdGX+HaKIiIiIiIgBJQlRREREREQ0rSREERERERHRtJIQRURERERE00pCFBERERERTSsJUURERERENK0kRBERERER0bSSEEVERERERNNKQhQREREREU1rzUYHEM2t/ZFFtEyc1ugwIiIiImI5LJw0ttEhrLDsEAUAkhZK2nAZ2m8q6YpyPUbSdTXXe/ZXnBERERERfSkJUSBp0LL2sf2o7XF1qsYASYgiIiIiYpWQhGgVI+krkj5d8/lrko6T9DlJMyTNk3RKTf3PJM2UtEDShJryxZJOlXQPsEcp/pyke8vXG0u7KZLG1fYr31skze8UWwtwLHC8pDmS9u6HWxARERER0WeSEK16fggcBSBpDeD9wGPASGB3YBQwWtI+pf0xtkcDrcBxkl5bytcB5tt+s+07S9lTtncHzgK+s6yB2V4InAucYXuU7TvqtZM0QVKbpLalSxYt6zQREREREX0mCdEqpiQdf5W0C7A/MBvYreZ6FrAtVYIEVRI0F7gb2LymfClwZafhL635vgf9xPZk2622WwcNG9Ff00RERERE9ChvmVs1/QAYD/wTcAHwDuAbts+rbSRpDLAfsIftJZJuBYaU6udsL+00rutcv0hJnCUJWLuvFhERERER0WjZIVo1XQ28i2pn6IbydYyk4QCSNpP0OmAE8LeSDG0LvKWHcQ+v+X5XuV4IjC7XBwNr9TDG08C6vV9KRERERETjZIdoFWT775JuAZ4suzw3SnoTcFe1icNi4IPA9cCxkuYBv6E6NtedweUlC2sAR5Sy84GfS7oXuAl4pocxrgWukHQw8KmuniOKiIiIiBgIZLvnVjGglJcpzAIOs/3bRsezIlpbW93W1tboMCIiIiJiNSZppu3WenU5MreKkbQd8D/ATat6MhQRERER0Wg5MreKsX0f8IZGxxERERERsTrIDlFERERERDStJEQREREREdG0khBFRERERETTSkIUERERERFNKwlRREREREQ0rSREERERERHRtJIQRURERERE08rfIYqGan9kES0TpzU6jIiIiIhYRgsnjW10CH0iO0SrMEmnStpvOfseJGliF3WLezuvpM9IGrY8MURERERENFp2iAYISQJk+6Xe9rF90vLOZ/sa4Jrl7Fs772eAS4AlyxtLRERERESjZIeogSS1SLpf0tnALOBLkmZImifplJp2X5L0gKRfSrpU0gmlfIqkceX6HZJmS2qXdIGkwaV8oaRTJM0qdduW8vGSzirXW0q6q8z9lU4xnlj6zZU0qXZeSccBmwK3SLpF0kcknVHT96OSvt2f9zAiIiIiYkUkIWq8bYCLgM8DmwG7A6OA0ZL2kdQKHArsArwXaO08gKQhwBTgcNs7Uu38faymyeO2dwXOAU6oE8N3gXNs7wb8qWbcA4BDgDfb3hn4Zm0n22cCjwL72t4X+AlwkKS1SpOjgQvrxDtBUpuktqVLFnV9ZyIiIiIi+lkSosZ72PbdwP7lazbVbtG2wEhgL+Dntp+1/TRwbZ0xtgEesv1g+fwjYJ+a+qvK95lAS53+bwUuLdcX15TvB1xoewmA7Se6W4jtZ4CbgQPLTtRattvrtJtsu9V266BhI7obMiIiIiKiX+UZosZ7pnwX8A3b59VWSjq+F2Ooh/rny/eldP0zdxfj1ivvzg+ALwAPUGd3KCIiIiJiIMkO0cBxA3CMpOEAkjaT9DrgTuDdkoaUunrvN3wAaJH0xvL5Q8BtyzD3dOD95frImvIbS0zDSkwb1On7NLBuxwfb9wCbAx/g5V2niIiIiIgBKQnRAGH7RuDHwF2S2oErgHVtz6B6G9xcqqNvbcCiTn2fo3pe5/LS9yXg3GWY/tPAJyTNAP5xhs329WXuNklzqP/80WTgF5JuqSn7KTDd9t+WIYaIiIiIiJVO9rKeiIqVTdJw24vLTs3twATbsxodV1ckXQecYfumntq2tra6ra1tJUQVEREREc1K0kzbr3o5GWSHaFUxuezQzAKuHKjJkKT1JT0IPNubZCgiIiIiotHyUoVVgO0PNDqG3rD9JLB1o+OIiIiIiOit7BBFRERERETTSkIUERERERFNKwlRREREREQ0rSREERERERHRtJIQRURERERE00pCFBERERERTSsJUURERERENK38HaJoqPZHFtEycVqjw4iIiIiIZbBw0thGh9BnVskdIkmL+2icFknzy/UoSf/aF+P2cu7xks7qo7FOlnRCD22mSBpXrm+V1NoXc3cx1/qSPt5f40dERERE9JVVMiHqJ6OAlZYQrUokDVrGLusDSYgiIiIiYsBbpRMiScMl3SRplqR2SQeX8hZJ90s6X9ICSTdKGlrqRkuaK+ku4BOlbG3gVOBwSXMkHS5pd0m/ljS7fN+mtP2spAvK9Y6S5ksaJmmhpPVrYvsfSRtLereke8o4v5K0cZ11/GP3pnxeXHP9OUkzJM2TdEpN+Rcl/UbSr4BtaspHSbq7tL9a0mt6uIfnSGor96l2/IWSTpJ0J3CYpHeV+zxX0k2lzcmSLig7Tr+TdFzpPgnYqtzL07v/KUZERERENM4qnRABzwHvsb0rsC/wX5JU6kYC37e9PfAkcGgpvxA4zvYeHYPY/jtwEnCZ7VG2LwMeAPaxvUup+3pp/h3gjZLeU8b6d9tLgJ8D7wGQ9GZgoe3HgDuBt5RxfgKc2NvFSdq/rGN3qh2s0ZL2kTQaeD+wC/BeYLeabhcBn7e9E9AOfLmHab5ouxXYCXibpJ1q6p6zvRdwE3A+cKjtnYHDatpsC/xLifHLktYCJgL/W+7l5+qsa0JJwtqWLlnUq3sREREREdEfVvWXKgj4uqR9gJeAzYCOHZiHbM8p1zOBFkkjgPVt31bKLwYO6GLsEcCPJI0EDKwFYPslSeOBecB5tqeX9pdRJU4XUiUrl5XyfwYuk7QJsDbw0DKsb//yNbt8Hk6VIK0LXF0SMSRdU753Xt+PgMt7mON9kiZQ/VvYBNiurK1jTQBvAW63/VC5B0/U9J9m+3ngeUl/5uX73yXbk4HJAIM3Geme2kdERERE9JdVfYfoSGAjYLTtUcBjwJBS93xNu6VUv/CLKrnpja8At9jeAXh3zbhQJSWLgU1ryu6i2jnaCDgEuKqUfw84y/aOwL93GqfDi5SfRdnhWruUC/hG2WkZZfuNtn9Y6lY4kZC0JXAC8I6yozStU3zP1MTR1Xz17nNERERExCphVU+IRgB/tv2CpH2BLbprbPtJYJGkvUrRkTXVT1PtvNSO/Ui5Ht9RWHZhvgvsA7y249kf2wauBr4N3G/7r3XGOaqL0BYCo8v1wZTdKOAG4BhJw8vcm0l6HXA78B5JQyWtS5WwYXsR8DdJe5f+HwI6dovqWY8q6VlUnm3qarfsLqrjdFuWODboZkx49b2MiIiIiBiQVvX/mj8VuFZSGzCH6rmfnhwNXCBpCVXC0eEWYKKkOcA3gG9SHZn7LHBzTbszgLNtPyjpI8Atkm63/WeqI2YzqEmggJOByyU9AtwNbFknpvOBn0u6l+p5nWcAbN8o6U3AXeXRqMXAB23PknRZWfPDwB01Yx0FnCtpGPC7st66bM+VNBtYUNpO76LdX8qxuqskrQH8GXhnN+P+VdJ0Va80/0W954giIiIiIgYCVRsbEY3R2trqtra2RocREREREasxSTPLi8ReZVU/MhcREREREbHckhBFRERERETTSkIUERERERFNKwlRREREREQ0rSREERERERHRtJIQRURERERE00pCFBERERERTSsJUURERERENK0kRBERERER0bTWbHQA0dzaH1lEy8RpjQ4jIiIiInph4aSxjQ6hz2WHqI9JapH0gT4c7xBJ2/XheFMkjSvXCyVtWKfNyZJOWI6xx0i6ri/ijIiIiIhYGZIQ9b0WoG5CJGl5duQOAfosIYqIiIiIiJetVgmRpA9LmidprqSLJW0k6UpJM8rXW0u7kyVdIOlWSb+TdFwpX0fStNJ/vqTDS/k/dlIktUq6tVy/TdKc8jVb0rrAJGDvUna8pPGSLpd0LXCjpOGSbpI0S1K7pIO7iX9P4CDg9DLeVpI+WtYyt6xtWOk7RdKZkn5d1tSxCyRJZ0m6T9I04HWdbtvnJN1bvt5Y556OknR3ietqSa8p5W+U9KsSxyxJW3Xqt1u5J29Y0Z9rRERERER/WW2eIZK0PfBF4K22H5e0AXAWcIbtOyW9HrgBeFPpsi2wL7Au8BtJ5wDvAh61PbaMOaKHaU8APmF7uqThwHPAROAE2weWMcYDewA72X6i7BK9x/ZTJcm6W9I1VLtAr4i/tL8GuM72FWW8J22fX66/CnwE+F6JZxNgr7K2a4ArgPcA2wA7AhsD9wEX1KzhKdu7S/ow8B3gwE5rvAj4lO3bJJ0KfBn4DDAVmGT7aklDqJLrzUtce5aYDrb9fz3cw4iIiIiIhlltEiLg7cAVth8HKMnEfsB2kjrarFd2cQCm2X4eeF7Sn6mShXbgW5JOo0pC7uhhzunAtyVNBa6y/YeauWr90vYT5VrA1yXtA7wEbFbmflX8Xcy5Q0mE1geGUyV5HX5m+yXgPkkbl7J9gEttLwUelXRzp/Eurfl+Rm1FSQjXt31bKfoRcHm5h5vZvrrE+lxpD1XCORnY3/aj9RYgaQIwAWDQeht1scyIiIiIiP63Oh2ZE+BOZWsAe9geVb42s/10qXu+pt1SYE3bDwKjqRKjb0g6qdS/yMv3akhHJ9uTgH8DhlLt9GzbRWzP1FwfCWwEjLY9CnisjFkv/nqmAJ+0vSNwSm08ndZUm5l1N667uO5O3ayv+CPVTtkuXU5oT7bdart10LCeNuEiIiIiIvrP6pQQ3QS8T9JrAcqRuRuBT3Y0kDSquwEkbQossX0J8C1g11K1kCpRAji0pv1Wttttnwa0UR1Ve5rqGF5XRgB/tv2CpH2BLbqJnzrjrQv8UdJaVMlVT24H3i9pkKRNqI4J1jq85vtdtRW2FwF/k7R3KfoQcJvtp4A/SDqkxDq441km4ElgLNUu2JhexBcRERER0TCrzZE52wskfQ24TdJSYDZwHPB9SfOo1no7cGw3w+xI9QKDl4AXgI+V8lOAH0r6AnBPTfvPlKRmKdWzOb+gOgb3oqS5VLs5f+s0x1TgWkltwBzggW7iHw/8BDhf1YsfxgFfKjE8TLWT1V3yBXA11XG8duBB4LZO9YMl3UOVHB9Rp/9RwLkl4fkdcHQp/xBwXnmu6AXgsI4Oth+T9G7gF5KOsX1P50EjIiIiIgYC2b09JRXR91pbW93W1tboMCIiIiJiNSZppu3WenWr05G5iIiIiIiIZZKEKCIiIiIimlYSooiIiIiIaFpJiCIiIiIiomklIYqIiIiIiKaVhCgiIiIiIppWEqKIiIiIiGhaSYgiIiIiIqJpJSGKiIiIiIimtWajA4jm1v7IIlomTmt0GBERERFNbeGksY0OoWGyQ9QLksZLOms5+06RNK4X42+6jOO2SJq/vPFJ+kK9sZah/3Lfk4iIiIiIgSIJ0cAwHlimhKgPfKHnJitGlfwbi4iIiIgBq6l/WZX0YUnzJM2VdLGkd0u6R9JsSb+StHGdPhtLurr0mStpz847LJJOkHRynb4nSZohab6kySVhGAe0AlMlzZE0VNJoSbdJminpBkmblP6jy5x3AZ/oNPzmkq6X9BtJX66Z82dlnAWSJpSyScDQMt/U0nSQpPNLuxslDS1tb5V0mqR7JT0oae/u5iz34n5JZwOzgM2X9ecSEREREbGyNG1CJGl74IvA223vDHwauBN4i+1dgJ8AJ9bpeiZwW+mzK7BgGaY9y/ZutncAhgIH2r4CaAOOtD0KeBH4HjDO9mjgAuBrpf+FwHG296gz9u7AkcAo4DBJraX8mDJOK3CcpNfangg8a3uU7SNLu5HA921vDzwJHFoz9pq2dwc+A3y5pryrObcBLrK9i+2Hl+H+RERERESsVM38UoW3A1fYfhzA9hOSdgQuKzsyawMPddHvw6XPUmCRpNf0cs59JZ0IDAM2oEqmru3UZhtgB+CXkgAGAX+UNAJY3/Ztpd3FwAE1/X5p+68Akq4C9qJKtI6T9J7SZnOqxOevdWJ7yPaccj0TaKmpu6qL8npz/gx42PbdXd2EslM1AWDQeht11SwiIiIiot81c0IkwJ3Kvgd82/Y1ksYAJ/dyrBd55W7bkFdNJg0BzgZabf++HKl7VbsS14LOu0CS1q8Tb63OdS5r2A/Yw/YSSbd2MSfA8zXXS6l2sDrXLeWV/2ZeNWf5/kw3cWJ7MjAZYPAmI7tbU0REREREv2raI3PATcD7JL0WQNIGwAjgkVJ/VDf9Plb6DJK0HvAY8DpJr5U0GDiwTr+ORORxScOB2jfPPQ2sW65/A2wkaY8yx1qStrf9JNVu1F6l3ZG80jslbVCe/TkEmF7W87eSDG0LvKWm/QuS1upijb1Vb86IiIiIiFVG0yZEthdQPZtzm6S5wLepdoQul3QH8HgXXT9NdfStneoI2fa2XwBOBe4BrgMeqDPfk8D5QDvVsbIZNdVTgHMlzaE6IjcOOK3ENQfYs7Q7Gvh+eanCs52muJPqGN0c4ErbbcD1wJqS5gFfAWqPsU0G5tW8VGF51JszIiIiImKVITsnlqJxWltb3daWPCoiIiIi+o+kmbZb69U17Q5RREREREREEqKIiIiIiGhaSYgiIiIiIqJpJSGKiIiIiIimlYQoIiIiIiKaVhKiiIiIiIhoWkmIIiIiIiKiaSUhioiIiIiIppWEKCIiIiIimtaajQ4gmlv7I4tomTit0WFERERENLWFk8Y2OoSGyQ7RACPpOEn3S5raTZvF5XuLpPnleryks5Zxri/UXP9jrBUl6TOShvXFWBERERER/SkJ0cDzceBfbR+5Eub6Qs9NlstngCREERERETHgJSEaQCSdC7wBuEbSIkkn1NTNl9TSwxCbS7pe0m8kfbmm788kzZS0QNKEUjYJGCppTs1u1CBJ55d2N0oaWtq+UdKvJM2VNEvSVpLGSLpV0hWSHpA0VZXjgE2BWyTd0nd3JyIiIiKi7yUhGkBsHws8CuwLnLEcQ+wOHAmMAg6T1FrKj7E9GmgFjpP0WtsTgWdtj6rZjRoJfN/29sCTwKGlfGop3xnYE/hjKd+FajdoO6pE7q22z+xYg+19l2MNERERERErTRKi1csvbf/V9rPAVcBepfw4SXOBu4HNqRKfeh6yPadczwRaJK0LbGb7agDbz9leUtrca/sPtl8C5gAtvQlS0gRJbZLali5ZtGwrjIiIiIjoQ0mIBq4XeeXPZ0gv+rjzZ0ljgP2APcoOz+xuxnq+5nop1VsI1c189dr3HKQ92Xar7dZBw0b0pktERERERL9IQjRwLQR2BZC0K7BlL/q8U9IG5dmfQ4DpwAjgb7aXSNoWeEtN+xckrdXdgLafAv4g6ZASy+BevEHuaWDdXsQbEREREdFQSYgGriuBDSTNAT4GPNiLPncCF1MdX7vSdhtwPbCmpHnAV6iOzXWYDMzr7hXfxYeojt3NA34N/FMP7ScDv8hLFSIiIiJioJPd+ZRVxMrT2trqtra2RocREREREasxSTNtt9aryw5RREREREQ0rSREERERERHRtJIQRURERERE00pCFBERERERTSsJUURERERENK0kRBERERER0bSSEEVERERERNNKQhQREREREU0rCVFERERERDStNRsdQDS39kcW0TJxWqPDiIiIiGiohZPGNjqEprVK7xBJOk7S/ZL+JmliD203lXRFN/UtkuavYDyLV6R/f6i5R1OXo2+LpA/UfH6tpFskLZZ0Vk35MEnTJD0gaYGkSX0Vf0REREREf1rVd4g+Dhxg+6GeGtp+FBjX/yGtXJIEyPZLXTTp9T2qowX4APDj8vk54EvADuWr1rds3yJpbeAmSQfY/sVyzBkRERERsdKssjtEks4F3gBcI+n4jh0LSVMknSnp15J+J2lcKf/HDpCk7SXdK2mOpHmSRpZhB0k6v+xy3ChpaGm/laTrJc2UdIekbUv5lpLukjRD0ldqYttE0u1l/PmS9i7lR0t6UNJtZZ7amMfV9F9cvg+XdJOkWZLaJR1cs5b7JZ0NzAI2l/S5Esc8Sad0cY/WkXRBaTe7ZrxBkk6v6f/vJZRJwN5lHcfbfsb2nVSJ0T/YXmL7lnL99xLTP6/ozzgiIiIior+tsgmR7WOBR4F9gb91qt4E2As4kOqX+s6OBb5rexTQCvyhlI8Evm97e+BJ4NBSPhn4lO3RwAnA2aX8u8A5tncD/lQz/geAG8r4OwNzJG0CnAK8FXgnsF0vlvkc8B7bu5Z1/lfZEQLYBrjI9i7leiSwOzAKGC1pn9p7ZPsM4IvAzSXefYHTJa0DfARYVMp3Az4qaUtgInCH7VGlf48krQ+8G7ipN+0jIiIiIhppVT8y15WflSNk90nauE79XcAXJf0zcJXt35Y84yHbc0qbmUCLpOHAnsDlL+ciDC7f38rLSdPFwGnlegZwgaS1SixzJL0DuNX2XwAkXQZs3cM6BHxd0j7AS8BmQMd6HrZ9d7nev3zNLp+HUyVIt3cab3/gIEknlM9DgNeX8p1qdqlGlP5/7yG+VwYrrQlcCpxp+3fdtJsATAAYtN5GyzJFRERERESfWl0ToudrrtW50vaPJd0DjAVukPRvwO869VsKDKXaRXuy7PbU4zrj316SmLHAxZJOB56q17Z4sczT8UzQ2qX8SGAjYLTtFyQtpEpiAJ7ptMZv2D6vi/Fr2x1q+zevKKzm/JTtGzqVj+lhvM4mA7+1/Z3uGtmeXNoyeJORXd2TiIiIiIh+t8oemVsRkt4A/M72mcA1wE5dtbX9FPCQpMNKX0nauVRPB95fro+sGX8L4M+2zwd+COwK3AOMKW9qWws4rGaahcDocn0wsFa5HlHGeUHSvsAWXYR5A3BM2c1C0maSXtdFu091HLuTtEtN+cdKXEjauhylexpYt6t7U0vSV0u8n+lN+4iIiIiIgaApEyLgcGC+pDnAtsBFPbQ/EviIpLnAAqqkBeDTwCckzaBKBjqMoXpuaDbVkbrv2v4jcDLVcb1fUb14oMP5wNsk3Qu8mZd3f6YCrZLaSgwP1AvO9o1Ub4K7S1I7cAX1E5mvUCVb88oLJjpeBPED4D5gVik/j2r3cB7woqS5ko4HKLtU3wbGS/qDpO3K0cMvUj0XNau8hOHf6t7JiIiIiIgBRHZOLDWCpPFAq+1PNjqWRhq8yUhvctR3Gh1GREREREPlD7P2L0kzbbfWq1tdnyGKVcSOm42gLf8DEBERERENkoSoQWxPAaY0OIyIiIiIiKbWrM8QRUREREREJCGKiIiIiIjmlYQoIiIiIiKaVhKiiIiIiIhoWkmIIiIiIiKiaSUhioiIiIiIppWEKCIiIiIimlb+DlE0VPsji2iZOK3RYUREREQ0zML8kfqGyg5RJ5IW91DfIukDvRjn130X1conaW9JCyTNkTR0Ofp/oT/iioiIiIjoS0mIll0L0GNCZHvP/g9lxUga1E31kcC3bI+y/exyDJ+EKCIiIiIGvCREXVDldEnzJbVLOrxUTQL2Ljsnx0vaXtK95fM8SSNL/8Xl+6mlbo6kRyRdWMo/WNPvPEmDyteUmjmPL21HS5or6a6OmEr5eEln1cR8naQx5focSW1ll+eUmjYLJZ0k6U7gMEn7l3FnSbpc0nBJ/wa8DzhJ0tTS73OSZpQ11o5Xbx2TgKGlbGr//IQiIiIiIlZcniHq2nuBUcDOwIbADEm3AxOBE2wfCCDpe8B3bU+VtDbwil0X2ydRJRYjgDuAsyS9CTgceKvtFySdTbUjswDYzPYOZez1yzAXAp+yfZuk03sZ/xdtP1F2gW6StJPteaXuOdt7SdoQuArYz/Yzkj4PfNb2qZL2Aq6zfYWk/YGRwO6AgGsk7QP8pd46bE+U9Enbo3oZa0REREREQyQh6tpewKW2lwKPSboN2A14qlO7u4AvSvpn4Crbv+08kCQBU4EzbM+U9ElgNFWSBTAU+DNwLfCGkmRNA24sidT6tm8rw10MHNCL+N8naQLVz3gTYDugIyG6rHx/SymfXuJYu6yns/3L1+zyeThVgrRTF+voVolrAsCg9TbqxVIiIiIiIvpHEqKuqTeNbP9Y0j3AWOAGSf9m++ZOzU4G/mD7wpqxf2T7/71qUmln4F+AT1AdW/ss4C6mf5FXHnscUsbYEjgB2M323yRN6agrnqmJ45e2j+hhmQK+Yfu8TrF+qqt1dMf2ZGAywOBNRna1toiIiIiIfpdniLp2O3B4eSZmI2Af4F7gaWDdjkaS3gD8zvaZwDVUuybU1B8IvBM4rqb4JmCcpNeVNhtI2qIcYVvD9pXAl4BdbT8JLCpH2KA6WtdhITBK0hqSNqc60gawHlXSs0jSxnS9o3Q38FZJbyxxDJO0dZ12NwDHSBpe2m1WYq+7jtLnBUlrdTFvRERERMSAkB2irl0N7AHMpdqhOdH2nyT9FXhR0lxgCtXOywclvQD8CTi10zj/AWwK3FuOlV1j+yRJ/0l1JG4N4AWqHaFngQtLGUDHzsvRwAWSllAlJx2mAw8B7cB8YBaA7bmSZlM9k/S70u5VbP9F0njgUkmDS/F/Ag92andjee7prrKGxcAHbd/XxToeptoBmidplu3aJC4iIiIiYsCQnRNLqxJJLVQvO9ih0bH0hcGbjPQmR32n0WFERERENEz+MGv/kzTTdmu9uuwQRUPtuNkI2vI/AhERERHRIEmIVjG2FwKrxe5QRERERESj5aUKERERERHRtJIQRURERERE00pCFBERERERTSsJUURERERENK0kRBERERER0bSSEEVERERERNNKQhQREREREU0rf4coGqr9kUW0TJzW6DAiIiJeYWH+aHhE08gOUURERERENK0kRP1I0hhJe66kuTaVdEUfjre4r8aKiIiIiBiomj4hkjSon8ZdExgDrJSEyPajtsetjLkiIiIiIlYXq3VCJKlF0gOSfiRpnqQrJA2TtFDSSZLuBA6TdISkdknzJZ1W03+xpP+SNEvSTZI2KuVbSbpe0kxJd0jatpRPkfRtSbcAlwHHAsdLmiNpb0kPSVqrtF2vxLFFqe/4WlrKtihzzivfX18zx5mSfi3pd5LG1ax1frkeL+mqEuNvJX2zZk0fkfSgpFslnS/prFK+paS7JM2Q9JVO9/FzpXyepFNK2TqSpkmaW+7b4aV8txLbXEn3Slq3f366ERERERErbrVOiIptgMm2dwKeAj5eyp+zvRdwO3Aa8HZgFLCbpENKm3WAWbZ3BW4DvlzKJwOfsj0aOAE4u2a+rYH9bB8KnAucYXuU7TuAW4GOpzTfD1xp++FSPwo4v6MMOAu4qMQ9FTizZo5NgL2AA4FJXax7FHA4sCNwuKTNJW0KfAl4C/BOYNua9t8FzrG9G/CnjkJJ+wMjgd3LmKMl7QO8C3jU9s62dwCul7Q2VSL4ads7A/sBz3YOTNIESW2S2pYuWdRF+BERERER/a8ZEqLf255eri+hSiSg+sUdYDfgVtt/sf0iVfKxT6l7qabdJcBekoZTHYO7XNIc4DyqBKXD5baXdhHLD4Cjy/XRwIUdFZLeCvwbcEwp2gP4cbm+uCZugJ/Zfsn2fcDGXcx1k+1Ftp8D7gO2oEpqbrP9hO0XgMtr2r8VuLRmvg77l6/ZwCyqJGok0A7sJ+k06f+zd6dhdlVl2sf/NyGdAIEggnREpQADNEMIpECZgyCtxhYRkCEiIJoGFBBfbNOOIA5BtBFEhoAQBEQGQRHUMEgChLEyVcLcktgKdDtBmKdwvx/2KjgUp1KVpCqnknP/rquu2mftNTx7Fx/y8Ky9j3a2vYAq+Xzc9j0Atp8q9/QNbE+03Wq7dcCqQ7sIPyIiIiKi7zXDa7fdxedny28t5lwrAU+Wik49z3bRju1pZWvbrsAA2x1b3IYBPwE+YrurlxnUXseLNcddxV/bZyHV37q7a+18rzrm/67tc950QhoFfAj4rqTrgV92MUdERERERL/UDBWid0navhwfCNzW6fxdwK6S1i4vWDiQanscVPen40UFBwG32X4KmCdpPwBVtupi7aeBzs/Q/JSqEnNBGT8QuBz4ku2HavrdTrWtDmBsnbiXxN1U1/qW8tKHfWrOTeu0XofJwKdKZQxJ60l6W9l+95zti4HvA9sADwBvl7Rt6bt6WSciIiIiol9qhoTofuAQSe3AWsBZtSdtPw78J3AzMJvqmaFfldPPAptLmk71jNE3S/tY4HBJs4F7gb26WPvXwN4dL1UobZcAb+H17Wk7UG3bO7HmxQpvB44BDitxHwwcu8R34PVrfRT4DlUSeCPVVrqOh3iOBT4r6R5gaM2Y66m27t0haQ5wJVWStyVwd9k2+BXgW7Zfonpu6Ufl3twADF7auCMiIiIi+orsFXeHk6QW4Nry0P+SjH/G9pBejmlfYC/bB/fmvIux/hDbz5TKzdXA+bavbkQsAK2trW5ra2vU8hERERHRBCRNt91a71y2My1Dkn4EfJDquZtGOUHSHlSVm47nfiIiIiIimtIKnRDZng8sUXWojO/V6pDto3tzviWM4fhGxxARERER0V80wzNEERERERERdSUhioiIiIiIppWEKCIiIiIimlYSooiIiIiIaFpJiCIiIiIiomklIYqIiIiIiKa1Qr92O/q/OY8uoGX8dY0OIyIi4g3mTxjT6BAiYhlJhSgiIiIiIppWEqIAQFKrpNPL8WhJOyzFXIdKenvvRRcRERER0TeyZS4AsN0GtJWPo4FngNuXcLpDgbnAY0sdWEREREREH0qFaAUlqUXS3JrPx0s6QdIUSSdLulvSQ5J2LudHS7pWUgtwBHCcpFmSdpa0rqSrJc0uPzuU+e+XdK6keyVdL2kVSfsCrcAlZfwqDbkBERERERE9kISoOa1sezvg88A3ak/Yng+cDZxqe6TtW4HTgam2twK2Ae4t3YcDP7a9OfAksI/tK6kqTWPL+Oc7Ly5pnKQ2SW0Ln1vQJxcYEREREdETSYia01Xl93SgpQf93wecBWB7oe2OLGae7VmLORe2J9putd06YNWhPY05IiIiIqLXJSFacb3CG/++g2uOXyy/F7J0z5G9WHO8tHNFRERERCxzSYhWXP8HvE3SWyUNAj68GGOfBlav+XwTcCSApAGS1ljM8RERERER/VISohWU7ZeBbwJ3AdcCDyzG8F8De3e8VAE4FthN0hyqrXGbdzN+EnB2XqoQEREREf2dbDc6hmhira2tbmtr675jRERERMQSkjTddmu9c6kQRURERERE00pCFBERERERTSsJUURERERENK0kRBERERER0bSSEEVERERERNNKQhQREREREU0rCVFERERERDStJEQREREREdG0Vm50ANHc5jy6gJbx1zU6jIiIaCLzJ4xpdAgR0Y+kQhQREREREU1ruU6IJLVImrsY/T8iaXw5PkHS8YuaU1KrpNN7L+IlJ2mKpNZy/BtJa5afo3owdqSkD/VBTG+XdGV38UZERERE9FfLdUK0uGxfY3vCYvRvs31MX8a0JGx/yPaTwJpAtwkRMBLo9YTI9mO29+3teSMiIiIilpUVISFaWdKFktolXSlpVUnzJa0Nr1V5ppTjQyWd0XkCSaMkzZZ0B/DZmvbRkq4txydIOr9UPh6RdExNv69JekDSDZIu7ag81c4r6ZSaytMb4pB0raTR5fgsSW2S7pV0Yr0Lrrm+CcBGkmaV+S+StFdNv0skfQT4JrB/6be/pIclrVP6rCTpvyWtLWmSpLMl3SrpIUkfLn0GlPnvKff530t7bTVtFUk/L+cvA1ZZrL9iREREREQDrAgJ0SbARNsjgKfoWcWkswuAY2xv302/TYF/BbYDviFpYNkWtg+wNfAxoHabWE/nrfUV263ACGBXSSMW0Xc88AfbI21/ETgPOAxA0lBgB+A3wNeBy0q/y4CLgbFljj2A2bb/Vj63ALsCY4CzJQ0GDgcW2N4W2Bb4jKQNOsVyJPBc+Tt8GxjVVdCSxpWkr23hcwt6cEsiIiIiIvrGipAQ/cn2tHJ8MbDT4gwuicOatqeWposW0f062y+W5OEvwLplvV/Zft7208Cvl2DeWh+XNAOYCWwObNbTaylrvVvS24ADgV/YfqVO1/OBT5bjT1Elbh0ut/2q7YeBR6iSwD2BT0qaBdwFvBUY3mnOXajuP7bbgfZFxDnRdqvt1gGrDu3p5UVERERE9LoV4bXbrvP5FV5P9gZ3M1515ujKizXHC6nun5Zg3tr4oMRYqi7HA9vafkLSJLqPv7OLqKo/B1AlO29i+0+S/k/S+4D38Hq1iDoxm+pajrY9ufaEpJY6fSMiIiIilhsrQoXoXZI6tqQdCNwGzOf1LVv7LGpweTnBAkkdlaWxi+hez23Av0kaLGkI1Vaz7uadD4wsz++8k2oLHsAawLNl3LrAB7tZ+2lg9U5tk4DPlxjuXUS/86gqOpfbXljTvl+JayNgQ+BBYDJwpKSBAJI2lrRap/lu6bhGSVtQbfmLiIiIiOjXVoSE6H7gEEntwFrAWcCJwGmSbqWq5HTnMODH5aUKzy/O4rbvAa4BZgNXAW1Ax4MxXc07DZgHzAG+D8woc82m2ip3L9W2tmksgu2/A9MkzZV0Smn7P6p7UrsN7mZgs46XKpS2a4AhnfpBlQBNBX4LHGH7Bark6T5gRnmJwjm8ubp4FjCk/B3+A7h7UbFHRERERPQHsrPLaWlJGmL7GUmrUlVKxtme0alPC3Ct7S36OJZVqRKtbWx3+caC8jKIU23vXNM2qcRY97uF+kJra6vb2tqW1XIRERER0YQkTS8vLnuTFaFC1B9MLC8cmEH1IoMZ3fTvE5L2AB4AftRNMjQe+AXwn8sqtoiIiIiI/igVomioVIgiIiIioq+lQhQREREREVFHEqKIiIiIiGhaSYgiIiIiIqJpJSGKiIiIiIimlYQoIiIiIiKaVhKiiIiIiIhoWis3OoBobnMeXUDL+OsaHUZERPRD8yeMaXQIEdEEUiGKiIiIiIim1W8TIkktkub2wjyjJe1Q8/kISZ9c2nn7Wue4e2G+z0tatbfmq5n3m5L2qNM+WtK1vb1eRERERERvaoYtc6OBZ4DbAWyf3dcLShpge+FSTjOamrh7weeBi4Hnemk+AGx/vTfni4iIiIhYlvpthagYIOlcSfdKul7SKpI2kvQ7SdMl3SppUwBJ/ybpLkkzJd0oaV1JLcARwHGSZknaWdIJko4vY6ZIOlnS3ZIekrRzaV9V0uWS2iVdVuZtLecOlDRH0lxJJ3cEKumZUi25C9he0gRJ95U5vi9pdUnzJA0s/deQNF/SQEnH1PT9eRdxryPpF5LuKT87lnlOkHRhuT/zJX1M0vdKjL/rmB94O3CzpJslHS7p1JrYPyPpv0pV7oEyX7ukKzuqSpJGSZpa7vtkScNK+yRJ+5bjD5TxtwEf67v/LCIiIiIiekd/T4iGAz+2vTnwJLAPMBE42vYo4HjgzNL3NuC9trcGfg78h+35wNnAqbZH2r61zhor296OqoLyjdJ2FPCE7RHAScAoAElvB04G3geMBLaV9NEyZjVgru33APcBewOblzm+ZftpYArQ8YToAcAvbL8MjAe2Ln2P6CLu08rnbct9OK/mGjYq8+5FVQW62faWwPPAGNunA48Bu9nerdyfj3QkZ8BhwAXleBNgYonlKeCo0u9HwL7lvp8PfLv2JkoaDJwL/BuwM/DPde51R99xktoktS18bkFX3SIiIiIi+lx/3zI3z/ascjwdaAF2AK6Q1NFnUPn9DuCyUrn4J2BeD9e4qtP8ADtRJSDYniupvbRvC0yx/VcASZcAuwC/BBYCvyj9ngJeAM6TdB3Q8SzNecB/lP6HAZ8p7e3AJZJ+Wc7VswewWc11ryFp9XL8W9svS5oDDAB+V9rn1FzTa2w/K+n3wIcl3Q8MtD2nVKb+ZHta6XoxcEyZbwvghrL+AODxTtNuSvX3erjcm4uBcfUuxPZEqsSWQcOGu4vrjYiIiIjoc/09IXqx5nghsC7wpO2Rdfr+CPgv29dIGg2csJhrLOT1+6Eu+nbVDvBCx3NDtl+RtB2wO1Ul6HPA+2xPK9vSdgUG2O54acQYqsTqI8DXJG1eZ/6VgO1tP/+GgKoE5cWy7quSXrbdkWS8Std/4/OALwMP8Hp1CKBzguJy3ffa3n4R119vbEREREREv9bft8x19hQwT9J+AKpsVc4NBR4tx4fUjHkaWJ3Fcxvw8bLGZsCWpf0uYFdJa0saABwITO08WNIQYKjt31BtxRtZc/qnwKWUJETSSsA7bd9MVT1aExhSJ+7rqRKrjjVq5+yJN8xn+y7gncBBJZ4O75LUkfgcSHUvHgTW6WgvzyV1TtoeADaQtFHN2IiIiIiIfm15S4gAxgKHS5oN3Ev13AxUFaErJN0K/K2m/6+BvTteTtDDNc6kSgDagS9RbWlbYPtx4D+Bm4HZwAzbv6ozfnXg2jJ+KnBczblLgLfwehIyALi4bHebSfWc0JN14j4GaC0vO7iP6qULi2Mi8FtJN9e0XQ5Ms/1ETdv9wCEl9rWAs2y/BOwLnFzu+yyqrYuvsf0C1Ra568pLFf64mPFFRERERCxzen13VXQo1Z+Btl8oFY+bgI1LYrC0c+8L7GX74KWdqxdiuZYqAbupfG4BrrW9xbKKobW11W1tbctquYiIiIhoQpKm226td66/P0PUKKtSvaJ6INXzM0f2UjL0I+CDwIeWdq6ljGNN4G5gdkcyFBERERHRjJIQ1VFekV03g1zKeY/u7TmXRNmSt3Gd9vlUb5OLiIiIiGgKy+MzRBEREREREb0iCVFERERERDStJEQREREREdG0khBFRERERETTSkIUERERERFNKwlRREREREQ0rbx2OxpqzqMLaBl/XaPDiIheNn/CmEaHEBER0SOpEEVERERERNNquoRI0jcl7bGEYz8iaXwX557py5gknSDp+DrtLZLmLunaZY75ktZemjmW5bwREREREb1lud4yJ0mAbL/a0zG2v76k69m+BrhmSccvYt4ljmlpSRrQqLUjIiIiIhptuasQlYrI/ZLOBGYAX5N0j6R2SSfW9PuapAck3SDp0o7qiqRJkvYtx7tLmilpjqTzJQ0q7fMlnShpRjm3aWk/VNIZ5XgDSXeUtU+qWXe0pCmSrizrX1ISNySNkjRV0nRJkyUNqxPTh8q42ySdLunamsvfrMz9iKRjatpXlnRhuQdXSlq1B9f3dUm3AfuVOY6uc71rSfplmfdOSSO6aX+rpOvLmucAWuo/eEREREREH1ruEqJiE+CnwJeA9YDtgJHAKEm7SGoF9gG2Bj4GtHaeQNJgYBKwv+0tqaplR9Z0+ZvtbYCzgDdtVQNOA86yvS3wv53ObQ18HtgM2BDYUdJA4EfAvrZHAecD364T0znAB23vBKzTad5NgX8t1/uNMmfH/ZhoewTwFHBUD67vBds72f75Iq73RGBmmffLVPd8Ue3fAG6zvTVVJe1dde4bksZJapPUtvC5BfW6REREREQsE8trQvRH23cCe5afmVTVok2B4cBOwK9sP2/7aeDXdebYBJhn+6Hy+UJgl5rzV5Xf04GWOuN3BC4txxd1One37T+XrXyzyvhNgC2AGyTNAr4KvKPTuE2BR2zPK58v7XT+Otsv2v4b8Bdg3dL+J9vTyvHFVNff3fVd1mnuete7U8e12f498FZJQxfRvktZH9vXAU9Qh+2Jtltttw5YdWi9LhERERERy8Ty+gzRs+W3gO/aPqf2pKTjejBHd9u5Xiy/F9L1fXI3Y2vHC7jX9va9EFPnuDrH4R7M9Wynz/Wut94cXc3tTr8jIiIiIvq95bVC1GEy8ClJQwAkrSfpbcBtwL9JGlzO1ftCjAeAFknvLp8PBqYuxtrTgAPK8dge9H8QWEfS9iXWgZI2rxPThpJayuf9exjLuzrmBQ6kuv6lvT6AWyjXJmk01ba6p3rY/kHgLYu5XkRERETEMrW8VogAsH29pH8B7ijvLXgG+ITteyRdA8wG/gi0AQs6jX1B0mHAFZJWBu4Bzl6M5Y8FfibpWOAXPYj1pfLihNPL9rKVgR8C99b0eV7SUcDvJP0NuLuHsdwPHFJeZPAw1bNNS3t9ACcAF0hqB54DDumm/UTgUkkzqJKv/1nM9SIiIiIilinZK+YOJ0lDbD9T3rh2CzDO9oxGx9WdmrgF/Bh42PapjY6rr7S2trqtra3RYURERETECkzSdNtvetEaLP9b5hZlYnl5wQzgF8tDMlR8psR9LzCU6q1zERERERHRB5brLXOLYvugRsewJEo1aIWtCEVERERE9CcrcoUoIiIiIiJikZIQRURERERE00pCFBERERERTSsJUURERERENK0kRBERERER0bSSEEVERERERNNKQhQREREREU1rhf0eolg+zHl0AS3jr2t0GBHL1PwJYxodQkRERBSpEEWvkzRa0g6NjiMiIiIiojtJiPopSf2ieidpwBIMGw0kIYqIiIiIfq9f/KN7RSbpa8BY4E/A34DpwAJgHPBPwH8DB9t+TtIk4B/A1sAMSZcBPwRWAZ4HDrP9oKRVgUnApsD9QAvwWdttkvYETgQGAX8oY56RtC1wGrAa8CKwO/BW4KLSBvA527dLGg18A3gcGClpS+Bk4F8BA+fa/pGk+cCFwL8BA4H9gBeAI4CFkj4BHG371l66nRERERERvSoJUR+S1ArsQ5XgrAzMoEqIrrJ9bunzLeBw4Edl2MbAHrYXSloD2MX2K5L2AL5T5jsKeML2CElbALPKXGsDXy3jn5X0JeALkiYAlwH7276nzPs88Bfg/bZfkDQcuBRoLXFsB2xhe56kI4ENgK1LLGvVXObfbG8j6SjgeNuflnQ28Izt73dxX8ZRJYQMWGOdJby7ERERERFLLwlR39oJ+JXt5wEk/bq0b1ESoTWBIcDkmjFX2F5YjocCF5ZkxVRVmI55TwOwPVdSe2l/L7AZME0SVBWoO4BNgMdt31PGPFXiWQ04Q9JIYCFVMtbhbtvzyvEewNm2Xynj/1HT76ryezrwsZ7cFNsTgYkAg4YNd0/GRERERET0hSREfUtdtE8CPmp7tqRDqZ656fBszfFJwM2295bUAkzpZl4BN9g+8A2N0giqhKqz44D/A7aiep7shS7iUBfjodp+B1VClf+eIiIiImK5kpcq9K3bgH+TNFjSEKDjXburA49LGkj1fFFXhgKPluNDO837cQBJmwFblvY7gR0lvbucW1XSxsADwNvLc0RIWr28tGEoVeXoVeBgoKsXKFwPHNHxoodOW+bqebpcY0REREREv5aEqA+VLWrXALOptpa1Ub1Q4WvAXcANVMlKV74HfFfSNN6YrJwJrFO2yn0JaAcW2P4rVeJ0aTl3J7Cp7ZeA/YEfSZpd1h1c5jlE0p1U2+Vqq0K1zgP+B2gv4w/q5tJ/DewtaZaknbvpGxERERHRMLLzCEdfkjSkvOVtVeAWYJztGUs55wBgYHkZwkbATcDGJfFZrrS2trqtra3RYURERETECkzSdNut9c7lmY++N7FsaxsMXLi0yVCxKnBz2XIn4MjlMRmKiIiIiGi0JER9zHZ328uWZM6nef312BERERERsYTyDFFERERERDStJEQREREREdG0khBFRERERETTSkIUERERERFNKwlRREREREQ0rSREERERERHRtJIQRURERERE08r3EEVDzXl0AS3jr2t0GBHL1PwJYxodQkRERBSpEC3HJH1e0qo96PflHs43X9LaSxDHIEk3Spolaf+erhcRERER0WhJiJYhSb1dkfs80G1CBPR1grI1MND2SNuXLYP1IiIiIiJ6RRKiJSDpa5IekHSDpEslHS/pM5LukTRb0i86KjeSJkn6L0k3AydL2k7S7ZJmlt+blH6rSrpcUrukyyTdJam1nNtT0h2SZki6QtIQSccAbwduLnMj6UBJcyTNlXRyaZsArFKqN5eUtl9Kmi7pXknj6lzfapKuK9cyV9L+pf0D5bpvk3S6pGslvQ24GBhZ1rii83oREREREf1VniFaTCVJ2YeqKrIyMAOYDlxl+9zS51vA4cCPyrCNgT1sL5S0BrCL7Vck7QF8p8x3FPCE7RGStgBmlbnWBr5axj8r6UvAF2x/U9IXgN1s/03S24GTgVHAE8D1kj5qe7ykz9keWXMZn7L9D0mrAPdI+oXtv9ec/wDwmO0xJYahkgYD5wLvA/4buAzA9l8kfRo43vaHS/9nOq3X+R6OA8YBDFhjnR7e+YiIiIiI3pcK0eLbCfiV7edtPw38urRvIelWSXOAscDmNWOusL2wHA8FrpA0Fzi1pt9OwM8BbM8F2kv7e4HNgGmSZgGHAOvXiWtbYIrtv9p+BbgE2KWLazhG0mzgTuCdwPBO5+cAe0g6WdLOthcAmwLzbD9s21RVoSVie6LtVtutA1YduqTTREREREQstVSIFp+6aJ8EfNT2bEmHAqNrzj1bc3wScLPtvSW1AFO6mVfADbYPXMK43thJGg3sAWxv+zlJU4DBtX1sPyRpFPAh4LuSrgeuAdyTNSIiIiIilhepEC2+24B/kzRY0hCg4/25qwOPSxpIVSHqylDg0XJ8aKd5Pw4gaTNgy9J+J7CjpHeXc6tK2rice7qsC3AXsKuktSUNAA4EppZzL5e4OtZ/oiRDm1JVoN6gbL97zvbFwPeBbYAHgA0kbVS6LSpBq10vIiIiIqLfSkK0mGzfQ1UtmQ1cBbQBC4CvUSUlN1AlD135HlXVZRowoKb9TGAdSe3Al6i2zC2w/VeqxOnScu5Oqu1rABOB30q62fbjwH8CN5fYZtj+VU2/9vKSg98BK5e5TirzdbYlcHfZovcV4Fu2X6B67uc6SbcBf1zENdauFxERERHRb6l6HCQWh6Qhtp8pb5K7BRhne8ZSzjmA6tXVL5QqzE3AxrZf6oWQe13ZevfaixSWVGtrq9va2nolpoiIiIiIeiRNt91a71yeIVoyE8u2tsHAhUubDBWrUr1CeyDV80BH9tdkKCIiIiJiRZGEaAnYPqgP5nwaqJu19ke2p/D6CyEiIiIiIpZLeYYoIiIiIiKaVhKiiIiIiIhoWkmIIiIiIiKiaSUhioiIiIiIppWEKCIiIiIimlYSooiIiIiIaFpJiCIiIiIiomnle4iioeY8uoCW8dc1OoyIxTJ/wphGhxARERG9ZIWsEEn6jaQ1l+F6UyS11q5dfo6q6fN2SVcuxRpLfE2SRkr60JKuvYh5u7ym2nsSEREREdFfrZAJke0P2X6ytk2VPr/emrXXBI6qaX/M9r69MO+SGAn0ekK0tNcUEREREdFoy31CJOmXkqZLulfSuNI2X9Laklok3S/pTGAG8E5J/yFpjqTZkiaU/iMl3SmpXdLVkt5S2qdIOlnS3ZIekrRzaV9F0s9L/8uAVWrimS9pbWACsJGkWZJOKbHMLX0GS7qgxDFT0m6l/VBJV0n6naSHJX2v87w113RuuebrJa1S+mxbYrqjrDlX0j8B3wT2L7HsX+Zep4xZSdJ/l7knSTpb0q3lej9c+gwo891T5v/30l57TV3ek4iIiIiI/mq5T4iAT9keBbQCx0h6a6fzmwA/tb01sBnwUeA9trcCOhKOnwJfsj0CmAN8o2b8yra3Az5f034k8Fzp/21gVJ24xgN/sD3S9hc7nfssgO0tgQOBCyUNLudGAvsDW1IlMe+sM/dw4Me2NweeBPYp7RcAR9jeHlhY1ngJ+DpwWYnlMuBiYGwZswcw2/bfyucWYFdgDHB2ietwYIHtbYFtgc9I2qBTTD25JwBIGiepTVLbwucWdNUtIiIiIqLPrQgJ0TGSZgN3Au+kShZq/dH2neV4D+AC288B2P6HpKHAmranlj4XArvUjL+q/J5OlSxQzl9c5mgH2hcz5p2Ai8r4B4A/AhuXczfZXmD7BeA+YP064+fZnlUbV3m+aHXbt5f2ny1i/fOBT5bjT1ElUh0ut/2q7YeBR4BNgT2BT0qaBdwFvJU33+ce3xPbE2232m4dsOrQRYQZEREREdG3luu3zEkaTZXkbG/7OUlTgMGduj1bOwTwYi7zYvm9kDfer8Wdp5Z6sF69Nbvqs0o3c76B7T9J+j9J7wPew+vVInjzdbnMfbTtybUnJLXU6RsRERERsdxY3itEQ4EnSjK0KfDebvpfD3xK0qoAktayvQB4ouP5IOBgYGpXExS3UJIISVsAI+r0eRpYvQfjNwbeBTzYzZqLZPsJ4GlJHffggG5iOY+qonO57YU17fuV54o2AjYscU0GjpQ0sCNmSast4pq6uicREREREf3K8p4Q/Q5YWVI7cBLVtrku2f4dcA3QVrZ/HV9OHQKcUuYZSfUSgkU5CxhS+v8HcHedtf4OTCsvNjil0+kzgQGS5gCXAYfafrHzHEvgcGCipDuoqjodD+jcDGzW8VKF0nYNMIQ3bpeDKgGaCvyW6nmkF6iSp/uAGeUlCufw5spVt/ckIiIiIqK/kZ1dTisKSUNsP1OOxwPDbB/bRd9W4FTbO9e0TQKutb3E35e0uFpbW93W1raslouIiIiIJiRpuu2635G5XD9DFG8yRtJ/Uv1d/wgcWq9TSZaO5I3PDkVERERENJ1UiKKhUiGKiIiIiL62qArR8v4MUURERERExBJLQhQREREREU0rCVFERERERDStJEQREREREdG0khBFRERERETTSkIUERERERFNKwlRREREREQ0rXwxazTUnEcX0DL+ukaHEcu5+RPGNDqEiIiIWE6lQtQgkqZIqvvlUEs57yBJN0qaJWn/Lvp8VNJmSzD3EZI+ufRRRkRERET0D6kQLYckrWz7lS5Obw0MtD1yEVN8FLgWuG8x1zy7x0FGRERERCwHUiHqhqQWSfdLOlfSvZKul7RKbYVH0tqS5pfjQyX9UtKvJc2T9DlJX5A0U9Kdktaqmf4Tkm6XNFfSdmX8apLOl3RPGbNXzbxXSPo1cL2ktco67WXeEZLeBlwMjCwVoo0kTZB0X+n3fUk7AB8BTqnpM7LM0S7paklvKWtOkfQdSVOBYyWdIOn4cm4jSb+TNF3SrZI2Le37leuZLemWZfE3ioiIiIhYUkmIemY48GPbmwNPAvt0038L4CBgO+DbwHO2twbuAGq3nK1mewfgKOD80vYV4Pe2twV2o0pcVivntgcOsf0+4ERgpu0RwJeBn9r+C/Bp4NZSIXoC2BvYvPT7lu3bgWuAL9oeafsPwE+BL5U+c4Bv1MS4pu1dbf+g0zVOBI62PQo4HjiztH8d+FfbW1ElXm8iaZykNkltC59b0M2tjIiIiIjoO9ky1zPzbM8qx9OBlm7632z7aeBpSQuAX5f2OcCImn6XAti+RdIaktYE9gQ+0lGJAQYD7yrHN9j+RzneiZKY2f69pLdKGtopjqeAF4DzJF1HtU3uDcqYNW1PLU0XAlfUdLmszpghwA7AFZI6mgeV39OASZIuB67qPLbEO5EqoWLQsOGu1yciIiIiYllIQtQzL9YcLwRWAV7h9Qrb4EX0f7Xm86u88Z53TgYMCNjH9oO1JyS9B3i2tqlOnG+Yz/YrZSve7sABwOeA99UZtyjP1mlbCXiy3nNKto8osY4BZkkaafvvi7lmRERERMQykS1zS24+MKoc77uEc+wPIGknYIHtBcBk4GiV0oukrbsYewswtvQZDfzN9lO1HUolZ6jt3wCfB0aWU08DqwOUNZ+QtHM5dzAwlUUo68yTtF9ZR5K2Kscb2b7L9teBvwHv7PYuREREREQ0SCpES+77wOWSDgZ+v4RzPCHpdmAN4FOl7STgh0B7SYrmAx+uM/YE4AJJ7cBzwCF1+qwO/ErSYKqK0nGl/efAuZKOoUrmDgHOlrQq8AhwWA9iHwucJemrwMAy52yqZ56Gl/VuKm0REREREf2S7DzCEY3T2trqtra2RocRERERESswSdNt1/0O0GyZi4iIiIiIppWEKCIiIiIimlYSooiIiIiIaFpJiCIiIiIiomklIYqIiIiIiKaVhCgiIiIiIppWEqKIiIiIiGhaSYgiIiIiIqJpJSGKiIiIiIimtXKjA4jmNufRBbSMv67RYcRSmD9hTKNDiIiIiFhiqRBFlyRNkdS6BOPWlHRUX8QUEREREdGbkhBFX1gTSEIUEREREf1eEqIVkKQWSQ9IulBSu6QrJa0qaXdJMyXNkXS+pEGlf932TnN+QNIMSbMl3VTaTij9p0h6RNIxpfsEYCNJsySdsuyuPCIiIiJi8SQhWnFtAky0PQJ4CvgCMAnY3/aWVM+PHSlpcL322okkrQOcC+xjeytgv5rTmwL/CmwHfEPSQGA88AfbI21/se8uMSIiIiJi6SQhWnH9yfa0cnwxsDswz/ZDpe1CYBeqxKlee633ArfYngdg+x81566z/aLtvwF/AdbtLjBJ4yS1SWpb+NyCJbm2iIiIiIhekYRoxeUe9lMP+3Q134s1xwvpwZsLbU+03Wq7dcCqQ3uwfERERERE30hCtOJ6l6Tty/GBwI1Ai6R3l7aDganAA12017oD2FXSBgCS1upm7aeB1Zcy/oiIiIiIPpeEaMV1P3CIpHZgLeBU4DDgCklzgFeBs22/UK+9diLbfwXGAVdJmg1ctqiFbf8dmCZpbl6qEBERERH9meye7qyK5YWkFuBa21s0OpbutLa2uq2trdFhRERERMQKTNJ023W/XzMVooiIiIiIaFrdPgAfyx/b84F+Xx2KiIiIiGi0VIgiIiIiIqJpJSGKiIiIiIimlYQoIiIiIiKaVhKiiIiIiIhoWkmIIiIiIiKiaSUhioiIiIiIppWEKCIiIiIimla+hygaas6jC2gZf12jw4huzJ8wptEhRERERPSJVIhWAJKmSGotx1/uozXOk7RZnfZDJZ1Rjk+VNKv8PCTpyb6IJSIiIiKit6RCtOL5MvCd3p7U9qd70Oe4jmNJRwNb93YcERERERG9KRWiBpLUIukBSRdKapd0paRVJe0uaaakOZLOlzSo9K/bXjPfBGCVUqG5RNJJko6tOf9tScdIGi3pFklXS7pP0tmSVip99pR0h6QZkq6QNKS011ahDisVoKnAjl1c3oHApb1/1yIiIiIiek8SosbbBJhoewTwFPAFYBKwv+0tqap4R0oaXK+9diLb44HnbY+0PRb4CXAIQEl4DgAuKd23A/4fsCWwEfAxSWsDXwX2sL0N0FbieY2kYcCJVInQ+4F62+jWBzYAfr/EdyUiIiIiYhlIQtR4f7I9rRxfDOwOzLP9UGm7ENiFKnGq194l2/OBv0vaGtgTmGn77+X03bYfsb2QqpKzE/BeqgRnmqRZVMnU+p2mfQ8wxfZfbb8EXFZn6QOAK8vcbyJpnKQ2SW0Ln1uwqEuIiIiIiOhTeYao8dzDflrC+c8DDgX+GTh/Eeu6rHGD7QO7mbO7mA8APtvlYHsiMBFg0LDhPb3+iIiIiIhelwpR471L0vbl+EDgRqBF0rtL28HAVOCBLto7e1nSwJrPVwMfALYFJte0bydpg7KVbn/gNuBOYMeONcrzTBt3mv8uYLSkt5Z19qs9KWkT4C3AHT27/IiIiIiIxklC1Hj3A4dIagfWAk4FDgOukDQHeBU42/YL9drrzDcRaJd0CUDZ1nYzcHmnLWx3ABOAucA84Grbf6WqJl1a4rkT2LR2ctuPAyeU8TcCMzqtfyDwc9up/EREREREv6f8u7VxJLUA19reog/XWIkqadnP9sOlbTRwvO0P99W6PdXa2uq2trZGhxERERERKzBJ02231juXCtEKrHyR6n8DN3UkQxERERER8bq8VKGBylvg+qw6ZPs+YMM67VOAKX21bkRERETE8iIVooiIiIiIaFpJiCIiIiIiomklIYqIiIiIiKaVhCgiIiIiIppWEqKIiIiIiGhaSYgiIiIiIqJpJSGKiIiIiIimle8hioaa8+gCWsZf1+gw+oX5E8Y0OoSIiIiIprPCVIgkrSnpqHI8WtK1jY5pcUk6VNLbl2DcJEn7Lkb/FkkHdVr3jL5cMyIiIiKiP1phEiJgTeCoxRkgaUDfhLL4SiyHAouVEElakipfC3BQd52W1hLGFhERERGxzKxICdEEYCNJs4BTgCGSrpT0gKRLJAlA0nxJX5d0G7CfpD0l3SFphqQrJA0p/UZJmippuqTJkobVtM8uY06RNLe0v6HKIulaSaPL8VmS2iTdK+nEmj61sRwItAKXSJolaZVFxDBF0nckTQWOLdPtIelWSQ9J+nDp11LaZpSfHWru1c5lneNK29sl/U7Sw5K+VxPjM5K+Xa75Tknr1tzzemseWu7jr4Hrl+LvGRERERHR51akhGg88AfbI4EvAlsDnwc2AzYEdqzp+4LtnYAbga8Ce9jeBmgDviBpIPAjYF/bo4DzgW+XsRcAx9jefjFi+4rtVmAEsKukEZ1jsX1xWX9suYZXFhEDwJq2d7X9g/K5BdgVGAOcLWkw8Bfg/eXa9gdOr7lXt9oeafvU0jay9NkS2F/SO0v7asCdtrcCbgE+UxNDvTUBtgcOsf2+xbhHERERERHL3Iq8pelu238GKFWjFuC2cu6y8vu9VAnTtFJA+ifgDmATYAvghtI+AHhc0lCqRGRqGX8R8MEexPJxSeOo7vewsmZ7p1g6qxtDzfnO4y63/SrwsKRHgE2BecAZkkYCC4GNFxHjTbYXAEi6D1gf+BPwEtDxPNZ04P3drAlwg+1/dLVQuRfjAAassc4iQoqIiIiI6FsrckL0Ys3xQt54rc+W36L6x/uBtQMlbQnc27kKJGlNwF2s9wpvrLgNLmM2AI4HtrX9hKRJHec6xdKZ6sWwiHGd4zJwHPB/wFYlthe6mAu6vl8v23ad9q7WrBfbGzvZE4GJAIOGDe/qfkZERERE9LkVacvc08DqiznmTmBHSe8GkLSqpI2BB4F1JG1f2gdK2tz2k8ACSTuV8WNr5poPjJS0Utlutl1pX4MqQVhQnr9ZVEWp9hrqxrCIsfuVtTei2iL4IDAUeLxUcQ6mqjJ1Xmdp1FszIiIiImK5scJUiGz/XdK08pKD56kqI92N+aukQ4FLJQ0qzV+1/VB5pfTpZZvcysAPgXuBw4DzJT0HTK6ZbhrVFrU5wFxgRlljtqSZZewjpV9XJlE9i/M81XM4XcVQz4PAVGBd4AjbL0g6E/iFpP2Am3m9ctMOvCJpdlnziUXEtCj11lzCqSIiIiIilj29vhsqFpekFuBa21s0OpblVWtrq9va2hodRkRERESswCRNLy85e5MVactcRERERETEYllhtsw1gu35VG+Ci4iIiIiI5VAqRBERERER0bSSEEVERERERNNKQhQREREREU0rCVFERERERDStJEQREREREdG0khBFRERERETTSkIUERERERFNK99DFA0159EFtIy/rtFh9AvzJ4xpdAgRERERTScVogaSdIyk+yU9IWl8L805WtK1vTHXUsTweUmrNjKGiIiIiIieSIWosY4CPmh7Xr2Tkla2/coyjqk3fB64GHiuwXFERERERCxSKkQNIulsYEPgGknHSTqjtE+S9F+SbgZOlrSRpN9Jmi7pVkmb1vQ7u7Q9JOnDddbYTtLtkmaW35uU9gGSvi9pjqR2SUeX9lGSppa1JksaVtrfLelGSbMlzSgxjZY0RdKVkh6QdIkqxwBvB24u1xARERER0W+lQtQgto+Q9AFgN6BzMrMxsIfthZJuAo6w/bCk9wBnAu8r/VqAXYGNqBKQd3ea5wFgF9uvSNoD+A6wDzAO2ADYupxbS9JA4EfAXrb/Kml/4NvAp4BLgAm2r5Y0mCqRfiewNbA58BgwDdjR9umSvgDsZvtvvXKzIiIiIiL6SBKi/umKkgwNAXYArpDUcW5QTb/Lbb8KPCzpEWDTTvMMBS6UNBwwMLC07wGc3bEdz/Y/JG0BbAHcUNYaADwuaXVgPdtXl74vAJQ+d9v+c/k8iypBu627i5M0jiopY8Aa6/TkfkRERERE9IkkRP3Ts+X3SsCTtkd20c/dfD4JuNn23pJagCmlXXX6CrjX9vZvaJTWWEScL9YcL6SH/z3ZnghMBBg0bHjnOCIiIiIilpk8Q9SP2X4KmCdpP4DyjM5WNV32k7SSpI2onkd6sNMUQ4FHy/GhNe3XA0dIWrnMu1YZu46k7UvbQEmblxj+LOmjpX1QD94g9zSw+uJdbURERETEspeEqP8bCxwuaTZwL7BXzbkHganAb6meM3qh09jvAd+VNI1qC1yH84D/AdrLvAfZfgnYl+pFDrOBWVTb9QAOBo6R1A7cDvxzNzFPBH6blypERERERH8nOzuWlkeSJgHX2r6y0bEsjUHDhnvYIT9sdBj9Qr6YNSIiIqJvSJpuu7XeuTxDFA215XpDaUsiEBERERENkoRoOWX70EbHEBERERGxvMszRBERERER0bSSEEVERERERNNKQhQREREREU0rCVFERERERDStJEQREREREdG0khBFRERERETTSkIUERERERFNK99DFA0159EFtIy/rtFh9Mj8fIFsRERExApnua4QSTpG0v2SnpA0vpu+b5d05SLOt0iau5TxPLM04/tCzT26ZAnGtkg6qNPn5yXNKj9n15wbJWmOpP+WdLok9dY1RERERET0leW9QnQU8EHb87rraPsxYN++D2nZKomHbL/aRZce36M6WoCDgJ/VtP3B9sg6fc8CxgF3Ar8BPgD8dgnWjIiIiIhYZpbbClGpTmwIXCPpOElnlPZJpUJxu6RHJO1b2l+rAEnaXNLdpcrRLml4mXaApHMl3SvpekmrlP4bSfqdpOmSbpW0aWnfQNIdku6RdFJNbMMk3VLmnytp59J+mKSHJE0t69TGvG/N+GfK7yGSbpI0o1Rf9qq5lvslnQnMAN4p6YsljnZJJ3Zxj1aTdH7pN7NmvgGSTqkZ/+8llAnAzuU6jlvE32IYsIbtO2wb+Cnw0SX6w0ZERERELEPLbUJk+wjgMWA34IlOp4cBOwEfpvpHfWdHAKeVSkcr8OfSPhz4se3NgSeBfUr7ROBo26OA44EzS/tpwFm2twX+t2b+g4DJZf6tgFklaTgR2BF4P7BZDy7zBWBv29uU6/xBzVa0TYCf2t66HA8HtgNGAqMk7VJ7j2yfCnwF+H2JdzfgFEmrAYcDC0r7tsBnJG0AjAdutT2yjAfYoCRTUzsSPWC9mntIOV6vB9cXEREREdFQy/uWua78smwhu0/SunXO3wF8RdI7gKtsP1zyjHm2Z5U+04EWSUOAHYArah6LGVR+78jrSdNFwMnl+B7gfEkDSyyzJO0OTLH9VwBJlwEbd3MdAr4jaRfgVaoko+N6/mj7znK8Z/mZWT4PoUqQbuk0357ARyQdXz4PBt5V2kfUVKmGlvEvdRr/OPAu23+XNAr4paTNS5yducuLksZRba9jwBrrdNUtIiIiIqLPragJ0Ys1x2/6x7rtn0m6CxgDTJb0aeCRTuMWAqtQVdGe7OK5GajzD3/bt5QkZgxwkaRTgKfq9S1eKet0PBP0T6V9LLAOMMr2y5LmUyUxAM92usbv2j6ni/lr++1j+8E3NFZrHm17cqf20Z2u60XKPbI9XdIfqJK6PwPvqOn6DqrKVF22J1JV3Rg0bHiXiVNERERERF9bbrfMLQ1JGwKP2D4duAYY0VVf208B8yTtV8ZK0lbl9DTggHI8tmb+9YG/2D4X+AmwDXAXMFrSW0vlaL+aZeYDo8rxXsDAcjy0zPOypN2A9bsIczLwqVLNQtJ6kt7WRb+jO7bdSdq6pv3IEheSNi5b6Z4GVq+5rnUkDSjHG1JVkR6x/TjwtKT3lrk/Cfyqi1gjIiIiIvqNFbVC1J39gU9Iepnq2Z9vAmssov9Y4CxJX6VKVn4OzAaOBX4m6VjgFzX9RwNfLPM/A3zS9uOSTqDarvc41csQBpT+5wK/knQ3cBOvV38uAX4tqQ2YBTxQLzjb10v6F+COkus8A3wC+EunricBPwTaS+Iyn+o5q/Oo3ig3o7T/leqlCO3AK5JmA5OA/wG+KekVqgraEbb/UeY+svRZhertcnnDXERERET0e6peChbLmqRDgVbbn2t0LI00aNhwDzvkh40Oo0fyxawRERERyydJ02231jvXrBWi6Ce2XG8obUk0IiIiIqJBkhA1iO1JVFvMIiIiIiKiQZrypQoRERERERGQhCgiIiIiIppYEqKIiIiIiGhaSYgiIiIiIqJpJSGKiIiIiIimlYQoIiIiIiKaVhKiiIiIiIhoWvkeomioOY8uoGX8dY0Oo0fm5wtkIyIiIlY4qRBFRERERETTamhCJOkYSfdLekLS+G76vl3SlYs43yJp7lLG80zN8SmS7i2/T5B0/NLMXTPvLpJmSHpF0r69MWdvq/m7XLIEY1skHdQXcUVERERE9LZGb5k7Cvig7XnddbT9GLAsE4h/B9ax/aKkE3pjQkkrA/8DHAr0SoK1hHEIkO1Xu+jS479LHS3AQcDPljC8iIiIiIhlpmEVIklnAxsC10g6TtIZpX2SpNMl3S7pkY4qSm0FSNLmku6WNEtSu6ThZdoBks4tlZ3rJa1S+m8k6XeSpku6VdKmpX0DSXdIukfSSTWxXQOsBtwlaf9OcY+UdGdZ92pJb+mmfYqk70iaChxre77tduDVTvMOk3RLuaa5knYu7YdJekjS1HJttfdp35rxz5TfQyTdVKpQcyTtVXP/7pd0JjADeKekL5Zrb5d0Yhd/l9UknV/6zayZb0CpnnWM//cSygRg53Idxy3BfxoREREREctMwxIi20cAjwG7AU90Oj0M2An4MNU/sDs7AjjN9kigFfhzaR8O/Nj25sCTwD6lfSJwtO1RVJWZM0v7acBZtrcF/rcmto8Az9seafuyTmv/FPiS7RHAHOAb3bQDrGl7V9s/6PqOcBAwuVzTVsAsScOAE4EdgfcDmy1ifIcXgL1tb0N1b39QKkIAmwA/tb11OR4ObAeMBEZJ2qX272L7VOArwO/LPdoNOEXSasDhwILSvi3wGUkbAOOBW8u9O7VegJLGSWqT1LbwuQU9uKSIiIiIiL7R6C1zXfll2c51n6R165y/A/iKpHcAV9l+uPybf57tWaXPdKBF0hBgB+CK1/MCBpXfO/J60nQRcPKigpI0lCq5mVqaLizz1m2vGdo5qarnHuB8SQOprn+WpN2BKbb/Wta/DNi4m3kEfEfSLlRVqPWAjnv4R9t3luM9y8/M8nkIVYJ0S6f59gQ+UvMM1WDgXaV9RE2VamgZ/1J3F2p7IlWSyqBhw91d/4iIiIiIvtJfE6IXa47V+aTtn0m6CxgDTJb0aeCRTuMWAqtQVcGeLJWXepbFP8if7a6D7VtKEjMGuEjSKcBTdB3fK5QKX6kA/VNpHwusA4yy/bKk+VRJTOc4BHzX9jndhCZgH9sPvqGxWvNo25M7tY/uZr6IiIiIiH5juXzttqQNgUdsnw5cA4zoqq/tp4B5kvYrYyVpq3J6GnBAOR7b3bq2FwBPdDzfAxwMTO2qfTGvaX3gL7bPBX4CbAPcBYyW9NZSOdqvZsh8YFQ53gsYWI6HlnlelrQbsH4XS04GPlUqaEhaT9Lbuuh3dMe2O0lb17QfWeJC0sZlK93TwOqLc+0REREREY2yXCZEwP7AXEmzgE2pnt9ZlLHA4ZJmA/dSJRAAxwKflXQPVSLRE4dQPUfTTvXszTe7aX8DSdtK+jNVcnOOpHvLqdFUzw3NpNrGd5rtx4ETqLYI3kj1MoQO5wK7SrobeA+vV38uAVoltZXrfqBeHLavp3oT3B2S5gBXUj+ROYkq2WpX9VKLjpdPnAfcB8wo7edQVRzbgVckzc5LFSIiIiKiv5OdRziWF5IOBVptf67RsfSW1tZWt7W1NTqMiIiIiFiBSZpuu7XeueW1QhQREREREbHU+utLFaIO25OASQ0OIyIiIiJihZEKUURERERENK0kRBERERER0bSSEEVERERERNNKQhQREREREU0rCVFERERERDStJEQREREREdG08trtaKg5jy6gZfx1jQ6jS/MnjGl0CBERERHRh1IhioiIiIiIprVcJ0SSjpF0v6QnJI3vpu/bJV25iPMtkuYuZTzPLM34vlBzjy5ZgrEtkg6q+fxWSTdLekbSGZ36TpH0oKRZ5edtvRF/RERERERfWt63zB0FfND2vO462n4M2LfvQ1q2JAmQ7Ve76NLje1RHC3AQ8LPy+QXga8AW5aezsbbblmCdiIiIiIiGWG4rRJLOBjYErpF0XEfFQtIkSadLul3SI5L2Le2vVYAkbS7p7lLJaJc0vEw7QNK5ku6VdL2kVUr/jST9TtJ0SbdK2rS0byDpDkn3SDqpJrZhkm4p88+VtHNpP0zSQ5KmlnVqY963Zvwz5fcQSTdJmiFpjqS9aq7lfklnAjOAd0r6YomjXdKJXdyj1SSdX/rNrJlvgKRTasb/ewllArBzuY7jbD9r+zaqxCgiIiIiYrm33CZEto8AHgN2A57odHoYsBPwYap/1Hd2BHCa7ZFAK/Dn0j4c+LHtzYEngX1K+0TgaNujgOOBM0v7acBZtrcF/rdm/oOAyWX+rYBZkoYBJwI7Au8HNuvBZb4A7G17m3KdPygVIYBNgJ/a3rocDwe2A0YCoyTtUnuPbJ8KfAX4fYl3N+AUSasBhwMLSvu2wGckbQCMB261PbKM784FJXn6Wk2cbyJpnKQ2SW0Ln1vQg2kjIiIiIvrG8r5lriu/LFvI7pO0bp3zdwBfkfQO4CrbD5d/v8+zPav0mQ60SBoC7ABcUfNv/EHl9468njRdBJxcju8Bzpc0sMQyS9LuwBTbfwWQdBmwcTfXIeA7knYBXgXWAzqu54+27yzHe5afmeXzEKoE6ZZO8+0JfETS8eXzYOBdpX1ETZVqaBn/Ujfx1Rpr+1FJqwO/AA4Gflqvo+2JVEkmg4YN92KsERERERHRq1bUhOjFmuM3VSps/0zSXcAYYLKkTwOPdBq3EFiFqor2ZKn21POmf9DbvqUkMWOAiySdAjxVr2/xSlmn45mgfyrtY4F1gFG2X5Y0nyqJAXi20zV+1/Y5Xcxf228f2w++obFa82jbkzu1j+5mvtfYfrT8flrSz6iqVXUTooiIiIiI/mK53TK3NCRtCDxi+3TgGmBEV31tPwXMk7RfGStJW5XT04ADyvHYmvnXB/5i+1zgJ8A2wF3A6PKmtoHAfjXLzAdGleO9gIHleGiZ52VJuwHrdxHmZOBTpZqFpPW6eMvbZODoju1skrauaT+yxIWkjctWuqeB1bu6NzXXu7KktcvxQKqtikv1xr6IiIiIiGWhKRMiYH9grqRZwKZ0X8kYCxwuaTZwL1XSAnAs8FlJ91AlLx1GUz03NJNqS91pth8HTqDarncj1csQOpwL7CrpbuA9vF79uQRoldRWYnigXnC2r6d6E9wdkuYAV1I/kTmJKtlqLy+Y6HgRxHnAfcCM0n4OVfWwHXhF0mxJxwGUKtV/AYdK+rOkzai2EE6W1A7MAh4t1xQRERER0a/JziMcjSDpUKDV9ucaHUsjtba2uq0tb+qOiIiIiL4jabrt1nrnmrVCFBERERERscK+VKHfsz0JmNTgMCIiIiIimloqRBERERER0bSSEEVERERERNNKQhQREREREU0rCVFERERERDStJEQREREREdG0khBFRERERETTymu3o6HmPLqAlvHXLdUc8yeM6aVoIiIiIqLZpEIUERERERFNKwnREpB0gqTjy/EUSa2LOf728rtF0twu+rw2r6TfSFpzKcPuaWybSpolaaakjSQdVHPurZJulvSMpDNq2lcvYzp+/ibph8si3oiIiIiIpZGEqBuq9Op9sr3DYvb/kO0nezOGRfgo8CvbWwPvBA6qOfcC8DXg+E7xPW17ZMcP8EfgqmUTbkRERETEkmuahEjSSZKOrfn8bUnHSLpJ0gxJcyTtVc61SLpf0pnADOCdkr4i6UFJNwKbdJr+E5JulzRX0nZljteqSOXzXEkt5fiZOvGtIunnktolXQasUnNuvqS1a+I6V9K9kq6XtErps20Ze4ekUzoqT5I2l3R3qdy0Sxpe2l+7HkmXSjpe0oeAzwOflnQzMAHYuYw9zvaztm+jSoy6us/DgbcBt/bsLxMRERER0ThNkxABPwEOASgVnwOAy4C9bW8D7Ab8QJJK/02An5ZKydql/9bAx4BtO829Wqn6HAWcv4TxHQk8Z3sE8G1gVBf9hgM/tr058CSwT2m/ADjC9vbAwpr+RwCnlcpNK/BnSaPqXY/t3wBnA6fa3g0YD9xaKj+n9vA6DgQus+2uOkgaJ6lNUtvC5xb0cNqIiIiIiN7XNG+Zsz1f0t8lbQ2sC8wE/gGcKmkX4FVgvXIO4I+27yzHOwNX234OQNI1naa/tKxxi6Q1lvB5n12A08s87ZLau+g3z/ascjwdaCnrrW779tL+M+DD5fgO4CuS3gFcZfthSd1dz9I4ADh4UR1sTwQmAgwaNrzLxCkiIiIioq81TUJUnAccCvwzVSVnLLAOMMr2y5LmA4NL32c7jV3UP9w7nzPwCm+swA2mez1JDl6sOV5ItbVOXfTF9s8k3QWMASZL+vRirLVYJG0FrGx7em/PHRERERHRF5ppyxzA1cAHqLaITQaGAn8pydBuwPpdjLsF2Ls857M68G+dzu8PIGknYIHtBcB8YJvSvg2wQTex3UKVoCFpC2BETy/K9hPA05LeW5oO6DgnaUPgEdunA9eUebu7ng5PA6v3NA6q7XKXLkb/iIiIiIiGaqoKke2XyssCnrS9UNIlwK8ltQGzgAe6GDejvOhgFtUb1Dq/MOCJ8irtNYBPlbZfAJ+UNAu4B3iom/DOAi4oW+VmAXcv3tVxOHCupGeBKUDHwzn7U7304WXgf4Fv2v5HN9fToR14RdJsYJLtU0sVbQ3gnyR9FNjT9n2l/8eBDy1m3BERERERDaNFPPu+wikvU5gB7Gf74UbH05skDbH9TDkeDwyzfWw3wzrGngA8Y/v7fRhiXa2trW5ra1vWy0ZEREREE5E03Xbd7w5tmi1zkjYD/hu4aUVLhoox5fXYc6leAvGtRgcUEREREdHfNc2WubKta8NGx9FXbF9G9RrxJRl7Qu9GExERERGxfGiaClFERERERERnSYgiIiIiIqJpJSGKiIiIiIimlYQoIiIiIiKaVhKiiIiIiIhoWkmIIiIiIiKiaTXNa7ejf5rz6AJaxl/X4/7zJ4zpw2giIiIiotmkQhQREREREU0rCdFikNQiae5SznGopDMWc8yXa47XlHRUD8fNl7T24sYYEREREdEskhAtH75cc7wm0KOEaGlIynbKiIiIiFjhJSFafAMknSvpXknXS1pF0mck3SNptqRfSFoVQNJ+kuaW9ltq5ni7pN9JeljS9zoaJR0oaU4Zc3JpmwCsImmWpEuACcBG5fMpkkZLurZmjjMkHVqz1hcl3V1+3l36rC/pJknt5fe7SvskSf8l6WbgZEm7lnVmSZopafWy3i2SrpZ0n6SzJa1Uxp8lqa3cmxP75O5HRERERPSiJESLbzjwY9ubA08C+wBX2d7W9lbA/cDhpe/XgX8t7R+pmWMksD+wJbC/pHdKejtwMvC+cn5bSR+1PR543vZI22OB8cAfyucv9iDep2xvB5wB/LC0nQH81PYI4BLg9Jr+GwN72P5/wPHAZ22PBHYGni99tgP+X4l/I+Bjpf0rtluBEcCukkbUC0jSuJI4tS18bkEPLiEiIiIiom8kIVp882zPKsfTgRZgC0m3SpoDjAU2L+enAZMkfQYYUDPHTbYX2H4BuA9YH9gWmGL7r7ZfoUpUdumFeC+t+b19Od4e+Fk5vgjYqab/FbYX1sT/X5KOAdYscQHcbfuR0u/SmvEflzQDmEl1DzarF5DtibZbbbcOWHXoUl5eRERERMSSS0K0+F6sOV5I9eryScDnbG8JnAgMBrB9BPBV4J3ALElvXcQcWsJ4XuGNf8fBnc67i+Ou+jz7WqM9Afg0sApwp6RNu5jHkjagqijtXipP19WJJSIiIiKiX0lC1DtWBx6XNJCqQgSApI1s32X768DfqBKjrtxFtc1sbUkDgAOBqeXcy2VugKfLeh3+CGwmaZCkocDunebdv+b3HeX4duCAcjwWuK1eQCX+ObZPBtqAjoRoO0kblGeH9i/j16BKphZIWhf44CKuNSIiIiKiX8ibxHrH16gSmj8Cc3g9YTlF0nCq6s9NwGyq54PexPbjkv4TuLn0/43tX5XTE4F2STNsj5U0rbz++7e2vyjpcqAdeJhqu1qtQZLuokp+DyxtxwDnS/oi8FfgsC6u6/OSdqOqYt0H/JZqu90dVC932BK4Bbja9quSZgL3Ao9QbbeLiIiIiOjXZHe1iyrizSSNBo63/eHemK+1tdVtbW29MVVERERERF2SppeXf71JtsxFRERERETTypa5WCy2pwBTGhxGRERERESvSIUoIiIiIiKaVhKiiIiIiIhoWkmIIiIiIiKiaSUhioiIiIiIppWEKCIiIiIimlYSooiIiIiIaFp57XY01JxHF9Ay/roe958/YUwfRhMRERERzSYVooiIiIiIaForbEIkaU1JR/XSXJMk7dvTPpLOk7RZb6wdERERERF9Z4VNiIA1gV5JiBaX7U/bvq8Ra/c3kgY0OoaIiIiIiK6syAnRBGAjSbMknSrpJkkzJM2RtBeApG0ltUsaLGk1SfdK2kKVMyTdJ+k64G0dk0oaJWmqpOmSJksa1nlhSVMktZbjZyR9W9JsSXdKWre0byDpDkn3SDpJ0jOlfbSka2vmOkPSoYtau6x3sqS7JT0kaefSPkDS98s1t0s6WtLukq6umf/9kq6SdLikU2vaPyPpv8rxJ8rcsySd05HkSDpLUlu5byfWjJ0v6euSbgP2W9o/ZEREREREX1mRE6LxwB9sjwS+COxtextgN+AHkmT7HuAa4FvA94CLbc8F9gY2AbYEPgPsACBpIPAjYF/bo4DzgW93E8dqwJ22twJuKfMBnAacZXtb4H+7u5gerL2y7e2AzwPfKG3jgA2ArW2PAC4Bfg/8i6R1Sp/DgAuAnwMfKeu81i7pX4D9gR3LvVwIjC19vmK7FRgB7CppRE08L9jeyfbP61zLuJJItS18bkF3lx4RERER0Wea5S1zAr4jaRfgVWA9YF2qROSbwD3AC8Axpf8uwKW2FwKPSfp9ad8E2AK4QRLAAODxbtZ+Ceio+EwH3l+OdwT2KccXASd3M093a19Vs0ZLOd4DONv2KwC2/wEg6SLgE5IuALYHPmn7lXKdH5Z0PzDQ9hxJnwNGAfeUdVcB/lLm/7ikcVT/HQ0DNgPay7nLuroQ2xOBiQCDhg13N9cdEREREdFnmiUhGgusA4yy/bKk+cDgcm4tYAgwsLQ9W9rr/UNdwL22t1+MtV+23THXQt54z+ut8QpvrNx1xNnd2i/WWUNdrHEB8GuqJPCKjoQJOA/4MvBA6dMxx4W2/7N2AkkbAMcD29p+QtKkmljh9fsYEREREdFvrchb5p4GVi/HQ4G/lGRoN2D9mn4Tga9RbSfrqNLcAhxQnsEZRrXNDuBBYB1J20O1jU3S5ksY3zTggHI8tqb9j8BmkgZJGgrsvhRrXw8cIWnlMmYtANuPAY8BXwUmdXS2fRfwTuAg4NLSfBOwr6S3dcwhaX1gDaqkZ0F5LuqDi3f5ERERERGNt8JWiGz/XdI0SXOptsRtKqkNmEVVAUHSJ4FXbP+svCjgdknvA64G3gfMAR4CppY5X1L1au3TS7KyMvBD4N4lCPFY4GeSjgV+URP3nyRdTrX17GFg5lKsfR6wMdAu6WXgXOCMcu4SYJ06b8O7HBhp+4my7n2SvgpcL2kl4GXgs7bvlDSzrP8IVYIXEREREbFc0eu7uaKRJD1je8gyXO8MYKbtn3RqvxY41fZNyyKO1tZWt7W1LYulIiIiIqJJSZpeXgb2JivylrnogqTpVG+Gu7imbU1JDwHPL6tkKCIiIiKi0VbYLXPLm2VZHSqv7e7c9iTV9rqIiIiIiKaRClFERERERDStJEQREREREdG0khBFRERERETTSkIUERERERFNKwlRREREREQ0rSREERERERHRtJIQRURERERE08r3EEVDzXl0AS3jr+tx//kTxvRhNBERERHRbJqiQiRpTUlH9dJckyTt29M+ks6TtFlvrL0sSdpP0v2SbpY0WtK1jY4pIiIiIqK3NUVCBKwJ9EpCtLhsf9r2fY1YeykdDhxle7dGBxIRERER0VeaJSGaAGwkaZakUyXdJGmGpDmS9gKQtK2kdkmDJa0m6V5JW6hyhqT7JF0HvK1jUkmjJE2VNF3SZEnDOi8saYqk1nL8jKRvS5ot6U5J65b2DSTdIekeSSdJeqa0v6EyU+I4dFFrl/VOlnS3pIck7VzaB0j6frnmdklHS9pd0tU1879f0lWSvg7sBJwt6ZRO17OdpNslzSy/Nyntq0q6vMx9maS7Oq47IiIiIqK/apaEaDzwB9sjgS8Ce9veBtgN+IEk2b4HuAb4FvA94GLbc4G9gU2ALYHPADsASBoI/AjY1/Yo4Hzg293EsRpwp+2tgFvKfACnAWfZ3hb43+4upgdrr2x7O+DzwDdK2zhgA2Br2yOAS4DfA/8iaZ3S5zDgAtvfBNqAsba/2Gn5B4BdbG8NfB34Tmk/CniizH0SMGoR8Y+T1CapbeFzC7q73IiIiIiIPtOML1UQ8B1JuwCvAusB61IlIt8E7gFeAI4p/XcBLrW9EHhM0u9L+ybAFsANkgAGAI93s/ZLQEfFZzrw/nK8I7BPOb4IOLmbebpb+6qaNVrK8R7A2bZfAbD9DwBJFwGfkHQBsD3wyW7WHgpcKGk4YGBgad+JKrHD9lxJ7V1NYHsiMBFg0LDh7ma9iIiIiIg+04wJ0VhgHWCU7ZclzQcGl3NrAUOo/pE/GHi2tNf7R7uAe21vvxhrv2y7Y66FvPH+11vjFd5YxeuIs7u1X6yzhrpY4wLg11RJ4BUdCdMinATcbHtvSS3AlJr5IyIiIiKWK82yZe5pYPVyPBT4S0mGdgPWr+k3Efga1XayjirNLcAB5RmcYVTb7AAeBNaRtD1U29gkbb6E8U0DDijHY2va/whsJmmQpKHA7kux9vXAEZJWLmPWArD9GPAY8FVgUg9iHQo8Wo4PrWm/Dfh4mXszqi2GERERERH9WlMkRLb/DkyTNBcYCbRKaqNKPh4AkPRJ4BXbP6N6CcO2kt4HXA08DMwBzgKmljlfAvYFTpY0G5hFeb5oCRwLfFbSPVQJR0fcfwIuB9qpkrSZS7H2ecD/AO1lzEE15y4B/tTDt+F9D/iupGlUW/U6nEmVpLUDXyox5wGhiIiIiOjX9PoOrugvJD1je8gyXO8MYKbtnyzFHAOAgbZfkLQRcBOwcUneutTa2uq2trYlXTYiIiIioluSptuu+wbkZnyGKGpImk71rNT/W8qpVgVuLm/AE3Bkd8lQRERERESjJSHqh5Zldai8trs35nkayPcORURERMRypSmeIYqIiIiIiKgnCVFERERERDStJEQREREREdG0khBFRERERETTSkIUERERERFNKwlRREREREQ0rSREERERERHRtPI9RNFQcx5dQMv46+qemz9hzDKOJiIiIiKaTSpEKwBJUyS1luMv99Ea50narE77oZLOKMdfkHSfpHZJN0lavy9iiYiIiIjoLUmIVjx9khDZ/rTt+7rpNhNotT0CuBL4Xl/EEhERERHRW5IQNZCkFkkPSLqwVFWulLSqpN0lzZQ0R9L5kgaV/nXba+abAKwiaZakSySdJOnYmvPflnSMpNGSbpF0danonC1ppdJnT0l3SJoh6QpJQ0p7bRXqMEkPSZoK7Ngxv+2bbT9XPt4JvKMv719ERERExNJKQtR4mwATS1XlKeALwCRgf9tbUj3ndaSkwfXaayeyPR543vZI22OBnwCHAJSE5wDgktJ9O+D/AVsCGwEfk7Q28FVgD9vbAG0lntdIGgacSJUIvR940za64nDgt/VOSBonqU1S28LnFnR7gyIiIiIi+koSosb7k+1p5fhiYHdgnu2HStuFwC5UiVO99i7Zng/8XdLWwJ7ATNt/L6fvtv2I7YXApcBOwHupEpxpkmZRJVOdnwN6DzDF9l9tvwRc1nldSZ8AWoFTuohrou1W260DVh26qEuIiIiIiOhTectc47mH/bSE858HHAr8M3D+ItZ1WeMG2wd2M2eXMUvaA/gKsKvtFxc72oiIiIiIZSgVosZ7l6Tty/GBwI1Ai6R3l7aDganAA120d/aypIE1n68GPgBsC0yuad9O0gZlK93+wG1Uz/3s2LFGeZ5p407z3wWMlvTWss5+HSdKJeoc4CO2/9LzWxARERER0RhJiBrvfuAQSe3AWsCpwGHAFZLmAK8CZ9t+oV57nfkmAu2SLgEo29puBi4v2+M63AFMAOYC84Crbf+Vqpp0aYnnTmDT2sltPw6cUMbfCMyoOX0KMKTEOEvSNUt0RyIiIiIilhHZPd2xFb1NUgtwre0t+nCNlaiSlv1sP1zaRgPH2/5wX63bU62trW5ra2t0GBERERGxApM03XZrvXOpEK3Ayhep/jdwU0cyFBERERERr8tLFRqovAWuz6pD5YtUN6zTPgWY0lfrRkREREQsL1IhioiIiIiIppWEKCIiIiIimlYSooiIiIiIaFpJiCIiIiIiomklIYqIiIiIiKaVhCgiIiIiIppWEqKIiIiIiGha+R6iaKg5jy6gZfx1b2qfP2FMA6KJiIiIiGaTCtFyStIUSa2LOP9NSXssy5giIiIiIpY3qRCtoGx/vdExRERERET0d6kQLWOSWiQ9IOlCSe2SrpS0qqTdJc2UNEfS+ZIGlf5122vmGyBpkqS5pc9xpX2SpH0ltUqaVX7mSHI5v5Gk30maLulWSZuW9v3KXLMl3VLaDpV0Rs2a10oaXY6fkXRymedGSduV6tUjkj6yLO5pRERERMSSSkLUGJsAE22PAJ4CvgBMAva3vSVV5e5ISYPrtXeaaySwnu0tSp8Lak/abrM90vZI4HfA98upicDRtkcBxwNnlvavA/9qeyugJwnNasCUMs/TwLeA9wN7A9+sN0DSOEltktoWPregB0tERERERPSNJESN8Sfb08rxxcDuwDzbD5W2C4FdqBKneu21HgE2lPQjSR+gSrDeRNLHgW2A8ZKGADsAV0iaBZwDDCtdpwGTJH0GGNCDa3mJKtECmANMtf1yOW6pN8D2RNuttlsHrDq0B0tERERERPSNPEPUGO5hP3U7kf2EpK2AfwU+C3wc+NQbJpE2B04EdrG9UNJKwJOlatR5viMkvQcYA8ySNBJ4hTcmz4Nrjl+23XE9rwIvlnlelZT/viIiIiKiX0uFqDHeJWn7cnwgcCPQIundpe1gYCrwQBftr5G0NrCS7V8AX6OqAtWeHwr8HPik7b8C2H4KmCdpv9JHJalC0ka27yovZfgb8E5gPjBS0kqS3gls10v3ISIiIiKiofJ/8BvjfuAQSecADwPHAndSbWFbGbgHONv2i5IO69zeaa71gAtK1QfgPzud/yiwPnCuVBWcSmVoLHCWpK8CA6mSptnAKZKGU1WnbiptAPOotsHNBWYs7Q2IiIiIiOgP9Ppup1gWJLUA19reotGx9Aetra1ua2trdBgRERERsQKTNN123e/wzJa5iIiIiIhoWtkyt4zZng+kOhQRERER0Q+kQhQREREREU0rCVFERERERDStJEQREREREdG0khBFRERERETTSkIUERERERFNKwlRREREREQ0rSREERERERHRtPI9RNFQcx5dQMv4697QNn/CmAZFExERERHNJhWi5YSkKZJay/GX+2iN8yRtVqf9UElnlONdJM2Q9IqkfTv1O0TSw+XnkL6IMSIiIiKiNyUhWj71SUJk+9O27+um2/8AhwI/q22UtBbwDeA9wHbANyS9pS/ijIiIiIjoLUmI+pikFkkPSLpQUrukKyWtKml3STMlzZF0vqRBpX/d9pr5JgCrSJol6RJJJ0k6tub8tyUdI2m0pFskXS3pPklnS1qp9NlT0h2l0nOFpCGlvbYKdZikhyRNBXbsmN/2fNvtwKudLvVfgRts/8P2E8ANwAd6/YZGRERERPSiJETLxibARNsjgKeALwCTgP1tb0n1LNeRkgbXa6+dyPZ44HnbI22PBX4CHAJQEp4DgEtK9+2A/wdsCWwEfEzS2sBXgT1sbwO0lXheI2kYcCJVIvR+4E3b6OpYD/hTzec/l7aIiIiIiH4rCdGy8Sfb08rxxcDuwDzbD5W2C4FdqBKneu1dsj0f+LukrYE9gZm2/15O3237EdsLgUuBnYD3UiU40yTNokqm1u807XuAKbb/avsl4LIeXKPqhVe3ozROUpuktoXPLejB1BERERERfSNvmVs26iYGddRLKnriPKrnev4ZOH8R67qscYPtA7uZs6cxd/gzMLrm8zuAKXUnticCEwEGDRu+uOtERERERPSaVIiWjXdJ2r4cHwjcCLRIendpOxiYCjzQRXtnL0saWPP5aqrndbYFJte0bydpg7KVbn/gNuBOYMeONcrzTBt3mv8uYLSkt5Z19uvBNU4G9pT0lvIyhT07xRIRERER0e8kIVo27gcOkdQOrAWcChwGXCFpDtULCs62/UK99jrzTQTaJV0CULa13QxcXrbHdbgDmADMBeYBV9v+K1U16dISz53AprWT234cOKGMvxGY0XFO0raS/kyVJJ0j6d4y5h/AScA95eebpS0iIiIiot+SnR1LfUlSC3Ct7S36cI2VqJKW/Ww/XNpGA8fb/nBfrdsbWltb3dbW1ugwIiIiImIFJmm67dZ651IhWs6VL1L9b+CmjmQoIiIiIiJ6Ji9V6GPlLXB9Vh0qX6S6YZ32KXTxUoOIiIiIiKikQhQREREREU0rCVFERERERDStJEQREREREdG0khBFRERERETTSkIUERERERFNKwlRREREREQ0rSREERERERHRtPI9RNFQcx5dQMv46177PH/CmAZGExERERHNZoWtEEk6RtL9kp6QNL6bvm+XdOUizrdImruU8TyzNOP7Qs09umQJxrZIOqhT2whJd0i6V9IcSYN7L9qIiIiIiN63IleIjgI+aHtedx1tPwbs2/chLVuSBMj2q1106fE9qqMFOAj4WVlrZeBi4GDbsyW9FXh5CeaNiIiIiFhmVsgKkaSzgQ2BayQdJ+mM0j5J0umSbpf0iKR9S/trFSBJm0u6W9IsSe2ShpdpB0g6t1Q/rpe0Sum/kaTfSZou6VZJm5b2DUq15B5JJ9XENkzSLWX+uZJ2Lu2HSXpI0tSyTm3M+9aMf6b8HiLpJkkzSjVmr5pruV/SmcAM4J2SvljiaJd0Yhf3aDVJ55d+M2vmGyDplJrx/15CmQDsXK7jOGBPoN32bADbf7e9sBf/rBERERERvW6FTIhsHwE8BuwGPNHp9DBgJ+DDVP+o7+wI4DTbI4FW4M+lfTjwY9ubA08C+5T2icDRtkcBxwNnlvbTgLNsbwv8b838BwGTy/xbAbMkDQNOBHYE3g9s1oPLfAHY2/Y25Tp/UCpCAJsAP7W9dTkeDmwHjARGSdql9h7ZPhX4CvD7Eu9uwCmSVgMOBxaU9m2Bz0jaABgP3Gp7ZBm/MWBJk0uS9h89uIaIiIiIiIZakbfMdeWXZQvZfZLWrXP+DuArkt4BXGX74ZJnzLM9q/SZDrRIGgLsAFzxei7CoPJ7R15Pmi4CTi7H9wDnSxpYYpklaXdgiu2/Aki6jCrBWBQB35G0C/AqsB7QcT1/tH1nOd6z/Mwsn4dQJUi3dJpvT+Ajko4vnwcD7yrtI2qqVEPL+Jc6jV+ZKtHcFngOuEnSdNs3vSlwaRwwDmDAGut0c5kREREREX2nGROiF2uO1fmk7Z9JugsYA0yW9GngkU7jFgKrUFXYnizVnnpcZ/5bShIzBrhI0inAU/X6Fq+UdTqeCfqn0j4WWAcYZftlSfOpkhiAZztd43dtn9PF/LX99rH94BsaqzWPtj25U/voTuP/DEy1/bdy/jfANsCbEiLbE6kqawwaNryr646IiIiI6HMr5Ja5pSFpQ+AR26cD1wAjuupr+ylgnqT9ylhJ2qqcngYcUI7H1sy/PvAX2+cCP6FKGu4CRkt6a6kc7VezzHxgVDneCxhYjoeWeV6WtBuwfhdhTgY+VapZSFpP0tu66Hd0x7Y7SVvXtB9Z4kLSxmUr3dPA6p3Gj5C0qqoXLOwK3NdFTBERERER/UISojfbH5graRawKfDTbvqPBQ6XNBu4lyppATgW+Kyke6iSlw6jqZ4bmkm1pe40248DJ1Bt17uR6mUIHc4FdpV0N/AeXq/+XAK0SmorMTxQLzjb11O9Ce4OSXOAK3ljItPhJKpkq728YKLjRRDnUSU2M0r7OVSVxXbgFUmzJR1n+wngv6i2BM4CZti+joiIiIiIfkx2diz1N5IOBVptf67RsfS11tZWt7W1NTqMiIiIiFiBlWfbW+udS4UoIiIiIiKaVjO+VKHfsz0JmNTgMCIiIiIiVnipEEVERERERNNKQhQREREREU0rCVFERERERDStJEQREREREdG0khBFRERERETTSkIUERERERFNKwlRREREREQ0rSRE0VBzHl1Ay/jraBl/XaNDiYiIiIgmlISon5J0gqTjF3NMi6S5ddpHSvpQ70X32rxvl3RlF+emSGrt7TUjIiIiInpTEqLmMBLo9YTI9mO29+3teSMiIiIilpUkRP2IpK9IelDSjcAmpW0jSb+TNF3SrZI2Le3rSrpa0uzys0OnuTaUNFPSe4BvAvtLmiVpf0kPS1qn9FtJ0n9LWlvSJElnl3UekvTh0meApFMk3SOpXdK/l/bXKlKSVpH083L+MmCVZXXfIiIiIiKW1MqNDiAqkkYBBwBbU/1dZgDTgYnAEbYfLsnNmcD7gNOBqbb3ljQAGAK8pcy1CfBz4DDbsyR9HWi1/blyflNgLPBDYA9gtu2/SQJoAXYFNgJulvRu4JPAAtvbShoETJN0PeCaSzgSeM72CEkjSvwREREREf1aEqL+Y2fgatvPAUi6BhgM7ABcUZIVgEHl9/uoEhVsLwQWSHoLsA7wK2Af2/d2sdb5pc8PgU8BF9Scu9z2q8DDkh4BNgX2BEZI6tgeNxQYDjxUM24XqiQN2+2S2ru6UEnjgHEAA9ZYp6tuERERERF9LglR/+JOn1cCnrQ9cjHmWAD8CdgRqJsQ2f6TpP+T9D7gPVTVoq5iMCDgaNuTa09Iaukm/rpsT6SqfDFo2PAejYmIiIiI6At5hqj/uAXYuzyLszrwb8BzwDxJ+wGoslXpfxPVNrWOZ3zWKO0vAR8FPinpoNL2NLB6p/XOAy6mqggtrGnfrzxXtBGwIfAgMBk4UtLAst7GklarE//Ycn4LYMQS3oeIiIiIiGUmCVE/YXsGcBkwC/gFcGs5NRY4XNJsqorPXqX9WGA3SXOonjXavGauZ4EPA8dJ2gu4Gdis46UKpds1VM8d1W6XgyoBmgr8lurZpReokqf7gBnlJQrn8Obq4lnAkLJV7j+Au5fwVkRERERELDOys2OpGZXvCDrV9s41bZOAa23X/W6hvtDa2uq2trZltVxERERENCFJ023X/Y7MPEPUhCSNp9puN7a7vhERERERK7IkRE3I9gRgQp32Q5d9NBERERERjZNniCIiIiIiomklIYqIiIiIiKaVhCgiIiIiIppWEqKIiIiIiGhaSYgiIiIiIqJpJSGKiIiIiIimlYQoIiIiIiKaVhKiaKg5jy6gZfx1jQ4jIiIiIppUUyREklokzV2M/h+RNL4cnyDp+EXNKalV0um9F/Hix9yD+Q6V9Pbemq9m3iMkfbJOe6/GHxERERHRF1ZudAD9ke1rgGsWo38b0NZ3EfWKQ4G5wGO9Oants3tzvoiIiIiIZakpKkTFypIulNQu6UpJq0qaL2lteK3KM6UcHyrpjM4TSBolabakO4DP1rSPlnRtOT5B0vmSpkh6RNIxNf2+JukBSTdIurSj8iTp3ZJuLHPPkLRRp3VbJN1azs2QtENpHybpFkmzJM2VtLOkAZImlc9zJB0naV+gFbik9B0j6eqa+d8v6apy/IykH5R1bpK0TmnfSNLvJE0vsWxac70d11H3/kRERERE9FfNlBBtAky0PQJ4CjhqCea4ADjG9vbd9NsU+FdgO+AbkgZKagX2AbYGPkaVoHS4BPix7a2AHYDHO833F+D9trcB9gc6tucdBEy2PRLYCpgFjATWs72F7S2BC2xfSVXBGlv6/gb4l45kBzisXBvAasCMstZU4BulfSJwtO1RwPHAmUtxfyIiIiIi+oVmSoj+ZHtaOb4Y2GlxBksaCqxpe2ppumgR3a+z/aLtv1ElM+uW9X5l+3nbTwO/LvOuTpXAXA1g+wXbz3WabyBwrqQ5wBXAZqX9HuAwSScAW5Z5HwE2lPQjSR+gSv7ewLZL/J+QtCawPfDbcvpV4LJyfDGwk6QhVInaFZJmAecAw5b0/kgaJ6lNUtvC5xZ01S0iIiIios810zNErvP5FV5PCgd3M1515ujKizXHC6nusxYxb3eOA/6Pqgq0EvACgO1bJO0CjAEuknSK7Z9K2oqqQvVZ4OPAp+rMeQFVUvYCcIXtV7pY22XNJ0t1qSs9vj+2J1JVnBg0bHhP72lERERERK9rpgrRuyR1bOU6ELgNmA+MKm37LGqw7SeBBZI6KktjF3P924B/kzS4VFzGlHmfAv4s6aMAkgZJWrXT2KHA47ZfBQ4GBpS+6wN/sX0u8BNgm/JM1Eq2fwF8DdimzPE0sHrN9TxG9YKFrwKTatZaCdi3HB8E3FZinCdpv7KuStL1ml64PxERERERy1wzJUT3A4dIagfWAs4CTgROk3QrVSWnO4cBPy4vDXh+cRa3fQ/Vm+tmA1dRPdPTsV/sYOCYEtvtwD93Gn5mif1OYGPg2dI+GpglaSZVQncasB4wpWxtmwT8Z+k7CTi7vFRhldJ2CdVWwvtq1noW2FzSdOB9wDdL+1jgcEmzgXuBvepc5hLfn4iIiIiIRlD1OEksC5KG2H6mVIBuAcbZ/v/t3XmYXUWd//H3hyQsEggiGSeAEsBAZE1Ig7KaKKKAgggMm4OAyjCMMDjDT3FwQRgVBhRBVIwIAUR0WGRRMSAmhB06kI3dAVQEJeyEPeHz++NU6+VyO91Juvve5H5ez8PT51bVqfqecxKe+03Vqb6jifGcAdxp+8c1ZfNsDx2oGFYYMcojPvkdHj5x14EaMiIiIiLajKTptjsa1bXTO0StYKKkjajeVzq3ycnQdKrZoP9sVgwAm641jM4kQxERERHRJEmIBpDt/ZsdQ5eyfXaj8gGbHYqIiIiIaLZ2eocoIiIiIiLiDZIQRURERERE20pCFBERERERbSsJUUREREREtK0kRBERERER0baSEEVERERERNtKQhQREREREW0rCVE01ew/P9vsECIiIiKijS3VCZGkIyXdI+lpScf00HZNSRcvpH6kpDlLGM+8JTm/P9TcowsW49yRkvav+fw2SVMkzZN0Rl3b30iaKekuSWdKGtQX8UdERERE9KfBzQ5gCR0O7Gz7oZ4a2n4U2Kv/QxpYkgTI9uvdNOn1PWpgJLA/8NPy+WXgy8Am5b9a/2T7uRLPxcDewM8WY8yIiIiIiAGz1M4QSToTWA+4QtLnumYsJE2SdLqkmyQ9KGmvUv63GSBJG0u6TdIMSbMkjSrdDpL0ozLLcbWklUr79csMyHRJ10saXcrXlXSzpNslnVAT2whJ00r/cyRtX8oPlnS/pOvKOLUx71Vz/rzyc6ikayXdIWm2pN1rruUeSd8H7gDeIen/lThmSfpaN/doZUlnl3Z31vQ3SNLJNef/SwnlRGD7ch2fs/2C7RuoEqM3sP1cORwMLA94sR9uRERERMQAWWoTItuHAY8CE4Cn66pHANsBH6H6Ul/vMOA022OADuCRUj4K+J7tjYFngD1L+UTgCNvjgKOB75fy04Af2N4S+EtN//sDk0v/mwMzJI0AvgZsC3wQ2KgXl/kysIftLcp1fqvMwABsCJxne2w5HgVsBYwBxknaofYe2T4VOBb4XYl3AnCypJWBTwHPlvItgc9IWhc4Brje9phy/kJJmgw8DjxPNUsUEREREdHSlvYlc925rCwhu1vS2xvU3wwcK2lt4FLbD5Q84yHbM0qb6cBISUOBbYCL/p6LsEL5uS1/T5rOB04qx7cDZ0saUmKZIekDwFTbcwEk/RzYoIfrEPANSTsArwNrAV3X8wfbt5Tjncp/d5bPQ6kSpGl1/e0E7Cbp6PJ5ReCdpXyzmlmqYeX8V3uI7w1sf0jSisAFwPuBaxpelHQocCjAoFWHL8oQERERERF9allNiF6pOVZ9pe2fSroV2BWYLOnTwIN15y0AVqKaRXumzPY08qalYbanlSRmV+B8SScDzzVqW8wv43S9E7R8KT8AGA6Ms/2apIepkhiAF+qu8Zu2f9hN/7Xt9rR93xsKqzGPsD25rnx8D/29ie2XJV0B7E43CZHtiVSzbqwwYlSW1kVERERE0yy1S+aWhKT1gAdtnw5cAWzWXdvybsxDkvYu50rS5qX6RmDfcnxATf/rAI/b/hHwY2AL4FZgfNmpbQjVpgNdHgbGlePdgSHleFjp5zVJE4B1uglzMnBImc1C0lqS/qGbdkd0LbuTNLam/F9LXEjaoCylex5Ypbt7U3O9Q8uSQCQNBnYB7u3pvIiIiIiIZmvLhAjYB5gjaQYwGjivh/YHAJ+SNBO4iyppAfh34N8k3U6VvHQZT/Xe0J1US+pOs/0YcBzVcr3fUm2G0OVHwPsk3Qa8h7/P/lwAdEjqLDE0TDJsX021E9zNkmZTvb/TKJE5gSrZmlU2mOjaCOIs4G7gjlL+Q6rZw1nAfFXbaX8OoMxSfRs4SNIjkjYCVqbauGEWMJPqPaIzG8UaEREREdFKZGfFUjNIOgjosP3ZZsfSTCuMGOVXHnug2WFERERExDJM0nTbHY3q2nWGKFrEpmsN67lRREREREQ/WVY3VWh5ticBk5ocRkREREREW8sMUUREREREtK0kRBERERER0baSEEVERERERNtKQhQREREREW0rCVFERERERLStJEQREREREdG2khBFRERERETbSkIUTTX7z882O4SIiIiIaGMtmRBJWk3S4c2OY1FIOkrSW/qwv/+q+3zTEvTVp7HV9Hu8pB0blI+X9Mu+Hi8iIiIioq+1ZEIErAYsVQkRcBTQMOmQNGgx+ntDQmR7m8Xoo8tRdBPbkrD9Fdu/7et+IyIiIiIGSqsmRCcC60uaIekiSbt3VUi6QNJukg6SdLmk30i6T9JXa9p8QtJt5fwfdiUkkj4s6Q5JMyVdW8pWl3SZpFmSbpG0WSk/TtLZkqZKelDSkaV8ZUm/Kn3MkbRPqVsTmCJpSmk3r8yg3ApsLelhSWuUug5JU8vxUEnnSJpdYthT0onASiX+C7r6Kz8l6eQy9mxJ+5Ty8SXWiyXdW+6T6mOT9ClJp9bcq89I+rakkeW8c0scF3fNKkkaJ+k6SdMlTZY0opRPkrRXzb29V9INwMf79E9DREREREQ/adWE6Bjg/2yPAc4ADgaQNAzYBvh1abcVcAAwBti7JBrvBvYBti3nLwAOkDQc+BGwp+3Ngb1LH18D7rS9GdWszHk1cYwGPlTG+aqkIcCHgUdtb257E+A3tk8HHgUm2J5Qzl0ZmGP7PbZvWMi1fhl41vamJYbf2T4GeMn2GNsH1LX/eLnezYEdgZO7EhRgLNVs0EbAeuUe1Mf2M2C3ci2Ue3tOOd4QmFjieA44vLT7LrCX7XHA2cDXawOStGK5tx8Ftgf+cSHXGxERERHRMlo1Ifob29cB75L0D8B+wCW255fqa2w/afsl4FJgO+ADwDjgdkkzyuf1gPcC02w/VPp9qvSxHXB+Kfsd8LaSeAH8yvYrtp8AHgfeDswGdpR0kqTtbXe3K8AC4JJeXOKOwPdqrvfpHtpvB1xoe4HtvwLXAVuWuttsP2L7dWAGMLL+ZNsvAL8DPiJpNDDE9uxS/SfbN5bjn5SxNgQ2Aa4p9/NLwNp13Y4GHrL9gG2Xc7sl6VBJnZI6F7yYTRUiIiIionkGNzuAXjqfaiZoX+CQmnLXtTMg4FzbX6ytkLRbg/aU9vW62r1SU7YAGGz7fknjgF2Ab0q62vbxDfp42faCms/z+XsCumLd+I3i6k6jeLu8Kd5u2p1FNRt2L3+fHaJBHF338y7bW/cQV6+vwfZEYCLACiNGLcq1R0RERET0qVadIXoeWKXm8ySqpWDYvqum/IPlHaCVgI8BNwLXAnuVGaWud4TWAW4G3idp3a7y0sc0qmQLSeOBJ2w/111gktYEXrT9E+AUYItuYq73MNXMFcCeNeVXA5+t6f+t5fC1mmVttaYB+0gaVJYB7gDctpBx3xSb7VuBdwD7AxfWtHunpK7EZz/gBuA+YHhXuaQhkjau6/9eYF1J69ecGxERERHR8loyIbL9JHBj2Tjg5LI07B7eOJsB1Rf286mWh11iu9P23VTLuq6WNAu4Bhhhey5wKHCppJnAz0sfxwEdpe2JwCd7CG9T4LayfOxY4L9L+UTgqq5NFRr4GnCapOupZm+6/Dfw1nKtM4Gud5AmArO6NlWo8QtgFjCTaunb523/pYeYG8X2v8CNdUv07gE+We7F6sAPbL8K7AWcVOKbQfUe19/Yfpnq3v6qbKrwhx7iiYiIiIhoCape+WhtZbez2cAWXe/sSDoI6LD92YWdG42p+j1Bp9ru2m1vJPDLslHEgFlhxCi/8tgDAzlkRERERLQZSdNtdzSqa8kZolqqfvHnvcB3F7KBQfSSql96ez/VLnbXNjueTdca1nOjiIiIiIh+slTMEMWyq6Ojw52dnc0OIyIiIiKWYUv1DFFERERERER/SUIUERERERFtKwlRRERERES0rSREERERERHRtpIQRURERERE20pCFBERERERbSsJUUREREREtK0kRNFUs/+c37UbEREREc2ThCgiIiIiItrWMpEQSZokaa8G5WtKurgcj5G0yxKMcZSktyxJnHX9jZf0y4XUj5S0fy/6OUjSGb0Ya5vFibOHfjsknd5N3cOS1ujrMSMiIiIi+tJSlRBJGrQo7W0/arsrURoDLHZCBBwFLFJCJGnwEow3EugxIeql8UCfJ0S2O20f2df9RkREREQMlJZKiCRdJmm6pLskHVrK5kk6XtKtwNaSDpQ0S9JMSefXnL6DpJskPdg1W1RmWeZIWh44HthH0gxJ+0haWdLZkm6XdKek3cs5gySdIml2GecISUcCawJTJE3piqsm7r0kTSrHkyR9u7Q7SdJWJa47y88NG1z3+0pcM0q7VYATge1L2eckXS9pTM05N0rarK6f4ZIuKdd0u6RtJY0EDgM+V/raXtJDkoaUc1YtszlDJE2V9J0S5xxJW5U23d2rv81ySXqbpKtL/Q8BLdYfgoiIiIiIAbQkMxj94RDbT0laCbhd0iXAysAc21+RtDFwLLCt7SckrV5z7ghgO2A0cAVwcVeF7VclfQXosP1ZAEnfAH5n+xBJqwG3SfotcCCwLjDW9nxJq5eY/gOYYPuJXlzHBsCOthdIWhXYofS1I/ANYM+69kcD/2b7RklDgZeBY4CjbX+kxPsUcBBwlKQNgBVsz5K0RU0/pwGn2r5B0juBybbfLelMYJ7tU0pfU4FdgcuAfYFLbL8mCWBl29tI2gE4G9ik3PNG96rWV4EbbB8vaVfg0O5uTkl2DwUYtOrwXtzOiIiIiIj+0WoJ0ZGS9ijH7wBGAQuAS0rZ+4GLu5IS20/VnHuZ7deBuyW9vRdj7QTsJuno8nlF4J3AjsCZtuc3GKO3LrK9oBwPA86VNAowMKRB+xuBb0u6ALjU9iMlOXlDn8CXJf0/4BBgUoN+dgQ2qjl31TLbVO8s4PNUCdHBwGdq6i4EsD2tzB6tRvf3qtYOwMfLub+S9HSDcSn1E4GJACuMGOXu2kVERERE9LeWSYgkjaf6Qr+17RfLLMaKwMs1yYWokopGXqntrjdDAnvavq8ujoWNUau2zYp1dS/UHJ8ATLG9R1m+NvVNHdknSvoV1TtOt5SZpPo2L0q6Btgd+Cego0FMy1Hdv5dqC+uTqzITNVLS+4BBtud0c11dn7u7V/WJZ5KbiIiIiFiqtNI7RMOAp8sX/9HAexu0uRb4J0lvA6hbMteT54Ha2ZLJwBElAULS2FJ+NXCYyoYINWPUn/9XSe+WtBywB90bBvy5HB/UqIGk9W3Ptn0S0Em17K9+PKhmdk4Hbu9m5upq4LM1/Y7pJnaA86hmg86pK9+nnLsd8KztZ+n+XtWaBhxQ6ncG3troWiMiIiIiWkkrJUS/AQZLmkU1q3JLfQPbdwFfB66TNBP49iL0P4VqOdkMSfuUMYYAsyTNKZ+hSjr+WMpn8ved3iYCV5XNEqB6x+eXwO+AxxYy7v8A35R0I9DdLnlHlU0MZgIvAVcBs4D5qjaP+Fy5/unAc7w5ielyJNChajOIu6k2UwC4Etija1OFUnYBVdJyYV0fT0u6CTgT+FQp6+5e1foa1cYWd1AtsftjNzFGRERERLQM2VnltLSQtCbVkrvR5X2pJelrL2B32/9cUzaVaiOHziXpe1F0dHS4s3PAhouIiIiINiRpuu1Gr5y0zjtEsXCSDqSaHfuPPkiGvgvszJL9XqaIiIiIiKVeEqKlhO3zqN776Yu+juimfHxf9B8RERERsbRopXeIIiIiIiIiBlQSooiIiIiIaFtJiCIiIiIiom0lIYqIiIiIiLaVhCgiIiIiItpWEqKIiIiIiGhbSYiiqWb/+dlmhxARERERbSwJUUREREREtK0kRANM0pGS7pH0tKRj+qjP8ZJ+2Rd91fQ5WtIMSXdKWl/SvL7sPyIiIiKiFQxudgBt6HBgZ9sPNaqUNNj2/AGOqZGPAZfb/iqApOZGExERERHRDzJDNIAknQmsB1wh6XOSzijlkyR9W9IU4KQyI/MbSdMlXS9pdE27M0vZ/ZI+0mCMrSTdVGZ2bpK0YSkfJOkUSbMlzZJ0RCkfJ+m6MtZkSSMk7QIcBXy6xFTb/1BJ10q6o/S1e03dlyXdK+kaSRdKOrp/7mRERERERN/IDNEAsn2YpA8DE4D6ZGYDYEfbCyRdCxxm+wFJ7wG+D7y/tBsJvA9YH5gi6V11/dwL7GB7vqQdgW8AewKHAusCY0vd6pKGAN8Fdrc9V9I+wNdtH1KSt3m2T6nr/2VgD9vPSVoDuEXSFcC4Ms5Yqj9XdwDTG90HSYeWeBi06vBe3buIiIiIiP6QhKh1XFSSoaHANsBFNcvUVqhp97+2XwcekPQgMLqun2HAuZJGAQaGlPIdgTO7luPZfkrSJsAmwDVlrEHAYz3EKeAbknYAXgfWAt4ObEe1xO4lAElXdteB7YnARIAVRoxyD+NFRERERPSbJESt44XyczngGdtjumlXn0DUfz4BmGJ7D0kjgamlXA3aCrjL9taLEOcBwHBgnO3XJD0MrFj6ioiIiIhYquQdohZj+zngIUl7A6iyeU2TvSUtJ2l9qveR7qvrYhjw53J8UE351cBhkgaXflcv5w6XtHUpGyJp4x5CHAY8XpKhCcA6pfwG4KOSViyzXLv2/qojIiIiIpojCVFrOgD4lKSZwF3A7jV19wHXAVdRvWf0ct25/wN8U9KNVEvgupwF/BGYVfrd3/arwF5UGznMBGZQLddbmAuADkmdJc57AWzfDlwBzAQuBTqB/NbViIiIiGhpsvMKx9JC0iTgl7YvbnYsjUgaanuepLcA04BDbd+xsHM6Ojrc2dk5MAFGRERERFuSNN12R6O6vEMUfWmipI2o3ik6t6dkKCIiIiKi2ZIQLUVsH9TsGBbG9v7NjiEiIiIiYlHkHaKIiIiIiGhbSYgiIiIiIqJtJSGKiIiIiIi2lYQoIiIiIiLaVhKiiIiIiIhoW0mIIiIiIiKibSUhioiIiIiItpWEKCIiIiIi2lYSosUg6ThJR5fjqZI6FvH8m8rPkZLmdNPmb/1K+rWk1ZYw7N7GNlrSDEl3Slpf0v41dR+UNF3S7PLz/TV1y0uaKOl+SfdK2nMg4o2IiIiIWBJJiHqgSp/eJ9vbLGL7XWw/05cxLMTHgMttjwXeAexfU/cE8FHbmwKfBM6vqTsWeNz2BsBGwHUDE25ERERExOIb3OwABoqkE4AnbJ9WPn8d+CuwO/BWYAjwJduXSxoJXAVMAbYGPibpE8CBwJ+AucD0mu4/Iel0YFXgENu3SToOmGf7lDLeHOAjth+WNM/20Lr4VgLOoUom7gFWqql7GOgAhpa4bgC2Af4M7G77JUlbAj8GXij1O9veRNLGpd/lqRLgPW0/IOnYBtdzN3AUsEDSDiWGd0uaAZxr+9SakO8CVpS0gu1XgEOA0QC2X6dKniIiIiIiWlo7zRD9mGpWgzLjsy/wc2AP21sAE4BvSVJpvyFwXpkpWaO0Hwt8HNiyru+Vy6zP4cDZixnfvwIv2t4M+Dowrpt2o4Dv2d4YeAboWpp2DnCY7a2BBTXtDwNOsz2GKql6RNK4Rtdj+9fAmcCpticAxwDX2x5TlwxRxr3T9is1y/lOkHSHpIskvb27C5V0qKROSZ1z585d+F2JiIiIiOhHbZMQ2X4YeFLSWGAn4E7gKeAbkmYBvwXWArq+yP/B9i3leHvgF7ZftP0ccEVd9xeWMaYBqy7m+z47AD8p/cwCZnXT7iHbM8rxdGBkGW8V2zeV8p/WtL8Z+C9JXwDWsf1SL65nocqs00nAv5SiwcDawI0lubwZOKW7821PtN1hu2P48OGLMnRERERERJ9qm4SoOAs4CDiYaibnAGA4MK7MoPwVWLG0faHuXC+k3/o6A/N54/1dkZ4tbIwur9QcL6BKRtRNW2z/FNgNeAmYXLMRQm/GehNJawO/AA60/X+l+EngxVIOcBGwxeL0HxERERExkNotIfoF8GGqJWKTgWFUGwG8JmkCsE43500D9pC0kqRVgI/W1e8DIGk74FnbzwIPU5ICSVsA6/YQ2zSqBA1JmwCb9faibD8NPC/pvaVo3646SesBD9o+nWomaLNeXE+X54FVavpaDfgV8EXbN9aMb+BKYHwp+gDV+0gRERERES2tbTZVALD9qqQpwDO2F0i6ALhSUicwA7i3m/PukPTz0uYPwPV1TZ4uW2mvSrW5AMAlwIFlQ4Lbgft7CO8HwDll+d4M4LZFuzo+BfxI0gvAVODZUr4P1aYPrwF/AY63/VQP19NlFjBf0kxgErAy8C7gy5K+XNrsZPtx4AvA+ZK+Q7VJw8GLGH9ERERExIBT9Y/77aFspnAHsLftB5odT1+SNNT2vHJ8DDDC9r/38tzjqNkRbyB1dHS4s7NzoIeNiIiIiDYiabrthr87tG2WzEnaCPg9cO2ylgwVu5ZfqDqHatOE/252QBERERERra5tlszZvhtYr9lx9BfbP6faRnxxzj2ub6OJiIiIiFg6tM0MUURERERERL0kRBERERER0baSEEVERERERNtKQhQREREREW0rCVFERERERLStJEQREREREdG2khBFRERERETbSkIUERERERFtKwlRP5A0UtKcZsexKErM+y/B+aMlzZB0p6T1l6SviIiIiIiBkoQouowEliSJ+Rhwue2xwDuWsK+IiIiIiAExuNkBLMMGSzoXGAvcDxwIHA18FFgJuAn4F9uWdCRwGDAfuNv2vpJWBr4LbEr1nI6zfbmkg6iSj0HAJsC3gOWBfwZeAXax/ZSk9YHvAcOBF4HP2L5X0iTgOaAD+Efg87YvBk4E3i1pBnAucDVwTul7OWBP2w9IOrZcy5+AucB04G7gKGCBpB3K9f2tL9un9uF9jYiIiIjoM5kh6j8bAhNtb0aVgBwOnGF7S9ubUCUNHyltjwHGlraHlbJjgd/Z3hKYAJxckiSoEqH9ga2ArwMvlpmZm6mSFYCJwBG2x1ElYt+viW0EsF0Z/8SaGK63PaYkMIcBp9keQ5U8PSJpHLAvVZL3cWBLANu/Bs4ETrU9oUFfbyDpUEmdkjrnzp27CLc0IiIiIqJvZYao//zJ9o3l+CfAkcBDkj4PvAVYHbgLuBKYBVwg6TLgsnLOTsBuko4un1cE3lmOp9h+Hnhe0rOlD4DZwGaShgLbABdJ6opnhZrYLrP9OnC3pLd3E//NwLGS1gYuLbND2wO/sP0igKQrFumOFLYnUiVsdHR0eHH6iIiIiIjoC0mI+k/9F31TzdJ02P6TpOOokhyAXYEdgN2AL0vaGBDVMrX7ajuR9B6qpXFdXq/5/DrVM10OeKbM7jRSe74aNbD9U0m3ltgmS/p0N9cVEREREbHUypK5/vNOSVuX4/2AG8rxE2UGZy8AScsB77A9Bfg8sBowFJgMHKEyxSNpbG8Htv0c1WzU3uVcSdq8h9OeB1bp+iBpPeBB26cDVwCbAdOAPSStJGkVqveheuwrIiIiIqJVZYao/9wDfFLSD4EHgB8Ab6Va1vYwcHtpNwj4iaRhVLM1p9p+RtIJwHeAWSUpepi/v3PUGwcAP5D0JWAI8DNg5kLazwLmS5oJTKKavfqEpNeAvwDHl80afg7MAP4AXN+bvrKpQkRERES0KtlZARWLpyz7m2f7lMXto6Ojw52dnX0XVEREREREHUnTbXc0qsuSuYiIiIiIaFtZMheLzfZxzY4hIiIiImJJZIYoIiIiIiLaVhKiiIiIiIhoW0mIIiIiIiKibSUhioiIiIiItpWEKCIiIiIi2lYSooiIiIiIaFtJiCIiIiIiom0lIYqIiIiIiLaVhKhFSTpK0lv6sL//6qu+6vo9S9JGDcoPknRGf4wZEREREdFXkhC1rqOARUqIJA1aSHW/JES2P2377v7oOyIiIiKivw1udgABklYG/hdYGxgEXASsCUyR9ITtCZL2o0pqBPzK9hfKufOAbwMfAn4taYztPUrdB4F/Be4HVpI0A7gLeBB4wvZppd3Xgb8Cs4DjgSeBDYFpwOG2X5e0E/A1YAXg/4CDbc+TNBU42nanpIOBLwKPlTFf6a97FhERERHRFzJD1Bo+DDxqe3PbmwDfAR4FJpRkaE3gJOD9wBhgS0kfK+euDMyx/R6qZObdkoaXuoOBc2wfA7xke4ztA4AfA58EkLQcsC9wQTlnK+A/gU2B9YGPS1oD+BKwo+0tgE7gP2ovQNIIqoRpW+CDwJuW0dW0PVRSp6TOuXPnLvLNioiIiIjoK0mIWsNsYEdJJ0na3vazdfVbAlNtz7U9nyp52aHULQAuAbBt4HzgE5JWA7YGrqofzPbDwJOSxgI7AXfafrJU32b7QdsLgAuB7YD3UiU4N5ZZpk8C69R1+56aGF8Fft7dxdqeaLvDdsfw4cO7axYRERER0e+yZK4F2L5f0jhgF+Cbkq6ua6KFnP5ySV66nANcCbwMXFQSqEbOAg4C/hE4uzac+vDK+NfY3m+hF/LmcyMiIiIiWlpmiFpAWRL3ou2fAKcAWwDPA6uUJrcC75O0Rtk4YT/gukZ92X6Uarndl4BJNVWvSRpS8/kXVEv1tgQm15RvJWndspRuH+AG4BZgW0nvKvG+RdIGdUPfCoyX9LYyzt6Lcg8iIiIiIpohM0StYVPgZEmvA69RbYSwNXCVpMfKe0RfBKZQzdb82vblC+nvAmB43e5vE4FZku6wfYDtVyVNAZ6pm2G6GTixxDQN+EXZVOEg4EJJK5R2X6LaOAEA249JOq6c/xhwB9UGERERERERLUvVayexLCm//+dO2z9eSJvlqJKWvW0/UMrGU+0Y95GBiBOgo6PDnZ2dAzVcRERERLQhSdNtdzSqy5K5ZYyk6cBmwE8W0mYj4PfAtV3JUEREREREO8qSuWWM7XG9aHM3sF6D8qnA1L6PKiIiIiKiNWWGKCIiIiIi2lYSooiIiIiIaFvZVCGaStLzwH3NjiOaZg3giWYHEU2RZ9/e8vzbW55/e2vW81/H9vBGFXmHKJrtvu52/Ihln6TOPP/2lGff3vL821uef3trxeefJXMREREREdG2khBFRERERETbSkIUzTax2QFEU+X5t688+/aW59/e8vzbW8s9/2yqEBERERERbSszRBERERER0baSEEW/k/RhSfdJ+r2kYxrUS9LppX6WpC2aEWf0j148/wPKc58l6SZJmzcjzugfPT3/mnZbSlogaa+BjC/6V2+ev6TxkmZIukvSdQMdY/SfXvz/f5ikKyXNLM//4GbEGX1P0tmSHpc0p5v6lvrul4Qo+pWkQcD3gJ2BjYD9JG1U12xnYFT571DgBwMaZPSbXj7/h4D32d4MOIEWXFsci6eXz7+r3UnA5IGNMPpTb56/pNWA7wO72d4Y2Hug44z+0cu///8G3G17c2A88C1Jyw9ooNFfJgEfXkh9S333S0IU/W0r4Pe2H7T9KvAzYPe6NrsD57lyC7CapBEDHWj0ix6fv+2bbD9dPt4CrD3AMUb/6c3ff4AjgEuAxwcyuOh3vXn++wOX2v4jgO38GVh29Ob5G1hFkoChwFPA/IENM/qD7WlUz7M7LfXdLwlR9Le1gD/VfH6klC1qm1g6Leqz/RRwVb9GFAOpx+cvaS1gD+DMAYwrBkZv/v5vALxV0lRJ0yUdOGDRRX/rzfM/A3g38CgwG/h3268PTHjRZC313W9wswaOtqEGZfVbG/amTSydev1sJU2gSoi269eIYiD15vl/B/iC7QXVPxLHMqQ3z38wMA74ALAScLOkW2zf39/BRb/rzfP/EDADeD+wPnCNpOttP9fPsUXztdR3vyRE0d8eAd5R83ltqn8JWtQ2sXTq1bOVtBlwFrCz7ScHKLbof715/h3Az0oytAawi6T5ti8bkAijP/X2//9P2H4BeEHSNGBzIAnR0q83z/9g4ERXvwPm95IeAkYDtw1MiNFELfXdL0vmor/dDoyStG55UXJf4Iq6NlcAB5YdR94LPGv7sYEONPpFj89f0juBS4F/zr8KL3N6fP6217U90vZI4GLg8CRDy4ze/P//cmB7SYMlvQV4D3DPAMcZ/aM3z/+PVLODSHo7sCHw4IBGGc3SUt/9MkMU/cr2fEmfpdo9ahBwtu27JB1W6s8Efg3sAvweeJHqX4xiGdDL5/8V4G3A98sswXzbHc2KOfpOL59/LKN68/xt3yPpN8As4HXgLNsNt+mNpUsv//6fAEySNJtqCdUXbD/RtKCjz0i6kGrnwDUkPQJ8FRgCrfndT9UsZURERERERPvJkrmIiIiIiGhbSYgiIiIiIqJtJSGKiIiIiIi2lYQoIiIiIiLaVhKiiIiIiIhoW0mIIiIiIiKibSUhioiIiIiItpWEKCIiIiIi2tb/Bxb6GLBtuk3/AAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 864x1440 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Find available information per feature\n",
    "plt.figure(figsize=(12,20))\n",
    "df_eda.notnull().mean().sort_values(ascending = True).plot(kind = 'barh')\n",
    "plt.title('Available Information by Feature')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "147e9911-e6cf-4ae0-a905-327fde1f6b79",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "propertylandusetypeid           5956065\n",
       "fips                            5956065\n",
       "rawcensustractandblock          5956065\n",
       "regionidcounty                  5956065\n",
       "longitude                       5956065\n",
       "latitude                        5956065\n",
       "assessmentyear                  5956062\n",
       "bedroomcnt                      5956039\n",
       "bathroomcnt                     5956015\n",
       "roomcnt                         5955990\n",
       "propertycountylandusecode       5955158\n",
       "regionidzip                     5943740\n",
       "taxamount                       5916432\n",
       "taxvaluedollarcnt               5893618\n",
       "calculatedfinishedsquarefeet    5869772\n",
       "structuretaxvaluedollarcnt      5868988\n",
       "yearbuilt                       5862673\n",
       "regionidcity                    5845461\n",
       "landtaxvaluedollarcnt           5842775\n",
       "censustractandblock             5820323\n",
       "calculatedbathnbr               5724366\n",
       "fullbathcnt                     5724366\n",
       "finishedsquarefeet12            5429970\n",
       "lotsizesquarefeet               5421629\n",
       "propertyzoningdesc              3961100\n",
       "unitcnt                         3958532\n",
       "buildingqualitytypeid           3879883\n",
       "heatingorsystemtypeid           3675565\n",
       "regionidneighborhood            2313143\n",
       "garagecarcnt                    1774275\n",
       "garagetotalsqft                 1774275\n",
       "airconditioningtypeid           1626881\n",
       "numberofstories                 1367745\n",
       "poolcnt                         1057166\n",
       "pooltypeid7                      991354\n",
       "threequarterbathnbr              627988\n",
       "fireplacecnt                     625761\n",
       "finishedfloor1squarefeet         406475\n",
       "finishedsquarefeet50             406475\n",
       "finishedsquarefeet15             380983\n",
       "yardbuildingsqft17               161943\n",
       "hashottuborspa                   119076\n",
       "taxdelinquencyyear               112981\n",
       "taxdelinquencyflag               112977\n",
       "pooltypeid2                       65131\n",
       "poolsizesum                       55918\n",
       "pooltypeid10                      53945\n",
       "finishedsquarefeet6               43483\n",
       "decktypeid                        34475\n",
       "buildingclasstypeid               25360\n",
       "finishedsquarefeet13              15343\n",
       "typeconstructiontypeid            13493\n",
       "architecturalstyletypeid          12122\n",
       "fireplaceflag                     10326\n",
       "yardbuildingsqft26                 5293\n",
       "basementsqft                       3255\n",
       "storytypeid                        3247\n",
       "dtype: int64"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Missing information in total values\n",
    "df_eda.notnull().sum().sort_values(ascending=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "71c50d88-ae92-406c-a802-241e0d0604cf",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Total # of variables with missing value in the dataset:  169959002\n"
     ]
    }
   ],
   "source": [
    "# Total # of missing values\n",
    "print(\"Total # of variables with missing value in the dataset: \", df_eda.isnull().sum().sum())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "fdc38111-ff20-4d18-938d-5199cf0aa688",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Total # of numerical variables in the dataset:  52\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>airconditioningtypeid</th>\n",
       "      <th>architecturalstyletypeid</th>\n",
       "      <th>basementsqft</th>\n",
       "      <th>bathroomcnt</th>\n",
       "      <th>bedroomcnt</th>\n",
       "      <th>buildingclasstypeid</th>\n",
       "      <th>buildingqualitytypeid</th>\n",
       "      <th>calculatedbathnbr</th>\n",
       "      <th>decktypeid</th>\n",
       "      <th>finishedfloor1squarefeet</th>\n",
       "      <th>...</th>\n",
       "      <th>yardbuildingsqft26</th>\n",
       "      <th>yearbuilt</th>\n",
       "      <th>numberofstories</th>\n",
       "      <th>structuretaxvaluedollarcnt</th>\n",
       "      <th>taxvaluedollarcnt</th>\n",
       "      <th>assessmentyear</th>\n",
       "      <th>landtaxvaluedollarcnt</th>\n",
       "      <th>taxamount</th>\n",
       "      <th>taxdelinquencyyear</th>\n",
       "      <th>censustractandblock</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>parcelid</th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>10754147</th>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>...</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>9.0</td>\n",
       "      <td>2015.0</td>\n",
       "      <td>9.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10759547</th>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>...</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>27516.0</td>\n",
       "      <td>2015.0</td>\n",
       "      <td>27516.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10843547</th>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>...</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>650756.0</td>\n",
       "      <td>1413387.0</td>\n",
       "      <td>2015.0</td>\n",
       "      <td>762631.0</td>\n",
       "      <td>20800.37</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10859147</th>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>3.0</td>\n",
       "      <td>7.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>...</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1948.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>571346.0</td>\n",
       "      <td>1156834.0</td>\n",
       "      <td>2015.0</td>\n",
       "      <td>585488.0</td>\n",
       "      <td>14557.57</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10879947</th>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>4.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>...</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1947.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>193796.0</td>\n",
       "      <td>433491.0</td>\n",
       "      <td>2015.0</td>\n",
       "      <td>239695.0</td>\n",
       "      <td>5725.17</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "<p>5 rows × 52 columns</p>\n",
       "</div>"
      ],
      "text/plain": [
       "          airconditioningtypeid  architecturalstyletypeid  basementsqft  \\\n",
       "parcelid                                                                  \n",
       "10754147                    NaN                       NaN           NaN   \n",
       "10759547                    NaN                       NaN           NaN   \n",
       "10843547                    NaN                       NaN           NaN   \n",
       "10859147                    NaN                       NaN           NaN   \n",
       "10879947                    NaN                       NaN           NaN   \n",
       "\n",
       "          bathroomcnt  bedroomcnt  buildingclasstypeid  buildingqualitytypeid  \\\n",
       "parcelid                                                                        \n",
       "10754147          0.0         0.0                  NaN                    NaN   \n",
       "10759547          0.0         0.0                  NaN                    NaN   \n",
       "10843547          0.0         0.0                  NaN                    NaN   \n",
       "10859147          0.0         0.0                  3.0                    7.0   \n",
       "10879947          0.0         0.0                  4.0                    NaN   \n",
       "\n",
       "          calculatedbathnbr  decktypeid  finishedfloor1squarefeet  ...  \\\n",
       "parcelid                                                           ...   \n",
       "10754147                NaN         NaN                       NaN  ...   \n",
       "10759547                NaN         NaN                       NaN  ...   \n",
       "10843547                NaN         NaN                       NaN  ...   \n",
       "10859147                NaN         NaN                       NaN  ...   \n",
       "10879947                NaN         NaN                       NaN  ...   \n",
       "\n",
       "          yardbuildingsqft26  yearbuilt  numberofstories  \\\n",
       "parcelid                                                   \n",
       "10754147                 NaN        NaN              NaN   \n",
       "10759547                 NaN        NaN              NaN   \n",
       "10843547                 NaN        NaN              NaN   \n",
       "10859147                 NaN     1948.0              1.0   \n",
       "10879947                 NaN     1947.0              NaN   \n",
       "\n",
       "          structuretaxvaluedollarcnt  taxvaluedollarcnt  assessmentyear  \\\n",
       "parcelid                                                                  \n",
       "10754147                         NaN                9.0          2015.0   \n",
       "10759547                         NaN            27516.0          2015.0   \n",
       "10843547                    650756.0          1413387.0          2015.0   \n",
       "10859147                    571346.0          1156834.0          2015.0   \n",
       "10879947                    193796.0           433491.0          2015.0   \n",
       "\n",
       "          landtaxvaluedollarcnt  taxamount  taxdelinquencyyear  \\\n",
       "parcelid                                                         \n",
       "10754147                    9.0        NaN                 NaN   \n",
       "10759547                27516.0        NaN                 NaN   \n",
       "10843547               762631.0   20800.37                 NaN   \n",
       "10859147               585488.0   14557.57                 NaN   \n",
       "10879947               239695.0    5725.17                 NaN   \n",
       "\n",
       "          censustractandblock  \n",
       "parcelid                       \n",
       "10754147                  NaN  \n",
       "10759547                  NaN  \n",
       "10843547                  NaN  \n",
       "10859147                  NaN  \n",
       "10879947                  NaN  \n",
       "\n",
       "[5 rows x 52 columns]"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Numerical variables\n",
    "num_vars = [var for var in df_eda.columns if df_eda[var].dtypes!= 'O'] # Non-Object Variables\n",
    "print(\"Total # of numerical variables in the dataset: \", len(num_vars))\n",
    "\n",
    "df_eda[num_vars].head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "8b782185-b658-46d4-ac99-423e917a5c08",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Total # of categorical variables in the dataset:  5\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>hashottuborspa</th>\n",
       "      <th>propertycountylandusecode</th>\n",
       "      <th>propertyzoningdesc</th>\n",
       "      <th>fireplaceflag</th>\n",
       "      <th>taxdelinquencyflag</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>parcelid</th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>10754147</th>\n",
       "      <td>NaN</td>\n",
       "      <td>010D</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10759547</th>\n",
       "      <td>NaN</td>\n",
       "      <td>0109</td>\n",
       "      <td>LCA11*</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10843547</th>\n",
       "      <td>NaN</td>\n",
       "      <td>1200</td>\n",
       "      <td>LAC2</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10859147</th>\n",
       "      <td>NaN</td>\n",
       "      <td>1200</td>\n",
       "      <td>LAC2</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10879947</th>\n",
       "      <td>NaN</td>\n",
       "      <td>1210</td>\n",
       "      <td>LAM1</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "         hashottuborspa propertycountylandusecode propertyzoningdesc  \\\n",
       "parcelid                                                               \n",
       "10754147            NaN                      010D                NaN   \n",
       "10759547            NaN                      0109             LCA11*   \n",
       "10843547            NaN                      1200               LAC2   \n",
       "10859147            NaN                      1200               LAC2   \n",
       "10879947            NaN                      1210               LAM1   \n",
       "\n",
       "         fireplaceflag taxdelinquencyflag  \n",
       "parcelid                                   \n",
       "10754147           NaN                NaN  \n",
       "10759547           NaN                NaN  \n",
       "10843547           NaN                NaN  \n",
       "10859147           NaN                NaN  \n",
       "10879947           NaN                NaN  "
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Categorical variables \n",
    "cat_vars = [var for var in df_eda.columns if df_eda[var].dtypes == 'O'] # Object Variables\n",
    "print(\"Total # of categorical variables in the dataset: \", len(cat_vars))\n",
    "\n",
    "df_eda[cat_vars].head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "6f21062e-0a09-4682-9c11-75ca677987af",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Total # of Temporal variables: 3\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>yearbuilt</th>\n",
       "      <th>assessmentyear</th>\n",
       "      <th>taxdelinquencyyear</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>parcelid</th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>10754147</th>\n",
       "      <td>NaN</td>\n",
       "      <td>2015.0</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10759547</th>\n",
       "      <td>NaN</td>\n",
       "      <td>2015.0</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10843547</th>\n",
       "      <td>NaN</td>\n",
       "      <td>2015.0</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10859147</th>\n",
       "      <td>1948.0</td>\n",
       "      <td>2015.0</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10879947</th>\n",
       "      <td>1947.0</td>\n",
       "      <td>2015.0</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "          yearbuilt  assessmentyear  taxdelinquencyyear\n",
       "parcelid                                               \n",
       "10754147        NaN          2015.0                 NaN\n",
       "10759547        NaN          2015.0                 NaN\n",
       "10843547        NaN          2015.0                 NaN\n",
       "10859147     1948.0          2015.0                 NaN\n",
       "10879947     1947.0          2015.0                 NaN"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Temporal variables \n",
    "year_var = [var for var in num_vars if 'Yr' in var or 'year' in var ]\n",
    "print('Total # of Temporal variables:', len(year_var))\n",
    "\n",
    "df_eda[year_var].head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "155e6250-81f2-4561-b10b-fe670e1300ec",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Total # of discrete variables:  19\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>airconditioningtypeid</th>\n",
       "      <th>architecturalstyletypeid</th>\n",
       "      <th>buildingclasstypeid</th>\n",
       "      <th>buildingqualitytypeid</th>\n",
       "      <th>decktypeid</th>\n",
       "      <th>fips</th>\n",
       "      <th>fireplacecnt</th>\n",
       "      <th>heatingorsystemtypeid</th>\n",
       "      <th>poolcnt</th>\n",
       "      <th>pooltypeid10</th>\n",
       "      <th>pooltypeid2</th>\n",
       "      <th>pooltypeid7</th>\n",
       "      <th>propertylandusetypeid</th>\n",
       "      <th>regionidcounty</th>\n",
       "      <th>storytypeid</th>\n",
       "      <th>threequarterbathnbr</th>\n",
       "      <th>typeconstructiontypeid</th>\n",
       "      <th>numberofstories</th>\n",
       "      <th>assessmentyear</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>parcelid</th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>10754147</th>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>6037.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>269.0</td>\n",
       "      <td>3101.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>2015.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10759547</th>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>6037.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>261.0</td>\n",
       "      <td>3101.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>2015.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10843547</th>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>6037.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>47.0</td>\n",
       "      <td>3101.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>2015.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10859147</th>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>3.0</td>\n",
       "      <td>7.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>6037.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>47.0</td>\n",
       "      <td>3101.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1.0</td>\n",
       "      <td>2015.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10879947</th>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>4.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>6037.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>31.0</td>\n",
       "      <td>3101.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>2015.0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "          airconditioningtypeid  architecturalstyletypeid  \\\n",
       "parcelid                                                    \n",
       "10754147                    NaN                       NaN   \n",
       "10759547                    NaN                       NaN   \n",
       "10843547                    NaN                       NaN   \n",
       "10859147                    NaN                       NaN   \n",
       "10879947                    NaN                       NaN   \n",
       "\n",
       "          buildingclasstypeid  buildingqualitytypeid  decktypeid    fips  \\\n",
       "parcelid                                                                   \n",
       "10754147                  NaN                    NaN         NaN  6037.0   \n",
       "10759547                  NaN                    NaN         NaN  6037.0   \n",
       "10843547                  NaN                    NaN         NaN  6037.0   \n",
       "10859147                  3.0                    7.0         NaN  6037.0   \n",
       "10879947                  4.0                    NaN         NaN  6037.0   \n",
       "\n",
       "          fireplacecnt  heatingorsystemtypeid  poolcnt  pooltypeid10  \\\n",
       "parcelid                                                               \n",
       "10754147           NaN                    NaN      NaN           NaN   \n",
       "10759547           NaN                    NaN      NaN           NaN   \n",
       "10843547           NaN                    NaN      NaN           NaN   \n",
       "10859147           NaN                    NaN      NaN           NaN   \n",
       "10879947           NaN                    NaN      NaN           NaN   \n",
       "\n",
       "          pooltypeid2  pooltypeid7  propertylandusetypeid  regionidcounty  \\\n",
       "parcelid                                                                    \n",
       "10754147          NaN          NaN                  269.0          3101.0   \n",
       "10759547          NaN          NaN                  261.0          3101.0   \n",
       "10843547          NaN          NaN                   47.0          3101.0   \n",
       "10859147          NaN          NaN                   47.0          3101.0   \n",
       "10879947          NaN          NaN                   31.0          3101.0   \n",
       "\n",
       "          storytypeid  threequarterbathnbr  typeconstructiontypeid  \\\n",
       "parcelid                                                             \n",
       "10754147          NaN                  NaN                     NaN   \n",
       "10759547          NaN                  NaN                     NaN   \n",
       "10843547          NaN                  NaN                     NaN   \n",
       "10859147          NaN                  NaN                     NaN   \n",
       "10879947          NaN                  NaN                     NaN   \n",
       "\n",
       "          numberofstories  assessmentyear  \n",
       "parcelid                                   \n",
       "10754147              NaN          2015.0  \n",
       "10759547              NaN          2015.0  \n",
       "10843547              NaN          2015.0  \n",
       "10859147              1.0          2015.0  \n",
       "10879947              NaN          2015.0  "
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Discrete Variables\n",
    "discrete_vars = [var for var in num_vars if len(df_eda[var].unique()) < 20]\n",
    "print('Total # of discrete variables: ', len(discrete_vars))\n",
    "\n",
    "df_eda[discrete_vars].head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "f95205a4-073a-4871-866d-e32828290f5e",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[<AxesSubplot:title={'center':'airconditioningtypeid'}>,\n",
       "        <AxesSubplot:title={'center':'architecturalstyletypeid'}>,\n",
       "        <AxesSubplot:title={'center':'buildingclasstypeid'}>,\n",
       "        <AxesSubplot:title={'center':'buildingqualitytypeid'}>],\n",
       "       [<AxesSubplot:title={'center':'decktypeid'}>,\n",
       "        <AxesSubplot:title={'center':'fips'}>,\n",
       "        <AxesSubplot:title={'center':'fireplacecnt'}>,\n",
       "        <AxesSubplot:title={'center':'heatingorsystemtypeid'}>],\n",
       "       [<AxesSubplot:title={'center':'poolcnt'}>,\n",
       "        <AxesSubplot:title={'center':'pooltypeid10'}>,\n",
       "        <AxesSubplot:title={'center':'pooltypeid2'}>,\n",
       "        <AxesSubplot:title={'center':'pooltypeid7'}>],\n",
       "       [<AxesSubplot:title={'center':'propertylandusetypeid'}>,\n",
       "        <AxesSubplot:title={'center':'regionidcounty'}>,\n",
       "        <AxesSubplot:title={'center':'storytypeid'}>,\n",
       "        <AxesSubplot:title={'center':'threequarterbathnbr'}>],\n",
       "       [<AxesSubplot:title={'center':'typeconstructiontypeid'}>,\n",
       "        <AxesSubplot:title={'center':'numberofstories'}>,\n",
       "        <AxesSubplot:title={'center':'assessmentyear'}>, <AxesSubplot:>]],\n",
       "      dtype=object)"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAABsMAAAaOCAYAAAAH3ENhAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAEAAElEQVR4nOzde5glVX3v//dHRhEviIpOcECHxFHDRYhMkMQkdkISRzRBE01QFDAkIx5M9HcmiWAumhgSPQnRGBUOUQJ4Q8QbEVCJno4mAooG5SZxlFEGEFQQGS/o4Pf3R1XLnmZ3T09370tXv1/Ps5/ee9Wqqu+qvXuvXfWtWpWqQpIkSZIkSZIkSeqie406AEmSJEmSJEmSJGlQTIZJkiRJkiRJkiSps0yGSZIkSZIkSZIkqbNMhkmSJEmSJEmSJKmzTIZJkiRJkiRJkiSps0yGSZIkSZIkSZIkqbNMhmnkkhyZ5CNjEMcZSf6mff6LSa6dpe4jk2xJstMirPeqJBMLXc4wbe89SzKZ5PeHGZMkLSVJViepJCtmmP7yJG8edlyLobc/XeTlVpJHL/ZyF8L+UJK2lWRTkl+dx3ynJvmL9vlEks2z1J3zftti2F6fvZT0bucZpo9dXytJo9bFvm0x9G6XpbL/6v6bTIZp5Krq7VX166OOo1dVfaKqHjv1enrHV1VfraoHVNVdi7CufatqcqHL2V7HupjG8T2TpC6pqr+tqt+HxT0Il+SVSd628AgXbr47lXNYrv2hJC0xVXVcVb1qHvNts9+2FA2zb57vdpYk7bjl1LctZP/V/TcNk8kwjbUunH0nSVqeutiHdbFNkiRJkiSp+0yGaWiSnJDkS0nuSHJ1kme25cck+c+eepXk+CRfBL7Ylh2e5PIk326Xsa4tf0SS85LcmmRjkj/oWc4rk5yT5Kx2nVclWdsz/WeSfLad9i7gvj3TfnxWQpK3Ao8E/q0dGvFPp5/lsMA4ei8r3l7dJyT573bau5O8K8nfJLk/cCHwiDbGLW1M303y0J75D0ry9ST3brf7fyX55yS3J/lCkkN76j4oyVuS3JTkhnY9O83wnv1aO//tSd4AZIc/IJK0RGynP/uvJK9NcivwyiS7JDk5yVfa78j/TLJLz+KOTPLVJN9I8mc96+g9S/zj7d9vtd/vP9fW+b0k1yS5LcmHkzyqZ/59k1zU9ks3pxm2Yh3wcuB32+V8rq27zRVavevu6e+OTfJV4GNt+buTfK1t08eT7DvDtto9yQeTfKuN5RNJ7jVD33p+kj+cNv/nkzyjz3J3TvIP7ba7Oc0QJLvYH0rSWPjZtn+8Lcm/Jrnv9O9L2HZIvswyxG7muN/Wvt6U5I/b/uP2NPtLvfX/tP0+vzHJ70+LYXt99tQyXtD2v3ck+XKSF/ZM69vvtdNe1vYjdyS5Nsmh/frmJM9O8plp69yQ5P092+rUNP38HUn+I9v+Bnhc7v4NcG2S3+mZts12TvInPdvj92Z6QyVJS7Zve2ia45XfTvKpJK+aijl9ruBKzzCBSX4qyceSfDPN/urbk+w2Q3tm2399ctsn7d9T/+FJvtf2X+6/aWhMhmmYvgT8IvAg4K+AtyXZY4a6zwCeCOyT5GDgLOBPgN2AXwI2tfXeCWwGHgE8C/jb3i9A4DeBs9v5zgPeAJDkPsD7gbcCDwHeDfx2v0Cq6vnAV4HfaIdG/D99qs0rjhnMFvP7gDPamN8JPLON8TvAU4Eb2xgfUFU3ApPA7/Qs+3nA2VX1w/b1E4EvA7sDrwDem+Qh7bQzga3Ao4GfAX4duMe4uUl2B94D/Hm7nC8BT5qlfZK01M3Wn019rz4cOAn4B+Ag4Odpvrv/FPhRz7J+AXgscCjwl0l+us/6fqn9u1v7/X5xmgTRy4HfAh4GfIKmXyDJA4F/Bz5E0y89GvhoVX0I+FvgXe1yDtiBNj8Z+GngKe3rC4E1bTs/C7x9hvk20PSPDwNWtjHXDH3rmTT9FG07DgBWARf0We5rgMcAB7btWwX8pf2hJI2FI2n6i5+i+a7+8/kuaEf223r8DrAO2Bt4PHBMu6x1wP8GfpXmO/3J0+bbXp895Rbg6cCuwAuA1yZ5Qjutb7+X5LHAi4GfraoH0myfTTP0zecBe0/7TfC8dhtMORJ4FU1/czltP5zmpJCLgHfQ9NHPAd6UPiettNvjj4Ffo+nTF33oYknqkKXat70R+D6wB/B77WPOoQJ/R7NP+dPAXsAr5zDf9P3X/6A5zvm8njrPAf69qr6C+28aoiWdDEtyepJbklw5x/q/02bxr0ryjkHHp21V1bur6saq+lFVvYvmqq+DZ6j+d1V1a1V9DzgWOL2qLmrnvaGqvpBkL5qDiC+rqu9X1eXAm4Hn9yznP6vqgvbeXm8Fpg78HQLcG3hdVf2wqs4FPj2fdi0wjn5mi3kF8Po25vcCn9pOeD8+sNieBfEctt2JuoW7t8G7gGuBpyVZSdMZvbSqvlNVtwCvBY7os47DgKur6ty2U3od8LXtxCVJS9Z2+rMbq+qfq2orcCfNzsZL2r7rrqr6ZFXd2bO4v6qq71XV54DPMXv/0OuFNH3lNe26/hY4sD2z7unA16rq5LZfuqOqLl1gs1/Z9gffa7fB6e1y76TZITogyYP6zPdDmh2vR7V9zSeqqmZYxweANUnWtK+fT3Nw8Ae9lZIE+APg/2t/K9xB0/5+fdQU+0NJGp43VNX1VXUrzYkhz1nAsuaz3/b6tp++Ffg3mhMnoDmo9q9VdVVVfZfmhBYA0ly9tb0+G4CqOr+qvlSN/wA+QnOSDMzc790F7Exzsue9q2pTVX2pX/DtOt/F3f3WvsBq4IM91c6vqo+3df8M+Ll2v/TpNEm2f62qrVX1WZoDfc/qs6qp7XFlezLJK2fepJIWm8c0l5yl2LftRJNk+8t2X+ZKmv2iOamqje2x2Dur6uvAP3LPZNtcnQk8t+1vodnXe+t26rv/pkW3pJNhNFfIrJtLxfbAyonAk6pqX+ClgwtL/SQ5Ks1Qh99K8i1gP5rMez/X9zzfiyY7P90jgKmDYFO+QnN2+JTeL7HvAvdtL/99BHDDtANyX5lbSxY1jn52JObrmd0HaHa4fpLmjL/bq6o3gdZvGzwCeBRNx3xTz/v1f2nOLpzuEb1xtMvbXlyStGRtpz/r/f7bnWa4i74Hu1rTv/MfMMcwHgX8U08Mt9KcubeKmfvNhfhxu5LslOTVaYaK/DZ3X63dr0//e2Aj8JE0Q0mdMNMK2gN65wDPa3eSpu/wTHkYcD/gMz3t/1BbPhP7Q0kant7vvqnv0/maz37bTH3rNt/T7HifDUCSpya5pB3y6Vs0B9Om+sC+/V5VbaQ5BvFK4JYkZyeZbbtMHTQMzQHDc6Yl5nr7my00vwOm+q0nTvVZbXxHAj/RZx3Tt8d894clzc8ZeExzKVmKfdvDaE6qn9d3fZqhDM9uhxr8NvA2Zj6OO6v25MzvAE9O8jiaq7bOm2UW9980EEs6GVZVH6f50fdjacYz/VCSz6QZn/tx7aQ/AN5YVbe1894y5HCXtfZM9X+hGRrioVW1G3AlM4/FOj3h81N96twIPKQdDmrKI4Eb5hDSTcCqdueid96ZzHQW+0Lj2BH9Yt6r5/k9Yqyq79McWDyS/mdd9NsGN9Js8zuB3atqt/axa/ujq19cP46jXd5efepJ0pI3h/6s97v4GzRDUvTrw3ZEvz7oeuCFPd/Ru1XVLlX1SWbuN2da1ndokktT+h0w653vucDhNENxPIjmbHXo06e3V49tqKqfBH4D+N89wwj3i+VMmj7rUOC7VXVxnzrfAL4H7NvT9gdV1dQOof2hJI1W73ff1PfpNn1Nkn59TT87ut+2vWXtOUOcc+qzk+xMc6XVPwAr298BF9D2gbP1e1X1jqr6BZoDdUUz5C/077cuAX5Ac8XZc7lnv9Xb3zyAZpitqX7rP6b9PnhAVb1ohu0x/b2SNCQe01xylmLf9nWa4QJn+q7/Tvt3pn3Bv6Ppox5fVbvSXKk1l3tqzXQMdepqr+cD57b7aH3ru/+mQVnSybAZnAb8YVUdRDP+9Zva8scAj0lzg71L0oypquG5P82X29ehuekwzZn0c/EW4AVpbjB8rySrkjyuqq4HPgn8XZobVz6eZkjFme5b0utimg7hj5KsSPJbzDxkI8DNwE/2m7DAOHbExTTDa7y4jfnwaTHfDDw09xym6iyasYR/k+Ysjl4Pp9kG907ybJoxgC+oqptohvs4Ocmu7Xb/qST9Loc+H9g3yW+1V7D9Ef0PpEpSF8y5P6uqHwGnA/+Y5ibAOyX5ufZA2o74Os09S3r7oVOBE9PeAyTNTYKf3U77IPATSV6aZOckD0zyxHbazcDq3D08BTT3Gjmi7QvW0n8opV4PpNnB+CbNjtPfzlQxydOTPLrdsfg2TT92V08s2/StbfLrR8DJzDBsRrtd/4XmHi0Pb9ezKsnU/czsDyVptI5Psmeae3e8nGbIv8/RfEcemOS+zH1Ivh3db5vNOTT7lT+d5H7AX05N2IE++z40wx1+Hdia5Kk09yIBZu73kjw2ya+0y/s+zUkdvf3h9L4Zmn7rDcDWqvrPadMOS/ILae478yrg0na/9IM0xz2e3/Zp907ys+l/T9JzgGOS7NNuj1dsfxNKGjCPaY6vpdi33QW8F3hlkvsl2Qc4umf612lO5H9e2+/9HtueFPJAYAvwrSSrgD+ZY0z99l+h2b97Jk1C7KyecvffNDSdSoa1Z0T9PPDuJJfTXAY5dUP7FTQ3hZ2gGXbnzUl2G36Uy1NVXU1zYOtimi+5/YH/muO8n6K9MTFwO/AfNGfTQfNerqbJ/r8PeEVVXTSHZf4A+C2aL9XbgN+l6SBm8nfAn6e5xPaP+0yfVxw7oifmY4Fv0XQeH6Q5IElVfQF4J/DlNs5HtOX/RdMJfbaqNk1b7KU0/xffoBnz+FlV9c122lE0O3tX02yjc7n7/6k3rm8AzwZeTXNgdA1zfG8laamZR3/2x8AVNGPA30pzFvgO/f5qx34/Cfiv9vv9kKp6X7uss9MMWXElzdjotMP2/hrNGelfo7mn2S+3i3t3+/ebST7bPv8Lmp2e22jGmN/ePQjOohmG4gaaPuKSWequAf6dZifqYuBNVTXZTpupbz2LZrtO3+Hp9TKaYaguadv/78Bjwf5QksbAO2gORH25ffxNVf0P8Nc039dfBKYnd/qax37bbMu6EHg98P9o+pCpq4+nhh/cbp/d9rF/RHPw8Taaq7Z6h3maqd/bmaZ/+AZN3/xwmoOp0L9vhuag4X70PznkHTTJq1uBg2jOnJ+K79dp7o1yY7uu17Tr77c9Xgd8rN0eH+uzHklD4jHNsbdU+7YX0wyp+DWaoTn/ddoi/oAmyfVNYF+ak/2n/BXwBJpjsefPNcZ++69t+WbgszQnl36ip777bxqa1Iz3MF8akqwGPlhV+yXZFbi2qu7xAU9yKnBJVZ3Rvv4ocEJVbe8GhdLYSnIpcGpVTe/Mptf7GPCOqnpzT9kxwO+3Q3VIkjQWkhwFrB9E/2R/KEma0l4tdSWwc1VtHXU80yXZBbgFeEJVfbGn/Axgc1X9+ahik7Q4PKapxba9vm3U+z5JTgdunGsf5v6bFlunrgyrqm8D100NE5TGAe3k99OeFZ1kd5pLjL88ijil+Ury5CQ/0V5GfTTweOBD25nnZ2nO5HjXMGKUJGm+2qE9/hfNEDGLvWz7Q0la5pI8M8l9kjyY5oqpfxvHRFjrRcCnexNhkrrLY5qar6XSt7XJ39+iuR3OXOq7/6ZFt6STYUneSXP552OTbE5yLM3wAMcm+RxwFc0N3gE+TDPswNU0l47+Sc/lk9JS8ViaMYlvBzbQXAZ800yVk5xJc7n2S9shMyRJGktp7vn1dZrhJ7c3VOOOLtv+UJIE8EKavuZLNPfsetFow+kvySbgJTT7fJI6yGOaWkRj37cleRXNFWt/X1XXzaG++28aiCU/TKIkSZIkSZIkSZI0kyV9ZZgkSZIkSZIkSZI0G5NhkiRJkiRJkiRJ6qwVow5gvnbfffdavXr1qMOY1Xe+8x3uf//7jzqMRWN7xpvtGV9LpS2f+cxnvlFVDxt1HNrWUujvdtRS+Z9YbLZ7ebHd48v+bjwttL8b58/euMY2rnGBsc2Xsc3PuMa20Ljs78bTUtu/G9f/j8XU9TZ2vX3Q/TZ2vX2wsDbuSH+3ZJNhq1ev5rLLLht1GLOanJxkYmJi1GEsGtsz3mzP+FoqbUnylVHHoHtaCv3djloq/xOLzXYvL7Z7fNnfjaeF9nfj/Nkb19jGNS4wtvkytvkZ19gWGpf93Xhaavt34/r/sZi63sautw+638autw8W1sYd6e8cJlGSJEmSJEmSJEmdZTJMkiRJkiRJkiRJnWUyTJIkSZIkSZIkSZ1lMkySJEmSJEmSJEmdZTJMkiRJkiRJkiRJnWUyTJIkSZIkSZIkSZ1lMkySJEmSJEmSJEmdZTJMkiRJkiRJkiRJnWUyTJIkSZIkSZIkSZ1lMkySJEmSJEmSJEmdZTJMkiRJkiRJkiRJnbVi1AGMyuoTzh/4Ojbsv5Vj+qxn06ufNvB1S5K0nA2jn5+J/bwkSeqahf62mun4yFz420qShs99anWRV4ZJkiRJkiRJkiSps0yGSZIkSZIkSZIkqbNMhkmSJEmSJEmSJKmzTIZJkiRJkiRJkiSps0yGSZIkSZIkSZIkqbNMhkmSJEmSJEmSJKmzTIZJkiRJkiRJkiSps0yGSZIkSZIkSZIkqbNMhkmSJEmSJEmSJKmzBp4MS3J6kluSXLmdej+b5K4kzxp0TJIkSZIkSZIkSVoehnFl2BnAutkqJNkJeA3w4SHEI0mSJEmSJEmSpGVi4Mmwqvo4cOt2qv0h8B7glkHHI0mSJEmSJEmSpOVj5PcMS7IKeCZw6qhjkSRJkqSu6zeUfZJ3Jbm8fWxKcnlbvjrJ93qmndozz0FJrkiyMcnrk6Qt37ld3sYklyZZPew2SpIkSVKvFaMOAHgd8LKquqvdd5pRkvXAeoCVK1cyOTk575Vu2H/rvOedq5W79F/PQuIepS1btizZ2PuxPeOtS+3pUlskSVInnAG8AThrqqCqfnfqeZKTgdt76n+pqg7ss5xTaPbPLgEuoBke/0LgWOC2qnp0kiNohsT/3T7zS5IkSdJQjEMybC1wdpsI2x04LMnWqnr/9IpVdRpwGsDatWtrYmJi3is95oTz5z3vXG3YfysnX3HPTbzpyImBr3sQJicnWcg2Hze2Z7x1qT1daoskSVr6qurjM12t1V7d9TvAr8y2jCR7ALtW1cXt67OAZ9Akww4HXtlWPRd4Q5JUVS1G/JIkSZK0o0aeDKuqvaeeJzkD+GC/RJgkSZIkaeB+Ebi5qr7YU7Z3kv8Gvg38eVV9AlgFbO6ps7kto/17PUBVbU1yO/BQ4Bu9K1rMkT/G+Ur8cY1tXOMCY5uvQca20NF1Zho5Zy4Gvb3H9T0d17gkSVqqBp4MS/JOYALYPclm4BXAvQGqyvuESZIkSdL4eA7wzp7XNwGPrKpvJjkIeH+SfYF+Y9xPXfk127S7CxZx5I9xvhJ/XGMb17jA2OZrkLEtdHSdmUbOmYtBj64zru/puMYlSdJSNfBkWFU9ZwfqHjPAUCRJkiRJM0iyAvgt4KCpsqq6E7izff6ZJF8CHkNzJdiePbPvCdzYPt8M7AVsbpf5IODWgTdAkiRJkmZwr1EHIEmSJEkaC78KfKGqfjz8YZKHJdmpff6TwBrgy1V1E3BHkkPa+4wdBXygne084Oj2+bOAj3m/MEnSUpPk9CS3JLlyhulHJvl8+/hkkgOGHaMkae5MhkmSJEnSMtIOZX8x8Ngkm5Mc2046gm2HSAT4JeDzST4HnAscV1VTV3m9CHgzsBH4EnBhW/4W4KFJNgL/GzhhYI2RJGlwzgDWzTL9OuDJVfV44FW0Q/9KksbTwIdJlCRJkiSNj5mGsu83bH1VvQd4zwz1LwP261P+feDZC4tSkqTRqqqPJ1k9y/RP9ry8hG2HD5YkjRmvDJMkSZIkSZKk+TuWu6+QliSNIa8MkyRJkiRJkqR5SPLLNMmwX5hh+npgPcDKlSuZnJwcXnALtGXLliUV73x0vY3zbd+G/bcufjBztKPx+h4ufcNqo8kwSZJobo4MPB24par2a8seArwLWA1sAn6nqm5rp51Is8NzF/BHVfXhtvwgmrHldwEuAF5SVZVkZ+As4CDgm8DvVtWmITVPkiRJkrTIkjye5v6ZT62qb/arU1Wn0d5PbO3atTUxMTG8ABdocnKSpRTvfHS9jfNt3zEnnL/4wczRpiMndqi+7+HSN6w2OkyiJEmNM7jnzZFPAD5aVWuAj7avSbIPcASwbzvPm5Ls1M5zCs1Zf2vax9QyjwVuq6pHA68FXjOwlkiSJEmSBirJI4H3As+vqv8ZdTySpNmZDJMkiebmyMCt04oPB85sn58JPKOn/OyqurOqrgM2Agcn2QPYtaourqqiuRLsGX2WdS5waJIMoi2SJEmSpIVJ8k7gYuCxSTYnOTbJcUmOa6v8JfBQmpMjL09y2ciClSRtl8MkSpI0s5VVdRNAVd2U5OFt+Srgkp56m9uyH7bPp5dPzXN9u6ytSW6n2XH6xuDClyRJkiTNR1U9ZzvTfx/4/SGFI0laIJNhkiTtuH5XdNUs5bPNc8+FL+EbLM/FMG6MOo43+10ON73tx3YvL8u13ZIkSZKk8WYyTJKkmd2cZI/2qrA9gFva8s3AXj319gRubMv37FPeO8/mJCuAB3HPYRmBpX2D5bkYxo1Rx/Fmv8vhprf92O7lZbm2W5IkSZI03rxnmCRJMzsPOLp9fjTwgZ7yI5LsnGRvYA3wqXZIxTuSHNLeD+yoafNMLetZwMfa+4pJkiRJkiRJGiCvDJMkiR/fHHkC2D3JZuAVwKuBc5IcC3wVeDZAVV2V5BzgamArcHxV3dUu6kXAGcAuwIXtA+AtwFuTbKS5IuyIITRLkiRJkiRJWvZMhkmSxKw3Rz50hvonASf1Kb8M2K9P+fdpk2mSJEmSJEmShsdhEiVJkiRJkiRJktRZXhkmSZIkSZIkSRpLq084f2DL3rD/Vo6ZZfmbXv20ga1b0nB5ZZgkSZIkSZIkSZI6y2SYJEmSJEmSJEmSOstkmCRJkiRJkiRJkjrLZJgkSZIkSZIkSZI6y2SYJEmSJEmSJEmSOstkmCRJkiRJkiRJkjrLZJgkSZIkSZIkSZI6y2SYJEmSJEmSJEmSOstkmCRJkiRJkiRJkjpr4MmwJKcnuSXJlTNMPzLJ59vHJ5McMOiYJEmSJEmSJEmStDwM48qwM4B1s0y/DnhyVT0eeBVw2hBikiRJkiRJkiRJ0jIw8GRYVX0cuHWW6Z+sqtval5cAew46JkmSJElarvqN3pHklUluSHJ5+zisZ9qJSTYmuTbJU3rKD0pyRTvt9UnSlu+c5F1t+aVJVg+1gZIkSZI0zbjdM+xY4MJRByFJkiRJHXYG/UfveG1VHdg+LgBIsg9wBLBvO8+bkuzU1j8FWA+saR9TyzwWuK2qHg28FnjNoBoiSZIkSXOxYtQBTEnyyzQ7Tb8wS531NDtbrFy5ksnJyXmvb8P+W+c971yt3KX/ehYS9yht2bJlycbej+0Zb11qT5faIkmSlr6q+vgOXK11OHB2Vd0JXJdkI3Bwkk3ArlV1MUCSs4Bn0JzceDjwynb+c4E3JElV1aI1QpIkSZJ2wFgkw5I8Hngz8NSq+uZM9arqNNp7iq1du7YmJibmvc5jTjh/3vPO1Yb9t3LyFffcxJuOnBj4ugdhcnKShWzzcWN7xluX2tOltkiSpE57cZKjgMuADe1w9qtohrOfsrkt+2H7fHo57d/rAapqa5LbgYcC3xhs+JIkSZLU38iTYUkeCbwXeH5V/c+o45EkSZKkZegU4FVAtX9PBn4PSJ+6NUs525n2Y4s58sc4X4k/rrGNa1xgbPM1yNgWOrrOTCPnzMWgt/e4vqfjGpckSUvVwJNhSd4JTAC7J9kMvAK4N0BVnQr8Jc1Zgm9q77e8tarWDjouSZIkSVKjqm6eep7kX4APti83A3v1VN0TuLEt37NPee88m5OsAB4E3NpnnYs28sc4X4k/rrGNa1xgbPM1yNgWOrrOTCPnzMWgR9cZ1/d0XOOSJGmpGngyrKqes53pvw/8/qDjkCRJkiT1l2SPqrqpfflM4Mr2+XnAO5L8I/AIYA3wqaq6K8kdSQ4BLgWOAv65Z56jgYuBZwEf835hkiRJkkZp5MMkSpIkSZKGZ4bROyaSHEgznOEm4IUAVXVVknOAq4GtwPFVdVe7qBcBZwC7ABe2D4C3AG9NspHmirAjBt4oSZIkSZqFyTBJkiRJWkZmGL3jLbPUPwk4qU/5ZcB+fcq/Dzx7ITFKkiRJ0mK616gDkCRJkiRJkiRJkgbFZJgkSZIkSZIkSZI6y2SYJEmSJEmSJPVIcnqSW5JcOcP0JHl9ko1JPp/kCcOOUZI0dybDJEmSJEmSJGlbZwDrZpn+VGBN+1gPnDKEmCRJ82QyTJIkSZIkSZJ6VNXHgVtnqXI4cFY1LgF2S7LHcKKTJO0ok2GSJEmSJEmStGNWAdf3vN7clkmSxtCKUQcgSZIkSZIkSUtM+pTVPSol62mGUWTlypVMTk4OOKzFs2XLlrGId8P+Wwe27JW7zL78cWj/Qsz3PRzkNt+eHY13XD6ng9L19sHw2mgyTJIkSZIkSZJ2zGZgr57XewI3Tq9UVacBpwGsXbu2JiYmhhLcYpicnGQc4j3mhPMHtuwN+2/l5CtmPkS+6ciJga17GOb7Hg5ym2/Pjm7zcfmcDkrX2wfDa6PDJEqSJEmSJEnSjjkPOCqNQ4Dbq+qmUQclSerPK8MkSZIkSZIkqUeSdwITwO5JNgOvAO4NUFWnAhcAhwEbge8CLxhNpJKkuTAZJkmSJEmSJEk9quo525lewPFDCkeStEAOkyhJkiRJkiRJkqTOMhkmSZIkSZIkSZKkzjIZJknSdiT5/5JcleTKJO9Mct8kD0lyUZIvtn8f3FP/xCQbk1yb5Ck95QcluaKd9vokGU2LJEmSJEmSpOXDZJgkSbNIsgr4I2BtVe0H7AQcAZwAfLSq1gAfbV+TZJ92+r7AOuBNSXZqF3cKsB5Y0z7WDbEpkiRJkiRJ0rJkMkySpO1bAeySZAVwP+BG4HDgzHb6mcAz2ueHA2dX1Z1VdR2wETg4yR7ArlV1cXuj5bN65pEkSZIkSZI0ICbDJEmaRVXdAPwD8FXgJuD2qvoIsLKqbmrr3AQ8vJ1lFXB9zyI2t2Wr2ufTyyVJkiRJkiQN0IpRByBJ0jhr7wV2OLA38C3g3UmeN9ssfcpqlvJ+61xPM5wiK1euZHJycgciHn9btmwZeJs27L91oMufzUxtG0a7x5HtXl6Wa7slSZIkSePNZJgkSbP7VeC6qvo6QJL3Aj8P3Jxkj6q6qR0C8Za2/mZgr57596QZVnFz+3x6+T1U1WnAaQBr166tiYmJxWvNGJicnGTQbTrmhPMHuvzZbDpyom/5MNo9jmz38rJc2y1JkiRJGm8OkyhJ0uy+ChyS5H5JAhwKXAOcBxzd1jka+ED7/DzgiCQ7J9kbWAN8qh1K8Y4kh7TLOapnHkmSJEmSJEkD4pVhkiTNoqouTXIu8FlgK/DfNFdtPQA4J8mxNAmzZ7f1r0pyDnB1W//4qrqrXdyLgDOAXYAL24ckSZIkSZKkATIZJknSdlTVK4BXTCu+k+YqsX71TwJO6lN+GbDfogcoSZIkSZIkaUYOkyhJkiRJkiRJkqTOGngyLMnpSW5JcuUM05Pk9Uk2Jvl8kicMOiZJkiRJkiRJkiQtD8O4MuwMYN0s058KrGkf64FThhCTJEmSJC1L/U5YTPL3Sb7QnqD4viS7teWrk3wvyeXt49SeeQ5KckV7YuPrk6Qt3znJu9ryS5OsHnYbJUmSJKnXwJNhVfVx4NZZqhwOnFWNS4Ddkuwx6LgkSZIkaZk6g3uesHgRsF9VPR74H+DEnmlfqqoD28dxPeWn0JzQOHVy49QyjwVuq6pHA68FXrP4TZAkSZKkuRuHe4atAq7veb25LZMkSZIkLbJ+JyxW1Ueqamv78hJgz9mW0Z7AuGtVXVxVBZwFPKOdfDhwZvv8XODQqavGJEmSJGkUVow6AKDfTlH1rZispznzkJUrVzI5OTnvlW7Yf+v2Ky3Qyl36r2chcY/Sli1blmzs/die8dal9nSpLZIkaVn4PeBdPa/3TvLfwLeBP6+qT9CcwLi5p07vSY0/PuGxqrYmuR14KPCNQQcuSZIkSf2MQzJsM7BXz+s9gRv7Vayq04DTANauXVsTExPzXukxJ5w/73nnasP+Wzn5intu4k1HTgx83YMwOTnJQrb5uLE9461L7elSWyRJUrcl+TNgK/D2tugm4JFV9c0kBwHvT7Ivs5/UOKcTHhfzZMdxPvloXGMb17jA2OZrkLEt9ITimU4WnotBb+9xfU/HNS5JkpaqcUiGnQe8OMnZwBOB26vqphHHJEmSJEnLSpKjgacDh7ZDH1JVdwJ3ts8/k+RLwGNoTmrsHUqx96TGqRMeNydZATyIPveRXsyTHcf55KNxjW1c4wJjm69BxrbQE4pnOll4LgZ9QvG4vqfjGpckSUvVwJNhSd4JTAC7J9kMvAK4N0BVnQpcABwGbAS+C7xg0DFJkiRJku6WZB3wMuDJVfXdnvKHAbdW1V1JfhJYA3y5qm5NckeSQ4BLgaOAf25nOw84GrgYeBbwsankmiRJkiSNwsCTYVX1nO1ML+D4QcchSZIkSZrxhMUTgZ2Bi5IAXFJVxwG/BPx1kq3AXcBxVTV1ldeLgDOAXYAL2wfAW4C3JtlIc0XYEUNoliRJkiTNaByGSZQkSZIkDckMJyy+ZYa67wHeM8O0y4D9+pR/H3j2QmKUJEmSpMV0r1EHIEmSJEmSJEmSJA2KyTBJkiRJkiRJkiR1lskwSZIkSZIkSZIkdZbJMEmSJEmSJEmSJHWWyTBJkiRJkiRJkiR1lskwSZIkSZIkSZIkdZbJMEmSJEmSJEmSJHWWyTBJkiRJkiRJ6pFkXZJrk2xMckKf6Q9K8m9JPpfkqiQvGEWckqS5MRkmSZIkSZIkSa0kOwFvBJ4K7AM8J8k+06odD1xdVQcAE8DJSe4z1EAlSXNmMkySJEmSJEmS7nYwsLGqvlxVPwDOBg6fVqeAByYJ8ADgVmDrcMOUJM2VyTBJkiRJkiRJutsq4Pqe15vbsl5vAH4auBG4AnhJVf1oOOFJknbUilEHIEmSJEmSJEljJH3KatrrpwCXA78C/BRwUZJPVNW3t1lQsh5YD7By5UomJycXPdhB2bJly1jEu2H/wV1wt3KX2Zc/Du1fiPm+h4Pc5tuzo/GOy+d0ULrePhheG02GSZIkSZIkSdLdNgN79bzek+YKsF4vAF5dVQVsTHId8DjgU72Vquo04DSAtWvX1sTExKBiXnSTk5OMQ7zHnHD+wJa9Yf+tnHzFzIfINx05MbB1D8N838NBbvPt2dFtPi6f00HpevtgeG10mERJkiRJkiRJutungTVJ9k5yH+AI4Lxpdb4KHAqQZCXwWODLQ41SkjRnXhkmSZIkSZIkSa2q2prkxcCHgZ2A06vqqiTHtdNPBV4FnJHkCpphFV9WVd8YWdCSpFmZDJMkSZIkSZKkHlV1AXDBtLJTe57fCPz6sOOSJM2PwyRKkiRJkiRJkiSps0yGSZIkSZIkSZIkqbNMhkmSJEmSJEmSJKmzTIZJkiRJkiRJkiSps0yGSZIkSZIkSZIkqbNMhkmSJEmSJEmSJKmzTIZJkiRJkiRJkiSps4aSDEuyLsm1STYmOaHP9Acl+bckn0tyVZIXDCMuSZIkSZIkSZIkddvAk2FJdgLeCDwV2Ad4TpJ9plU7Hri6qg4AJoCTk9xn0LFJkiRJkiRJkiSp24ZxZdjBwMaq+nJV/QA4Gzh8Wp0CHpgkwAOAW4GtQ4hNkqTtSrJbknOTfCHJNUl+LslDklyU5Ivt3wf31D+xvRr62iRP6Sk/KMkV7bTXt/2eJElDleT0JLckubKnbNH6tSQ7J3lXW35pktVDbaAkSZIkTTOMZNgq4Pqe15vbsl5vAH4auBG4AnhJVf1oCLFJkjQX/wR8qKoeBxwAXAOcAHy0qtYAH21f0179fASwL7AOeFN7lTTAKcB6YE37WDfMRkiS1DqDe/ZBi9mvHQvcVlWPBl4LvGZgLZEkSZKkOVgxhHX0O+u9pr1+CnA58CvATwEXJflEVX17mwUl62l2tli5ciWTk5PzDmrD/oO/8GzlLv3Xs5C4R2nLli1LNvZ+bM9461J7utSW5SjJrsAvAccAtFc5/yDJ4TRD+wKcCUwCL6O5+vnsqroTuC7JRuDgJJuAXavq4na5ZwHPAC4cUlMkSQKgqj7e52qtxezXDgde2S7rXOANSVJV0/cDJUmSJGkohpEM2wzs1fN6T5orwHq9AHh1u3O0Mcl1wOOAT/VWqqrTgNMA1q5dWxMTE/MO6pgTzp/3vHO1Yf+tnHzFPTfxpiMnBr7uQZicnGQh23zc2J7x1qX2dKkty9RPAl8H/jXJAcBngJcAK6vqJoCquinJw9v6q4BLeuafuiL6h+3z6eWSJI2DxezXfjw6SFVtTXI78FDgG4MLX5IkSZJmNoxk2KeBNUn2Bm6gGWLjudPqfBU4FPhEkpXAY4EvDyE2SZK2ZwXwBOAPq+rSJP9EO3TUDGa6InouV0o3C1jEK6HH0TCulhzGFeAzmalty/UqUdu9vCzXdnfcfPq1OfV5i9nfjfNnb1xjG9e4wNjma5CxLfS31Uwj58zFoLf3uL6n4xqXJElL1cCTYe2ZgC8GPgzsBJxeVVclOa6dfirwKuCMJFfQ7Di9rKo8a1CSNA42A5ur6tL29bk0ybCbk+zRnj2/B3BLT/1+V0Rvbp9PL7+HxbwSehwN42rJYVwBPpOZrgBfrleJ2u7lZbm2uyMWs1+bmmdzkhXAg4Bbp69wMfu7cf7sjWts4xoXGNt8DTK2hf62mmnknLkY9Og64/qejmtckiQtVfcaxkqq6oKqekxV/VRVndSWndomwqiqG6vq16tq/6rar6reNoy4JEnanqr6GnB9kse2RYcCVwPnAUe3ZUcDH2ifnwcckWTn9qroNcCn2qGn7khySJIAR/XMI0nSqC1mv9a7rGcBH/N+YZIkSZJGaRjDJEqStNT9IfD2JPehGcb3BTQnlJyT5Fia4X6fDdBe/XwOTcJsK3B8Vd3VLudFwBnALsCF7UOSpKFK8k5gAtg9yWbgFcCrWbx+7S3AW5NspLki7IghNEuSJEmSZmQyTJKk7aiqy4G1fSYdOkP9k4CT+pRfBuy3qMFJkrSDquo5M0xalH6tqr5Pm0yTJEmSpHEwlGESJUmSJEmSJEmSpFEwGSZJkiRJkiRJkqTOMhkmSZIkSZIkSZKkzjIZJkmSJEmSJEmSpM4yGSZJkiRJkiRJkqTOMhkmSZIkSZIkSZKkzjIZJkmSJEmSJEmSpM4yGSZJkiRJkiRJkqTOMhkmSZIkSZIkSZKkzjIZJkmSJEmSJEmSpM4yGSZJkiRJkiRJkqTOMhkmSZIkSZIkST2SrEtybZKNSU6Yoc5EksuTXJXkP4YdoyRp7laMOgBJkiRJkiRJGhdJdgLeCPwasBn4dJLzqurqnjq7AW8C1lXVV5M8fCTBSpLmxCvDJEmSJEmSJOluBwMbq+rLVfUD4Gzg8Gl1ngu8t6q+ClBVtww5RknSDvDKMEmSJEmSJEm62yrg+p7Xm4EnTqvzGODeSSaBBwL/VFVnTV9QkvXAeoCVK1cyOTk5iHgHYsuWLWMR74b9tw5s2St3mX3549D+hZjvezjIbb49OxrvuHxOB6Xr7YPhtdFkmCRJkiRJkiTdLX3KatrrFcBBwKHALsDFSS6pqv/ZZqaq04DTANauXVsTExOLH+2ATE5OMg7xHnPC+QNb9ob9t3LyFTMfIt905MTA1j0M830PB7nNt2dHt/m4fE4Hpevtg+G10WSYJEmSJEmSJN1tM7BXz+s9gRv71PlGVX0H+E6SjwMHAP+DJGnseM8wSZIkSZIkSbrbp4E1SfZOch/gCOC8aXU+APxikhVJ7kczjOI1Q45TkjRHXhkmSZIkSZIkSa2q2prkxcCHgZ2A06vqqiTHtdNPraprknwI+DzwI+DNVXXl6KKWJM3GZJgkSZIkSZIk9aiqC4ALppWdOu313wN/P8y4JEnz4zCJkiRJkiRJkiRJ6iyTYZIkSZIkSZIkSeosh0mUJEmSJEmSJGmMrD7h/AUvY8P+WzlmEZYjdcFQrgxLsi7JtUk2JjlhhjoTSS5PclWS/xhGXJIkSZIkSZIkSeq2gSfDkuwEvBF4KrAP8Jwk+0yrsxvwJuA3q2pf4NmDjkuSJEmSdLckj21PUJx6fDvJS5O8MskNPeWH9cxzYnvS47VJntJTflCSK9ppr0+S0bRKkiRJkoZzZdjBwMaq+nJV/QA4Gzh8Wp3nAu+tqq8CVNUtQ4hLkiRJktSqqmur6sCqOhA4CPgu8L528munplXVBQDtSY5HAPsC64A3tSdDApwCrAfWtI91w2uJJEmSJG1rGMmwVcD1Pa83t2W9HgM8OMlkks8kOWoIcUmSJEmS+jsU+FJVfWWWOocDZ1fVnVV1HbARODjJHsCuVXVxVRVwFvCMgUcsSZIkSTNYMYR19BsOo/rEcRDNDtcuwMVJLqmq/9lmQcl6mrMLWblyJZOTk/MOasP+W+c971yt3KX/ehYS9yht2bJlycbej+0Zb11qT5faIkmSlo0jgHf2vH5xe9LiZcCGqrqN5iTHS3rqTJ34+MP2+fRySZIkSRqJYSTDNgN79bzeE7ixT51vVNV3gO8k+ThwALBNMqyqTgNOA1i7dm1NTEzMO6hjTjh/3vPO1Yb9t3LyFffcxJuOnBj4ugdhcnKShWzzcWN7xluX2tOltkiSpO5Lch/gN4ET26JTgFfRnNT4KuBk4PeY+cTHuZwQuagnO47zyUfjGtu4xgXGNl+DjG2hJxTPdLLwXAx6e4/rezqucUmStFQNIxn2aWBNkr2BG2jOMHzutDofAN6QZAVwH+CJwGuHEJskSZIkaVtPBT5bVTcDTP0FSPIvwAfblzOd+Li5fT69fBuLebLjOJ98NK6xjWtcYGzzNcjYFnpC8UwnC8/FoE8oHtf3dFzjkiRpqRr4PcOqaivwYuDDwDXAOVV1VZLjkhzX1rkG+BDweeBTwJur6spBxyZJkiRJuofn0DNEYnsPsCnPBKb21c4Djkiyc3vy4xrgU1V1E3BHkkOSBDiK5gRISZIkSRqJYVwZRlVdAFwwrezUaa//Hvj7YcQjSZIkSbqnJPcDfg14YU/x/0lyIM1Qh5umprUnOZ4DXA1sBY6vqrvaeV4EnEFzT+gL24ckSZIkjcRQkmGSJEmSpPFXVd8FHjqt7Pmz1D8JOKlP+WXAfoseoCRJkiTNg8kwSZIkSZIkSZI0cqt38D6VG/bfuuB7W07Z9OqnLcpyNJ4Gfs8wSZIkSZIkSZIkaVRMhkmSNAdJdkry30k+2L5+SJKLknyx/fvgnronJtmY5NokT+kpPyjJFe201yfJKNoiSZIkSZIkLScmwyRJmpuXANf0vD4B+GhVrQE+2r4myT7AEcC+wDrgTUl2auc5BVgPrGkf64YTuiRJkiRJkrR8mQyTJGk7kuwJPA14c0/x4cCZ7fMzgWf0lJ9dVXdW1XXARuDgJHsAu1bVxVVVwFk980iSJEmSJEkaEJNhkiRt3+uAPwV+1FO2sqpuAmj/PrwtXwVc31Nvc1u2qn0+vVySJEmSJEnSAK0YdQCSJI2zJE8HbqmqzySZmMssfcpqlvJ+61xPM5wiK1euZHJyck6xLhVbtmwZeJs27L91oMufzUxtG0a7x5HtXl6Wa7slSZIkSePNZJgkSbN7EvCbSQ4D7gvsmuRtwM1J9qiqm9ohEG9p628G9uqZf0/gxrZ8zz7l91BVpwGnAaxdu7YmJiYWsTmjNzk5yaDbdMwJ5w90+bPZdORE3/JhtHsc2e7lZbm2W5IkSZI03hwmUZKkWVTViVW1Z1WtBo4APlZVzwPOA45uqx0NfKB9fh5wRJKdk+wNrAE+1Q6leEeSQ5IEOKpnHkmSJEmSJEkD4pVhkiTNz6uBc5IcC3wVeDZAVV2V5BzgamArcHxV3dXO8yLgDGAX4ML2IUmSJEmSJGmATIZJkjRHVTUJTLbPvwkcOkO9k4CT+pRfBuw3uAglSZIkSZIkTecwiZIkSZIkSZIkSeosrwyTJEmSJEmagytuuJ1jTjh/JOve9OqnjWS9kiRJXeCVYZIkSZIkSZIkSeosk2GSJEmSJEmSJEnqLJNhkiRJkiRJkiRJ6iyTYZIkSZIkSZLUI8m6JNcm2ZjkhFnq/WySu5I8a5jxSZJ2jMkwSZIkSZIkSWol2Ql4I/BUYB/gOUn2maHea4APDzdCSdKOMhkmSZIkSZIkSXc7GNhYVV+uqh8AZwOH96n3h8B7gFuGGZwkacetGHUAkiRJkiRJkjRGVgHX97zeDDyxt0KSVcAzgV8BfnamBSVZD6wHWLlyJZOTk4sd68Bs2bJlLOLdsP/WgS175S6zL3+U7V+Mdm+vfV2wmG0ch8/7dOPyfzhIw2qjyTBJkiRJkiRJulv6lNW0168DXlZVdyX9qrczVZ0GnAawdu3ampiYWKQQB29ycpJxiPeYE84f2LI37L+Vk6+Y+RD5piMnBrbu7VmMdm+vfV2wmG0c5fs9k3H5PxykYbWx2/8JkiRJkiRJkrRjNgN79bzeE7hxWp21wNltImx34LAkW6vq/UOJUJK0Q0yGSZIkSZIkSdLdPg2sSbI3cANwBPDc3gpVtffU8yRnAB80ESZJ48tkmCRJkiRJkiS1qmprkhcDHwZ2Ak6vqquSHNdOP3WkAUqSdti9hrGSJOuSXJtkY5ITZqn3s0nuSvKsYcQlSZIkSbpbkk1JrkhyeZLL2rKHJLkoyRfbvw/uqX9iu593bZKn9JQf1C5nY5LXZ7abqUiSNIaq6oKqekxV/VRVndSWndovEVZVx1TVucOPUpI0VwNPhiXZCXgj8FRgH+A5SfaZod5raM64kCRJkiSNxi9X1YFVtbZ9fQLw0apaA3y0fU27X3cEsC+wDnhTu18HcAqwHljTPtYNMX5JkiRJ2sYwrgw7GNhYVV+uqh8AZwOH96n3h8B7gFuGEJMkSZIkaW4OB85sn58JPKOn/OyqurOqrgM2Agcn2QPYtaourqoCzuqZR5IkSZKGbhjJsFXA9T2vN7dlP5ZkFfBMwPF2JUmSJGl0CvhIks8kWd+WrayqmwDavw9vy2fa11vVPp9eLkmSJEkjsWII6+g3NnxNe/064GVVdddsQ8m3O2PrAVauXMnk5OS8g9qw/9Z5zztXK3fpv56FxD1KW7ZsWbKx92N7xluX2tOltkiSpM57UlXdmOThwEVJvjBL3Zn29eayD7io+3fj/HtrXGMb17hgvGObaT9/GLa3TQa53Rba5oVst0F/Fsb18zaucUmStFQNIxm2Gdir5/WewI3T6qwFzm4TYbsDhyXZWlXv761UVacBpwGsXbu2JiYm5h3UMSecP+9552rD/ls5+Yp7buJNR04MfN2DMDk5yUK2+bixPeOtS+3pUlskSVK3VdWN7d9bkryPZtj7m5PsUVU3tUMgTg1tP9O+3ub2+fTy6etatP27cf69Na6xjWtcMN6x/fPbP9B3P38YtncsYZDbbaHHUGY6PjIXgz6GMq6ft3GNS5KkpWoYwyR+GliTZO8k96G5wfJ5vRWqau+qWl1Vq4Fzgf81PREmSZIkSRqcJPdP8sCp58CvA1fS7L8d3VY7GvhA+/w84IgkOyfZG1gDfKodSvGOJIekOePxqJ55JEmSJGnoBn46U1VtTfJi4MPATsDpVXVVkuPa6d4nTJIkSZJGbyXwvnbEjhXAO6rqQ0k+DZyT5Fjgq8CzAdr9unOAq4GtwPFVdVe7rBcBZwC7ABe2D0mSJEkaiaFc219VFwAXTCvrmwSrqmOGEZMkSZIk6W5V9WXggD7l3wQOnWGek4CT+pRfBuy32DFKkiRJ0nwMY5hESZIkSZIkSZIkaSRMhkmSJEmSJEmSJKmzTIZJkiRJkiRJkiSps0yGSZIkSZIkSZIkqbNMhkmSJEmSJEmSJKmzTIZJkiRJkiRJkiSps0yGSZIkSZIkSZIkqbNMhkmSJEmSJEmSJKmzTIZJkiRJkiRJkiSps0yGSZIkSZIkSZIkqbNMhkmSJEmSJEmSJKmzTIZJkiRJkiRJkiSps0yGSZIkSZIkSZIkqbNMhkmSJEmSJEmSJKmzTIZJkjSLJHsl+X9JrklyVZKXtOUPSXJRki+2fx/cM8+JSTYmuTbJU3rKD0pyRTvt9UkyijZJkiRJkiRJy4nJMEmSZrcV2FBVPw0cAhyfZB/gBOCjVbUG+Gj7mnbaEcC+wDrgTUl2apd1CrAeWNM+1g2zIZIkSZIkSdJyZDJMkqRZVNVNVfXZ9vkdwDXAKuBw4My22pnAM9rnhwNnV9WdVXUdsBE4OMkewK5VdXFVFXBWzzySJEmSJEmSBsRkmCRJc5RkNfAzwKXAyqq6CZqEGfDwttoq4Pqe2Ta3Zava59PLJUmSJEmSJA3QilEHIEnSUpDkAcB7gJdW1bdnud1Xvwk1S3m/da2nGU6RlStXMjk5ucPxjrMtW7YMvE0b9t860OXPZqa2DaPd48h2Ly/Ltd2SJEmSpPFmMkySpO1Icm+aRNjbq+q9bfHNSfaoqpvaIRBvacs3A3v1zL4ncGNbvmef8nuoqtOA0wDWrl1bExMTi9WUsTA5Ocmg23TMCecPdPmz2XTkRN/yYbR7HNnu5WW5tluSJEmSNN4cJlGSpFmkuQTsLcA1VfWPPZPOA45unx8NfKCn/IgkOyfZG1gDfKodSvGOJIe0yzyqZx5JkiRJ0hhJsi7JtUk2Jjmhz/Qjk3y+fXwyyQGjiFOSNDdeGSZJ0uyeBDwfuCLJ5W3Zy4FXA+ckORb4KvBsgKq6Ksk5wNXAVuD4qrqrne9FwBnALsCF7UOSJEmSNEaS7AS8Efg1mlE+Pp3kvKq6uqfadcCTq+q2JE+lGd3jicOPVpI0FybDJEmaRVX9J/3v9wVw6AzznASc1Kf8MmC/xYtOkiRJkjQABwMbq+rLAEnOBg6nOekRgKr6ZE/9S9h2WHxJ0phxmERJkiRJkiRJutsq4Pqe15vbspkciyN/SNJY88owSZIkSZIkSbpbv9FBqm/F5JdpkmG/MMP09cB6gJUrVzI5OblIIQ7eli1bxiLeDftvHdiyV+4y+/JH2f7FaPf22tcFi9nGcfi8Tzcu/4eDNKw2DiUZlmQd8E/ATsCbq+rV06YfCbysfbkFeFFVfW4YsUmSJEmSIMlewFnATwA/Ak6rqn9K8krgD4Cvt1VfXlUXtPOcSHMA8C7gj6rqw235Qdx9n8wLgJdUVd+DiJIkjaHNwF49r/cEbpxeKcnjgTcDT62qb/ZbUFWdRnM/MdauXVsTExOLHuygTE5OMg7xHnPC+QNb9ob9t3LyFTMfIt905MTA1r09i9Hu7bWvCxazjaN8v2cyLv+HgzSsNg58mMSeG04+FdgHeE6SfaZVm7rh5OOBV9F2EJIkSZKkodkKbKiqnwYOAY7v2Xd7bVUd2D6mEmH7AEcA+wLrgDe1+38Ap9CcBb+mfawbYjskSVqoTwNrkuyd5D40/d15vRWSPBJ4L/D8qvqfEcQoSdoBw7hn2I9vOFlVPwCmbjj5Y1X1yaq6rX3pDSclSZIkaciq6qaq+mz7/A7gGma/P8rhwNlVdWdVXQdsBA5Osgewa1Vd3F4NdhbwjMFGL0nS4qmqrcCLgQ/T9IfnVNVVSY5Lclxb7S+Bh9KcDHJ5kstGFK4kaQ6GcY1kvxtOPnGW+jPecHIxx9gdxlipM41XulTH+Oza+KS2Z7x1qT1daoskSVoekqwGfga4FHgS8OIkRwGX0Vw9dhvNvt4lPbNtbst+2D6fXi5J0pLRXgl9wbSyU3ue/z7w+8OOS5I0P8NIhi3aDScXc4zdQY41O2Wm8UrHcezRueja+KS2Z7x1qT1daoskSeq+JA8A3gO8tKq+neQUmuHsq/17MvB7zLyvN6d9wMU82XGcTz4a19jGNS4Y79hmOul1GLa3TQa53Rba5oVst0F/Fsb18zaucUmStFQNIxm2aDeclCRJkiQNTpJ70yTC3l5V7wWoqpt7pv8L8MH25Uz7epvZduj7vvuAi3my4ziffDSusY1rXDDesf3z2z/Q96TXYdjeibWD3G4LPaF4ppOF52LQJxSP6+dtXOOSJGmpGsY9w7zhpCRJkiSNuSQB3gJcU1X/2FO+R0+1ZwJXts/PA45IsnOSvYE1wKeq6ibgjiSHtMs8CvjAUBohSZIkSX0M/HSmqtqaZOqGkzsBp0/dcLKdfirb3nASYGtVrR10bJIkSZKkH3sS8HzgiiSXt2UvB56T5ECaoQ43AS8EaPfrzgGuBrYCx1fVXe18LwLOAHahuSd03/tCS5IkSdIwDOXafm84KUmSJEnjrar+k/73+7qgT9nUPCcBJ/UpvwzYb/GikyRJkqT5G8YwiZIkSZIkSZIkSdJImAyTJEmSJEmSJElSZ5kMkyRJkiRJkiRJUmeZDJMkSZIkSZIkSVJnmQyTJEmSJEmSJElSZ5kMkyRJkiRJkiRJUmeZDJMkSZIkSZIkSVJnmQyTJEmSJEmSJElSZ60YdQCS1CWrTzj/HmUb9t/KMX3KF9umVz9t4OuQJEmSJElaLvod55G0NHllmCRJkiRJkiRJkjrLZJgkSZIkSZIkSZI6y2SYJEmSJEmSJEmSOstkmCRJkiRJkiRJkjrLZJgkSZIkSZIkSZI6a8WoA5AkSZIkSZIkzW71CecPdX0b9t/KMe06N736aUNdtyQtNpNhkiRJkiRJkiRpWRt2wrmXCefBc5hESZIkSZIkSZIkdZZXhkmS1GGe1SRJkiRJkqTlzivDJEmSJEmSJEmS1FleGSZJkiRJkiRJkjQiM43ss2H/rRwz4FF/lsvIPl4ZJkmSJEmSJEmSpM7yyjBJkiRpnjx7T5IkSZK0lI3yfvMAZ6y7/1DW45VhkiRJkiRJkiRJ6iyTYZIkSZIkSZIkSeqsoSTDkqxLcm2SjUlO6DM9SV7fTv98kicMIy5JkoZte32iJEldYZ8nSVrKPJ4pSd0y8HuGJdkJeCPwa8Bm4NNJzquqq3uqPRVY0z6eCJzS/lWHjHLsUe+pIWkczLFPlCRpybPPkyQtZR7PvKdR31NIkhZq4Mkw4GBgY1V9GSDJ2cDhQG/ncThwVlUVcEmS3ZLsUVU3DSE+SZKGZS59oiRJXTDUPu+KG27nmBEdpBvliXcLOTC5Yf+tC9pmnnAoqeM8nilJHTOMZNgq4Pqe15u551kS/eqsAuw8JO0wz1bSGJtLn9gZM/0vLvTgmyRpSVhWfZ6kbhrlvuUZ6+4/snUL8HimJHXOMJJh6VNW86hDkvXA+vblliTXLjC2gfoj2B34xvTyvGYEwSyOvu1ZCmbY5ku2PTOwPWNqpu+CxbYI3y2PWoQwNLtO9nc7alj/E6Myy/9ip9s9i2XZ7mF8zsf0N+VSeL/t74Zju33eIvd3I/vszeF/cSz/Lxb6PTXg76Cx3GYtP2vzsJDP2xD6u7Hcbr/8mgXHZX+3MMv2eGavru+7Qffb2PX2Qffb2PX2wYL7vDn3d8NIhm0G9up5vSdw4zzqUFWnAactdoCDkuSyqlo76jgWi+0Zb7ZnfHWpLVqwTvZ3O2q5/k/Y7uXFdkvb7/MWs78b58/euMY2rnGBsc2Xsc3PuMY2rnEtI8v2eGav5fA57Hobu94+6H4bu94+GF4b7zXoFQCfBtYk2TvJfYAjgPOm1TkPOCqNQ4DbHV9XktRBc+kTJUnqAvs8SdJS5vFMSeqYgV8ZVlVbk7wY+DCwE3B6VV2V5Lh2+qnABcBhwEbgu8ALBh2XJEnDNlOfOOKwJEladPZ5kqSlzOOZktQ9wxgmkaq6gKaD6C07ted5AccPI5YhW5KXQM/C9ow32zO+utQWLVC/PnEZWq7/E7Z7ebHdWvaG3OeN82dvXGMb17jA2ObL2OZnXGMb17iWjWV8PLPXcvgcdr2NXW8fdL+NXW8fDKmNab63JUmSJEmSJEmSpO4Zxj3DJEmSJEmSJEmSpJEwGTYASfZK8v+SXJPkqiQvGXVMC5VkpyT/neSDo45lMSTZLcm5Sb7Qvk8/N+qY5ivJ/9d+zq5M8s4k9x11TDsiyelJbklyZU/ZQ5JclOSL7d8HjzLGHTFDe/6+/ax9Psn7kuw2whClkUqyKckVSS5Pctmo4xmUrn23zdUM7X5lkhva9/zyJIeNMsZBmOm3X9ff81na3fn3XKPR7ztm2vQkeX2Sje3vrieMUWwTSW7v+b/4yyHFtd1901FttznGNqrtdt8kn0ryuTa2v+pTZ1TbbS6xjWS7teue8djBKP9H5xDbKLfZrL+PR73dtDzN5Tu6C2b7XuiCdOj4Zz9Z4sdE++n3mzId2q+coX1DO25qMmwwtgIbquqngUOA45PsM+KYFuolwDWjDmIR/RPwoap6HHAAS7RtSVYBfwSsrar9aG7qesRoo9phZwDrppWdAHy0qtYAH21fLxVncM/2XATsV1WPB/4HOHHYQUlj5per6sCqWjvqQAboDLr13TZXZ3DPdgO8tn3PD2zvvdA1M/326/p7Pttv3q6/5xqNM+j/HTPlqcCa9rEeOGUIMU05g9ljA/hEz//FXw8hJpjbvumotttc95tHsd3uBH6lqg4ADgTWJTlkWp1Rbbe5xAaj2W4w+7GDUf6PwvaPa4xqm8Hsv49Hvd20PHXx2GY/XTveOV0njn/205Fjov2cQbePJZzBCI+bmgwbgKq6qao+2z6/g+aLZtVoo5q/JHsCTwPePOpYFkOSXYFfAt4CUFU/qKpvjTSohVkB7JJkBXA/4MYRx7NDqurjwK3Tig8Hzmyfnwk8Y5gxLUS/9lTVR6pqa/vyEmDPoQcmaai69t02VzO0u/Nm+e3X6fe8a795Nf7m8B1zOHBWNS4Bdkuyx5jENhJz/D8dyXYb5++QdltsaV/eu31Mv+H6qLbbXGIbiTkcOxjZ/+gSP64xsu2m5Wucv6MXyxL/XtiuDh7/7GdJHxPtp+vHEkZ93NRk2IAlWQ38DHDpiENZiNcBfwr8aMRxLJafBL4O/Gt7KfSbk9x/1EHNR1XdAPwD8FXgJuD2qvrIaKNaFCur6iZofoABDx9xPIvp94ALRx2ENEIFfCTJZ5KsH3UwQ9bl77bteXE75MHpS3lIh7mY9ttv2bznfX7zLpv3XGNlFXB9z+vNjNeBu59rh7a7MMm+w175LPumI99u29lvHsl2a4fOuhy4BbioqsZmu80hNhjNdnsdsx87GOVn7XVs/7jGqP5Ht/f7eOT/o1reOnJss5/X0a3jndN15vhnPx0+JtrPstmvZMDHTU2GDVCSBwDvAV5aVd8edTzzkeTpwC1V9ZlRx7KIVgBPAE6pqp8BvsMSvby0Pbh0OLA38Ajg/kmeN9qoNJMkf0Yz1MDbRx2LNEJPqqon0Az3cnySXxp1QBq4U4CfohnK6Sbg5JFGM0Bd+O03H33avWzec42d9CkbiytmgM8Cj2qHtvtn4P3DXPl2vp9Gut22E9vItltV3VVVB9KcnXxwkv2mVRnZdptDbEPfbnM8djCSbTbH2Eb5P7q938fj/N2mjuvq79uOHu+crjPHP/vxmGj3DOO4qcmwAUlyb5rO4u1V9d5Rx7MATwJ+M8km4GzgV5K8bbQhLdhmYHPP2XPn0nQOS9GvAtdV1der6ofAe4GfH3FMi+HmqWEf2r+3jDieBUtyNPB04MiqcsdFy1ZV3dj+vQV4H3DwaCMaqs59t81FVd3cHrT7EfAvdPQ9n+G3X+ff837tXi7vucbSZmCvntd7MibD5VTVt6eGtqvmPnr3TrL7MNY9h33TkW237cU2yu3WE8O3gEnueX+LkX/eZoptRNttLscORrXNthvbKD9rc/h9PPLPmpanDh3b7KeLxzun69Lxz366eky0n+WwXzmU46YmwwYgSWjGY72mqv5x1PEsRFWdWFV7VtVqmpsQfqyqlnSWvaq+Blyf5LFt0aHA1SMMaSG+ChyS5H7t5+5QunEzzPOAo9vnRwMfGGEsC5ZkHfAy4Der6rujjkcalST3T/LAqefArwNXjjaqoerUd9tcTbunxTPp4Hs+y2+/Tr/nM7V7ObznGlvnAUelcQjNcDk3jToogCQ/0f7PkORgmn3xbw5hvXPZNx3JdptLbCPcbg9Lslv7fBeaA25fmFZtVNttu7GNYrvN8djBSLbZXGIb4WdtLr+Px/a7Td3VpWOb/XTxeOd0HTv+2U9Xj4n20/X9yqEdN10xyIUvY08Cng9ckWYcb4CXt2cXaTz8IfD2JPcBvgy8YMTxzEtVXZrkXJohHbYC/w2cNtqodkySdwITwO5JNgOvAF4NnJPkWJrO7dmji3DHzNCeE4GdgYva/atLquq4kQUpjc5K4H3t/8EK4B1V9aHRhjQYXftum6sZ2j2R5ECa4Xw2AS8cVXwD1Pe3H91/z2dq93OWwXuuEZjhO+beAFV1KnABcBiwEfguQ/yNP4fYngW8KMlW4HvAEUMaLWCm/9NH9sQ2qu02l9hGtd32AM5MshNNUuScqvpgkuN6YhvVdptLbKPabvcwJtusrzHZZn1/H4/zdtOy4bHNbujE8c9+unBMtJ+uH0sY9XHTOFqXJEmSJEmSJEmSusphEiVJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDtOwkOSPJ3yxg/mOS/OdixjTPOF6e5M2zTN+U5FeHGZMkqXuSPDbJfye5I8mPkvzFqGOSJC0/w+qPkrwyydsGsWxJ0vga1nG0JKe6TzVekjwyyZYkO80w3d8GHbFi1AFIXZLkGOD3q+oXBr2uqvrbQa9DkiTgT4HJqvqZUQciSVrWln1/lOQMYHNV/fmoY5EkbV+/44RVddzoIlpcSTbRtO/fB7T8AtZU1cZBLH9KVX0VeMAg16Hx4JVhkiRJms2jgKtGHYQkadmbU3+UxJN+JUnqkYZ5AC17/hOo85L8TJLPtsNpvAu4b8+0pye5PMm3knwyyeN7pu2V5L1Jvp7km0neMMPy/z7JfyY5ADgV+Ln20tpvJfnZJDf37pAl+e0kl7fPX5nk3CTvauP7bLucqbqPSPKeNobrkvxRz7RtLtFN8vwkX2lj/bPF2XqSpOUsyceAXwbe0PZt75gaajjJRJLN7bC932iHFTmyZ97Dklzd9m83JPnjUbVDkrS0zbE/elmSrwH/muReSU5I8qV2/+icJA9p669OUknWJ7kxyU1JNsyy7ncn+VqS25N8PMm+PdN2SXJyux92e7tfuEs77ZB2H/NbST6XZKJnvock+dd2/bclef+0tmxIcksb2wvaaeuBI4E/bbfBvy3uVpYkAQcm+Xz7nf6uJPeF7R4/nOpv7mj3f57Zlv80044TtuU/vn3LbN/77fSHJvm3JN9O8ukkf5OeW7ck+fm2/Pb278/3TJtMclKS/wK+C/xkmlu/fLmN9bokRybZOcmtSfbvmffhSb6X5GFJdk/ywbbttyb5RNvPvhV4JPBvbfv+tJ13tv5vsm3DJ6f6sraNb+9p4+q27sfb2T7X1v3dJFcm+Y2e5d07zb7ogdvr3zO33wYr2td7J/mPdjtdBOy+4x8ljSOTYeq0JPcB3g+8FXgI8G7gt9tpTwBOB14IPBT4v8B5bSewE/BB4CvAamAVcPa0Zd8ryb8Ajwd+vao+BxwHXFxVD6iq3arq08A3gV/rmfV5bTxTDm/jegjwDuD97Zf5vYB/Az7Xrv9Q4KVJntKnnfsApwDPBx7RtmfPHd1ekiT1qqpfAT4BvLiqHgD8YFqVn6DZMVgFHA2cluSx7bS3AC+sqgcC+wEfG07UkqSumWN/9BCaq8fWA38EPAN4Ms3+0W3AG6fN88vAGuDXgRMy831iLmzrPRz4LPD2nmn/ABwE/Hy7/j8FfpRkFXA+8Ddt+R8D70nysHa+twL3A/Ztl/vaaW15EE3feizwxiQPrqrT2nX/n3Z/8zeQJC223wHWAXvTHO87Zrbjh+08XwJ+kea7+6+AtyXZo6quYdpxwhnW2fd7v532RuA7bZ2j2wfQnFhB09e8vo3rH4Hzkzy0Z9nPp+kXHwh8va371HYf7eeBy6vqTppjns/rme85wL9X1deBDcBm4GHASuDlQFXV84GvAr/Rtu//zKH/AziijWsV8FPAxcC/tvWvAV5Bs4Jfausf0C7/XcBZ0+I8DLipqi7vKZupf5/Lb4Mp7wA+Q7Ov+yp6truWNpNh6rpDgHsDr6uqH1bVucCn22l/APzfqrq0qu6qqjOBO9t5Dqb5YvyTqvpOVX2/qv6zZ7n3Bt5J80X9G1X13VliOJP2i7rtqJ5C86U65TNVdW5V/ZCm47pvG8PPAg+rqr+uqh9U1ZeBf6HpNKZ7FvDBqvp424n9BfCjuW0iSeMuyentWWJXzrH+77RnpF2V5B3bn0NakL+oqjur6j9odnx+py3/IbBPkl2r6raq+uzoQpQkddyPgFe0/dH3aA5Y/llVbW73j14JPCvbDqH4V+2+3hU0B+Ge02/BVXV6Vd3Rs5wDkjyoPXnx94CXVNUN7T7lJ9t6zwMuqKoLqupHVXURcBlwWJI9gKcCx7X94w/bPnTKD4G/bssvALYAj0VSp7iPN7ZeX1U3VtWtNCeoH8jsxw+pqne38/yoTdh8kea44lz1/d5vT9T/bZr+7btVdTXNMcYpTwO+WFVvraqtVfVO4AtA78kSZ1TVVVW1FdhK01/ul2SXqrqpqqaGHz4TeG7uHkrx+dx9Iv8PgT2AR7UxfqKqaoa2zNj/9dT516r6UlXdTnPCyZeq6t/bGN8NzHZv0LfR9KW79olzykz9+1x+G5DkkTTHZKf2cz9O81lQB5gMU9c9Arhh2pf0V9q/jwI2tJftfivN5cp7tfPsBXyl/SLu59E0V3T9VVVNPytxurcBv5HkATQHCD9RVTf1TL9+6klV/YjmbItHtPE9Ylp8L6c5C6NfO3uX8x2aK9IkdcMZNGenbVeSNcCJwJOqal/gpYMLS+K2ts+Z8hWaPgmaHbfDgK+0Q0z83NCjkyQtF1+vqu/3vH4U8L6e/ahrgLvYdl/q+p7nvf3XjyXZKcmr2yGVvg1saift3j7uS3NFwHSPAp49bV/uF2gOJu4F3FpVt83Qlm9O2w/9LvCAGepKWrrOwH28cfS1nudT37+zHT8kyVG5ewjFb9GMirEjw+rN9L3/MGAF2/ZXvc8fwd3HOKd8heaKq3vUb/fbfpfmarWbkpyf5HHttEtprkB7clv2aOC8dta/BzYCH0kzxOIJs7Rltv5vys09z7/X5/WMfV5V3Qj8F/DbSXajObnk7dOqzdS/z+W3AW39fvu56gCTYeq6m4BVSdJT9sj27/XASdUMZzj1uF97JsX1wCOnnx3Q4xrgBcCFPcNBAdzjzIiquoHmkt9n0v+Mhb2mnrRnYOwJ3NjGcN20+B5YVYdxTzdNW879aC6RltQB7ZlIt/aWJfmpJB9K8pk0Y3Y/rp30B8Abpw6wVNUtQw5Xy8uDk9y/5/UjafowqurTVXU4zfBP7wfOGX54kqRlYvp+2PU0w0D17kvdt903m7JXz/Mf91/TPJfmJMhfpRnCanVbHuAbwPdphnia7nrgrdPWf/+qenU77SHtQbwdNdOZ+JKWGPfxlpQZjx8meRTNKE4vBh5azVCIV9L0E7Cw7+2v01zN1XsblN6+60aaBE+vRwK9fd0266+qD1fVr9Ekp77Qxj5lamSr5wPnTp1k0l4dvaGqfpLmqrP/neTQfstn9v5vsUzF+WyaIShvmDZ9pv59Lr8NoDnG2m8/Vx1gMkxddzFNx/FHSVYk+S3uvlT5X4DjkjwxjfsneVqSBwKfovnye3Vbft8kT+pdcJs0eznw70mmdoBuBvZs71XW6yya8eP3B943bdpBSX6rTby9lOZS60vaGL6d5kbQu7RnJe6X5Gf7tPNc4OlJfqFd91/j/7fUdacBf1hVB9GMw/2mtvwxwGOS/FeSS5LM6WxDaQH+Ksl9kvwi8HTg3e3rI5M8qJphgL9Nc9adJEnDcCpwUnuQkiQPS3L4tDp/keR+SfalOdHxXX2W80Ca/bNv0tzj62+nJrSjepwO/GOSR7T7az+X5h4yU6ODPKUtv2+SiSR7tqOEXAi8KcmD09wv+pfuueq+bgZ+cs5bQdJS4z7eeJrt+OH9aRJCXwdI8gKaK8OmzHSccLuq6i7gvcAr2/7qccBRPVUuoPlcPLc95vm7wD7AB/stL8nKJL/ZJnnupBmOsXcf7a00J/I/j+Y45tR8T0/y6PZCg6n9uqn5pvdLM/Z/O9r+GZYPzYmWTwBe0htnj5n697n8NqCqvkIztOPUfu4vsO3Qk1rCPFiuTmuHMPwt4BiaGyP+Lk1HQlVdRnN2zRvaaRvbelMdzm/QXBb8VZqhC3+3z/LPpEk8fSzJauBjwFXA15J8o6fq+2gvx512mS3AB9pl30Zz9sVvtWPwTsVwIHAdzZmHb6Y5I3F6HFcBx9Pci+ymdlmb57CJJC1BaYZd/XmapMPlNDfwnRp2YAXNzWInaMbGfvM8zzyW5uJrNH3OjTTDUxxXVV9opz0f2JRmWKnj2PZGx5IkDdI/0Qzv9JEkd9CcbPjEaXX+g2Yf8KPAP1TVR/os5yyaoZFuAK5ul9Prj4EraO5LfSvwGuBeVXU9zRVlL6c5QHo98CfcfQzm+TT3YPkCcAtzH/LsLTT34/xWkvfPcR5JS4D7eONrO8cPrwZOpjkZ/2aak+D/q2f2mY4TztWLaY4Dfo0mWfVOmkQWVfVNmpMRN9CctPGnwNOraqb13KuteyNNn/Vk4H/1tHMz8Fma5N4neuZbA/w7TfLsYuBNVTXZTvs74M/bfumP59D/7ahXAme2y/+dNs7vAe8B9qY9xjvNTP37XH4bTHluO+1W4BX0T7ppCcrM97uTtJiSfAl4YVX9e0/ZK4FHV5UHCCXNqk24f7Cq9ktzs9hrq2qPPvVOBS6pqjPa1x8FTqiqTw8zXnVfkgngbVU137P8JEkauvY31XXAvWe5R7QkDZz7eNpRSV4D/ERVHT2g5Z8O3FhVfz6I5S+WJH8JPKb3eKr9u+bCK8OkIUjy2zRnVnxs1LFIWvqq6tvAdUmeDdAO1XBAO/n9wC+35bvTDKnx5VHEKUmSJEnaPvfx1E+SxyV5fPt5OBg4lnvefmWx1rWaZnSttwxi+YslyUNotsNpo45FS4/JMGnAkkwCpwDHt+PKS9IOSfJOmuEIHptkc5JjgSOBY5N8jmbYhamxrj8MfDPJ1cD/A/6kHT5BkiRJkjQG3MfTHD2QZijA7wDn0AzJ+IHFXkmSVwFXAn9fVdct9vIXS5I/oBl68cKq+vio49HS4zCJkiRJkiRJkiRJ6iyvDJMkSZIkSZIkSVJnmQyTJEmSJEmSJElSZ60YdQDztfvuu9fq1atHHca8fOc73+H+97//qMMYOtu9vNjupeczn/nMN6rqYaOOQ9sadn+3VD7Dxrm4jHNxGefiWuw47e/G00L7u3H+PI9rbOMaFxjbfBnb/IxrbAuNy/5uPE31d+P6uRsVt8e23B53c1tsy+1xt6ltsSP93ZJNhq1evZrLLrts1GHMy+TkJBMTE6MOY+hs9/Jiu5eeJF8ZdQy6p2H3d0vlM2yci8s4F5dxLq7FjtP+bjwttL8b58/zuMY2rnGBsc2Xsc3PuMa20Ljs78bTVH83rp+7UXF7bMvtcTe3xbbcHneb2hY70t85TKIkSZIkSZIkSZI6y2SYJEmSJEmSJEmSOstkmCRJkiRJkiRJkjpru8mwJPdN8qkkn0tyVZK/astfmeSGJJe3j8N65jkxycYk1yZ5Sk/5QUmuaKe9Pkna8p2TvKstvzTJ6gG0VZIkSZIkSZIkScvMXK4MuxP4lao6ADgQWJfkkHbaa6vqwPZxAUCSfYAjgH2BdcCbkuzU1j8FWA+saR/r2vJjgduq6tHAa4HXLLhlkiRJkiRJkiRJWva2mwyrxpb25b3bR80yy+HA2VV1Z1VdB2wEDk6yB7BrVV1cVQWcBTyjZ54z2+fnAodOXTUmSZIkSZIkSZIkzdec7hmWZKcklwO3ABdV1aXtpBcn+XyS05M8uC1bBVzfM/vmtmxV+3x6+TbzVNVW4HbgoTveHEmSJEmSJEmSJOluK+ZSqaruAg5MshvwviT70Qx5+Cqaq8ReBZwM/B7Q74qumqWc7Uz7sSTraYZZZOXKlUxOTs4l/LGzZcuWJRv7Qtju5cV2S5IkSZIkSZLGwZySYVOq6ltJJoF1VfUPU+VJ/gX4YPtyM7BXz2x7Aje25Xv2Ke+dZ3OSFcCDgFv7rP804DSAtWvX1sTExI6EPzYmJydZqrEvhO1eXmy3lqv2PpmXATdU1dOnTQvwT8BhwHeBY6rqs8OPUpIkSZIkSVo+tjtMYpKHtVeEkWQX4FeBL7T3AJvyTODK9vl5wBFJdk6yN7AG+FRV3QTckeSQ9mDgUcAHeuY5un3+LOBj7X3FJElaal4CXDPDtKfS9ItraK50PmVYQUmSJEmSJEnL1VyuDNsDOLM90/1ewDlV9cEkb01yIM1whpuAFwJU1VVJzgGuBrYCx7fDLAK8CDgD2AW4sH0AvAV4a5KNNFeEHbHwpknj4YobbueYE84fybo3vfppI1mvtFwl2RN4GnAS8L/7VDkcOKs94eOSJLsl2aM9YWQgVu/g98+G/bcu2neW30GSpGHxN7ckaTnY0f27xWR/J2mp224yrKo+D/xMn/LnzzLPSTQHAqeXXwbs16f8+8CztxeLJElj7nXAnwIPnGH6KuD6nteb27KBJcMkSZIkSZKk5W6H7hkmSZL6S/J04Jaq+kySiZmq9Sm7x7DASdbTDKPIypUrmZycnHdcG/bfukP1V+6y4/PMZCFxb8+WLVsGuvzFYpyLyzgXl3FKkiRJkpYLk2GSJC2OJwG/meQw4L7ArkneVlXP66mzGdir5/WewI3TF1RVpwGnAaxdu7YmJibmHdSODhm1Yf+tnHzF4vw82HTkxKIsp5/JyUkWsl2GxTgXl3EuLuOUJEmSJC0X9xp1AJIkdUFVnVhVe1bVapp7X35sWiIM4DzgqDQOAW4f5P3CJEmSJEmSJHllmCRJA5XkOICqOhW4ADgM2Ah8F3jBCEOTJEmSJEmSlgWTYZIkLbKqmgQm2+en9pQXcPxoopIkSZIkSZKWJ4dJlCRJkiRJkiRJUmeZDJMkSZIkSZIkSVJnmQyTJEmSJEmSJElSZ5kMkyRJkiRJkiRJUmeZDJMkSZIkSZIkSVJnmQyTJEmSJEmSJElSZ5kMkyRJkiRJkiRJUmeZDJMkSZIkSZIkSVJnmQyTJEmSpA5KsinJFUkuT3JZW/aQJBcl+WL798E99U9MsjHJtUme0lN+ULucjUlenyRt+c5J3tWWX5pkdc88R7fr+GKSo4fYbEmSJEm6B5NhkiRJktRdv1xVB1bV2vb1CcBHq2oN8NH2NUn2AY4A9gXWAW9KslM7zynAemBN+1jXlh8L3FZVjwZeC7ymXdZDgFcATwQOBl7Rm3STJEmSpGEzGSZJkiRJy8fhwJnt8zOBZ/SUn11Vd1bVdcBG4OAkewC7VtXFVVXAWdPmmVrWucCh7VVjTwEuqqpbq+o24CLuTqBJkiRJ0tCZDJMkSZKkbirgI0k+k2R9W7ayqm4CaP8+vC1fBVzfM+/mtmxV+3x6+TbzVNVW4HbgobMsS5IkSZJGYsWoA5AkSZIkDcSTqurGJA8HLkryhVnqpk9ZzVI+33nuXmGToFsPsHLlSiYnJ2cJb3Yrd4EN+2+d9/wLsb24t2zZsqC2Dcq4xgXGNl/GNj/jGtu4xiVJ0lJlMkySJEmSOqiqbmz/3pLkfTT377o5yR5VdVM7BOItbfXNwF49s+8J3NiW79mnvHeezUlWAA8Cbm3LJ6bNM9knvtOA0wDWrl1bExMT06vM2T+//QOcfMVodm83HTkx6/TJyUkW0rZBGde4wNjmy9jmZ1xjG9e4JElaqhwmUZIkSZI6Jsn9kzxw6jnw68CVwHnA0W21o4EPtM/PA45IsnOSvYE1wKfaoRTvSHJIez+wo6bNM7WsZwEfa+8r9mHg15M8OMmD23V/eIDNlSRJkqRZeWWYJEmSJHXPSuB9Tf6KFcA7qupDST4NnJPkWOCrwLMBquqqJOcAVwNbgeOr6q52WS8CzgB2AS5sHwBvAd6aZCPNFWFHtMu6NcmrgE+39f66qm4dZGMlSZIkaTYmwyRJkiSpY6rqy8ABfcq/CRw6wzwnASf1Kb8M2K9P+fdpk2l9pp0OnL5jUUuSJEnSYDhMoiRJkiRJkiRJkjrLZJgkSZIkSZIkSZI6y2SYJEmSJEmSJEmSOstkmCRJkiRJkiRJkjrLZJgkSZIkSZIkSZI6a7vJsCT3TfKpJJ9LclWSv2rLH5LkoiRfbP8+uGeeE5NsTHJtkqf0lB+U5Ip22uuTpC3fOcm72vJLk6weQFslSRqYmfrLaXUmktye5PL28ZejiFWSJEmSJElaTuZyZdidwK9U1QHAgcC6JIcAJwAfrao1wEfb1yTZBzgC2BdYB7wpyU7tsk4B1gNr2se6tvxY4LaqejTwWuA1C2+aJElDNVN/Od0nqurA9vHXQ41QkiRJkiRJWoa2mwyrxpb25b3bRwGHA2e25WcCz2ifHw6cXVV3VtV1wEbg4CR7ALtW1cVVVcBZ0+aZWta5wKFTV41JkrQUzNJfSpIkSZIkSRqhOd0zLMlOSS4HbgEuqqpLgZVVdRNA+/fhbfVVwPU9s29uy1a1z6eXbzNPVW0FbgceOo/2SJI0MjP0l9P9XDuU4oVJ9h1uhJIkSZKk7UmyV5L/l+Sadhj8l/Spk/Y2MBuTfD7JE0YRqyRpblbMpVJV3QUcmGQ34H1J9puler8rumqW8tnm2XbByXqaYRZZuXIlk5OTs4QxvrZs2bJkY1+I5drulbvAhv23jmTdo9zey/X9Xq7tVqNff1lVV/ZU+SzwqKrakuQw4P00wwZvYzH7ux39/lnM76xB/i8slf8141xcxrm4jFOSJGlGW4ENVfXZJA8EPpPkoqq6uqfOU7n7VjBPpLk9zBOHH6okaS7mlAybUlXfSjJJc6+vm5PsUVU3tUMg3tJW2wzs1TPbnsCNbfmefcp759mcZAXwIODWPus/DTgNYO3atTUxMbEj4Y+NyclJlmrsC7Fc2/3Pb/8AJ1+xQ/9qi2bTkRMjWS8s3/d7ubZb25rWX17ZU/7tnucXJHlTkt2r6hvT5l+0/u6YE87fofob9t+6aN9Zg/wOWir/a8a5uIxzcRmnJElSf+0oWFMjYt2R5Bqaka16k2GHA2e1t4O5JMluU8dKhx+xJGl7tjtMYpKHtWe4k2QX4FeBLwDnAUe31Y4GPtA+Pw84IsnOSfamOTviU21HcEeSQ9r7gR01bZ6pZT0L+FjbkUiStCTM0l/21vmJqXtiJjmYph/+5pBDlSRJkiTNUZLVwM8A04fBn+lWMZKkMTSXU7/3AM5MshPNQbtzquqDSS4GzklyLPBV4NkAVXVVknNozpTYChzfDhsF8CLgDGAX4ML2AfAW4K1JNtJcEXbEYjROkqQhmqm/PA6gqk6lOeHjRUm2At8DjvDkD0mSJEkaT0keALwHeGnvSB9Tk/vMMqfbvsx3GOhR3YYDHAZ/mNwed3NbbMvtcbf5bIvtJsOq6vM0Zz9ML/8mcOgM85wEnNSn/DLgHvcbq6rv0ybTJElaimbpL0/tef4G4A3DjEuSJEmStOOS3JsmEfb2qnpvnyoz3SpmG/2GwZ/vMNA7Ogz+YnIY/OFxe9zNbbEtt8fd5rMttjtMoiRJkiRJkiQtF+3w9m8Brqmqf5yh2nnAUWkcAtzu/cIkaXzNZZhESZIkSZIkSVoungQ8H7giyeVt2cuBR8KPRwC5ADgM2Ah8F3jB8MOUJM2VyTBJkiRJkiRJalXVf9L/nmC9dQo4fjgRSZIWymESJUmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJkiRJkiRJktRZJsMkSZIkSZIkSZLUWSbDJEmSJEmSJEmS1FkmwyRJWgRJ7pvkU0k+l+SqJH/Vp06SvD7JxiSfT/KEUcQqSZIkSZIkLScmwyRJWhx3Ar9SVQcABwLrkhwyrc5TgTXtYz1wylAjlCQtO0l2SvLfST7Yvn5IkouSfLH9++Ceuie2J2xcm+QpPeUHJbminfb6JGnLd07yrrb80iSre+Y5ul3HF5McPcQm6/9n7//jJCvrO+//9ZZBnKgoP2JnnCGBjWM2KOsPOkjW3WxHoozGzZiNJmNIQG92Z2Ux0XvJRsjee2vMl/1idtEEVLhnhQUMiixqmFWQsJiK8V7khy46DkicwEQ6zEIEgrSJaOPn/qPOSNFU9/R0V1dVV72ej8d59KnrXNepz3WqZk5Vfc51HUmSJElPYjJMkqQeqLaZ5uGBzVJzqm0GLmvqfgF4dpJ1/YxTkjR23gbc0fH4TOCGqtoI3NA8JsnRwBbgBcAm4INJDmjaXED7Io69F3RsaspPBR6qqucB7wPe0+zrUOCdwMuA44B3dibdJEmSJKnfTIZJktQjzdX3twH3A9dX1U1zqqwH7ul4PN2USZLUc0k2AD8PfKijeDNwabN+KfC6jvIrqurRqrob2AUc11y0cXBV3VhVBVw2p83efV0FnNCMGjuR9nnwwap6CLiexxNokiRJktR3awYdgCRJo6KqHgNenOTZwCeTvLCqvtpRJd2azS1IspX2FfhMTEzQarWWHNMZx8zuV/2JtfvfZj7LiXtfZmZmVnT/vWKcvWWcvWWcY+EPgN8GntlRNlFVewCqak+S5zTl64EvdNTbe8HG95r1ueV729zT7Gs2ycPAYXjxhyRJkqQhYzJMkqQeq6q/TdKifRV8ZzJsGjii4/EG4N4u7bcB2wAmJydrampqybG86cxP71f9M46Z5dwdvfl4sPukqZ7sp5tWq8Vyjku/GGdvGWdvGedoS/Ja4P6q+mKSqcU06VJWC5QvtU1njD27+KOXF3Psr33FPawJ3WGNC4xtqYxtaYY1tmGNS5Kk1Wqfv3YlOYL2VBg/Anwf2FZVf5jkXcC/Av6mqfo7VXVN0+Ys2vPHPwb8ZlVd15QfC1wCrAWuAd5WVZXkoOY5jgUeAH6lqnb3qI+SJK24JD8MfK9JhK0Ffo7m3ikdtgNvTXIF7fuoPLz36nxJknrs5cAvJHkN8DTg4CR/BNyXZF0zKmwd7al9Yf4LNqab9bnlnW2mk6wBngU82JRPzWnTmhtgLy/+OP/yq3t2Mcf+2tfFH8Oa0B3WuMDYlsrYlmZYYxvWuCRJWq0Wc8+wWeCMqvpJ4Hjg9ObmygDvq6oXN8veRFjPbrwsSdIqsg740yRfAW6hfa+UTyV5S5K3NHWuAe6ifR+W/wL8m8GEKkkadVV1VlVtqKojaX8/+2xV/RrtCzNOaaqdAlzdrG8HtiQ5KMlRtL+v3dxctPFIkuOb+4GdPKfN3n29vnmOAq4DXpXkkCSHAK9qyiRJkiRpIPZ56Vzz5WfvnPKPJLmDhed7/8GNl4G7k+y98fJumhsvAyTZe+Pla5s272raXwW8P0maL1KSJA29qvoK8JIu5Rd2rBdwej/jkiRpjnOAK5OcCnwDeANAVe1MciVwO+0LIk9v7oUJcBqPz/BxbbMAXAR8uPnO9yDtpBtV9WCS36N9cQjAu6vqwZXumCRJkiTNZ7/mkUhyJO0f+m6iPe3GW5OcDNxKe/TYQ/T2xsvfnPP8PZtTfpDGdd7nce33MN+/YCWN6+s9rv2WJEnDq6paNNMUVtUDwAnz1DsbOLtL+a3AC7uUf4cmmdZl28XAxUuNWZIkSZJ6adHJsCTPAD4OvL2qvpXkAuD3aN8I+feAc4H/g97eePmJBT2cU36QxnXe53Ht9zDfv2AljevrPa79liRJkiRJkqRhtZh7hpHkQNqJsMur6hMAVXVfVT1WVd+nfd+T45rqy7nxMnNuvCxJkiRJkiRJkiQt2T6TYc1Nki8C7qiq93aUr+uo9ovAV5v1Xt54WZIkSZIkSZIkSVqyxczd9nLg14EdSW5ryn4HeGOSF9OeznA38K+htzdeliRJkiRJkiRJkpZjn8mwqvo83e/pdc0CbXp242VJkiRJkiRJkiRpqRZ1zzBJkiRJkiRJkiRpNTIZJkmSJEmSJEmSpJFlMkySJEmSJEmSJEkjy2SYJEmSJEmSJEmSRpbJMEmSJEmSJEmSJI0sk2GSJEmSJEmSJEkaWSbDJEmSJEmSJEmSNLJMhkmSJEmSJEmSJGlkmQyTJEmSJEmSJEnSyDIZJkmSJEmSJEmSpJFlMkySJEmSJEmSJEkjy2SYJEmSJEmSJHVIcnGS+5N8dZ7tU0keTnJbs/zf/Y5RkrR4awYdgCRJkiRJkiQNmUuA9wOXLVDnz6vqtf0JR5K0HI4MkyRJkiRJkqQOVfU54MFBxyFJ6g1HhkmSJEmSJEnS/vvpJF8G7gV+q6p2zq2QZCuwFWBiYoJWq8XMzAytVmu/n+yMY2aXGe7SLSXexVrq8RhVHo/HeSyeyOPxuKUcC5NhkiRJkiRJkrR/vgT8WFXNJHkN8MfAxrmVqmobsA1gcnKypqamaLVaTE1N7fcTvunMTy8r4OXYfdLUiu17qcdjVHk8HuexeCKPx+OWciycJlGSpB5IckSSP01yR5KdSd7WpY43WJYkSZKkEVBV36qqmWb9GuDAJIcPOCxJ0jwcGSZJUm/MAmdU1ZeSPBP4YpLrq+r2OfW8wbIkSZIkrXJJfgS4r6oqyXG0Bx08MOCwJEnzMBkmSVIPVNUeYE+z/kiSO4D1wNxkmCRJkiRpyCX5KDAFHJ5kGngncCBAVV0IvB44Lcks8PfAlqqqAYUrSdoHk2GSJPVYkiOBlwA3ddm8zxssS5IkSZIGq6reuI/t7wfe36dwJEnLZDJMkqQeSvIM4OPA26vqW3M2L+oGy0m2AlsBJiYmaLVaS47njGNm96v+xNr9bzOf5cS9LzMzMyu6/14xzt4yzt4yTkmSJEnSuDAZJklSjyQ5kHYi7PKq+sTc7Z3Jsaq6JskHkxxeVd+cU28bsA1gcnKypqamlhzTm8789H7VP+OYWc7d0ZuPB7tPmurJfrpptVos57j0i3H2lnH2lnFKkiRJksbFUwYdgCRJoyBJgIuAO6rqvfPU+ZGmHt5gWZIkSZIkSeoPR4ZJktQbLwd+HdiR5Lam7HeAHwVvsCxJkiRJkiQNiskwSZJ6oKo+D2QfdbzBsiRJkiRJktRnTpMoSZIkSZIkSZKkkWUyTJIkSZIkSZIkSSNrn8mwJEck+dMkdyTZmeRtTfmhSa5P8vXm7yEdbc5KsivJnUlO7Cg/NsmOZtt5SdKUH5TkY035TUmOXIG+SpIkSZIkSZIkacwsZmTYLHBGVf0kcDxwepKjgTOBG6pqI3BD85hm2xbgBcAm4INJDmj2dQGwFdjYLJua8lOBh6rqecD7gPf0oG+SJEmSJEmSJEkac/tMhlXVnqr6UrP+CHAHsB7YDFzaVLsUeF2zvhm4oqoeraq7gV3AcUnWAQdX1Y1VVcBlc9rs3ddVwAl7R41JkiRJkiRJkiRJS7Vf9wxrpi98CXATMFFVe6CdMAOe01RbD9zT0Wy6KVvfrM8tf0KbqpoFHgYO25/YJEmSJEmSJEmSpLnWLLZikmcAHwfeXlXfWmDgVrcNtUD5Qm3mxrCV9jSLTExM0Gq19hH1cJqZmVm1sS/HuPZ7Yi2ccczsQJ57kMd7XF/vce23JEmSJEmSJA2rRSXDkhxIOxF2eVV9oim+L8m6qtrTTIF4f1M+DRzR0XwDcG9TvqFLeWeb6SRrgGcBD86No6q2AdsAJicna2pqajHhD51Wq8VqjX05xrXf519+NefuWHTeuad2nzQ1kOeF8X29x7XfkiRJkiRJkjSs9jlNYnPvrouAO6rqvR2btgOnNOunAFd3lG9JclCSo4CNwM3NVIqPJDm+2efJc9rs3dfrgc829xWTJEmSJO2nJE9LcnOSLyfZmeR3m/JDk1yf5OvN30M62pyVZFeSO5Oc2FF+bJIdzbbz9t7fufnO97Gm/KZmWv29bU5pnuPrSU5BkiRJkgZoMfcMeznw68ArktzWLK8BzgFemeTrwCubx1TVTuBK4HbgM8DpVfVYs6/TgA8Bu4C/BK5tyi8CDkuyC/i3wJm96JwkSZIkjalHgVdU1YuAFwObkhxP+7vWDVW1EbiheUySo4EtwAuATcAHkxzQ7OsC2tPVb2yWTU35qcBDVfU84H3Ae5p9HQq8E3gZcBzwzs6kmyRJkiT12z7nbquqz9P9nl4AJ8zT5mzg7C7ltwIv7FL+HeAN+4pFkiRJkrRvzUwbM83DA5ulgM3AVFN+KdAC3tGUX1FVjwJ3NxcqHpdkN3BwVd0IkOQy4HW0L2zcDLyr2ddVwPubUWMnAtdX1YNNm+tpJ9A+uiKdlSRJkqR9GMyNjCRJkiRJK6oZ2fVF4HnAB6rqpiQTzRT2NPd/fk5TfT3whY7m003Z95r1ueV729zT7Gs2ycPAYZ3lXdpIkiTtlyPP/PTAnnv3OT8/sOeW1FsmwyRJkiRpBDXT1b84ybOBTyZ50iwdHbrNBlILlC+1zeNPmGylPf0iExMTtFqtBcJb2MRaOOOY2SW3X459xT0zM7Osvq2UYY0LjG2pjG1phjW2YY1LkqTVymSYJEmSJI2wqvrbJC3aUxXel2RdMypsHXB/U20aOKKj2Qbg3qZ8Q5fyzjbTSdYAzwIebMqn5rRpdYlrG7ANYHJysqampuZWWbTzL7+ac3cM5uvt7pOmFtzearVYTt9WyrDGBca2VMa2NMMa27DGJUnSavWUQQcgSZIkSeqtJD/cjAgjyVrg54CvAduBU5pqpwBXN+vbgS1JDkpyFLARuLmZUvGRJMc39wM7eU6bvft6PfDZ5l5l1wGvSnJIkkOAVzVlkiRJkjQQjgyTJEmSpNGzDri0uW/YU4Arq+pTSW4ErkxyKvAN4A0AVbUzyZXA7cAscHozzSLAacAlwFrg2mYBuAj4cJJdtEeEbWn29WCS3wNuaeq9u6oeXNHeSpIkSdICTIZJkiRJ0oipqq8AL+lS/gBwwjxtzgbO7lJ+K/Ck+41V1Xdokmldtl0MXLx/UUuSJEnSynCaREmSJEmSJEmSJI0sk2GSJEmSJEmSJEkaWSbDJEmSJEmSJEmSNLJMhkmSJEmSJEmSJGlkmQyTJEmSJEmSJEnSyDIZJkmSJEmSJEmSpJFlMkySpB5IckSSP01yR5KdSd7WpU6SnJdkV5KvJHnpIGKVJEmSJEmSxsmaQQcgSdKImAXOqKovJXkm8MUk11fV7R11Xg1sbJaXARc0fyVJkiRJkiStEEeGSZLUA1W1p6q+1Kw/AtwBrJ9TbTNwWbV9AXh2knV9DlWSJEmSJEkaK44MkySpx5IcCbwEuGnOpvXAPR2Pp5uyPXPabwW2AkxMTNBqtZYcyxnHzO5X/Ym1+99mPsuJe19mZmZWdP+9Ypy9ZZy9ZZySJEmSpHFhMkySpB5K8gzg48Dbq+pbczd3aVJPKqjaBmwDmJycrKmpqSXH86YzP71f9c84ZpZzd/Tm48Huk6Z6sp9uWq0Wyzku/WKcvWWcvWWckiRJkqRx4TSJkiT1SJIDaSfCLq+qT3SpMg0c0fF4A3BvP2KTJEmSJEmSxpXJMEmSeiBJgIuAO6rqvfNU2w6cnLbjgYeras88dSVJkiRJkiT1gNMkSpLUGy8Hfh3YkeS2pux3gB8FqKoLgWuA1wC7gL8D3tz/MCVJkiRJkqTxYjJMkqQeqKrP0/2eYJ11Cji9PxFJkiRJkiRJAqdJlCRJkiRJkiRJ0ggzGSZJkiRJkiRJkqSRZTJMkiRJkiRJkiRJI8tkmCRJkiRJkiRJkkaWyTBJkiRJkiRJkiSNLJNhkiRJkiRJkiRJGln7TIYluTjJ/Um+2lH2riR/neS2ZnlNx7azkuxKcmeSEzvKj02yo9l2XpI05Qcl+VhTflOSI3vcR0mSJEmSJEmSJI2pxYwMuwTY1KX8fVX14ma5BiDJ0cAW4AVNmw8mOaCpfwGwFdjYLHv3eSrwUFU9D3gf8J4l9kWSJEmSJEmSlq3bAIE529Nc8L8ryVeSvLTfMUqSFm+fybCq+hzw4CL3txm4oqoeraq7gV3AcUnWAQdX1Y1VVcBlwOs62lzarF8FnLB31JgkSZIkSZIkDcAldB8gsNerefyi/620BwJIkobUcu4Z9tbmqoeLkxzSlK0H7umoM92UrW/W55Y/oU1VzQIPA4ctIy5JkiRJkiRJWrJFDBDYDFxWbV8Ant0MCJAkDaE1S2x3AfB7QDV/zwX+D6DbiK5aoJx9bHuCJFtpX2nBxMQErVZrv4IeFjMzM6s29uUY135PrIUzjpkdyHMP8niP6+s9rv2WJEmSJGnMzDcoYM9gwpEkLWRJybCqum/vepL/AnyqeTgNHNFRdQNwb1O+oUt5Z5vpJGuAZzHPVRdVtQ3YBjA5OVlTU1NLCX/gWq0WqzX25RjXfp9/+dWcu2Opeefl2X3S1ECeF8b39R7XfkuSJEmSNGYWdYF/t4v7l3oh7aAutoaVveB6X8djVPs9Hy+0fpzH4ok8Ho9byrFY0i/0SdZV1d6rHH4R2Hsjye3AR5K8F3gu7Tlzb66qx5I8kuR44CbgZOD8jjanADcCrwc+29xXTJIkSZIkSZKG0XyDAp6g28X9S72Q9k1nfnppkfbASl5wva/jMar9no8XWj/OY/FEHo/HLeVY7DMZluSjwBRweJJp4J3AVJIX077aYTfwrwGqameSK4HbgVng9Kp6rNnVabRvPLkWuLZZAC4CPpxkF+0RYVv2qweSJEmSJEmS1F/bgbcmuQJ4GfBwx+ABSdKQ2WcyrKre2KX4ogXqnw2c3aX8VuCFXcq/A7xhX3FIkiRJkiRJUj/MM0DgQICquhC4BngNsAv4O+DNg4lUkrQYg7mRkSRJkiRJkiQNqXkGCHRuL+D0PoUjSVqmpww6AEmSJEmSJEmSJGmlmAyTJEmSJEmSJEnSyDIZJkmSJEmSJEmSpJFlMkySJEmSJEmSJEkjy2SYJEmSJEmSJEmSRpbJMEmSJEmSJEmSJI0sk2GSJPVAkouT3J/kq/Nsn0rycJLbmuX/7neMkqTxkeSIJH+a5I4kO5O8rSk/NMn1Sb7e/D2ko81ZSXYluTPJiR3lxybZ0Ww7L0ma8oOSfKwpvynJkR1tTmme4+tJTulj1yVJkiTpSUyGSZLUG5cAm/ZR58+r6sXN8u4+xCRJGl+zwBlV9ZPA8cDpSY4GzgRuqKqNwA3NY5ptW4AX0D6ffTDJAc2+LgC2AhubZe/57lTgoap6HvA+4D3Nvg4F3gm8DDgOeGdn0k2SJEmS+s1kmCRJPVBVnwMeHHQckiQBVNWeqvpSs/4IcAewHtgMXNpUuxR4XbO+Gbiiqh6tqruBXcBxSdYBB1fVjVVVwGVz2uzd11XACc2osROB66vqwap6CLiefV8wIkmSJEkrxmSYJEn989NJvpzk2iQvGHQwkqTx0Exf+BLgJmCiqvZAO2EGPKepth64p6PZdFO2vlmfW/6ENlU1CzwMHLbAviRJkiRpINYMOgBJksbEl4Afq6qZJK8B/pj2VFNPkmQr7emomJiYoNVqLflJzzhmdr/qT6zd/zbzWU7c+zIzM7Pg/nf89cMr9tz7csz6Z/1gfV9xDgvj7C3j7K3VEuewSvIM4OPA26vqW83tvrpW7VJWC5QvtU1nbD073/Xy/LW/9hX3sL6HhzUuMLalMralGdbYhjUuSZJWK5NhkiT1QVV9q2P9miQfTHJ4VX2zS91twDaAycnJmpqaWvLzvunMT+9X/TOOmeXcHb35eLD7pKme7KebVqvFQsdlf/vdS5393lecw8I4e8s4e2u1xDmMkhxIOxF2eVV9oim+L8m6qtrTTIF4f1M+DRzR0XwDcG9TvqFLeWeb6SRrgGfRnjJ4Gpia06Y1N75enu/Ov/zqnp2/9te+znfD+h4e1rjA2JbK2JZmWGMb1rgkSVqtnCZRkqQ+SPIjzX1USHIc7XPwA4ONSpI0qppzzkXAHVX13o5N24FTmvVTgKs7yrckOSjJUbRHL9/cTKX4SJLjm32ePKfN3n29Hvhsc1+x64BXJTkkySHAq5oySZIkSRoIR4ZJktQDST5K+yr4w5NMA+8EDgSoqgtp/0h4WpJZ4O+BLc0PhpIkrYSXA78O7EhyW1P2O8A5wJVJTgW+AbwBoKp2JrkSuB2YBU6vqseadqcBlwBrgWubBdrJtg8n2UV7RNiWZl8PJvk94Jam3rur6sEV6qckSZIk7ZPJMEmSeqCq3riP7e8H3t+ncCRJY66qPk/3e3cBnDBPm7OBs7uU3wq8sEv5d2iSaV22XQxcvNh4JUmSJGklOU2iJEmSJEmSJEmSRpbJMEmSJEmSJEmSJI0sk2GSJEmSJEmSJEkaWSbDJEmSJEmSJEmSNLJMhkmSJEmSJEmSJGlkmQyTJEmSJEmSJEnSyDIZJkmSJEmSJEmSpJFlMkySJEmSJEmSJEkjy2SYJEmSJEmSJEmSRpbJMEmSJEmSJEmSJI0sk2GSJEmSJEmSJEkaWSbDJEmSJEmSJEmSNLL2mQxLcnGS+5N8taPs0CTXJ/l68/eQjm1nJdmV5M4kJ3aUH5tkR7PtvCRpyg9K8rGm/KYkR/a4j5IkSZIkSZIkSRpTixkZdgmwaU7ZmcANVbURuKF5TJKjgS3AC5o2H0xyQNPmAmArsLFZ9u7zVOChqnoe8D7gPUvtjCRJkiRJkiRJktRpn8mwqvoc8OCc4s3Apc36pcDrOsqvqKpHq+puYBdwXJJ1wMFVdWNVFXDZnDZ793UVcMLeUWOSJEmSJEmSJEnScqxZYruJqtoDUFV7kjynKV8PfKGj3nRT9r1mfW753jb3NPuaTfIwcBjwzblPmmQr7dFlTExM0Gq1lhj+YM3MzKza2JdjXPs9sRbOOGZ2IM89yOM9rq/3uPZbkiRJkiRJkobVUpNh8+k2oqsWKF+ozZMLq7YB2wAmJydrampqCSEOXqvVYrXGvhzj2u/zL7+ac3f0+p/a4uw+aWogzwvj+3qPa78lSZIkSZIkaVgt5p5h3dzXTH1I8/f+pnwaOKKj3gbg3qZ8Q5fyJ7RJsgZ4Fk+ellGSJEmSJEmSJEnab0tNhm0HTmnWTwGu7ijfkuSgJEcBG4GbmykVH0lyfHM/sJPntNm7r9cDn23uKyZJkiRJkiRJkiQtyz7nbkvyUWAKODzJNPBO4BzgyiSnAt8A3gBQVTuTXAncDswCp1fVY82uTgMuAdYC1zYLwEXAh5Psoj0ibEtPeiZJkiRJkiRJkqSxt89kWFW9cZ5NJ8xT/2zg7C7ltwIv7FL+HZpkmiRJkiRJkiRJktRLS50mUZIkSZIkSZIkSRp6JsMkSeqBJBcnuT/JV+fZniTnJdmV5CtJXtrvGCVJkiRJkqRxZDJMkqTeuATYtMD2VwMbm2UrcEEfYpIkSZIkSZLGnskwSZJ6oKo+Bzy4QJXNwGXV9gXg2UnW9Sc6SZIkSZIkaXyZDJMkqT/WA/d0PJ5uyiRJkiRJkiStoDWDDkCSpDGRLmXVtWKylfZUikxMTNBqtZb8pGccM7tf9SfW7n+b+Swn7n2ZmZlZcP+96sNSdMa1rziHhXH2lnH21mqJU5IkSZI0vEyGSZLUH9PAER2PNwD3dqtYVduAbQCTk5M1NTW15Cd905mf3q/6Zxwzy7k7evPxYPdJUz3ZTzetVouFjsv+9ruXOvu9rziHhXH2lnH21mqJU5IkSZI0vJwmUZKk/tgOnJy244GHq2rPoIOSJEmSJEmSRp0jwyRJ6oEkHwWmgMOTTAPvBA4EqKoLgWuA1wC7gL8D3jyYSCVJkiRJkqTxYjJMkqQeqKo37mN7Aaf3KRxJkiRJ0jIk2QT8IXAA8KGqOmfO9ingauDupugTVfXufsYoSVo8k2GSJEmSJEmS1EhyAPAB4JW07/98S5LtVXX7nKp/XlWv7XuAkqT95j3DJEmSJEmSJOlxxwG7ququqvoucAWwecAxSZKWwWSYJEmSJEmSJD1uPXBPx+Pppmyun07y5STXJnlBf0KTJC2F0yRKkiRJkiRJ0uPSpazmPP4S8GNVNZPkNcAfAxuftKNkK7AVYGJiglarxczMDK1Wa7+DOuOY2f1u0ytLiXex9nU8RrXf81nq+2MUeSyeyOPxuKUcC5NhkiRJkiRJkvS4aeCIjscbgHs7K1TVtzrWr0nywSSHV9U359TbBmwDmJycrKmpKVqtFlNTU/sd1JvO/PR+t+mV3SdNrdi+93U8RrXf81nq+2MUeSyeyOPxuKUcC6dJlCRJkiRJkqTH3QJsTHJUkqcCW4DtnRWS/EiSNOvH0f6d9YG+RypJWhRHhkmSJEmSJElSo6pmk7wVuA44ALi4qnYmeUuz/ULg9cBpSWaBvwe2VNXcqRQlSUPCkWGSJEmSNGKSXJzk/iRf7Sg7NMn1Sb7e/D2kY9tZSXYluTPJiR3lxybZ0Ww7r+MK+IOSfKwpvynJkR1tTmme4+tJTulTlyVJ6qmquqaqnl9VP15VZzdlFzaJMKrq/VX1gqp6UVUdX1X/c7ARS5IWYjJMkiRJkkbPJcCmOWVnAjdU1UbghuYxSY6mPf3TC5o2H0xyQNPmAmArsLFZ9u7zVOChqnoe8D7gPc2+DgXeCbwMOA54Z2fSTZIkSZIGwWSYJEmSJI2Yqvoc8OCc4s3Apc36pcDrOsqvqKpHq+puYBdwXJJ1wMFVdWMz7dNlc9rs3ddVwAnNqLETgeur6sGqegi4nicn5SRJkiSpr0yGSZIkSdJ4mKiqPQDN3+c05euBezrqTTdl65v1ueVPaFNVs8DDwGEL7EuSJEmSBmbNoAOQJEmSJA1UupTVAuVLbfPEJ0220p6CkYmJCVqt1j4Dnc/EWjjjmNklt1+OfcU9MzOzrL6tlGGNC4xtqYxtaYY1tmGNS5Kk1cpkmCRJkiSNh/uSrKuqPc0UiPc35dPAER31NgD3NuUbupR3tplOsgZ4Fu1pGaeBqTltWt2CqaptwDaAycnJmpqa6lZtUc6//GrO3TGYr7e7T5pacHur1WI5fVspwxoXGNtSGdvSDGtswxqXJEmrldMkSpIkSdJ42A6c0qyfAlzdUb4lyUFJjgI2Ajc3Uyk+kuT45n5gJ89ps3dfrwc+29xX7DrgVUkOSXII8KqmTJIkSZIGxpFhkiRJkjRiknyU9gitw5NMA+8EzgGuTHIq8A3gDQBVtTPJlcDtwCxwelU91uzqNOASYC1wbbMAXAR8OMku2iPCtjT7ejDJ7wG3NPXeXVUPrmBXJUmSJGmfTIZJkiRJ0oipqjfOs+mEeeqfDZzdpfxW4IVdyr9Dk0zrsu1i4OJFBytJkiRJK8xpEiVJkiRJkiRJkjSylpUMS7I7yY4ktyW5tSk7NMn1Sb7e/D2ko/5ZSXYluTPJiR3lxzb72ZXkvGY+ekmSJEmSJEmSJGlZejEy7Ger6sVVNdk8PhO4oao2Ajc0j0lyNO155F8AbAI+mOSAps0FwFbaN2re2GyXJEmSJEmSJEmSlmUlpkncDFzarF8KvK6j/IqqerSq7gZ2AcclWQccXFU3VlUBl3W0kSRJkiRJkiRJkpZsucmwAv4kyReTbG3KJqpqD0Dz9zlN+Xrgno62003Z+mZ9brkkSZIkSZIkSZK0LGuW2f7lVXVvkucA1yf52gJ1u90HrBYof/IO2gm3rQATExO0Wq39DHc4zMzMrNrYl2Nc+z2xFs44ZnYgzz3I4z2ur/e49luSJEmSJEmShtWykmFVdW/z9/4knwSOA+5Lsq6q9jRTIN7fVJ8GjuhovgG4tynf0KW82/NtA7YBTE5O1tTU1HLCH5hWq8VqjX05xrXf519+NefuWG7eeWl2nzQ1kOeF8X29x7XfakuyCfhD4ADgQ1V1zpztU8DVwN1N0Seq6t39jFGSJEmSJEkaN0ueJjHJ05M8c+868Crgq8B24JSm2im0f/SjKd+S5KAkRwEbgZubqRQfSXJ8kgAnd7SRJGlVSHIA8AHg1cDRwBuTHN2l6p9X1YubxUSYJEmSJEmStMKWM1xlAvhkO3/FGuAjVfWZJLcAVyY5FfgG8AaAqtqZ5ErgdmAWOL2qHmv2dRpwCbAWuLZZJElaTY4DdlXVXQBJrgA20z7vSZIkSZIkSRqQJSfDmh/7XtSl/AHghHnanA2c3aX8VuCFS41FkqQhsB64p+PxNPCyLvV+OsmXaU8J/FtVtbMfwUmSpJVx5JmfXnD7GcfM8qZ91Fmq3ef8/IrsV5IkSRo1g7mRkSRJoyddymrO4y8BP1ZVM0leA/wx7WmDn7ijZCuwFWBiYoJWq7XkoM44Zna/6k+s3f8281lO3PsyMzOz4P571Yel6IxrX3EOC+PsLePsrdUSpyRJkiRpeJkMkySpN6aBIzoeb6A9+usHqupbHevXJPlgksOr6ptz6m0DtgFMTk7W1NTUkoPa3yvRzzhmlnN39Objwe6Tpnqyn25arRYLHZeVugJ/MTr7va84h4Vx9pZx9tZqiVOSJEmSNLyeMugAJEkaEbcAG5McleSpwBZge2eFJD+S5mabSY6jfR5+oO+RSpIkSZIkSWPEkWGSJPVAVc0meStwHXAAcHFV7Uzylmb7hcDrgdOSzAJ/D2ypqrlTKUrS0NvXPZJ6ae79lrxHkiRJkiRpf5kMkySpR6rqGuCaOWUXdqy/H3h/v+OSJEmSJEmSxpnTJEqSJEmSJEmSJGlkmQyTJEmSJEmSJEnSyDIZJkmSJEmSJEmSpJFlMkySJEmSJEmSJEkjy2SYJEmSJEmSJEmSRpbJMEmSJEmSJEmSJI0sk2GSJEmSJEmSJEkaWSbDJEmSJEmSJEmSNLJMhkmSJEmSJEmSJGlkmQyTJEmSJEmSJEnSyDIZJkmSJEmSJEmSpJFlMkySJEmSJEmSJEkjy2SYJEmSJEmSJEmSRpbJMEmSJEmSJEmSJI0sk2GSJEmSJEmSJEkaWSbDJEmSJEmSJEmSNLJMhkmSJEmSJEmSJGlkmQyTJEmSJEmSJEnSyDIZJkmSJEmSJEmSpJG1ZtABSJIkSZKk/XfkmZ9ectszjpnlTctov/ucn19yW0mSJKnfHBkmSZIkSZIkSZKkkeXIMEmSJEmSJEmSpDG0nNkGeqFfMw4MzciwJJuS3JlkV5IzBx2PJEn7a1/nsrSd12z/SpKXDiJOSZL6we94kqTVzO93kjRahiIZluQA4APAq4GjgTcmOXqwUUmStHiLPJe9GtjYLFuBC/oapCRJfeJ3PEnSaub3O0kaPUORDAOOA3ZV1V1V9V3gCmDzgGOSJGl/LOZcthm4rNq+ADw7ybp+BypJUh/4HU+StJr5/U6SRsyw3DNsPXBPx+Np4GUDikWSpKVYzLmsW531wJ6VDU2SpL7zO96IW8l7S5xxzCxvWmD//bqvhKSx5vc7SX21mM9W+/qMpIUNSzIsXcrqSZWSrbSHHQPMJLlzRaNaOYcD3xx0EANgv/ss7xnEs/6Ar/fq82ODDmCVW8y5bOjPd7/Zw/fwCv8fNLT/1ub0e2jjnMM4e8s4e2ju/0s9+L/F811/7POc1+Pz3dC+n3t5bu2lYY0L9h2b33PmZWxLM6yxLTcuz3fLs9Lf74b1fTcvv9/11dAejwHwWHQY5s9vy7HEf2d7j8Wiz3fDkgybBo7oeLwBuHduparaBmzrV1ArJcmtVTU56Dj6zX6PF/utMbSYc9nQn+9Wy3vYOHvLOHvLOHtrtcSpJ9nnOa+X57thfp8Ma2zDGhcY21IZ29IMa2zDGtcYWdHvd76+T+TxeCKPx+M8Fk/k8XjcUo7FsNwz7BZgY5KjkjwV2AJsH3BMkiTtj8Wcy7YDJ6fteODhqnIKDUnSKPI7niRpNfP7nSSNmKEYGVZVs0neClwHHABcXFU7BxyWJEmLNt+5LMlbmu0XAtcArwF2AX8HvHlQ8UqStJL8jidJWs38fidJo2cokmEAVXUN7ZPIOFj1Uz0ukf0eL/ZbY6fbuaz5krR3vYDT+x3Xflot72Hj7C3j7C3j7K3VEqfm6PN3vGF+nwxrbMMaFxjbUhnb0gxrbMMa19hY4e93vr5P5PF4Io/H4zwWT+TxeNx+H4u0/9+WJEmSJEmSJEmSRs+w3DNMkiRJkiRJkiRJ6jmTYT2U5NlJrkrytSR3JPnpJO9K8tdJbmuW1yy2bb/jX6pl9vv/TLIzyVeTfDTJ0/od/1LN95ol+Y0kdzb9+v152m5q6uxKcmZ/I1++pfY9yRFJ/rRpszPJ2/of/dIt5zVv6h2Q5H8l+VT/opbm/X/60CTXJ/l68/eQOW1+NMlMkt/qKDs2yY7m/67zkmRQcSY5Msnfd5xnLuzYz9DE2dT/R0lubP6P2LH3XDdMcSY5qeNY3pbk+0lePIRxHpjk0iaeO5Kc1bGfYYrzqUn+axPPl5NMDTjONzTvv+8nmZxT/6wmljuTnDiMcSY5LO3PDzNJ3j9nPysap4ZfkouT3J/kq4OOpVOG+HNvkqclubn5/2lnkt8ddExzZUg/NyfZ3fyfc1uSWwcdT6du/68OQUw/MefzxbeSvH3Qce2VIf49Isnbmrh2DtMxU29klf8m1GvD/H/rSuv2OWa+7xjjYJ7jsajfmEfNfJ8lx/H9scCx2P/3RlW59GgBLgX+ZbP+VODZwLuA31pK20H3Z6X7DawH7gbWNo+vBN406P4ss98/C/wP4KCm/Dld2h0A/CXwD5p2XwaOHnR/+tT3dcBLm/VnAn+xmvq+1H53tP+3wEeATw26Ly7jtczz3v194Mym7EzgPXPafBz4b53/lwM3Az8NBLgWePWg4gSOBL46z36GKc41wFeAFzWPDwMOGLY457Q7BrhrSI/nrwJXNOs/BOwGjhzCOE8H/muz/hzgi8BTBhjnTwI/AbSAyY66R9P+HHIQcBTtzyeDfH/OF+fTgX8CvAV4/5z9rGicLsO/AD8DvJR5zgkDjGtoP/c2/16e0awfCNwEHD/ouObEOJSfm5vzzuGDjmOe2Ib6NwXa34P/N/Bjg46liWdof48AXgh8lfZnnTW0v29uHHRcLj17fVf9b0IrcEyG9v/WPvT9SZ9jWMR3tlFd5jke72IRv62P2jLfZ8lxfH8scCz2+73hyLAeSXIw7X+wFwFU1Xer6m9Xuu2g9SD2NcDaJGtof9C7t+dBroAF+n0acE5VPdqU39+l+XHArqq6q6q+C1wBbO5L4D2wnL5X1Z6q+lKz/ghwB+0vIUNvma85STYAPw98qC8BS40F3rubaf9oQvP3dR1tXgfcBezsKFsHHFxVN1b7E8hlnW0GEec8+xm2OF8FfKWqvtzUf6CqHhvCODu9Efhos59hi7OApzefG9YC3wW+NYRxHg3c0NS9H/hbYHJQcVbVHVV1Z5cmm2knFx+tqruBXcBxwxZnVX27qj4PfGfOflY0Tq0OVfU54MFBxzHXMH/urbaZ5uGBzTI0NxP3c/P+WyW/KZwA/GVV/dWgA+kwrL9H/CTwhar6u6qaBf4M+MUBx6TeWdW/Cam35vkcs1/fgUfJsH6uG4QFPkuO3fujl5+rTYb1zj8A/gb4r810Dh9K8vRm21uTfKUZ6tlt6OJCbYfdkvtdVX8N/GfgG8Ae4OGq+pO+Rb488/X7+cA/TXJTkj9L8lNd2q4H7ul4PM2QfDFepOX0/QeSHAm8hPaVqKvBcvv9B8BvA9/vT7jSD8z33p2oqj3Q/mBBe/QKzbZ3AHOnTFpP+/+rvXr9f9d+xdk4qqn7Z0n+6ZDG+XygklyX5EtJfntI4+z0KzTJsCGM8yrg27Q/N3wD+M9V9eAQxvllYHOSNUmOAo4FjhhgnPOZ7zPJsMU5n5WOU+qJYfzcm/Y0hLcB9wPXV9XQxMZwf24u4E+SfDHJ1kEH02E1/Kawhcc/XwzckP8e8VXgZ9KeJviHgNfQ/hyh0bDafxNaCcP6f+ugLOY727jZ12/rI23OZ8mxfn90+Vy9X+8Nk2G9s4b2MM4LquoltH+oORO4APhx4MW0P2Cdux9tV4Ml97t5g26mPS3Pc2lf6f1r/Ql72ebr9xrgEOB44N8BVyZPundFt3tZDM2VmIuwnL4DkOQZtKdge3tVfasvUS/fkvud5LXA/VX1xf6GLAH7f475XeB9HVeM77XS/3ftb5x7gB9t6v5b4CPNVdHDFuca2tO7ndT8/cUkJwxhnAAkeRnwd1W1d472YYvzOOAx2p8bjgLOSPIPhjDOi2n/sHEr7R91/ycwO4RxzhfPsMU5n9X+mUpjYFg/91bVY1X1YmAD7RGhLxxwSMCq+Nz88qp6KfBq4PQkPzPogBpD/ZtCkqcCv0B7Cu6hMMy/R1TVHcB7gOuBz9C+yGZ2oEGpl/z88mTD+n+rhsNiflsfWcP6WXIQuhyL/X5vmAzrnWlguuOKuqtoz2V5X/NF4/vAf6H9I86i2q54xL2xnH7/HHB3Vf1NVX0P+ATwj/sS9fLN95pNA59oph65mfbVjId3adt5VdcGhmc6hsVYTt9JciDt/7gur6pP9CnmXlhOv18O/EKS3bSnQHhFkj/qT9jSvO/d+5opxvZONbZ3is+XAb/fvF/fDvxOkrc2+9nQsd9e/9+1X3E207o90Kx/kfa8+88ftjib+n9WVd+sqr8DruHx/zuGKc695l61PWxx/irwmar6XrWnH/x/gclhi7OqZqvq/6yqF1fVZtr3w/r6AONcqH63zyTDFudC+1nJOKVlWQ2fe5up9FrApsFG8gND/bm5qu5t/t4PfJLu33MHYdh/U3g18KWqum/QgXQY6t8jquqiqnppVf0M7SnDvj7omNQzq/03oZ4b4v9bB2Vf39nGyiJ/Yx5J83yWHMv3R7djsZT3hsmwHqmq/w3ck+QnmqITgNv3vjkbv0h7uPui2q5kvL2ynH7Tno7g+CQ/1IykOYH2nJ9Db4HX7I+BVwAkeT7tm6F+c07zW4CNSY5qrpDbAmzvR9y9sJy+N6/zRcAdVfXefsXcC8vpd1WdVVUbqupI2q/3Z6tqKK461Ohb4L27HTilKTsFuLqp/0+r6sjm/foHwH+sqvc3w+8fSXJ882/55L1tBhFnkh9OckCz/g+AjcBdwxYncB3wj5pz3RrgnwG3D2GcJHkK8AbaPz7u3c+wxfkN2j+Mppn+6Xjga8MWZ/N6P71ZfyUwW1WDfN3nsx3YkuSgtKdz3AjcPIRxzrefFY1TWo5h/tzbnEOf3ayvpZ0U+NpAg2oM8+fmJE9P8sy967TvC9rte27frYLfFH5wP9IhMtS/RyTZO4X5jwL/guE7flq6Vf2bUK8N8/+tAzTvd7ZxtMjfmEfOAp8lx+79Md+xWNJ7o6pcerTQHpJ3K/AV2j+QHwJ8GNjRlG0H1jV1nwtcs1DbQfenT/3+XdpfvL7atDlo0P1ZZr+fCvxR058vAa+Yp9+vAf6C9kiGfz/ovvSr77SnCKum3W3N8ppB96cfr3nHPqaATw26Ly7jtczz3j0MuIH2VaY3AId2afcu4Lc6Hk827/W/BN4PZFBxAr8E7KQ9bcyXgH8+jHE29X+tifWrwO8PcZxTtG/WPnc/QxMn8AzaUyztpP0j378b0jiPBO6k/aPa/wB+bMBx/iLtq5AfBe4Druuo/++bWO4EXj3Ece6mfWX8TFPn6H7E6TL8C+0fiPcA32veG6cOOqYmrqH93Av8I+B/NbF9Ffi/Bx3TPHFOMUSfm2nfl+vLzbKTIfse1+3/1UHH1MT1Q8ADwLMGHUuX2Ib29wjgz2l/1vkycMKg43Hp+eu7qn8T6vGxGOr/W/vQ/yd9jmER39VHdZnneHT9jXnUl/k+S47j+2OBY7Hf7400O5QkSZIkSZIkSZJGjtMkSpIkSZIkSZIkaWSZDJMkSZIkSZIkSdLIMhkmSZIkSZIkSZKkkWUyTJIkSZIkSZIkSSPLZJgkSZIkSZIkSZJGlskwSZIkSZIkSZIkjSyTYZIkSZIkSZIkSRpZJsMkSZIkSZIkSZI0skyGSZIkSZIkSZIkaWSZDJMkSZIkSZIkSdLIMhkmSZIkSZIkSZKkkWUyTJIkSZIkSZIkSSPLZJgkSZIkSZIkSZJGlskwSZIkSZIkSZIkjSyTYZIkSZIkSZIkSRpZJsMkSZIkSZIkSZI0skyGSZIkSZIkSZIkaWSZDJMkSZIkSZIkSdLIMhkmSZIkSZIkSZKkkWUyTJIkSZIkSZIkSSPLZJgkSZIkSZIkSZJGlskwSZIkSZIkSZIkjSyTYZIkSZIkSZIkSRpZJsMkSZIkSZIkSZI0skyGSZIkSZIkSZIkaWSZDJMkSZIkSZIkSdLIMhkmSZIkSZIkSZKkkWUyTJIkSZIkSZIkSSPLZJgkSZIkSZIkSZJGlskwSZIkSZIkSZIkjSyTYZIkSZIkSZIkSRpZJsMkSZIkSZIkSZI0skyGSZIkSZIkSZIkaWSZDJMkSZIkSZIkSdLIMhkmSZIkSZIkSZKkkWUyTJIkSZIkSZIkSSPLZJgkSZIkSZIkSZJGlskwSZIkSZIkSZIkjSyTYZIkSZIkSZIkSRpZJsMkSZIkSZIkSZI0skyGSZIkSZIkSZIkaWSZDJMkSZIkSZIkSdLIMhkmSZIkSZIkSZKkkWUyTJIkSZIkSZIkSSPLZJgkSZIkSZIkSZJGlskwSZIkSZIkSZIkjSyTYZIkSZIkSZIkSRpZJsOkFZakleRfDjoOSZLmM6hzVZLfSfKhBbbvTvJz/YxJkjQ+PP9JksaB5zupzWSYtAokmUoyPeg4JEmjL8mbkny+H89VVf+xqhb1pSzJC5Ncl+SbSarL9kOTfDLJt5P8VZJf7X3EkqRRNcTnv1OSfDHJt5JMJ/n9JGtWOkZJ0mga4vPdhUlmOpZHkzyy0jFqvJgMkyRJ0mrwPeBK4NR5tn8A+C4wAZwEXJDkBX2KTZKklfJDwNuBw4GXAScAvzXIgCRJ6rWqektVPWPvAnwU+G+DjkujxWSY1KEZnntWktuTPJTkvyZ5WrPtXyXZleTBJNuTPLej3T9OckuSh5u//3iB5/hXSe5I8kjzPC/teO7fSvKVZj8fS/K0JE8HrgWe23F1xHPn278kabSt5LkqyU8CFwI/3Zxv/jbJTyW5r/Mq9CS/lOS2Zv1dSa5qzluPJPlSkhd11H1uko8n+Zskdyf5zY5t70ryRx2Pf70Z1fVAkn/fGVtV3VlVFwE7u8T9dOCXgP9QVTNV9XlgO/Dr+3+EJUnDaIzPfxdU1Z9X1Xer6q+By4GX9+CQSpKG0Lie7+bEuff73aVLPIxSVybDpCc7CTgR+HHg+cD/leQVwP8f+GVgHfBXwBXQnpYJ+DRwHnAY8F7g00kOm7vjJG8A3gWcDBwM/ALwQEeVXwY2AUcB/wh4U1V9G3g1cG/HFRL39rjPkqTVZUXOVVV1B/AW4MbmfPPsqrqF9rnqlR1Vfw34cMfjzbSv2jsU+Ajwx0kOTPIU4L8DXwbW076a/e1JTpzboSRHAxfQTmA9t4lzwyKPx/OBx6rqLzrKvgw4MkySRovnP/gZulwYIkkaKeN+vvsl4G+Azy18mKT9s6qTYUkuTnJ/kq8usv4vN1n1nUk+stLxadV6f1XdU1UPAmcDb6R9Erq4qr5UVY8CZ9G+iuJI4OeBr1fVh6tqtqo+CnwN+Odd9v0vgd+vqluqbVdV/VXH9vOq6t7muf878OIV66UkaTVbyXNVN5fS/kK094vWibS/BO31xaq6qqq+R/uL19OA44GfAn64qt7dXNF+F/BfgC1dnuP1wKeq6nNN/P8B+P4i43sG8PCcsoeBZy6yvSRpdRjr81+SNwOTwH9eZPyShpS/aWofxvp8B5wCXFZVT7pXtLQcq/2mq5cA7wcu21fFJBtp/yfx8qp6KMlzVjg2rV73dKz/Fe2rFZ4LfGlvYVXNJHmA9lUPz23qMafd+i77PgL4ywWe+393rP9ds29JkuZayXNVN38E3JHkGbSvRPzzqtrTLZ6q+n6S6eY5i/Y0v3/bUfcA4M+7PMdz5+zn2038izFDe8R1p4MBb7gsSaNlbM9/SV4HnAP8XFV9c5HxSxpel+BvmprfOJ/vjgD+GfCvFhm7tGiremRYVX0OeLCzLMmPJ/lMki8m+fMk/7DZ9K+AD1TVQ03b+/scrlaPIzrWfxS4t1l+bG9hM3ftYcBfz93W0e6vu+z7HtpDnPeXV0JIkjqt5LnqSeec5h4lNwK/SHtaiw/PqfKDeJqpMjY0z3kPcHcz/cbe5ZlV9Zouz7tnzn5+qIl/Mf4CWNP8ULDXi3AaKUkaNWN5/kuyifaV9v+8qnZ02YekVcbfNLUPY3m+a5wM/M9mlJnUU6s6GTaPbcBvVNWxwG8BH2zKnw88P8n/m+QLzYdJqZvTk2xohgX/DvAx2kOD35zkxUkOAv4jcFNV7Qauof3e+tUka5L8CnA08Kku+/4Q8FtJjk3b85LMPVl1cx9wWJJn9aB/kqTVbyXPVfcBG5I8dU75ZcBvA8cAn5yz7dgk/yLtmy6/HXgU+AJwM/CtJO9IsjbJAUlemOSnujzvVcBrk/yT5rnfTcdn1ea8+TTgqc3jpzX9pLm/5ieAdyd5epKX057Xfu6XOEnS6jaO579XAJcDv1RVNy/uMElapfxNU3uN3fmuw8m0R05KPTdSybBmKOc/Bv5bktuA/4f2DQWhPSXkRmCK9jyrH0ry7P5HqVXgI8CfAHc1y/+vqm6gPZftx2lfyfDjNPPfVtUDwGuBM2jfcPK3gdd2m7qiqv4b7bl+P0J76qY/pn3zyQVV1deAjwJ3JfnbJE6fKEnjbcXOVcBnaY+o+t9JOrd/kvbVhp9skk+drgZ+BXiI9pWE/6KqvldVj9Gep/7FwN3AN2lfGPKkizuqaidwetO3Pc2+pjuq/Bjw9zw+2uvvgTs7tv8bYC1wP+1z5mnNPiVJo2Mcz3//oWl3TZKZZrl24cMkabXxN03NMY7nO5L8NO1RZ/9twaMjLVFW+33o0r5J4Keq6oVJDgburKp1XepdCHyhqi5pHt8AnFlVt/QzXg23JLuBf1lV/2PQsUiS1M0gz1VJ/hL4153PneRdwPOq6tf6HY8kaXx4/pM0avxNU914vpNWzkiNDKuqbwF3J3kD/GA6nRc1m/8Y+Nmm/HDaQ4yde1SSJGkRkvwS7fnlPzvoWCRJ6hfPf5L6wd80NWie7zQO1gw6gOVI8lHaQ4QPTzINvBM4Cbggyf8FHAhcAXwZuA54VZLbgceAf9cMIZUkSdICkrRozzn/61X1/QGHI0lSX3j+k7RS/E1Tw8TzncbFqp8mUZIkSZIkSZIkSZrPSE2TKEmSJEmSJEmSJHUyGSZJkiRJkiRJkqSRtWrvGXb44YfXkUceOegwluTb3/42T3/60wcdRt/Z7/Fiv1efL37xi9+sqh8edBx6Is93q4/9Hi/2e/XxfDecPN+tPvZ7vNjv1cfz3XDyfLf62O/xYr9Xn/05363aZNiRRx7JrbfeOugwlqTVajE1NTXoMPrOfo8X+736JPmrQcegJ/N8t/rY7/Fiv1cfz3fDyfPd6mO/x4v9Xn083w0nz3erj/0eL/Z79dmf853TJEqSJEmSJEmSJGlkmQyTJEmSJEmSJEnSyDIZJkmSJEmSJEmSpJFlMkySJEmSJEmSJEkjy2SYJEmSJEmSJHVIcnGS+5N8dZ7tSXJekl1JvpLkpf2OUZK0eCbDJEmSJEmSJOmJLgE2LbD91cDGZtkKXNCHmCRJS2QyTJIkSZIkSZI6VNXngAcXqLIZuKzavgA8O8m6/kQnSdpfJsMkSZIkSZIkaf+sB+7peDzdlEmShtCaQQcgSZIkSZIkSatMupTVkyolW2lPo8jExAStVmuFw1oZMzMzqzb25bDf48V+jzaTYZIkSZIkSZK0f6aBIzoebwDunVupqrYB2wAmJydramqqL8H1WqvVYrXGvhz2e7zY79FmMkxaYTv++mHedOanB/Lcu8/5+YE8ryRp/Hi+kySNA893kjpsB96a5ArgZcDDVbVnwDFJPeH5TqPIZJgkSZIkSZIkdUjyUWAKODzJNPBO4ECAqroQuAZ4DbAL+DvgzYOJVJK0GCbDJEmSJEmSJKlDVb1xH9sLOL1P4UiSlukpgw5AkiRJkiRJkiRJWikmwyRJkiRJkiRJkjSyTIZJkiRJkiRJkiRpZJkMkyRJkiRJkiRJ0sgyGSZJkiRJkiRJkqSRZTJMkiRJkiRJkiRJI8tkmCRJkiRJkiRJkkbWiifDklyc5P4kX51ne5Kcl2RXkq8keelKxyRJkiRJkiRJkqTx0I+RYZcAmxbY/mpgY7NsBS7oQ0ySJEmSJEmSJEkaAyueDKuqzwEPLlBlM3BZtX0BeHaSdSsdlyRJkiRJkiRJkkbfMNwzbD1wT8fj6aZMkiRJkiRJkiRJWpY1gw4ASJey6lox2Up7KkUmJiZotVorGNbKmZmZWbWxL8e49ntiLZxxzOxAnnuQx3tcX+9x7bckSZIkSZIkDathSIZNA0d0PN4A3NutYlVtA7YBTE5O1tTU1IoHtxJarRarNfblGNd+n3/51Zy7YzD/1HafNDWQ54Xxfb3Htd+jIslu4BHgMWC2qiaTHAp8DDgS2A38clU91NQ/Czi1qf+bVXVdU34s7XtmrgWuAd5WVZXkIOAy4FjgAeBXqmp3n7onSZIkSZIkjaVhmCZxO3By2o4HHq6qPYMOSpI0tn62ql5cVZPN4zOBG6pqI3BD85gkRwNbgBcAm4APJjmgaXMB7ZHMG5tlU1N+KvBQVT0PeB/wnj70R5KkJ0jy7CRXJflakjuS/HSSQ5Ncn+Trzd9DOuqflWRXkjuTnNhRfmySHc2285KkKT8oycea8puSHDmAbkqSJEnSD6x4MizJR4EbgZ9IMp3k1CRvSfKWpso1wF3ALuC/AP9mpWOSJGk/bAYubdYvBV7XUX5FVT1aVXfTPo8dl2QdcHBV3VhVRXsk2Ou67Osq4IS9PxxKktRHfwh8pqr+IfAi4A68+EOSJEnSCFvxuduq6o372F7A6SsdhyRJi1DAnyQp4P9ppued2Dtiuar2JHlOU3c98IWOttNN2fea9bnle9vc0+xrNsnDwGHAN1eoP5IkPUGSg4GfAd4EUFXfBb6bZDMw1VS7FGgB76Dj4g/g7iR7L/7YTXPxR7PfvRd/XNu0eVezr6uA9ydJ891PkiRJkvpuGO4ZJknSsHh5Vd3bJLyuT/K1Bep2G9FVC5Qv1OaJO0620r7SnomJCVqt1oJBD6uZmZlVG/tyjGu/J9bCGcfMDuS5B3m8x/X1Htd+j4h/APwN8F+TvAj4IvA2vPhDkiRJ0ggzGSZJUqOq7m3+3p/kk8BxwH1J1jU/DK4D7m+qTwNHdDTfANzblG/oUt7ZZjrJGuBZwINd4tgGbAOYnJysqamp3nSwz1qtFqs19uUY136ff/nVnLtjMB8td580NZDnhfF9vce13yNiDfBS4Deq6qYkf0gzJeI8vPhjH8Y1OTyu/fbij/Eyrv2WJGkUmQyTJAlI8nTgKVX1SLP+KuDdwHbgFOCc5u/VTZPtwEeSvBd4Lu17pdxcVY8leSTJ8cBNwMnA+R1tTqF9L83XA591yihJUp9NA9NVdVPz+CrayTAv/liicU0Oj2u/vfhjvIxrvyVJGkVPGXQAkiQNiQng80m+DNwMfLqqPkM7CfbKJF8HXtk8pqp2AlcCtwOfAU6vqseafZ0GfAjYBfwl7funAFwEHNbcb+XfsvCV+JIk9VxV/W/gniQ/0RSdQPtctveCDXjyxR9bkhyU5Cgev/hjD/BIkuOThPbFH51t9u7Liz8kSZIkDZwjwyRJAqrqLuBFXcofoP1DYbc2ZwNndym/FXhhl/LvAG9YdrCSJC3PbwCXJ3kqcBfwZtoXSl6Z5FTgGzTnq6ramWTvxR+zPPnij0uAtbQv/Oi8+OPDzcUfDwJb+tEpSZIkSZqPyTBJkiRJGiNVdRsw2WWTF39IkiRJGklOkyhJkiRJkiRJkqSRZTJMkiRJkiRJkiRJI8tkmCRJkiRJkiRJkkaWyTBJkiRJkiRJkiSNLJNhkiRJkiRJkiRJGlkmwyRJkiRJkiRJkjSyTIZJkiRJkiRJkiRpZJkMkyRJkiRJkiRJ0sgyGSZJkiRJkiRJkqSRZTJMkiRJkiRJkiRJI8tkmCRJkiRJkiRJkkaWyTBJkiRJkiRJkiSNLJNhkiRJkiRJkiRJGlkmwyRJkiRJkiRJkjSyTIZJkiRJkiRJkiRpZJkMkyRJkiRJkiRJ0sgyGSZJkiRJkiRJkqSRZTJMkiRJkiRJkiRJI8tkmCRJkiRJkiRJkkaWyTBJkiRJkiRJkiSNLJNhkiRJkiRJktQhyaYkdybZleTMLtufleS/J/lykp1J3jyIOCVJi2MyTJIkSZIkSZIaSQ4APgC8GjgaeGOSo+dUOx24vapeBEwB5yZ5al8DlSQtmskwSZIkSZIkSXrcccCuqrqrqr4LXAFsnlOngGcmCfAM4EFgtr9hSpIWy2SYJEmSJEmSJD1uPXBPx+PppqzT+4GfBO4FdgBvq6rv9yc8SdL+WjPoACRJkiRJkiRpiKRLWc15fCJwG/AK4MeB65P8eVV96wk7SrYCWwEmJiZotVo9D7YfZmZmVm3syzGu/Z5YC2ccM5iBjoM83uP6eo9Lv02GSZIkSZIkSdLjpoEjOh5voD0CrNObgXOqqoBdSe4G/iFwc2elqtoGbAOYnJysqamplYp5RbVaLVZr7Msxrv0+//KrOXfHYFIHu0+aGsjzwvi+3uPSb6dJlCRJkiRJkqTH3QJsTHJUkqcCW4Dtc+p8AzgBIMkE8BPAXX2NUpK0aI4MkyRJkiRJkqRGVc0meStwHXAAcHFV7Uzylmb7hcDvAZck2UF7WsV3VNU3Bxa0JGlBJsMkSZIkSZIkqUNVXQNcM6fswo71e4FX9TsuSdLSOE2iJEmSJEmSJEmSRpbJMEmSJEkaI0l2J9mR5LYktzZlhya5PsnXm7+HdNQ/K8muJHcmObGj/NhmP7uSnJckTflBST7WlN+U5Mi+d1KSJEmSOpgMkyRJkqTx87NV9eKqmmwenwncUFUbgRuaxyQ5GtgCvADYBHwwyQFNmwuArcDGZtnUlJ8KPFRVzwPeB7ynD/2RJEmSpHmZDJMkSZIkbQYubdYvBV7XUX5FVT1aVXcDu4DjkqwDDq6qG6uqgMvmtNm7r6uAE/aOGpMkSZKkQehLMizJpmZKjV1Jzuyy/VlJ/nuSLyfZmeTN/YhLkiRJksZQAX+S5ItJtjZlE1W1B6D5+5ymfD1wT0fb6aZsfbM+t/wJbapqFngYOGwF+iFJkiRJi7JmpZ+gmULjA8AraX9BuiXJ9qq6vaPa6cDtVfXPk/wwcGeSy6vquysdnyRJkiSNmZdX1b1JngNcn+RrC9TtNqKrFihfqM0Td9xOxG0FmJiYoNVqLRj0sJqZmVm1sS/HuPZ7Yi2ccczsQJ57kMd7XF/vce23JEmjaMWTYcBxwK6qugsgyRW0p83oTIYV8Mxm6oxnAA8Cg/l0KUmSJEkjrKrubf7en+STtL+z3ZdkXVXtaaZAvL+pPg0c0dF8A3BvU76hS3lnm+kka4Bn0f6ONzeObcA2gMnJyZqamupNB/us1WqxWmNfjnHt9/mXX825O/rxU8qT7T5paiDPC+P7eo9rvyVJGkX9+ATXbVqNl82p835gO+0vT88EfqWqvj93R145uLqNa7+9cnC8jGu/R0kzovlW4K+r6rVJDgU+BhwJ7AZ+uaoeauqeBZwKPAb8ZlVd15QfC1wCrAWuAd5WVZXkINr3VDkWeID2+W533zonSRp7SZ4OPKWqHmnWXwW8m/b3sVOAc5q/VzdNtgMfSfJe4LnARuDmqnosySNJjgduAk4Gzu9ocwpwI/B64LPNfcUkSZIkaSD6kQxbzBQZJwK3Aa8Afpz2VB1/XlXfekIjrxxc1ca13145OF7Gtd8j5m3AHcDBzeMzgRuq6pzmvpdnAu9IcjSwBXgB7R8H/0eS51fVY8AFtC/e+ALtZNgm4FraibOHqup5SbYA7wF+pX9dkySJCeCT7Uk5WAN8pKo+k+QW4MokpwLfAN4AUFU7k1xJe2aPWeD05lwHcBqPX/xxbbMAXAR8OMku2iPCtvSjY5IkSZI0n378Qj/ftBqd3gyc01wtuCvJ3cA/BG7uQ3ySJAGQZAPw88DZwL9tijcDU836pUALeEdTfkVVPQrc3fzgd1yS3cDBVXVjs8/LgNfR/oFwM/CuZl9XAe9PEq+WlyT1SzN9/Yu6lD8AnDBPm7Npnxvnlt8KvLBL+XdokmmSJEmSNAye0ofnuAXYmOSoJE+lfVXg9jl1vkHzxSvJBPATwF19iE2SpE5/APw20DlV70RV7QFo/j6nKe82DfD6ZpnuUv6ENlU1CzwMHNbTHkiSJEmSJEl6ghUfGVZVs0neClwHHABc3Ey18ZZm+4XA7wGXJNlBe1rFd1TVN1c6NkmS9kryWuD+qvpikqnFNOlSVguUL9RmbizeI3MVG9d+e4/M8TKu/ZYkSZIkrU59uZFRVV1D+54pnWUXdqzfS/vGzZIkDcrLgV9I8hrgacDBSf4IuC/Juqrak2QdcH9Tf75pgKeb9bnlnW2mk6wBnkX7XipP4D0yV7dx7bf3yBwv49pvSZIkSdLq1I9pEiVJGnpVdVZVbaiqI2lP6fvZqvo12lP7ntJUOwW4ulnfDmxJclCSo4CNwM3NVIqPJDk+SYCT57TZu6/XN8/h/cIkSZIkSZKkFTSYy3clSVo9zgGuTHIq7XtcvgGgmfL3SuB2YBY4vaoea9qcBlwCrAWubRaAi4APJ9lFe0TYln51QpIkSZIkSRpXJsMkSZqjqlpAq1l/ADhhnnpnA2d3Kb8VeGGX8u/QJNMkSZIkSZIk9YfTJEqSJEmSJEmSJGlkmQyTJEmSJEmSJEnSyDIZJkmSJEmSJEmSpJFlMkySJEmSJEmSJEkjy2SYJEmSJEmSJEmSRpbJMEmSJEmSJEmSJI0sk2GSJEmSJEmSJEkaWSbDJEmSJEmSJEmSNLJMhkmSJEmSJEmSJGlkmQyTJEmSJEmSJEnSyDIZJkmSJEmSJEmSpJFlMkySJEmSJEmSJEkjy2SYJEmSJEmSJEmSRpbJMEmSJEmSJEmSJI0sk2GSJEmSJEmSJEkaWSbDJEmSJEmSJEmSNLJMhkmSJEmSJEmSJGlkmQyTJEmSJEmSJEnSyDIZJkmSJEmSJEmSpJFlMkySJEmSJEmSJEkjy2SYJEmSJEmSJEmSRpbJMEmSJEmSJEmSJI0sk2GSJEmSJEmSJEkaWSbDJEmSJGnMJDkgyf9K8qnm8aFJrk/y9ebvIR11z0qyK8mdSU7sKD82yY5m23lJ0pQflORjTflNSY7sewclSZIkqYPJMEmSJEkaP28D7uh4fCZwQ1VtBG5oHpPkaGAL8AJgE/DBJAc0bS4AtgIbm2VTU34q8FBVPQ94H/Cele2KJEmSJC3MZJgkSZIkjZEkG4CfBz7UUbwZuLRZvxR4XUf5FVX1aFXdDewCjkuyDji4qm6sqgIum9Nm776uAk7YO2pMkiRJkgbBZJgkSZIkjZc/AH4b+H5H2URV7QFo/j6nKV8P3NNRb7opW9+szy1/QpuqmgUeBg7raQ8kSVphSTY1UwTvSnLmPHWmktyWZGeSP+t3jJKkxVsz6AAkSZIkSf2R5LXA/VX1xSRTi2nSpawWKF+ozdxYttKeZpGJiQlardYiwhk+MzMzqzb25RjXfk+shTOOmR3Icw/yeI/r6z2u/Vb73prAB4BX0r7g45Yk26vq9o46zwY+CGyqqm8keU7XnUmShoLJMEmSJEkaHy8HfiHJa4CnAQcn+SPgviTrqmpPMwXi/U39aeCIjvYbgHub8g1dyjvbTCdZAzwLeHBuIFW1DdgGMDk5WVNTU73pYZ+1Wi1Wa+zLMa79Pv/yqzl3x2B+Stl90tRAnhfG9/Ue134LgOOAXVV1F0CSK2hPA3x7R51fBT5RVd8AqKr7n7QXSdLQMBkmSZIkSWOiqs4CzoL21E7Ab1XVryX5T8ApwDnN36ubJtuBjyR5L/BcYCNwc1U9luSRJMcDNwEnA+d3tDkFuBF4PfDZ5r5ikiStFt2mCX7ZnDrPBw5M0gKeCfxhVV02d0eOhF7dxrXfjoQeL+PSb5NhkiRJkqRzgCuTnAp8A3gDQFXtTHIl7SvhZ4HTq+qxps1pwCXAWuDaZgG4CPhwkl20R4Rt6VcnJEnqkcVM+bsGOBY4gfa58MYkX6iqv3hCI0dCr2rj2m9HQo+Xcem3yTBJkiRJGkNV1QJazfoDtH/M61bvbODsLuW3Ai/sUv4dmmSaJEmr1HzTBM+t882q+jbw7SSfA14E/AWSpKHzlEEHIEmSJEmSJElD5BZgY5KjkjyV9ijn7XPqXA380yRrkvwQ7WkU7+hznJKkRXJkmCRJkiRJkiQ1qmo2yVuB64ADgIubqYPf0my/sKruSPIZ4CvA94EPVdVXBxe1JGkhJsMkSZIkSZIkqUNVXQNcM6fswjmP/xPwn/oZlyRpafoyTWKSTUnuTLIryZnz1JlKcluSnUn+rB9xSZIkSZIkSZIkabSt+MiwJAcAHwBeSfvGkrck2V5Vt3fUeTbwQWBTVX0jyXNWOi5JkiRJkiRJkiSNvn6MDDsO2FVVd1XVd4ErgM1z6vwq8Imq+gZAVd3fh7gkSZIkSZIkSZI04vqRDFsP3NPxeLop6/R84JAkrSRfTHJyH+KSJOkHkjwtyc1JvtxM2fu7TfmhSa5P8vXm7yEdbc5qpgC+M8mJHeXHJtnRbDsvSZryg5J8rCm/KcmRfe+oJEmSJEmSNGZWfJpEIF3KqkscxwInAGuBG5N8oar+4gk7SrYCWwEmJiZotVq9j7YPZmZmVm3syzGu/Z5YC2ccMzuQ5x7k8R7X13tc+z0iHgVeUVUzSQ4EPp/kWuBfADdU1TnNfS/PBN6R5GhgC/AC4LnA/0jy/Kp6DLiA9vnqC7RvuLwJuBY4FXioqp6XZAvwHuBX+ttNSZIkSZIkabz0Ixk2DRzR8XgDcG+XOt+sqm8D307yOeBFwBOSYVW1DdgGMDk5WVNTUysV84pqtVqs1tiXY1z7ff7lV3Pujn78U3uy3SdNDeR5YXxf73Ht9yioqgJmmocHNkvRntp3qim/FGgB72jKr6iqR4G7k+wCjkuyGzi4qm4ESHIZ8DraybDNwLuafV0FvD9JmueWJEmSJEmStAL6MU3iLcDGJEcleSrtq+i3z6lzNfBPk6xJ8kPAy4A7+hCbJEk/kOSAJLcB9wPXV9VNwERV7QFo/j6nqT7fNMDrm/W55U9oU1WzwMPAYSvSGUmSJEmSJElAH0aGVdVskrcC1wEHABdX1c4kb2m2X1hVdyT5DPAV4PvAh6rqqysdmyRJnZopDl+c5NnAJ5O8cIHq800DvND0wIuZOthpgVe5ce230wKPl3HttyRJkiRpderL3G1VdQ3te6Z0ll045/F/Av5TP+KRJGkhVfW3SVq07/V1X5J1VbUnyTrao8Zg/mmAp5v1ueWdbaaTrAGeBTzY5fmdFngVG9d+Oy3weBnXfkuSJEmSVqd+TJMoSdLQS/LDzYgwkqwFfg74Gu2pfU9pqp1Ce2pfmvItSQ5KchSwEbi5mUrxkSTHJwlw8pw2e/f1euCz3i9MkiRJkiRJWlmDuXxXkqThsw64NMkBtC8WubKqPpXkRuDKJKcC3wDeANBM+XslcDswC5zeTLMIcBpwCbAWuLZZAC4CPpxkF+0RYVv60jNJkiRJkiRpjJkMkyQJqKqvAC/pUv4AcMI8bc4Gzu5SfivwpPuNVdV3aJJpkiRJkiRJkvrDaRIlSZIkSZIkSZI0skyGSZIkSZIkSZIkaWSZDJMkSZIkSZIkSdLIMhkmSZIkSZIkSZKkkWUyTJIkSZIkSZIkSSPLZJgkSZIkSZIkSZJGlskwSZIkSZIkSZIkjSyTYZIkSZIkSZIkSRpZJsMkSZIkSZIkSZI0skyGSZIkSZIkSZIkaWSZDJMkSZIkSZIkSdLIMhkmSZIkSZIkSZKkkWUyTJIkSZIkSZIkSSPLZJgkSZIkSZIkSZJGlskwSZIkSZIkSZIkjSyTYZIkSZI0JpI8LcnNSb6cZGeS323KD01yfZKvN38P6WhzVpJdSe5McmJH+bFJdjTbzkuSpvygJB9rym9KcmTfOypJkiRJHUyGSZIkSdL4eBR4RVW9CHgxsCnJ8cCZwA1VtRG4oXlMkqOBLcALgE3AB5Mc0OzrAmArsLFZNjXlpwIPVdXzgPcB7+lDvyRJkiRpXibDJEmSJGlMVNtM8/DAZilgM3BpU34p8LpmfTNwRVU9WlV3A7uA45KsAw6uqhurqoDL5rTZu6+rgBP2jhqTJEmSpEEwGSZJkiRJYyTJAUluA+4Hrq+qm4CJqtoD0Px9TlN9PXBPR/Pppmx9sz63/AltqmoWeBg4bEU6I0mSJEmLsGbQAUiSJEmS+qeqHgNenOTZwCeTvHCB6t1GdNUC5Qu1eeKOk620p1lkYmKCVqu1QBjDa2ZmZtXGvhzj2u+JtXDGMbMDee5BHu9xfb3Htd+SJI0ik2GSJEmSNIaq6m+TtGjf6+u+JOuqak8zBeL9TbVp4IiOZhuAe5vyDV3KO9tMJ1kDPAt4sMvzbwO2AUxOTtbU1FSPetZfrVaL1Rr7coxrv8+//GrO3TGYn1J2nzQ1kOeF8X29x7XfkiSNIqdJlCRJkqQxkeSHmxFhJFkL/BzwNWA7cEpT7RTg6mZ9O7AlyUFJjgI2Ajc3Uyk+kuT45n5gJ89ps3dfrwc+29xXTJIkSZIGwpFhkiRJkjQ+1gGXJjmA9sWRV1bVp5LcCFyZ5FTgG8AbAKpqZ5IrgduBWeD0ZppFgNOAS4C1wLXNAnAR8OEku2iPCNvSl55JkiRJ0jxMhkmSJEnSmKiqrwAv6VL+AHDCPG3OBs7uUn4r8KT7jVXVd2iSaZIkSZI0DJwmUZIkSZIkSZIkSSPLZJgkSZIkSZIkSZJGlskwSZIkSZIkSZIkjSyTYZIkSZIkSZIkSRpZJsMkSZIkSZIkSZI0skyGSZIkSZIkSVKHJJuS3JlkV5IzF6j3U0keS/L6fsYnSdo/JsMkSZIkSZIkqZHkAOADwKuBo4E3Jjl6nnrvAa7rb4SSpP1lMkySJEmSJEmSHnccsKuq7qqq7wJXAJu71PsN4OPA/f0MTpK0/0yGSZIkSZIkSdLj1gP3dDyebsp+IMl64BeBC/sYlyRpidYMOgBJkiRJkiRJGiLpUlZzHv8B8I6qeizpVr3ZUbIV2AowMTFBq9XqUYj9NTMzs2pjX45x7ffEWjjjmNmBPPcgj/e4vt7j0m+TYZIkSZIkSZL0uGngiI7HG4B759SZBK5oEmGHA69JMltVf9xZqaq2AdsAJicna2pqaoVCXlmtVovVGvtyjGu/z7/8as7dMZjUwe6TpgbyvDC+r/e49Lsv0yQm2ZTkziS7kpy5QL2fSvJYktf3Iy5JkiRJkiRJmuMWYGOSo5I8FdgCbO+sUFVHVdWRVXUkcBXwb+YmwiRJw2PFk2FJDgA+ALwaOBp4Y5Kj56n3HuC6lY5JkiRJkiRJkrqpqlngrbR/p7wDuLKqdiZ5S5K3DDY6SdJS9GNk2HHArqq6q6q+C1wBbO5S7zeAjwP39yEmSZKeIMkRSf40yR1JdiZ5W1N+aJLrk3y9+XtIR5uzmlHPdyY5saP82CQ7mm3npZk3I8lBST7WlN+U5Mi+d1SSJEmStE9VdU1VPb+qfryqzm7KLqyqC7vUfVNVXdX/KCVJi9WPZNh64J6Ox9NN2Q8kWQ/8IvCkk4kkSX0yC5xRVT8JHA+c3oxkPhO4oao2Ajc0j2m2bQFeAGwCPtiMcga4gPYNkjc2y6am/FTgoap6HvA+2iOiJUmSJEmSJK2gftwFL13Kas7jPwDeUVWPNRfPd99RspX2j4tMTEzQarV6FGJ/zczMrNrYl2Nc+z2xFs44ZnYgzz3I4z2ur/e49nsUVNUeYE+z/kiSO2hfvLEZmGqqXQq0gHc05VdU1aPA3Ul2Accl2Q0cXFU3AiS5DHgdcG3T5l3Nvq4C3p8kVTX3vChJkiRJkiSpR/qRDJsGjuh4vAG4d06dSeCKJhF2OPCaJLNzbzpZVduAbQCTk5M1NTW1QiGvrFarxWqNfTnGtd/nX3415+7oxz+1J9t90tRAnhfG9/Ue136Pmmb6wpcANwETTaKMqtqT5DlNtfXAFzqa7R35/L1mfW753jb3NPuaTfIwcBjwzZXpiSRJkiRJkqR+/EJ/C7AxyVHAX9OeUupXOytU1VF715NcAnxqbiJMkqR+SPIM2vewfHtVfWuBEcvzjXxeaET0YkZLOxJ6lRvXfjsSeryMa78lSZIkSavTiifDmivf3wpcBxwAXFxVO5O8pdnufcIkSUMhyYG0E2GXV9UnmuL7kqxrRoWtA+5vyucb+TzdrM8t72wznWQN8CzgwblxOBJ6dRvXfjsSeryMa78lSZIkSavTU/rxJFV1TVU9v6p+vKrObsou7JYIq6o3VdVV/YhLkqS90h4CdhFwR1W9t2PTduCUZv0U4OqO8i1JDmpGP28Ebm6mVHwkyfHNPk+e02bvvl4PfNb7hUmSJEmSJEkrazCX70qSNHxeDvw6sCPJbU3Z7wDnAFcmORX4BvAGgGaU85XA7cAscHpVPda0Ow24BFgLXNss0E62fTjJLtojwrascJ8kSZIkSZKksWcyTJIkoKo+T/d7egGcME+bs4Gzu5TfCrywS/l3aJJpkiRJkiRJkvqjL9MkSpIkSZIkSZIkSYNgMkySJEmSJEmSJEkjy2SYJEmSJEmSJEmSRpbJMEmSJEmSJEmSJI0sk2GSJEmSJEmSJEkaWSbDJEmSJEmSJEmSNLJMhkmSJEmSJEmSJGlkmQyTJEmSpDGR5Igkf5rkjiQ7k7ytKT80yfVJvt78PaSjzVlJdiW5M8mJHeXHJtnRbDsvSZryg5J8rCm/KcmRfe+oJEmSJHUwGSZJkiRJ42MWOKOqfhI4Hjg9ydHAmcANVbURuKF5TLNtC/ACYBPwwSQHNPu6ANgKbGyWTU35qcBDVfU84H3Ae/rRMUmSJEmaj8kwSZIkSRoTVbWnqr7UrD8C3AGsBzYDlzbVLgVe16xvBq6oqker6m5gF3BcknXAwVV1Y1UVcNmcNnv3dRVwwt5RY5IkSZI0CCbDJEmSJGkMNdMXvgS4CZioqj3QTpgBz2mqrQfu6Wg23ZStb9bnlj+hTVXNAg8Dh61IJyRJkiRpEdYMOgBJkiRJUn8leQbwceDtVfWtBQZuddtQC5Qv1GZuDFtpT7PIxMQErVZrH1EPp5mZmVUb+3KMa78n1sIZx8wO5LkHebzH9fUe135LkjSKTIZJkiRJ0hhJciDtRNjlVfWJpvi+JOuqak8zBeL9Tfk0cERH8w3AvU35hi7lnW2mk6wBngU8ODeOqtoGbAOYnJysqampHvSu/1qtFqs19uUY136ff/nVnLtjMD+l7D5paiDPC+P7eo9rvyVJGkVOkyhJkiRJY6K5d9dFwB1V9d6OTduBU5r1U4CrO8q3JDkoyVHARuDmZirFR5Ic3+zz5Dlt9u7r9cBnm/uKSZIkSdJAODJMkiRJksbHy4FfB3Ykua0p+x3gHODKJKcC3wDeAFBVO5NcCdwOzAKnV9VjTbvTgEuAtcC1zQLtZNuHk+yiPSJsywr3SZIkSZIWZDJMkiRJksZEVX2e7vf0AjhhnjZnA2d3Kb8VeGGX8u/QJNMkSZIkaRg4TaIkSZIkSZIkSZJGlskwSZIkSZIkSZIkjSyTYZIkSZIkSZIkSRpZJsMkSZIkSZIkSZI0skyGSZIkSZIkSZIkaWSZDJMkSZIkSZIkSdLIMhkmSZIkSZIkSZKkkWUyTJIkSZIkSZIkSSPLZJgkSZIkSZIkSZJGlskwSZIkSZIkSZIkjSyTYZIkSZIkSZIkSRpZJsMkSZIkSZIkSZI0skyGSZIkSZIkSZIkaWSZDJMkSZIkSZIkSdLIMhkmSZIkSZIkSZKkkWUyTJIkSZIkSZIkSSPLZJgkSZIkSZIkSZJGlskwSZIkSZIkSZIkjSyTYZIkSZIkSZLUIcmmJHcm2ZXkzC7bT0rylWb5n0leNIg4JUmLYzJMkiRJkiRJkhpJDgA+ALwaOBp4Y5Kj51S7G/hnVfWPgN8DtvU3SknS/jAZJkmSJEmSJEmPOw7YVVV3VdV3gSuAzZ0Vqup/VtVDzcMvABv6HKMkaT+s6ceTJNkE/CFwAPChqjpnzvaTgHc0D2eA06rqy/2ITZIkSZIkSZI6rAfu6Xg8DbxsgfqnAtd225BkK7AVYGJiglar1aMQ+2tmZmbVxr4c49rvibVwxjGzA3nuQR7vcX29x6XfK54M6xhW/EraJ45bkmyvqts7qu0dVvxQklfTHla80AlGkqSeSnIx8Frg/qp6YVN2KPAx4EhgN/DLe6/8S3IW7S88jwG/WVXXNeXHApcAa4FrgLdVVSU5CLgMOBZ4APiVqtrdp+5JkiRJkhYvXcqqa8XkZ2l/N/wn3bZX1TaaKRQnJydramqqRyH2V6vVYrXGvhzj2u/zL7+a/4+9e4+3ra7r/f96C4iUIiC64qZYbj0p5G2L+LM6qyjBS2GlhocEjdrpwdRzqAS7aBql50TehXZKgDckUiGFjLSVegIRCOWmuZWt7NiCAiLbW278/P4Y3yWTxVxrr732WmveXs/HYz72nN9x+3znnHt+1hifMb7jlKtW5Tqae9h49PRAtguT+3lPSr9XY5hELyuWJI2CM4Aj5rSdCHy0qtYAH22vaWPFHwU8qi3ztnbyB8CpdGf9rWmP2XUeB9xWVQ8DXg+8bsV6IkmSJEnaEZuAA3pe7w/cOHemJD8FvB04sqpuWaXYJElLsBrFsH6XFe+3wPzzXlYsSdJKqaqPA7fOaT4SOLM9PxN4Zk/72VX1vaq6HtgAHJJkH2D3qrq4qoruSrBn9lnXucBhSfqdbShJkiRJGqxPA2uSPDTJvelOhjy/d4YkDwbeDzyvqv5jADFKkrbDalzruGyXFTvG7mib1H47xu5kmdR+j7GpqtoMUFWbkzyote9HdyXzrNkTPb7fns9tn13mhraurUluBx4AfH3uRs13o21S+22+myyT2m9JkjQZ2j7bi4GPADsBp1fVNUle2KafBvwJ3T7d29p5jlurau2gYpYkLWw1imHbe1nxU+e7rNgxdkfbpPbbMXYny6T2ewLNd6LHQieALPrkEPPdaJvUfpvvJsuk9luSJE2OqrqA7j7QvW2n9Tz/LeC3VjsuSdLSrMYwiV5WLEkaVTe1oQ9p/97c2uc70WMTd7/vZe8JID9cJsnOwP2557CMkiRJkiRJkpbZihfDqmorMHtZ8XXAObOXFc9eWszdLyu+MsllKx2XJEmLcD5wbHt+LHBeT/tRSXZN8lBgDXBpG1LxjiSHtvuBHTNnmdl1PQv4WLuvmCRJkiRJkqQVtCpj2XhZsSRp2CV5LzAN7J1kE/BK4LXAOUmOA74CPBugndRxDnAtsBU4vqrubKt6EXAGsBtwYXsAvAN4Z5INdFeEHbUK3ZIkSZIkSZIm3mBu7CBJ0pCpqufOM+mweeY/GTi5T/tlwEF92r9LK6ZJkjRISU4HngHcXFUHtba9gPcBBwIbgedU1W1t2knAccCdwEuq6iOt/fHcdQLIBcBLq6qS7AqcBTweuAX49arauErdkyRJkqR7WI17hkmSJEmShscZwBFz2k4EPlpVa4CPttckeSTd1cyPasu8LclObZlTgXV0wwWv6VnnccBtVfUw4PXA61asJ5IkSZK0CBbDJEmSJGmCVNXH6Ybs7XUkcGZ7fibwzJ72s6vqe1V1PbABOCTJPsDuVXVxuwfmWXOWmV3XucBh7V6akiRJkjQQDpMoSZIkSZqqqs0AVbU5yYNa+37AJT3zbWpt32/P57bPLnNDW9fWJLcDDwC+3rvBJOvorixjamqKmZmZ5ezPqtmyZcvIxr4jJrXfU7vBCQdvHci2B/l+T+rnPan9liRpHFkMkyRJkiTNp98VXbVA+0LL3L2haj2wHmDt2rU1PT29xBAHa2ZmhlGNfUdMar/f/O7zOOWqwRxK2Xj09EC2C5P7eU9qvyVJGkcOkyhJkiRJuqkNfUj79+bWvgk4oGe+/YEbW/v+fdrvtkySnYH7c89hGSVJkiRp1VgMkyRJkiSdDxzbnh8LnNfTflSSXZM8FFgDXNqGVLwjyaHtfmDHzFlmdl3PAj7W7ismSZIkSQPhMImSJEmSNEGSvBeYBvZOsgl4JfBa4JwkxwFfAZ4NUFXXJDkHuBbYChxfVXe2Vb0IOAPYDbiwPQDeAbwzyQa6K8KOWoVuSZIkSdK8LIZJkiRJ0gSpqufOM+mweeY/GTi5T/tlwEF92r9LK6ZJkiRJ0jBwmERJkiRJkiRJkiSNLYthkiRJkiRJkiRJGlsWwyRJkiRJkiRJkjS2LIZJkiRJkiRJkiRpbFkMkyRJkiRJkiRJ0tiyGCZJkiRJkiRJkqSxZTFMkiRJkiRJkiRJY8timCRJkiRJkiRJksaWxTBJkiRJkiRJkiSNLYthkiRJkiRJkiRJGlsWwyRJkiRJkiRJkjS2LIZJkiRJkiRJkiRpbFkMkyRJkiRJkiRJ0tiyGCZJkiRJkiRJkqSxZTFMkiRJkiRJkiRJY8timCRJkiRJkiRJksaWxTBJkiRJkiRJkiSNLYthkiRJkiRJkiRJGlsWwyRJkiRJkiRJkjS2LIZJkiRJkiRJkiRpbFkMkyRJkiRJkiRJ0tiyGCZJkiRJkiRJkqSxZTFMkiRJkiRJkiRJY8timCRJkiRJkiRJksaWxTBJkiRJkiRJkiSNLYthkiRJkiRJkiRJGlsWwyRJkiRJkiRJkjS2LIZJkiRJkiRJkiRpbFkMkyRJkiRJkiRJ0thalWJYkiOSfD7JhiQn9pmeJG9q0z+b5HGrEZckSattWzlRkqRxYc6TJI0yj2dK0nhZ8WJYkp2AtwJPBR4JPDfJI+fM9lRgTXusA05d6bgkSVpti8yJkiSNPHOeJGmUeTxTksbPalwZdgiwoaq+VFX/BZwNHDlnniOBs6pzCbBHkn1WITZJklbTYnKiJEnjwJwnSRplHs+UpDGzGsWw/YAbel5vam3bO48kSaPOfCdJmhTmPEnSKPN4piSNmZ1XYRvp01ZLmIck6+guOwbYkuTzOxjboOwNfH3QQQyA/V5led0gtvpDft6j5yGDDmACmO8mg/1eZea7gRjlfpvvVsc2c575buTZ71VmvhuIUe63+W7HeDzznkb5/8OOsN+rzHw3EKPc70Xnu9Uohm0CDuh5vT9w4xLmoarWA+uXO8DVluSyqlo76DhWm/2eLPZb6st8NwHs92Sx39K8tpnzzHejzX5PFvutCeTxzDkm9f+D/Z4s9nu8rcYwiZ8G1iR5aJJ7A0cB58+Z53zgmHQOBW6vqs2rEJskSatpMTlRkqRxYM6TJI0yj2dK0phZ8SvDqmprkhcDHwF2Ak6vqmuSvLBNPw24AHgasAH4NvCClY5LkqTVNl9OHHBYkiQtO3OeJGmUeTxTksbPagyTSFVdQJcgettO63lewPGrEcuQGPlLo5fIfk8W+y310S8njrFJ/f9gvyeL/ZbmMUE5b1L/P9jvyWK/NXE8nnkPk/r/wX5PFvs9xtL9bkuSJEmSJEmSJEnjZzXuGSZJkiRJkiRJkiQNhMWwFZLkiCSfT7IhyYnzzDOd5Mok1yT519WOcSVsq99J7p/kH5J8pvV7LMZTTnJ6kpuTXD3P9CR5U3tfPpvkcasd40pYRL+Pbv39bJJ/S/Lo1Y5xJWyr3z3zPSHJnUmetVqxSattUvMdTGbOM9+Z7+aZz3yniTCpOc9813e6+c58J40t8535rme6+W6M8h2Y8yyGrYAkOwFvBZ4KPBJ4bpJHzplnD+BtwC9X1aOAZ692nMttMf2mG0v52qp6NDANnJLk3qsa6Mo4AzhigelPBda0xzrg1FWIaTWcwcL9vh7471X1U8BrGJ/xZ89g4X7P/n94Hd3NdqWxNKn5DiY6552B+a4f8535TmNuUnOe+W5e5jvznTSWzHfmuznMd+OV72DCc57FsJVxCLChqr5UVf8FnA0cOWee/wG8v6q+AlBVN69yjCthMf0u4H5JAtwXuBXYurphLr+q+jhdX+ZzJHBWdS4B9kiyz+pEt3K21e+q+requq29vATYf1UCW2GL+LwBfhf4e2Ac/m9L85nUfAcTmvPMd/NON9+Z7zT+JjXnme/6M9+Z76RxZb4z3/Uy341RvgNznsWwlbEfcEPP602trdfDgT2TzCS5PMkxqxbdyllMv98C/CRwI3AV8NKq+sHqhDdQi3lvxt1xwIWDDmI1JNkP+BXgtEHHIq2wSc13YM6bj/nOfCeNq0nNeea7/sx35jtpXJnvOua7jvlugvIdjH/O23nQAYyp9GmrOa93Bh4PHAbsBlyc5JKq+o+VDm4FLabfhwNXAj8P/ARwUZJPVNU3Vzi2QVvMezO2kvwcXfL46UHHskreALy8qu7sThiSxtak5jsw583HfGe+k8bVpOY8811/5jvznTSuzHd3Md+Z7yYt38GY5zyLYStjE3BAz+v96c4amDvP16vqW8C3knwceDQwyoljMf1+AfDaqipgQ5Lrgf8GXLo6IQ7MYt6bsZTkp4C3A0+tqlsGHc8qWQuc3ZLG3sDTkmytqg8ONCpp+U1qvgNz3nzMd+Y7853G1aTmPPNdf+Y78535TuPKfNcx33XMd5OV72DMc57DJK6MTwNrkjy03UjxKOD8OfOcB/xMkp2T/AjwROC6VY5zuS2m31+hO3OEJFPAI4AvrWqUg3E+cEw6hwK3V9XmQQe10pI8GHg/8LwRP0Nou1TVQ6vqwKo6EDgX+J/jkjSkOSY134E5bz7mO/PdBwcblbRiJjXnme/6M9+Z7z442KikFWO+M9/1Mt9NUL6D8c95Xhm2Aqpqa5IXAx8BdgJOr6prkrywTT+tqq5L8o/AZ4EfAG+vqqsHF/WOW0y/gdcAZyS5iu5S25dX1dcHFvQySfJeYBrYO8km4JXALvDDfl8APA3YAHyb7mySkbeIfv8J8ADgbe2Mgq1VtXYw0S6fRfRbmgiTmu9gcnOe+c58h/lOE2pSc575znyH+c58p4livjPfYb4b23wH5rx0V3ZKkiRJkiRJkiRJ48dhEiVJkiRJkiRJkjS2LIZJkiRJkiRJkiRpbFkMkyRJkiRJkiRJ0tiyGCZJkiRJkiRJkqSxZTFMkiRJkiRJkiRJY8timCRJkiRJkiRJksaWxTBJkiRJkiRJkiSNLYthkiRJkiRJkiRJGlsWwyRJkiRJkiRJkjS2LIZJkiRJkiRJkiRpbFkMkyRJkiRJkiRJ0tiyGCZJkiRJkiRJkqSxZTFMkiRJkiRJkiRJY8timCRJkiRJkiRJksaWxTBJkiRJkiRJkiSNLYthkiRJkiRJkiRJGlsWwyRJkiRJkiRJkjS2LIZJkiRJkiRJkiRpbFkMkyRJkiRJkiRJ0tiyGCZJkiRJkiRJkqSxZTFMkiRJkiRJkiRJY8timCRJkiRJkiRJksaWxTBJkiRJkiRJkiSNLYthkiRJkiRJkiRJGlsWwyRJkiRJkiRJkjS2LIZJkiRJkiRJkiRpbFkMkyRJkiRJkiRJ0tiyGCZJkiRJkiRJkqSxZTFMkiRJkiRJkiRJY8timCRJkiRJkiRJksaWxTBJkiRJkiRJkiSNLYthkiRJkiRJkiRJGlsWwyRJkiRJkiRJkjS2LIZJkiRJkiRJkiRpbFkMkyRJkiRJkiRJ0tiyGCZJkiRJkiRJkqSxZTFMkiRJkiRJkiRJY8timCRJkiRJkiRJksaWxTBJkiRJkiRJkiSNLYthkiRJkiRJkiRJGlsWwyRJkiRJkiRJkjS2LIZJkiRJkiRJkiRpbFkMkyRJkiRJkiRJ0tiyGCZJkiRJkiRJkqSxZTFMkiRJkiRJkiRJY8timCRJkiRJkiRJksaWxTBJkiRJkiRJkiSNLYth0gKSPD/JJ1do3Wck+bOVWPcgJNmS5MfnmbZi76MkaWFJHtx+o3dahnVdk2R6nmnTSTbt6DYkSZpkSV6R5O0LTN+Y5BdWMyZJ0l2SHJikkuw86FiG0XK/Pzu6n9liedhyxKLRZzFME2XSDtSt5o5SVd23qr60GtuSJC1eVX2l/UbfuQzrelRVzSxDWCvCA4SSNJmSvCrJu1Zw/at2cl9V/XlV/dZqbEuStDiTsp8xDMdNLV5pJVkM00Ct5lkUnrEhSRpV5jBJklaOeVaStFJGJccsR5yj0tf5jHr82jaLYVoR7YyJk5Jcm+S2JH+b5D6zZxgkeXmSrwJ/m2TXJG9IcmN7vCHJrm09s/O/IsnX23qP7tnOrkn+MslXktyU5LQku81ZdnZb7wUuBPZtw0VtSbJvkm8neUDPOh+f5GtJdunTrzcmuSHJN5NcnuRneqa9Ksk5Sc5KckcbSmptz/THJrmiTXsfcJ+eafc407D3TIgkT2vv5R1J/jPJ7/XM94wkVyb5RpJ/S/JTrf2dwIOBf2h9/YMkH07yu3O289kkz+zZ5kuSfKm93/83yb165v3NJNe1z/QjSR4yT7wPSHJ+e58uBX5iga+LJKmPlvNenuSzwLeS/HT7nf9Gks+kZ7jCJA9N8vGWJ/45yVtnz5DPnGEqWu47P8mtSTYk+e2e9Wwrl/3wjMgku6Ub8ve2JNcCT5gT/wFJ3t9y6i1J3tLa75Xkj5J8OcnNbVv3b9PucSbinG3OG99S8p4kafS03PifLQ98PsnTgVcAv95+/z/T5ttWvjs3ybuSfBM4MfPvFx4MnAY8qa3/G0mekG7/c+ee+X8tyZVz1v++FucVSR7dM+++Sf6+rf/6JC+ZE9u7el4/r+XMW5L84Uq8p5Kk+c3dzwCe0yYdne545Nd7f5/75JjnJ7l/knck2dxy2J+lZxj7LHy87ReTfC7J7UnekuRfk/xWz7Z6c8bcfb8XtPXeke5Y3+/0zLvY46b3SnJiki+2XHROkr3mbO+4JF8BPtbz1v1muuO8m5Oc0LPdQ5Jc3PLp5tane7dpH2+zfaZt/9d7ljsh3f7j5iQv6Gk/I93+74dbPz+VZO5xyKelz7HOdMdj/1+S1ye5FXjVwt8GjTqLYVpJRwOH0xVCHg78UWv/MWAv4CHAOuAPgUOBxwCPBg7pmXd2/r2B/YBjgfVJHtGmva6t+zHAw9o8fzJn2dltHQM8FbixDRd136q6EZjhrkQG8BvA2VX1/T59+nTb1l7Ae4C/S3Kfnum/DJwN7AGcD8we+Ls38EHgnW3ZvwN+rc/65/MO4Heq6n7AQbTkkuRxwOnA7wAPAP4aOD/JrlX1POArwC+1vv4f4MzWP9ryj6Z7zy7o2davAGuBxwFHAr/Z5n0m3U7mrwIPBD5Blyj7eSvwXWCftvxvbkdfJUl3eS7wdODHgfOAP6PLI78H/H2SB7b53gNcSpcLXgU8b4F1vhfYBOwLPAv48ySH9Uzvm8v6eCVdjv8Junx/7OyEtmP3IeDLwIF0uebsNvn57fFzrV/3XWAb/fSNbwfyniRpRLT9wBcDT2j7RocDnwP+HHhf+/2fLTptK98dCZxLl09OYf79wquAFwIXt/XvUVWfBm4BfnHO/O+cs/6/4659xw8m2aUdgPsH4DN0Oekw4GVJDu/T30cCp9Ll9X3p8vz+i3u3JEnLYe5+BnBOm/TTwCPofsf/JMlP9izWm2PeTbdfspXu2OVjgacAswWtZzLP8bYkewN/T3ecdG/gi8CTtyP8m4FnALsDLwBe344lzlrMcdOXAM8E/jtdLrqN7rhfr/8O/CRdXp71c8Ca1tcTc9cwk3cC/6v150l079//BKiqn23zPLpt/309cd6fLm8eB7w1yZ4923ou8KfAnsAG4OQ58fU91tk8EfgS8KA+y2nMjHQxLMnprSJ89SLnf066q2uuSfKelY5PvKWqbqiqW+l+TJ7b2n8AvLKqvldV36Ermr26qm6uqq/R/XjNPYj3x23+fwU+DDwnSYDfBv5XVd1aVXfQ7QQd1bPc3G3188MDZe3g3XO5+07MD1XVu6rqlqraWlWnALvSJb5Zn6yqC9p9Wd5JV9yDrti3C/CGqvp+VZ1LV1hbrO8Dj0yye1XdVlVXtPbfBv66qj5VVXdW1ZnA99r2+jkPWJNkTXv9PLqdxv/qmed17f38CvAG7vrcfgf4i6q6rqq20r3Xj+k9WwV++B7+GvAnVfWtqrqa7j2WtETmu4n2pqq6gS5PXdByzA+q6iLgMroz3B5Md1XWn1TVf1XVJ+mKRPeQ5AC6nbaXV9V3q+pK4O3cPe/Ol8vmeg5wcssZNwBv6pl2CN2O0u+3XPDdFhd0ef+vqupLVbUFOAk4KosfkmKx8cHi8p4kaXTcSbcP9sgku1TVxqr64tyZFpnvLq6qD7a8+h22Y7+w6Z1/L7oDgL1/d11eVee2kyz/im5kkEPpcvYDq+rVLW9/Cfgb7r4fO+tZwIeq6uNV9T3gj+n2cSVJg/enVfWdqvoM3QkOvfslP8wxdIWopwIva/tGNwOv567f/YWOtz0NuLYnn7wB+OpiA6yqD1fVF6vzr8A/AT/TM8tijpv+DvCHVbWp5aJXAc+as//2qta33nX8aWu7Cvhb2vHFqrq8qi5px1Y30p3Y/9+30ZXv0x07/n5VXQBs4e7HY99fVZe29+/ddBcy9JrvWCd0xb83t3jmew80Jka6GAacARyxmBnbQZCTgCdX1aOAl61cWGpu6Hn+ZbqDYgBfq6rv9kzbt03vNy/AbVX1rT7THwj8CHB5u7T2G8A/tvZZc7fVz3l0O1M/Tndm3+1VdWm/Gdslude1S5O/QXdWwt49s/QmpG8D92nJYV/gP6uq5vRjsX6NLgF+uV0O/aTW/hDghNn+t5gO4O7v3w+1pHUO8BvtjMR+O3jzfW4PAd7Ys51bgdCdldHrgcDOfdYjaenOwHw3qWZ/Sx8CPHvO7/1P012Buy9wa1V9u89yc83Oe0dP25e5+2/5fLms37rm+60/APhy2xnpt9zcvL8zMDVPzHMtNr7F5j1J0oioqg10f9u8Crg5ydlJ+u37LCbfzc2Vi94vbN4F/FKS+9KdIPKJqtrcb/3tYOjsVWoPoRuCqjenv4L+eXDfOev5Ft0VaZKkwZu7X3Lfnte9OeYhdCfIb+753f9ruiuRZqfPd7xtbh4o5t/Xu4ckT01ySbohg79Bd2yx9zjmYo6bPgT4QE9819GdnNKbt/rF1Pf4YpKHJ/lQkq+mG0byz+fE1M8tc/Yt577fC30W88ayQOwaUyNdDKuqj9P9QPxQkp9I8o/p7uf0iST/rU36beCtVXVbW/bmVQ53Eh3Q8/zBwI3tec2Z70a6H9Z+8wLsmeRH+0z/OvAd4FHVDVWxR1Xdv7pLlmfN3dbc17Qf/XPozlR/HvMcJEt3f7CX0+3o7FlVewC30yWobdkM7NeuZuvtx6xv0RX2Zrf1Y3Ni/HRVHUmXKD/IXZdk30B3Vv4ePY8fqarZ4Qvv0V+6MxiPprsM+dtVdfGc6fN9bjfQDdXYu63dqurf5iz/NbpLv+euR9ISme8m2uzv+A3AO+f8Bv9oVb2WLsfsleRHepY74B5r6tzY5r1fT9uDgf9cQmybmf+3/gbgwfMUqfrl/a3ATdwzH+7E3U9y2Zal5D1J0gipqvdU1U/T5ZKiGzq/3z7mtvLd3ZbZxn5hv/3I/wQupht6qd9+5A9zZDshY/8W1w3A9XNy+v2q6ml9urt5znp+hG6oREnS6uq3n7HY+W+gG8Vp757f/d3byauz0+c73jY3D4S774Pdbf+JbjjB2Xl3pRti8S+BqXYc8wLufhxzm8dNW3xPnRPffVoeXGi5+Y4vnko3xPGaqtqd7oSQxRxb3RHzxQLb/9lqhI10MWwe64HfrarH091P422t/eHAw9PdFO+SJIs6w1475Pgk+7chI14BvG+e+d4L/FGSB7axcP+E7iy7Xn+a5N6tIPUM4O/a2XV/Qzfe7YMAkuzXb6z1HjcBD0hy/zntZ9Hdv+SX+2x71v3oDtZ9Ddg5yZ/QXeq8GBe3ZV+SZOckv0o3hNSszwCPSvKYdPcge9XshNbvo5Pcv10S/U26MzCg6/8LkzwxnR9N8vSenb6b6O7H8kPtIOAP6MbF71f4+/0ke7ahRV7KXZ/bacBJSR7V4rp/kmfPXbi6YaveD7wqyY+kG+f+2LnzSdph5rvJMnv2+eFJdkpyn3Q3PN6/qr5MN2Tiq1rOeBLwS/1WUt1whv8G/EVbx0/Rjbn+7iXEdA5dXtgzyf7A7/ZMu5Rux+21LTfdJ8ns2PbvBf5Xkoe2s+ln7/OyFfgPuiu9np5kF7qx8XfdjpiWkvckSSMiySOS/Hw7wPddupMj76T7/T+wFZ12JN/Nt194E7B/untBz53/D4CDgQ/Mmfb4JL/aTgx5Gd2B0EvocuQ3k7w8yW4trx+U5Al94jkXeEaSn27bfjXjeRxHkobdPfYzFqtdNfxPwClJdk9yr3Zy6+zQgAsdb/sw3fHC2XzyEnoKXsCVwM8meXA71nlSz7R70+1LfQ3YmuSpdPfv2lY/5x43PQ04Oe02Ke347ZGL6Poft+OCj6K7X9ns8cX70R3b3NJO6n1RnxiW9F4vYL5jnZowY/VHVDug8v8Bf5fkSrpLTvdpk3emu2nfNN0QOW9PssfqRzlR3kP3Y/+l9vizeeb7M7qDeJ8FrgKumDPvV+luzngj3c7LC6vqc23ay+lujHhJuktr/5m7jxl7N2259wJfSnd5776t/f/RHSi7oo1X289HgAvpDtR9mW7na1GX0lZ3b5Jfpduxug34dbqC0ez0/6Dbsfln4AvAJ+es4nnAxtbHF9LGpq+qy+iuAnlLW++Gto1Zf0FXaPxGkt/raT+LboetX+HvPOByuoT6YeAdbVsfoDvr8uwWx9V0Yx7382K6S5K/Sje829/OM5+kJTDfTZ52UO9IupNLvkaXf36fu/6WO5ru5sO30OXQ99EddOvnucCBdHn1A3RjxF+0hLD+lC4fXk+X739YaGonRvwS3Q2iv0I3NNSvt8mnt3k/3pb9Lq2QVlW30908+e10Z+9/qy27WEvJe5Kk0bEr8Fq6UUK+SjdyxiuAv2vTb0kye3/l7c53C+wXfgy4Bvhqkq/3tH+ANnzUnKH9oduv+nW6/bTnAb/a7nUymyMfQ5cHv06X9+aesElVXQMcT7dvvbmta3vyoiRpefxwP4Pufo7b6xi64tS1dL/l59L24Rc63lZVXweeTZf7bqHb1/9/syttee19dMdULwc+1DPtDrri2Tltm/+Dee4t3bNMv+Omb2zL/VOSO+hO7HjiIvr8r3THKT8K/GVV/VNr/70Wyx10J/nPLUy9Cjizbf85i9jOYvQ91qnJk7vfwmj0JDmQ7oayByXZHfh8Ve3TZ77TgEuq6oz2+qPAiVX16dWMd1Ik2Qj8VlX98w6uZxp4V1XtvwxhbWtbHwPeU1VvX+ltDVqSY4B1bXiR3vaiu0x5w2AikzQf8522R5L3AZ+rqlcOOpZhMF/ekyRpru3dL0zyRbrhrf65p+1VwMOq6jdWJkpJ0qRKMkN3rHTsj19Ky22srgyrqm8C189eStqGjXt0m/xB4Oda+950w0h9aRBxavi0ISkexwRcJptunPn/STfEmqQRZL7TXEme0IbauFcbGvNIuu/CxDPvSZIWa3v3C5P8Gt29Rj62knFJkiRpx410MSzJe+nuxfSIJJuSHEc3TNBxST5DN4zB7BimH6EbMuFa4F+A36+qWwYRt4ZLkjPphid8WbuEeGylu5/a1+jG333PgMORtEjmOy3CjwEzwBbgTcCLqurfBxrREDDvSZIWa3v3C9uZ+acCx7f7WUuSJGmIjfwwiZIkSZIkSZIkSdJ8RvrKMEmSJEmSJEmSJGkhFsMkSZIkSZIkSZI0tnYedABLtffee9eBBx446DD6+ta3vsWP/uiPDjqMgbDvk9f3Se03jF/fL7/88q9X1QMHHYfubrH5blS+j6MSJxjrShmVWEclTjDW7WW+G07DvH+3LcPwvR4E+z1Z7PfoMd8Npx3Nd6P8nexnnPozTn0B+zPs7M9dtiffjWwx7MADD+Syyy4bdBh9zczMMD09PegwBsK+Tw86jFU3qf2G8et7ki8POgbd02Lz3ah8H0clTjDWlTIqsY5KnGCs28t8N5yGef9uW4bhez0I9nuy2O/RY74bTjua70b5O9nPOPVnnPoC9mfY2Z+7bE++c5hESZIkSZIkSZIkjS2LYZIkSZIkSZIkSRpbFsMkSZIkSZIkSZI0tiyGSZIkSZIkSZIkaWxZDJMkSZIkSZIkSdLYshgmSZIkSZIkSZKksWUxTJIkSZIkSZIkSWPLYpgkSZIkSZIkSZLGlsUwSZKWUZKdkvx7kg/1mZYkb0qyIclnkzxuEDFKkiRJkiRJk8RimCRJy+ulwHXzTHsqsKY91gGnrlZQkiRJkiRJ0qTaedABSNI4OfDEDw9s2xtf+/SBbVudJPsDTwdOBv53n1mOBM6qqgIuSbJHkn2qavNqxilJO2qp+e6Eg7fy/B3MleY7SYN01X/evsO/Y0vl758kf4Mkaem8MkySpOXzBuAPgB/MM30/4Iae15tamyRJkiRJkqQV4pVhkiQtgyTPAG6uqsuTTM83W5+26rOudXTDKDI1NcXMzMw2t79ly5ZFzTdooxInGOtKGZVYRyVOGEysJxy8dUnLTe229GVnjcrnIkmSJEkaHhbDJElaHk8GfjnJ04D7ALsneVdV/UbPPJuAA3pe7w/cOHdFVbUeWA+wdu3amp6e3ubGZ2ZmWMx8gzYqcYKxrpRRiXVU4oTBxLrU4XlOOHgrp1y1Y7sgG4+e3qHlJUmSJEmTx2ESJUlaBlV1UlXtX1UHAkcBH5tTCAM4HzgmnUOB271fmCRJkiRJkrSyvDJMkqQVlOSFAFV1GnAB8DRgA/Bt4AUDDE2SJEmSJEmaCF4ZJknSMquqmap6Rnt+WiuEUZ3jq+onqurgqrpssJFKkiRJ0mhLskeSc5N8Lsl1SZ6UZK8kFyX5Qvt3z575T0qyIcnnkxze0/74JFe1aW9Kkta+a5L3tfZPJTmwZ5lj2za+kOTYVe24JGm7WAyTJEmSJEmSNKreCPxjVf034NHAdcCJwEerag3w0faaJI+kG9b+UcARwNuS7NTWcyqwDljTHke09uOA26rqYcDrgde1de0FvBJ4InAI8MreopskabhYDJMkSZIkSZI0cpLsDvws8A6AqvqvqvoGcCRwZpvtTOCZ7fmRwNlV9b2qup5uCPtDkuwD7F5VF1dVAWfNWWZ2XecCh7Wrxg4HLqqqW6vqNuAi7iqgSZKGjPcMkyRJkiRJkjSKfhz4GvC3SR4NXA68FJiqqs0AVbU5yYPa/PsBl/Qsv6m1fb89n9s+u8wNbV1bk9wOPKC3vc8yP5RkHd0VZ0xNTTEzM7PUvjK1G5xw8NYlL78jdiTu+WzZsmVF1jsI49QXsD/Dzv4sjcUwSZIkSZIkSaNoZ+BxwO9W1aeSvJE2JOI80qetFmhf6jJ3NVStB9YDrF27tqanpxcIb2Fvfvd5nHLVYA7nbjx6etnXOTMzw468H8NknPoC9mfY2Z+lcZhESZIkSZIkSaNoE7Cpqj7VXp9LVxy7qQ19SPv35p75D+hZfn/gxta+f5/2uy2TZGfg/sCtC6xLkjSELIZJkiRJkiRJGjlV9VXghiSPaE2HAdcC5wPHtrZjgfPa8/OBo5LsmuShwBrg0jak4h1JDm33AztmzjKz63oW8LF2X7GPAE9JsmeSPYGntDZJ0hBymERJkiRJkiRJo+p3gXcnuTfwJeAFdBcAnJPkOOArwLMBquqaJOfQFcy2AsdX1Z1tPS8CzgB2Ay5sD4B3AO9MsoHuirCj2rpuTfIa4NNtvldX1a0r2VFJ0tJZDJMkSZIkSZI0kqrqSmBtn0mHzTP/ycDJfdovAw7q0/5dWjGtz7TTgdO3I1xJ0oA4TKIkSZIkSZIkSZLGlsUwSZIkSRJJ7pPk0iSfSXJNkj9t7XsluSjJF9q/e/Ysc1KSDUk+n+TwnvbHJ7mqTXtTu/+KJEmSJA2ExTBJkiRJEsD3gJ+vqkcDjwGOSHIocCLw0apaA3y0vSbJI+num/Io4AjgbUl2aus6FVgHrGmPI1axH5IkSZJ0NxbDJEmSJElUZ0t7uUt7FHAkcGZrPxN4Znt+JHB2VX2vqq4HNgCHJNkH2L2qLq6qAs7qWUaSJEmSVt3Ogw5AkiRJkjQc2pVdlwMPA95aVZ9KMlVVmwGqanOSB7XZ9wMu6Vl8U2v7fns+t33uttbRXT3G1NQUMzMzy9yb1bFly5aRjX1HTGq/p3aDEw7eOpBtD/L9ntTPe1L7LUnSOLIYJkmSJEkCoKruBB6TZA/gA0kOWmD2fvcBqwXa525rPbAeYO3atTU9Pb3d8Q6DmZkZRjX2HTGp/X7zu8/jlKsGcyhl49HTA9kuTO7nPan9liRpHA1VMSzJRuAO4E5ga1WtHWxEkiRJkjR5quobSWbo7vV1U5J92lVh+wA3t9k2AQf0LLY/cGNr379PuyRJkiQNxDDeM+znquoxFsIkSZIkafUkeWC7IowkuwG/AHwOOB84ts12LHBee34+cFSSXZM8FFgDXNqGVLwjyaFJAhzTs4wkSZIkrbqhujJMkiRJkjQw+wBntvuG3Qs4p6o+lORi4JwkxwFfAZ4NUFXXJDkHuBbYChzfhlkEeBFwBrAbcGF7SJIkSdJADFsxrIB/SlLAX7cx5CVJkiRJK6yqPgs8tk/7LcBh8yxzMnByn/bLgIXuNyZJkiRJq2bYimFPrqobkzwIuCjJ56rq47MTk6wD1gFMTU0xMzMzoDAXtmXLlqGNbaXZ95lBh7HqJrXf0L/vJxy8dTDBwMR+DpIkSZIkSZK0kKEqhlXVje3fm5N8ADgE+HjP9PXAeoC1a9fW9PT0IMLcppmZGYY1tpVm36cHHcaqm9R+Q/++P//EDw8mGGDj0dMD27YkSZIkSZIkDat7DTqAWUl+NMn9Zp8DTwGuHmxUkiRJkiRJkiRJGmXDdGXYFPCBJNDF9Z6q+sfBhiRJkiRJkiRJkqRRNjTFsKr6EvDoQcchSdJSJLkP3dC+u9Ll13Or6pVz5pkGzgOub03vr6pXr2KYkiRJkiRJ0sQZmmKYJEkj7nvAz1fVliS7AJ9McmFVXTJnvk9U1TMGEJ8kSZIkSZI0kSyGSZK0DKqqgC3t5S7tUYOLSJIkSZIkSRLAvQYdgCRJ4yLJTkmuBG4GLqqqT/WZ7UlJPpPkwiSPWt0IJUmSJEmSpMnjlWGSJC2TqroTeEySPYAPJDmoqq7umeUK4CFtKMWnAR8E1sxdT5J1wDqAqakpZmZmtrntLVu2LGq+QRuVOMFYV8qoxDoqccJgYj3h4K1LWm5qt6UvO2tUPhdJkiRJ0vCwGCZJ0jKrqm8kmQGOAK7uaf9mz/MLkrwtyd5V9fU5y68H1gOsXbu2pqent7nNmZkZFjPfoI1KnGCsK2VUYh2VOGEwsT7/xA8vabkTDt7KKVft2C7IxqOnd2h5SZIkSdLkcZhESZKWQZIHtivCSLIb8AvA5+bM82NJ0p4fQpeHb1nlUCVJkiRJkqSJ4pVhkiQtj32AM5PsRFfkOqeqPpTkhQBVdRrwLOBFSbYC3wGOqqoaWMSSJEmSJEnSBLAYJknSMqiqzwKP7dN+Ws/ztwBvWc24JEmSJEmSpEnnMImSJEmSJEmSJEkaWxbDJEmSJEmSJEmSNLYshkmSJEmSJEmSJGlsWQyTJEmSJEmSJEnS2LIYJkmSJEmSJEmSpLFlMUySJEmSJEmSJEljy2KYJEmSJEmSJEmSxpbFMEmSJEmSJEmSJI0ti2GSJEmSJEmSRlKSjUmuSnJlksta215JLkryhfbvnj3zn5RkQ5LPJzm8p/3xbT0bkrwpSVr7rkne19o/leTAnmWObdv4QpJjV7HbkqTtZDFMkiRJkiRJ0ij7uap6TFWtba9PBD5aVWuAj7bXJHkkcBTwKOAI4G1JdmrLnAqsA9a0xxGt/Tjgtqp6GPB64HVtXXsBrwSeCBwCvLK36CZJGi4WwyRJkiRJkiSNkyOBM9vzM4Fn9rSfXVXfq6rrgQ3AIUn2AXavqourqoCz5iwzu65zgcPaVWOHAxdV1a1VdRtwEXcV0CRJQ8ZimCRJkiRJkqRRVcA/Jbk8ybrWNlVVmwHavw9q7fsBN/Qsu6m17deez22/2zJVtRW4HXjAAuuSJA2hnQcdgCRJkiRJkiQtnmJzqwAAdjlJREFU0ZOr6sYkDwIuSvK5BeZNn7ZaoH2py9y1wa5Atw5gamqKmZmZBcJb2NRucMLBW5e8/I7Ykbjns2XLlhVZ7yCMU1/A/gw7+7M0FsMkSZIkSZIkjaSqurH9e3OSD9Ddv+umJPtU1eY2BOLNbfZNwAE9i+8P3Nja9+/T3rvMpiQ7A/cHbm3t03OWmekT33pgPcDatWtrenp67iyL9uZ3n8cpVw3mcO7Go6eXfZ0zMzPsyPsxTMapL2B/hp39WRqHSZQkSZIkSZI0cpL8aJL7zT4HngJcDZwPHNtmOxY4rz0/Hzgqya5JHgqsAS5tQynekeTQdj+wY+YsM7uuZwEfa/cV+wjwlCR7JtmzbfsjK9hdSdIO8MowSZIkSZIkSaNoCvhAV79iZ+A9VfWPST4NnJPkOOArwLMBquqaJOcA1wJbgeOr6s62rhcBZwC7ARe2B8A7gHcm2UB3RdhRbV23JnkN8Ok236ur6taV7KwkaekshkmSJEmSSHIAcBbwY8APgPVV9cYkrwJ+G/ham/UVVXVBW+Yk4DjgTuAlVfWR1v547jqgeAHw0nYWvSRJy6aqvgQ8uk/7LcBh8yxzMnByn/bLgIP6tH+XVkzrM+104PTti1qSNAgWwyRJkiRJ0J0hf0JVXdGGnLo8yUVt2uur6i97Z07ySLqz4x8F7Av8c5KHtzPsTwXWAZfQFcOO4K4z7CVJkiRpVXnPMEmSJEkSVbW5qq5oz+8ArgP2W2CRI4Gzq+p7VXU9sAE4JMk+wO5VdXG7Guws4JkrG70kSZIkzc9imCRJkiTpbpIcCDwW+FRrenGSzyY5PcmerW0/4IaexTa1tv3a87ntkiRJkjQQDpMoSZIkSfqhJPcF/h54WVV9M8mpwGuAav+eAvwmkD6L1wLtc7ezjm4oRaamppiZmVmW+Ffbli1bRjb2HTGp/Z7aDU44eOtAtj3I93tSP+9J7bckSePIYpgkSZIkCYAku9AVwt5dVe8HqKqbeqb/DfCh9nITcEDP4vsDN7b2/fu0301VrQfWA6xdu7amp6eXrR+raWZmhlGNfUdMar/f/O7zOOWqwRxK2Xj09EC2C5P7eU9qvyVJGkcOkyhJ0jJIcp8klyb5TJJrkvxpn3mS5E1JNrShph43iFglSeonSYB3ANdV1V/1tO/TM9uvAFe35+cDRyXZNclDgTXApVW1GbgjyaFtnccA561KJyRJkiSpD68MkyRpeXwP+Pmq2tLOqv9kkgur6pKeeZ5Kd6BwDfBE4NT2ryRJw+DJwPOAq5Jc2dpeATw3yWPohjrcCPwOQFVdk+Qc4FpgK3B8Vd3ZlnsRcAawG3Bhe0iSJEnSQFgMkyRpGVRVAVvay13aY+79UY4EzmrzXpJkjyT7tDPoJUkaqKr6JP3v93XBAsucDJzcp/0y4KDli06SJEmSls5hEiVJWiZJdmpn0t8MXFRVn5ozy37ADT2vN7U2SZIkSZIkSSvEK8MkSVombWioxyTZA/hAkoOq6uqeWfqdbT/36jGSrAPWAUxNTTEzM7PNbW/ZsmVR8w3aqMQJxrpSRiXWUYkTBhPrCQdvXdJyU7stfdlZo/K5SJIkSZKGh8UwSZKWWVV9I8kMcATQWwzbBBzQ83p/4MY+y68H1gOsXbu2pqent7nNmZkZFjPfoI1KnGCsK2VUYh2VOGEwsT7/xA8vabkTDt7KKVft2C7IxqOnd2h5SZIkSdLkGaphEtvwUv+e5EODjkWSpO2R5IHtijCS7Ab8AvC5ObOdDxyTzqHA7d4vTJIkSZIkSVpZw3Zl2EuB64DdBx2IJEnbaR/gzCQ70Z1sck5VfSjJCwGq6jTgAuBpwAbg28ALBhWsJEmSJEmSNCmGphiWZH/g6cDJwP8ecDiSJG2Xqvos8Ng+7af1PC/g+NWMS5IkSZIkSZp0wzRM4huAPwB+MOA4JEmSJEmSJEmSNCaG4sqwJM8Abq6qy5NMLzDfOmAdwNTUFDMzM6sS3/basmXL0Ma20uz7zKDDWHWT2m/o3/cTDt46mGBgYj8HSZIkSZIkSVrIUBTDgCcDv5zkacB9gN2TvKuqfqN3pqpaD6wHWLt2bU1PT696oIsxMzPDsMa20uz79KDDWHWT2m/o3/fnn/jhwQQDbDx6emDbliRJkiRJkqRhNRTDJFbVSVW1f1UdCBwFfGxuIUySJEmSJEmSJEnaXkNRDJMkSZIkSZIkSZJWwrAMk/hDVTUDzAw4DEmSJEmSJEmSJI0BrwyTJEmSJEmSJEnS2LIYJkmSJEmSJEmSpLFlMUySJEmSJEmSJEljy2KYJEmSJEmSJEmSxpbFMEmSJEmSJEmSJI0ti2GSJEmSJEmSJEkaWxbDJEmSJEmSJEmSNLYshkmSJEmSJEmSJGlsWQyTJEmSJEmSJEnS2LIYJkmSJEmSJEmSpLFlMUySJEmSJEmSJEljy2KYJEmSJEmSpJGVZKck/57kQ+31XkkuSvKF9u+ePfOelGRDks8nObyn/fFJrmrT3pQkrX3XJO9r7Z9KcmDPMse2bXwhybGr2GVJ0nayGCZJkiRJkiRplL0UuK7n9YnAR6tqDfDR9pokjwSOAh4FHAG8LclObZlTgXXAmvY4orUfB9xWVQ8DXg+8rq1rL+CVwBOBQ4BX9hbdJEnDxWKYJEmSJEmSpJGUZH/g6cDbe5qPBM5sz88EntnTfnZVfa+qrgc2AIck2QfYvaourqoCzpqzzOy6zgUOa1eNHQ5cVFW3VtVtwEXcVUCTJA0Zi2GSJEmSJEmSRtUbgD8AftDTNlVVmwHavw9q7fsBN/TMt6m17deez22/2zJVtRW4HXjAAuuSJA2hnQcdgCRJkiRJkiRtryTPAG6uqsuTTC9mkT5ttUD7UpfpjXEd3fCLTE1NMTMzs4gw+5vaDU44eOuSl98ROxL3fLZs2bIi6x2EceoL2J9hZ3+WxmKYJEnLIMkBdENp/BjdGYnrq+qNc+aZBs4Drm9N76+qV69imJIkSZI0Tp4M/HKSpwH3AXZP8i7gpiT7VNXmNgTizW3+TcABPcvvD9zY2vfv0967zKYkOwP3B25t7dNzlpmZG2BVrQfWA6xdu7amp6fnzrJob373eZxy1WAO5248enrZ1zkzM8OOvB/DZJz6AvZn2NmfpXGYREmSlsdW4ISq+kngUOD4dnPmuT5RVY9pDwthkiRJkrREVXVSVe1fVQcCRwEfq6rfAM4Hjm2zHUt3UiKt/agkuyZ5KLAGuLQNpXhHkkPb/cCOmbPM7Lqe1bZRwEeApyTZM8mewFNamyRpCFkMkyRpGVTV5qq6oj2/A7gOx4uXJI2QJAck+Zck1yW5JslLW/teSS5K8oX27549y5yUZEOSzyc5vKf98UmuatPe1A4sSpK0Wl4L/GKSLwC/2F5TVdcA5wDXAv8IHF9Vd7ZlXgS8HdgAfBG4sLW/A3hAkg3A/wZObOu6FXgN8On2eHVrkyQNIYdJlCRpmSU5EHgs8Kk+k5+U5DN0Q278XtsZkyRpGMxe5XxFkvsBlye5CHg+8NGqem2SE+kOAr68XQF9FPAoYF/gn5M8vB1UPJXu/iiXABcAR3DXQUVJkpZdVc3QhimsqluAw+aZ72Tg5D7tlwEH9Wn/LvDsedZ1OnD6UmOWJK0ei2GSJC2jJPcF/h54WVV9c87kK4CHVNWWNqb9B+mG5Zi7ju2+wfKo3Dx1VOIEY10poxLrqMQJg4l1qTduX46bvo/K5zKK2hBRm9vzO5LMXuV8JHfdE+VMugONL2/tZ1fV94Dr2xnzhyTZCOxeVRcDJDkLeCYWwyRJkiQNiMUwSZKWSZJd6Aph766q98+d3lscq6oLkrwtyd5V9fU58233DZZH5eapoxInGOtKGZVYRyVOGEyszz/xw0ta7oSDt+7wTd9X4ubtuqc5VzlPtUIZVbU5yYPabPvRXfk1a1Nr+357PrddkiRJkgbCYpgkScug3QvlHcB1VfVX88zzY8BNVVVJDqG7d+ctqximJEnbNPcq5wVu99VvQi3QPnc7230l9DAapStJl9Ok9ns5rnBdqkG+35P6eU9qvyVJGkcWwyRJWh5PBp4HXJXkytb2CuDBAFV1GvAs4EVJtgLfAY6qqnscHJQkaVDmucr5piT7tKvC9gFubu2bgAN6Ft+f7p6Ym9rzue13s5QroYfRKF1Jupwmtd9vfvd5O3yF61IN8srYSf28J7XfkiSNI4thkiQtg6r6JP3PhO+d5y3AW1YnIkmSts8CVzmfDxwLvLb9e15P+3uS/BWwL919MC+tqjuT3JHkULphFo8B3rxK3ZAkSZKke7AYJkmSJEmC+a9yfi1wTpLjgK8AzwaoqmuSnANcC2wFjq+qO9tyLwLOAHYDLmwPSZIkSRoIi2GSJEmSpG1d5XzYPMucDJzcp/0y4KDli06SJEmSlu5egw5AkiRJkiRJkiRJWikWwyRJkiRJkiRJkjS2LIZJkiRJkiRJkiRpbFkMkyRJkiRJkiRJ0tiyGCZJkiRJkiRJkqSxZTFMkiRJkiRJkiRJY8timCRJkiRJkiRJksaWxTBJkiRJkiRJkiSNLYthkiRJkiRJkiRJGltDUQxLcp8klyb5TJJrkvzpoGOSJEmSJEmSJEnS6Nt50AE03wN+vqq2JNkF+GSSC6vqkkEHJkmSJEmSJEmSpNE1FMWwqipgS3u5S3vU4CKSJEmSJEmSJEnSOBiKYRIBkuyU5ErgZuCiqvrUgEOSJEmSJEmSJEnSiBuKK8MAqupO4DFJ9gA+kOSgqrq6d54k64B1AFNTU8zMzKx6nIuxZcuWoY1tpdn3mUGHseomtd/Qv+8nHLx1MMHAxH4OkiRJkiRJkrSQoSmGzaqqbySZAY4Arp4zbT2wHmDt2rU1PT296vEtxszMDMMa20qz79ODDmPVTWq/oX/fn3/ihwcTDLDx6OmBbVuSJEmSJEmShtVQDJOY5IHtijCS7Ab8AvC5gQYlSZIkSZIkSZKkkTcsV4btA5yZZCe6At05VfWhAcckSZIkSZIkSZKkETcUxbCq+izw2EHHIUmSJEmSJEmSpPEyFMMkSpIkSZIkSZIkSSvBYpgkSZIkSZIkSZLGlsUwSZKWQZIDkvxLkuuSXJPkpX3mSZI3JdmQ5LNJHjeIWCVJkiRJkqRJMhT3DJMkaQxsBU6oqiuS3A+4PMlFVXVtzzxPBda0xxOBU9u/kiRJkiRJklaIV4ZJkrQMqmpzVV3Rnt8BXAfsN2e2I4GzqnMJsEeSfVY5VEmSJEmSJGmiWAyTJGmZJTkQeCzwqTmT9gNu6Hm9iXsWzCRJkiRJkiQtI4dJlCRpGSW5L/D3wMuq6ptzJ/dZpPqsYx2wDmBqaoqZmZltbnfLli2Lmm/QRiVOMNaVMiqxjkqcMJhYTzh465KWm9pt6cvOGpXPRZIkrbwk9wE+DuxKd5zz3Kp6ZZK9gPcBBwIbgedU1W1tmZOA44A7gZdU1Uda++OBM4DdgAuAl1ZVJdkVOAt4PHAL8OtVtbEtcyzwRy2cP6uqM1e4y5KkJbIYJknSMkmyC10h7N1V9f4+s2wCDuh5vT9w49yZqmo9sB5g7dq1NT09vc1tz8zMsJj5Bm1U4gRjXSmjEuuoxAmDifX5J354ScudcPBWTrlqx3ZBNh49vUPLS5KksfI94OerakvbH/tkkguBXwU+WlWvTXIicCLw8iSPBI4CHgXsC/xzkodX1Z1093ReB1xCVww7AriQrnB2W1U9LMlRwOuAX28Ft1cCa+lOcrw8yfmzRTdJ0nBxmERJkpZBkgDvAK6rqr+aZ7bzgWPSORS4vao2r1qQkiRJkjRG2v2Yt7SXu7RH0d2vefYqrTOBZ7bnRwJnV9X3qup6YANwSLuX8+5VdXFVFd2VYL3LzK7rXOCwtv93OHBRVd3aCmAX0RXQJElDyCvDJElaHk8GngdcleTK1vYK4MEAVXUa3dmFT6Pb4fo28ILVD1OSJEmSxkeSnYDLgYcBb62qTyWZmj3xsKo2J3lQm30/uiu/Zs3ex/n77fnc9tllbmjr2prkduABeE9oSRopFsMkSVoGVfVJ+t8TrHeeAo5fnYgkSZIkafy1IQ4fk2QP4ANJDlpg9vnu47zQ/Z2XssxdG1zCPaHnsxz3X12qlbhv6yjdp3dbxqkvYH+Gnf1ZGothkiRJkiSSnA48A7i5qg5qba8Cfhv4WpvtFVV1QZt2Et19VO4EXlJVH2ntjwfOAHajuyr6pe2EEEmSVkxVfSPJDN1QhTcl2addFbYPcHObbb77OG9qz+e29y6zKcnOwP2BW1v79JxlZvrEtd33hJ7Pm9993g7ff3WpVuK+raN0n95tGae+gP0ZdvZnabxnmCRJkiQJugJWv3udvL6qHtMes4WwRwJHAY9qy7ytDVMFcCrdGfBr2sP7p0iSVkSSB7YrwkiyG/ALwOfo7td8bJvtWOC89vx84KgkuyZ5KF2eurQNqXhHkkPb/cCOmbPM7LqeBXysneTxEeApSfZMsifwlNYmSRpCXhkmSZIkSaKqPp7kwEXOfiRwdlV9D7g+yQbgkCQbgd2r6mKAJGcBzwQuXP6IJUliH+DMdkLGvYBzqupDSS4GzklyHPAV4NkAVXVNknOAa4GtwPFtmEWAF3HXlc0XclfuegfwzpbrbqU7GYSqujXJa4BPt/leXVW3rmhvJUlLZjFMkiRJkrSQFyc5BrgMOKGqbgP2Ay7pmWdTa/t+ez63XZKkZVdVnwUe26f9FuCweZY5GTi5T/tlwD3uN1ZV36UV0/pMOx04ffuiliQNgsUwSZIkSdJ8TgVeA1T79xTgN4H0mbcWaL+HJOvohlNkampqZG8CPm43MF+sSe331G5wwsFbB7LtQb7fk/p5T2q/JUkaRxbDJEmSJEl9VdVNs8+T/A3wofZyE3BAz6z7Aze29v37tPdb93pgPcDatWtrVG8CPm43MF+sSe33m999HqdcNZhDKRuPnh7IdmFyP+9J7bckSePoXoMOQJIkSZI0nJLs0/PyV4Cr2/PzgaOS7JrkocAa4NKq2gzckeTQJAGOAc5b1aAlSZIkaQ6vDJMkSZIkkeS9wDSwd5JNwCuB6SSPoRvqcCPwOwBVdU2Sc4Brga3A8VV1Z1vVi4AzgN2AC9tDkiRJkgbGYpgkSZIkiap6bp/mdyww/8nAyX3aLwMOWsbQJEmSJGmHOEyiJEmSJEmSJEmSxpbFMEmSJEmSJEmSJI0ti2GSJEmSJEmSJEkaWxbDJEmSJEmSJEmSNLYshkmSJEmSJEmSJGlsWQyTJEmSJEmSJEnS2LIYJkmSJEmSJEmSpLFlMUySJEmSJEmSJEljy2KYJEmSJEmSJEmSxpbFMEmSJEmSJEmSJI0ti2GSJEmSJEmSJEkaWxbDJEmSJEmSJEmSNLYshkmSJEmSJEmSJGlsWQyTJEmSJEmSJEnS2LIYJknSMkhyepKbk1w9z/TpJLcnubI9/mS1Y5QkSZIkSZIm0c6DDkCSpDFxBvAW4KwF5vlEVT1jdcKRJEmSJEmSBF4ZJknSsqiqjwO3DjoOSZIkSZIkSXc3FFeGJTmA7kz6HwN+AKyvqjcONipJkpbdk5J8BrgR+L2quqbfTEnWAesApqammJmZ2eaKt2zZsqj5Bm1U4gRjXSmjEuuoxAmDifWEg7cuabmp3Za+7KxR+VwkSZIkScNjKIphwFbghKq6Isn9gMuTXFRV1w46MEmSlskVwEOqakuSpwEfBNb0m7Gq1gPrAdauXVvT09PbXPnMzAz95jvwxA8vOeAdtfG1T79H23xxDiNjXRmjEuuoxAmDifX5S/xtOeHgrZxy1Y7tgmw8enqHlpckSZIkTZ6hGCaxqjZX1RXt+R3AdcB+g41KkqTlU1XfrKot7fkFwC5J9h5wWJIkSZIkSdLYG5Yrw34oyYHAY4FP9Zm23cNGDcIoDauz3Oz7zKDDWHWT2m/o3/cdHfppR0zq5zAqkvwYcFNVVZJD6E5IuWXAYUmSJEmSJEljb6iKYUnuC/w98LKq+ubc6UsZNmoQRmlYneVm36cHHcaqm9R+Q/++L3XYqOXgsFGDleS9wDSwd5JNwCuBXQCq6jTgWcCLkmwFvgMcVVU1oHAlSZIkSZKkiTE0xbAku9AVwt5dVe8fdDySJG2PqnruNqa/BXjLKoUjSZIkSZIkqRmKe4YlCfAO4Lqq+qtBxyNJkiRJkiRJkqTxMBTFMODJwPOAn09yZXs8bdBBSZIkSZIkSZIkabQNxTCJVfVJIIOOQ5IkSZIkSZIkSeNlWK4MkyRJkiRJkiRJkpadxTBJkiRJkiRJIyfJAUn+Jcl1Sa5J8tLWvleSi5J8of27Z88yJyXZkOTzSQ7vaX98kqvatDclSWvfNcn7WvunkhzYs8yxbRtfSHLsKnZdkrSdLIZJkiRJkiRJGkVbgROq6ieBQ4HjkzwSOBH4aFWtAT7aXtOmHQU8CjgCeFuSndq6TgXWAWva44jWfhxwW1U9DHg98Lq2rr2AVwJPBA4BXtlbdJMkDReLYZIkSZIkSZJGTlVtrqor2vM7gOuA/YAjgTPbbGcCz2zPjwTOrqrvVdX1wAbgkCT7ALtX1cVVVcBZc5aZXde5wGHtqrHDgYuq6taqug24iLsKaJKkIbPzoAOQJEmSJA1ektOBZwA3V9VBrW0v4H3AgcBG4DntgB9JTqI7W/5O4CVV9ZHW/njgDGA34ALgpe3AoiRJK6YNX/hY4FPAVFVthq5gluRBbbb9gEt6FtvU2r7fns9tn13mhraurUluBx7Q295nmd641tFdccbU1BQzMzNL7uPUbnDCwVuXvPyO2JG457Nly5YVWe8gjFNfwP4MO/uzNBbDJEmSJEnQFbDeQnc2/KzZYaZem+TE9vrlc4aZ2hf45yQPr6o7uWuYqUvoimFHABeuWi8kSRMnyX2BvwdeVlXfbLf76jtrn7ZaoH2py9zVULUeWA+wdu3amp6eni+2bXrzu8/jlKsGczh349HTy77OmZkZduT9GCbj1BewP8PO/iyNwyRKkiRJkqiqjwO3zmlezmGmJEladkl2oSuEvbuq3t+ab2o5ifbvza19E3BAz+L7Aze29v37tN9tmSQ7A/eny5fzrUuSNIQshkmSJEmS5nO3YaaA3mGm+g0NtR/zDzMlSdKyavfuegdwXVX9Vc+k84Fj2/NjgfN62o9KsmuShwJrgEtbjrsjyaFtncfMWWZ2Xc8CPtZO+PgI8JQkeybZE3hKa5MkDSGHSZQkSZIkba8dGjIKlvceKoM0bvdsWKxJ7fe43a9nsSb1857Ufo+YJwPPA65KcmVrewXwWuCcJMcBXwGeDVBV1yQ5B7gW2Aoc34b4BXgRd93z8kLuGuL3HcA7k2yguyLsqLauW5O8Bvh0m+/VVTX3CmtJ0pCwGCZJkiRJms9NSfapqs3LMMzU3SznPVQGadzu2bBYk9rvcbtfz2JN6uc9qf0eJVX1SfqfiAFw2DzLnAyc3Kf9MuCgPu3fpRXT+kw7HTh9sfFKkgbHYRIlSZIkSfNZzmGmJEmSJGkgvDJMkiRJkkSS9wLTwN5JNgGvZHmHmZIkSZKkgbAYJkmSJEmiqp47z6RlGWZKkiRJkgbFYRIlSZIkSZIkSZI0tiyGSZIkSZIkSZIkaWxZDJMkSZIkSZIkSdLYshgmSZIkSZIkSZKksWUxTJIkSZIkSZIkSWPLYpgkSZIkSZIkSZLGlsUwSZKWQZLTk9yc5Op5pifJm5JsSPLZJI9b7RglSZIkSZKkSWQxTJKk5XEGcMQC058KrGmPdcCpqxCTJEmSJEmSNPEshkmStAyq6uPArQvMciRwVnUuAfZIss/qRCdJkiRJkiRNLothkiStjv2AG3peb2ptkiRJkiRJklbQzoMOQJKkCZE+bdV3xmQd3VCKTE1NMTMzs82Vb9mype98Jxy8dXtiXFb94pkvzmFkrCtjVGIdlThhMLEu9bdlarcd/10alc9FkiRJkjQ8LIZJkrQ6NgEH9LzeH7ix34xVtR5YD7B27dqanp7e5spnZmboN9/zT/zw9ke6TDYePX2PtvniHEbGujJGJdZRiRMGE+tSf1tOOHgrp1y1Y7sg/X5bJEmSJElaiMMkSpK0Os4HjknnUOD2qto86KAkSZIkSZKkceeVYZIkLYMk7wWmgb2TbAJeCewCUFWnARcATwM2AN8GXjCYSCVJkiRJkqTJYjFMkqRlUFXP3cb0Ao5fpXAkSZIkSZIkNQ6TKEmSJEmSJEmSpLFlMUySJEmSJEmSJEljy2KYJEmSJEmSJEmSxpbFMEmSJEmSJEmSJI0ti2GSJEmSJEmSJEkaWxbDJEmSJEmSJEmSNLYshkmSJEmSJEmSJGlsWQyTJEmSJEmSJEnS2BqaYliS05PcnOTqQcciSZIkSZIkSZKk8TA0xTDgDOCIQQchSZIkSZIkSZKk8TE0xbCq+jhw66DjkCRJkiRJkiRJ0vgYmmKYJEmSJEmSJEmStNx2HnQA2yPJOmAdwNTUFDMzM4MNaB5btmwZ2thWmn2fGXQYq25S+w39+37CwVsHEwxM7OcgSZIkSZIkSQsZqWJYVa0H1gOsXbu2pqenBxvQPGZmZhjW2FaafZ8edBirblL7Df37/vwTPzyYYICNR08PbNuSJEmSJK22JKcDzwBurqqDWttewPuAA4GNwHOq6rY27STgOOBO4CVV9ZHW/njgDGA34ALgpVVVSXYFzgIeD9wC/HpVbWzLHAv8UQvlz6rqzBXuriRpBzhMoiRJkiRpQUk2JrkqyZVJLmtteyW5KMkX2r979sx/UpINST6f5PDBRS5JGnNnAEfMaTsR+GhVrQE+2l6T5JHAUcCj2jJvS7JTW+ZUutGo1rTH7DqPA26rqocBrwde19a1F/BK4InAIcAre/OgJGn4DE0xLMl7gYuBRyTZlOS4QcckSZIkSfqhn6uqx1TV2vZ6KQcbJUlaNlX1ceDWOc1HArNXaZ0JPLOn/eyq+l5VXQ9sAA5Jsg+we1VdXFVFdyXYM/us61zgsCQBDgcuqqpb21VnF3HPopwkaYgMzTCJVfXcQccgSZIkSVq0I4Hp9vxMYAZ4OT0HG4Hrk2ygO2v+4gHEKEmaPFNVtRmgqjYneVBr3w+4pGe+Ta3t++353PbZZW5o69qa5HbgAb3tfZaRJA2hoSmGSZIkSZKGVgH/lKSAv273c97eg42SJA1S+rTVAu1LXebuG03W0Q3ByNTUFDMzM9sMdD5Tu8EJB29d8vI7Ykfins+WLVtWZL2DME59Afsz7OzP0lgMkyRJkiRty5Or6sZW8LooyecWmHdRBwiX8+DgII3bwYjFmtR+j9uB6MWa1M97Uvs9Bm5Ksk87UWMf4ObWvgk4oGe+/YEbW/v+fdp7l9mUZGfg/nTDMm7irqujZ5eZ6RdMO4FkPcDatWtrenq632yL8uZ3n8cpVw3mcO7Go6eXfZ0zMzPsyPsxTMapL2B/hp39WRqLYZIkSZKkBVXVje3fm5N8gG7Yw+092Dh3nct2cHCQxu1gxGJNar/H7UD0Yk3q5z2p/R4D5wPHAq9t/57X0/6eJH8F7AusAS6tqjuT3JHkUOBTwDHAm+es62LgWcDHqqqSfAT48yR7tvmeApy08l2TJC3VvQYdgCRJkiRpeCX50ST3m31Od8Dvau46QAj3PNh4VJJdkzyUdrBxdaOWJE2CJO+lK1Q9IsmmJMfRFcF+MckXgF9sr6mqa4BzgGuBfwSOr6o726peBLwd2AB8Ebiwtb8DeEC7/+X/Bk5s67oVeA3w6fZ4dWuTJA0prwyTJEmSJC1kCvhAEuj2Id9TVf+Y5NPAOe3A41eAZ0N3sDHJ7MHGrdz9YKMkScumqp47z6TD5pn/ZODkPu2XAQf1af8uLb/1mXY6cPqig5UkDZTFMEmSlkmSI4A3AjsBb6+q186ZPk131vz1ren9VfXq1YxRkqTtVVVfAh7dp/0WtvNgoyRJkiQNgsUwSZKWQZKdgLfSDcOxCfh0kvOr6to5s36iqp6x6gFKkiRJkiRJE8p7hkmStDwOATZU1Zeq6r+As4EjBxyTJEmSJEmSNPEshkmStDz2A27oeb2ptc31pCSfSXJhkketTmiSJEmSJEnS5HKYREmSlkf6tNWc11cAD6mqLUmeBnwQWHOPFSXrgHUAU1NTzMzMbHPjW7Zs6TvfCQdv3eayK6VfPPPFOYyMdWWMSqyjEicMJtal/rZM7bbjv0uj8rlIkiRJkoaHxTBJkpbHJuCAntf7Azf2zlBV3+x5fkGStyXZu6q+Pme+9cB6gLVr19b09PQ2Nz4zM0O/+Z5/4ocX34NltvHo6Xu0zRfnMDLWlTEqsY5KnDCYWJf623LCwVs55aod2wXp99siSZIkSdJCHCZRkqTl8WlgTZKHJrk3cBRwfu8MSX4sSdrzQ+jy8C2rHqkkSZIkSZI0QbwyTJKkZVBVW5O8GPgIsBNwelVdk+SFbfppwLOAFyXZCnwHOKqq5g6lKC3ZgYO8EvC1Tx/YtiVJkiRJkhZiMUySpGVSVRcAF8xpO63n+VuAt6x2XJIkSZIkSdIkc5hESZIkSZIkSZIkjS2LYZIkSZIkSZIkSRpbFsMkSZIkSZIkSZI0tiyGSZIkSZIkSZIkaWxZDJMkSZIkSZIkSdLYshgmSZIkSZIkSZKksWUxTJIkSZIkSZIkSWPLYpgkSZIkSZIkSZLG1s6DDkCSltuBJ354VbZzwsFbef4qbUuSJEmSJEmStDReGSZJkiRJkiRJkqSxZTFMkiRJkiRJkiRJY8timCRJkiRJkiRJksaWxTBJkiRJkiRJkiSNLYthkiRJkiRJkiRJGlsWwyRJkiRJkiRJkjS2LIZJkiRJkiRJkiRpbFkMkyRJkiRJkiRJ0tiyGCZJkiRJkiRJkqSxZTFMkiRJkiRJkiRJY8timCRJkiRJkiRJksaWxTBJkiRJkiRJkiSNLYthkiRJkiRJkiRJGltDUwxLckSSzyfZkOTEQccjSdL22lYuS+dNbfpnkzxuEHFKkrQa3MeTJE0C850kjYahKIYl2Ql4K/BU4JHAc5M8crBRSZK0eIvMZU8F1rTHOuDUVQ1SkqRV4j6eJGkSmO8kaXTsPOgAmkOADVX1JYAkZwNHAtcONCpJkhZvMbnsSOCsqirgkiR7JNmnqjavfriSJK0o9/EkSZNgYvLdgSd+eNnXecLBW3n+Ita78bVPX/ZtS5o8w1IM2w+4oef1JuCJK7nBlfgBn7WtH3J/wCVpLC0ml/WbZz/AYpgkadys+j6eJEkDYL6TpBExLMWw9Gmre8yUrKMbVgpgS5LPr2hUS/QS2Bv4+nzT87pVDGb1Ldj3MTepfZ/Ufm/z//pqW4bflocsQxiTbDG5bCXz3VB9H2He7+TQxbkAY90O2/EbNPBYF2lU4oQRinU5cqf5bmRsM+eNyv7dIozM/8FlZr9X2YCPJfh5jx7z3epY7Xw3yt/Je1js34Yjcix1rD4b7M+wsz93WXS+G5Zi2CbggJ7X+wM3zp2pqtYD61crqKVKcllVrR10HINg3yev75Pab5jsvquvxeSyFct3o/J9HJU4wVhXyqjEOipxgrFqaG0z543K/t22TOr32n5PFvstzWtV8924fSfHqT/j1BewP8PO/izNvVZ6A4v0aWBNkocmuTdwFHD+gGOSJGl7LCaXnQ8ck86hwO3eL0ySNKbcx5MkTQLznSSNiKG4MqyqtiZ5MfARYCfg9Kq6ZsBhSZK0aPPlsiQvbNNPAy4AngZsAL4NvGBQ8UqStJLcx5MkTQLznSSNjqEohgFU1QV0BwnHwcgP9bED7PvkmdR+w2T3XX30y2WtCDb7vIDjV2jzo/J9HJU4wVhXyqjEOipxgrFqSI3ZPt5CJvV7bb8ni/2W5rHK+W7cvpPj1J9x6gvYn2Fnf5Yg3XE5SZIkSZIkSZIkafwMyz3DJEmSJEmSJEmSpGVnMWwZJNmY5KokVya5rLXtleSiJF9o/+456DiXQ5LTk9yc5Oqetnn7muSkJBuSfD7J4YOJesfN0+9XJfnP9rlfmeRpPdPGpd8HJPmXJNcluSbJS1v7JHzm8/V97D93DYd5fnf+b5LPJflskg8k2aO1H5jkOz3fy9N6lnl8y1EbkrwpSVYp1u3+v7LSsc4T5/t6YtyY5MrWPuj3dNl+f1fhfZ0v1qH6vi7n7/oA39Oh+74muU+SS5N8psX6p6196L6r0o5KskeSc9tv23VJntRnnun2//CaJP86iDiX27b6neT+Sf6h53dg5O+JmuQRPb+pVyb5ZpKXzZkn7bdqQ8t1jxtQuMtmkf0+uvX3s0n+LcmjBxTusllMv3vmfUKSO5M8a5XD1IRLn32ZUTXf37qjar6/h0ddkp2S/HuSDw06lh2VPsfuR9Vi/h4dFduTf5dNVfnYwQewEdh7Ttv/AU5sz08EXjfoOJeprz8LPA64elt9BR4JfAbYFXgo8EVgp0H3YRn7/Srg9/rMO0793gd4XHt+P+A/Wv8m4TOfr+9j/7n7GI7HPL87TwF2bs9f1/N/78De+eas51LgSUCAC4GnrlKs2/1/ZaVj7RfnnOmnAH8yJO/psv3+rsL7Ol+sQ/V9Xc7f9UG9p8P4fW3rvW97vgvwKeDQYfyu+vCxow/gTOC32vN7A3vMmb4HcC3w4Pb6QYOOeZX6/Yqe/+MPBG4F7j3ouJex/zsBXwUeMqf9ae23Ku1371ODjnWV+v3/AXu250+dlH73TPsY3b2hnjXoWH1M1oNt7MuM0mMxf+uO0mO+v4cHHdcy9Ot/A+8BPjToWJahLxuZc+x+VB/b+rtsVB8L5d/lfHhl2Mo5ku7LSfv3mYMLZflU1cfpdm56zdfXI4Gzq+p7VXU9sAE4ZDXiXG7z9Hs+49TvzVV1RXt+B3AdsB+T8ZnP1/f5jE3fNRz6/e5U1T9V1db28hJg/4XWkWQfYPequri6vy7OYgXy0XL8Rq5GrAvF2a5AeQ7w3oXWsYrv6bL8/q7S+9o31mH7vi7X7/og39PZ6cP0fa3OlvZyl/YohvC7Ku2IJLvTHYh8B0BV/VdVfWPObP8DeH9VfaXNc/OqBrkCFtnvAu7XfpvuS5drtzI+DgO+WFVfntN+JHBW+x28BNij/ZaNi779rqp/q6rb2stt5vcRNN/nDfC7wN8DI/9/W6NnO/e5htoS/i4fagv8PTyykuwPPB14+6Bj0V0W+XfZqFoo/y4bi2HLo4B/SnJ5knWtbaqqNkP3Iw88aGDRrbz5+rofcEPPfJsY4eQ2jxe34SFO7xn+Zyz7neRA4LF0Z7hM1Gc+p+8wQZ+7htpv0p2NPOuhbQiDf03yM61tP7rv4qzV/l5uz/+VQcf6M8BNVfWFnraheE938Pd3VePt83s5a6i+rzv4uz4M7+lQfV/bECpX0h0cvKiqhv67Ki3BjwNfA/62/V97e5IfnTPPw4E9k8y0fcNjVj/MZbeYfr8F+EngRuAq4KVV9YNVjnMlHUX/kw/G/e//+frd6zjunt/HQd9+J9kP+BXgtHssIWnJFth/GCnz/D08yt4A/AEwLvm837H7UbSYv8tG1WL+7thhFsOWx5Or6nF0QwQcn+RnBx3QkOh3n4eRPjNijlOBnwAeA2ymG64IxrDfSe5Ldwbcy6rqmwvN2qdt3Po+MZ+7hleSP6Q74/rdrWkz3ZBMj6UNZdDOGBrk93J7/68M+v/Qc7n7H15D8Z4uw+/vqsU7X6zD9n1dht/1gb+nDNn3tarurKrH0F0dcEiSgxaYfeDvq7REO9MNT3Vq+7/2LbohQOfO83i6M6kPB/44ycNXNcrlt5h+Hw5cCexL91v6lvYbNPKS3Bv4ZeDv+k3u0zYWv1vb6PfsPD9HVwx7+WrFtdK20e83AC+vqjtXNShpjG3Hvs7Q286/h4dakmcAN1fV5YOOZRmNy7H7xfxdNnIW83fHcrEYtgyq6sb2783AB+iGRrtpdoiE9u84X0Y/X183AQf0zLc/3dmCY6GqbmrJ7gfA33DXkHhj1e8ku9D9cfLuqnp/a56Iz7xf3yflc9fwSnIs8Azg6DaUGG24sVva88vp7sHzcLrvZe/QNav2vVzC/5WBxZpkZ+BXgffNtg3De7pMv7+rEu88sQ7d93WZftcH/Z4O5fe1bfsbwAxwBEP6XZV2wCZgU8+Z3ufSHYyYO88/VtW3qurrwMeBR69ijCthMf1+Ad3wkFVVG4Drgf+2ijGupKcCV1TVTX2mjfPf/wv1myQ/RTd01pGz+WdMLNTvtcDZSTYCzwLeluSZqxibNFbm+1t31M35e3hUPRn45fZ7dzbw80neNdiQdsw8x+5H0WL+LhtFC/7dsZwshu2gJD+a5H6zz+luFn81cD5wbJvtWOC8wUS4Kubr6/nAUUl2TfJQYA3djdHHwpzx4H+F7nOHMep3G/f/HcB1VfVXPZPG/jOfr++T8LlreCU5gu7s21+uqm/3tD8wyU7t+Y/Tff++1IYmuyPJoe07fQyrlI+29//KIGMFfgH4XFX9cIi2Qb+ny/X7uxrxLvB7OVTf1+X6XR/ke9oM1fe1bXuP9ny32fgYwu+qtCOq6qvADUke0ZoOA66dM9t5wM8k2TnJjwBPpLsPyshaZL+/0tpJMgU8AvjSqgW5suZeidvrfOCYdA4Fbp8dHnYMzNvvJA8G3g88r6r+Y1WjWnnz9ruqHlpVB1bVgXQHH/9nVX1wFWOTxsY2/tYdOQv8PTySquqkqtq//d4dBXysqn5jwGEt2QLH7kfOIv8uG0UL/b21vKrKxw486Mbq/Ex7XAP8YWt/APBR4Avt370GHesy9fe9dEPxfJ+uGn3cQn0F/pDu7OTPA08ddPzL3O930o2J/1m6HaF9xrDfP0031Mdn6YY+uRJ42oR85vP1few/dx/D8Zjnd2cD3b0pZr+Tp7V5f63loM8AVwC/1LOetXR/6H2R7p4eWaVYt/v/ykrH2i/O1n4G8MI58w76PV22399VeF/ni3Wovq8LxDmM39W+sQ7j9xX4KeDfW6xXA3/S2ofuu+rDx44+6IYAvKx93z8I7Am8sPf/JPD7dAclrqYb9mngca90v+mGR/yn9lt6NfAbg455mfr9I8AtwP172nr7HeCt7TfrKmDtoGNepX6/HbitJz9dNuiYV6Pfc+Y9A3jWoGP2MVkP5tmXGcXHQn/rjuJjvr+Hx+EBTAMfGnQcO9iHvsfuR/XR7++yQce0g/25R/5dyUfaRiVJkiRJkiRJkqSx4zCJkiRJkiRJkiRJGlsWwyRJkiRJkiRJkjS2LIZJkiRJkiRJkiRpbFkMkyRJkiRJkiRJ0tiyGCZJkiRJkiRJkqSxZTFMkiRJkiRJkiRJY8timCRJkiRJkiRJksaWxTBJkiRJkiRJkiSNLYthkiRJkiRJkiRJGlsWwyRJkiRJkiRJkjS2LIZJkiRJkiRJkiRpbFkMkyRJkiRJkiRJ0tiyGCZJkiRJkiRJkqSxZTFMkiRJkiRJkiRJY8timCRJkiRJkiRJksaWxTBJkiRJkiRJkiSNLYthkiRJkiRJkiRJGlsWwyRJkiRJkiRJkjS2LIZJkiRJkiRJkiRpbFkMkyRJkiRJkiRJ0tiyGCZJkiRJkiRJkqSxZTFMkiRJkiRJkiRJY8timCRJkiRJkiRJksaWxTBJkiRJkiRJkiSNLYthkiRJkiRJkiRJGlsWwyRJkiRJkiRJkjS2LIZJkiRJkiRJkiRpbFkMkyRJkiRJkiRJ0tiyGCZJkiRJkiRJkqSxZTFMkiRJkiRJkiRJY8timCRJkiRJkiRJksaWxTBJkiRJkiRJkiSNLYthkiRJkiRJkiRJGlsWwyRJkiRJkiRJkjS2LIZJkiRJkiRJkiRpbFkMkyRJkiRJkiRJ0tiyGCZJkiRJkiRJkqSxZTFMkiRJkiRJkiRJY8timCRJkiRJkiRJksaWxTBJkiRJkiRJkiSNLYthkiRJkiRJkiRJGlsWwyRJkiRJkiRJkjS2LIZJkiRJkiRJkiRpbFkMkyRJkiRJkiRJ0tiyGCZJkiRJkiRJkqSxZTFMkiRJkiRJkiRJY8timCRJkiRJkiRJksaWxTBpSCU5LckfDzqO7ZHkFUnevsD0jUl+YTVjkiTd3Wr+Fid5RJJ/T3JHkpes8LZ+JsnnV3IbkiRJkiRpNFkM07Ibx4JHkukkm1Zw/c9P8snetqp6YVW9ZiXWvVKq6s+r6rdWY1uSpJHwB8BMVd2vqt4030zLkWer6hNV9YgdWYckSaslyUwS950kSZJWicUwaZkk2XnQMUiStFoWmfceAlwzJLFIkjTxzJmSJGlSWQzTskryTuDBwD8k2ZLkO0l+d848n03yzPa8krwkyZeSfD3J/01yr555fzPJdUluS/KRJA/pmfaoJBcluTXJTUle0dp3TfKGJDe2xxuS7NqmTSfZlOSEJDcn2ZzkBT3rfFqSa9twTv+Z5PeS/ChwIbBv69OWJPsmeVWSc5O8K8k3gecnOSPJn/Ws725nuic5IMn7k3wtyS1J3pLkJ4HTgCe1dX+jzTt3Xb+dZEPr7/lJ9u2ZVklemOQL7b16azr3WHeSJ7T3a+ee5X8tyZXt+Wy/3tfehyuSPLpn3n2T/H3rw/W9w161Zd/V8/p5Sb7c+vqH2/r+SNIka1dW/17Lk7e33+H79LvCt/3uP6w9PyPJ25Jc2H7r/1+SH2v577Ykn0vy2Dmbe0LLd7cl+dsk9+lZ9zOSXNlyxr8l+ak5Mb48yWeBbyXZOckvJ7mmzT/Tcg9JPgb8HPCWFtfDtzPPLiafvzzJV4G/7ZNzF8pXhyS5LMk3W078q2X6GCVJ25DkxCRfbLng2iS/0tofluRfWw78epL3tfYkeX26/bfbW548qE3bNclfJvlK+z0/LclubdreST7U8tOtST6Rtq/Z8sd/thg+n+Sw1v6qJH+Xbh/vjiRXtfx1Utv+DUme0tOX+yd5R7r9yv9M8mdJdmrTnp/kky2+21ouemqbdjLwM9yVI9+Sbh/ulDnv1T8keVl7vq28dnHr6+a2vnv3TK8kxyf5AvCF5f5MJUmSRoHFMC2rqnoe8BXgl6rqvsCxwG/MTk9XVNkPuKBnsV8B1gKPA44EfrPN+0zgFcCvAg8EPgG8t027H/DPwD8C+wIPAz7a1veHwKHAY4BHA4cAf9SzvR8D7t/iOA54a5I927R3AL9TVfcDDgI+VlXfAp4K3FhV922PG9v8RwLnAnsA717ovWk7RR8Cvgwc2LZ/dlVdB7wQuLite48+y/488BfAc4B92jrOnjPbM4AntD4/Bzi837qr6tPALcAv9iz7G8A7e14fCfwdsBfwHuCDSXZpO4//AHymxX8Y8LIkh/eJ+ZHAqcDz6D6jBwD7L/QeSZJ4DnAE8FDgp4Dnb8dyfwTsDXwPuBi4or0+F5hb7DkaOBz4CeDhbVmSPA44Hfgdut/tvwbOny1CNc8Fnk6X+36cLje/jC5XX0B3Qsy9q+rn6XL3i1sO+g+2L88uJp/vRXf12brezi0iX70ReGNV7d7eg3MWfHclScvpi3SFoPsDfwq8K8k+wGuAfwL2pNtveHOb/ynAz9Llqz2AX6fbnwF4XWt/DN0+4X7An7RpJwCb6PLTFN2+ZSV5BPBi4AktHx0ObOyJ75fo9o32BP4d+AjdsZP9gFfT5cZZZwJb27Yf22LtHfrwicDn6fLx/wHekSRV9YfcPUe+uK3ruT0Fu73p8td7F5HX7gT+V9vOk9r0/znnfX9mi+eRSJIkTSCLYVpp5wFrkqxpr58HvK+q/qtnntdV1a1V9RXgDXQH2aA7EPcXVXVdVW0F/hx4TLqrw54BfLWqTqmq71bVHVX1qbbc0cCrq+rmqvoa3Q7W83q29/02/ftVdQGwBXhEz7RHJtm9qm6rqiu20b+Lq+qDVfWDqvrONuY9hK4o9PtV9a0W92Lv5XU0cHpVXVFV3wNOorva68CeeV5bVd9o7+O/0O0QzudMWpEyyV50O4Dv6Zl+eVWdW1XfpzuAeh+6A5JPAB5YVa+uqv+qqi8BfwMc1WcbzwI+VFUfbzH/MfCDRfZXUo8kp7ezka9e5PzPaWdaX5PkPdteQkPkTVV1Y1XdSnfQ6zGLXO4DVXV5VX0X+ADw3ao6q6ruBN5Hd4Cu11uq6oa2nZO5K/f+NvDXVfWpqrqzqs6kK64dOifGG1re+3Xgw1V1UcsZfwnsBvx/88S5PXl2W/n8B8Arq+p7fXLwtvLV94GHJdm7qrZU1SULxCFplZjvJkNV/V3LdT+oqvfRXal0CN1v80OAfefsK30fuB/w34C0/cPNSUKXt/5X25+8g26fsfe3fh/gIW3f7xNVVXSFo13p8tEuVbWxqr7YE+InquojbR/07+iKaa9tee5s4MAkeySZojuZ42Vt/+5m4PXcfd/oy1X1Ny0fn9nimZrnfbkUuJ2ukEVbz0xV3cQ28lr7G+CSqtpaVRvpCnb/fc4m/qK9T9vab5UkSRpLFsO0oloR5BzgN9rZbM/l7lcgAdzQ8/zLdAUj6HaE3tiGevgGcCsQujPhDqA7o7Cffdt6+q0T4Ja2YzPr28B92/NfA54GfDndEB1P2kYXb9jG9F4H0O0Mbd3mnPd0tz5V1Ra6syH365nnqz3Pe/vUz7uAX0pyX7qrCT5RVZt7pv+wX1X1A7ozKvel7ZzOfibtc3kF/Xfo9p2znm9x1xmckrbPGXRXC21TO/ngJODJVfUouit2NDq257e81009z7/T5/Xc9SyUe0+Y8zt/AHfPo73Lzs1PP2jTe/NTr+3Js9vK519rxb9+tpWvjqO7kuBzST6d5BkLxCFp9ZyB+W7sJTkmdw3H+w26K4X3Bv6Abn/v0lbg/E2AqvoY8BbgrcBNSdYn2Z2uSPUjwOU96/rH1g7wf4ENwD+lG5b/xLa+DXTfl1cBNyc5Oz1D0HPPHPr1VsyafQ1dXn0IsAuwuWf7fw08qGf5H+b1qvp2z7Lz+eFJi9x99I4F81q6oRw/lOSr6Ybw//P2nvbann1XSfr/27v7cEvPuj703x8JYIgEFGQMmdhEDWhIAGWMtHjsCFUHRIOtnAZRCGLnQCNCGyuJnlPaak7xsqhQBc4Y0kkUiJEXSQkvIrpLrUl4E8gb4JSkZEgggrwNXkIn/M4f6xlZmdl7z8zaa++1Zu3P57r2NWvdz8v67nvWWs9e6/fc9wOwcBTDWA990P3LMzq7+wlJ/ra7rz1o+aljt78lyYEpCG/PaCqlB479nNDdfzEs+7YVHv+OjD4sLLfP1YN3v6e7z83oA8wf5WvTJh38O2WF9i9l9IHsgG8eu317km+p5S9YvNL+D7jH71Sj66s8KMknDrPdsvvu7k9kNIXWj2d0lv3BBcq//z8Ziphbhwy3J7n1oP+T+3f3k5Z53DsP2s/9hszAUerud2V0QsDfq6pvq6q3VdX7anQNjO8YFv2LJL/T3Z8dtr1rg+Myffc4tlTVN6+y7pFa7dh7yUHv8/fr7teOrT9+XDn4+FTDvpc9Ph3lcfZwx/PVjp2rHq+6+6+6+2lDjl9L8rrh2ArMkOPd4htm+fjdjKYpfFCPpoi/MaMRX5/s7n/R3Q/NaJaQl9dwfczufll3PybJIzI6meHfJPl0RsWpR4y91z+gR9P1p0ezh1zY3d+a0dSH/7qGa4N192u6+/syOs50RseCo3V7RqOnHzz2+CcNxdkjsdxx7PeTnDtcXuA7MzpWHnis1T6HvSLJh5Oc0aMpgH8po8Li4R4PAGDTUAxjPXwqo2uIJEmG4tdXk7wkhxZdkuTfVNU3VNWpSZ6f0XROSfLKJBdX1SOSv7848VOHZW9O8s1V9YIaXTT5/lX1vcOy1yb5v6vqm4Z51v9tRh8qVlVV96mqp1fVA4YpML6Q0RQaB36nB1XVAw6zmw8keVJVfePwZeULxpa9O6MC0Yur6sSq+rqqetzY/rfW2EWOD/KaJM+qqkfX6Lot/2+S64cpMA5npX1fkdHZl2dnNKXWuMdU1T8dCncvyOhD3nXD7/CFGl1w+oSqOq6qzqqq71nmcV+X5MlV9X3DY/+HeM+BadqV5HnDF0O/kOTlQ/vDkjysqv5HVV1XVUd0hj1z7YNJHjEcA74uozPZ1+qCqtpao6lyfylfO/b+bpLnVNX31siJVfUjNbpW53KuSvIjVfWEqrp3Rtdn+XKSvzh4xQmOsxMdzwerHq+q6qeq6puGkWyfG7a5e6WdATPleLdYTsyoKPPXSVJVz8poZFiq6qlVdeAaw58d1ru7qr5nOC7dO6MTRP4uyd3De/jvJvnNqnrIsI9TariOVlU9uaq+fThR48Ax5+6qenhVPX74XPV3GRXUjvoYMMys8cdJXlJVJ1XVvYbi7cHTE67kHp+bh33uTfKejD43v35sSsPDfQ67//A77hsKxs892t8HAGDR+WKa9fAfM/ry6nNV9QtD2xUZFV2W+xLrTUnel1Eh6Zokr0qS7n5jRmfoXTlM9XBjRnOyZ5gP/gczOsPvkxnNM/8Dw/5+Ncl7k3woyQ1J3j+0HYmfTnLb8HjPyTBFRXd/OKMv5T42/F4PXWH738voS8vbMvpgdODLxQxTa/xoRhdX/nhGUw/+82Hxnya5Kcknq+rTB++0u9+Z0TW3Xp9RQe3bsvx1upaz0r7fmNGZkG8cpjAc96Yh22cz6pN/Osyzf+B3eHSSWzM6G/PSjC5+fXDmm5JckFEh785hX3uPMDOwihpNcfqPkvxhVX0goyl5Th4WH5/kjCTbM5qa9tKqeuDGp2RauvujGZ1Q8CcZHe+O9HqTq3lNRsepjw0/vzo81nszGm3x2xm9b+9Jcv4q2T6S0bHyP2d0TPjRJD/a97w26LijOc5OfDw/guPVjiQ3VdW+JC9Nct4qUy4CM+J4t3i6++aMTpK8NqNi0NlJ/sew+HuSXD+8N1+d5PndfWuSkzIqen02oylzP5PRNSqT5IUZHauuG44tf5KvXQ/6jOH+vuHxXt7dSxldL+zFGR0bPpnRKOFfmvBXekaS+yS5ecj3unztOXo4L03yE1X12ap62Vj75Rn1y9+fSHoEx7VfSPKTSb6YUV/9QQAAuIfqNlKe9VdVz0iyc5iKYry9M5rKYc9skm1uVfU/M5qK8k/G2v5dkm/v7p9acUNgw1XVaUne3N1n1eg6GR/p7kO+bKmqVya5rrt3D/ffmeSi7n7PRuYFgEk43rHZVdX3Z3QS6WnD6DcAAKbAyDDWXY2uFfUvM5rihDlRVf8so6lH/nTWWYCj091fSHLrgaljh+nsHjUs/qMMI2WHqeUeltHIHwA4pjjesdkMU0E+P8mlCmEAANN12GJYVV1WVXdV1Y3LLPuFqurhw8eBtourak9VfeTAXN1D+2Oq6oZh2cuGebtTo+s9/cHQfv1wJiALYngO/HVGU2C8ZsZxGFTVUkYXWb7AhyyYf1X12oym93l4Ve2tqmcneXqSZ1fVBzOaCvXcYfW3J/lMVd2c5M+S/Jvu/swscgPA0XC8YzOrqu/M6DqWJyf5rZmGAQBYQIedJnEYor8vyRXdfdZY+6kZzVH9HUke092frqozM7rewzlJHprR/NwP6+67q+rdGZ3hdF2StyR5WXe/tar+ZZJHdvdzquq8JD/e3f88AAAAAAAAsEaHHRnW3e9K8jfLLPrNJL+Y0TRrB5yb5Mru/vJwods9Sc6pqpOTnNTd1/ao+nZFkqeMbXP5cPt1SZ5wYNQYAAAAAAAArMVE1wyrqh9L8onu/uBBi05JcvvY/b1D2ynD7YPb77FNd+9P8vkkD5okFwAAAAAAAIw7/mg3qKr7JfnlJD+03OJl2nqV9tW2We6xdybZmSQnnHDCY0499dTD5l3JV7/61dzrXhPVAheWPjmUPjmUPjnUovTJRz/60U939zfNOgf39OAHP7hPO+20FZd/6UtfyoknnrhxgY6CbEdvXnMlsk1KtsmsZ7b3ve99jndz6HDHu8PZrM/ntZjXXIlsk5JtMvOaba25HO8A4J6OuhiW5NuSnJ7kg8NshluTvL+qzsloxNd4hWprkjuG9q3LtGdsm71VdXySB2T5aRnT3buS7EqSbdu29Xvf+94J4o8sLS1l+/btE2+/iPTJofTJofTJoRalT6rqf806A4c67bTTstrxbp6ff7IdvXnNlcg2Kdkms57ZHO/m0+GOd4ezWZ/PazGvuRLZJiXbZOY121pzOd4BwD0d9VCG7r6hux/S3ad192kZFbO+u7s/meTqJOdV1X2r6vQkZyR5d3ffmeSLVfXY4Xpgz0jypmGXVyd55nD7J5L86XBdMQAAAAAAAFiTwxbDquq1Sa5N8vCq2ltVz15p3e6+KclVSW5O8rYkF3T33cPi5ya5NMmeJP8zyVuH9lcleVBV7Unyr5NcNOHvAgAzU1WXVdVdVXXjYdb7nqq6u6p+YqOyAQAAAMBmdthpErv7aYdZftpB9y9Jcsky6703yVnLtP9dkqceLgcAzLndSX47yRUrrVBVxyX5tSRv36BMAAAAALDpHfU0iQDAobr7XVnhmpdjnpfk9UnuWv9EAAAAAEByBCPDAIC1q6pTkvx4kscn+Z7DrLszyc4k2bJlS5aWllZcd9++fasunyXZjt685kpkm5Rsk5nnbAAAABx7FMMAYGP8VpIXdvfdVbXqit29K8muJNm2bVtv3759xXWXlpay2vJZku3ozWuuRLZJyTaZec7GkauqB2Z03eizknSSn+nua2caCgAA2JQUwwBgY2xLcuVQCHtwkidV1f7u/qOZpgKA9fPSJG/r7p+oqvskud+sAwEAAJuTYhgAbIDuPv3A7araneTNCmEALKqqOinJ9yc5P0m6+ytJvjLLTAAAwOalGAYAU1BVr02yPcmDq2pvkhcluXeSdPcrZxgNAGbhW5P8dZL/UlWPSvK+JM/v7i8dWOForpF5OPN8nbl5zTavuRLZJiXbZOY127zmAoBjlWIYAExBdz/tKNY9fx2jAMA8OD7Jdyd5XndfX1UvTXJRkv/nwApHc43Mw5nn68zNa7Z5zZXINinZJjOv2eY1FwAcqxTDNpnTLrpmxWUXnr0/56+yfK1ue/GPrNu+AVjeau/76233jhNn9tgAzNzeJHu7+/rh/usyKoYBHDP8LQ0Ai+Nesw4AAADAYunuTya5vaoePjQ9IcnNM4wEAABsYkaGAQAAsB6el+TVVXWfJB9L8qwZ5wEAADYpxTAAAACmrrs/kGTbrHMAAACYJhEAAAAAAICFpRgGAAAAAADAwlIMAwAAAAAAYGEphgEAAAAAALCwFMMAAAAAAABYWIphAAAAAAAALCzFMAAAAAAAABaWYhgAAAAAAAALSzEMAAAAAACAhaUYBgAAAAAAwMJSDAMAAAAAAGBhKYYBAAAAAACwsBTDAAAAAAAAWFiKYQAAAAAAACwsxTAAAAAAAAAWlmIYAAAAAAAAC0sxDAAAAAAAgIWlGAYAAAAAAMDCOmwxrKouq6q7qurGsbZfr6oPV9WHquqNVfXAsWUXV9WeqvpIVf3wWPtjquqGYdnLqqqG9vtW1R8M7ddX1WnT/RUBAAAAAADYrI5kZNjuJDsOantHkrO6+5FJPprk4iSpqjOTnJfkEcM2L6+q44ZtXpFkZ5Izhp8D+3x2ks9297cn+c0kvzbpLwMAAAAAAADjDlsM6+53Jfmbg9r+uLv3D3evS7J1uH1ukiu7+8vdfWuSPUnOqaqTk5zU3dd2dye5IslTxra5fLj9uiRPODBqDAAAAAAAANbi+Cns42eS/MFw+5SMimMH7B3a/vdw++D2A9vcniTdvb+qPp/kQUk+ffADVdXOjEaXZcuWLVlaWpo49L59+9a0/bHqwrP3r7hsywmrL1+rY7G/N+vzZDX65FD6BAAAAABgfq2pGFZVv5xkf5JXH2haZrVepX21bQ5t7N6VZFeSbNu2rbdv3340ce9haWkpa9n+WHX+RdesuOzCs/fnJTdMoz66vNuevn3d9r1eNuvzZDX65FD6BAAAAABgfh3JNcOWVVXPTPLkJE8fpj5MRiO+Th1bbWuSO4b2rcu032Obqjo+yQNy0LSMADDvquqyqrqrqm5cYfnTq+pDw89fVNWjNjojAAAAAGxGExXDqmpHkhcm+bHu/tuxRVcnOa+q7ltVpyc5I8m7u/vOJF+sqscO1wN7RpI3jW3zzOH2TyT507HiGgAcK3Yn2bHK8luT/OPufmSSX8kw0hkAAAAAWF+HnROvql6bZHuSB1fV3iQvSnJxkvsmeceotpXruvs53X1TVV2V5OaMpk+8oLvvHnb13Iy+KDwhyVuHnyR5VZLfq6o9GY0IO286vxoAbJzufldVnbbK8r8Yu3td7jliGgAAAABYJ4cthnX305ZpftUq61+S5JJl2t+b5Kxl2v8uyVMPlwMAFsiz87WTQgAAAACAdXTYYhgAMD1V9QMZFcO+b5V1dibZmSRbtmzJ0tLSivvbt2/fqssvPHv/hEnX7nDZZmles81rrkS2Sck2mXnOBgAAwLFHMQwANkhVPTLJpUme2N2fWWm97t6V4Zpi27Zt6+3bt6+4z6Wlpay2/PyLrpkw7drt3nHiqtlm6XD9NivzmiuRbVKyTWaeswEAAHDsudesAwDAZlBV35LkDUl+urs/Ous8AAAAALBZGBkGAFNQVa9Nsj3Jg6tqb5IXJbl3knT3K5P82yQPSvLyqkqS/d29bTZpAQAAAGDzUAwDgCno7qcdZvnPJvnZDYoDAAAAAAxMkwgAAAAAAMDCMjIMAACAqauq25J8McndMT0wAAAwQ4phAAAArJcf6O5PzzoEAACwuZkmEQAAAAAAgIVlZBgAAADroZP8cVV1kv+vu3eNL6yqnUl2JsmWLVuytLQ08QPt27dvTduvp3nNNq+5EtkmJdtkVst24dn7NzbMmHnuMwA4FimGAQAAsB4e1913VNVDkryjqj7c3e86sHAoju1Kkm3btvX27dsnfqClpaWsZfv1NK/Z5jVXItukZJvMatnOv+iajQ0zZveOE+e2zwDgWGSaRAAAAKauu+8Y/r0ryRuTnDPbRAAAwGalGAYAAMBUVdWJVXX/A7eT/FCSG2ebCgAA2KxMkwgAAMC0bUnyxqpKRp87X9Pdb5ttJAAAYLNSDAMAAGCquvtjSR416xwAAACJaRIBAAAAAABYYIphAAAAAAAALCzFMAAAAAAAABaWYhgAAAAAAAALSzEMAAAAAACAhaUYBgAAAAAAwMJSDAMAAAAAAGBhKYYBAAAAAACwsBTDAAAAAAAAWFiKYQAAAAAAACwsxTAAAAAAAAAWlmIYAAAAAAAAC0sxDAAAAAAAgIV12GJYVV1WVXdV1Y1jbd9YVe+oqr8a/v2GsWUXV9WeqvpIVf3wWPtjquqGYdnLqqqG9vtW1R8M7ddX1WlT/h0BAAAAAADYpI5kZNjuJDsOarsoyTu7+4wk7xzup6rOTHJekkcM27y8qo4btnlFkp1Jzhh+Duzz2Uk+293fnuQ3k/zapL8MAAAAAAAAjDtsMay735Xkbw5qPjfJ5cPty5M8Zaz9yu7+cnffmmRPknOq6uQkJ3X3td3dSa44aJsD+3pdkiccGDUGAAAAAAAAazHpNcO2dPedSTL8+5Ch/ZQkt4+tt3doO2W4fXD7Pbbp7v1JPp/kQRPmAgAAAAAAgL93/JT3t9yIrl6lfbVtDt151c6MplrMli1bsrS0NEHEkX379q1p+2PVhWfvX3HZlhNWX75Wx2J/b9bnyWr0yaH0CQAAAADA/Jq0GPapqjq5u+8cpkC8a2jfm+TUsfW2JrljaN+6TPv4Nnur6vgkD8ih0zImSbp7V5JdSbJt27bevn37hPFHhZm1bH+sOv+ia1ZcduHZ+/OSG6ZdH/2a256+fd32vV426/NkNfrkUPqEJKmqy5I8Ocld3X3WMssryUuTPCnJ3yY5v7vfv7EpAQAAAGDzmXSaxKuTPHO4/cwkbxprP6+q7ltVpyc5I8m7h6kUv1hVjx2+DHzGQdsc2NdPJPnT4bpiAHAs2Z1kxyrLn5jRcfGMjEY5v2IDMgEAAADApnfYYUBV9dok25M8uKr2JnlRkhcnuaqqnp3k40memiTdfVNVXZXk5iT7k1zQ3XcPu3puRl8UnpDkrcNPkrwqye9V1Z6MRoSdN5XfDAA2UHe/q6pOW2WVc5NcMZzwcV1VPfDAKOuNSQgAAAAAm9Nhi2Hd/bQVFj1hhfUvSXLJMu3vTXLItFHd/XcZimkAsMBOSXL72P29Q5tiGAAAAACso/W7QBQAMK6WaVt2WuCq2pnRVIrZsmVLlpaWVtzpvn37Vl1+4dn7jybjVB0u2yzNa7Z5zZXINinZJjPP2QAAADj2KIYBwMbYm+TUsftbk9yx3IrdvSvJriTZtm1bb9++fcWdLi0tZbXl5190zdEnnZLdO05cNdssHa7fZmVecyWyTUq2ycxzNgAAAI4995p1AADYJK5O8owaeWySz7teGAAAAACsPyPDAGAKquq1SbYneXBV7U3yoiT3TpLufmWStyR5UpI9Sf42ybNmkxQAAAAANhfFMACYgu5+2mGWd5ILNigOAAAAADAwTSIAAAAAAAALSzEMAAAAAACAhaUYBgAAAAAAwMJSDAMAAAAAAGBhKYYBAAAAAACwsBTDAAAAAAAAWFiKYQAAAExdVR1XVX9ZVW+edRYAAGBzUwwDAABgPTw/yS2zDgEAAKAYBgAAwFRV1dYkP5Lk0llnAQAAOH7WAQAAAFg4v5XkF5Pcf6UVqmpnkp1JsmXLliwtLU38YPv27VvT9utpXrPNa65EtknJNpnVsl149v6NDTNmnvsMAI5FimEAAABMTVU9Ocld3f2+qtq+0nrdvSvJriTZtm1bb9++4qqHtbS0lLVsv57mNdu85kpkm5Rsk1kt2/kXXbOxYcbs3nHi3PYZAByLTJMIAADAND0uyY9V1W1Jrkzy+Kr6/dlGAgAANjPFMAAAAKamuy/u7q3dfVqS85L8aXf/1IxjAQAAm5hiGAAAAAAAAAvLNcMAAABYF929lGRpxjEAAIBNzsgwAAAAAAAAFpZiGAAAAAAAAAtLMQwAAAAAAICFpRgGAAAAAADAwlIMAwAAAAAAYGEphgEAAAAAALCwFMMAAAAAAABYWIphAAAAAAAALCzFMAAAAAAAABbWmophVfWvquqmqrqxql5bVV9XVd9YVe+oqr8a/v2GsfUvrqo9VfWRqvrhsfbHVNUNw7KXVVWtJRcAAAAAAAAkayiGVdUpSX4+ybbuPivJcUnOS3JRknd29xlJ3jncT1WdOSx/RJIdSV5eVccNu3tFkp1Jzhh+dkyaCwAAAAAAAA5Y6zSJxyc5oaqOT3K/JHckOTfJ5cPyy5M8Zbh9bpIru/vL3X1rkj1Jzqmqk5Oc1N3XdncnuWJsGwAAAAAAAJjYxMWw7v5Ekv+U5ONJ7kzy+e7+4yRbuvvOYZ07kzxk2OSUJLeP7WLv0HbKcPvgdgA4plTVjmEq4D1VddEyyx9QVf+1qj44TDP8rFnkBAAAAIDN5PhJNxyuBXZuktOTfC7JH1bVT622yTJtvUr7co+5M6PpFLNly5YsLS0dReJ72rdv35q2P1ZdePb+FZdtOWH15Wt1LPb3Zn2erEafHEqfkCTD1L+/k+QHMzqx4z1VdXV33zy22gVJbu7uH62qb0rykap6dXd/ZQaRAQAAAGBTmLgYluSfJLm1u/86SarqDUn+UZJPVdXJ3X3nMAXiXcP6e5OcOrb91oymVdw73D64/RDdvSvJriTZtm1bb9++feLwS0tLWcv2x6rzL7pmxWUXnr0/L7lhLU+J1d329O3rtu/1slmfJ6vRJ4fSJwzOSbKnuz+WJFV1ZUYnjYwXwzrJ/auqknx9kr9Jsn5nIQAAAAAAa7pm2MeTPLaq7jd8qfeEJLckuTrJM4d1npnkTcPtq5OcV1X3rarTk5yR5N3DVIpfrKrHDvt5xtg2AHCsWGk64HG/neQ7Mzrp44Ykz+/ur25MPAAAAADYnCYeBtTd11fV65K8P6Oz2v8yo1FbX5/kqqp6dkYFs6cO699UVVdldIb8/iQXdPfdw+6em2R3khOSvHX4AYBjyZFM+/vDST6Q5PFJvi3JO6rqv3f3F+6xo6OYFvhw03Su5/S3hzPPU4jOa7Z5zZXINinZJjPP2QAAADj2rGlOvO5+UZIXHdT85YxGiS23/iVJLlmm/b1JzlpLFgCYsZWmAx73rCQv7u5Osqeqbk3yHUnePb7S0UwLfLhpOlebHne97d5x4txOITqv05vOa65EtknJNpl5zgYAAMCxZy3TJAIAX/OeJGdU1elVdZ8k52U0RfC4j2c4YaSqtiR5eJKPbWhKAAAAANhk1jQyDAAY6e79VfVzSd6e5Lgklw1TBD9nWP7KJL+SZHdV3ZDRtIov7O5Pzyw0AAAAAGwCimEAMCXd/ZYkbzmo7ZVjt+9I8kMbnQsAAAAANjPTJAIAAAAAALCwFMMAAAAAAABYWIphAAAAAAAALCzFMAAAAAAAABaWYhgAAAAAAAALSzEMAAAAAACAhaUYBgAAAAAAwMJSDAMAAAAAAGBhKYYBAAAAAACwsBTDAAAAAAAAWFiKYQAAAAAAACwsxTAAAAAAAAAWlmIYAAAAU1VVX1dV766qD1bVTVX172edCQAA2LyOn3UAAAAAFs6Xkzy+u/dV1b2T/HlVvbW7r5t1MAAAYPNRDAMAAGCquruT7Bvu3nv46dklAgAANjPTJAIAADB1VXVcVX0gyV1J3tHd1884EgAAsEkZGQYAAMDUdffdSR5dVQ9M8saqOqu7bzywvKp2JtmZJFu2bMnS0tLEj7Vv3741bb+e5jXbvOZKZJuUbJNZLduFZ+/f2DBj5rnPAOBYpBgGAADAuunuz1XVUpIdSW4ca9+VZFeSbNu2rbdv3z7xYywtLWUt26+nec02r7kS2SYl22RWy3b+RddsbJgxu3ecOLd9BgDHItMkAgAAMFVV9U3DiLBU1QlJ/kmSD880FAAAsGkZGQYAAMC0nZzk8qo6LqOTMK/q7jfPOBMAALBJKYYBAAAwVd39oSTfNescAAAAiWkSAQAAAAAAWGCKYQAAAAAAACwsxTAAAAAAAAAWlmIYAAAAAAAAC2tNxbCqemBVva6qPlxVt1TVP6yqb6yqd1TVXw3/fsPY+hdX1Z6q+khV/fBY+2Oq6oZh2cuqqtaSCwAAAAAAAJK1jwx7aZK3dfd3JHlUkluSXJTknd19RpJ3DvdTVWcmOS/JI5LsSPLyqjpu2M8rkuxMcsbws2ONuQAAAAAAAGDyYlhVnZTk+5O8Kkm6+yvd/bkk5ya5fFjt8iRPGW6fm+TK7v5yd9+aZE+Sc6rq5CQndfe13d1JrhjbBgAAAAAAACa2lpFh35rkr5P8l6r6y6q6tKpOTLKlu+9MkuHfhwzrn5Lk9rHt9w5tpwy3D24HgGNKVe0YpgLeU1UXrbDO9qr6QFXdVFX/baMzAgAAAMBmc/wat/3uJM/r7uur6qUZpkRcwXLXAetV2g/dQdXOjKZTzJYtW7K0tHRUgcft27dvTdsfqy48e/+Ky7acsPrytToW+3uzPk9Wo08OpU9IkmHq399J8oMZndjxnqq6urtvHlvngUlenmRHd3+8qh6y7M4AAAAAgKlZSzFsb5K93X39cP91GRXDPlVVJ3f3ncMUiHeNrX/q2PZbk9wxtG9dpv0Q3b0rya4k2bZtW2/fvn3i8EtLS1nL9seq8y+6ZsVlF569Py+5YS1PidXd9vTt67bv9bJZnyer0SeH0icMzkmyp7s/liRVdWVGUwTfPLbOTyZ5Q3d/PEm6+65D9gIAAAAATNXE0yR29yeT3F5VDx+anpDRF35XJ3nm0PbMJG8abl+d5Lyqum9VnZ7kjCTvHqZS/GJVPbaqKskzxrYBgGPFStMBj3tYkm+oqqWqel9VPWPD0gEAAADAJrXWYUDPS/LqqrpPko8leVZGBbarqurZST6e5KlJ0t03VdVVGRXM9ie5oLvvHvbz3CS7k5yQ5K3DDwAcS45k2t/jkzwmoxNITkhybVVd190fvceOjmJa4MNN07me098ezjxPITqv2eY1VyLbpGSbzDxnAwAA4NizpmJYd38gybZlFj1hhfUvSXLJMu3vTXLWWrIAwIytNB3wwet8uru/lORLVfWuJI9Kco9i2NFMC3y4aTpXmx53ve3eceLcTiE6r9ObzmuuRLZJyTaZec4GAADAsWfiaRIBgHt4T5Izqur0YcT0eRlNETzuTUn+j6o6vqrul+R7k9yywTkBAAAAYFNZ6zSJAECS7t5fVT+X5O1Jjkty2TBF8HOG5a/s7luq6m1JPpTkq0ku7e4bZ5caAAAAABafYhgATEl3vyXJWw5qe+VB9389ya9vZC4AAAAA2MxMkwgAAAAAAMDCUgwDAAAAAABgYSmGAQAAAAAAsLAUwwAAAAAAAFhYimEAAAAAAAAsLMUwAAAAAAAAFpZiGAAAAAAAAAtLMQwAAAAAAICFpRgGAAAAAADAwlIMAwAAAAAAYGEphgEAAAAAALCwFMMAAAAAAABYWIphAAAAAAAALCzFMAAAAAAAABaWYhgAAAAAAAALSzEMAAAAAACAhaUYBgAAAAAAwMJSDAMAAGCqqurUqvqzqrqlqm6qqufPOhMAALB5HT/rAAAAACyc/Uku7O73V9X9k7yvqt7R3TfPOhgAALD5GBkGAADAVHX3nd39/uH2F5PckuSU2aYCAAA2K8UwAAAA1k1VnZbku5JcP+MoAADAJmWaRAAAANZFVX19ktcneUF3f+GgZTuT7EySLVu2ZGlpaeLH2bdv35q2X0/zmm1ecyWyTUq2yayW7cKz929smDHz3GcAcCxSDAMAAGDqqureGRXCXt3dbzh4eXfvSrIrSbZt29bbt2+f+LGWlpaylu3X07xmm9dciWyTkm0yq2U7/6JrNjbMmN07TpzbPgOAY5FpEgEAAJiqqqokr0pyS3f/xqzzAAAAm5tiGAAAANP2uCQ/neTxVfWB4edJsw4FAABsTqZJBAAAYKq6+8+T1KxzAAAAJFMYGVZVx1XVX1bVm4f731hV76iqvxr+/YaxdS+uqj1V9ZGq+uGx9sdU1Q3DspcNU2oAAAAAAADAmkxjmsTnJ7ll7P5FSd7Z3WckeedwP1V1ZpLzkjwiyY4kL6+q44ZtXpFkZ5Izhp8dU8gFAAAAAADAJremYlhVbU3yI0kuHWs+N8nlw+3LkzxlrP3K7v5yd9+aZE+Sc6rq5CQndfe13d1JrhjbBgCOGVW1Yxj9vKeqLlplve+pqrur6ic2Mh8AAAAAbEZrHRn2W0l+MclXx9q2dPedSTL8+5Ch/ZQkt4+tt3doO2W4fXA7ABwzhtHOv5PkiUnOTPK0YVT0cuv9WpK3b2xCAAAAANicjp90w6p6cpK7uvt9VbX9SDZZpq1XaV/uMXdmNJ1itmzZkqWlpSPKupx9+/ataftj1YVn719x2ZYTVl++Vsdif2/W58lq9Mmh9AmDc5Ls6e6PJUlVXZnRqOibD1rveUlen+R7NjYeAAAAAGxOExfDkjwuyY9V1ZOSfF2Sk6rq95N8qqpO7u47hykQ7xrW35vk1LHttya5Y2jfukz7Ibp7V5JdSbJt27bevn37xOGXlpaylu2PVedfdM2Kyy48e39ecsNanhKru+3p29dt3+tlsz5PVqNPDqVPGCw3Avp7x1eoqlOS/HiSx0cxDAAAAAA2xMSVj+6+OMnFSTKMDPuF7v6pqvr1JM9M8uLh3zcNm1yd5DVV9RtJHprkjCTv7u67q+qLVfXYJNcneUaS/zxpLgCYkSMZ6fxbSV44HPtW3tFRjIQ+3MjE9RzxezjzPGpyXrPNa65EtknJNpl5zgYAAMCxZz2GAb04yVVV9ewkH0/y1CTp7puq6qqMpovan+SC7r572Oa5SXYnOSHJW4cfADiWrDQCety2JFcOhbAHJ3lSVe3v7j8aX+loRkIfbmTiaiOC19vuHSfO7ajJeR3ROa+5EtkmJdtk5jkbAAAAx56pFMO6eynJ0nD7M0mesMJ6lyS5ZJn29yY5axpZAGBG3pPkjKo6PcknkpyX5CfHV+ju0w/crqrdSd58cCEMAAAAAJiu9btAFABsIt29v6p+LsnbkxyX5LJhVPRzhuWvnGlAAAAAANikFMMAYEq6+y1J3nJQ27JFsO4+fyMyAQAAAMBmd69ZBwAAAAAAAID1ohgGAAAAAADAwlIMAwAAAAAAYGEphgEAAAAAALCwFMMAAAAAAABYWIphAAAAAAAALCzFMAAAAAAAABaWYhgAAAAAAAALSzEMAAAAAACAhaUYBgAAAAAAwMJSDAMAAAAAAGBhKYYBAAAAAACwsBTDAAAAAAAAWFiKYQAAAAAAACwsxTAAAAAAAAAWlmIYAAAAAAAAC0sxDAAAAAAAgIWlGAYAAAAAAMDCUgwDAAAAAABgYSmGAQAAAAAAsLAUwwAAAAAAAFhYimEAAAAAAAAsLMUwAAAApqqqLququ6rqxllnAQAAUAwDAABg2nYn2THrEAAAAIliGAAAAFPW3e9K8jezzgEAAJAohgEAAAAAALDAjp90w6o6NckVSb45yVeT7Orul1bVNyb5gySnJbktyf/Z3Z8dtrk4ybOT3J3k57v77UP7YzKaRuOEJG9J8vzu7kmzAQAAMN+qameSnUmyZcuWLC0tTbyvffv2rWn79TSv2eY1VyLbpGSbzGrZLjx7/8aGGTPPfQYAx6KJi2FJ9ie5sLvfX1X3T/K+qnpHkvOTvLO7X1xVFyW5KMkLq+rMJOcleUSShyb5k6p6WHffneQVGX0Iui6jYtiOJG9dQzYA2HBVtSPJS5Mcl+TS7n7xQcufnuSFw919SZ7b3R/c2JQAMB+6e1eSXUmybdu23r59+8T7Wlpaylq2X0/zmm1ecyWyTUq2yayW7fyLrtnYMGN27zhxbvsMAI5FE0+T2N13dvf7h9tfTHJLklOSnJvk8mG1y5M8Zbh9bpIru/vL3X1rkj1Jzqmqk5Oc1N3XDqPBrhjbBgCOCVV1XJLfSfLEJGcmedpwIsi4W5P84+5+ZJJfyfAFIAAAAACwfqZyzbCqOi3JdyW5PsmW7r4zGRXMkjxkWO2UJLePbbZ3aDtluH1wOwAcS85Jsqe7P9bdX0lyZUYngvy97v6LA1MHZzQaeusGZwSADVFVr01ybZKHV9Xeqnr2rDMBAACb11qmSUySVNXXJ3l9khd09xeqasVVl2nrVdqXe6xNMaf8elptvustJ6zvfNjHYn9v1ufJavTJofQJg+VO+vjeVdZ/dkwJDMCC6u6nzToDAADAAWsqhlXVvTMqhL26u98wNH+qqk7u7juHKRDvGtr3Jjl1bPOtSe4Y2rcu036IzTKn/Hpabb7rC8/en5fcsOb66Ipue/r2ddv3etmsz5PV6JND6RMGR3Nyxw9kVAz7vhWWH/HJH4crxrro9/LmNdu85kpkm5Rsk5nnbAAAABx7Jq581GgI2KuS3NLdvzG26Ookz0zy4uHfN421v6aqfiPJQ5OckeTd3X13VX2xqh6b0TSLz0jynyfNBQAzstJJH/dQVY9McmmSJ3b3Z5bb0dGc/HG4YqyLfi9vXovY85orkW1Ssk1mnrMBAABw7FnLMKDHJfnpJDdU1QeGtl/KqAh21TAn/MeTPDVJuvumqroqyc1J9ie5oLvvHrZ7bpLdSU7IaMoo00YBcKx5T5Izqur0JJ9Icl6Snxxfoaq+Jckbkvx0d3904yMCAAAAwOYzcTGsu/88y08JlSRPWGGbS5Jcskz7e5OcNWkWAJi17t5fVT+X5O1Jjkty2XAiyHOG5a9M8m+TPCjJy4drbO7v7m2zygwAAAAAm8H6XSAKADaZ7n5Lkrcc1PbKsds/m+RnNzoXAAAAAGxm95p1AAAAAAAAAFgvimEAAAAAAAAsLMUwAAAAAAAAFpZiGAAAAAAAAAtLMQwAAAAAAICFpRgGAAAAAADAwlIMAwAAAAAAYGEphgEAAAAAALCwFMMAAAAAAABYWIphAAAAAAAALCzFMAAAAAAAABaWYhgAAAAAAAALSzEMAAAAAACAhaUYBgAAAAAAwMJSDAMAAAAAAGBhKYYBAAAAAACwsBTDAAAAAAAAWFiKYQAAAAAAACwsxTAAAAAAAAAWlmIYAAAAAAAAC0sxDAAAAAAAgIWlGAYAAAAAAMDCUgwDAAAAAABgYSmGAQAAAAAAsLAUwwAAAAAAAFhYimEAAAAAAAAsLMUwAAAAAAAAFpZiGAAAAAAAAAtrbophVbWjqj5SVXuq6qJZ5wGAo3W4Y1mNvGxY/qGq+u5Z5ASAjeAzHgAAMC/mohhWVccl+Z0kT0xyZpKnVdWZs00FAEfuCI9lT0xyxvCzM8krNjQkAGwQn/EAAIB5cvysAwzOSbKnuz+WJFV1ZZJzk9y8Xg94wyc+n/Mvuma9dr+q2178IzN5XADW1ZEcy85NckV3d5LrquqBVXVyd9+58XEBYF1t+Gc8AACAlcxLMeyUJLeP3d+b5HtnlAVgYqfNqMieKLTPgSM5li23zilJFMMAWDQ+4wEAAHNjXophtUxbH7JS1c6MppVKkn1V9ZE1POaDk3x6DdtPrH5tFo96eD+/zn0yr7/3YczseTLH9Mmh5qJPpvAa+wdTiLGZHcmxbD2Od3Px/FvOD/za/GbL/PbbvOZKZJuUbJNZz2yOdxvjsMe8Rfl8dwTmNdu85kpkm5Rsk5nLbFP4W9rxDgDGzEsxbG+SU8fub01yx8ErdfeuJLum8YBV9d7u3jaNfS0KfXIofXIofXIofcLgSI5lUz/ezfPzT7ajN6+5EtkmJdtk5jkbR+ywx7zN8vluXrPNa65EtknJNpl5zTavuQDgWHWvWQcYvCfJGVV1elXdJ8l5Sa6ecSYAOBpHciy7OskzauSxST7vemEALCif8QAAgLkxFyPDunt/Vf1ckrcnOS7JZd1904xjAcARW+lYVlXPGZa/MslbkjwpyZ4kf5vkWbPKCwDryWc8AABgnsxFMSxJuvstGX1JuFGmMh3HgtEnh9Inh9Inh9InJFn+WDYUwQ7c7iQXTPlh5/n5J9vRm9dciWyTkm0y85yNI7TBn/Hm+Tkzr9nmNVci26Rkm8y8ZpvXXABwTKrR93IAAAAAAACweOblmmEAAAAAAAAwdZuyGFZVx1XVX1bVm2edZR5U1QOr6nVV9eGquqWq/uGsM81aVf2rqrqpqm6sqtdW1dfNOtNGq6rLququqrpxrO0bq+odVfVXw7/fMMuMG22FPvn14bXzoap6Y1U9cIYR2USqakdVfaSq9lTVRbPOM66qbquqG6rqA1X13hlnmdv3shWy/buq+sTQdx+oqifNKNupVfVnw98FN1XV84f2mfbdKrlm3m9V9XVV9e6q+uCQ7d8P7TN/vq2Sbeb9NpbxHn+fz0O/MVuTvA9V1cXDcfEjVfXDY+2PGY5Le6rqZVVV85Ctqu5XVdcMf0veVFUvnodcB+3z6vHj1Dxkq6r7VNWuqvro0Hf/bI6yPW14rn2oqt5WVQ/eyGxV9aBh/X1V9dsH7Wumr4OVsk37dTDNbAftc82vhSn/f071dQAAm8GmLIYleX6SW2YdYo68NMnbuvs7kjwqm7xvquqUJD+fZFt3n5XRBb/Pm22qmdidZMdBbRcleWd3n5HkncP9zWR3Du2TdyQ5q7sfmeSjSS7e6FBsPlV1XJLfSfLEJGcmeVpVnTnbVIf4ge5+dHdvm3GO3Znf97LdOTRbkvzm0HePHq63Mwv7k1zY3d+Z5LFJLhieY7Puu5VyJbPvty8neXx3PyrJo5PsqKrHZvZ9tlq2ZPb9dsDBf5/PQ78xW0f1PjQsOy/JIzJ6b335cLxMklck2ZnkjOFnuffeWWX7T8PnsO9K8riqeuKc5EpV/dMk+9aQZ72y/XKSu7r7YRn9HfTf5iFbVR2f0WfrHxg+G3woyc9tZLYkf5fk/0nyC8vsa6avg8Nkm+brYNrZpvlamGauab8OAGDhbbpiWFVtTfIjSS6ddZZ5UFUnJfn+JK9Kku7+Snd/bqah5sPxSU4YPtDcL8kdM86z4br7XUn+5qDmc5NcPty+PMlTNjLTrC3XJ939x929f7h7XZKtGx6MzeicJHu6+2Pd/ZUkV2b0+uQg8/xetkK2udDdd3b3+4fbX8yoSHFKZtx3q+SauR458EXZvYefzhw831bJNhdW+Pt85v3GbE3wPnRukiu7+8vdfWuSPUnOqaqTk5zU3df26ILZV2SNz6dpZevuv+3uPxv285Uk788a/pacVq4kqaqvT/Kvk/zqpHnWK1uSn0nyH4d9fbW7Pz0n2Wr4OXEYdXVS1vg58mizdfeXuvvPMyqi/L15eB2slG3ar4NpZkum+1qYZq5M+XUAAJvBpiuGJfmtJL+Y5KszzjEvvjXJXyf5LzWamubSqjpx1qFmqbs/keQ/Jfl4kjuTfL67/3i2qebGlu6+Mxn9IZ/kITPOM29+JslbZx2CTeGUJLeP3d+bOSkIDDrJH1fV+6pq56zDLGPe38t+bphe6bKag6nhquq0jM7Uvj5z1HcH5UrmoN+GkQEfSHJXknd099z02QrZkjnotyz/9/lc9Bvz4Qjfh1Y6Np4y3D64fR6yje/ngUl+NKNRIfOQ61eSvCTJ304jz7Sy1demJP+Vqnp/Vf1hVW2Zh2zd/b+TPDfJDRkVwc7McNLpBmZbyTy8Do5kPw/MFF8HU8q2Lq+FteRa79cBACyqTVUMq6onZzSM/H2zzjJHjk/y3Ule0d3fleRL2eTT0AxfBJ2b5PQkD83ozL6fmm0q5l1V/XJG0168etZZ2BSWu8bD3IzySPK47v7ujKZxvKCqvn/WgY4hr0jybRlNZXdnRl++zMxwNvTrk7ygu78wyyzjlsk1F/3W3Xd396MzOqP9nKo6axY5lrNCtpn3m7/POZyjeB9a6di4bsfMKWQ7sJ/jk7w2ycu6+2OzzlVVj07y7d39xrVmmXa2jD6/bk3yP4a/Na7N6ETGmWerqntnVAz7row+R34oU5pCfQrH43l4HRxuP1N9HUwj23q9FqbQZ+v2OgCARbapimFJHpfkx6rqtoymlHp8Vf3+bCPN3N4ke8fODn5dRsWxzeyfJLm1u/96OLvvDUn+0YwzzYtPDVNsHJhq464Z55kLVfXMJE9O8vRh2hFYb3uTnDp2f2vmaDrX7r5j+PeuJG/M16Y1mhdz+17W3Z8aihZfTfK7mWHfDV/qvT7Jq7v7DUPzzPtuuVzz1G9Dns8lWcroeiwz77Nx49nmpN9W+vt8rvqN2TjK96GVjo17c88p16ZyzJxStgN2Jfmr7v6tOcn1D5M8Znhd/nmSh1XV0pxk+0xGI3QOFCf+MFP4/DqlbI9Oku7+n8Nngqsyhc+RUzoez8Pr4HCm9jqYYrapvxamlGtdXgcAsOg2VTGsuy/u7q3dfVpGF7z90+7e1CN+uvuTSW6vqocPTU9IcvMMI82Djyd5bFXdb5jr/Qm55wXdN7OrkzxzuP3MJG+aYZa5UFU7krwwyY9199SnkYEVvCfJGVV1elXdJ6Nj2tUzzpQkqaoTq+r+B24n+aEkN8421SHm9r3swBchgx/PjPpuOP69Kskt3f0bY4tm2ncr5ZqHfquqbzowbVFVnZDRyTUfzhw831bKNg/9tsrf5zPvN2Zrgvehq5OcV1X3rarTk5yR5N3DlGNfrKrHDvt8Rtb4fJpWtmFfv5rkAUlesJZM08zV3a/o7ocOr8vvS/LR7t4+J9k6yX9NciDPmj+/TvH/8xNJzqyqbxrW+8Gs8XPktI7Hc/I6WG1fU3sdTDPbtF8LU8w19dcBAGwGtVkHMVTV9iS/0N1PnnGUmRuG/l+a5D5JPpbkWd392ZmGmrGq+vdJ/nlG0979ZZKf7e4vzzbVxqqq12b0x/WDk3wqyYuS/FFGZzh+S0ZFw6d299/MKOKGW6FPLk5y34zOzkuS67r7OTMJyKZSVU/K6Do7xyW5rLsvmW2ikar61nztLNXjk7xmltnm+b1shWzbMzqzvJPcluT/OnANiQ3O9n1J/ntG1z05cB2nX8rouhYz67tVcj0tM+63qnpkRhe9Py6jE86u6u7/UFUPyoyfb6tk+73MwfNtLOf2DH+fz0O/MVuTvA/VaNrqn8nob/gXdPdbh/ZtSXYnOSGj67s+by2j+aeVraq2ZnTtqQ8nOfBZ47e7+9JZ5jpon6cleXN3r2na1yn/f/6DJL+X5IEZXf/6Wd398TnJ9pwkz0/yv5P8ryTnd/dnMqEJs92W5KSMPt9/LskPdffNc/I6OCRbki9kiq+DaWbr7pvH9nla1vhamPL/51RfBwCwGWzaYhgAAAAAAACLb1NNkwgAAAAAAMDmohgGAAAAAADAwlIMAwAAAAAAYGEphgEAAAAAALCwFMMAAAAAAABYWIphAAAAAAAALCzFMAAAAAAAABaWYhgAAAAAAAAL6/8H4iztJUJdhr4AAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 2160x2160 with 20 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Distribution of discrete variables\n",
    "df_eda[discrete_vars].hist(figsize=(30,30))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "d5ebc27f-0212-445e-9111-862dcd0a9273",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Total # of continuous variables :  33\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>basementsqft</th>\n",
       "      <th>bathroomcnt</th>\n",
       "      <th>bedroomcnt</th>\n",
       "      <th>calculatedbathnbr</th>\n",
       "      <th>finishedfloor1squarefeet</th>\n",
       "      <th>calculatedfinishedsquarefeet</th>\n",
       "      <th>finishedsquarefeet12</th>\n",
       "      <th>finishedsquarefeet13</th>\n",
       "      <th>finishedsquarefeet15</th>\n",
       "      <th>finishedsquarefeet50</th>\n",
       "      <th>...</th>\n",
       "      <th>unitcnt</th>\n",
       "      <th>yardbuildingsqft17</th>\n",
       "      <th>yardbuildingsqft26</th>\n",
       "      <th>yearbuilt</th>\n",
       "      <th>structuretaxvaluedollarcnt</th>\n",
       "      <th>taxvaluedollarcnt</th>\n",
       "      <th>landtaxvaluedollarcnt</th>\n",
       "      <th>taxamount</th>\n",
       "      <th>taxdelinquencyyear</th>\n",
       "      <th>censustractandblock</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>parcelid</th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>10754147</th>\n",
       "      <td>NaN</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>...</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>9.0</td>\n",
       "      <td>9.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10759547</th>\n",
       "      <td>NaN</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>...</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>27516.0</td>\n",
       "      <td>27516.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10843547</th>\n",
       "      <td>NaN</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>73026.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>73026.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>...</td>\n",
       "      <td>2.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>650756.0</td>\n",
       "      <td>1413387.0</td>\n",
       "      <td>762631.0</td>\n",
       "      <td>20800.37</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10859147</th>\n",
       "      <td>NaN</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>5068.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>5068.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>...</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1948.0</td>\n",
       "      <td>571346.0</td>\n",
       "      <td>1156834.0</td>\n",
       "      <td>585488.0</td>\n",
       "      <td>14557.57</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10879947</th>\n",
       "      <td>NaN</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1776.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1776.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>...</td>\n",
       "      <td>1.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1947.0</td>\n",
       "      <td>193796.0</td>\n",
       "      <td>433491.0</td>\n",
       "      <td>239695.0</td>\n",
       "      <td>5725.17</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "<p>5 rows × 33 columns</p>\n",
       "</div>"
      ],
      "text/plain": [
       "          basementsqft  bathroomcnt  bedroomcnt  calculatedbathnbr  \\\n",
       "parcelid                                                             \n",
       "10754147           NaN          0.0         0.0                NaN   \n",
       "10759547           NaN          0.0         0.0                NaN   \n",
       "10843547           NaN          0.0         0.0                NaN   \n",
       "10859147           NaN          0.0         0.0                NaN   \n",
       "10879947           NaN          0.0         0.0                NaN   \n",
       "\n",
       "          finishedfloor1squarefeet  calculatedfinishedsquarefeet  \\\n",
       "parcelid                                                           \n",
       "10754147                       NaN                           NaN   \n",
       "10759547                       NaN                           NaN   \n",
       "10843547                       NaN                       73026.0   \n",
       "10859147                       NaN                        5068.0   \n",
       "10879947                       NaN                        1776.0   \n",
       "\n",
       "          finishedsquarefeet12  finishedsquarefeet13  finishedsquarefeet15  \\\n",
       "parcelid                                                                     \n",
       "10754147                   NaN                   NaN                   NaN   \n",
       "10759547                   NaN                   NaN                   NaN   \n",
       "10843547                   NaN                   NaN               73026.0   \n",
       "10859147                   NaN                   NaN                5068.0   \n",
       "10879947                   NaN                   NaN                1776.0   \n",
       "\n",
       "          finishedsquarefeet50  ...  unitcnt  yardbuildingsqft17  \\\n",
       "parcelid                        ...                                \n",
       "10754147                   NaN  ...      NaN                 NaN   \n",
       "10759547                   NaN  ...      NaN                 NaN   \n",
       "10843547                   NaN  ...      2.0                 NaN   \n",
       "10859147                   NaN  ...      NaN                 NaN   \n",
       "10879947                   NaN  ...      1.0                 NaN   \n",
       "\n",
       "          yardbuildingsqft26  yearbuilt  structuretaxvaluedollarcnt  \\\n",
       "parcelid                                                              \n",
       "10754147                 NaN        NaN                         NaN   \n",
       "10759547                 NaN        NaN                         NaN   \n",
       "10843547                 NaN        NaN                    650756.0   \n",
       "10859147                 NaN     1948.0                    571346.0   \n",
       "10879947                 NaN     1947.0                    193796.0   \n",
       "\n",
       "          taxvaluedollarcnt  landtaxvaluedollarcnt  taxamount  \\\n",
       "parcelid                                                        \n",
       "10754147                9.0                    9.0        NaN   \n",
       "10759547            27516.0                27516.0        NaN   \n",
       "10843547          1413387.0               762631.0   20800.37   \n",
       "10859147          1156834.0               585488.0   14557.57   \n",
       "10879947           433491.0               239695.0    5725.17   \n",
       "\n",
       "          taxdelinquencyyear  censustractandblock  \n",
       "parcelid                                           \n",
       "10754147                 NaN                  NaN  \n",
       "10759547                 NaN                  NaN  \n",
       "10843547                 NaN                  NaN  \n",
       "10859147                 NaN                  NaN  \n",
       "10879947                 NaN                  NaN  \n",
       "\n",
       "[5 rows x 33 columns]"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Continuous variables\n",
    "cont_vars = [var for var in num_vars if var not in discrete_vars and year_var]\n",
    "print('Total # of continuous variables : ', len(cont_vars))\n",
    "\n",
    "df_eda[cont_vars].head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "0e870f92-368b-4042-86e1-aaa270135637",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[<AxesSubplot:title={'center':'basementsqft'}>,\n",
       "        <AxesSubplot:title={'center':'bathroomcnt'}>,\n",
       "        <AxesSubplot:title={'center':'bedroomcnt'}>,\n",
       "        <AxesSubplot:title={'center':'calculatedbathnbr'}>,\n",
       "        <AxesSubplot:title={'center':'finishedfloor1squarefeet'}>,\n",
       "        <AxesSubplot:title={'center':'calculatedfinishedsquarefeet'}>],\n",
       "       [<AxesSubplot:title={'center':'finishedsquarefeet12'}>,\n",
       "        <AxesSubplot:title={'center':'finishedsquarefeet13'}>,\n",
       "        <AxesSubplot:title={'center':'finishedsquarefeet15'}>,\n",
       "        <AxesSubplot:title={'center':'finishedsquarefeet50'}>,\n",
       "        <AxesSubplot:title={'center':'finishedsquarefeet6'}>,\n",
       "        <AxesSubplot:title={'center':'fullbathcnt'}>],\n",
       "       [<AxesSubplot:title={'center':'garagecarcnt'}>,\n",
       "        <AxesSubplot:title={'center':'garagetotalsqft'}>,\n",
       "        <AxesSubplot:title={'center':'latitude'}>,\n",
       "        <AxesSubplot:title={'center':'longitude'}>,\n",
       "        <AxesSubplot:title={'center':'lotsizesquarefeet'}>,\n",
       "        <AxesSubplot:title={'center':'poolsizesum'}>],\n",
       "       [<AxesSubplot:title={'center':'rawcensustractandblock'}>,\n",
       "        <AxesSubplot:title={'center':'regionidcity'}>,\n",
       "        <AxesSubplot:title={'center':'regionidneighborhood'}>,\n",
       "        <AxesSubplot:title={'center':'regionidzip'}>,\n",
       "        <AxesSubplot:title={'center':'roomcnt'}>,\n",
       "        <AxesSubplot:title={'center':'unitcnt'}>],\n",
       "       [<AxesSubplot:title={'center':'yardbuildingsqft17'}>,\n",
       "        <AxesSubplot:title={'center':'yardbuildingsqft26'}>,\n",
       "        <AxesSubplot:title={'center':'yearbuilt'}>,\n",
       "        <AxesSubplot:title={'center':'structuretaxvaluedollarcnt'}>,\n",
       "        <AxesSubplot:title={'center':'taxvaluedollarcnt'}>,\n",
       "        <AxesSubplot:title={'center':'landtaxvaluedollarcnt'}>],\n",
       "       [<AxesSubplot:title={'center':'taxamount'}>,\n",
       "        <AxesSubplot:title={'center':'taxdelinquencyyear'}>,\n",
       "        <AxesSubplot:title={'center':'censustractandblock'}>,\n",
       "        <AxesSubplot:>, <AxesSubplot:>, <AxesSubplot:>]], dtype=object)"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAABs0AAAabCAYAAABKDuD6AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAEAAElEQVR4nOz9fbxtZV3v/7/eAiIiCIhuEUgs0JNgouyDdDins705QGph56dFRwWKIk3LTnQDnkqSKO0bat6LQYDhDZkKKaikrtDiRjCUO0mSnWwhUTZ325LY+Pn9Ma4lcy/WzVxrz7XmnGu+no/HfKw5rzGuMT5jrjU/a8xxjeu6UlVIkiRJkiRJkiRJk+xhww5AkiRJkiRJkiRJGjYbzSRJkiRJkiRJkjTxbDSTJEmSJEmSJEnSxLPRTJIkSZIkSZIkSRPPRjNJkiRJkiRJkiRNPBvNJEmSJEmSJEmSNPFsNJswSdYned6w4xhl6fxlkjuTXDHseKTVbJA5KUkl2XcQ25I0eQaVj5KcleSPBhGTpNVrEDknybFJvjComBbY1z7tXGvb9noqyS8NcPtLfj/MuxpXSZ6S5J+S3Jvk+0l+v8967+5n3QF/19ric5bklUm+lWRTksf4XWx+M9+vYccjTVsF5yNrklzS8uhpSV6b5C/63FZf6w7ynCfJyUn+ahDbWikz/lf9+rDjWSk2mmlVSLIuyYYBbe6/A/8L2KuqDh7HhCatdoO+UDOKBpzXJKkvM7+ISlodRuk7jY1c0g/8DjBVVTtV1cOq6pR+KlXVK/pddzkk2Q54E3BYVT2qqu5Y5v2dnuTG1rB47HLuazkM8v3yO6LG3TKcjxwPfAfYuapOqKo/rqq+rhUtZt0J1/u/6q1bs6Fx6sxjo5n0UE8E1lfVd4cdiKTl4cVgScNg7pGk4TIPa8Q8Ebhu2EEswRrgESxz7D2f1y8Dvwp8aTn3t1R95JUVeb+kCfVE4PqqqmEHMo76PC8a1/9VW8VGs8n0X5Nc34Yf/Mskj0iya5KPJ/l2K/94kr2mK7Sutl9vXTFvTvLSnmW/mOSGVu9TSZ7Ys6yS/GqSr7W6pyT5kSSXJrknyXlJHt6z/guTXJ3kriT/mOTHepatT/JbSb6S5O4kH2qx7whcBDyhdXXflOQJSQ5OcmXbz7eSvKlnWy9P8q9J7kjy/6ZbupMcB/wF8ONtO5cDrwV+rr3+8nL9UqQJtqiclORU4H8Ab2+fy7f3bOt5Ld/cmeQdSdLqHJvkH5K8OclG4OQkj05yTtvHvyb5vSQPa+s/rL3+1yS3t/Ue3ZZN98L4hSS3tH29Isl/bfnprhkxkeSXW568tx3rM1v5ovLacv8iJD00H8GC5yfPSPKl9vn+EN1Fkell65JsSPK7Sf4N+Msk2yd5S5Jb2+MtSbbvqfPLSW5KsjHJBb2f/Sz+vOrIFvc9Sf4lyRGtfKrV/Ye2nU8n2b1Vu6T9vKvlnh9fjjdaWk2S7J3kI+2c4o4kb2+fzc+2199Jcm6SXeaov026IYL+pX0mr2rbfEjPz8zT2z7Jn7dzk3vaNv5HKz+CWb7TtHOhM5LcluSbSf4oyTY9Mf1Zi/3rwAtm2eWPJLmincOcn2S3nlj+Osm/tWWXJNm/lR8PvBT4nRbL3/Zs78CZ50StznQuPSHdedltSX5hRiy7J7m4vX9/n4d+J31Vkq8BX5vtvZNWWpLPAs/mwe8070/rgbnQ33x6emsm2T3dd6W72rnD59O+0zSzfq5a3UWf3yR5MnBjW+2udhwzj21rv2cdl+QbwGcBquodVfUZ4Huz7GvR131mvoe973nP6xPzYE6+PsnP9Cyb7bvl9i1nfqPF8e4kO8z1fiX5Ly1nbUzXi+5ne7Y/17b8jqh5ZcLOR5KcBRzDg+cUz0tPT7aeuI9pn6fvJPl/PfV7131Ekr9q79NdSb6YZE3PYT0xs393Iskh6XLoXUm+nGRdz7InpTsvuTfJxUBvvTn3ObNe+11Ox/qQHqfZMr8dnO774V3tPX17tvyO+JDzoszx/yAP/V/15LlyVM/259rW+4AfAv62bet3Zvv7GRlV5WOCHsB64Fpgb2A34B+APwIeA/z/gEcCOwF/DXys1dkRuAd4Snu9B7B/e/4i4CbgR4Ftgd8D/rFnfwVcAOwM7A/cB3wG+GHg0cD1wDFt3WcCtwPPArahS3zrge17Yr8CeEKL/QbgFW3ZOmDDjGO9FHh5e/4o4JD2/KnAJuAngO3puslvBp7Xlh8LfKFnOycDfzXs350PH6vxsZSc1OpNAb80Y1sFfBzYhe4f8beBI9qyY9vn/NdartoBOAc4v21/H+CfgePa+r/YctsPt/zxEeB9bdk+bV/vpvvyeBjdF7iPAY8D9my57H+29V8CfBP4r0CAfYEn9hx/33nNhw8fy/eYJx/NeX4CPBz4V+D/AtsBLwbuB/6obXNdyz1vbOvvALweuKzli8cC/wic0tZ/Dt3wIs9s678NuKQnxsWcVx0M3E035PTDWm76L23ZFPAvwJNbTFPAG9qy6Ry37bB/Jz58jMOj5YUvA2+m+970CLrh3vdtn7/t22f9EuAtPfXW8+D3j98GrgGe0s4Vnk53LvSQzyM950A89HvLy1q9bYETgH8DHtGWncyM7zR05y7vaXE/ju6c5FfaslcAX+XBnPi53lhaHN8EDmj1/6Z3+3TnUju1438LcHXPsrNoeXLG+zHfOdFmuvy5HfB84N+BXXu2dy8Pfr/78xnvSwEXt+3uMOy/GR8+ph8zPs8/+Fz0+Tc/ve6f0H0v2a49/geQtmy+z9XWnN/sw0NzUwH7tudb+z3rnJZXdpjxfn0BOHZG2VKv+/zgPex5zzf0vH5Je98eBvwc8F1gj7bsWB763fItdOdou7Xj/lvgT2Z7v9qx3QL8Qqv/TLrzv+nrbPNta4s4ffiYfjC55yMzP8s/2H5P3O9tn9On031/+tFZ1v2V9ll7ZHsvD6Ib8nH6WOf67rQncAddnn5Ye6/vAB7bll9Kl3+2p8tH9/a5z/nqreOh18B7f48HAYe0938futz/Gz3rbnFexMLX46fouf7G/Dmqn2v7zxv256Wfhz3NJtPbq+qWqtoInAr8fFXdUVV/U1X/XlX3tvL/2VPn+8ABSXaoqtuqarpb5q/QfTBuqKrNwB/T3cn0xJ66b6yqe1qda4FPV9XXq+puurtkntHW+2XgPVV1eVU9UFVn0yWzQ3q29daqurXF/rfAgfMc5/3Avkl2r6pNVXVZK38x8PGquqSq7gN+vx2fNFBJzkx399y1fa7/s+nuYrsuyfuXO74RspScNJc3VNVdVfUNupOpA3uW3VpVb2u56j/pvvycVFX3VtV64DTg5W3dlwJvarlqE3AScFS27Lp+SlV9r6o+Tfcl6gNVdXtVfRP4PA/mtl8C/rSqvlidm6rqX3u2s5i8Ji2KeWjRHpKPmP/85BC6i0lvqar7q+rDwBdnbPP7wOuq6r6q+g+6/PL6li++DfwhW+aeM6vqS+0c5SS63u/79Gyv3/Oq49q2Lq6q71fVN6vqqz3b+cuq+ucW03mYe7RMJiAPHUx3YfW3q+q77dzgC+3//cXts/9tugsfc53L/BLwe1V1YztX+HItYc6bqvqrdg61uapOo7vQ8pTZ1m13Mv8k3UWU71bV7XQX2o5qq/wsXW6bzol/Mstm3ldV11Y3rP3vAz87fWd4VZ3ZzrHuo7so9fS03iTzmO+c6H663Hl/VV1IdzG899g+0fP97v/R5c69e5b/SVVtbDlPGgcL/c33rrcH3U1591fV56u6K5PNXJ+rrT2/mVXLAVv7Pevklpf6+bwuy3Wfqvrr9r59v6o+RNcb4+CeVXq/W36P7v38vy3P3Et3beyoh24ZgBfSTQnyly1ff4nuxoMXJ8kit6UxsQLnQ5N8PrKQP6yq/6iqL9M1LD59lnXup2vo27flxKuq6p6e5XN9d3oZcGFVXdjyxcXAlcDzk/wQ3c3Tv9/e/0vo8vC8++yj3rzadi5r7/96ugbJmb/z3vOifq7HA9BHjup7W6PORrPJdEvP83+l69r9yCTvSdd1/R66Ow92SbJN+xL0c3St+7cl+USS/9LqPxH489bl8i5gI93dCHv27ONbPc//Y5bXj+rZ1gnT22rb25su6U/7t57n/95TdzbH0d0F8NXWxfWFrfwJve9BO75lnbhWE+ss4Ih+VkyyH90XhkOran/gN5YvrJGzqJy0wLbmyxG9+9mdB++g7N33dO56wizLtqUbj35av7ltb7q7kpYSs7S1zsI8tBgPyUfMf37yBOCbMy5O9eYOgG9XVe9wQrPllyfMtqxdTLqDpZ1XmXs0Ks5ideehvYF/bRdOfyDJ45J8MN0wQ/cAf0XPkDyzbGO+z2tf0g3ldkO6YdjuouuBOtc+n0h3Ufy2ntz2Hro7vGHGdyYemtuYZfl2dMMkbpPkDemGd7qH7q5i5oll2nx56Y4Z7/Gc53ktd25ky++RvbFK42Chv/lp/x9dz61Pp5tS48QZy+f6XG3t+c1cBvE9azGf12W57pPk6J7hxe6i61Xbm8N6Y3wsXU+Rq3rW/2Qrn80TgWfNeO9fCjx+CdvS+DiL5T0fmuTzkYX0873nfcCngA+mG0L/T5Ns18c2ngi8ZMbn+b/T3czwBODOln9mi3+ufS5Ub17phlD8eLphsu+ha9Sa+f73vqf9XI+ftlCOWsy2RpqNZpOp9467HwJupesu+xTgWVW1M13XT+gawKiqT1XV/6L70H+VrmsrdB+yX6mqXXoeO1TVPy4hrluAU2ds65FV9YE+6tZDCqq+VlU/T5do3wh8ON0Y0LfR8x4keSRdy37f25b60e4G2dhblm486U+mG9f58z0N0L8MvKOq7mx1b1/hcIdp0TmJpX0ue+t8h+6unt5esT9EN8wQLYaZyzaz5cXpft0C/MgS6pl7tNXMQ4s2Wz6a7/zkNmDPdsddb71eMz/Ls+WXW2db1s5bHsODuWkxzD0aCROQh24BfigPnUj9T+g+Tz/WzmVexoPnMbNtY7bP6/TFkkf2lD1+tg2kmy/kd+nuyN61qnahG6J1rnOnW+ju/N29J7ft3C7OwYzvTDw0tzHL8vvpzrH+D3Ak8Dy6C2X7TIc5RyyD0Pv97lF0Qwbd2rPc3KZVqbreXCdU1Q8DPwX8ZpLn9lF1a89v5jKI71l9f1634rrPd5kjt6YbOem9wKuBx7R8ei1b5vCZ3y3/g254xen38tFVNdcNSbcAfz/jvX9UVb2yj22Zy8bUCpwPTfL5yFarrlftH1bVU4H/Rtcj9Og+qt5C1/O+9/O8Y1W9gS72XVtOmvaD+OfZ57z1mJG/2s3lvQ3r76K7dr9f+52/lof+znt/D4u5Hr9QjlpoW2OTw2w0m0yvSrJXuomaXwt8iG4M0v+gm5h0N+B10ysnWZPkp9uH9T66YQEeaIvfDZyUByd2fnSSlywxrvcCr0jyrHR2TPKCJDv1UfdbwGPSM+RHkpcleWxVfR+4qxU/AHwYeGGS/55uIsTXM/9n4VvAPtlyMl1pqU4Hfq2qDgJ+C3hnK38y8OR0E4telm6C1EmxqJzUfItuHPwlqaoH6LrUn5pkp/bF6Dfp7roC+ADwf9NNvvooujtzPjTzrq0+/QXwW0kOarlt32w5hO1cHpLXpAExD81ttnw03/nJpXQXen49ybZJ/jdbDt0zmw8Av5fksekmkP4DHsw97wd+IcmBSbanyz2XVzesxmKd0bb13CQPS7Jnzxfx+XybbviiJedYqQ+rKQ9dQXdx4w0tPzwiyaF05zKb6M5l9qSbJ2QufwGckmS/lmd+LMljqhtG6ZvAy9L13vpF5m4M34kuH30b2DbJH9DNfzhti+80VXUb8GngtCQ7tzzxI0mmh+85jy637ZVkV2Bm7xVaXE9tF6NfD3y4nWPtRPe98Q66izp/PKPeVp3HzeH5Pd/vTqHLnfYu06qX5IXt+0Xo5qJ/gAev18xn0Oc3wPJ8z0ry8CSPoLvou13Lsw9ry5Z63edquryxW5LHs2VPnh3pLux+u+3jF+h6ms11zN+nez/fnORxrc6eSQ6fo8rH6f7XvTzJdu3xX5P8aB/b8jvi6jLI86FJPh/ZakmeneRp6Rqg7qFr/O8nl/4V8FNJDm/vzSOSrEuyV3XTclwJ/GHLY/+d7uaGefe5UD26eSIf0XL2dsDv0Q2BOW2ntr1N7fvfKxc4hr6vx/eRoxba1nKcAy4LGwEm0/vpEtLX2+OP6Cbx24Guxfgyuq6V0x5G1+vjVrq7Iv4n8KsAVfVRurt5Ppiuy+e1dGPRLlpVXUl3N8XbgTvphhg4ts+6X6U7+fp6uu6fT6Dr9nxdkk10k0EfVd2YvtcBr2rvw21tXxvm2fxft593JPnSog9MatqXgv8G/HWSq+m6nO/RFm8L7Ec3oefPA3+RZJeVj3IoFpuToPtMvzjJnUneusT9/hrdHTpfp5tU+v3AmW3ZmXRd5S8BbqYbp/7XlrKTqvprurmR3k83eevH6O5+XqjebHlN2irmoQU9JB/Nd35SVf8J/O/2+k664aw/ssA+/ojuS9BX6Cba/lIro6o+Qzfnxt/QnaP8CEucw6KqrqCbYP7NdHd3/j1b3tk9V71/p8tZ/9Byz9iNP6/RttryULtA/FPAvsA36L5X/BzdfIXPpPv8fYL5c8Ob6C4KfZruIscZdOdB0OWf36ZrgNofmGtEj0/RzWv4z3RD+HyPLYfeme07zdF0w6hdT5fDPsyDv4v3tm1+mS5PzRb/++iGm/o34BHAr7fyc1oM32zbvmxGvTOAp7Yc87E5jmex3k93k9VG4CC6oc6kSbAf8Hd0F8UvBd5ZVVMLVVqG85teg/6e9Wm6Gyr/G10jw3/w4EgkS73u8z66/La+bf9D0wuq6nq6edgupbvA+zTgHxaI8Xfp3sPL2rWxv2OOOZyqmwPoMLpzvFvpcugbefCi95zb8jvi6jHo86EJPx8ZhMe3/d4D3ED33emv5q0BtBt0jqS74fLbdMf62zzY5vJ/gGfRnZ+8ju4cqZ99zlmvurmsf5WukfObdPm2N7/9Vqt/L9379yHmsYTr8fPlqIW29Sd0N5DeleS35otr2FJbDFEsTaYk64Ffqqq/G3YsWl2S7EM3AfEBSXYGbqyqPWZZ793AZVV1Vnv9GeDEquprwmVJmot5SNKwmYckScPmdR8Nm+dDWqokJwP7VtXLhh3LpLCnmSStkKq6B7g5bQjT1lX56W3xx4Bnt/Ld6brjf30YcUpavcxDkobNPCRJkiad50PSaLPRTJKWSZIP0A3p8JQkG5IcRzdUzHFJvgxcR9eNG7ru5nckuR74HPDbVXXHMOKWtHqYhyQNm3lIkiRNOs+HpPHi8IySJEmSJEmSJEmaePY0kyRJkiRJkiRJ0sSz0UySJEmSJEmSJEkTb9thBzBou+++e+2zzz4Lrvfd736XHXfccfkDWibGPzzjHDssb/xXXXXVd6rqscuy8TEzKbloa0zyscNkH795aGWsljw06vGBMQ7CqMcH/cdoHnrQzDw07N/zsPdvDKOx/0mIwTz0oHE6Hxp2DJO+/1GIYdj7H2QM5qEHjVMe6pexDt64xAnjE+tW5aGqWlWPgw46qPrxuc99rq/1RpXxD884x161vPEDV9YI5IFReExKLtoak3zsVZN9/OYh89BijHp8VcY4CKMeX1X/MZqH5s5Dw/49D3v/xjAa+5+EGMxDc+ehuaz2vwn3Px4xDHv/g4zBPDSeeahfxjp44xJn1fjEujV5yOEZJUmSJEmSJEnqQ5Jdknw4yVeT3JDkx4cdk6TBWXXDM0qSJEmSJEmStEz+HPhkVb04ycOBRw47IEmDsyw9zZLsneRzraX9uiSvaeUnJ/lmkqvb4/k9dU5KclOSG5Mc3lN+UJJr2rK3JslyxCxJkiRJkiRJ0lyS7Az8BHAGQFX9Z1XdNdSgJA3Ucg3PuBk4oap+FDgEeFWSp7Zlb66qA9vjQoC27Chgf+AI4J1Jtmnrvws4HtivPY5YppglSZIkSZIkSZrLDwPfBv4yyT8l+YskOw47KEmDsyzDM1bVbcBt7fm9SW4A9pynypHAB6vqPuDmJDcBBydZD+xcVZcCJDkHeBFw0XLELUmSJEmSpLm1m5yvBL5ZVS9MshvwIWAfYD3ws1V1Z1v3JOA44AHg16vqU638IOAsYAfgQuA1VVVJtgfOAQ4C7gB+rqrWtzrHAL/Xwvijqjp72Q9Wkh5qW+CZwK9V1eVJ/hw4Efj96RWSHE/XCYQ1a9YwNTW14EY3bdrU13qjwFgHb1zihPGKdamWfU6zJPsAzwAuBw4FXp3kaLoTrBPaidSewGU91Ta0svvb85nlM/exqhPRbIx/eMY5dhj/+CVJkiRJQ/Ua4AZg5/b6ROAzVfWGJCe21787Y1ShJwB/l+TJVfUAD44qdBldo9kRdDdIHwfcWVX7JjkKeCPwc61h7nXAWqCAq5JcMN04J0kraAOwoaoub68/TJf3fqCqTgdOB1i7dm2tW7duwY1OTU3Rz3qjwFgHb1zihPGKdamWtdEsyaOAvwF+o6ruSfIu4BS6E5xTgNOAXwRmm6es5infsmCVJ6LZGP/wjHPsMP7xS5IkSZKGI8lewAuAU4HfbMVHAuva87OBKeB3WdqoQkcCJ7dtfRh4e5vb/nDg4qra2OpcTNfQ9oHlOVJJml1V/VuSW5I8papuBJ4LXD/suCQNzrI1miXZjq7B7Nyq+ghAVX2rZ/l7gY+3lxuAvXuq7wXc2sr3mqVckiRJkiRJK+stwO8AO/WUrWnTdFBVtyV5XCtfyqhCewK3tG1tTnI38Jje8lnqbGFcRyMadgyTvv9RiGHY+x+VGMbErwHnJnk48HXgF4Ycj6QBWpZGs3YX0BnADVX1pp7yPaZPpICfAa5tzy8A3p/kTXRd9vcDrqiqB5Lcm+QQuuEdjwbeNqg49znxE4PaFADr3/CCgW5P0up3zTfv5tgB5iLzkKRhG/T51VlHOKe2tNr5vUwaD0leCNxeVVclWddPlVnKFhpVaKtGIoKljUb0tnPP57QvfHfB9RZjsblo2KPCTPr+RyGGYe9/VGIYB1V1Nd1wsQMz6OtD4DmRtFTL1dPsUODlwDVJrm5lrwV+PsmBdCc264FfAaiq65KcR9eVdTPwqjbGNcAreXBy2IvaQ5IkSZIkSSvnUOCnkzwfeASwc5K/Ar41fZN0kj2A29v6SxlVaLrOhiTbAo8GNrbydTPqTA3u0CRJkjrL0mhWVV9g9ruALpynzql0Y2LPLL8SOGBw0UmSJEmSJGkxquok4CSA1tPst6rqZUn+P+AY4A3t5/mtylJGFbqgbeNS4MXAZ6uqknwK+OMku7b1DpuORZIkaZCWbU4zSZIkSZIkrXpvAM5LchzwDeAlsORRhc4A3pfkJroeZke1bW1Mcgrwxbbe66tq43IfmCRJmjw2mkmSJEmSJKlvVTVFGx6xqu4AnjvHeosaVaiqvkdrdJtl2ZnAmUuNWZIkqR8PG3YAkiRJkiRJkiRJ0rDZaCZJkiRJkiRJkqSJZ6OZJEmSJEmSJEmSJp6NZpIkSZIkSZIkSZp4NppJkiRJkiRJkiRp4tloJkmSJEmSJEmSpIlno5kkSZIkSZIkSZImno1mkiRJkiRJkiRJmnjbDjsASRoHSdYD9wIPAJurau1wI5K0WiR5BHAJsD3dudmHq+p1M9ZZB5wP3NyKPlJVr1/BMCVJkiRJklY9G80kqX/PrqrvDDsISavOfcBzqmpTku2ALyS5qKoum7He56vqhUOIT5IkSZIkaSLYaCZJkjREVVXApvZyu/ao4UUkSZIkSZI0mZzTTJL6U8Cnk1yV5PhhByNpdUmyTZKrgduBi6vq8llW+/EkX05yUZL9VzZCSZIkSZKk1c+eZpLUn0Or6tYkjwMuTvLVqrqkd4XWmHY8wJo1a5iamlpwo2t2gBOetnlgQfazz1GxadOmsYp30Cb5+Cf52OdSVQ8ABybZBfhokgOq6tqeVb4EPLEN4fh84GPAfjO3s5Q8NOjfxyBzGozH34sxbr1Rjw/GI8bl5hyvkiRJklY7G80kqQ9VdWv7eXuSjwIHA5fMWOd04HSAtWvX1rp16xbc7tvOPZ/TrhlcKl7/0oX3OSqmpqbo5z1arSb5+Cf52BdSVXclmQKOAK7tKb+n5/mFSd6ZZPeZ8ywuJQ8N+vdx7ImfGNi2AM46YseR/3sZh7/pUY9x1OOD8YhxhTjHqyRJkqRVy+EZJWkBSXZMstP0c+Awei5mS9LWSPLY1sOMJDsAzwO+OmOdxydJe34w3TncHSscqiRJkiRJ0qpmTzNJWtgauuHSoMub76+qTw43JEmryB7A2Um2oWsMO6+qPp7kFQBV9W7gxcArk2wG/gM4qqpqaBFLmlTTc7wW8J7Wu3UL8w0TO9cQl4Me1nWuYTRHYYhNYxj+/o1BkiRJ87HRTJIWUFVfB54+7DgkrU5V9RXgGbOUv7vn+duBt69kXJI0iwXneJ1vmNi5hrgc9LCucw1XPQpDbBrD8PdvDJIkSZqPwzNKkiRJkhbUO8crMD3Hq6QJkuQRSa5I8uUk1yX5w1Z+cpJvJrm6PZ7fU+ekJDcluTHJ4T3lByW5pi17a89Q1Nsn+VArvzzJPj11jknytfY4ZgUPXZIkTQgbzSRJkiRJ83KOV0nNfcBzqurpwIHAEUkOacveXFUHtseFAEmeChwF7A8cAbyzDUkN8C664Vz3a48jWvlxwJ1VtS/wZuCNbVu7Aa8DnkXXaP+6JLsu58FKkqTJY6OZJEmSJGkha4AvJPkycAXwCed4lSZPdTa1l9u1x3zzrB4JfLCq7quqm4GbgIOT7AHsXFWXtnlazwFe1FPn7Pb8w8BzWy+0w4GLq2pjVd0JXMyDDW2SJEkD4ZxmkiRJkqR5OcerpGmtp9hVwL7AO6rq8iQ/Cbw6ydHAlcAJrWFrT+CynuobWtn97fnMctrPWwCqanOSu4HH9JbPUqc3vuPperCxZs0apqamFjymNTvACU/bvOB6i9HPfntt2rRp0XUGadL3PwoxDHv/oxKDJA2bjWaSJEmSJEnqS1U9AByYZBfgo0kOoBtq8RS6XmenAKcBvwhktk3MU84S6/TGdzpwOsDatWtr3bp18xxN523nns9p1wz2Etn6ly68315TU1P0E+tymfT9j0IMw97/qMQgScPm8IySJEmSJElalKq6C5gCjqiqb1XVA1X1feC9dHOOQdcbbO+eansBt7byvWYp36JOkm2BRwMb59mWJK2oJOuTXJPk6iRXDjseSYNlo5kkSZIkSZIWlOSxrYcZSXYAngd8tc1RNu1ngGvb8wuAo5Jsn+RJwH7AFVV1G3BvkkPafGVHA+f31DmmPX8x8Nk279mngMOS7JpkV+CwViZJw/DsqjqwqtYOOxBJg+XwjJIkSZIkSerHHsDZbV6zhwHnVdXHk7wvyYF0wyWuB34FoKquS3IecD2wGXhVG94R4JXAWcAOwEXtAXAG8L4kN9H1MDuqbWtjklOAL7b1Xl9VG5fxWCVJ0gSy0UySJEmSJEkLqqqvAM+Ypfzl89Q5FTh1lvIrgQNmKf8e8JI5tnUmcOYiQpak5VDAp5MU8J42l6KkVcJGM0mSJEmSJEmS+nNoVd2a5HHAxUm+WlWXTC9McjxwPMCaNWuYmppacINrdoATnrZ5oEH2s9+l2LRp07Jte9DGJdZxiRPGK9alstFMkiRJkiRJkqQ+VNWt7eftST4KHAxc0rP8dOB0gLVr19a6desW3Obbzj2f064Z7KX69S9deL9LMTU1RT/HNArGJdZxiRPGK9aletiwA5AkSZIkSZIkadQl2THJTtPPgcOAa4cblaRBsqeZJEmSJEmSJEkLWwN8NAl019bfX1WfHG5IkgbJRjNJkiRJkiRJkhZQVV8Hnj7sOCQtn2UZnjHJ3kk+l+SGJNcleU0r3y3JxUm+1n7u2lPnpCQ3JbkxyeE95QcluaYte2taM74kSZIkSZIkSZI0KMs1p9lm4ISq+lHgEOBVSZ4KnAh8pqr2Az7TXtOWHQXsDxwBvDPJNm1b7wKOB/ZrjyOWKWZJkiRJkiRJkiRNqGVpNKuq26rqS+35vcANwJ7AkcDZbbWzgRe150cCH6yq+6rqZuAm4OAkewA7V9WlVVXAOT11JEmSJEmSJEmSpIFY9jnNkuwDPAO4HFhTVbdB17CW5HFttT2By3qqbWhl97fnM8tn7uN4ut5orFmzhqmpqQXj2rRpEyc87YFFHs38+tnvoGzatGlF9zdo4xz/OMcO4x+/JEmSJEmSJEnLYVkbzZI8Cvgb4Deq6p55piObbUHNU75lQdXpwOkAa9eurXXr1i0Y29TUFKd94bsLrrcY61+68H4HZWpqin6Oc1SNc/zjHDuMf/yStNokeQRwCbA93bnZh6vqdTPWCfDnwPOBfweOne7VL0mSJEmSpMFYrjnNSLIdXYPZuVX1kVb8rTbkIu3n7a18A7B3T/W9gFtb+V6zlEuSJK0W9wHPqaqnAwcCRyQ5ZMY6P8mD87seTzfnqyRJkiRJkgZoWRrN2t3QZwA3VNWbehZdABzTnh8DnN9TflSS7ZM8ie6C0BVtKMd7kxzStnl0Tx1JkqSxV51N7eV27TGzZ/2RwDlt3cuAXaZvRJIkSZIkSdJgLNfwjIcCLweuSXJ1K3st8AbgvCTHAd8AXgJQVdclOQ+4HtgMvKqqpicceyVwFrADcFF7SJIkrRpJtgGuAvYF3lFVl89YZU/glp7X0/O83rYyEUqSJEmSJK1+y9JoVlVfYPb5yACeO0edU4FTZym/EjhgcNFJkiSNlnaz0IFJdgE+muSAqrq2Z5W+5nlNcjzd8I2sWbOGqampBfe9adOmvtbr1wlP2zywbcHg41sOxrj1Rj0+GI8YJUmSJElbZ7l6mkmSJGmRququJFPAEUBvo9lc87/OrH86cDrA2rVra926dQvuc2pqin7W69exJ35iYNsCOOuIHQca33IY9Hu4HEY9xlGPD8YjRkmSJEnS1lmWOc0kSZLUnySPbT3MSLID8DzgqzNWuwA4Op1DgLvb3K+SJEmSJEkaEHuaSZIkDdcewNltXrOHAedV1ceTvAKgqt4NXAg8H7gJ+HfgF4YVrCRJkiRJ0mplTzNJkqQhqqqvVNUzqurHquqAqnp9K393azCjOq+qqh+pqqe1OV8lSZJWVJJHJLkiyZeTXJfkD1v5bkkuTvK19nPXnjonJbkpyY1JDu8pPyjJNW3ZW5OklW+f5EOt/PIk+/TUOabt42tJjlnBQ5ckSRPCRjNJkiRJkiT14z7gOVX1dOBA4Ig2dPSJwGeqaj/gM+01SZ4KHAXsTzdn6ztb73qAdwHHA/u1xxGt/DjgzqraF3gz8Ma2rd2A1wHPAg4GXtfbOCdJkjQINppJkiRJkiRpQa33+6b2crv2KOBI4OxWfjbwovb8SOCDVXVfVd1MN9T0wUn2AHauqkurqoBzZtSZ3taHgee2XmiHAxdX1caquhO4mAcb2iRJkgbCOc0kqU/tjsgrgW9W1QuHHY8kSZIkrbT2vegqYF/gHVV1eZI1VXUbQFXdluRxbfU9gct6qm9oZfe35zPLp+vc0ra1OcndwGN6y2ep0xvf8XQ92FizZg1TU1MLHtOaHeCEp21ecL3F6Ge/vTZt2rToOoM06fsfhRiGvf9RiUGShs1GM0nq32uAG4Cdhx2IJEmSJA1DVT0AHJhkF+CjSQ6YZ/XMtol5ypdapze+04HTAdauXVvr1q2bJ7zO2849n9OuGewlsvUvXXi/vaampugn1uUy6fsfhRiGvf9RiUGShs3hGSWpD0n2Al4A/MWwY5EkSZKkYauqu4ApuiESv9WGXKT9vL2ttgHYu6faXsCtrXyvWcq3qJNkW+DRwMZ5tiVJkjQwNppJUn/eAvwO8P0hxyFJkjQ0SbZJ8k9JPj7sWCStvCSPbT3MSLID8Dzgq8AFwDFttWOA89vzC4Cjkmyf5EnAfsAVbSjHe5Mc0uYrO3pGneltvRj4bJv37FPAYUl2TbIrcFgrkyRJGhiHZ5SkBSR5IXB7VV2VZN086w197PxxGnt80sdKn+Tjn+Rjl6RVwOGqpcm2B3B2m9fsYcB5VfXxJJcC5yU5DvgG8BKAqrouyXnA9cBm4FVteEeAVwJnATsAF7UHwBnA+5LcRNfD7Ki2rY1JTgG+2NZ7fVVtXNajlSRJE8dGM0la2KHATyd5PvAIYOckf1VVL+tdaRTGzl/suPnDNOljpU/y8U/ysUvSOOsZrvpU4DeHHI6kIaiqrwDPmKX8DuC5c9Q5lS5vzCy/EnjIfGhV9T1ao9ssy84Ezlxc1JIkSf1zeEZJWkBVnVRVe1XVPnR3OX52ZoOZJEnSBHgLDlctSZIkaRWzp5kkSZIkaV6DGK56ruF5BzlUNcw9XPUoDA9sDMPfvzFIkiRpPjaaSdIiVNUUMDXkMCRJklbaVg9XPdfwvMee+ImBBjrXcNWjMDywMQx//8YgSZKk+Tg8oyRJkiRpXg5XLUmSJGkS2GgmSZIkSZIkSZKkiefwjJIkSZKkvjlctSRJkqTVyp5mkiRJkiRJkiT1Ick2Sf4pyceHHYukwbPRTJIkSZIkSZKk/rwGuGHYQUhaHjaaSZIkSZIkSZK0gCR7AS8A/mLYsUhaHs5pJkmSJEmSJEnSwt4C/A6w01wrJDkeOB5gzZo1TE1NLbjRNTvACU/bPJgIm372uxSbNm1atm0P2rjEOi5xwnjFulQ2mkmSJA1Rkr2Bc4DHA98HTq+qP5+xzjrgfODmVvSRqnr9CoYpSZIkSRMtyQuB26vqqvYdbVZVdTpwOsDatWtr3bo5V/2Bt517PqddM9hL9etfuvB+l2Jqaop+jmkUjEus4xInjFesS2WjmSRJ0nBtBk6oqi8l2Qm4KsnFVXX9jPU+X1UvHEJ8kiRJkiQ4FPjpJM8HHgHsnOSvquplQ45L0gA5p5kkSdIQVdVtVfWl9vxeugml9xxuVJIkSZKkXlV1UlXtVVX7AEcBn7XBTFp9bDSTJEkaEUn2AZ4BXD7L4h9P8uUkFyXZf2UjkyRJkiRJWv0cnlGSJGkEJHkU8DfAb1TVPTMWfwl4YlVtakOBfAzYb5ZtLHrC6UFP4jvoyavHYZJhY9x6ox4fjEeMkiRJWhlVNQVMDTkMScvARjNJkqQhS7IdXYPZuVX1kZnLexvRqurCJO9MsntVfWfGeouecHrQk/gee+InBrYtgLOO2HHkJxkeh4mQRz3GUY8PxiNGSZIkSdLWcXhGSZKkIUoS4Azghqp60xzrPL6tR5KD6c7h7li5KCVJkiRJklY/G80kSZKG61Dg5cBzklzdHs9P8ookr2jrvBi4NsmXgbcCR1VVDStgSZI0mZLsneRzSW5Icl2S17Tyk5N8s/dcpqfOSUluSnJjksN7yg9Kck1b9taeG4S2T/KhVn55m/N1us4xSb7WHses4KFLkqQJ4fCMkiRJQ1RVXwCywDpvB96+MhFJkiTNaTNwQlV9KclOwFVJLm7L3lxVf9a7cpKnAkcB+wNPAP4uyZOr6gHgXXRzsV4GXAgcAVwEHAfcWVX7JjkKeCPwc0l2A14HrAWq7fuCqrpzmY9ZkiRNEHuaSZIkSZIkaUFVdVtVfak9vxe4AdhznipHAh+sqvuq6mbgJuDgJHsAO1fVpa33/DnAi3rqnN2efxh4buuFdjhwcVVtbA1lF9M1tEmSJA2MPc0kSZIkSZK0KG3YxGcAl9MNN/3qJEcDV9L1RruTrkHtsp5qG1rZ/e35zHLaz1sAqmpzkruBx/SWz1KnN67j6XqwsWbNGqamphY8ljU7wAlP27zgeovRz357bdq0adF1BmnS9z8KMQx7/6MSgyQN27I0miU5E3ghcHtVHdDKTgZ+Gfh2W+21VXVhW3YSXff7B4Bfr6pPtfKDgLOAHei66r/G+TskSZIkSZKGJ8mjgL8BfqOq7knyLuAUumETTwFOA36R2YegrnnKWWKdBwuqTgdOB1i7dm2tW7du3mMBeNu553PaNYO9RLb+pQvvt9fU1BT9xLpcJn3/oxDDsPc/KjFI0rAt1/CMZzF7F/k3V9WB7THdYNY7vvURwDuTbNPWnx7fer/2sNu9JEmSJEnSkCTZjq7B7Nyq+ghAVX2rqh6oqu8D7wUObqtvAPbuqb4XcGsr32uW8i3qJNkWeDSwcZ5tSZIkDcyyNJpV1SV0JzT9WMr41pIkSZIkSVpBbW6xM4AbqupNPeV79Kz2M8C17fkFwFFJtk/yJLoboq+oqtuAe5Mc0rZ5NHB+T51j2vMXA59t14U+BRyWZNckuwKHtTJJkqSBWek5zQY1vvUWljJe9aZNmzjhaQ8s4RDmtpJj/o77GMPjHP84xw7jH78kSZIkaWgOBV4OXJPk6lb2WuDnkxxIN1zieuBXAKrquiTnAdcDm4FXVdX0xZhX8uCUHBe1B3SNcu9LchPdDdlHtW1tTHIK8MW23uurqt8btiVJkvqyko1mgxzfesvCJYxXPTU1xWlf+G4/cfdtseNVb41xH2N4nOMf59hh/OOXJEmSJA1HVX2B2a/XXDhPnVOBU2cpvxI4YJby7wEvmWNbZwJn9huvJEnSYi3XnGYPMeDxrSVJkiRJkiRJkqSBWbFGswGPby1JkiRJkiRJkiQNzLIMz5jkA8A6YPckG4DXAesGOL61JEmSJEmSJEmSNDDL0mhWVT8/S/EZ86y/qPGtJUmSJEmSJEmSpEFaseEZJUmSJEmSJEmSpFFlo5kkSZIkSZIkSZImno1mkrSAJI9IckWSLye5LskfDjsmSZIkSZIkSdJgLcucZpK0ytwHPKeqNiXZDvhCkouq6rJhByZJkiRJkiRJGgx7mknSAqqzqb3crj1qiCFJkiStKHveS5IkSZoENppJUh+SbJPkauB24OKqunzIIUmSJK2k6Z73TwcOBI5IcshwQ5IkSZKkwXJ4RknqQ1U9AByYZBfgo0kOqKpre9dJcjxwPMCaNWuYmppacLtrdoATnrZ5YHH2s89RsWnTprGKd9Am+fgn+dglaVxVVQH2vJckSZK0qtloJkmLUFV3JZkCjgCunbHsdOB0gLVr19a6desW3N7bzj2f064ZXCpe/9KF9zkqpqam6Oc9Wq0m+fgn+dglaZwl2Qa4CtgXeIc97yVJkiStNjaaSdICkjwWuL81mO0APA9445DDkrRKJNkbOAd4PPB94PSq+vMZ6wT4c+D5wL8Dx1bVl1Y6VkmTbWt73s/V03iQve5h7p73o9DT2RiGv39jkCRJ0nxsNJOkhe0BnN3urn4YcF5VfXzIMUlaPTYDJ1TVl5LsBFyV5OKqur5nnZ8E9muPZwHvaj8lacUttef9XD2Njz3xEwONb66e96PQ09kYhr9/Y5AkSdJ8bDSTpAVU1VeAZww7DkmrU1XdBtzWnt+b5AZgT6C30exI4Jw2p9BlSXZJskerK0nLzp73kiRJkiaBjWaSJEkjIsk+dI30M+cJ2hO4pef1hla2RaPZfMOizWXQw0MNepi1cRi+yhi33qjHB+MR4zKz570kSZp4SR4BXAJsT3dt/cNV9brhRiVpkGw0kyRJGgFJHgX8DfAbVXXPzMWzVKmHFMwzLNpcBj081KCHWTvriB1HfviqcRhia9RjHPX4YDxiXE72vJckSQLgPuA5VbUpyXbAF5JcVFWXDTswSYPxsGEHIEmSNOnal62/Ac6tqo/MssoGYO+e13sBt65EbJIkSdOS7J3kc0luSHJdkte08t2SXJzka+3nrj11TkpyU5IbkxzeU35QkmvasrcmSSvfPsmHWvnlrSf+dJ1j2j6+luSYFTx0SQKgOpvay+3a4yE3NEoaXzaaSZIkDVG7QHQGcENVvWmO1S4Ajk7nEOBu5zOTJElDsBk4oap+FDgEeFWSpwInAp+pqv2Az7TXtGVHAfsDRwDvbMO8AryLbljp/drjiFZ+HHBnVe0LvJk2f2KS3YDXAc8CDgZe19s4J0krJck2Sa4GbgcurqqZw+tLGmMOzyhJkjRchwIvB65pX7wAXgv8EEBVvRu4EHg+cBPw78AvrHyYkiRp0rWbdm5rz+9NcgPdPKtHAuvaamcDU8DvtvIPVtV9wM1JbgIOTrIe2LmqLgVIcg7wIuCiVufktq0PA29vNxkdTndxemOrczFdQ9sHlu2AJWkWVfUAcGCSXYCPJjmgqq6dXr6UuabX7DD4+aGXaz7ecZrrd1xiHZc4YbxiXSobzSRJkoaoqr7A7HOW9a5TwKtWJiJJkqSFtWETnwFcDqyZ7gVfVbcleVxbbU+gd56fDa3s/vZ8Zvl0nVvatjYnuRt4TG/5LHV64xrLi9XDvgg56fsfhRiGvf9RiWGcVNVdSaboGvCv7Slf9FzTbzv3fE67ZrCX6te/dOH9LsU4zfU7LrGOS5wwXrEulY1mkiRJkiRJ6luSR9HNx/obVXVPm45s1lVnKat5ypda58GCMb1YPeyLkJO+/1GIYdj7H5UYRl2SxwL3twazHYDn0YaRlbQ6OKeZJEmSJEmS+pJkO7oGs3Or6iOt+FtJ9mjL96Cb5we63mB791TfC7i1le81S/kWdZJsCzwa2DjPtiRpJe0BfC7JV4Av0g0b+/EhxyRpgGw0kyRJkiRJ0oLa3GJnADdU1Zt6Fl0AHNOeHwOc31N+VJLtkzwJ2A+4og3leG+SQ9o2j55RZ3pbLwY+24aq/hRwWJJdk+wKHNbKJGnFVNVXquoZVfVjVXVAVb1+2DFJGiyHZ5QkSZIkSVI/DgVeDlyT5OpW9lrgDcB5SY4DvgG8BKCqrktyHnA9sBl4VVU90Oq9EjgL2AG4qD2ga5R7X5Kb6HqYHdW2tTHJKXQ9OwBeX1Ubl+k4JUnShLLRTJIkSZIkSQuqqi8w+9xiAM+do86pwKmzlF8JHDBL+fdojW6zLDsTOLPfeCVJkhbL4RklSZIkSZIkSZI08Ww0kyRJkiRJkiRJ0sSz0UySJEmSJEmSJEkTz0YzSZIkSZIkSZIkTTwbzSRJkiRJkiRJkjTxbDSTJEmSJEmSJEnSxLPRTJIkSZIkSZIkSRPPRjNJkiRJkiRJkiRNPBvNJEmSJEmSJEmSNPFsNJMkSZIkSZIkSdLE23bYAUiSJGl4rvnm3Rx74ieGHYYkSZIkSdLQLUtPsyRnJrk9ybU9ZbsluTjJ19rPXXuWnZTkpiQ3Jjm8p/ygJNe0ZW9NkuWIV5IkSZIkSZIkSZNtuYZnPAs4YkbZicBnqmo/4DPtNUmeChwF7N/qvDPJNq3Ou4Djgf3aY+Y2JUmSJEmSJEmSpK22LI1mVXUJsHFG8ZHA2e352cCLeso/WFX3VdXNwE3AwUn2AHauqkurqoBzeupIkiStCrP10J+xfF2Su5Nc3R5/sNIxSpIkSZIkTYKVnNNsTVXdBlBVtyV5XCvfE7isZ70Nrez+9nxm+UMkOZ6uRxpr1qxhampqwWA2bdrECU97YJGHML9+9jsomzZtWtH9Ddo4xz/OscP4xy9Jq9BZwNvpbhCay+er6oUrE44kSZIkSdJkWslGs7nMNk9ZzVP+0MKq04HTAdauXVvr1q1bcKdTU1Oc9oXv9h9lH9a/dOH9DsrU1BT9HOeoGuf4xzl2GP/4JWm1qapLkuwz7DgkSZIkSZIm3XLNaTabb7UhF2k/b2/lG4C9e9bbC7i1le81S7kkSdKk+fEkX05yUZL9hx2MJEmSJEnSarSSPc0uAI4B3tB+nt9T/v4kbwKeAOwHXFFVDyS5N8khwOXA0cDbVjBeSZKkUfAl4IlVtSnJ84GP0Z0vPcRShqxeswOc8LTNAwt20MZhWGFj3HqjHh+MR4yStNySnAm8ELi9qg5oZScDvwx8u6322qq6sC07CTgOeAD49ar6VCs/iG6I6h2AC4HXVFUl2Z5uyOqDgDuAn6uq9a3OMcDvtX38UVWdvawHK0mSJtKyNJol+QCwDtg9yQbgdXSNZeclOQ74BvASgKq6Lsl5wPXAZuBVVTU92dgrefAk6qL2kKQVlWRvui9ujwe+D5xeVX8+3KgkTYqquqfn+YVJ3plk96r6zizrLnrI6redez6nXTMKI3bP7qwjdhz5YYXHYejjUY9x1OOD8YhRklbAWcw+F+ubq+rPeguSPBU4Ctif7ibpv0vy5HbN5110N/pcRtdodgTdNZ/jgDurat8kRwFvBH4uyW5015bW0k3dcVWSC6rqzuU5TEmSNKmW5QpJVf38HIueO8f6pwKnzlJ+JXDAAEOTpKXYDJxQVV9KshPdF7SLq+r6YQcmafVL8njgW+3u64Pphte+Y8hhSZKkCbTIuViPBD5YVfcBNye5CTg4yXpg56q6FCDJOcCL6BrNjgRObvU/DLw9SYDDgYuramOrczFdQ9sHBnBYkiRJPzC6txVL0oioqtuA29rze5PcAOxJ10NWkrbKHD30twOoqncDLwZemWQz8B/AUVVVQwpX0oSy572kBbw6ydHAlXQ3HN5J953psp51NrSy+9vzmeW0n7cAVNXmJHcDj+ktn6XOFkZluOrFDuk77GGAJ33/oxDDsPc/KjFI0rDZaCZJi9DuqnwG3VyLkrTV5umhP7387XTDIEnSMNnzXtJc3gWcQjds4inAacAvApll3ZqnnCXW2bJwRIarXv/Shffba9jDAE/6/kchhmHvf1RikKRhs9FMkvqU5FHA3wC/0TvHUM/yod/ROE53hE36HWyTfPyTfOySNK7seS9pLlX1rennSd4LfLy93ADs3bPqXsCtrXyvWcp762xIsi3waGBjK183o87UoI5BkiRpmo1mktSHJNvRNZidW1UfmW2dUbijcbF3Mw7TpN/BNsnHP8nHLkmrgT3vJfVKskdrWAf4GeDa9vwC4P1J3gQ8AdgPuKKqHkhyb5JD6PLI0cDbeuocA1xKN0T1Z9u8rp8C/jjJrm29w4CTlvvYJEnS5LHRTJIW0CaePgO4oareNOx4JEmShmVret7P1dN4peYRGoWezsYw/P0bw9aZYy7WdUkOpBsucT3wKwBVdV2S8+h6pG4GXlVVD7RNvRI4C9gBuKg9oPve9b4kN9H1MDuqbWtjklOAL7b1Xl9VG5ftQCVpDs7zKq1+NppJ0sIOBV4OXJPk6lb22qq6cHghSZIkrayt7Xk/V0/jY0/8xEDjnKvn/Sj0dDaG4e/fGLbOHHOxnjHP+qcCp85SfiVwwCzl3wNeMse2zgTO7DtYSVoezvMqrXI2mknSAqrqC8w+8bQkSdJEsOe9JEmS87xKk+Bhww5AkiRJkjTypnvePyfJ1e3x/GEHJUmSNCzO8yqtTvY0kyRJkiTNy573kiRJD5pvntf55nidy5odVm6e1601TvNyjkus4xInjFesS2WjmSRJkiRJkiRJfVhontf55nidy9vOPZ/Trhnspfq55nndWuM0L+e4xDouccJ4xbpUDs8oSZIkSZIkSdICnOdVWv1sNJMkSZIkSZIkaWHO8yqtcg7PKEmSJEmSJEnSApznVVr97GkmSZIkSZIkSZKkiWejmSRJkiRJkiRJkiaejWaSJEmSJEmSJEmaeDaaSZIkSZIkSZIkaeLZaCZJkiRJkiRJkqSJZ6OZJEmSJEmSJEmSJp6NZpIkSZIkSZIkSZp4NppJkiQNUZIzk9ye5No5lifJW5PclOQrSZ650jFKkiRJkiRNgm2HHcBqss+Jnxjo9ta/4QUD3Z4kSRpJZwFvB86ZY/lPAvu1x7OAd7WfkiRJkiRJGiB7mkmSJA1RVV0CbJxnlSOBc6pzGbBLkj1WJjpJkiRJkqTJYaOZJEnSaNsTuKXn9YZWJkmStKJmG1Y6yW5JLk7ytfZz155lJ7Uhpm9McnhP+UFJrmnL3pokrXz7JB9q5Zcn2aenzjFtH19LcswKHbIkSZowDs8oSZI02jJLWc26YnI8cDzAmjVrmJqaWnDja3aAE562eWviW1abNm3q6ziGyRi33qjHB+MRoyStgLN46LDSJwKfqao3JDmxvf7dJE8FjgL2B54A/F2SJ1fVA3TDTR8PXAZcCBwBXAQcB9xZVfsmOQp4I/BzSXYDXgespTsPuirJBVV157IfsSRJmig2mkmSJI22DcDePa/3Am6dbcWqOh04HWDt2rW1bt26BTf+tnPP57RrRveU8KwjdqSf4ximqakpY9xKox4fjEeMkrTcquqS3t5fzZHAuvb8bGAK+N1W/sGqug+4OclNwMFJ1gM7V9WlAEnOAV5E12h2JHBy29aHgbe3XmiHAxdX1cZW52K6hrYPDPoYJUnSZBvdKySSJEkCuAB4dZIPAs8C7q6q24YckyRJ0rQ10+cmVXVbkse18j3pepJNmx5i+v72fGb5dJ1b2rY2J7kbeAyLGK56VHreL7Z38rB7NE/6/kchhmHvf1RikKRhs9FMkiRpiJJ8gO7u7N2TbKAbemg7gKp6N92QRc8HbgL+HfiF4UQqSZK0KHMNMT3f0NNLqbNl4Yj0vF//0oX322vYPZonff+jEMOw9z8qMUjSsNloJkmSNERV9fMLLC/gVSsUjiRJ0mJ9K8kerZfZHsDtrXyuIaY3tOczy3vrbEiyLfBoYGMrXzejztRgD0OSJAkeNuwAJEmSJEmSNLYuAI5pz48Bzu8pPyrJ9kmeBOwHXNGGcrw3ySFtvrKjZ9SZ3taLgc+2G4g+BRyWZNckuwKHtTJJkqSBsqeZJEmSJEmSFjTHsNJvAM5LchzwDeAlAFV1XZLzgOuBzcCrquqBtqlXAmcBOwAXtQfAGcD7ktxE18PsqLatjUlOAb7Y1nt9VW1cxkOVJEkTykYzSZIkSZIkLWieYaWfO8f6pwKnzlJ+JXDALOXfozW6zbLsTODMvoOVJElaAodnlCRJkiRJkiRJ0sRb8UazJOuTXJPk6iRXtrLdklyc5Gvt564965+U5KYkNyY5fKXjlSRJkiRJkiRJ0uo3rJ5mz66qA6tqbXt9IvCZqtoP+Ex7TZKn0o1fvT9wBPDOJNsMI2BJkiRJkiRJkiStXqMyPOORwNnt+dnAi3rKP1hV91XVzcBNwMErH54kSZIkSZIkSZJWs22HsM8CPp2kgPdU1enAmqq6DaCqbkvyuLbunsBlPXU3tLItJDkeOB5gzZo1TE1NLRjEpk2bOOFpD2zNcSy7+Y5j06ZNfR3nqBrn+Mc5dhj/+CVJkiRJkiRJWg7DaDQ7tKpubQ1jFyf56jzrZpayekhB1/B2OsDatWtr3bp1CwYxNTXFaV/4bn8RD8n6l66bc9nU1BT9HOeoGuf4xzl2GP/4hyHJmcALgdur6oBhxyNJkjQMnhNJkiRJWu1WfHjGqrq1/bwd+CjdcIvfSrIHQPt5e1t9A7B3T/W9gFtXLlpJAuAsunkVJUmSJtlZeE4kSZIkaRVb0UazJDsm2Wn6OXAYcC1wAXBMW+0Y4Pz2/ALgqCTbJ3kSsB9wxUrGLElVdQmwcdhxSJIkDZPnRJIkadIlOTPJ7UmuHXYskpbHSg/PuAb4aJLpfb+/qj6Z5IvAeUmOA74BvASgqq5Lch5wPbAZeFVVjfZEZJIm1lLmV1yzA5zwtM0Di2Gc5qub9Pn1Jvn4J/nYJUmSJElj7Szg7cA5Q45D0jJZ0Uazqvo68PRZyu8AnjtHnVOBU5c5NEnaakuZX/Ft557PadcMLhXPNxfiqJn0+fUm+fgn+dglabWb7yaiuW6aGOQNRDD3TUSjcNOGMQx//8YgSdoaVXVJkn2GHYek5bPSPc0kSZIkSavUfDcRzXXTxLEnfmKgMcx1E9Eo3LRhDMPfvzFIkiRpPjaaSZIkSZIkSZI0AKMwfQcs3xQe49RbelxiHZc4YbxiXSobzSRpAUk+AKwDdk+yAXhdVZ0x3KgkSZJWludEkiRJCxuF6Ttg+abwGKfe0uMS67jECeMV61LZaCZJC6iqnx92DJIkScPmOZEkSZKk1e5hww5AkiRJkiRJkqRR13reXwo8JcmGJMcNOyZJg2WjmSRJ0hAlOSLJjUluSnLiLMvXJbk7ydXt8QfDiFOSJEmSJl1V/XxV7VFV21XVXg5VLa0+Ds8oSZI0JEm2Ad4B/C9gA/DFJBdU1fUzVv18Vb1wxQOUJEmSJEmaIPY0kyRJGp6DgZuq6utV9Z/AB4EjhxyTJEnSoiVZn+Sa1jP+yla2W5KLk3yt/dy1Z/2TWk/7G5Mc3lN+UNvOTUnemiStfPskH2rllyfZZ8UPUpIkrXo2mkmSJA3PnsAtPa83tLKZfjzJl5NclGT/lQlNkiRp0Z5dVQdW1dr2+kTgM1W1H/CZ9pokTwWOAvYHjgDe2XrgA7wLOB7Yrz2OaOXHAXdW1b7Am4E3rsDxSJKkCePwjJIkScOTWcpqxusvAU+sqk1Jng98jO4C0kM3lhxPd5GJNWvWMDU1tWAAa3aAE562eREhr6xNmzb1dRzDZIxbb9Tjg/GIUZJG0JHAuvb8bGAK+N1W/sGqug+4OclNwMFJ1gM7V9WlAEnOAV4EXNTqnNy29WHg7UlSVTPPnSRJkpbMRjNJkqTh2QDs3fN6L+DW3hWq6p6e5xcmeWeS3avqOzM3VlWnA6cDrF27ttatW7dgAG8793xOu2Z0TwlPeNpmTvvCdwe6zfVveMFAtzc1NUU/7/UwjXqMox4fjEeMkjRkBXw6SQHvaecla6rqNoCqui3J49q6ewKX9dSd7m1/f3s+s3y6zi1tW5uT3A08BtjinGhUbiJa7I0Ww745Y9L3PwoxDHv/oxKDJA3b6F4hkSRJWv2+COyX5EnAN+mGKfo/vSskeTzwraqqJAfTDa99x4pHKkmSNL9Dq+rW1jB2cZKvzrPuXL3t5+uF308P/ZG5iWj9Sxfeb69h35wx6fsfhRiGvf9RiUGShs1GM0mSpCFpd0m/GvgUsA1wZlVdl+QVbfm7gRcDr0yyGfgP4CiHIZIkSaOmqm5tP29P8lHgYOBbSfZovcz2AG5vq8/V235Dez6zvLfOhiTbAo8GNi7X8UiSpMn0sGEHIEmSNMmq6sKqenJV/UhVndrK3t0azKiqt1fV/lX19Ko6pKr+cbgRS5IkbSnJjkl2mn4OHAZcC1wAHNNWOwY4vz2/ADgqyfatx/1+wBVtKMd7kxySJMDRM+pMb+vFwGe9kUiSJA2aPc0kSZIkSZK0NdYAH+3audgWeH9VfTLJF4HzkhwHfAN4CUDrWX8ecD2wGXhVVT3QtvVK4CxgB+Ci9gA4A3hfkpvoepgdtRIHJkmSJouNZpIkSZIkSVqyqvo68PRZyu8AnjtHnVOBU2cpvxI4YJby79Ea3SRJkpaLwzNKkiRJkiRJkiRp4tloJkmSJEmSJEmSpInn8IyStIrsc+InBrq99W94wUC3J0mSJEmSJEmjyp5mkiRJkiRJkiRJmng2mkmSJEmSJEmSJGni2WgmSZIkSZIkSZKkiWejmSRJkiRJkiRJkiaejWaSJEmSJEmSJEmaeDaaSZIkSZIkSZIkaeLZaCZJkiRJkiRJkqSJZ6OZJEmSJEmSJEmSJp6NZpIkSZIkSZIkSZp4NppJkiRJkiRJkiRp4m077AA0t31O/MScy0542maOnWf5bNa/4QVbG5IkSZIkSZIkSdKqZKOZJEmSJGnVmOvmw6XceDjNGxAlSZKkyeDwjJIkSZIkSZIkSZp49jSTJEmSJEmSBmi+KTdms1BvWHu8SpK0Msaip1mSI5LcmOSmJCcOOx5Jk8c8JGm5LJRf0nlrW/6VJM8cRpyS5PmQpGEzD0kaBeYiaXUb+Z5mSbYB3gH8L2AD8MUkF1TV9cONTNKkMA9JWi595pefBPZrj2cB72o/tUSLvfN7IWcdseNAtyeNIs+HJA2beUjSKDAXSavfyDeaAQcDN1XV1wGSfBA4EjARLdKgLxCBwwNoYkxsHjJvSMuun/xyJHBOVRVwWZJdkuxRVbetfLiSJtjEng9JGhnmIUmjwFwkrXLj0Gi2J3BLz+sNzLi7OsnxwPHt5aYkN/ax3d2B7wwkwiH49RGJP29cctWRiH+Jxjl2WN74n7hM2x22BfMQTGYuWoqevDFxxz7DJB+/eehB/eSX2dbZE3hIo9lqzEOjcs4zn2e/cfRjZPTfx1GPD/qPcdzyUL8GcT401N/z1uSTrfjeM9Mo/K0PO4Zh738SYjAPjeH50EI5aoB5aC7Dfg+Gvf9RiGHY+x9kDKs1D8EYXatexrwxCn+r/RqXWMclThifWJ+y1Irj0GiWWcpqixdVpwOnL2qjyZVVtXZrAhsm4x+ecY4dxj/+IVkwD8Fk5qKtMcnHDpN9/JN87LPoJ7/0lYNgdeahUY8PjHEQRj0+GI8Yl9lWnw8N+z0c9v6NYTT2bwxjbVV/Lxt2DJO+/1GIYdj7H5UYxsDEX6s21sEblzhhfGJNcuVS6z5skIEskw3A3j2v9wJuHVIskiaTeUjScuknv5iDJI0Cc5GkYTMPSRoF5iJplRuHRrMvAvsleVKShwNHARcMOSZJk8U8JGm59JNfLgCOTucQ4G7nM5M0BJ4PSRo285CkUWAukla5kR+esao2J3k18ClgG+DMqrpuAJteVBfZEWT8wzPOscP4x7/iljEPwWT/Pib52GGyj3+Sj30Lc+WXJK9oy98NXAg8H7gJ+HfgFwYcxqj/PkY9PjDGQRj1+GA8Ylw2AzofGvZ7OOz9gzGMwv7BGMbSBHwvG3YMk75/GH4Mw94/jEYMI81r1YCxLodxiRPGJ9Ylx5mqWafEkCRJkiRJkiRJkibGOAzPKEmSJEmSJEmSJC0rG80kSZIkSZIkSZI08Say0SzJEUluTHJTkhOHHQ9Akr2TfC7JDUmuS/KaVr5bkouTfK393LWnzkntGG5McnhP+UFJrmnL3pokK3QM2yT5pyQfH8PYd0ny4SRfbb+DHx+z+P9v+7u5NskHkjxinOKfRKOYh5ZTkjOT3J7k2p6yOf9GV5Ol5PfVouWiK5J8uR37H7byVX/s42Ac8lCS9e3/0tVJrhx2PDD6+WyO+E5O8s32Pl6d5PnDiq/FM9J5cZ74Rup9HCejkG+GkU+GnS9GIR+Mwud92J9pz4dG33LmqNlyz3y/+2zld/XF5p3F7i/J9kk+1MovT7JPnzHM+XkbdAxLyTuDjGEpOWfA+190zlmOvwPNLQvknHTe2pZ/JckzhxFni2WhWF/aYvxKkn9M8vRRjLNnvf+a5IEkL17J+GbEsGCsSda1PHFdkr9f6RhbDAv97h+d5G97cs2g517vW2b5vzNj+eI/U1U1UQ+6CRr/Bfhh4OHAl4GnjkBcewDPbM93Av4ZeCrwp8CJrfxE4I3t+VNb7NsDT2rHtE1bdgXw40CAi4CfXKFj+E3g/cDH2+txiv1s4Jfa84cDu4xL/MCewM3ADu31ecCx4xL/JD5GNQ8t8zH/BPBM4Nqesln/RlfbY7H5fTU9Wi55VHu+HXA5cMgkHPuoP8YlDwHrgd2HHceMmEY6n80R38nAbw37veuJZ6Tz4jzxjdT7OC6PUck3w8gnw84Xo5APRuHzPuzPtOdDo/1Y7hw1W+6Z63fPAL6rLybvLGV/wK8C727PjwI+1GcMs37eliOGxeadQccwz/5X5D1gkTlnuf4OfCw95wDPb+932u/u8hGO9b8Bu7bnPzmMWPuJs2e9zwIXAi8e4fd0F+B64Ifa68eNaJyv7ckjjwU2Ag8f0vv6kP87M5Yv+jM1iT3NDgZuqqqvV9V/Ah8EjhxyTFTVbVX1pfb8XuAGusaQI+kadGg/X9SeHwl8sKruq6qbgZuAg5PsAexcVZdW91dxTk+dZZNkL+AFwF/0FI9L7DvTfbjOAKiq/6yqu8Yl/mZbYIck2wKPBG4ds/gnzUjmoeVUVZfQ/QPtNdff6KqyhPy+alRnU3u5XXsUE3DsY2Di8tCgjHo+myO+kTLqeXGe+LQ0E5tvhp0vRiEfjMLnfdifac+HRt4wctSyfVdfZN5Zyv56t/Vh4LnTvY8WiGEuA49hha6vzRnDEnLOoPe/2JyzLH8HmlM/OedI4Jz2u7wM2KX9PlbagrFW1T9W1Z3t5WXAXiscI/Sfx38N+Bvg9pUMboZ+Yv0/wEeq6hsAVTWMePuJs4Cd2mf/UXR5f/PKhtkCWfj/zqI/U5PYaLYncEvP6w2M2Jfg1q35GXR3g6ypqtug+8cLPK6tNtdx7Nmezyxfbm8Bfgf4fk/ZuMT+w8C3gb9MN7zkXyTZkTGJv6q+CfwZ8A3gNuDuqvo0YxL/hBr5PLRC5vobXbX6zO+rSrqhe6+mOzG9uKom5thH3LjkoQI+neSqJMcPO5h5jMPf9KvbUBRnZoSGABv1vDgjPhjR93HEjUq+GZV8Mgp/50P5Ox6Fz/uwPtOeD4205c5Rs+Welf6uPsj9/aBOVW0G7gYe02ccs33eljWGZby+1lcMfeacge9/kTlnpf8OJl0/OWdUzp0WG8dxdL15VtqCcSbZE/gZ4N0rGNds+nlPnwzsmmSq/e84esWie1A/cb4d+FG6jhvXAK+pqu8zmhb9mZrERrPZ7nyoFY9iDkkeRdfq/RtVdc98q85SVvOUL5skLwRur6qr+q0yS9lQYm+2pevC+a6qegbwXbqu6nMZqfjbidaRdN3onwDsmORl81WZpWyY7/8k8r2eQIvI76tKVT1QVQfS3XF2cJIDhhySOuOShw6tqmfSDfXxqiQ/MeyAxtS7gB8BDqS7wea0oUbTjHpenCW+kXwfx8Co5BvzSWcof8ej8Hkf5mfa86GRttw5ajG5Z6W/qy9lf0uNZa7P27LFsMzX1xaMYRE5Z+D7X2TOWcm/A/X33o3K+9t3HEmeTddo9rvLGtHs+onzLcDvVtUDyx/OvPqJdVvgILoR3Q4Hfj/Jk5c7sBn6ifNw4Gq6a9EHAm9vo7mNokV/piax0WwDsHfP673oWkSHLsl2dP9Qz62qj7Tib013F2w/p7tkznUcG9iyK+xKHN+hwE8nWU/XXfM5Sf6K8Yh9Op4N7c4b6LqWP5Pxif95wM1V9e2quh/4CN2YwuMS/yQa2Ty0wub6G111FpnfV6Xqhr2dAo5gwo59RI1FHqqqW9vP24GP0g0TMYpG+m+6qr7VLp58H3gvI/A+jnpenC2+UXwfx8RI5JsRyidD/Tsfxt/xKHzeR+Uz7fnQSFrWHDVH7lnp7+qD3N8P6rTpIR5NH0MxzvN5W5YYVuD62rwxLDLnLNvvoc+cs2J/BwL6yzkjce7UbxxJfoxuup4jq+qOFYqtVz9xrgU+2K5dvxh4Z5IXrUh0W+r39//JqvpuVX0HuAR4+grF1xvDQnH+At0wklVVNwE3A/9lheJbrEV/piax0eyLwH5JnpTk4XQTVl4w5Jho43+eAdxQVW/qWXQBcEx7fgxwfk/5UUm2T/IkYD/gitbF+t4kh7RtHt1TZ1lU1UlVtVdV7UP3fn62ql42DrG3+P8NuCXJU1rRc+kmXByL+OmGZTwkySPbfp9LN2b2uMQ/iUYyDw3BXH+jq8oS8vuqkeSxSXZpz3ega+T/KhNw7GNg5PNQkh2T7DT9HDgMuHa4Uc1ppP+mpy+QND/DkN/HUc+Lc8U3au/jGBl6vhmxfDLUv/OV/jsehc/7sD/Tng+NvGXLUfPknpX+rj7I/fVu68V0138W7AEzz+dt4DGs0PW1OWNYQs4Z9P4Xm3NW7O9AQH855wLg6HQOoZuG5baVDpQ+Yk3yQ3Q377+8qv55CDFCH3FW1ZOqap927frDwK9W1cdWPNL+fv/nA/8jybZJHgk8i+4676jF+Q26a9AkWQM8Bfj6ikbZv8V/pqpq4h7A84F/Bv4F+H/DjqfF9N/pugV+ha5r49UtzscAnwG+1n7u1lPn/7VjuBH4yZ7ytXT/fP+FbnzRrOBxrAM+3p6PTex03UivbO//x4Bdxyz+P6Q7CboWeB+w/TjFP4mPUcxDy3y8H6AbguJ+ujs8jpvvb3Q1PZaS31fLA/gx4J/asV8L/EErX/XHPg6PUc9DdHOOfrk9rhuVGEc9n80R3/voxpn/Ct0Xhj2G/B6OdF6cJ76Reh/H6THsfDOsfDLsfDEK+WAUPu/D/kzj+dDIP5YrR82Ve+b73bOV39UXm3cWuz/gEcBfAzcBVwA/3GcMc37eBh3DUvLOIGOYZ/8r8h6whJyzHH8HPhaXc4BXAK9ozwO8oy2/Blg7wrH+BXBnz9/6laMY54x1zwJePKrvaXv923QdOq6lG+J15OKkG5bx0+1v9FrgZUN8T2f7v7NVn6npZCdJkiRJkiRJkiRNrEkcnlGSJEmSJEmSJEnago1mkiRJkiRJkiRJmng2mkmSJEmSJEmSJGni2WgmSZIkSZIkSZKkiWejmSRJkiRJkiRJkoYuyZlJbk9ybZ/r/2yS65Ncl+T9W73/qtrabUiSJEmSJEmSJElbJclPAJuAc6rqgAXW3Q84D3hOVd2Z5HFVdfvW7N+eZpIkSZIkSZIkSRq6qroE2NhbluRHknwyyVVJPp/kv7RFvwy8o6rubHW3qsEMbDSTJEmSJEmSJEnS6Dod+LWqOgj4LeCdrfzJwJOT/EOSy5IcsbU72nZrNyBJkiRJkiRJkiQNWpJHAf8N+Osk08Xbt5/bAvsB64C9gM8nOaCq7lrq/mw0kyRJkiRJkiRJ0ih6GHBXVR04y7INwGVVdT9wc5Ib6RrRvrg1O5MkSZIkSZIkSZJGSlXdQ9cg9hKAdJ7eFn8MeHYr351uuMavb83+bDSTJEmSJEmSJEnS0CX5AHAp8JQkG5IcB7wUOC7Jl4HrgCPb6p8C7khyPfA54Ler6o6t2n9VbU19SZIkSZIkSZIkaezZ00ySJEmSJEmSJEkTz0YzSZIkSZIkSZIkTTwbzSRJkiRJkiRJkjTxbDSTJEmSJEmSJEnSxLPRTJIkSZIkSZIkSRPPRjNJkiRJkiRJkiRNPBvNJEmSJEmSJEmSNPFsNJMkSZIkSZIkSdLEs9FMkiRJkiRJkiRJE89GM0mSJEmSJEmSJE08G80kSZIkSZIkSZI08Ww0kyRJkiRJkiRJ0sSz0UySJEmSJEmSJEkTz0YzSZIkSZIkSZIkTTwbzSRJkiRJkiRJkjTxbDSTJEmSJEmSJEnSxLPRTJIkSZIkSZIkSRPPRjNJkiRJkiRJkiRNPBvNJEmSJEmSJEmSNPFsNJMkSZIkSZIkSdLEs9FsBCV5SpJ/SnJvku8n+f0+6727n3WTrE/yvK2PFJKcleSPBrGtlZLk0CRfS7IpyYuGHY80isxDy8s8JPXHXLS8zEXSwsxDy8s8JC2eeWl5mZek0TMj7/36AutWkn3b8x/koCTrkmwYYEzHJvnCoLan0WKj2Wj6HWCqqnaqqodV1Sn9VKqqV/S77oR7PfD2qnpUVX1sazbUm4jb64cn+XA7yawk62as/9tJrm1J/uYkv701+5eWkXloeQ0zD/1Gkq8nuSfJrUnenGTbrYlBWkbmouU1zFx0cpL72wWp6ccPb00M0jIxDy2v5cxD+7Sy3jzz+z3Lk+SNSe5ojz9Nkq2JQVoh5qXltWx5qZU9Msk7k3wnyd1JLtmafUgTojfvvXWld95zTrHs107G8WaD1chGs9H0ROC6YQcxjvpMXsv9/n4BeBnwb7MsC3A0sCtwBPDqJEctYyzSUpmHlmgM8tDfAs+sqp2BA4CnA/PeqSUNkbloicYgFwF8qF2Qmn58fRljkZbKPLREI5KHAHbpyTO9DQbHAy+iOxf6MeCFwK8scyzSIJiXlmhE8tLpwG7Aj7af/3cZ9yWtFuY9rSgbzUZMks8Czwbe3u6Ee//MbqRJTkhye5LbkvxCT93eLqe7J/l4kruSbEzy+SS9v+8Dk3yl3dXyoSSP6NnOC5Nc3er+Y5If61n2jCRfaj2lPgT01ptznzPrJflgT6wP6c6aLbvSviBdF9x7ktyS5OSe9aZb+o9L8g3gs638F5PckOTOJJ9K8sRW/i/ADwN/297f7ZM8OskZ7f38ZpI/SrJNzz7m2tb03UBfbtv6uar6z6p6S1V9AXhg5u+3qv60qr5UVZur6kbgfODQ+f8qpJVlHvrBtlZrHvqXqrpretPA94F9Z64nDZu56AfbWpW5SBoH5qEfbGss81Afv+JjgNOqakNVfRM4DTi2j3rS0JiXfrCtscxLSZ4C/DRwfFV9u6oeqKqrFvVHIE2YWfLerUl+qWf5Q3LEAtt7bbqenuuTvLSnfM5cAkx/nu9qMfx4T70/a5//m5P8ZE/5bkn+ssV7Z5KPtfI5c3WS44GXAr/T9vO3i3mvNDg2mo2YqnoO8Hng1VX1KOA/Z6zyeODRwJ7AccA7kuw6y6ZOADYAjwXWAK8Fqmf5z9L1dHoS3V11xwIkeSZwJt0ddo8B3gNc0E4UHg58DHgf3d0wfw38/xbaZx/1FvJdut5ZuwAvAF6Zh44r/T/p7tI5vC17LfC/WyyfBz4AUFU/AnwD+Kl2p+F9wNnAZrqLxs8ADgN+qb0f823rJ9q+n9629aFFHBNJAvwPvFNCI8Y8NKtVlYeS/J8k9wDfobu7+j19vxPSCjEXzWpV5SLgp9pFs+uSvLLvd0FaIeahWY1jHvrXdnHqL5Ps3lO+P/DlntdfbmXSyDIvzWqc8tKzgH8F/rBdtL8myWKOVZo4s+S9f96KzT0e2J0uRx4DnN4as2H+XDL9eZ7uvX5pe/0s4Ma2zT8FzmjXe6HLaY+kO7d4HPDmGXE8JFdX1enAucCftv381FYcq7bCqm00S3Jma629ts/1fzbJ9e1L+/uXO76tcD/w+qq6v6ouBDYBT5ljvT2AJ7Z1P19VvSdAb62qW6tqI91QXQe28l8G3lNVl7c7Xs4G7gMOaY/tgLe0bX4Y+GIf+1yo3ryqaqqqrqmq71fVV+hOQP7njNVOrqrvVtV/0J28/UlV3VBVm4E/prtL6okzt51kDfCTwG+0+rfTJbHpIRP73tYSnEz3GfzLAWxLWknmoTHPQ1X1/uqGZ3wy8G7gW0vdljRE5qLxzkXn0V28eizde/0HSX5+iduShsU8NNp56DvAf6Ub0ukgYCe6C1HTHgXc3fP6buBRPRe7pHFkXhrtvLQX3RD5dwNPAF4NnJ3kR/s9Xo2nVXydehz9flXdV1V/D3yC7iaBfnPJTP9aVe+tqgfoGtj3ANYk2YMud7yiqu5sue3ve+r1m6s1JKu20Qw4i+6umAUl2Q84CTi0qvYHfmP5wtpqd7R/xNP+ne5kf6b/D7gJ+HSSryc5ccby3rklerfxROCEdN3l70pyF7A33T/zJwDfnHEi9a997HOhevNK8qwkn0vy7SR3A6+ga8HvdUvP8ycCf94T/0a6Icj2nGXzT6Q7ObutZ/330N0BsNht9S3Jq+nuXnhBdXcuSePEPLQK8hBAVX2NrrfrO7d2W9IQmIvGOBdV1fXtYtwDVfWPwJ8DL17KtqQhMg+NcB6qqk1VdWV1Q+N/i+7i9GFJdm6rbAJ27qmyM7BpxnsjjRvz0gjnJeA/6C6W/1F1Q1n/PfA5ut5rWt3OYnVepx43d1bVd3te/ytdDuo3l8z0g1xZVf/enj6KLi9urKo756jXb67WkKzaRrOquoTun9UPJPmRJJ9MclW6sZP/S1v0y8A7pv+Q250jY62q7q2qE6rqh4GfAn4zyXP7qHoLcGpV7dLzeGRVfQC4Ddhzxp13P9THPuetR9f99ZHTL5I8fkZM7wcuAPauqkfT9YqYefdf78nVLcCvzDiGHdoFmdmO9z5g9551d27/lBa7rb4k+UXgROC5VbVhqduRRp15aHTz0AzbAj8yoG1JI8dcNDa5qGY5FmlVMA+NTB6ajms63uvohqme9nQcOl8Twrw0tLz0lTnKtcrVhF+nHrAtcgLdUIf92jXJjj2vfwi4tT2fL5cs9oaaW4DdkuyyyHpL2ZeWwaptNJvD6cCvVdVBwG/x4J31TwaenOQfklyWpK+W/1GWbmLWfdtJxz10E7D3Mwn7e4FXtNb1JNkx3USIOwGX0o3j/OtJtk3yv4GD+9jnvPVoY8cnOTDd5LInz4hpJ7rW+e8lORj4Pwscw7uBk5Ls3+J6dJKXzLZiVd0GfBo4LcnOSR7W/mn9zz639S26SWJ/IN1Y3tOT3T48ySOmT/7STTD5x8D/qqqvL3Ac0lgzD41sHvqlJI9rz59KdwfbZxY4HmlsmYtGNhcdmWTX9t4eDPw6cP4CxyONJfPQcPJQe9+e0rbzGOCtwFRVTQ/JeA7dRfs9kzyBbr6lsxY4HmlVMC8N7fzoEro5005qx3oosA741AIxa3WamOvUA3Y18L+TPDLJvnRzgi3GHyZ5eJL/AbyQbh5FmD+XfBv4PjO+78yl5Y6LgHe27zzbJfmJheo1D/lepZU3MY1mSR4F/Dfgr5NcTdedeo+2eFtgP7p/VD8P/EWW1hI8SvYD/o5uyIlLgXdW1dRClarqSro7Gt4O3EnXdf7Ytuw/6SY3PbYt+zngIwvtc6F6VfXPwOtb3a8BX5gR1q8Cr09yL/AHdHNgzHcMHwXeCHwwyT3AtXTjyM7laODhwPUtvg/T/jb62NbJdONP35XkZ1vZjXRd7vekO/H5D7ou+wB/RDdZ7heTbGqPd893PNIYMw+NZh46FLgmyXeBC9vjtfMdjzTmzEWjmYuOontP76W7cP3G6uZFkVYj89Bw8tAPA5+kyzPX0vUW6Z078T10czVd05Z/opVJk8C8NIS8VFX3A0cCz6eb1+y9wNFV9dX5YtbqM4HXqQfpzcB/0jUunc2W85Uu5N/oPse3tnqv6Pn8zZlLqht68VTgH9rn+ZA+9vVyuuFYvwrcTv/DbJ4BPLXt52N91tGApVbxcN1J9gE+XlUHpBu3/Maq2mOW9d4NXFZVZ7XXnwFOrKq+Jx7V4iQ5C9hQVb837FgkTSbzkKRRYC6SNGzmIUmjxryk1cjr1NL4mJieZlV1D3DzdDfp1o18evzyjwHPbuW703WDdeg8SZIkSZIkSdLAeJ1aGm2rttEsyQfouns/JcmGJMcBLwWOS/Jlugl+j2yrfwq4I8n1wOeA366qO4YRtyRJkiRJkiRpdfA6tTReVvXwjJIkSZIkSZIkSVI/Vm1PM0mSJEmSJEmSJKlfNppJkiRJkiRJkiRp4m077AAGbffdd6999tmnr3W/+93vsuOOOy5vQMtknGOH8Y5/nGOH5Y3/qquu+k5VPXZZNj5mdtlll9p3332HHcacRvnveJRjg9GOb5Rjg5WJzzz0oH7PiUb976Yfq+EYYHUcx2o4Bti64zAPPWhS8tC4xw/jfwzjHj8M9hjMQw9ajXloXGIdlzjBWJeDeehBM/PQKPwOhx3DsPdvDKOx/+WOYavyUFWtqsdBBx1U/frc5z7X97qjZpxjrxrv+Mc59qrljR+4skYgD4zC48lPfvLWvp3LapT/jkc5tqrRjm+UY6tamfjMQw8++j0nGvW/m36shmOoWh3HsRqOoWrrjsM8NHl5aNzjrxr/Yxj3+KsGewzmodWdh8Yl1nGJs8pYl4N5aO48NAq/w2HHMOz9G8No7H+5Y9iaPOTwjJIkSZIkSZIkSZp4NppJkiRJkiRJkiRp4tloJkmSJEmSJEmSpIlno5kkSZIkSZIkSZImno1mkiRJkiRJkiRJmng2mkmSJEmSJEmSJGni2WgmSZIkSWMiyTZJ/inJx9vr3ZJcnORr7eeuPeuelOSmJDcmObyn/KAk17Rlb02SVr59kg+18suT7NNT55i2j68lOWYFD1mSJEmSVoyNZpIkSZI0Pl4D3NDz+kTgM1W1H/CZ9pokTwWOAvYHjgDemWSbVuddwPHAfu1xRCs/DrizqvYF3gy8sW1rN+B1wLOAg4HX9TbOSZIkSdJqse2wAxima755N8ee+ImBbW/9G14wsG1JkoZjnwX+L5zwtM2L/t/h/weNMs+HpPGRZC/gBcCpwG+24iOBde352cAU8Lut/INVdR9wc5KbgIOTrAd2rqpL2zbPAV4EXNTqnNy29WHg7a0X2uHAxVW1sdW5mK6h7QODOC7zkKRhG3QeAnORpMVZ6FrEUpiHpKWxp5kkSZIkjYe3AL8DfL+nbE1V3QbQfj6ule8J3NKz3oZWtmd7PrN8izpVtRm4G3jMPNuSJEmSpFVlonuaSZIkSdI4SPJC4PaquirJun6qzFJW85Qvtc6WO02Opxv6kTVr1jA1NbVgoGt26HpyD0o/+xykTZs2rfg+B23cj2Hc44fVcQySJEmrgY1mklatNvzQvcADwOaqWtvm5PgQsA+wHvjZqrqzrX8S3VweDwC/XlWfauUHAWcBOwAXAq+pqlkvFEmSJC2TQ4GfTvJ84BHAzkn+CvhWkj2q6rYkewC3t/U3AHv31N8LuLWV7zVLeW+dDUm2BR4NbGzl62bUmZotyKo6HTgdYO3atbVu3brZVtvC2849n9OuGdxX0/UvXXifgzQ1NUU/xznKxv0Yxj1+WB3HIEmStBo4PKOk1e7ZVXVgVa1tr08EPlNV+wGfaa9J8lTgKGB/ujk63plkm1bnXXR3TO/XHkesYPySJElU1UlVtVdV7UN3zvLZqnoZcAFwTFvtGOD89vwC4Kgk2yd5Et05zBVtCMd7kxzS5is7ekad6W29uO2jgE8BhyXZNcmuwGGtTJIkSZJWFXuaSZo0R/LgndJn090l/but/INVdR9wc5KbgINbb7Wdq+pSgCTnAC8CLlrRqCVJkmb3BuC8JMcB3wBeAlBV1yU5D7ge2Ay8qqoeaHVeyYO96C/iwfOaM4D3tfOgjXSNc1TVxiSnAF9s672+qjYu94FJkiRJ0kqz0UzSalbAp5MU8J42XNCadoc1bRijx7V19wQu66k7PcH9/e35zPKH6J3D47GPfexIz0kwynMmDDu2heZUWcq8Kyt1PMN+7xYy6vFJ0rioqina8IhVdQfw3DnWOxU4dZbyK4EDZin/Hq3RbZZlZwJnLjVmSZIkSRoHNppJWs0OrapbW8PYxUm+Os+6Wz3xfe8cHk95ylP6msNjWEZ5zoRhx3bsiZ+Yd/kJT9u86HlXVmpulWG/dwsZ9fgkSZIkSZI02ZzTTNKqVVW3tp+3Ax8FDga+lWQPgPbz9rb69MT30/YCbm3le81SLkmSJEmSJElaRWw0k7QqJdkxyU7Tz+kmrL+WLSe4P4YtJ74/Ksn2SZ4E7Adc0YZyvDfJIUkCHN1TR5IkSZIkSZK0Sjg8o6TVag3w0a6di22B91fVJ5N8ETgvyXHAN2jzdlTVdUnOA64HNgOvqqoH2rZeCZwF7ABc1B6SJEmSJEmSpFXERjNJq1JVfR14+izldwDPnaPOqcCps5RfCRww6BglSZIkSZIkSaPD4RklSZIkSZIkSZI08cai0SzJ+iTXJLk6yZXDjkeSJE2WJI9IckWSLye5LskftvKTk3yznaNcneT5PXVOSnJTkhuTHN5TflA7r7kpyVvbfIm0ORU/1MovT7JPT51jknytPY5BkiRpBCXZJsk/Jfl4e71bkovbOczFSXbtWXdg50qSJEmDMhaNZs2zq+rAqlo77EAkSdLEuQ94TlU9HTgQOCLJIW3Zm9s5yoFVdSFAkqcCRwH7A0cA70yyTVv/XcDxwH7tcUQrPw64s6r2Bd4MvLFtazfgdcCzgIOB1/VecJIkSRohrwFu6Hl9IvCZqtoP+Ex7PdBzJUmSpEEap0YzSZKkoajOpvZyu/aoeaocCXywqu6rqpuBm4CDk+wB7FxVl1ZVAecAL+qpc3Z7/mHgue3O6sOBi6tqY1XdCVzMgxePJEmSRkKSvYAXAH/RU9x7fnM2W573DOpcSZIkaWC2HXYAfSrg00kKeE9Vnd67MMnxdHchsWbNGqampvra6Jod4ISnbR5YkP3udxA2bdq0ovsbtHGOf5xjh/GPX5KGpd39fBWwL/COqro8yU8Cr05yNHAlcEJr2NoTuKyn+oZWdn97PrOc9vMWgKranORu4DG95bPUkSRJGhVvAX4H2KmnbE1V3QZQVbcleVwrH+S50ncGexiSJGmSjUuj2aFVdWs7ubo4yVer6pLpha0R7XSAtWvX1rp16/ra6NvOPZ/TrhncW7D+pf3tdxCmpqbo9zhH0TjHP86xw/jHL0nDUlUPAAcm2QX4aJID6IYPOoXuBp9TgNOAXwRmu+u55ilniXW2sJQbicb5JqJpq+WGkNVwHKvhGGD1HIckrZQkLwRur6qrkqzrp8osZUs9V5oZy9DPh2D5zonG5X/UuMQJxipJ2tJYNJpV1a3t5+1JPko3n8cl89eSJEkavKq6K8kUcERV/dl0eZL3Ah9vLzcAe/dU2wu4tZXvNUt5b50NSbYFHg1sbOXrZtSZmiO2Rd9INM43EU1bLTeErIbjWA3HAKvnOCRpBR0K/HSS5wOPAHZO8lfAt5Ls0XqZ7QHc3tYf5LnSFkbhfAiW75xoXP5HjUucYKxavCTrgXuBB4DNVbV2uBFJGqSRn9MsyY5Jdpp+DhwGXDvcqCRJ0iRJ8tjWw4wkOwDPA77aLv5M+xkePEe5ADgqyfZJnkQ3if0VbXiie5Mc0ubgOBo4v6fOMe35i4HPtrk8PgUclmTXJLvSnQt9armOVZIkabGq6qSq2quq9gGOojuPeRlbnt8cw5bnPYM6V5KkYXh2VR1og5m0+oxDT7M1dEMgQRfv+6vqk8MNSZIkTZg9gLPbvGYPA86rqo8neV+SA+mGBloP/ApAVV2X5DzgemAz8Ko2vCPAK4GzgB2Ai9oD4AzgfUluortr+qi2rY1JTgG+2NZ7fVU95K5qSZKkEfQG4LwkxwHfAF4Cgz1XkiRJGqSRbzSrqq8DTx92HJIkaXJV1VeAZ8xS/vJ56pwKnDpL+ZXAAbOUf492IWmWZWcCZy4iZEmSpKGoqinaUNJVdQfw3DnWG9i5kiStsAI+naSA97RhYX9gvrkV55qXbtDzKsLccysOe268Ye/fGEZj/6MSw2xGvtFMkiRJkiRJkqQRcWhV3ZrkccDFSb5aVZdML5xvbsW55qU79sRPDDzIueZWHPbceMPevzGMxv5HJYbZjPycZpIkSZIkSZIkjYKqurX9vB34KHDwcCOSNEg2mkmSJEnSiEvyiCRXJPlykuuS/GErPznJN5Nc3R7P76lzUpKbktyY5PCe8oOSXNOWvTVtAukk2yf5UCu/PMk+PXWOSfK19jhmBQ9dkiRpZCTZMclO08+Bw4BrhxuVpEFyeEZJkiRJGn33Ac+pqk1JtgO+kOSituzNVfVnvSsneSpwFLA/8ATg75I8uaoeAN5FN8/GZcCFwBHARcBxwJ1VtW+So4A3Aj+XZDfgdcBaujk8rkpyQVXduczHLEmSNGrWAB9t9xxtC7y/qj453JAkDZKNZpIkSZI04qqqgE3t5XbtUfNUORL4YFXdB9yc5Cbg4CTrgZ2r6lKAJOcAL6JrNDsSOLnV/zDw9tYL7XDg4qra2OpcTNfQ9oFBHZ8kSdI4qKqvA08fdhySlo/DM0qSJEnSGEiyTZKrgdvpGrEub4teneQrSc5Msmsr2xO4paf6hla2Z3s+s3yLOlW1GbgbeMw825IkSZKkVcWeZpIkSZI0BtrQigcm2YVuWKAD6IZaPIWu19kpwGnALwKZbRPzlLPEOltIcjzd0I+sWbOGqampOY7mQWt2gBOetnnB9frVzz4HadOmTSu+z0Eb92MY9/hhdRyDJEnSamCjmSRJkiSNkaq6K8kUcETvXGZJ3gt8vL3cAOzdU20v4NZWvtcs5b11NiTZFng0sLGVr5tRZ2qO2E4HTgdYu3ZtrVu3brbVtvC2c8/ntGsG99V0/UsX3ucgTU1N0c9xjrJxP4Zxjx9WxzFIkiStBg7PKEmSJEkjLsljWw8zkuwAPA/4apI9elb7GeDa9vwC4Kgk2yd5ErAfcEVV3Qbcm+SQNl/Z0cD5PXWOac9fDHy2zaX2KeCwJLu24R8Pa2WSJEmStKrY00ySJEmSRt8ewNlJtqG7+fG8qvp4kvclOZBuuMT1wK8AVNV1Sc4Drgc2A69qwzsCvBI4C9gBuKg9AM4A3pfkJroeZke1bW1Mcgrwxbbe66tq4zIeqyRJkiQNhY1mkiRJkjTiquorwDNmKX/5PHVOBU6dpfxK4IBZyr8HvGSObZ0JnLmIkCVJkiRp7Dg8oyRJkiRJkiRJkiaejWaSJEmSJEn6/7P39/G2VnW9//96C4i7FAXEFe5NYYkUQmrskI7nZiklaJ6w708TIwWjSI+d7ETlpnO+B9Po4PmGmpDaTgkwFDmkQQoSYSvqxI3gQbnT2MlOthAkG5VtR3Lj5/fHNZbMvVh3ezPXvFuv5+MxH2vOcY1xXZ8x11xjXfMa1xhDkiRp1bPTTJIkSZIkSZIkSauenWaSJEmSJEmSJEla9ew0kyRJkiRJkiRJ0qpnp5kkSZIkSZIkSZJWPTvNJEmSJEmSJEmStOrtPuwAJEl6LA7c8Ilhh6BVIMkTgKuBPenOny6uqtOS7AN8BDgQ2Az8TFU90MqcCpwEPAz8SlVd0dIPB84F1gCXAW+qqkqyJ3A+cDhwP/CqqtrcypwA/LcWzu9U1XkrXGVJkiRJkqRVx5FmkiRJS3sIeFFVPQd4LnBMkiOBDcBVVXUQcFV7TZJDgOOAZwPHAO9Jslvb13uBk4GD2uOYln4S8EBVPRN4J/D2tq99gNOA5wNHAKcl2XtFaytJkiRJkrQK2WkmSZK0hOpsay/3aI8CjgVmR32dB7y8PT8WuLCqHqqqO4FNwBFJ9gf2qqprqqroRpb1lpnd18XAUUkCHA1cWVVb2yi2K3mko02SJEmSJEl9YqeZpImWZLck/yfJx9vrfZJcmeSO9nPvnrynJtmU5AtJju5JPzzJzW3bu9tFbEmrTGtPbgLuo+vEug6Yqqp7ANrPp7Xsa4G7eopvaWlr2/O56TuUqartwNeAfRfZlyRJkiRJkvrINc0kTbo3AbcDe7XXs1OpnZFkQ3v95jlTqT0d+Mskz6qqh3lkKrVr6dYfOga4fLDVkDRsrT14bpKnAB9Lcugi2efrXK9F0ne1zI4HTU6ma6+YmppiZmZmkRA7U2vglMO2L5lvuZZzzH7btm3bUI7bb5NQj0moA0xOPSRJkiRJO8dOM0kTK8k64CeB04Ffa8nHAtPt+XnADPBmeqZSA+5MMjuV2mbaVGptn7NTqdlpJq1SVfXVJDN0Hej3Jtm/qu5pUy/e17JtAQ7oKbYOuLulr5snvbfMliS7A08Gtrb06TllZhaIbSOwEWD9+vU1PT09X7YdnHXBJZx5c/9OCTcfv/Qx+21mZobl1HXUTUI9JqEOMDn1kCRJkiTtHKdnlDTJ3gX8JvDtnrR+TqUmaZVIsl8bYUaSNcCPA58HLgVOaNlOAC5pzy8FjkuyZ5JnAAcB17d258EkR7apXl87p8zsvl4BfKqte3YF8OIke7cpZV/c0iRJkiRJktRHjjSTNJGSvAy4r6puTDK9nCLzpO3ytGj77bffSE/rNMrTTu1sbP2cVm45dmUqu0G916P8e4XRj28J+wPnJdmN7qaji6rq40muAS5KchLwJeCVAFV1a5KLgNuA7cAb2/SOAG8AzgXW0I1anR25+gHgg22k61a6KWOpqq1J3gZ8uuV7a1VtXdHaSpIkSZIkrUJ2mkmaVC8AfirJS4EnAHsl+RP6O5XaDnqnRTv44IOXNS3asIzytFM7G9uJGz6xcsHM45TDtu/0VHaDmq5ulH+vMPrxLaaqPgc8b570+4GjFihzOt30sHPTbwAetR5aVX2T1uk2z7ZzgHN2LmpJkiRJkiTtDKdnlDSRqurUqlpXVQfSjdb4VFX9HP2dSk2SJGkgkjwhyfVJPpvk1iS/3dL3SXJlkjvaz717ypyaZFOSLyQ5uif98CQ3t23vbuc4tPOgj7T065Ic2FPmhHaMO5KcgCRJkiRNIDvNJK02ZwA/keQO4Cfaa6rqVmB2KrVP8uip1N4PbAL+gUemUpMkSRqUh4AXVdVzgOcCxyQ5EtgAXFVVBwFXtdckOYTuxqFnA8cA72lTzAK8l25K6YPa45iWfhLwQFU9E3gn8Pa2r32A04DnA0cAp/V2zkmSJEnSpBib6RnbF7wbgC9X1cuGHY+k8VFVM8BMe963qdQkSZIGpaoK2NZe7tEeBRwLTLf08+jOed7c0i+sqoeAO9t6iUck2QzsVVXXACQ5H3g53U1BxwJvafu6GDi7jUI7Grhydj3FJFfSdbR9eEUqK0mSJElDMk4jzd4E3D7sICRJkiRpGJLsluQmujVZr6yq64CpNp007efTWva1wF09xbe0tLXt+dz0HcpU1Xbga8C+i+xLkiRJkibKWIw0S7IO+Em6ESC/NuRwJEmSJGng2tTRz03yFOBjSRYbCZ/5drFI+q6W2fGgycl0Uz8yNTXFzMzMIiF2ptbAKYdtXzLfci3nmP20bdu2gR+z38a9DuMeP0xGHSRJkibBWHSaAe8CfhN40pDjkCRJkqShqqqvJpmhmyLx3iT7V9U9SfanG4UG3WiwA3qKrQPubunr5knvLbMlye7Ak4GtLX16TpmZBWLbCGwEWL9+fU1PT8+XbQdnXXAJZ97cv6+mm49f+pj9NDMzw3LqOcrGvQ7jHj9MRh0kSZImwch3miV5GXBfVd2YZHqBPDt9NyOM9x2N434X2jjHP86xw/jHL0mStBol2Q/4VuswWwP8OPB24FLgBOCM9vOSVuRS4ENJ3gE8HTgIuL6qHk7yYJIjgeuA1wJn9ZQ5AbgGeAXwqaqqJFcAv5tk75bvxcCpK1tjSZKk0ZRkN+AG4MtV9bJhxyOpv0a+0wx4AfBTSV4KPAHYK8mfVNXPzWbYlbsZYbzvaBz3u9DGOf5xjh3GP35JkqRVan/gvHaR5nHARVX18STXABclOQn4EvBKgKq6NclFwG3AduCNbXpHgDcA5wJrgMvbA+ADwAeTbKIbYXZc29fWJG8DPt3yvbWqtq5obSVJkkbXm4Dbgb2GHYik/hv5TrOqOpV2F2MbafbrvR1mkiRJkjTpqupzwPPmSb8fOGqBMqfTrQs9N/0G4FHroVXVN2mdbvNsOwc4Z+eiliRJmixJ1gE/SXeO9WtDDkfSCnjcsAOQJEmSJEmSJGkMvAv4TeDbQ45D0goZ+ZFmvapqhgUWnJYkSZIkSZIkaSUkeRlwX1Xd2GZEWyjfycDJAFNTU8zMzHxn27Zt23Z4PeuUw7b3N1iY9ziLxTAowz6+MYzG8UclhvmMVaeZJEmSJEmSRkuSJwBXA3vSXWu6uKpOS7IP8BHgQGAz8DNV9UArcypwEvAw8CtVdUVLP5xH1l28DHhTVVWSPYHzgcOB+4FXVdXmAVVRkgBeAPxUkpcCTwD2SvInc5cSqqqNwEaA9evX1/T09He2zczM0Pt61okbPtH3YDcf/+jjLBbDoAz7+MYwGscflRjm4/SMkiRJkiRJeiweAl5UVc8Bngsck+RIYANwVVUdBFzVXpPkEOA44NnAMcB7kuzW9vVeuhEaB7XHMS39JOCBqnom8E7g7QOolyR9R1WdWlXrqupAujbsU3M7zCSNPzvNJEmSJEmStMuqs6293KM9CjgWOK+lnwe8vD0/Friwqh6qqjuBTcARSfYH9qqqa6qq6EaW9ZaZ3dfFwFFJsnK1kiRJq5GdZpIkSZIkSXpMkuyW5CbgPuDKqroOmKqqewDaz6e17GuBu3qKb2lpa9vzuek7lKmq7cDXgH1XpDKStISqmqmqlw07Dkn955pmkiRJkiRJekyq6mHguUmeAnwsyaGLZJ9vhFgtkr5YmR13nJxMN70jU1NTzMzMLBJGZ2oNnHLY9iXz7YzlHHdXbNu2bcX23U/jEicYqyRpR3aaSZIkSZIkqS+q6qtJZujWIrs3yf5VdU+bevG+lm0LcEBPsXXA3S193TzpvWW2JNkdeDKwdZ7jbwQ2Aqxfv76mp6eXjPmsCy7hzJv7e4ls8/FLH3dXzMzMsJw6Ddu4xAnGKknakdMzSpIkSZIkaZcl2a+NMCPJGuDHgc8DlwIntGwnAJe055cCxyXZM8kzgIOA69sUjg8mObKtV/baOWVm9/UK4FNt3TNJkqS+sdNMkiRpCUkOSPJXSW5PcmuSN7X0tyT5cpKb2uOlPWVOTbIpyReSHN2TfniSm9u2d88uYN8uGn2kpV+X5MCeMickuaM9TkCSJGm07A/8VZLPAZ+mW9Ps48AZwE8kuQP4ifaaqroVuAi4Dfgk8MY2vSPAG4D3A5uAfwAub+kfAPZNsgn4NWDDIComSZJWF6dnlCRJWtp24JSq+kySJwE3JrmybXtnVf1eb+YkhwDHAc8Gng78ZZJntYtB76VbZ+Na4DK6qYsuB04CHqiqZyY5Dng78Kok+wCnAevp1u24McmlVfXACtdZkiRpWarqc8Dz5km/HzhqgTKnA6fPk34D8Kj10Krqm8ArH3OwkiRJi3CkmSRJ0hKq6p6q+kx7/iBwO7B2kSLHAhdW1UNVdSfdndJHtLU89qqqa9p0QucDL+8pc157fjFwVBuFdjTd3dpbW0fZlXQdbZIkSZIkSeojO80kSZJ2Qps28XnAdS3pl5N8Lsk5SfZuaWuBu3qKbWlpa9vzuek7lKmq7cDXgH0X2ZckSZIkSZL6yOkZJUmSlinJE4E/BX61qr6e5L3A2+imTXwbcCbw80DmKV6LpLOLZebGdzLd1I9MTU0xMzOzYF1mTa2BUw7bvmS+5VrOMftt27ZtQzluv01CPSahDjA59ZAkSZIk7Rw7zSRJkpYhyR50HWYXVNVHAarq3p7tfwR8vL3cAhzQU3wdcHdLXzdPem+ZLUl2B54MbG3p03PKzMwXY1VtBDYCrF+/vqanp+fLtoOzLriEM2/u3ynh5uOXPma/zczMsJy6jrpJqMck1AEmpx6SJEmSpJ3j9IySJElLaGuLfQC4vare0ZO+f0+2nwZuac8vBY5LsmeSZwAHAddX1T3Ag0mObPt8LXBJT5kT2vNXAJ9q655dAbw4yd5t+scXtzRJkiRJkiT1kSPNJEmSlvYC4DXAzUluamm/Bbw6yXPppkvcDPwSQFXdmuQi4DZgO/DGqnq4lXsDcC6wBri8PaDrlPtgkk10I8yOa/vamuRtwKdbvrdW1dYVqaUkSZIkSdIqZqeZJEnSEqrqb5l/bbHLFilzOnD6POk3AIfOk/5N4JUL7Osc4Jzlxitp8iQ5ADgf+B7g28DGqvr9JG8BfhH455b1t6rqslbmVOAk4GHgV6rqipZ+OI903l8GvKmqKsme7RiHA/cDr6qqza3MCcB/a8f4nao6b0UrLEmSJElDYKeZJEmSJI2+7cApVfWZJE8CbkxyZdv2zqr6vd7MSQ6hG7H6bODpwF8meVYb9fpe4GTgWrpOs2PoRr2eBDxQVc9MchzwduBVSfYBTgPW042svTHJpVX1wArXWZIkSZIGyjXNJEmSJGnEVdU9VfWZ9vxB4HZg7SJFjgUurKqHqupOYBNwRFuLca+quqatm3g+8PKeMrMjyC4GjmrrLx4NXFlVW1tH2ZV0HW2SJEmSNFHsNJMkSZKkMZLkQOB5wHUt6ZeTfC7JOUn2bmlrgbt6im1paWvb87npO5Spqu3A14B9F9mXJEmSJE0Up2eUJEmSpDGR5InAnwK/WlVfT/Je4G100ya+DTgT+HnmX4exFklnF8vMje9kuqkfmZqaYmZmZsG6zJpaA6cctn3JfMu1nGP207Zt2wZ+zH4b9zqMe/wwGXWQJEmaBHaaSZIkSdIYSLIHXYfZBVX1UYCqurdn+x8BH28vtwAH9BRfB9zd0tfNk95bZkuS3YEnA1tb+vScMjPzxVhVG4GNAOvXr6/p6en5su3grAsu4cyb+/fVdPPxSx+zn2ZmZlhOPUfZuNdh3OOHyaiDJEnSJHB6RkmSJEkacW1tsQ8At1fVO3rS9+/J9tPALe35pcBxSfZM8gzgIOD6qroHeDDJkW2frwUu6SlzQnv+CuBTbd2zK4AXJ9m7Tf/44pYmSZIkSRPFkWaSJEmSNPpeALwGuDnJTS3tt4BXJ3ku3XSJm4FfAqiqW5NcBNwGbAfeWFUPt3JvAM4F1gCXtwd0nXIfTLKJboTZcW1fW5O8Dfh0y/fWqtq6IrWUJEmSpCGy00zSREryBOBqYE+6tu7iqjotyT7AR4AD6S4s/UxVPdDKnAqcBDwM/EpVXdHSD+eRC0uXAW9qd11LkiQNRFX9LfOvLXbZImVOB06fJ/0G4NB50r8JvHKBfZ0DnLPceCVJkiRpHDk9o6RJ9RDwoqp6DvBc4JgkRwIbgKuq6iDgqvaaJIfQ3U39bOAY4D1Jdmv7ei/dgvYHtccxA6yHJEmSJEmSJGkA7DSTNJGqs6293KM9CjgWOK+lnwe8vD0/Friwqh6qqjuBTcARbZ2Qvarqmja67PyeMpIkSZIkSZKkCWGnmaSJlWS3tubHfcCVVXUdMFVV9wC0n09r2dcCd/UU39LS1rbnc9MlSZIkSZIkSRPENc0kTay22P1zkzwF+FiSR63d0WO+NUJqkfRH7yA5mW4aR/bbbz9mZmZ2Kt5B2rZt28jGt7OxnXLY9pULZh5Ta3b+mIN6r0f59wqjH58kSZIkSZJWNzvNJE28qvpqkhm6tcjuTbJ/Vd3Tpl68r2XbAhzQU2wdcHdLXzdP+nzH2QhsBDj44INrenq6n9Xoq5mZGUY1vp2N7cQNn1i5YOZxymHbOfPmnfv3ufn46ZUJZo5R/r3C6McnSZIkSZKk1W3kp2dM8oQk1yf5bJJbk/z2sGOSNPqS7NdGmJFkDfDjwOeBS4ETWrYTgEva80uB45LsmeQZwEHA9W0KxweTHJkkwGt7ykiSJEmSJEmSJsQ4jDR7CHhRVW1Lsgfwt0kur6prhx2YpJG2P3Bekt3obhC4qKo+nuQa4KIkJwFfAl4JUFW3JrkIuA3YDryxTe8I8AbgXGANcHl7SJIkSZIkSZImyMh3mlVVAdvayz3aY971hCRpVlV9DnjePOn3A0ctUOZ04PR50m8AFlsPTZIkSZIkSZI05kZ+ekaAJLsluYlu7aErq+q6IYckSZIkSZIkSVpFXEpImnwjP9IMoE2R9ty2PtHHkhxaVbfMbk9yMnAywNTUFDMzM8va79QaOOWw7X2Lc7nH7Ydt27YN9Hj9Ns7xj3PsMP7xS5IkSZIkSUPiUkLShBuLTrNZVfXVJDPAMcAtPekbgY0A69evr+np6WXt76wLLuHMm/v3Fmw+fnnH7YeZmRmWW89RNM7xj3PsMP7xS5IkSZIkScPgUkLS5Bv56RmT7NdGmJFkDfDjwOeHGpQkSVpVkhyQ5K+S3N6m4HhTS98nyZVJ7mg/9+4pc2qSTUm+kOTonvTDk9zctr07SVr6nkk+0tKvS3JgT5kT2jHuSHLCAKsuSZIkSerhUkLSZBuHkWb7A+cl2Y2uk++iqvr4kGOSJEmry3bglKr6TJInATcmuRI4Ebiqqs5IsgHYALw5ySHAccCzgacDf5nkWW3K6ffSTSt9LXAZ3Qj6y4GTgAeq6plJjgPeDrwqyT7AacB6ujsYb0xyaVU9MLDaS5IkSZKAx7aU0ELLpvRzCaFZCy3PMuylW4Z9fGMYjeOPSgzzGflOs6r6HPC8YcchSZJWr6q6B7inPX8wye3AWuBYYLplOw+YAd7c0i+sqoeAO5NsAo5IshnYq6quAUhyPvByuk6zY4G3tH1dDJzdRqEdTXf34tZW5kq6jrYPr1iFJUmSJEmL2pWlhBZaNuXEDZ/oe3wLLSU07KVbhn18YxiN449KDPMZ+U4zSZKkUdKmTXwecB0w1TrUqKp7kjytZVtLN5Js1paW9q32fG76bJm72r62J/kasG9v+jxl5sa24B2NC5la09+7Godxl9io3p22syahHpNQB5icekiSJKm/kuwHfKt1mM0uJfT2IYclqY/sNJMkSVqmJE8E/hT41ar6eluObN6s86TVIum7WmbHxEXuaFzIWRdcwpk39++UcKG7GVfSqN6dtrMmoR6TUAeYnHpIkiSp71xKSJpwdppJkiQtQ5I96DrMLqiqj7bke5Ps30aZ7U+3EDR0o8EO6Cm+Dri7pa+bJ723zJYkuwNPBra29Ok5ZWb6VC1JkiRJ0jK5lJA0+R437AAkSZJGXVtb7APA7VX1jp5NlwIntOcnAJf0pB+XZM8kzwAOAq5vUzk+mOTIts/Xzikzu69XAJ+qqgKuAF6cZO8kewMvbmmSVpEkByT5qyS3J7k1yZta+j5JrkxyR/u5d0+ZU5NsSvKFJEf3pB+e5Oa27d2tPaK1WR9p6de16Whny5zQjnFHkhOQJEmSpAlkp5kkSdLSXgC8BnhRkpva46XAGcBPJLkD+In2mqq6FbgIuA34JPDGqnq47esNwPuBTcA/AJe39A8A+ybZBPwasKHtayvwNuDT7fHWliZpddkOnFJVPwQcCbwxySF0bcVVVXUQcFV7Tdt2HPBsusXp39OmEQJ4L936hwe1xzEt/STggap6JvBO2vocSfYBTgOeDxwBnNbbOSdJkiRJk8LpGSVJkpZQVX/L/GuLARy1QJnTgdPnSb8BOHSe9G8Cr1xgX+cA5yw3XkmTp41Uvac9fzDJ7cBa4FgemcL1PLrpW9/c0i+sqoeAO1uH/BFJNgN7VdU1AEnOB15O14F/LPCWtq+LgbPbKLSjgStnO+yTXEnX0fbhFauwJEmSJA2BI80kSZIkaYy0aROfB1wHTLUOtdmOtae1bGuBu3qKbWlpa9vzuek7lKmq7cDXgH0X2ZckSZIkTRRHmkmSJEnSmEjyROBPgV+tqq+35cjmzTpPWi2Svqtl5sZ3Mt3Uj0xNTTEzM7NQfN8xtQZOOWz7kvmWaznH7Kdt27YN/Jj9Nu51GPf4YTLqIEmSNAnsNJMkSZKkMZBkD7oOswuq6qMt+d4k+1fVPUn2B+5r6VuAA3qKrwPubunr5knvLbMlye7Ak4GtLX16TpmZ+WKsqo3ARoD169fX9PT0fNl2cNYFl3Dmzf37arr5+KWP2U8zMzMsp56jbNzrMO7xw2TUQZIkaRI4PaMkSZIkjbi2ttgHgNur6h09my4FTmjPTwAu6Uk/LsmeSZ4BHARc36ZwfDDJkW2fr51TZnZfrwA+VVUFXAG8OMneSfYGXtzSJEmSJGmi2GkmSZIkSaPvBcBrgBcluak9XgqcAfxEkjuAn2ivqapbgYuA24BPAm+sqofbvt4AvB/YBPwDcHlL/wCwb5JNwK8BG9q+tgJvAz7dHm9taZIEQJIDkvxVktuT3JrkTS19nyRXJrmj/dy7p8ypSTYl+UKSo3vSD09yc9v27tbBT7sJ4CMt/bq2vqMkSVJfOT2jJEmSJI24qvpb5l9bDOCoBcqcDpw+T/oNwKHzpH8TeOUC+zoHOGe58UpadbYDp1TVZ5I8CbgxyZXAicBVVXVGkg10nfFvTnIIcBzwbODpwF8meVbr3H8v3dqI1wKXAcfQde6fBDxQVc9MchzwduBVA62lJEmaeI40kyRJkiRJ0i6rqnuq6jPt+YPA7cBa4FjgvJbtPODl7fmxwIVV9VBV3Uk38vWItjbjXlV1TZse9vw5ZWb3dTFw1OwoNEmSpH6x00ySJEmSJEl90aZNfB5wHTDV1lKk/Xxay7YWuKun2JaWtrY9n5u+Q5mq2g58Ddh3RSohSZJWLadnlCRJkiRJ0mOW5InAnwK/WlVfX2Qg2HwbapH0xcrMjeFkuukdmZqaYmZmZomoYWoNnHLY9iXz7YzlHHdXbNu2bcX23U/jEicYqyRpR3aaSZIkSZIk6TFJsgddh9kFVfXRlnxvkv2r6p429eJ9LX0LcEBP8XXA3S193TzpvWW2JNkdeDKwdW4cVbUR2Aiwfv36mp6eXjL2sy64hDNv7u8lss3HL33cXTEzM8Ny6jRs4xInGKskaUdOzyhJkiRJkqRd1tYW+wBwe1W9o2fTpcAJ7fkJwCU96ccl2TPJM4CDgOvbFI4PJjmy7fO1c8rM7usVwKfaumeSJEl940gzSZIkSZIkPRYvAF4D3Jzkppb2W8AZwEVJTgK+BLwSoKpuTXIRcBuwHXhjVT3cyr0BOBdYA1zeHtB1yn0wySa6EWbHrXCdJEnSKmSnmSRJkiRJknZZVf0t8685BnDUAmVOB06fJ/0G4NB50r9J63STJElaKU7PKEmSJEmSJEmSpFXPTjNJkiRJkiRJkiStenaaSZIkSZIkSZIkadWz00ySJEmSJEmSJEmrnp1mkiRJkiRJkiRJWvV2H3YAkqTV5cANn1h0+ymHbefEJfJIw5DkHOBlwH1VdWhLewvwi8A/t2y/VVWXtW2nAicBDwO/UlVXtPTDgXOBNcBlwJuqqpLsCZwPHA7cD7yqqja3MicA/60d43eq6rwVrawkSZIkSdIq5EgzSRMpyQFJ/irJ7UluTfKmlr5PkiuT3NF+7t1T5tQkm5J8IcnRPemHJ7m5bXt3kgyjTpKG7lzgmHnS31lVz22P2Q6zQ4DjgGe3Mu9JslvL/17gZOCg9pjd50nAA1X1TOCdwNvbvvYBTgOeDxwBnNbbdkmSJEmSJKk/7DSTNKm2A6dU1Q8BRwJvbBexNwBXVdVBwFXt9a5e4Ja0ilTV1cDWZWY/Friwqh6qqjuBTcARSfYH9qqqa6qq6EaWvbynzOwIsouBo1on/dHAlVW1taoeAK7EdkiSJEmSJKnv7DSTNJGq6p6q+kx7/iBwO7CWHS9Kn8eOF6t39gK3JAH8cpLPJTmnZwTYWuCunjxbWtra9nxu+g5lqmo78DVg30X2JUmSJEmSpD5yTTNJEy/JgcDzgOuAqaq6B7qOtSRPa9nWAtf2FJu9KP0tFr7ALUnvBd4GVPt5JvDzwHzTuNYi6eximR0kOZluZCxTU1PMzMwsEnpnak23lmC/LOeY/bZt27ahHLffJqEek1AHGN16uLaiJEmSJK2ske80S3IA3Re37wG+DWysqt8fblSSxkWSJwJ/CvxqVX19keXI+nqxer/99hvJi22zhnkxcKmL8/2+gN9vuxLfoN7rUb3IO2vU49sVVXXv7PMkfwR8vL3cAhzQk3UdcHdLXzdPem+ZLUl2B55MNx3kFmB6TpmZBeLZCGwEWL9+fU1PT8+XbQdnXXAJZ97cv1PCzccvfcx+m5mZYTl1HXWTUI9JqAOMdD3OBc6m+37U651V9Xu9CXOmnn468JdJnlVVD/PI1NPX0nWaHQNcTs/aikmOo1tb8VU9ayuupzsPujHJpW3KWEmSJEmaGCPfacYj6xJ9JsmT6L6gXVlVtw07MEmjLckedB1mF1TVR1vyvUn2b6PM9gfua+m7coF7B70Xqw8++OBlXawelmFeDDxxwycW3X7KYdv7egG/33YlvkF1IozwRV5g9OPbFbPtSXv508At7fmlwIeSvIPuYvVBwPVV9XCSB5McSTf69bXAWT1lTgCuAV4BfKqN/LgC+N2eqR9fDJy60nWTNHqq6uo2gn45vjP1NHBnktmppzfTpp4GSDI79fTlrcxbWvmLgbPnrq3YysyurfjhPlRLkiRJkkbGyK9ptsi6RJK0oHaB5wPA7VX1jp5NsxelaT8v6Uk/LsmeSZ7BIxe47wEeTHJk2+dre8pIWkWSfJiuQ+vgJFuSnAT8zyQ3J/kc8ELgvwBU1a3ARcBtwCeBN7bRHQBvAN5Pt3biP9BdqIauzdq3Xdj+NWBD29dWuqkfP90eb529cC1JjWsrSpIkDUCSA5L8VZLbk9ya5E3DjklSf43urfzzmLMukSQt5gXAa4Cbk9zU0n4LOAO4qF3s/hLwSugucCeZvcC9nUdf4D6Xbt2Py3nkArekVaSqXj1P8gcWyX86cPo86TcAh86T/k1amzTPtnOAc5YdrKTVxLUV5xj0VMCTMP3wuNdh3OOHyaiDJK0SzoomTbix6TSbuy7RnG07/cUMxvvL2bifUI9z/OMcO4x//MtVVX/L/Bd4AI5aoMxOXeCWJEkaNtdWfLRBr604CdMPj3sdxj1+mIw6SNJq0GYkuqc9fzDJ7KxodppJE2IsOs0WWJfoO3blixmM95ezcT+hHuf4xzl2GP/4NVgHLrH+mCRJGi7XVpQkSRoOZ0WTJtPId5otsi6RJEmSJK0abW3FaeCpSbYApwHTSZ5LN13iZuCXYJennv4A8MG2tuJW4Li2r61JZtdWBNdWlCRJq9yuzoq20AxQ/ZwNbdZCM00NexaqYR/fGEbj+KMSw3xGvtOMBdYlqqrLhheSJEmSJA2WaytKkiQN32OZFW2hGaBOXIGZfhaaFW3Ys1AN+/jGMBrHH5UY5jPynWZLrEskSZIkSZIkSdKKc1Y0afI9btgBSJIkSZIkSZI0BmZnRXtRkpva46XDDkpS/4z8SDNJkiRJkiRJkobNWdGkyedIM0mSJEmSJEmSJK16dppJkiRJkiRJkiRp1bPTTJIkSZIkSZIkSauenWaSJEmSJEmSJEla9ew0kyRJkiRJkiRJ0qpnp5kkSZIkSZIkSZJWPTvNJEmSJEmSJEmStOrZaSZJkiRJkiRJkqRVz04zSZIkSZIkSZIkrXp2mkmSJEmSJEmSJGnVs9NMkiRJkiRJkiRJq56dZpIkScuQ5Jwk9yW5pSdtnyRXJrmj/dy7Z9upSTYl+UKSo3vSD09yc9v27iRp6Xsm+UhLvy7JgT1lTmjHuCPJCQOqsiRJkiRJ0qqy+7ADkCT1z4EbPrGsfKcctp0Tl5lX0necC5wNnN+TtgG4qqrOSLKhvX5zkkOA44BnA08H/jLJs6rqYeC9wMnAtcBlwDHA5cBJwANV9cwkxwFvB16VZB/gNGA9UMCNSS6tqgdWvMaSJEmSJEmriCPNJEmSlqGqrga2zkk+FjivPT8PeHlP+oVV9VBV3QlsAo5Isj+wV1VdU1VF1wH38nn2dTFwVBuFdjRwZVVtbR1lV9J1tEmSJEmSJKmP7DSTJEnadVNVdQ9A+/m0lr4WuKsn35aWtrY9n5u+Q5mq2g58Ddh3kX1JWmWcJlaSJEmSVpbTM0qSJPVf5kmrRdJ3tcyOB01Oppv6kampKWZmZpYMdGpNN2VrvyznmP22bdu2oRy33yahHpNQBxjpepyL08RKkiRJ0oqx00ySJGnX3Ztk/6q6p029eF9L3wIc0JNvHXB3S183T3pvmS1JdgeeTDcd5BZgek6ZmfmCqaqNwEaA9evX1/T09HzZdnDWBZdw5s39OyXcfPzSx+y3mZkZllPXUTcJ9ZiEOsDo1qOqru4d/dUcyyNtxHl07cOb6ZkmFrgzyew0sZtp08QCJJmdJvbyVuYtbV8XA2fPnSa2lZmdJvbD/a6jJEmSJA2T0zNKkiTtukuB2WnKTgAu6Uk/rk119gzgIOD6NoXjg0mObBeiXzunzOy+XgF8qq17dgXw4iR7t2nXXtzSJAmcJlaSJEmS+saRZpIkScuQ5MN0ozmemmQL3VRlZwAXJTkJ+BLwSoCqujXJRcBtwHbgjW1KNIA30E2xtoZuZMflLf0DwAfbaJCtdNOqUVVbk7wN+HTL99bZ0R6StAiniR2QEZ7Oc9nGvQ7jHj+Mfx2SnAO8DLivqg5tafsAHwEOBDYDPzM7rWuSU+mmhH0Y+JWquqKlH84j50mXAW+qqkqyJ93UtIcD9wOvqqrNA6qeJElaRew0kyRJWoaqevUCm45aIP/pwOnzpN8AHDpP+jdpnW7zbDsHOGfZwUpaTZwmdo5BTxM7qtN57oxxr8O4xw8TUYdzGcKaiwOpmSRJWlWcnlGSJEmSxpfTxEoauqq6mq6TvdexdGst0n6+vCf9wqp6qKruBGbXXNyftuZia3vOn1Nmdl8XA0e1NkySJKmvHGkmSZIkSWPAaWIljZkd1lxM0rvm4rU9+WbXSfwWy1xzMcnsmotfmXvQUZgmFlZuqthxmcpzXOIEY5Uk7chOM0mSJEkaA04TK2lC9HPNxUcnjsA0sbByU8WOy1Se4xInGKskaUdOzyhJkiRJkqR+u7dNuUgf11xkzpqLkiRJfWWnmSRJkiRJkvptEGsuSpIk9ZWdZpImUpJzktyX5JaetH2SXJnkjvZz755tpybZlOQLSY7uST88yc1t27tdbFqSJEmSdtTWXLwGODjJlrbO4hnATyS5A/iJ9pqquhWYXXPxkzx6zcX3A5uAf2DHNRf3bWsu/hqwYSAVkyRJq45rmkmaVOcCZwPn96RtAK6qqjOSbGiv35zkELqF7p8NPB34yyTPal/c3ku3iPS1wGXAMTzyxU2SJEmSVr1hrrkoSZLUTyM/0my+0SKStJSquppHz3F/LHBee34e8PKe9Aur6qGqupPursYj2rz7e1XVNW3qj/N7ykiSJEmSJEmSJsjId5rRjRY5ZthBSJoIU22efNrPp7X0tcBdPfm2tLS17fncdEmSJEmSJK0yDvCQJt/IT89YVVcnOXDYcUiaaPOtU1aLpM+/k+Rkuqkc2W+//ZiZmelLcDvjlMO2Lyvf1Jrl5x20UY4Ndi2+QX0Wtm3bNpTP3XKNenySJEmSJC3hXB69HIikCTLynWaS1Ef3Jtm/qu5pUy/e19K3AAf05FsH3N3S182TPq+q2ghsBDj44INrenq6j6Evz4kbPrGsfKcctp0zbx7NfwGjHBvsWnybj59emWDmmJmZYRifu+Ua9fgkSZIkSVqMAzykyTe6VyV3Qu/ojqmpqWXfxd7v0QyDvHt+3O/WH+f4xzl2GP/4H6NLgROAM9rPS3rSP5TkHcDTgYOA66vq4SQPJjkSuA54LXDW4MOWJEmSJEmSJK20ieg06x3dsX79+mWP7jjrgkv6OpphUCMJYPzv1h/n+Mc5dhj/+JcryYeBaeCpSbYAp9F1ll2U5CTgS8ArAarq1iQXAbcB24E3VtXDbVdvoBt6vwa4vD0kSZIkSZKkR1lsgMdCN7OvxDIVC900P+wb6od9fGMYjeOPSgzzmYhOM0maq6pevcCmoxbIfzpw+jzpNwCH9jE0SZIkSZIkTajFBngsdDP7cpfb2BkLDfAY9g31wz6+MYzG8Uclhvk8btgBLKWNFrkGODjJljZCRJIkSZIkSZIkSeqbkR9ptshoEUmSJEmSJEmSBmK+5UCq6gPDjUpSP418p5kkSZIkSZIkScPmAA9p8o389IySJEmSJEmSJEnSSrPTTJIk6TFKsjnJzUluSnJDS9snyZVJ7mg/9+7Jf2qSTUm+kOTonvTD2342JXl3krT0PZN8pKVfl+TAgVdSkiRJkiRpwtlpJkmS1B8vrKrnVtX69noDcFVVHQRc1V6T5BDgOODZwDHAe5Ls1sq8FzgZOKg9jmnpJwEPVNUzgXcCbx9AfSRJkiRJklYVO80kSZJWxrHAee35ecDLe9IvrKqHqupOYBNwRJL9gb2q6pqqKuD8OWVm93UxcNTsKDRJAke8SpIkSVI/2GkmSZL02BXwF0luTHJyS5uqqnsA2s+ntfS1wF09Zbe0tLXt+dz0HcpU1Xbga8C+K1APSePNEa+SJEmS9BjsPuwAJEmSJsALquruJE8Drkzy+UXyzjdCrBZJX6zMjjvuOuxOBpiammJmZmbRoAGm1sAph21fMt9yLeeY/bZt27ahHLffJqEek1AHmJx60I1SnW7PzwNmgDfTM+IVuDPJ7IjXzbQRrwBJZke8Xt7KvKXt62Lg7CRpI2MlSZIkaSLYaSZJkvQYVdXd7ed9ST4GHAHcm2T/qrqnTb14X8u+BTigp/g64O6Wvm6e9N4yW5LsDjwZ2DpPHBuBjQDr16+v6enpJWM/64JLOPPm/p0Sbj5+6WP228zMDMup66ibhHpMQh1gbOsxO+K1gD9s7cEOI15bxz50o1ev7Sk7O7L1WyxzxGuS2RGvX+kNYjV23k9CJ+u412Hc44fJqIMkSdIksNNMkiTpMUjy3cDjqurB9vzFwFuBS4ETgDPaz0takUuBDyV5B/B0uunPrq+qh5M8mORI4DrgtcBZPWVOAK4BXgF8ytEdkuYYiRGvq7Hzfkw7WXcw7nUY9/hhMuogSZI0Cew0kyRJemymgI8lge7c6kNV9ckknwYuSnIS8CXglQBVdWuSi4DbgO3AG6vq4bavNwDnAmvopkO7vKV/APhgm0JtK91aRJL0HaMy4lWSJEmSxpmdZpIkSY9BVX0ReM486fcDRy1Q5nTg9HnSbwAOnSf9m7RON0mayxGvkiRJktQfdppJkiRJ0nhzxKskSZIk9YGdZpIkSZI0xhzxKkmSJEn98bhhByBJkiRJkiRJkiQNm51mkiRJkiRJkiRJWvXsNJMkSZIkSZIkSdKqZ6eZJEmSJEmSJEmSVj07zSRJkiRJkiRJkrTq2WkmSZIkSZIkSZKkVc9OM0mSJEmSJEmSJK16dppJkiRJkiRJkiRp1bPTTJIkSZIkSZIkSauenWaSJEmSJEmSJEla9ew0kyRJkiRJkiRJ0qq3+7ADkKTV7MANnxh2CBqAfv+eN5/xk33dnyRJkiRJkiRHmkmSJEmSJEmSJEl2mkmSJEmSJEmSJEl2mkmSJEmSJEmSJGnVc00zSZLGzEJrpJ1y2HZO3IX101wjTZIkSZKkyeK1A2nXjMVIsyTHJPlCkk1JNgw7Hkmrj+2QpGGzHZI0bLZDkobNdkjSKLAtkibbyHeaJdkN+APgJcAhwKuTHDLcqCStJrZDkobNdkjSsNkOSRo22yFJo8C2SJp84zA94xHApqr6IkCSC4FjgduGGpWk1cR2SBNtoSkbdpVTNqwI2yFJw2Y7JGnYbIckjQLbImnCjUOn2Vrgrp7XW4DnDykWSauT7ZCkYbMdkjRsY9MO9ftmEPCGEGlEjE07JGmi2RZJE24cOs0yT1rtkCE5GTi5vdyW5AvL3PdTga88hth2kLf3a0/L0tfYh2Cc4x/n2GFl4/++FdrvsC3ZDsGj2qKHktyyolE9Br8ywp/jUY4NRju+UYltkf+Hg4jPdmjnz4nG+Xxo1kh89vtgEuoxCXWAx1YP26Eht0MrYYm2beTjX4Zxr8O4xw/9rYPt0Ai0Qyt4TjQun/dxiROMdSUcPOwAVtBjvVY99N/hrl476GO7NvT3wBhG4vgrHcMunw+NQ6fZFuCAntfrgLt7M1TVRmDjzu44yQ1Vtf6xhTcc4xw7jHf84xw7jH/8Q7JkOwQ7tkWj/j6PcnyjHBuMdnyjHBuMfnwjbqfboeWahN/LJNQBJqMek1AHmJx69Jnt0ALGPX4Y/zqMe/wwGXUYANshxifWcYkTjHUlJLlh2DGsoMd0rXoUfofDjmHYxzeG0Tj+qMQwn8cNO4Bl+DRwUJJnJHk8cBxw6ZBjkrS62A5JGjbbIUnDZjskadhshySNAtsiacKN/Eizqtqe5JeBK4DdgHOq6tYhhyVpFbEdkjRstkOShs12SNKw2Q5JGgW2RdLkG/lOM4Cqugy4bAV2vdNTOo6QcY4dxjv+cY4dxj/+odiFdmjU3+dRjm+UY4PRjm+UY4PRj2+keT60qEmoA0xGPSahDjA59egr26EFjXv8MP51GPf4YTLqsOJsh4DxiXVc4gRjXQnjEucueYxt0Si8N8OOYdjHB2MYhePDaMTwKKl61JqpkiRJkiRJkiRJ0qoyDmuaSZIkSZIkSZIkSStqVXaaJTkmyReSbEqyYcDHPifJfUlu6UnbJ8mVSe5oP/fu2XZqi/MLSY7uST88yc1t27uTpKXvmeQjLf26JAf2lDmhHeOOJCfsQuwHJPmrJLcnuTXJm8Ys/ickuT7JZ1v8vz1O8bd97Jbk/yT5+LjFvpoMs43piWFz+z3flOSGlrbTn5c+xrOibd8KxPaWJF9u799NSV46pNhWvN1dofhG4v3T4kakrRrrc4s5dRn7/9FJnpLk4iSfb7+THxu3eiT5L+2zdEuSD6c7/xurOqwmo9AOzYlnaN/V+hT/UNvUPsQ/1O9rfazHUP4faHmWanfSeXfb/rkkPzKicR7f4vtckr9L8pxhxNliWVZbnuRHkzyc5BWDjG9ODEvGmmQ63XeYW5P89aBjbDEs9ft/cpI/72kvXzekOB/1f3PO9pH4exoVy/1bWeEYHnWtaADH3KnzqwHGsOB1ixU4/k6fow0whkG+Dzt9rjc0VbWqHnQLNP4D8P3A44HPAocM8Pj/HvgR4JaetP8JbGjPNwBvb88PafHtCTyjxb1b23Y98GNAgMuBl7T0/wS8rz0/DvhIe74P8MX2c+/2fO+djH1/4Efa8ycBf99iHJf4AzyxPd8DuA44clzib/v5NeBDwMfH6bOzmh4MuY3piWMz8NQ5aTv9eeljPCva9q1AbG8Bfn2evIOObcXb3RWKbyTePx+L/u5Gpa0a63OLOXUZ+//RwHnAL7TnjweeMk71ANYCdwJr2uuLgBPHqQ6r6cGItENzYhrKd7U+xj+0NrVP8Q/t+1qffw8D/3/gY9m/myXbHeCl7T1P+/xdN6Jx/hva/xngJcOIc7mx9uT7FN0aUK8Y1Vjpzn1uA763vX7aiMb5Wz1tyX7AVuDxQ4j1Uf8352wf+t/TqDyW+7cygDg2M+da0TA+Jwv9bxxwDG9hnusWK3T8nTpHG3AMg3wfdupcb5iP1TjS7AhgU1V9sar+FbgQOHZQB6+qq+n+mfU6lu4iBe3ny3vSL6yqh6rqTmATcESS/YG9quqa6j5N588pM7uvi4Gj2l1pRwNXVtXWqnoAuBI4Zidjv6eqPtOePwjcTndxYlzir6ra1l7u0R41LvEnWQf8JPD+nuSxiH2VGWobs4Sd+rz088ADaPv6HdtCBh3bINrdlYhvIQONT4saibZq3M8tZk3C/+gke9F9mfwAQFX9a1V9ddzqAewOrEmyO/BdwN1jWIfVYiTaoV5D/K7Wr/iH2ab2I/5hfl/riyH+P9DyLKfdORY4v30erwWe0n4nIxVnVf1d+38DcC2wbsAxzlpuW/6fgT8F7htkcHMsJ9afBT5aVV8CqKphxLucOAt4Uvv7fyLd/67tgw1zWd+lR+HvaVSM3HnPoOzk+dUgYxiYXThHG2QMA7ML53pDsxo7zdYCd/W83sKAPyDzmKqqe6D7AANPa+kLxbq2PZ+bvkOZqtoOfA3Yd5F97ZJ000A8j65HeGziTzdVxk10J2pXVtU4xf8u4DeBb/ekjUvsq8movF8F/EWSG5Oc3NJ29vOy0vr5+V0Jv9ymkDinZ2j40GJbwXZ3JeKDEXv/9Cij0lZ9x7ieWzTvYvz/R38/8M/AH6ebVuz9Sb57nOpRVV8Gfg/4EnAP8LWq+otxqsMqMy7v2SA+P303hDa1X3EP6/tav7yL4fw/0PIsp90ZhbZpZ2M4iW40zzAsGWuStcBPA+8bYFzzWc77+ixg7yQz7bv0awcW3SOWE+fZwA/R3Rx0M/Cmqvo2o2cU/p5Gxai8F/NdKxqGhf43Dtp81y1W1DLP0QYZAwzwfdjJc72hWY2dZvPdhVUDj2J5Fop1sTrsSpmdCyp5It0dQr9aVV9fLOsuxLKi8VfVw1X1XLq7sI5Icugi2Ucm/iQvA+6rqhuXW2QX4ljxz84qMSrv1wuq6kfopup4Y5J/v0jeUYl51ih8Ft8L/ADwXLoLr2e29KHEtsLt7mM2T3wj9f5pXiP1no/zucUE/Y/enW7KkvdW1fOAb9BNjbGQkatH+4J3LN00Z08HvjvJzy1WZBfisR3rn3F/z/r5+emrIbWpfTHE72uP2ZD/H2h5lvP+jcJ7vOwYkryQrtPszSsa0cKWE+u7gDdX1cMrH86ilhPr7sDhdCNGjwb+3yTPWunA5lhOnEcDN9Gd7zwXOLvNGjBqRuHvaVSMynuxM9eKJt1C1y1WzE6cow0yhoG+Dzt5rjc0q7HTbAtwQM/rdXR3ZgzTvbPDk9vP2eHfC8W6hR2H3vfW4Ttl2rQ0T6YbftqXeifZg+4P64Kq+ui4xT+ruumGZuimzRmH+F8A/FSSzXRDuF+U5E/GJPbVZiTer6q6u/28D/gY3VQAO/t5WWn9/Pz2VVXd2/6Rfxv4Ix6ZrnLgsQ2g3e17fKP0/mlBI9FWwUScW0zK/+gtwJZ2px900379yJjV48eBO6vqn6vqW8BH6dZ8Gac6rCbj8p4N4vPTN0NsU/tqCN/X+mGY/w+0PMtpd0ahbVpWDEl+mG4q0GOr6v4BxTbXcmJdD1zY/jZeAbwnycsHEt2Olvv7/2RVfaOqvgJcDTxnQPH1xrBUnK+jm0ayqmoT3ZquPzig+HbGKPw9jYqReC8WuFY0DAv9bxyYRa5brIidPEcbWAyDfh9mLfNcb2hWY6fZp4GDkjwjyePpFs+9dMgxXQqc0J6fAFzSk35ckj2TPAM4CLi+DVN8MMmRSQK8dk6Z2X29AvhUVRVwBfDiJHu3u3Bf3NKWrR3rA8DtVfWOMYx/vyRPac/X0F1Y+fw4xF9Vp1bVuqo6kO4z+6mq+rlxiH0VGnobk+S7kzxp9jnd7+wWdvLzMoBQ+/n57avZf9bNT9O9fwOPbUDtbt/jG5X3T4saelsF439uAZPzP7qq/gm4K8nBLeko4LYxq8eXgCOTfFc79lF0c/WPUx1Wk5Foh5ZhEJ+fvhhym9qP+If5fe0xG/L/Ay3PctqdS4HXpnMk3VS/94xanEm+l+7mkNdU1d8POL5eS8ZaVc+oqgPb38bFwH+qqj8beKTL+/1fAvy7JLsn+S7g+XTnEqMW55foznNIMgUcDHxxoFEuzyj8PY2KoZ/3ZOFrRcOw0P/GgVnkusVKHGtnz9EGFsOA34edPdcbnqpadQ/gpcDfA/8A/NcBH/vDdEMdv0V3l8FJdHOQXwXc0X7u05P/v7Y4vwC8pCd9Pd2H+B/o5jJOS38C8L/oFhG+Hvj+njI/39I3Aa/bhdj/Ld3Q4c/RDQO/qb2X4xL/DwP/p8V/C/DfW/pYxN+zn2ng4+MY+2p5MMQ2ph3/+4HPtsetszHsyueljzGtaNu3ArF9kG5u+M/R/fPef0ixrXi7u0LxjcT752PJ399Q26olPkNj+f+NMf8fTTclxw3t9/FnwN7jVg/gt+m+eN3S2qI9x60Oq+nBCLRDc+IZ2ne1PsU/1Da1D/EP9ftan38X0wz4/4GPZf9uHtXuAK8HXt+eB/iDtv1mYP2Ixvl+4IGev/UbRvU9nZP3XOAVoxwr8Bt0Nw7dQjd92cjFSTct41+0z+gtwM8NKc75/m+O3N/TqDzm+70O+PjzXisa0udkwf+NA4xhwesWK3D8nT5HG2AMg3wfdvpcb1iP2RMvSZIkSZIkSZIkadVajdMzSpIkSZIkSZIkSTuw00ySJEmSJEmSJEmrnp1mkiRJkiRJkiRJWvXsNJMkSZIkSZIkSdKqZ6eZJEmSJEmSJIkk5yS5L8kty8z/M0luS3Jrkg+tdHyStNJSVcOOQZIkSZIkSZI0ZEn+PbANOL+qDl0i70HARcCLquqBJE+rqvsGEackrRRHmkmSJEmSJEmSqKqrga29aUl+IMknk9yY5G+S/GDb9IvAH1TVA62sHWaSxp6dZpIkSZIkSZKkhWwE/nNVHQ78OvCelv4s4FlJ/neSa5McM7QIJalPdh92AJIkSZIkSZKk0ZPkicC/Af5XktnkPdvP3YGDgGlgHfA3SQ6tqq8OOExJ6hs7zSRJkiRJkiRJ83kc8NWqeu4827YA11bVt4A7k3yBrhPt0wOMT5L6yukZJUmSJEmSJEmPUlVfp+sQeyVAOs9pm/8MeGFLfyrddI1fHEacktQvdppJkiRJkiRJkkjyYeAa4OAkW5KcBBwPnJTks8CtwLEt+xXA/UluA/4K+I2qun8YcUtSv6Sqhh2DJEmSJEmSJEmSNFSONJMkSZIkSZIkSdKqZ6eZJEmSJEmSJEmSVj07zSRJkiRJkiRJkrTq2WkmSZIkSZIkSZKkVc9OM0mSJEmSJEmSJK16dppJkiRJkiRJkiRp1bPTTJIkSZIkSZIkSauenWaSJEmSJEmSJEla9ew0kyRJkiRJkiRJ0qpnp5kkSZIkSZIkSZJWPTvNJEmSJEmSJEmStOrZaSZJkiRJkiRJkqRVz04zSZIkSZIkSZIkrXp2mkmSJEmSJEmSJGnVs9NMkiRJkiRJkiRJq56dZpIkSZIkSZIkSVr17DSTJEmSJEmSJEnSqmenmSRJkiRJkiRJklY9O80kSZIkSZIkSZK06tlpJkmSJEmSJEmSpFXPTjNJkiRJkiRJkiStenaaSZK0wpJUkmc+xn2sSfLnSb6W5H/1KzZJuy7J5iQ/vgvl/l2SL/QxjukkW/q1P0mjaVfbnJ08xrYk39/H/T3mcyBJO6ffbUWS721tw2792ueoSnJwkv+T5MEkvzLseCT1R5KZJL+wjHx9PQ/S+LLTTFpEkhOT/O2w45C0cnb2YvMQL06/ApgC9q2qVyY5N8nvDCEOSTtp7kXjqvqbqjq4Z/uKXwiXpOWoqidW1RcBPNeQJtfOfKepqi+1tuHhlY5rBPwmMFNVT6qqdz+WHXl+J42f3vMgrW52mmmXpTPyn6Ekuw87Bkm7blzamgH4PuDvq2r7sAORJEmSpHGyzGtD3wfcutKxSJJGmxchV6kkP9Iz5Px/JflIkt9JsneSjyf55yQPtOfresrNJDk9yf8G/gX4/iSvS3J729cXk/zSnGP9ZpJ7ktyd5Bd677ZOsmeS30vypST3JnlfkjU9ZY9NclOSryf5hyTHtPQFjzl7x1SSNyf5J+CPk+yW5LfaPh5McmOSA1r+SvL6JHe0Ov9Bu0j/Q8D7gB9rw3O/unK/EWkyjXpbk+S7gcuBp7e/821Jnt7yv6vt6+72fM9F8h+R5JokX20xnJ3k8Qu8Jy9Nclurx5eT/HrPtt/oqcPPz9YhyW8D/x14VTvmLwHHA7/ZXv95f39zknbGYm1Akqtbts+2v9dXpefu7iQfBL4X+PO2/Tczz93f6blbubVf57b28zbgR+fkfXqSP21t7J1xeiFpoix0ntK2zX4XOiXJfa1Nel1P2X3TTff89SSfbudlf9uzffbc42TmOdfInJGzmTMabe65zDxxL/jdT1J/7eJ3mhta+3Bvkne0/RzY/vZ3T/JjPWW2Jflmks0t3+OSbEh33eX+JBcl2adte0KSP2npX23tz1Tb9owkf92+H13ZzqP+pG1b6pxo0e9hLe43JrkDuKOlvSzddaavJvm7JD/c0j8FvBA4u9XtWUu1W4vs61Hnd/3/DUuTqf2Nn5ruuskDSf44yRPatl9MsinJ1iSXJnl6T7l/09qWr7Wf/2aB/T+ztTlfS/KVJB/p2TZ7HtTbPm5L8i9Jqiffz6e7PvVAkiuSfF9LT5J3pjsH+1qSzyU5tG3bYYrIzJndrB37P6W7Pv1gkrcl+YHWxn29tanzXmdS/9lptgq1P7CPAecC+wAfBn66bX4c8Md0d9d8L/B/gbPn7OI1wMnAk4B/BO4DXgbsBbwOeGeSH2nHOgb4NeDHgWcC/2HOvt4OPAt4btu+lu7CMEmOAM4HfgN4CvDvgc2t3ILHbL6n1e37Wqy/BrwaeGkr8/N0F+JnvYzugtNzgJ8Bjq6q24HXA9e04blPmfteSlrYOLQ1VfUN4CXA3e3v/IlVdTfwX4EjW/7nAEcA/22R/A8D/wV4KvBjwFHAf1rgrfkA8EtV9STgUOBTPXX4deAngINaXQCoqtOA3wU+0o75h8AFwP9sr//jAseSNBgLtgFV9e9bnue0v9eP9BasqtcAXwL+Y9v+P5dxvNOAH2iPo4ETZjekG5n758Bn6dq6o4BfTXL0rldP0oiZ9zylZ/v3AE+mawNOAv4gyd5t2x8A32h5TqCn/ehVVRvZyXONxc5lmgW/+0laETv7neb3gd+vqr3ozjEumrvDqpq9PvJEYG/gWrrveQC/Aryc7rvY04EH6Noc6NqaJwMHAPvSXWv5v23bh4Ab6c6j3sYC7dIClvM97OXA84FD2vfHc4BfanH8IXBpkj2r6kXA3wC/3Or49yx+zWqxfe3K+Z2kRxxP9z3nB+j+Bv9bkhcB/4Puuu3+dNeJLgRI10H/CeDddH+P7wA+kWTfefb9NuAv6NqwdcBZczNUVW/7+ES6a1uzx3o58FvA/wPsR9duzLaDL6a7fv0sumvZrwLu34l6HwMcTtd2/yawsb0XB9BdP3r1TuxLj8HEdpolOaf16t6yzPw/03qwb03yoZWOb8iOBHYH3l1V36qqjwLXA1TV/VX1p1X1L1X1IHA6j774fG5V3VpV21v5T1TVP1Tnr+kann/X8v4M8Mct/78Avz27kyQBfhH4L1W1tR3vd4HjWpaTgHOq6sqq+nZVfbmqPt/iXOyYAN8GTquqh6rq/wK/QHdy+IVW5rNV1dtonVFVX62qLwF/RXdCJD0mtkNj09bM53jgrVV1X1X9c9vfaxbKXFU3VtW1LdbNdF+Y5tZn1rfovrDtVVUPVNVn5tThlvZF9i2LxCcti+3QYOxkG9APPwOc3tq0u+i+HM76UWC/qnprVf1rdXPy/xGLt3nSirEdWhFLnad8q23/VlVdBmwDDk6yG/D/o/ue9C9VdRtwXh/jWvBcZhfPx6S+WMXt0E59p6FrO56Z5KlVta2qrl1i/++m64T/r+31LwH/taq2VNVDdG3AK9JNi/gtugvZz6yqh9u509eTfC/ducv/267fXE1388+yLPMc7H+0duf/0rVDf1hV17U4zgMeovvuuoNltFvL3peknXZ2Vd1VVVvprhe9mq5NO6eqPtPamFPpZgc7EPhJ4I6q+mBrDz4MfB6Y76afb9HdwP30qvpmVf3tPHm+I8mbgR+kG4ABXVv3P6rq9uqWz/hd4LlttNm36G78/kEgLc89O1Hvt1fV16vqVuAW4C+q6otV9TW6EcLP24l96TGY2E4zupENxywnY5KD6P7QXlBVzwZ+deXCGglPB75cVdWTdhdAku9K8odJ/jHJ14Grgae0L1g75J2V5CVJrk03NPardKO5ntpzrLsWKLsf8F3AjemGsn8V+GRLh64X/R/mq8ASxwT456r6Zs/rBffV/FPP838BnrhIXmm5zmV1t0Pj0tYsFPs/9rz+x5Y2r3RTd3w8yT+1+vwuO7ZJvf5/LfZ/TDclwI8tUId/fFRJaeedy+puhwZiJ9uAflisvfg+uumWvtrT5v0WMLWC8UiLORfboX5b6jzl/tpxDdTZ7zf70d3QtNA5Uz/iWqht2pXzMalfzmV1tkM79Z2G7sblZwGfTze12csWyphuuvhp4Ger6tst+fuAj/X8jd9ONxJsCvggcAVwYbqpIv9nkj1aPA+0jvbeOJdlmedgve3S9wGnzDlPOoD535el2q2d2ZeknTP3fOLpzGnTqmob3SiutXO39ZRbO8++fxMIcH27OeLn58kDdNehgDcBL28d79D97f9+z9/91ra/tVX1KbpZlP4AuDfJxiR7La/KANzb8/z/zvPa69UDMrGdZu3ulK29aenmAf1kuvWs/ibJD7ZNvwj8QVU90MreN+BwB+0eYG27a2bWAe3nKcDBwPOrG5I/O6VQb97eOVz3BP4U+D1gqropDC/ryX8P3VDXuccB+ArdH/yzq+op7fHk6oa9QtdA/sDc4JdxzB1iXGxfyzB3P9Ky2Q6NTVsz39/53XQnQrO+t6UtlP+9dHcxHdTq81tz6vJIpao+XVXHAk8D/oxHpj25Z07c3ztf+d5dLbFdsh0anGW3AQuY+/f8DbqLNAC0Gwp6Lywv1l7cBdzZ0949paqeVFUv3Yl4pL6xHVoRi52nLOafge0sfM4013znGv9CT/tEN83jrMXapqXOx6QVs4rboZ36TlNVd1TVq+m+p7wduDjd+mc7SPLv6KY3O7aNfph1F/CSOecgT6hu1qBvVdVvV9UhwL+hm3b/tXTtxt5zjtPbdix1TrScc7C5N3GePifG72qjUuZazjWrxfbl9zVp1809n7ibOW1aazf2Bb48d1tPuS/P3XFV/VNV/WJVPZ1u1Nh70rNea8/+D6Ybkf8z1c3uMesuuiU3ev/211TV37X9v7uqDgeeTXcjwm+0cju0Z+x4DqURM7GdZgvYCPzn9sH9deA9Lf1ZwLOS/O82imFZdyCNsWvo7vb55XQLuR5LN7c1dENI/y/w1XTzwZ62xL4eD+xJ+wLWeuBf3LP9IuB1SX4oyXfRM2d9uxvpj+jWJXoaQJK1eWTNjQ+0skelW1B2bTuRXeqY83k/8LYkB6Xzw5l/Xtu57gXWxYUW1T+rqR0al7bmXmDfJE/u2d+H6ebM3i/JU9v+/mSR/E8Cvg5sa+3UG+arRJLHJzk+yZOr6lutzMM9dTgxySGtDku9J/cC379EHmk+q6kdGpSl2oCl/l7nbv974AlJfrLdhf3f6NrAWRcBpybZO8k64D/3bLse+HqSNydZk2S3JIcm+dFdq5q0ImyHHpvFzlMWVFUPAx8F3pJu1P8P0l20Xsh8bddNwM+2tuUYdpwGbcFzmWWcj0mDthraoZ36TpPk55Ls1/5ev9qSH+7dYZIDgI8Ar61uza9e7wNOTzdFGe24x7bnL0xyWOv0+jrdFGYPV9U/AjcAv92+K/1bdpxObalzomV9D+vxR8Drkzy/XRv67rbvJ83NuIx2a6l9+X1N2nVvTLKuXS/6Lbp250N0132em+7G6t8FrqtuatbL6Nrun23Xn14FHAJ8fO6Ok7yyfYeCbu3F4tFt3V7AJXRL/cydvvF9dN/Fnt3yPjnJK9vzH21twh50nWTf7Nn3TcD/087Bnkk3ulcjatV0miV5It3dLP8ryU108xzv3zbvTrdQ8TTdHKnvT/KUwUc5GFX1r3SLFZ5EdyL0c3SNyEPAu4A1dHfUXEs39HyxfT1It9jrRXQNzc8Cl/Zsv5xunuu/AjbRXUSnHQvgzS392nRD6f+SbvQJVXU98DrgncDXgL8Gvm+pYy7gHS3/X9CdUH2g1XMpnwJuBf4pyVeWkV9a0Gprh8aorfk83RfKL6YbXv904Hfovrx9DrgZ+ExLWyj/r7eYHqT78vSRRarzGmBzi+P17X2ZrcO76NqdTe3nYj5AtzbaV5P82RJ5JWD1tUMDtFQb8BbgvPb3+jPzlP8fdBe1vprk19td2/+J7qafL9N94drSk/+36aYbuZPu3OaDsxvaRfH/SLc+65107ez7gd6OfmlobIf6YsHzlGX4Zbr24J/o2o4P88j50lzznWu8ia6N+Srd2iKz6cs5l1nwfEwapFXUDu3sd5pjgFuTbAN+Hziudlz2AuAoutERFyfZ1h63tm2/T/cd7S+SPEj3Pe/5bdv3ABfTXY+5ne76zmwH3s+2fFvpOtvPnz3YMs6JduZ7GFV1A91owrPpvlduAk5cpMhi3yOX2tcO53eLxSXpUT5E9z3ni+3xO1V1FfD/0s1CdA/djGLHAVTV/XQjWE+hm7LxN4GXVdV813J/FLiutXWXAm+qqjvn5PkRur/1d/S0ddvasT5GNxr3wtYu3AK8pJXbi64teoDu+9r9dDMmQXd9+1/pOtTPAy7YtbdGg5DaYamZyZJuIcCPV9WhrYf4C1W1/zz53gdcW1XnttdXARuq6tODjHeYklwHvK+q/niFj/NDdI3JnrXjPPvSRLId2pFtzc5JUnRTjWwadiwaX7ZDkobNdmh0JXk78D1VdcKwY5FWku3Q+EjyFuCZVfVzw45F0uAl2Qz8QlX95bBj0eq1akaaVdXXgTt7hksmyXPa5j8DXtjSn0o3HP+Lw4hzUJL8hyTf04asngD8MEuM9HgMx/rpdMPs96brif/zcb6ILe2q1dgO2dZIo2U1tkOSRovt0HAl+cF0U9UnyRF0MwJ8bNhxSYNkOyRJkhYzsZ1mST5MNz3XwUm2JDmJbvqIk5J8lm7KvWNb9iuA+5PcRje112+0YZ2T7GDgs3TTHp4CvKKq7lmhY/0S3TpE/0A3j+tSc0xLE8F2CLCtkYbKdkjSsNkOjZwn0a1r9g26aa/PpFuzQ5pYtkOSJGlnTPT0jJIkSZIkSZIkSdJyTOxIM0mSJEmSJEmSJGm5dh92AP321Kc+tQ488MBl5f3GN77Bd3/3d69sQI/BqMcHox/jqMcHkxXjjTfe+JWq2m8AIY285bZF4/D73xnWZ/RNWp3m1sd26BFPecpT6pnPfOawwxiZz5xxjF4coxDDSsRhO/SIpc6HRuUz0C+TVh+wTuPC86GFrdbvZb2s2/iZhHrZDj3CdujRVktdredwPZZ2aOI6zQ488EBuuOGGZeWdmZlhenp6ZQN6DEY9Phj9GEc9PpisGJP848pHMx6W2xaNw+9/Z1if0TdpdZpbH9uhR0xNTS37nGgljcpnzjhGL45RiGEl4rAdesRS50Oj8hnol0mrD1inceH50MJW6/eyXtZt/ExCvWyHHmE79Girpa7Wc7geSzvk9IySJEmSJEmSJEla9ew0kyRJkiRJkiRJ0qpnp5kkSZIkSZIkSZJWvYlb00yS5kpyDvAy4L6qOnSe7b8BHN9e7g78ELBfVW1Nshl4EHgY2F5V6wcTtSRJkiRJkiRpkBxpJmk1OBc4ZqGNVfX/VdVzq+q5wKnAX1fV1p4sL2zb7TCTJEmSJEmSpAllp5mkiVdVVwNbl8zYeTXw4RUMR5IkSZIkSZI0guw0k6QmyXfRjUj7057kAv4iyY1JTh5OZJIkSZIkSZKkleaaZpL0iP8I/O85UzO+oKruTvI04Mokn28j1x6ldaqdDDA1NcXMzMySB9y2bduy8o0L6zP6Jq1Ok1YfSZIkSZIkDY+dZpL0iOOYMzVjVd3dft6X5GPAEcC8nWZVtRHYCLB+/fqanp5e8oAzMzMsJ9+4sD6jb9LqNGn1kSRJkiRJ0vCs6k6zm7/8NU7c8Im+7W/zGT/Zt31JGqwkTwb+A/BzPWnfDTyuqh5sz18MvLWfx7UdkpTkHOBlwH1Vdegi+X4UuBZ4VVVdPKj4NHgH9vH/wiz/P0gatpVo23bFKYdtn/f823Zydev39zLwMyVp59gOSaNjVXeaSVodknwYmAaemmQLcBqwB0BVva9l+2ngL6rqGz1Fp4CPJYGuvfxQVX1yUHFLWjXOBc4Gzl8oQ5LdgLcDVwwoJkmSJEmSpFXHTjNJE6+qXr2MPOfSXbjuTfsi8JyViUqSOlV1dZIDl8j2n4E/BX505SOSJEmSJElanR437AAkSZK0sCRr6UbDvm+pvJIkSZIkSdp1jjSTJEkabe8C3lxVD7fpYheU5GTgZID99tuPmZmZFQ9uKdu2bTOOnYzjlMO29/24c485Cu/HKMQwSnFIkiRJkobPTjNJkqTRth64sHWYPRV4aZLtVfVnczNW1UZgI8DBBx9c09PTAwxzfjMzMxjHzsXR7wXAATYfv+MxR+H9GIUYRikOSZIkSdLw2WkmSZI0wqrqGbPPk5wLfHy+DjNJkiRJkiQ9Nq5pJkmSNERJPgxcAxycZEuSk5K8Psnrhx2bpNUhyTlJ7ktyywLbj0/yufb4uyTPGXSMkiRJkjQIjjSTJEkaoqp69U7kPXEFQ5G0ep0LnA2cv8D2O4H/UFUPJHkJ3TSwzx9QbJIkSZI0MEMdabbUHY0tz3SSm5LcmuSvBxmfJEmSJE26qroa2LrI9r+rqgfay2uBdQMJTJIkSZIGbNjTM54LHLPQxiRPAd4D/FRVPRt45WDCkiRJkiTN4yTg8mEHIUmSJEkrYajTM1bV1UkOXCTLzwIfraovtfz3DSQwSZIkSdIOkryQrtPs3y6S52TgZICpqSlmZmYW3N+2bdsW3T5uJq0+0N86nXLY9r7s57GaWjN/LOP8u5vEz54kSdKwjPqaZs8C9kgyAzwJ+P2qWmiefUmSJEnSCkjyw8D7gZdU1f0L5auqjXRrnrF+/fqanp5ecJ8zMzMstn3cTFp9oL91OnHDJ/qyn8fqlMO2c+bNj74Usvn46cEH0yeT+NlbTJLNwIPAw8D2qlo/3IgkSdIkGfVOs92Bw4GjgDXANUmuraq/7820M3cz9lroDrNd1e87u8bhbrFRj3HU4wNjlCRJ0mhL8r3AR4HXzP0uJklD8sKq+sqwg5AkSZNn1DvNtgBfqapvAN9IcjXwHGCHL2o7czdjr7MuuGTeO8x2Vb/vTBuHu8VGPcZRjw+MUZIkScOV5MPANPDUJFuA04A9AKrqfcB/B/YF3pMEHNkhSZIkaUKNeqfZJcDZSXYHHg88H3jncEOSJEmSpMlRVa9eYvsvAL8woHAkaSkF/EWSAv6w3UgtSZLUF0PtNFvqjsaquj3JJ4HPAd8G3l9VtwwrXkmSJEmSJA3VC6rq7iRPA65M8vmqunp2464s4dHv5Tug/0t47KpJXm5hUus2qfWSpHEx1E6zpe5obHn+P+D/G0A4kiRJkiRJGmFVdXf7eV+SjwFHAFf3bN/pJTz6vXwH9H8Jj101ycstTGrdJrVekjQuHjfsACRJkiRJkqSlJPnuJE+afQ68GHBGIkkAJNmc5OYkNyW5oaXtk+TKJHe0n3v35D81yaYkX0hydE/64W0/m5K8O21R1yR7JvlIS78uyYEDr6SkFWenmSRJkiRJksbBFPC3ST4LXA98oqo+OeSYJI2WF1bVc6tqfXu9Abiqqg4CrmqvSXIIcBzwbOAY4D1Jdmtl3ks3zetB7XFMSz8JeKCqngm8E3j7AOojacCGOj2jJEmSNO4O3PCJZec95bDtnLgT+SVJ0iOq6ovAc4Ydh6Sxciww3Z6fB8wAb27pF1bVQ8CdSTYBRyTZDOxVVdcAJDkfeDlweSvzlravi4Gzk6SqahAVkTQYdppJkiRJkqQdHLjhE3b0S5LGTQF/kaSAP2xrHE5V1T0AVXVPkqe1vGuBa3vKbmlp32rP56bPlrmr7Wt7kq8B+wJf6Q0iycl0I9WYmppiZmZmycCn1nQ32PXTco47DNu2bRvZ2PrJeo4vO80kSZIkSZIkSePuBVV1d+sYuzLJ5xfJm3nSapH0xcrsmNB11m0EWL9+fU1PTy8aNMBZF1zCmTf391L95uOXPu4wzMzMsJz3ZNxZz/HlmmaSJEmSJEmSpLFWVXe3n/cBHwOOAO5Nsj9A+3lfy74FOKCn+Drg7pa+bp70Hcok2R14MrB1JeoiaXgcaSZp4iU5B3gZcF9VHTrP9mngEuDOlvTRqnpr23YM8PvAbsD7q+qMQcQsSVo5O7MGmSRJkqTRl+S7gcdV1YPt+YuBtwKXAicAZ7Sfl7QilwIfSvIO4OnAQcD1VfVwkgeTHAlcB7wWOKunzAnANcArgE+5npk0eew0k7QanAucDZy/SJ6/qaqX9SYk2Q34A+An6O4m+nSSS6vqtpUKVJIkSZIkSTttCvhYEuiueX+oqj6Z5NPARUlOAr4EvBKgqm5NchFwG7AdeGNVPdz29Qa6a0lrgMvbA+ADwAeTbKIbYXbcIComabDsNJM08arq6iQH7kLRI4BNVfVFgCQXAsfSnVBJkiRJkiRpBLRrN8+ZJ/1+4KgFypwOnD5P+g3Ao2Yqqqpv0jrdJE0u1zSTpM6PJflsksuTPLulrQXu6smzpaVJkiRJkiRJkiaMI80kCT4DfF9VbUvyUuDP6Oayzjx5F5yrOsnJwMkAU1NTzMzMLHngqTVwymHbdyHk+S3nmCtp27ZtQ4+hnyatPjB5dZq0+kiSJEmSJGl47DSTtOpV1dd7nl+W5D1Jnko3suyAnqzrgLsX2c9GYCPA+vXra3p6esljn3XBJZx5c/+a4s3HL33MlTQzM8Ny6j0uJq0+MHl1moT6JDkHeBlwX1U9agqQJMcDb24vtwFvqKrPDjBESZIkSZKkVcHpGSWtekm+J22l2CRH0LWN9wOfBg5K8owkj6db4PXS4UUqaUKdCxyzyPY7gf9QVT8MvI3WOS9JkiRJkqT+cqSZpImX5MPANPDUJFuA04A9AKrqfcArgDck2Q78X+C4qipge5JfBq4AdgPOqapbh1AFSROsqq5OcuAi2/+u5+W1dKNeJUmSJEmS1Gd2mkmaeFX16iW2nw2cvcC2y4DLViIuSdoFJwGXDzsISZIkSZKkSWSnmSRJ0hhI8kK6TrN/u0iek4GTAfbbbz9mZmYGE9witm3bNnJxnHLY9qHFMbVmOMef+zsYhd/LKMQwSnFIkiRJkoZvqJ1mSy1835PvR+mmI3pVVV08qPgkSZJGQZIfBt4PvKSq7l8oX1VtpK15dvDBB9f09PRgAlzEzMwMoxbHiRs+MbQ4TjlsO2fePPhT8M3HT+/wehR+L6MQwyjFIUmSJEkavscN+fjnsvjC9yTZDXg73ZpCkiRJq0qS7wU+Crymqv5+2PFIkiRJkiRNqqGONFtq4fvmPwN/CvzoykckSZI0WEk+DEwDT02yBTgN2AOgqt4H/HdgX+A9SQC2V9X64UQrSZIkSZI0uUZ6TbMka4GfBl7EIp1mvet3TE1NLXtNgn6vKdHvtRDGYX2FUY9x1OMDY5Sk1a6qXr3E9l8AfmFA4UiSJEmSJK1aI91pBrwLeHNVPdzurJ5X7/od69evX/b6HWddcElf15SYu1bEYzUO6yuMeoyjHh8YoyRJkiRJkiRJo2DUO83WAxe2DrOnAi9Nsr2q/myoUUmSJEmSJEmSJGmiPG7YASymqp5RVQdW1YHAxcB/ssNMkiRJkvonyTlJ7ktyywLbk+TdSTYl+VySHxl0jJIkSZI0CEPtNGsL318DHJxkS5KTkrw+yeuHGZckSZIkrSLnAscssv0lwEHtcTLw3gHEJEmSJEkDN9TpGZda+H5O3hNXMBRJkiRJWpWq6uokBy6S5Vjg/Koq4NokT0myf1XdM5gIJUmSJGkwRnp6RkmSJEnS0K0F7up5vaWlSZIkSdJEGepIM0mSJEnSyMs8aTVvxuRkuikcmZqaYmZmZsGdbtu2bdHt42bS6nPKYduZWtP9nCQL1Wmcf3eT9tmTJEkaJjvNJEmSJEmL2QIc0PN6HXD3fBmraiOwEWD9+vU1PT294E5nZmZYbPu4mbT6nLjhE5xy2HbOvHmyLhssWKebv9H3Y20+4yf7vs/5TNpnT5IkaZicnlGSJEmStJhLgdemcyTwNdczkyRJkjSJJuuWMUmSJEnSTknyYWAaeGqSLcBpwB4AVfU+4DLgpcAm4F+A1w0nUkmSJElaWXaaSZIkSdIqVlWvXmJ7AW8cUDiSJEmSNDROzyhJkiRJkiRJkqRVz04zSZIkSZIkjYUkuyX5P0k+PuxYJEnS5LHTTJIkSZIkSePiTcDtww5CkiRNJjvNJEmSJEmSNPKSrAN+Enj/sGORJEmTyU4zSZIkSZIkjYN3Ab8JfHvIcUiSpAm1+7ADkKSVluQc4GXAfVV16Dzbjwfe3F5uA95QVZ9t2zYDDwIPA9urav1AgpYkSZIkfUeS2e90NyaZXiTfycDJAFNTU8zMzCy576k1cMph2/sTaLOc4w7Ctm3bRiaWfpvUuk1qvSRpXNhpJmk1OBc4Gzh/ge13Av+hqh5I8hJgI/D8nu0vrKqvrGyIkiRJkqRFvAD4qSQvBZ4A7JXkT6rq53ozVdVGuu90rF+/vqanp5fc8VkXXMKZN/f3Etnm45c+7iDMzMywnPdgHE1q3Sa1XoOSZDfgBuDLVfWyJPsAHwEOBDYDP1NVD7S8pwIn0d0o/StVdUVLP5zuWtIa4DLgTVVVSfaku7Z0OHA/8Kqq2jywykkaCKdnlDTxqupqYOsi2/9u9oQJuBZYN5DAJEmSJEnLUlWnVtW6qjoQOA741NwOM0kC3gTc3vN6A3BVVR0EXNVek+QQurbk2cAxwHtahxvAe+lGrB7UHse09JOAB6rqmcA7gbevbFUkDYOdZpK0o5OAy3teF/AXSW5s03xIkiRJkiRpxCRZB/wk8P6e5GOB89rz84CX96RfWFUPVdWdwCbgiCT7A3tV1TVVVXQjy14+z74uBo5KkhWqjqQhGer0jI9lnSFJ6rckL6TrNPu3PckvqKq7kzwNuDLJ59vItfnKD33u/GHPez5pc69PWn1g8uo0CfVZxvlQgN8HXgr8C3BiVX1msFFKkiSNjqqaAWaGHIak0fMu4DeBJ/WkTVXVPQBVdU+7vgOwlm62oVlbWtq32vO56bNl7mr72p7ka8C+gEt6SBNk2GuanctjW2dIkvoiyQ/T3Yn0kqq6fza9qu5uP+9L8jHgCGDeTrNRmDt/2PPmT9rc65NWH5i8Ok1Ifc5l8fOhl/DItCDPp5sqxPMhSZIkSWqSzN6IeGOS6eUUmSetFklfrMzcWIZ+UzUM/8bqhUzCza/LYT3H11A7zarq6iQHLrL973peus6QpBWR5HuBjwKvqaq/70n/buBxVfVge/5i4K1DClPShFrqfIhuCpDz29Qg1yZ5SpL9Z++WlCRJkiTxAuCnkrwUeAKwV5I/Ae6d/f7Upl68r+XfAhzQU34dcHdLXzdPem+ZLUl2B54MbJ0byCjcVA3Dv7F6IRNy8+uSrOf4Gqc1zeauMyRJy5Lkw8A1wMFJtiQ5Kcnrk7y+ZfnvdMPp35PkpiQ3tPQp4G+TfBa4HvhEVX1y4BWQtNp9ZwqQpnd6EEmSJEla9arq1KpaV1UHAscBn6qqnwMuBU5o2U4ALmnPLwWOS7JnkmfQzexxfbs58cEkR7ap8l87p8zsvl7RjvGokWaSxtuwp2dclgXWGerdvtNDXmH01xIah6GNox7jqMcHxjgIVfXqJbb/AvAL86R/EXjOSsUlScu0rClAYMdzov32228k2u5R+R/SG0e/pz3ZGSsx7cpyzP0djMLvZRRiGKU4JEmStCLOAC5KchLwJeCVAFV1a5KLgNuA7cAbq+rhVuYNdNPor6EbxDE7kOMDwAeTbKIbYXbcoCohaXBGvtNsoXWGeu3KkFcY/bWExmFo46jHOOrxgTFKkpa00LQhj9J7TnTwwQcv+5xoJY3K/5DeOE7c8ImhxXHKYdv7Pu3Kcsw9Tx2F38soxDBKcUiSJKk/qmoGmGnP7weOWiDf6cDp86TfABw6T/o3aZ1ukibXSE/PuNA6Q5IkSavIpcBr0zkS+JrrmUmSJEmSJPXfUEeatXWGpoGnJtkCnAbsAVBV72PHdYYAtlfV+uFEK0mS1H/LOB+6DHgpsAn4F+B1w4lUkiRJkiRpsg2102xX1xmSJEmaFMs4HyrgjQMKR5IkSZIkadUa6ekZJUmSJEmSJEmSpEGw00ySJEmSJEmSJEmrnp1mkiRJkiRJkiRJWvXsNJMkSZIkSZIkSdKqZ6eZJEmSJEmSJEmSVj07zSRJkiRJkiRJkrTq2WkmSZIkSatckmOSfCHJpiQb5tn+5CR/nuSzSW5N8rphxClJkiRJK8lOM0mSJElaxZLsBvwB8BLgEODVSQ6Zk+2NwG1V9RxgGjgzyeMHGqgkSZIkrTA7zSRJkiRpdTsC2FRVX6yqfwUuBI6dk6eAJyUJ8ERgK7B9sGFKkiRJ0srafdgBSJIkSZKGai1wV8/rLcDz5+Q5G7gUuBt4EvCqqvr23B0lORk4GWBqaoqZmZkFD7pt27ZFt4+bSavPKYdtZ2pN93OSDLJOg/o8TNpnT5IkaZjsNJMkSZKk1S3zpNWc10cDNwEvAn4AuDLJ31TV13coVLUR2Aiwfv36mp6eXvCgMzMzLLZ93ExafU7c8AlOOWw7Z948WZcNBlmnzcdPD+Q4k/bZkyRJGianZ5QkSZKk1W0LcEDP63V0I8p6vQ74aHU2AXcCPzig+CRJkiRpIOw0kyRJkqTV7dPAQUmekeTxwHF0UzH2+hJwFECSKeBg4IsDjVKSJEmSVthkzbMgSZIkSdopVbU9yS8DVwC7AedU1a1JXt+2vw94G3BukpvppnN8c1V9ZWhBS5IkSdIKsNNMkiRJkla5qroMuGxO2vt6nt8NvHjQcUmSJEnSIA11esYk5yS5L8ktC2xPkncn2ZTkc0l+ZNAxSpIkSZIkSZIkafINe02zc4FjFtn+EuCg9jgZeO8AYpIkSZIkSZIkSdIqM9ROs6q6Gti6SJZjgfOrcy3wlCT7DyY6SZPisYxqTXJMki+0bRsGF7UkSZIkSZIkaZCGPdJsKWuBu3peb2lpkrQzzmUXRrUm2Q34g7b9EODVSQ5Z0UglSZIkSZIkSUOx+7ADWELmSatHZUpOprvQzdTUFDMzM8va+dQaOOWw7Y8lvh0s97jLtW3btr7vs99GPcZRjw+McRCq6uokBy6S5TujWoFrk8yOaj0Q2FRVXwRIcmHLe9sKhyxJkiRJkiRJGrBR7zTbAhzQ83odcPfcTFW1EdgIsH79+pqenl7Wzs+64BLOvLl/b8Hm45d33OWamZlhuXUZllGPcdTjA2McEQuNap0v/fkL7WRXOvBHvfN+Z417B+tck1YfmLw6TUJ9khwD/D6wG/D+qjpjzvYnA38CfC/dudvvVdUfDzxQSZIkSZKkCTfqnWaXAr/cRnc8H/haVd0z5JgkTZ6FRrUua7TrdzbsQgf+qHfe76xJ62CdtPrA5NVp3OvTMw3sT9B1zH86yaVV1Tui9Y3AbVX1H5PsB3whyQVV9a9DCFmSJGkokjwBuBrYk+561sVVddpwo5IkSZNmqJ1mST4MTANPTbIFOA3YA6Cq3gdcBrwU2AT8C/C64UQqacItNKr18QukS1K/HMHS08AW8KQkAZ4IbAX6N0RVkiRpPDwEvKiqtiXZA/jbJJdX1bXDDkySJE2OoXaaVdWrl9hedHdXS9JKmndUa5J/Bg5K8gzgy8BxwM8OMU5Jk2c508CeTddO3Q08CXhVVX17MOFJkiSNhnaNaFt7uUd7LDgTiCRJ0q4Y9ekZJekx29VRrVW1PckvA1fQrTV0TlXdOvAKSJpky5kG9mjgJuBFwA8AVyb5m6r6+qN21rO24n777TcS672NyrpzvXH0cy3JndXvtSyXa+7vYBR+L6MQwyjFIUlaWpva+kbgmcAfVNV1Qw5JkiRNGDvNJE28xzKqtaouo+tUk6SVsND0sL1eB5zR2qpNSe4EfhC4fu7OetdWPPjgg5e1tuJKG5V153rjOHHDJ4YWxymHbe/rWpbLNXfNy1H4vYxCDKMUhyRpaVX1MPDcJE8BPpbk0Kq6pTdP701EU1NTy7oxYiVuahmVGzIm+eaQSa3bpNZrpS207mGSfYCPAAcCm4GfqaoHWplTgZOAh4FfqaorWvrhwLnAGrprQm+qqkqyJ3A+cDhwP90sIJsHVEVJA2KnmSRJ0vB8mqWngf0ScBTwN0mmgIOBLw40SkmSpBFSVV9NMgMcA9wyZ9t3biJav379sm4iOuuCS/p+U8vcG1aGZZJvDpnUuk1qvQZg3nUPgf8HuKqqzkiyAdgAvDnJIXTfv54NPB34yyTPap3z76XrfL+WrtPsGOByug62B6rqmUmOA94OvGqw1ZS00h437AAkSZJWq6raDsxOA3s7cFFV3Zrk9Ule37K9Dfg3SW4GrgLeXFVfGU7EkiRJw5FkvzbCjCRrgB8HPj/UoCSNjOrMt+7hscB5Lf084OXt+bHAhVX1UFXdSbdkxxFJ9gf2qqpr2mwf588pM7uvi4Gjksw35b6kMeZIM0mSpCGabxrYtt7i7PO7gRcPOi5JkqQRsz9wXlvX7HF0Nxt9fMgxSRoh8617mGSqqu4BqKp7kjytZV9LN5Js1paW9q32fG76bJm72r62J/kasC/gTY3SBLHTTJIkSZIkSSOtqj4HPG/YcUgaXfOte7hI9vlGiNUi6YuV2XHHrq24qNWybp/1HF92mkmSJEmSJEmSJsKcdQ/vTbJ/G2W2P3Bfy7YFOKCn2Drg7pa+bp703jJbkuwOPBnYOs/xXVtxEatl3T7rOb5c00ySJEmSJEmSNLYWWffwUuCElu0E4JL2/FLguCR7JnkGcBBwfZvK8cEkR7b1yl47p8zsvl4BfKqteyZpgjjSTJIkSZIkSZI0zuZd9zDJNcBFSU4CvgS8EqCqbk1yEXAbsB14Y5veEeANwLnAGuDy9gD4APDBJJvoRpgdN5CaSRooO80kSZIkSZIkSWNroXUPq+p+4KgFypwOnD5P+g3Ao9ZDq6pv0jrdJE0uO80kSZI0sg7c8Im+7OeUw7ZzYp/2JUmSJEmSJpNrmkmSJEmSJEmSJGnVs9NMkiRJkiRJkiRJq56dZpIkSZIkSZIkSVr17DSTJEmSpFUuyTFJvpBkU5INC+SZTnJTkluT/PWgY5QkSZKklTbUTrOlvpgleXKSP0/y2fbF7HXDiFOSJEmSJlWS3YA/AF4CHAK8Oskhc/I8BXgP8FNV9WzglYOOU5IkSZJW2tA6zZbzxQx4I3BbVT0HmAbOTPL4gQYqSZIkSZPtCGBTVX2xqv4VuBA4dk6enwU+WlVfAqiq+wYcoyRJkiStuGGONFvOF7MCnpQkwBOBrcD2wYYpSZIkSRNtLXBXz+stLa3Xs4C9k8wkuTHJawcWnSRJkiQNyO5DPPZ8X8yePyfP2cClwN3Ak4BXVdW3BxOeJEmSJK0KmSet5rzeHTgcOApYA1yT5Nqq+vsddpScDJwMMDU1xczMzIIH3bZt26Lbx82k1eeUw7Yztab7OUkGWadBfR4m7bMnSZI0TMPsNFvOF7OjgZuAFwE/AFyZ5G+q6us77Ggnvpj16vfJcr9PUsfhxHfUYxz1+MAYJUmSNHRbgAN6Xq+ju3Fxbp6vVNU3gG8kuRp4DrBDp1lVbQQ2Aqxfv76mp6cXPOjMzAyLbR83k1afEzd8glMO286ZNw/zskH/DbJOm4+fHshxJu2zJ0mSNEzDPPtdzhez1wFnVFUBm5LcCfwgcH1vpp35YtbrrAsu6evJcr9PiMfhxHfUYxz1+MAYJUmSNHSfBg5K8gzgy8BxdGuY9boEODvJ7sDj6WYJeedAo5QkSZKkFTbMNc2+88UsyePpvphdOifPl+im/yDJFHAw8MWBRilJkiRJE6yqtgO/DFwB3A5cVFW3Jnl9kte3PLcDnwQ+R3cT4/ur6pZhxSxJkiRJK2FoI82qanuS2S9muwHnzH4xa9vfB7wNODfJzXTTOb65qr4yrJglja8kxwC/T9fevL+qzpiz/TeA49vL3YEfAvarqq1JNgMPAg8D26tq/f+fvX8Pl6Uu77z/90dAxSMoukUggpGYqMTTDmjMYY0aRTQheR5NMCRKxgw/HDWabJ+ImTzRmHEenIyJB1RmRxlEiWg8wQhGibqiTgQVgnKSuCNEthBRUWRrom68f39ULezd9Dp3d/Xh/bquvlZ31beq7291r7ur6+761tgClyRJGoOqOh84v2/aaX2P/xz483HGJUmSJEnj1Ong5Kt9Mauq64EnjTsuSbMlyV7AG4Bfohka9jNJzq2qK5fa9B4ESvLLwO9X1U09q/kPFu0lSZIkSZIkaXZ1OTyjJI3LkcCOqvpSVX0fOBs4doX2zwTeMZbIJEmSJEmSJEkTwaKZpHlwEHBdz+Od7bTbSXIX4GjgPT2TC/hwkouTnDiyKCXNpSRHJ7k6yY4kJy/TZiHJpUmuSPL3445RkiRJkiRpHnQ6PKMkjUkGTKtl2v4y8H/6hmZ8XFVdn+S+wAVJvlBVH7/dkzQFtRMBtmzZwuLi4qqBbdkXth2xe9V2a7WW5xylXbt2dR7DMM1af2D2+jTt/VnL8LFJ9gPeCBxdVV9uc5EkSZIkSZKGzKKZpHmwEzik5/HBwPXLtD2OvqEZ2+srUlU3JnkfzXCPtyuaVdV2YDvA1q1ba2FhYdXAXn/WObz6suGl4muPX/05R2lxcZG19HtazFp/YPb6NAP9uW34WIAkS8PHXtnT5jeB91bVl6HJRWOPUpIkSZIkaQ44PKOkefAZ4PAkhyW5I01h7Nz+RknuCfwicE7PtLsmufvSfeBJwOVjiVrSPFjL8LE/AeyfZLEdJvZZY4tOkiRJkiRpjnimmaSZV1W7kzwf+BCwF3B6VV2R5KR2/mlt018DPlxV3+lZfAvwviTQ5My/rqq/HV/0kmbcWoaP3Rt4NPAEYF/gU0kurKp/ut3KeoaJvc997jMRQ1dudgjNYQ1hO+zhcKctjv7XYBKGNp2EGCYpDkmSJElS9yyaSZoLVXU+cH7ftNP6Hp8BnNE37UvAw0ccnqT5tZbhY3cCX28L+t9J8nGavHS7olnvMLEPfvCD1zRM7KhtdgjNE04+byhxbDti91CHw522OPqH752EoU0nIYZJikOSJEmS1D2HZ5QkSerOWoaPPQf4+SR7J7kLcBRw1ZjjlCRJkiRJmnnd/9xWkiRpTq1l+NiquirJ3wKfB34IvLmqvLaiJEmSJEnSkFk0kyRJ6tAah4/9c+DPxxmXJEmSJEnSvHF4RkmSJEmSJEmSJM09i2aSJEmSJEmSJEmaexbNJEmSJEmSNNGSHJLkY0muSnJFkhd2HZMkSZo9XtNMkiRJkiRJk243sK2qLklyd+DiJBdU1ZVdByZJkmaHZ5pJkiRJkiRpolXVDVV1SXv/FuAq4KBuo5I0KZY7GzXJvZJckOSL7d/9e5Z5aZIdSa5O8uSe6Y9Oclk773VJ0k6/U5J3ttMvSnLo2DsqaeQ6PdMsydHAa4G9gDdX1SkD2iwArwH2Ab5eVb84xhAlSZKkqXfoyeft8XjbEbs5oW/aelx7ylM3G5IkSRvWHqh+JHDRgHknAicCbNmyhcXFxVXXt2Xf5rNxmNbyvOOwa9euiYll2Ga1b7ParzEYeDYqcALwkao6JcnJwMnAS5I8BDgOeChwf+DvkvxEVd0KvIkmj1wInA8cDXwQeA7wzap6UJLjgFcBvzHWXkoauc6KZkn2At4A/BKwE/hMknN7T6tPsh/wRuDoqvpykvt2EqwkSZIkSZI6l+RuwHuAF1XVt/vnV9V2YDvA1q1ba2FhYdV1vv6sc3j1ZcM9RHbt8as/7zgsLi6ylm0wjWa1b7Par1GrqhuAG9r7tyRZOhv1WGChbfZWYBF4STv97Kr6HnBNkh3AkUmuBe5RVZ8CSHIm8Ks0RbNjgZe363o3cGqSVFWNuHuSxqjL4RmPBHZU1Zeq6vvA2TSJp9dvAu+tqi8DVNWNY45RkiRJkiRJEyDJPjQFs7Oq6r1dxyNpMvWdjbqlLagtFdaWTso4CLiuZ7Gd7bSD2vv90/dYpqp2AzcD9x5JJyR1psvhGQclpqP62vwEsE+SReDuwGur6szxhCdJkiRJkqRJ0F5T6C3AVVX1F13HI2ky9Z+N2l6ObGDTAdNqhekrLdMfg8PErmBehiC1n9Ory6LZWpLM3sCjgScA+wKfSnJhVf3THivaQCKC4SejYb85puENN+kxTnp8YIySJEmSJK3B44DfBi5Lcmk77Y+q6vzuQpI0SZY5G/WrSQ6sqhuSHAgsjWS2EzikZ/GDgevb6QcPmN67zM4kewP3BG7qj8NhYlc2L0OQ2s/p1WXRbLnE1N/m61X1HeA7ST4OPBzYo2i2kUQEw09Gw05E0/CGm/QYJz0+MEZJkiRJklZTVZ9k8A+wJWmls1HPBZ4NnNL+Padn+l8n+Qvg/sDhwKer6tYktyR5DM3wjs8CXt+3rk8BTwc+6vXMpNnTZdHsM8DhSQ4DvgIcR3MNs17n0FxQcW/gjjTDN/7lWKOUJEmSJElap0NPPm+o67v2lKcOdX2SNGMGno1KUyx7V5LnAF8GngFQVVckeRdwJbAbeF5V3dou91zgDJqRzz7Y3qApyr0tyQ6aM8yOG3GfJHWgs6JZVe1O8nzgQ8BewOltsjqpnX9aVV2V5G+BzwM/BN5cVZd3FbMkSZIkSZIkabKscjbqE5ZZ5pXAKwdM/yzwsAHT/5226CZpdnV5phntuNPn9007re/xnwN/Ps64JEmSJGmeJDkaeC3NDxrfXFWnLNPuZ4ALgd+oqnePMURJkiRJGrk7dB2AJEmSJKk7SfYC3gA8BXgI8MwkD1mm3atoRguRJEmSpJlj0UySJEmS5tuRwI6q+lJVfR84Gzh2QLsXAO8BbhxncJIkSZI0LhbNJEmSJGm+HQRc1/N4ZzvtNkkOAn4N2GM4fUmSJEmaJZ1e00ySJEmS1LkMmFZ9j18DvKSqbk0GNW9XlJwInAiwZcsWFhcXl227a9euFedPm1nrz7YjdrNl3+bvLJnmPi33/pq1954kSVKXLJpJmgurXdw+yQJwDnBNO+m9VfWKtSwrSZI05XYCh/Q8Phi4vq/NVuDstmB2AHBMkt1V9f7eRlW1HdgOsHXr1lpYWFj2SRcXF1lp/rTpsj+HnnzeCNa6N9uO2M2rL5utwwbT3Kdrj18YOH3W/pckSZK6NJ17ipK0Dj0Xt/8lmoNCn0lyblVd2df0E1X1tA0uK0mSNK0+Axye5DDgK8BxwG/2Nqiqw5buJzkD+EB/wUySJEmSpp3XNJM0D9Z6cfthLytJq0pydJKrk+xIcvIK7X4mya1Jnj7O+CTNvqraDTwf+BBwFfCuqroiyUlJTuo2OkmSJEkaH880kzQPBl3c/qgB7R6b5HM0wxG9uKquWMeykrRuaz2btW33KpoD2pI0dFV1PnB+37TTlml7wjhikiRJkqRxs2gmaR6s5eL2lwAPqKpdSY4B3g8cvsZlmydZx4Xvlwz7QuRdXwB81i5CPmv9gdnr0wz057azWQGSLJ3N2j8E7AuA9wA/M97wJEmSJEmS5odFM0nzYNWL21fVt3vun5/kjUkOWMuyPcut+cL3S15/1jlDvRD5chcHH5dZuwj5rPUHZq9PM9CfVc9mTXIQ8GvA47FoJkmSJEmSNDIWzSTNg1Uvbp/kfsBXq6qSHElzzcdvAN9abVlJ2oS1nM36GuAlVXVrMqh5z8p6zni9z33uMxFn4W32bMBhnY077DN75z2OYby3JuVM0UmJQ5IkSZLUPYtmkmZeVe1OsnRx+72A05cubt/OPw14OvDcJLuBfwOOq6oCBi7bSUckzaK1nM26FTi7LZgdAByTZHdVvb9/Zb1nvD74wQ9e0xmvo7bZswFPOPm8ocSx7YjdQz2zd97jGMaZzZNypuikxCFJkiRJ6l7339glaQxWu7h9VZ0KnLrWZSVpSFY9E7aqDlu6n+QM4AODCmaSJEmSJEnaHItmkiRJHVnjmbCSJEmSJEkaA4tmkiRJHVrtTNi+6SeMIyZJkiRJkqR5dIeuA5AkSZIkSZIkSZK61mnRLMnRSa5OsiPJySu0+5kktyZ5+jjjkyRJkiRJkiRJ0nzorGiWZC/gDcBTgIcAz0zykGXavYrmWh+SJEmSJEmSJEnS0HV5ptmRwI6q+lJVfR84Gzh2QLsXAO8BbhxncJIkSZIkSZIkSZofe3f43AcB1/U83gkc1dsgyUHArwGPB35muRUlORE4EWDLli0sLi6uKYAt+8K2I3avK+iVrPV512rXrl1DX+ewTXqMkx4fGKMkSZIkSZIkSZOgy6JZBkyrvsevAV5SVbcmg5q3C1VtB7YDbN26tRYWFtYUwOvPOodXXza8TXDt8Wt73rVaXFxkrX3pyqTHOOnxgTFKkiRJkiRJkjQJuiya7QQO6Xl8MHB9X5utwNltwewA4Jgku6vq/WOJUJIkSZIkSZIkSXOhy6LZZ4DDkxwGfAU4DvjN3gZVddjS/SRnAB+wYCZJkiRJkiRJkqRh66xoVlW7kzwf+BCwF3B6VV2R5KR2/mldxSZJkiRJkiRJkqT50uWZZlTV+cD5fdMGFsuq6oRxxCRJkiRJkiRJkqT5c4euA5AkSZIkSZIkSZK6ZtFMkiRJkiRJEy/J6UluTHJ517FIkqTZZNFMkiRJkiRJ0+AM4Oiug5A0mQYV1pPcK8kFSb7Y/t2/Z95Lk+xIcnWSJ/dMf3SSy9p5r0uSdvqdkryznX5RkkPH2kFJY2HRTJIkSZIkSROvqj4O3NR1HJIm1hncvrB+MvCRqjoc+Ej7mCQPAY4DHtou88Yke7XLvAk4ETi8vS2t8znAN6vqQcBfAq8aWU8kdWbvrgOQJEnS7Dj05PP2eLztiN2c0DdNkiRJkoatqj4+4OyvY4GF9v5bgUXgJe30s6vqe8A1SXYARya5FrhHVX0KIMmZwK8CH2yXeXm7rncDpyZJVdVoeiSpCxbNJEmSJEmSNBOSnEhzhghbtmxhcXFx1WW27Nv80GeY1vK847Br166JiWXYZrVvs9qvDm2pqhsAquqGJPdtpx8EXNjTbmc77Qft/f7pS8tc165rd5KbgXsDXx9d+JLGzaKZJEmSJM25JEcDrwX2At5cVaf0zT+e5lfZALuA51bV58YbpSStrqq2A9sBtm7dWgsLC6su8/qzzuHVlw33ENm1x6/+vOOwuLjIWrbBNJrVvs1qvyZQBkyrFaavtMyeK7Z4v6J5KQzbz+ll0UySJEmS5lh7/Y43AL9E82vqzyQ5t6qu7Gl2DfCLVfXNJE+hOSB91PijlSRJWpevJjmwPcvsQODGdvpO4JCedgcD17fTDx4wvXeZnUn2Bu7JgOssWrxf2bwUhu3n9LpD1wFIkiRJkjp1JLCjqr5UVd8Hzqa5Zsdtquofquqb7cML2fNgkiSNRZJ3AJ8CHpxkZ5LndB2TpIl3LvDs9v6zgXN6ph+X5E5JDgMOBz7dDuV4S5LHJAnwrL5lltb1dOCjXs9Mmj2eaSZpLmxmyKH2IrC3ALcCu6tq67jiliRJGoPbrs/R2snKZ5E9B/jgSCOSpAGq6pldxyBpcrWF9QXggCQ7gZcBpwDvaovsXwaeAVBVVyR5F3AlsBt4XlXd2q7qucAZwL40+zxL+z1vAd6WZAfNGWbHjaFbksbMopmkmTekIYf+Q1V5YVdJQ+d1hCRNgDVdnwMgyX+gKZr93DLz13wNj1m7/kGX/Rn2NVCWjOL6Kl2b5j4t9/6atf8lSdqoFQrrT1im/SuBVw6Y/lngYQOm/ztt0U3S7LJoJmke3DbkEECSpSGHbiuaVdU/9LR3yCFJY+F1hCRNiOWu6bGHJD8NvBl4SlV9Y9CK1nMNj1m7/kGX/Tnh5PNGst5tR+we+vVVujbNfVru2jSz9r8kSZLUJa9pJmkeDBpy6KAV2vcPOVTAh5Nc3P56WpKGxesISZoEnwEOT3JYkjvSDDV0bm+DJD8GvBf47ar6pw5ilCRJkqSRm86fV0nS+mx2yKHHVdX1Se4LXJDkC1X18QHLrnk4oiXDHh6m62FZZm1omFnrD8xen2agP15HSFLnqmp3kucDH6IZKvb09jofJ7XzTwP+BLg38MYk4HVeJUmSJM0gi2aS5sGmhhyqquvbvzcmeR/NmSG3K5qtZziiJa8/65yhDg+z3JAt4zJrQ8PMWn9g9vo0A/0Z2nWE2ja3Fe/vc5/7dFJQ7P8hwKRcO8Y4hhvHMN5bk1L0npQ4ulZV5wPn9007ref+7wK/O+64ZtWhIxpOUZIkSdLmdFo088L3ksbktiGHgK/QDDn0m70NlhtyKMldgTtU1S3t/ScBrxhb5JJm3dCuIwR7Fu8f/OAHr6l4P2z919WZlGvHGMdw4xjGjzQmpeg9KXFIkiRJkrrX2Td2L3wvaVw2OeTQFuB97bS9gb+uqr/toBuSZtOGi/pSl4Zxlsy2I3bfVmS99pSnbnp90pJRnMXle1SSJEmaD13+zPW2C98DJFm68P1tRbOq+oee9l74XtKGbXTIoTZHPXzkAUqaS15HSJIkSZIkaXJ0WTTzwveSJGnueR0hSZIkSZKkydBl0WxoF77vvej9li1b1nwh72FfiH3YFxCfhouST3qMkx4fGKMkSZIkSZIkSZOgy6LZ0C5833vR+61bt675ovevP+ucoV6IfRgXRO81DRcln/QYJz0+MEZJkiRJkiRJkibBHTp87tsufJ/kjjQXvj+3t4EXvpckSZIkSZIkSdI4dHammRe+lyRJkiRJkiRJ0qTocnhGL3wvSZIkSZIkSZKkidBp0UySJEmSDj35vKGv89pTnjr0dUqSJEmSZluX1zSTJEmSJEmSJEmSJoJnmkmSJEmStIK1nA257YjdnDCCsyYlSZIkjY9nmkmSJEmSJEmSJGnuWTSTJEmSJEmSJEnS3LNoJkmSJEmSJEmSpLln0UySJEmSJEmSJElzz6KZJEmSJEmSJEmS5t7eXQcgSZIkScN26MnnrandtiN2c8Ia2l57ylM3G5IkSZIkacJ5ppkkSZIkSZIkSZLmnkUzSZIkSZIkSZIkzT2HZ5QkSZpjax3CTpIkSZIkadZZNBuiYR90OuPouw51fZIkSZIkSZIkSRrM4RklSZIkSZIkSZI09zzTTJJmyLDPeL32lKcOdX2SJEmSJEmSNKk6PdMsydFJrk6yI8nJA+Ynyeva+Z9P8qgu4pQ0/TaTb1ZbVpI2w/0hSZPAXCRpGvjdTNIkMBdJs62zM82S7AW8AfglYCfwmSTnVtWVPc2eAhze3o4C3tT+laQ120y+WeOykrQh7g9JmgRd56JhnykvaTb53UzSJDAXSbOvyzPNjgR2VNWXqur7wNnAsX1tjgXOrMaFwH5JDhx3oJKm3mbyzVqWlaSNcn9I0iQwF0maBn43kzQJzEXSjOvymmYHAdf1PN7J7X+pOKjNQcANow1tMlz2lZs5YYi/uvTaRJpjm8k3a1lWkjbK/SFJk8BcJGka+N1M0iQwF0kzrsuiWQZMqw20IcmJwIntw11Jrl5jDAcAX19j27H7vSHHl1cNa017mOhtyOTHB7MV4wNGHcgGbSbfrCkPwYZz0US//hvIGxPdnw2Ytf7A7PWpvz+TmoeWM7T9IbhdHvpekss3EdtQDHt/xjhmJ45JiGE9cazjM3Ha8hB0991sIt4DwzIp7+lhsk+TZYU8NO37Q2s1ymNEQ39fjOgYzEZM7Xt+DWa1b7PQr1nNQ7CGXGQeWtUsvMfXwn52a8N5qMui2U7gkJ7HBwPXb6ANVbUd2L7eAJJ8tqq2rne5cZn0+GDyY5z0+MAYx2Qz+eaOa1gW2FgumoFtuwf7M/lmrU8z0J+h7Q/BnnloUraNcRjHJMcwSXF0rJPvZrO27WetP2CfpsUs9mkZIztGNMvb0L5Nn1nt1wxZNReZh1Y2L321n9Ory2uafQY4PMlhSe4IHAec29fmXOBZaTwGuLmqHP5D0nptJt+sZVlJ2ij3hyRNAnORpGngdzNJk8BcJM24zs40q6rdSZ4PfAjYCzi9qq5IclI7/zTgfOAYYAfwXeB3uopX0vTaTL5ZbtkOuiFpBrk/JGkSmIskTQO/m0maBOYiafZ1OTwjVXU+zZev3mmn9dwv4HkjDGHdQzqO2aTHB5Mf46THB8Y4FpvJN4OWHaKp37Z97M/km7U+TX1/Rrg/NCnbxjj2ZBw/MgkxwOTE0amOvpvN2raftf6AfZoWs9ingUb43WyWt6F9mz6z2q+ZMaJcNE+v+7z01X5OqTTffSRJkiRJkiRJkqT51eU1zSRJkiRJkiRJkqSJMJdFsyRHJ7k6yY4kJ3cdzyBJrk1yWZJLk3y263gAkpye5MYkl/dMu1eSC5J8sf27/4TF9/IkX2m346VJjukwvkOSfCzJVUmuSPLCdvokbcPlYpyY7TgrpiEPwcbet0le2vbr6iRP7pn+6Dav7UjyuiTpok9tLHsl+cckH2gfT3t/9kvy7iRfaF+rx05zn5L8fvt+uzzJO5LceZr7s1lt/z+d5HPtdvnTAW2OTfL5Nkd/NsnPtdMH/g+389aV2zcTRztv4L7NSq/tCLbHg3v6e2mSbyd50ai2R0/bn0lya5Kn90wb+Dkwiu2xXBzjfn+ssj3G9v5YYXsM7f0xz5I8o932P0yydYV2t9t3b6c/PMmn2vfD/05yj3b6oUn+rec1OG3wmodvVH1q5w38DBu1IfTpEUkuzI/y7JHt9E5ep1H1p503ra/RO3teh2uTXNpO7+x/aVJlSr6brddy741plxX2X6bdevZjNFtmIQ9lwP58ZuB7/KBcOsx+JblT+5m9I8lFSQ4dkubHIwAA7K1JREFUawd/FN+6jqtPaz/XrKrm6kZzgcZ/Bh4I3BH4HPCQruMaEOe1wAFdx9EX0y8AjwIu75n234GT2/snA6+asPheDry4623XxnIg8Kj2/t2BfwIeMmHbcLkYJ2Y7zsJtWvLQKu+Jge/bdt7ngDsBh7X93Kud92ngsUCADwJP6bBffwD8NfCB9vG09+etwO+29+8I7DetfQIOAq4B9m0fvws4YVr7M6RtEuBu7f19gIuAx/S1uRs/Gnb7p4EvtPcH/g+3j1/OOnL7ZuJoH1/LgH2b5V7bUcXR02Yv4F+BB4xqe/Q8z0dprnnw9J5pAz8HRrE9VohjrO+P5eIY9/tjpTiG9f6Y5xvwU8CDgUVg6wrtbrfv3k7/DPCL7f3/CPxZe//Q/rYz0KdlP8OmoE8fpv1cBY4BFrt8nUbYn6l9jfravBr4ky5fo0m9MUXfzTbQt1XfG9N4Y4X9l2m/scb9GG+zdZuVPMSA/Xlm4Hv8oFw6zH4B/xk4rb1/HPDOCernyxnwHWia+7nW2zyeaXYksKOqvlRV3wfOBo7tOKapUFUfB27qm3wszcFa2r+/Os6Yei0T38Soqhuq6pL2/i3AVTQHhydpGy4Xo4ZravLQBt63xwJnV9X3quoaYAdwZJIDgXtU1aeq+YQ8k47e60kOBp4KvLln8jT35x40OzdvAaiq71fVt5jiPgF7A/sm2Ru4C3A9092fTanGrvbhPu2t+trsavsJcNel+cPM65uJYxXr+hwcYhxPAP65qv5lDTFuKI7WC4D3ADf2TFvpc2Do22O5OMb9/lgujlWMbXv02dT7Y55V1VVVdfUa2i237/5g4OPt/QuA/3uI4W3ICPs08DNsCCGvagh9KmDpjLl70nxWd2aE/Znm1wiA9hfevw68Y4jhzZKp+W62XpN+jGSjZvm4xTr2YzRbZjYPMQPf49d5PHwj/epd17uBJ3Rxdt06PzOmtp9rNY9Fs4OA63oe72QyP1wL+HCSi5Oc2HUwK9hSVTdAs+MC3LfjeAZ5fpohmk5Ph0Mf9mpPQX0kza+GJnIb9sUIE7gdp9i05KE9rPF9u1zfDmrv90/vwmuAPwR+2DNtmvvzQOBrwP9KM+Tkm5PclSntU1V9BfgfwJeBG4Cbq+rDTGl/hiXNkKKX0hzgv6CqLhrQ5teSfAE4j+ZMhv75h7JnXod15vZNxrHcvs26PweHsT1oft3WfwBxqNsjyUHArwH9Q1+t9Dkw9O2xQhy9bQ5lxO+PVeIY2/tjLduDIbw/tGGXA7/S3n8GcEjPvMPaz7q/T/Lz4w9tw5br01TuE7ZeBPx5kutoPrdf2jNvGl+nFzG4P9P8Gi35eeCrVfXFnmnT+BqNyiy8xnNrmf2XqbaW/VzNnFnJQ4P252f1e/ww+3XbMlW1G7gZuPfIIl+/Qd+BZrGfe5jHotmgCuYk/mrjcVX1KOApwPOS/ELXAU2pNwE/DjyC5uDrqzuNBkhyN5pfNr+oqr7ddTyDDIhx4rbjlJuWPHSbdbxvl+vbRPQ5ydOAG6vq4rUuMmDaxPSntTfNKfRvqqpHAt+hGR5gORPdp3Yn7FiaU/zvD9w1yW+ttMiAaRPTn2Gpqlur6hHAwTS/4HrYgDbvq6qfpPkl15/1zlvmf3jduX2TcQxt32YI2+OONAew/6Zn8ii2x2uAl1TVrX3Th/r+3EQcTTDje3+sFMc43x8rxTG098csS/J3aa472X8bxq+i/yPNe+BimmG3vt9OvwH4sfaz7g+Av07PtcE2q6M+jfSzasR9ei7w+1V1CPD7tGe8M8LXqaP+TPNrtOSZ7PkjgJH+L02hmdpnnCfTcGxlI9ayn6uZMyt5aD3787P6PX4j/ZrkPi/3HWjW+nk7e3cdQAd2suevFQ+m46EkBqmq69u/NyZ5H82puh9fealOfDXJgVV1Q5pTMNc61M5YVNVXl+4n+SvgAx2GQ5J9aHbqzqqq97aTJ2obDopx0rbjDJiKPLRkne/b5fq2s73fP33cHgf8SpqLl94ZuEeStzO9/aGNZWfPLxDfTVM0m9Y+PRG4pqq+BpDkvcDPMr39Gaqq+laSReBomjMXBrX5eJIfT3JAVX19mf/hTeX2jcSxwr7Nhj8HNxJHO/kpwCW922BE22MrcHaaUScOAI5JspuVPwdGsT0GxlFV7x/z+2PZOMb8/lg2jnb+UN8fs6iqnjjCdX8BeBJAkp+gGVKZqvoe8L32/sVJ/hn4CeCzQ3resfeJEe8TjrJPwLOBF7b3/4Z22OtRvk5d9Ifpfo1IM9T1/wU8uuc5R/q/NIWm6ruZGsvtv8ySteznambMRB5aZn9+Vr/HD7NfS8vsbD+378mEDK27wnegmernIPN4ptlngMOTHNb+ivQ44NyOY9pDkrsmufvSfZovWJP6AXkuzRcM2r/ndBjL7bSJa8mv0eF2THNk5i3AVVX1Fz2zJmYbLhfjJG3HGTHxeWjJBt635wLHJblTksOAw4FPt6er35LkMe06n0UH7/WqemlVHVxVh9Js949W1W8xpf0BqKp/Ba5L8uB20hOAK5nePn0ZeEySu7RxPIHmOgXT2p9NS3KfJPu19/elKSx+oa/Ng9p+kuRRNBeQ/sYK/8Przu2bjGOlfZt1fQ5uJo6eJv2/uh/J9qiqw6rq0DbnvBv4z21hZqXPgaFvj+XiGPf7Y4U4xvr+WOF1WbLp94c2Lsl92793AP6YdhjN9rXdq73/QJp8/6Wu4lyP5frEMp9h3US5btcDv9jefzzwRZjq12lgf5ju1wjaHFhVtw2VNMWv0ahMzXczNVbaf5l2a9mP0Uya+jy0wv78rH6PH2a/etf1dJrjVBNxBtYK34Fmqp8DVdXc3YBjgH8C/hn4L13HMyC+BwKfa29XTEqMNAcPbgB+QFMdfg7N2KMfoflS8RHgXhMW39uAy4DP0/xzHthhfD9Hc9rp54FL29sxE7YNl4txYrbjrNwmPQ+t4T2x7PsW+C9tv64GntIzfSvNB+w/A6cC6bhvC8AH2vtT3R+aU+U/275O7wf2n+Y+AX9K8+Xw8jb/3Gma+zOE7fHTwD+2r+/lwJ+0008CTmrvv4Rmn+FS4FPAz7XTB/4Pt/PWlds3Gcey+zYrvbbDjqOddxeaAto9+9Y79O3R1/4M4Ok9jwd+DoxieywXx7jfHyvEMdb3xyqvy1DeH/N8o/lCvZPmTJavAh9qp98fOL+n3e323dvpL2z/N/4JOIU2dwP/d/v++BxwCfDL096ndt7Az7Ap6NPPARe3r8dFwKO7fJ1G1Z9pfo3aeWfQl/u6/F+a1BtT8t1sA/1a9r0xzTdW2H+Z9hvL7Md4m/3btOchltmfZwa+xw/KpcPsF80oSH8D7KD5Yc4DJ6ify34HmtZ+rvW2FLQkSZIkSZIkSZI0t+ZxeEZJkiRJkiRJkiRpDxbNJEmSJEmSJEmSNPcsmkmSJEmSJEmSJGnuWTSTJEmSJEmSJEnS3LNoJkmSJGnuJTk9yY1JLl9D279Mcml7+6ck3xpDiJJm3Drz0I8l+ViSf0zy+STHjCNGSbPPXCRp3qWquo5BkiRJkjqV5BeAXcCZVfWwdSz3AuCRVfUfRxacpLmwnjyUZDvwj1X1piQPAc6vqkPHEKakGWcukjTvPNNMkiRJ0tyrqo8DN/VOS/LjSf42ycVJPpHkJwcs+kzgHWMJUtJMW2ceKuAe7f17AtePMVRJM8xcJGne7d11AJIkSZI0obYDJ1XVF5McBbwRePzSzCQPAA4DPtpRfJJm33J56OXAh9uzXe8KPLG7ECXNAXORpLlh0UySJEmS+iS5G/CzwN8kWZp8p75mxwHvrqpbxxmbpPmwSh56JnBGVb06yWOBtyV5WFX9sINQJc0wc5GkeWPRTJIkSZJu7w7At6rqESu0OQ543njCkTSHVspDzwGOBqiqTyW5M3AAcOP4wpM0J8xFkuaK1zSTJEmSpD5V9W3gmiTPAEjj4UvzkzwY2B/4VEchSppxq+ShLwNPaKf/FHBn4GudBCppppmLJM0bi2aSJEmS5l6Sd9AUwB6cZGeS5wDHA89J8jngCuDYnkWeCZxdVTX+aCXNonXmoW3Af2qnvwM4wXwkaRjMRZLmXcxjkiRJkiRJkiRJmneeaSZJkiRJkiRJkqS5Z9FMkiRJkiRJkiRJc8+imSRJkiRJkiRJkuaeRTNJkiRJkiRJkiTNPYtmkiRJkiRJkiRJmnsWzSRJkiRJkiRJkjT3LJpJkiRJkiRJkiRp7lk0kyRJkiRJkiRJ0tyzaCZJkiRJkiRJkqS5Z9FMkiRJkiRJkiRJc8+imSRJkiRJkiRJkuaeRTNJkiRJkiRJkiTNPYtmkiRJkiRJkiRJmnsWzSRJkiRJkiRJkjT3LJpJkiRJkiRJkiRp7lk0kyRJkiRJkiRJ0tyzaCZJkiRJkiRJkqS5Z9FMkiRJkiRJkiRJc8+imSRJkiRJkiRJkuaeRTNJkiRJkiRJkiTNPYtm0holWUzyu+39E5J8ci1tN/F8ZyT5r5tZhzSPkvxYkl1J9hrCuq5IsrDMvIUkO9ewjuOTfHizsUhan0nLBZt9ngFtr03yxFHH1K5vLPskw45bUmPS8mGS05L8v5uNRZIkaZjcR9GSvbsOQPMjyRnAzqr64xGt/1rgd6vq70axfknToaq+DNxtSOt66BDWcRZw1tLjJAUcXlU7NrtuScubtFwwSc8jab5MWj6sqpOGEYskrVeSReDtVfXmrmORNHl691HaHwm9vaoO3ux6k5xAc8z65za7Lo2HZ5p1KIlFyx5uD0lLzAeSwFwwCXwNpMng/6KkSWeekiTNCotmY9YOpfOSJJ8HvpPkj5P8c5JbklyZ5Nd62v5Lkke3938rSSV5SPv4d5O8v72/V5I/6lnPxUkOaef9ZJILktyU5Ookv96z/jOSvCHJee1yFyX58XZekvxlkhuT3Jzk80ke1s7bY+jB3qEKl1suyYnA8cAftkOD/O9ltsfeSU5ebpu0y/ynJFf1zH9UkrcBPwb873b9f9i2/Zsk/9rG8vEkD+1Zz7L9b+f/UpIvtMueCuT2L2de387/QpInLPOa36F9nf+l3S5nJrlnz/yfS/IPSb6V5Lr21wf967h7ko8leV2S/jikmTAgH/T+b3wuPUMBJTms/Z++Jcnftf/Lb2/nHdrmy73bx/dPcm6bB3ck+U8963l5kne1/5e3pBlyaGtfTE9s7+/b5o1vJrkS+Jm++A9J8t4kX0vyjTZv9OfIj7fNP9fmqt9IcnmSX+5Zzz5Jvp7kEcPcvtK0mIFccG2SF6fZB7o5yTuT3Lln/tOSXNr25x+S/PQKz/PW9nmuSvKHuf2wZ49Y7nnadfxRm0+uTXJ8z/R7tn39Wpr9kz9Ocod23glJ/k+a/bmbgJe3i+2f5feZfjbJZ9o4PpPkZ3vmrbTdV9yW0ryb5nyYZh9nV8/te2nO8NhjyNe0Qzoul68kTbYBeepX2rzxrTTHjn6qp+1PtdO+1bb5lZ55ZyR5Y5IPtjnj/yS5X5LXtDnmC0ke2dN+xe9eSf5Hu9w1SZ7Sznsl8PPAqe1znDq2DSVpbNp9ngf1PB6037EtzTHaG5L8Tn/bJHcFPgjcv2df5v5Z+Rh8JTkpyRfb/POGNH4KOA14bLueb411g2hDLJp145nAU4H9gKtpPrTvCfwp8PYkB7bt/h5YaO//AvAl4Bd7Hv99e/8P2nUeA9wD+I/Ad9t/8AuAvwbu27Z5Y3oKR+20PwX2B3YAr2ynP6l9jp9o4/wN4Btr6NvA5apqO83wZP+9qu5WVb/cs8xt26OqdgP/vNw2SfIMmoM3z2r7+ivt+n8b+DLwy+36/3u77g8Ch7f9v4SeIdJW6n+SA4D3AH8MHNDG9Li+ZY+ieU0OAF4GvDfJvQZskxPa238AHkgzNMrSDt2PtTG+HrgP8Ajg0t6Fk9wb+Ajwf6rq96qqBjyHNCuW8sEDgXOA/wrcC3gx8J4k92nb/TXwaeDeNDnht1dY5zuAncD9gacD/y17Frl/BTibJmedS/v/OcDLgB9vb08Gnr00I801Qj4A/AtwKHBQu849VNUvtHcf3uaqdwJnAr/V0+wY4IaqunSFPkmzbipzQY9fB44GDgN+mmY/gCSPAk4H/n9tzP8TODfJnZZ5nkNptsEvsWeeWPF5Wvej2Uc5qI1xe5IHt/NeT7Of9UCafctnAb/Ts+zSPs59+dG+4XL7TPcCzgNe1/bpL4Dz2v0XWHm7r2VbSvNuKvNhVb2z3de5W/s8X2qfd5CV8pWkybeUp46k+T9/Ec3xjfNpfth8xyT7AP8b+DDN/sULgLP6/td/nR8dg/ke8Cma4zgHAO+m2cdYy3evo2iOtR0A/HfgLUlSVf8F+ATw/DY/PX+oW0HStLgfzXehg4DnAG9Isn9vg6r6DvAU4Pql/Zmqup5ljsH3LPo0mh8RPZwmpz25qq4CTgI+1a5nv1F2TsMxs0WzJKe3FePL19j+19OctXRFkr8ecXivq6rrqurfqupvqur6qvphe/D0izQ7GtAUxZaKZD8P/H89j3+RHxXNfhf446q6uhqfq6pv0PyjXltV/6uqdlfVJTSFoKf3xPLeqvp0W6w6i6ZoA/AD4O7ATwKpqquq6oY19G0jy922PQBW2Sa/S1N4+0zb1x1V9S/LrbiqTq+qW6rqezRfHh+enrO8Vuj/McCVVfXuqvoB8BrgX/tWfyPwmqr6QRvn1TQ7iv2OB/6iqr5UVbuAlwLHpfml5/HA31XVO9r1fKPvQPn9aV7nvxnVteA0OhOehybV66rqOpqDw+dX1fltLrgA+CxwTFts/hngT6rq+1X1SZoDOrfT/uLn54CXVNW/t/9fb2bPA0mfbJ/nVuBtNDs3g/w68MqquqmN8XU9846k+X/9f6rqO+1zfXKNfX572697tI9/u41DmmfTmgt647++qm6iOUD0iHb6fwL+Z1VdVFW3VtVbaQ4KPWaZ5/lvVfXNqtq5zudZ8v9W1feq6u9pClu/3h5o+g3gpe0+0rXAq/u2xfVV9fp2//Hf2mnL7TM9FfhiVb2tbf8O4AvAL69hu69lW2rKuT+0aVOdD9OcxfrXwGJV/c8V+nm7fLVCW2ldzEMjt5SnfgU4r6ouaI+j/A9gX+BnafZ17gac0uapj9IUvp7Zs573VdXFVfXvwPuAf6+qM9tc9E5g6Uyz1b57/UtV/VW73FuBA4Eto+q8tBbmoYnyA+AV7XHY84FdwFp/rLPcMfglp1TVt6q5nuzHuP33M02JmS2aAWfQ/PJ2VUkOpylkPK6aCxu/aHRhAXBdz3M/Kz8aoudbwMNofg0DTbHk55PcD9iLZifhcUkOpamIX9q2O4TmTKh+DwCOWlp3u/7jaSrqS3oLQd+lvUB0uwNzKvAG4KtJtvcc0F3WBpe7rvfBKttkub7eTnvK7CntKbPfBq5tZx3Q02xg/2l2wG6Lq6qqP07gK+30Jf/SLtfv/u283nZ70+y0rdafp9LsZJ62QhtNrjOY3Dw0qZb+zx4APKMvf/0czRee+wM3VdV3ByzXb6ntLT3T/oXmF0VL+vPAnTN4PP498gJ7/l8fQvPlbPcycSyrml8r/R/g/06yH82vmfrPipXmzbTmguXWtbR/8QBgW19/DmH5/Yfe5xnUt+WeB+Cb1fxCsjfO+9PsB92R2++b9G6L9TxX/35O7/pW2+5r2Zaafmfg/tBmTHs+fCXNjyp/b5l4YPl8JQ3LGZiHRmkpD+yxT1BVP2znLe0TXNdOW9Kfe77ac//fBjxe2vdY7bvXbTmsJy/ebZm20ricgXloUnyjL3/0f49ayWrHcVf6fqYpMrNFs6r6OHBT77QkP57kb9OMN/qJJD/ZzvpPwBuq6pvtsjeOOrw2ngcAfwU8H7h3NadnXk577ayq2kHzD/Z7wMfbLzb/CpxI8+u/pZ2N62iGxOh3HfD3VbVfz+1uVfXcNQVZ9bqqejTwUJrhFv+fdtZ3gLv0NL3fGpdbbljB26avtk1W6Oug9f8mcCzwRJoi46FLT7PM8r1uoEmES3Gl93HroHb6kh8Drh+wrutpvuT2tttNswO4Un+g2RZ/C5yfZrhNTZEJz0OTaun/+DrgbX35665VdQrN/+e9kvTmof7/zyXXt23v3jPtx4CvbCC2PfJCu54l1wE/tswBpbV4K80vyJ9Bc8r+RuKTZsm05oLVXEdzVkZvf+7Snp016HkO7nm8XN+Ws3/fvsPSfsrXaX5d2b9v0rst1jMUdP9+Tu/6Vtvum9mWmhLuD23a1ObDJMfRnEXy9Pask+Usl6+koTAPjdxSntpjn6DnOMrSPsEh7dmnSzaaezbz3cvLXagT5qGx+i4rHLdeh0H5YrXjuOtZlybYzBbNlrEdeEFb0Hkx8MZ2+k8AP5HmQqMXJllT5X8I7krzT/M1gDQXHnxYX5u/pykgLQ3FuNj3GJrhNP4syeFp/HSa60h8gKZfv51kn/b2M+m5EOty2nZHpRl3+jvAvwO3trMvBf6vJHdJc2HF56xxua/SjMW/mW3yZuDFSR7d9vVBbaFt0PrvTjPk0TdokuV/W63fPc4DHprk/2p3xH6P2yfZ+wK/127XZwA/RTNmd793AL+f5uLcd2vjeGf9aHijJ6Y57XrvJPdO8oi+5Z9PM/TjB5Lsu44+aDJNWh6aVG+nGdrrye1Zo3dOc8HWg6sZkvWzwMvTjI//WOCXB62kHSbkH4D/r13HT9PkrI2cyfUu4KVJ9k9yMM04/Es+TXPg6JQkd22fq/86iEsG5cL3A48CXkhzjTNJjWnLBav5K+Ckdl8pbb54at/B60HPcxDN/sB6/Wm7bX6eZtjuv2mHKnoX8Mokd2/3o/6AZltvxPk0n1+/2e7L/AbwEOADa9jum9mWmm7uD63fVOXDJI+kuX7ir1bV19awrtvlqw3EI62HeWj43gU8NckT2mNC22iOyfwDcBHNMaI/bI+hLNDkqdtdB3oN1vPdq99ajktJ42IeGo1Lgd9s95eO5keXOlqvrwL3zp6X+VnuGPxa1nVwkjtuMBaN2dwUzdpixc8Cf5PkUpoLrx/Yzt4bOBxYoPkl3JvTDJE1UlV1Jc01JD5F889zBM0QXb3+nqb48/FlHkNzMdR30VxQ9dvAW4B92zPTngQcR/Ornn8FXgUMuth8v3vQHNj5Js0p89+gGY8a4C+B77cxv5U9v2CttNxbgIekGU7k/YOedLVtUlV/QzPEx18Dt9AcaL5XO/v/A/64Xf+LaQ48/wvNL5euBC5cQ7+XnufrNGd8nNL24XBu/9pc1E7/ehvT0/vGsV1yOs21AD4OXENTSHxB+zxfprl+2jaaX5xcSt81A9ohIE+k+TXDOUnuvNZ+aLJMYh6aVO0BnWOBP6Ipol9Hc9bq0ufW8cBjaf4//yvN8LXfW2Z1z6Q50/R6mrHxX1bNdUDW609pcso1NPn2tuuOtQehfxl4EPBlYCfNNYMGeTnw1jZX/Xq7/L/RXHPyMOC9G4hNmknTlgtWU1Wfpfnl6Kk0+0o7gBOWaf4KmlxyDfB3wLtZvm+D/Gv7HNfT7KudVFVfaOe9gObA1ZeAT9LsV52+jnXfpn50Hd1tNK/DHwJPa/elYOXtvuFtqenl/tDGTGE+PBbYH/hkkl3t7YPLrGelfCUNnXloNKrqaprRM15Pc5zkl4FfruYaZt+nuebZU9p5bwSetZH/9XV+9+r3WuDpSb6ZxGupqjPmoZF6IU2O+BbN/tH7N7KSNj+9A/hSe/zm/ixzDH4Nq/socAXwr0m+vlpjdS97XpJptqS59tcHquphaa6rdXVVHTig3WnAhVV1Rvv4I8DJVfWZccYrafaYh8YjyTuBL1TVy7qOZaOS/AnwE1X1W13HIk2rWcgFy0nyXOC4qtroLyWlzrg/NH7Tkg/bs03eXlUHr9JU2hTzkKSumYek6TE3Z5pV1beBa9IMo0d7CuXSGT3vB/5DO/0AmtNgv9RFnJJml3loeNIMBfvjSe7Qnm5/LBv89dAkSHIvmqGRtncdizRNZi0X9EpyYJLHtX17MM2ZXO/rOi5ps9wfGo1ZzofSsJmHJHXNPCRNtpktmiV5B80Qfw9OsjPJc2hOyXxOks/RnBJ5bNv8Q8A3klwJfAz4f5YZZk+S1sw8NFL3o7nG4y7gdcBzq+ofO41og5L8J5ohlj7YXhxY0trNTC4Y4I40w7TcQjOcxzn86DoH0tRwf2hsZjkfSptiHpLUNfOQNF1menhGSZIkSZIkSZIkaS1m9kwzSZIkSZIkSZIkaa0smkmSJEmSJEmSJGnu7d11AMN2wAEH1KGHHjqUdX3nO9/hrne961DWNUlmtV9g37p28cUXf72q7tN1HJNgtVw0Da/nesxSf2apLzBb/VlLX8xDPzJveWjJLPZrFvsEs9mv73znO3zhC18wD7U28t1sEt4XkxDDpMRhDJMVx1pjcH/oR9aahybh9V2raYnVOIdrWuIE94f6zVIeMsbNm/T4YHZi3NT+UFXN1O3Rj350DcvHPvaxoa1rksxqv6rsW9eAz9YE5IFJuK2Wi6bh9VyPWerPLPWlarb6s5a+mIfmNw8tmcV+zWKfqmazXx/72MfMQ+vIQ8ttw65NQgxVkxGHMfzIJMSx1hjMQ+vPQ5Pw+q7VtMRqnMM1LXFWuT/Uf5ulPGSMmzfp8VXNToybyUMOzyhJkiRJkiRJkqS5Z9FMkiRJkiRJkiRJc8+imSRJkiRJkiZekv2SvDvJF5JcleSxXcckabYk2SvJPyb5wIB5SfK6JDuSfD7Jo7qIUdJoWTSTJEmSJK3Kg9WSJsBrgb+tqp8EHg5c1XE8kmbPC1k+tzwFOLy9nQi8aVxBSRofi2aSJEmSpLXwYLWkziS5B/ALwFsAqur7VfWtToOSNFOSHAw8FXjzMk2OBc6sxoXAfkkOHFuAksbCopkkSZIkaUUerJY0AR4IfA34X+3QaW9Octeug5I0U14D/CHww2XmHwRc1/N4ZztN0gzZu+sAkuwFfBb4SlU9rW9eaH7NeAzwXeCEqrpk/FFKkiRJ0lzrPVj9cOBi4IVV9Z3eRklOpBmuiC1btrC4uLiuJ9m1a9e6lxm2SYhhUuIwhsmKYxJi6NjewKOAF1TVRUleC5wM/L+9jTaSh6Zp205LrMY5XNMSJzSxTqMkTwNurKqLkyws12zAtBqwrpnMQ8a4eZMeHxgjTEDRjB+NE3uPAfN6x4k9imac2KPGF5okSZIkiTUerK6q7cB2gK1bt9bCwsK6nmRxcZH1LjNskxDDpMRhDJMVxyTE0LGdwM6quqh9/G6aPLSHjeShadq20xKrcQ7XtMQJTPzB9hU8DviVJMcAdwbukeTtVfVbPW12Aof0PD4YuL5/RbOah4xx8yY9PjBG6Lho1jNO7CuBPxjQ5LZxYoEL2wtPH1hVNwzj+Q89+bwV5287YjcnrNKm17WnPHWzIUmaM6vlofUyD0nS6sy90oas6WD1RvT+T673O9gg/k9Ks6mq/jXJdUkeXFVXA08ArhzGui/7ys2bzj39zEXSdKmqlwIvBWjPNHtxX8EM4Fzg+UnOpjmx4+ZhHac2D0mTo+szzV5DM07s3ZeZv9w4sUNJRpIkSZKk1Y3yYLUkrcMLgLOS3BH4EvA7HccjacYlOQmgqk4Dzqe5jNAOmksJmYOkGdRZ0azrcWKh+RXjSrbsu3qbXtNy+vE0jEu6UfZNkiRJGhkPVkvqVFVdCmztOg5Js62qFoHF9v5pPdMLeF43UUkaly7PNOt0nFhg1VNetx2xm1dftvZNdO3xa3verk3DuKQbZd8kSZKk0fBgtSRJkqRZd4eunriqXlpVB1fVocBxwEeXGSf2WWk8hiGOEytJkiRJkiRJkiQt6fqaZrfjOLGSJEmSJEmSJEkat4komjlOrCRJkiRJkiRJkrrU2fCMkiRJkiRJkiRJ0qSwaCZp7iXZK8k/JvnAgHlJ8rokO5J8PsmjuohRkiRJkiRJkjRaFs0kCV4IXLXMvKcAh7e3E4E3jSsoSZIkSZIkSdL4WDSTNNeSHAw8FXjzMk2OBc6sxoXAfkkOHFuAkiRJkiRJkqSxsGgmad69BvhD4IfLzD8IuK7n8c52miRJkiRJkiRphuzddQCS1JUkTwNurKqLkyws12zAtFpmfSfSDOHIli1bWFxcXPa5d+3axeLiItuO2L2ekFe10nOO0lJ/ZsEs9QVmqz+z1BdJkiRJkiRNHotmkubZ44BfSXIMcGfgHkneXlW/1dNmJ3BIz+ODgesHrayqtgPbAbZu3VoLCwvLPvHi4iILCwuccPJ5m+tBn2uPX/45R2mpP7NglvoCs9WfWeqLJEmSJEmSJo/DM0qaW1X10qo6uKoOBY4DPtpXMAM4F3hWGo8Bbq6qG8YdqyRJkiRJkiRptDzTTJL6JDkJoKpOA84HjgF2AN8FfqfD0CRJkiRJkiRJI2LRTJKAqloEFtv7p/VML+B53UQlSZIkSZIkSRoXh2eUJEmSJEmSJEnS3LNoJkmSJEmSJEmSpLln0UySJEmSJEmSJElzz6KZJEmSJEmSJEmS5p5FM0mSpDVI8vtJrkhyeZJ3JLlzknsluSDJF9u/+/e0f2mSHUmuTvLknumPTnJZO+91SdJOv1OSd7bTL0pyaM8yz26f44tJnj3WjkuSJEnSjGu/3306yefa731/OqDNQpKbk1za3v6ki1gljZZFM0mSpFUkOQj4PWBrVT0M2As4DjgZ+EhVHQ58pH1Mkoe08x8KHA28Mcle7ereBJwIHN7ejm6nPwf4ZlU9CPhL4FXtuu4FvAw4CjgSeFlvcU6SJEmStGnfAx5fVQ8HHgEcneQxA9p9oqoe0d5eMdYIJY1FZ0Uzq/eSJGnK7A3sm2Rv4C7A9cCxwFvb+W8FfrW9fyxwdlV9r6quAXYARyY5ELhHVX2qqgo4s2+ZpXW9G3hCexbak4ELquqmqvomcAE/KrRJkiRJkjapGrvah/u0t+owJEkd2bvD516q3u9Ksg/wySQfrKoL+9p9oqqe1kF8kiRJAFTVV5L8D+DLwL8BH66qDyfZUlU3tG1uSHLfdpGDgN59mp3ttB+09/unLy1zXbuu3UluBu7dO33AMpIkSZKkIWhHB7kYeBDwhqq6aECzxyb5HM2PKF9cVVcMWM+JNKOLsGXLFhYXF1d97i37wrYjdm8i+ttby/Oux65du4a+zmGb9BgnPT4wRuiwaNb+utrqvSRJmnjtcIjHAocB3wL+JslvrbTIgGm1wvSNLtMf55q/nE3DjvBGrKVfk/5ltN88v1bTZteuXas3kiRJ0kSqqluBRyTZD3hfkodV1eU9TS4BHtCeBHIM8H6aIff717Md2A6wdevWWlhYWPW5X3/WObz6suEeqr/2+NWfdz0WFxdZS1+6NOkxTnp8YIzQ7ZlmQ6veS5IkjdgTgWuq6msASd4L/Czw1SQHtmeZHQjc2LbfCRzSs/zBNPsyO9v7/dN7l9nZDgF5T+CmdvpC3zKLg4Jcz5ezpZ3MQ08+b6V+r9u1pzx1qOtbr7XsPJ8w7D4P+ctov2n40rIRs9ivWSsCSpIkzaOq+laSRZph8S/vmf7tnvvnJ3ljkgOq6usdhClpRDotmg2rer+RU15h9V8Zr/e02Gn5kjyLv+pdYt8kSSPyZeAxSe5CMzzjE4DPAt8Bng2c0v49p21/LvDXSf4CuD/N/sunq+rWJLe0F5S+CHgW8PqeZZ4NfAp4OvDRqqokHwL+W3u2G8CTgJeOtLeSJEmSNEeS3Af4QVsw25fmh5Ov6mtzP+Cr7fe0I4E7AN8Yf7SSRqnTotmSzVbvN3LKK6z+K+NtR+xe12mxo/6V8bDM4q96l9g3SdIoVNVFSd5N84Oe3cA/0ux73A14V5Ln0BTWntG2vyLJu4Ar2/bPa38sBPBc4AxgX+CD7Q3gLcDbkuygOcPsuHZdNyX5M+AzbbtXVNVNI+yuJEmSJM2bA4G3tiOj3QF4V1V9IMlJAFV1Gs2PG5+bZDfNjymPay9BJGmGdFY0s3ovSZKmSVW9DHhZ3+Tv0Zx1Nqj9K4FXDpj+WeBhA6b/O23RbcC804HT1xmyJA1VkmuBW4Bbgd1VtbXbiCTNI3ORpFGoqs8Djxww/bSe+6cCp44zLknj1+WZZlbvJUmSJGm6/Aev2yFpApiLJEnSSHRWNLN6L0mSJEmSJEmSpElxh64DkCRJkiRNhQI+nOTiJCd2HYykuWUukiRJI9Pl8IyS1KkkdwY+DtyJJh++u71mUW+bBeAc4Jp20nur6hVjDFOSJGlSPK6qrk9yX+CCJF+oqo/3NmgPYJ8IsGXLFhYXF1dd6bYjdt92f8u+ez7eiLU850p27dq16XUMwyTEYQyTFcckxDAhVsxFG8lDw8g9/Ub1Wk3L+8A4h2ta4oQmVkmaZhbNJM2z7wGPr6pdSfYBPpnkg1V1YV+7T1TV0zqIT5IkaWJU1fXt3xuTvA84kuYHSL1ttgPbAbZu3VoLCwurrveEk8+77f62I3bz6ss29zX12uNXf86VLC4uspa4R20S4jCGyYpjEmKYBKvloo3kodefdc6mc0+/zeai5UzL+8A4h2ta4oTRFYwlaVwcnlHS3KrG0k+g9mlv1WFIkiRJEynJXZPcfek+8CTg8m6jkjRvzEWSJGnUPNNM0lxLshdwMfAg4A1VddGAZo9N8jngeuDFVXXFOGOUJEmaAFuA9yWB5nvkX1fV33YbkqQ5ZC6SJEkjZdFM0lyrqluBRyTZj+bL18OqqveXipcAD2iHcDwGeD9w+KB1rWfs/KXxyKdl3PzVTNP46quZpb7AbPVnlvoiSdOmqr4EPLzrOCTNN3ORJEkaNYtmkgRU1beSLAJH0zO8R1V9u+f++UnemOSAqvr6gHWseez8pfHIe6/hMQyjGjd/NdM0vvpqZqkvMFv9maW+SJIkSZIkafJ4TTNJcyvJfdozzEiyL/BE4At9be6XduyPJEfS5M1vjDlUSZIkSZIkSdKIeaaZpHl2IPDW9rpmdwDeVVUfSHISQFWdBjwdeG6S3cC/AcdVVXUWsSRJkiRJkiRpJCyaSZpbVfV54JEDpp/Wc/9U4NRxxiVJkiRJkiRJGj+HZ5QkSZIkSZIkSdLcs2gmSZIkSZIkSZKkuWfRTJIkSZIkSZIkSXPPopkkSZIkSZIkSZLmnkUzSZIkSZIkSZIkzT2LZpIkSZIkSZKkuZXkzkk+neRzSa5I8qcD2iTJ65LsSPL5JI/qIlZJo7V31wFIkiRJkiRJktSh7wGPr6pdSfYBPpnkg1V1YU+bpwCHt7ejgDe1fyXNkM7ONLN6L0mSJEmSJEnqWjV2tQ/3aW/V1+xY4My27YXAfkkOHGeckkavy+EZl6r3DwceARyd5DF9bXqr9yfSVO8lSZIkSZIkSRqaJHsluRS4Ebigqi7qa3IQcF3P453tNEkzpLPhGauqgDVX74ELk+yX5MCqumGMoUqSJEmSJEmSZlhV3Qo8Isl+wPuSPKyqLu9pkkGL9U9IciLNCSBs2bKFxcXFVZ97y76w7YjdGwl7WWt53vXYtWvX0Nc5bJMe46THB8YIHV/TLMlewMXAg4A3rKN6b9FMkiRJkiRJkjRUVfWtJIvA0UBv0WwncEjP44OB6wcsvx3YDrB169ZaWFhY9Tlff9Y5vPqy4R6qv/b41Z93PRYXF1lLX7o06TFOenxgjNBx0azL6j2sXr1fb4V/0iuwS6ahWrxR9k2SJEmSJEnSeiS5D/CDtmC2L/BE4FV9zc4Fnp/kbOAo4GZHRJNmT6dFsyVdVO8BTjj5vBXnbzti97oq/MOu3o/KNFSLN8q+SZIkSZIkSVqnA4G3tiOj3QF4V1V9IMlJAFV1GnA+cAywA/gu8DtdBStpdDormlm9lyRJkiRJkiR1rao+DzxywPTTeu4X8LxxxiVp/Lo808zqvSRJkiRJkiRJkiZCZ0Uzq/eSupbkzsDHgTvR5MN3V9XL+toEeC1NAf+7wAlVdcm4Y5UkSZIkSZIkjdZEXNNMkjryPeDxVbUryT7AJ5N8sKou7GnzFODw9nYU8Kb2ryRJkiRJkiRphtyh6wAkqSvV2NU+3Ke9VV+zY4Ez27YXAvslOXCccUqSJEmSJEmSRs+imaS5lmSvJJcCNwIXVNVFfU0OAq7rebyznSZJkiRJkiRJmiEOzyhprlXVrcAjkuwHvC/Jw6rq8p4mGbTYoHUlORE4EWDLli0sLi4u+7y7du1icXGRbUfs3mjoA630nKO01J9ZMEt9gdnqzyz1RZIkSZIkSZPHopkkAVX1rSSLwNFAb9FsJ3BIz+ODgeuXWcd2YDvA1q1ba2FhYdnnW1xcZGFhgRNOPm9zgfe59vjln3OUlvozC2apLzBb/ZmlvkiSJEmSJGnyODyjpLmV5D7tGWYk2Rd4IvCFvmbnAs9K4zHAzVV1w3gjlSRJkiRJkiSNmmeaSZpnBwJvTbIXzY8I3lVVH0hyEkBVnQacDxwD7AC+C/xOV8FKkiR1rd1v+izwlap6WtfxSJo/5iFJkjRKFs0kza2q+jzwyAHTT+u5X8DzxhmXJEnSBHshcBVwj64DkTS3zEOSJGlkHJ5RkiRJkrSqJAcDTwXe3HUskuaTeUiSJI2aRTNJkqQ1SLJfkncn+UKSq5I8Nsm9klyQ5Ivt3/172r80yY4kVyd5cs/0Rye5rJ33uiRpp98pyTvb6RclObRnmWe3z/HFJM8ea8cl6UdeA/wh8MOO45A0v16DeUiSJI2QwzNKkiStzWuBv62qpye5I3AX4I+Aj1TVKUlOBk4GXpLkIcBxwEOB+wN/l+QnqupW4E3AicCFNNdNPBr4IPAc4JtV9aAkxwGvAn4jyb2AlwFbgQIuTnJuVX1zfF2XNO+SPA24saouTrKwQrsTaXIcW7ZsYXFxcdV1bzti9233t+y75+ONWMtzrmTXrl2bXscwTEIcxjBZcUxCDF0aZR4aRu7pN6rXalreB8Y5XNMSJzSxStI0s2gmSZK0iiT3AH4BOAGgqr4PfD/JscBC2+ytwCLwEuBY4Oyq+h5wTZIdwJFJrgXuUVWfatd7JvCrNEWzY4GXt+t6N3Bqexbak4ELquqmdpkLaApt7xhVfyVpgMcBv5LkGODOwD2SvL2qfqu3UVVtB7YDbN26tRYWFlZd8Qknn3fb/W1H7ObVl23ua+q1x6/+nCtZXFxkLXGP2iTEYQyTFcckxNCxkeWh1591zqZzT7/N5qLlTMv7wDiHa1rihNEVjCVpXCyaSZIkre6BwNeA/5Xk4cDFNBeh31JVNwBU1Q1J7tu2P4jmTLIlO9tpP2jv909fWua6dl27k9wM3Lt3+oBl9rCeX1Yv/Vp1Wn5VvVZr+RXutPV5mn5ZvB6z2K9Z/mV1Vb0UeClAe4bHi/sPVEvSKJmHJEnSOFg0kyRJWt3ewKOAF1TVRUleSzMU43IyYFqtMH2jy+w5cR2/rF76tWrvGR7DMKpfVa/VWn6FO219nqZfFq/HLPZr1oqAkiRJkjRv7tB1AJIkSVNgJ7Czqi5qH7+bpoj21SQHArR/b+xpf0jP8gcD17fTDx4wfY9lkuwN3BO4aYV1SVInqmqxqp7WdRyS5pd5SJIkjYpFM0mSpFVU1b8C1yV5cDvpCcCVwLnAs9tpzwbOae+fCxyX5E5JDgMOBz7dDuV4S5LHtNcre1bfMkvrejrw0aoq4EPAk5Lsn2R/4EntNEmSJEnSECQ5JMnHklyV5IokLxzQZiHJzUkubW9/0kWskkbL4RklSZLW5gXAWUnuCHwJ+B2aHyC9K8lzgC8DzwCoqiuSvIumsLYbeF5V3dqu57nAGcC+wAfbG8BbgLcl2UFzhtlx7bpuSvJnwGfadq+oqptG2VFJkiRJmjO7gW1VdUmSuwMXJ7mgqq7sa/cJz3SVZltnRbMkhwBnAvcDfghsr6rX9rVZoPn19TXtpPdW1SvGGKYkSRIAVXUpsHXArCcs0/6VwCsHTP8s8LAB0/+dtug2YN7pwOnrCFeSJEmStEbtqCA3tPdvSXIVcBDNDyElzZEuzzSzei9JkiRJkiRJmhhJDgUeCVw0YPZjk3yO5jrTL66qK8YZm6TR66xoZvVekiRJkiRJkjQpktwNeA/woqr6dt/sS4AHVNWuJMcA76e5fnX/Ok4ETgTYsmULi4uLqz7vln1h2xG7Nxd8n7U873rs2rVr6OsctkmPcdLjA2OECbmmmdV7SZIkSZIkSVJXkuxDUzA7q6re2z+/t4hWVecneWOSA6rq633ttgPbAbZu3VoLCwurPvfrzzqHV1823EP11x6/+vOux+LiImvpS5cmPcZJjw+MESagaNZV9R5Wr96vt8I/6RXYJdNQLd4o+yZJkiRJkiRpPZIEeAtwVVX9xTJt7gd8taoqyZHAHYBvjDFMSWPQadGsy+o9wAknn7fi/G1H7F5XhX/Y1ftRmYZq8UbZN0mSJEmSJEnr9Djgt4HLklzaTvsj4McAquo04OnAc5PsBv4NOK6qqoNYJY1QZ0Uzq/eSJEmSJEmSpK5V1SeBrNLmVODU8UQkqStdnmlm9V5Sp5IcApwJ3A/4IbC9ql7b12YBOAe4pp303qp6xRjDlCRJkiRJkiSNQWdFM6v3kibAbmBbVV2S5O7AxUkuqKor+9p9oqqe1kF8kiRJkiRJkqQxuUPXAUhSV6rqhqq6pL1/C3AVcFC3UUmSJEmSJEmSutDl8IySNDGSHAo8ErhowOzHJvkccD3w4qq6Ypl1nAicCLBlyxYWFxeXfb5du3axuLjItiN2bzLyPa30nKO01J9ZMEt9gdnqzyz1RZIkSZIkSZPHopmkuZfkbsB7gBdV1bf7Zl8CPKCqdiU5Bng/cPig9VTVdmA7wNatW2thYWHZ51xcXGRhYYETTj5v8x3oce3xyz/nKC31ZxbMUl9gtvozS32RJEmSJEnS5HF4RklzLck+NAWzs6rqvf3zq+rbVbWrvX8+sE+SA8YcpiRJkiRJkiRpxCyaSZpbSQK8Bbiqqv5imTb3a9uR5EiavPmN8UUpSZIkSZIkSRoHh2eUNM8eB/w2cFmSS9tpfwT8GEBVnQY8HXhukt3AvwHHVVV1EKskSZIkSZIkaYQsmkmaW1X1SSCrtDkVOHU8EUmSJEmSJEmSuuLwjJIkSZIkSZIkSZp7Fs0kSZIkSZIkSZI09yyaSZIkSZIkSZIkae5ZNJMkSZIkSZIkSdLcs2gmSZIkSZIkSZKkuWfRTJIkSZIkSZIkSXPPopkkSZIkSZIkSZLmnkUzSZIkSZIkSZIkzT2LZpIkSZKkFSW5c5JPJ/lckiuS/GnXMUmaL+YhSaOU5JAkH0tyVZtjXjigTZK8LsmOJJ9P8qguYpU0Wnt3HYAkSZIkaeJ9D3h8Ve1Ksg/wySQfrKoLuw5M0twwD0kapd3Atqq6JMndgYuTXFBVV/a0eQpweHs7CnhT+1fSDOnsTDOr95IkSZI0Haqxq324T3urDkOSNGfMQ5JGqapuqKpL2vu3AFcBB/U1OxY4s81HFwL7JTlwzKFKGrEuh2dcqt7/FPAY4HlJHtLXprd6fyJN9V6SJEmSNGZJ9kpyKXAjcEFVXdRxSJLmjHlI0jgkORR4JNCfYw4Crut5vJPbF9YkTbnOhmesqhuAG9r7tyRZqt73nvJ6W/UeuDDJfkkObJeVJEmSJI1JVd0KPCLJfsD7kjysqi7vbZPkRJofPLJlyxYWFxdXXe+2I3bfdn/Lvns+3oi1POdKdu3atel1DMMkxGEMkxXHJMTQtVHloWHknn6jeq2m5X1gnMM1LXFCE+s0S3I34D3Ai6rq2/2zByxyuzNeZzUPTcP7cNJjnPT4wBhhQq5ptoHq/R5Fs40kIlg9Ea03WU36m2nJNLzxN8q+SZIkSaNVVd9KsggcDVzeN287sB1g69attbCwsOr6Tjj5vNvubztiN6++bHNfU689fvXnXMni4iJriXvUJiEOY5isOCYhhkkx7Dz0+rPO2XTu6bfZXLScaXkfGOdwTUucMD3HRwdpr5f4HuCsqnrvgCY7gUN6Hh8MXN/faFbz0DS8Dyc9xkmPD4wRJqBoNozq/UYSEez55WyQ9X5hG9UO0bBNwxt/o+ybJEmSNHxJ7gP8oD1QvS/wROBVHYclaY6YhySNUpIAbwGuqqq/WKbZucDzk5wNHAXc7Iho0uzptGg2rOq9JG1EkkOAM4H7AT8EtlfVa/vaBHgtcAzwXeCEpQvDSpIkzZEDgbcm2Yvm2tjvqqoPdByTpPliHpI0So8Dfhu4rL12IsAfAT8GUFWnAefTHB/aQXOM6HfGH6akUeusaGb1XtIE2A1sq6pLktwduDjJBVXVe23FpwCHt7ejgDe1fyVJkuZGVX2eZkh9SeqEeUjSKFXVJxk86llvmwKeN56IJHWlyzPNrN5L6lRbhL+hvX9LkqtorpvYWzQ7Fjiz3TG6MMl+SQ60gC9JkiRJkiRJs6WzopnVe0mTJMmhNL9avKhv1kHAdT2Pd7bTLJpJkiRJkiRJ0gzp9JpmkjQJktyN5vqKL6qqb/fPHrBILbOeE4ETAbZs2cLi4uKyz7lr1y4WFxfZdsTuDcW8nJWec5SW+jMLZqkvMFv9maW+SJIkSZIkafJYNJM015LsQ1MwO6uq3jugyU7gkJ7HBwPXD1pXVW0HtgNs3bq1FhYWln3excVFFhYWOOHk8zYY+WDXHr/8c47SUn9mwSz1BWarP7PUF0mSJEmSJE2eO3QdgCR1JUmAtwBXVdVfLNPsXOBZaTwGuNnrmUnzK8leSf4xyQfax/dKckGSL7Z/9+9p+9IkO5JcneTJPdMfneSydt7r2lxEkjsleWc7/aJ22NilZZ7dPscXkzx7jF2WJEmSJEmaGxbNJM2zxwG/DTw+yaXt7ZgkJyU5qW1zPvAlYAfwV8B/7ihWSZPhhcBVPY9PBj5SVYcDH2kfk+QhwHHAQ4GjgTcm2atd5k00Q7ke3t6Obqc/B/hmVT0I+EvgVe267gW8DDgKOBJ4WW9xTpIkSZIkScPh8IyS5lZVfZLB1yzrbVPA88YTkaRJluRg4KnAK4E/aCcfCyy0998KLAIvaaefXVXfA65JsgM4Msm1wD2q6lPtOs8EfhX4YLvMy9t1vRs4tT0L7cnABVV1U7vMBTSFtneMpqeSJEmSJEnzyTPNJEmS1uY1wB8CP+yZtmVpyNb2733b6QcB1/W029lOO6i93z99j2WqajdwM3DvFdYlSZIkSZKkIfJMM0mSpFUkeRpwY1VdnGRhLYsMmFYrTN/oMns+aXIizdCPbNmyhcXFxWUD3LVrF4uLi2w7YveybTZipecch6V+rWTa+ryWPk2jWezXrl27ug5BkiRJkrQJFs0kSZJW9zjgV5IcA9wZuEeStwNfTXJgVd2Q5EDgxrb9TuCQnuUPBq5vpx88YHrvMjuT7A3cE7ipnb7Qt8zioCCrajuwHWDr1q21sLAwqBnQFHoWFhY44eTzVuz4el17/PLPOQ5L/VrJtPV5LX2aRrPYr1krAkqSJEnSvHF4RkmSpFVU1Uur6uCqOhQ4DvhoVf0WcC7w7LbZs4Fz2vvnAscluVOSw4DDgU+3QzjekuQx7fXKntW3zNK6nt4+RwEfAp6UZP8k+wNPaqdJkiRJkiRpiDzTTJIkaeNOAd6V5DnAl4FnAFTVFUneBVwJ7AaeV1W3tss8FzgD2Bf4YHsDeAvwtiQ7aM4wO65d101J/gz4TNvuFVV106g7JkmSJEmSNG8smkmSJK1DVS3SDo9YVd8AnrBMu1cCrxww/bPAwwZM/3faotuAeacDp280ZkmSJEmSJK3O4RklSZIkSZIkSZI09zzTTJIkSUNz6MnnDXV9157y1KGuT5IkSZIkaTmeaSZJkiRJkiRJkqS5Z9FMkiRJkiRJkiRJc8+imSRJkiRJkiRpbiU5PcmNSS5fZv5CkpuTXNre/mTcMUoaj06LZiYjSZIkSZIkSVLHzgCOXqXNJ6rqEe3tFWOISVIH9u74+c8ATgXOXKHNJ6rqaeMJR5IkSZIkSZI0T6rq40kO7ToOSd3r9Eyzqvo4cFOXMUiSJEmSJEmStIrHJvlckg8meWjXwUgaja7PNFuLxyb5HHA98OKquqLrgCRJkiRJkiRJc+MS4AFVtSvJMcD7gcMHNUxyInAiwJYtW1hcXFx15Vv2hW1H7B5asMCannc9du3aNfR1Dtukxzjp8YExwuQXzdaUjDaSiGD1RLTeZDXpb6Yl0/DG3yj7pvVKcjrwNODGqnrYgPkLwDnANe2k9zputSRJkiRJ0vyoqm/33D8/yRuTHFBVXx/QdjuwHWDr1q21sLCw6vpff9Y5vPqy4R6qv/b41Z93PRYXF1lLX7o06TFOenxgjDDhRbO1JqONJCKAE04+b8X5247Yva5kNexEdOgq8a3Xtac8FZiON/5G2TdtwBl4bUVJkiRJkiQtI8n9gK9WVSU5kuayR9/oOCxJIzDRRTOTkaRR80KvkiRJkiRJ8y3JO4AF4IAkO4GXAfsAVNVpwNOB5ybZDfwbcFxVVUfhShqhTotmJiNJU8JrK0qSJEmSJM2oqnrmKvNPpRmpSNKM67RoZjKSNAVGcqHXpWvUTfpFXtdqlq65N0t9gdnqzyz1RZKmTZJDaIazvh/wQ2B7Vb2226gkzRPzkCRJGoeJHp5Rkro2qgu9Ll2jbrVrK67XsK+tuFazdM29WeoLzFZ/ZqkvkjSFdgPbquqSJHcHLk5yQVVd2XVgkuaGeUiSJI3cHboOQJImWZL7JUl732srSpKkuVRVN1TVJe39W4CrgIO6jUrSPDEPSZKkcfBMM0lzzWsrSpIkrU+SQ4FHAhd1HIqkOWUekiRJo2LRTNJc89qKkiRJa5fkbsB7gBf1DmPdM3/N13hd0nuN1y37sulrvm72+peTcg3NSYjDGCYrjkmIYRKMIg8NI/f0G9VrNS3vA+McrmmJE5pYJWmaWTSTpBly6LCvkXbKU4e6PkmSNL2S7ENzoPqsqnrvoDbrucbrkt5rvG47YjevvmxzX1M3e43XSbmG5iTEYQyTFcckxNC1UeWh1591zqZzT79RXW96Wt4Hxjlc0xInjK5gLEnj4jXNJEmSJEkraq/x+hbgqqr6i67jkTR/zEOSJGkcLJpJkiRJklbzOOC3gccnubS9HdN1UJLminlIkiSNnMMzSpIkSZJWVFWfBNJ1HJLml3lIkiSNg2eaSZIkSZIkSZIkae5ZNJMkSZIkSZIkSdLcs2gmSZIkSZIkSZKkuWfRTJIkSZIkSZIkSXPPopkkSZIkSZIkSZLmnkUzSZIkSZIkSZIkzT2LZpIkSZIkSZIkSZp7Fs0kSZIkSZIkSZI09yyaSZIkSZIkSZLmVpLTk9yY5PJl5ifJ65LsSPL5JI8ad4ySxqPTopnJSJIkSZIkSZLUsTOAo1eY/xTg8PZ2IvCmMcQkqQNdn2l2BiYjSZIkSZIkSVJHqurjwE0rNDkWOLMaFwL7JTlwPNFJGqdOi2YmI0mSJEmSJEnShDsIuK7n8c52mqQZs3fXAaxiuWR0QzfhSJo1SU4HngbcWFUPGzA/wGuBY4DvAidU1SXjjVKSJEmSJEkdyoBpNbBhciLNqGls2bKFxcXFVVe+ZV/YdsTuzcR3O2t53vXYtWvX0Nc5bJMe46THB8YIk180W1My2kgigtUT0XqT1bBfqFElyml442+UfdMGnAGcCpy5zPzeYWKPohkm9qixRCZJkiRJkqRJsBM4pOfxwcD1gxpW1XZgO8DWrVtrYWFh1ZW//qxzePVlwz1Uf+3xqz/veiwuLrKWvnRp0mOc9PjAGGHyi2ZrSkYbSUQAJ5x83orztx2xe13JatiJaLX41mspvml442+UfdN6VdXHkxy6QpPbhokFLkyyX5IDq8ozXiVJkiRJkubDucDzk5xN82Pqmz02JM2mTq9ptgbnAs9K4zGYjCSNn2NWSyLJIUk+luSqJFckeWE7/V5JLkjyxfbv/j3LvDTJjiRXJ3lyz/RHJ7msnfe6dhhYktwpyTvb6Rf1FvSTPLt9ji8mefYYuy5JkiRJMy/JO4BPAQ9OsjPJc5KclOSktsn5wJeAHcBfAf+5o1AljVinZ5q1yWgBOCDJTuBlwD4AVXUaTTI6hiYZfRf4nW4ilTTHRjJm9dJwm8MehnXY1jok6CwNHzpLfYHZ6k/HfdkNbKuqS5LcHbg4yQXACcBHquqUJCcDJwMvSfIQ4DjgocD9gb9L8hNVdSvNMK8nAhfS7OscDXwQeA7wzap6UJLjgFcBv5HkXjT7SFtp8s/FSc6tqm+OrfeSJEmSNMOq6pmrzC/geWMKR1KHOi2amYwkTYGRjFm9NNzmsIdhHba1Djs7S8OHzlJfYLb602Vf2jPdb2jv35LkKpqzTo+l+QEQwFuBReAl7fSzq+p7wDVJdgBHJrkWuEdVfQogyZnAr9IUzY4FXt6u693Aqe1ZaE8GLqiqm9plLqAptL1jZB2WJEmSJEmaQ5M+PKMkdc1hYiXtoR028ZHARcCWpZzQ/r1v22y5oV0Pau/3T99jmaraDdwM3HuFdUmSJEmSJGmIOj3TTJK65jCxktYjyd2A9wAvqqpvt5cjG9h0wLRaYfpGl+mPb26HiV2ylmE8h93nUQ8bOkvDrPaaxX7t2rWr6xAkSZIkSZtg0UzSXHOYWElrlWQfmoLZWVX13nbyV5McWFU3JDkQuLGdvtzQrjvb+/3Te5fZmWRv4J7ATe30hb5lFgfFOM/DxC5ZyzCew+7zemNcr1kaZrXXLPZr1oqAkiRJkjRvHJ5RkiRpFe21xd4CXFVVf9Ez61zg2e39ZwPn9Ew/LsmdkhwGHA58uh3C8ZYkj2nX+ay+ZZbW9XTgo23h/kPAk5Lsn2R/4EntNEmSJEmSJA2RZ5pJkiSt7nHAbwOXJbm0nfZHwCnAu5I8B/gy8AyAqroiybuAK4HdwPOq6tZ2uecCZwD7Ah9sb9AU5d6WZAfNGWbHteu6KcmfAZ9p272iqm4aUT8lSZIkSZLmlkUzSZKkVVTVJxl8bTGAJyyzzCuBVw6Y/lngYQOm/ztt0W3AvNOB09caryRJkiRJktbP4RklSZIkSZIkSZI09yyaSZIkSZIkSZIkae5ZNJMkSZIkrSrJ6UluTHJ517FImk/mIUmSNGoWzSRJkiRJa3EGcHTXQUiaa2dgHpIkSSNk0UySJEmStKqq+jhwU9dxSJpf5iFJkjRqFs0kSZIkSZIkSZI09/buOgBJkiRJ0mxIciJwIsCWLVtYXFxcdZltR+y+7f6Wffd8vBFrec6V7Nq1a9PrGIZJiMMYJiuOSYhhGmwkDw0j9/Qb1Ws1Le8D4xyuaYkTmlglaZpZNJMkSZIkDUVVbQe2A2zdurUWFhZWXeaEk8+77f62I3bz6ss29zX12uNXf86VLC4uspa4R20S4jCGyYpjEmKYBhvJQ68/65xN555+m81Fy5mW94FxDte0xAmjKxhL0rg4PKMkSZIkSZIkSZLmnmeaSZIkSRPk0J6zbqA58+aEvmnrde0pT93U8hJAkncAC8ABSXYCL6uqt3QblaR5Yh6SJEmjZtFMkiRJkrSqqnpm1zFImm/mIUmjlORo4LXAXsCbq+qUvvkLwDnANe2k91bVK8YZo6TR63R4xiRHJ7k6yY4kJw+Yv5Dk5iSXtrc/6SJOSZIkSZIkSdJsSrIX8AbgKcBDgGcmeciApp+oqke0Nwtm0gzqrGhmIpI0CSzeS5IkSZIkzb0jgR1V9aWq+j5wNnBsxzFJ6kCXZ5qZiCR1yuK9JEmSJEmSgIOA63oe72yn9Xtsks8l+WCSh44nNEnj1OU1zQYloqMGtHtsks8B1wMvrqorxhGcpLlwW/EeIMlS8f7KTqOSJEmSJEnSOGXAtOp7fAnwgKraleQY4P3A4bdbUXIicCLAli1bWFxcXPXJt+wL247Yvc6QV7aW512PXbt2DX2dwzbpMU56fGCM0G3RrNNEBKsnovUmq2G/UKNKlNPwxt8o+6Z1sngvSZIkSZKkncAhPY8PpjkOdJuq+nbP/fOTvDHJAVX19b5224HtAFu3bq2FhYVVn/z1Z53Dqy8b7qH6a49f/XnXY3FxkbX0pUuTHuOkxwfGCN0WzTpNRAAnnHzeivO3HbF7Xclq2IlotfjWaym+aXjjb5R90zoNrXgP6yvgLxVBh10cH7a1Fmpnqag7S32B2erPLPVFkiRJkjRRPgMcnuQw4CvAccBv9jZIcj/gq1VVSY6kufTRN8YeqaSR6rJoZiKSRuDQIRdbAa495alDX+eEGFrxvp2/5gL+UhF02MXxYVvrjwFmqag7S32B2erPLPVFkiRJkjQ5qmp3kucDHwL2Ak6vqiuSnNTOPw14OvDcJLuBfwOOq6r+H19LmnKdFc1MRJImgMV7SZIkSZIkUVXnA+f3TTut5/6pwKnjjkvSeHV5ppmJSFKnLN5LkiRJkiRJkpZ0WjSTpK5ZvJckSZIkSZIkgUUzSZIkSTNo2Nd5neFrvEqSJEmSWnfoOgBJkiRJkiRJkiSpaxbNJEmSJEmSJEmSNPcsmkmSJEmSJEmSJGnuWTSTJEmSJEmSJEnS3LNoJkmSJEmSJEmSpLln0UySJEmSJEmSJElzz6KZJEmSJEmSJEmS5p5FM0mSJEmSJEmSJM09i2aSJEmSJEmSJEmaexbNJEmSJEmSJEmSNPcsmkmSJEmSJEmSJGnuWTSTJEmSJEmSJEnS3LNoJkmSJEmSJEmSpLm3d9cBSJIm16Enn7emdtuO2M0Ja2x77SlP3UxIkiRJkiRJkjQSFs0kSdJQrbXYuh4WWyVJkiRJkjRqnQ7PmOToJFcn2ZHk5AHzk+R17fzPJ3lUF3FKml3mIUnTYrV8JUmjZh6S1DXzkKRR8hiRJOiwaJZkL+ANwFOAhwDPTPKQvmZPAQ5vbycCbxprkJJmmnlI0rRYY76SpJExD0nqmnlI0ih5jEjSki6HZzwS2FFVXwJIcjZwLHBlT5tjgTOrqoALk+yX5MCqumH84UqaQeYhSdNiLflqJq13uM/1XGNRk2MUw7pq6OY2D0maGFOVh4b92eZw5dLIeYxIEtBt0ewg4LqexzuBo9bQ5iBgj0SU5ESa6j7AriRXDyPA34MDgK+vtX1eNYxnHZ2e+NbVrylj30ZgHe/tB4wwjFEYWh6CdeeimXqvridfTnquZMZeG2akP+37Zi19mbY8tFZryVdznYeWrHf/bRhGndeG0acJzb1T9R5c4zY8APPQpr6bTcj7fVLem5MQhzH8yCTEsdYYzEPrz0OT8PquaAqP6RjncE1LnDC9+0NdH6se+ms8gu8A0/A+nPQYJz0+mJ0YN5yHuiyaZcC02kAbqmo7sH0YQe3x5Mlnq2rrsNfbtVntF9g3rdvQ8hCsLxfN2us5S/2Zpb7AbPVnlvqyAUPfJ5rV7TmL/ZrFPsFs9qvt06FdxzEiY/luNgnvi0mIYVLiMIbJimMSYujYyPLQNG3baYnVOIdrWuKEqd4f6vRY9TS8xsa4eZMeHxgjdHhNM5pK/CE9jw8Grt9AG0naKPOQpGlhLpLUNfOQpK6ZhySNkseIJAHdFs0+Axye5LAkdwSOA87ta3Mu8Kw0HgPc7BixkobIPCRpWqwlX0nSKJmHJHXNPCRplDxGJAnocHjGqtqd5PnAh4C9gNOr6ookJ7XzTwPOB44BdgDfBX5nzGEOfcjHCTGr/QL7pnXoOA/N2us5S/2Zpb7AbPVnlvqyLsvlq02udla35yz2axb7BLPZr1nsEzCyPDTIJGzDSYgBJiMOY/iRSYhjEmLozIjz0DRt22mJ1TiHa1rihOmK9TYTcKx6GrabMW7epMcHxkiqBl6aR5IkSZIkSZIkSZobXQ7PKEmSJEmSJEmSJE0Ei2aSJEmSJEmSJEmae3NZNEuyX5J3J/lCkquSPLZvfpK8LsmOJJ9P8qi++Xsl+cckHxhv5KvbTN9WW7Zrm+zb7ye5IsnlSd6R5M7j78Hy1tC3n0zyqSTfS/LivnlHJ7m67ffJ441c6zVpr1eS05PcmOTynmn3SnJBki+2f/fvmffSNvarkzy5Z/qjk1zWzntdkrTT75Tkne30i5Ic2rPMs9vn+GKSZw+hL4ck+Vj7P3RFkhdOa3+S3DnJp5N8ru3Ln05rX/r6tcfn57T3Z1pNWh6C2fr/XaZ/M/Xez4D9lmnvU7vu30/f/uIs9GtcMhn7FDcn+XZfDC9P8pUkl7a3Y0YcwzXt/0XX+Wy5OMa5PXYk+fckX053+zPLxTDu98UX2/sz9XkwKzKB+0awsf2jLq3n/d1hjOvah+lS1rlfMsa4hvJ5rz1NQh7aSM7p6vUdxufpiOMbyvelEca3rvwyjviGlVuyzH7SulXV3N2AtwK/296/I7Bf3/xjgA8CAR4DXNQ3/w+AvwY+0HVfhtm31Zbt+rbRvgEHAdcA+7aP3wWc0HV/1tm3+wI/A7wSeHHP9L2AfwYe2C73OeAhXffH27Kv88S9XsAvAI8CLu+Z9t+Bk9v7JwOvau8/pI35TsBhbV/2aud9Gnhs+//3QeAp7fT/DJzW3j8OeGd7/17Al9q/+7f3999kXw4EHtXevzvwT23MU9ef9nnv1t7fB7iozWtT15e+fu3x+Tnt/ZnGGxOYh9q4Zub/dx7e+wzYb5mBPg3cX5z2fo35/3gS9imOAXYCV/bE8HJ69p97po8qhp9sY9ifbvPZcnGMe3sc3N6/D93tzwyKYdzbYel/+78wQ58Hs3BjQveN2tjWtX/U9Y017u90HOOa92E6jnNd+yVjjm0on/fe9timE5GH1ptzunx915pvuopxPblm3DGuN7+MK75h5RaW2U9adzzjeCNP0g24R/vGyApt/ifwzJ7HVwMHtvcPBj4CPJ4JK5ptpm9rWXaK+3YQcB3Nl4S9gQ8AT+q6T+vpW0/bl7Nn0eyxwId6Hr8UeGnXffK27Os3ka8XcGjfh1JvzjsQuHpQvMCH2j4dCHyhZ/ozgf/Z26a9vzfw9faD67Y27bw9/n+H1K9zgF+a9v4AdwEuAY6a5r4w4PNzmvszrTcmNA8NiHMm/n/b9c3Ue59l9lumuU/tugbuL057v8Z9YzL2Kc4Crut5/HIGF0fG8hoyIfmsJ46utsez6X5/pjeGLrbD24HLmZHPg1m5MSX7Rm1sK+aTjmNb8/5OhzGuax+m41jXtV/SQXyHsonP+66376TdJjUPrZZzunp915Nvuohxvblm3DGuN7+MM77N5hZW2E9a720eh2d8IPA14H+1p3G+Ocld+9osvXmW7GynAbwG+EPgh6MOdAM207e1LNulDfetqr4C/A/gy8ANwM1V9eFxBL1Gm9n2K71XNXmm5fXaUlU3ALR/79tOXy7+g9r7/dP3WKaqdgM3A/deYV1DkWY4mkfS/Jp4KvvTDjdwKXAjcEFVTW1fWq/h9p+f09yfaTXx22MW/n/7vIbZeu8vt98yzX1ihf3Fqe7XBOhi+/0rzQGAXs9PM3z76T3Duoz8NZyUfNYXx1i3R5K9gF8BttPR/swyMYx1O7QeCVzA7HwezIqp2D5rzCddeg1r39/pynr3YTqzgf2Srq03l2lPE7edNrkPM2qvYfPfr0ZpWN+XRmKI33vGYZj7Sesyj0WzvWlO9XtTVT0S+A7N6X29MmC5SvI04MaqunjEMW7Uhvu2xmW7tJnXbX/gWJrTNe8P3DXJb40y2HXazLZf7vXUZJr212u5+Ffq10aW2ZQkdwPeA7yoqr69UtMNxDa2/lTVrVX1CJpfUR2Z5GErNJ/ovmzg83Oi+zPlJnp7zMr/75IZfe+vd79lGvrEBvYXp6JfE2zU26/Xm4AfBx5Bc2Dg1WOKYR8mI5/1xzHW7VFVtwJvBF5BR/szy8Qw1u3Qfh7sYs8DOSsxx4zPxG+fdewfdWIKjpUtmfTjXreZguNYazXx/18TYqK20xC+k43MEL9fjdKwvi+NxBC/93Rp5Ps881g02wns7Pl12btp3sj9bQ7peXwwcD3wOOBXklwLnA08PsnbRxvuumymb2tZtkub6dsTgWuq6mtV9QPgvcDPjjje9djMtl+uz5pM0/J6fTXJgQDt3xvb6Svlj4MHTN9jmSR7A/cEblphXZuSZOnA0FlV9d5p7w9AVX0LWASOnuK+LPf5Oa39mWYTuz1m8f+X2XzvL7ffMs19guX3F6e9X13rYvvdD9i99KCqvtr+EOWHwF8BR44hhh+juaZU1/nsdnF0tD2Wrim2SLf7M7fF0MF2eBzwUJqDZrPyeTArJnr7rHP/qCvr3d/pynr3Ybq03v2Srq03l2lPE7OdhvSdbJSG9f1qlIb1fWlUhvW9ZxyGuZ+0LnNXNKuqfwWuS/LgdtITgCv7mp0LPCuNx9CcpnhDVb20qg6uqkNpvnx8tKom5pcem+zbWpbtzGb6RnO66WOS3CVJ2mWvGlfsq9nktv8McHiSw5LckeZ9ee4IwtRwTMvrdS7NNR9o/57TM/24JHdKchhwOPDp9v/sliSPaf/HntW3zNK6nk6TN4tmvOEnJdm//ZXLk9ppG9Y+91uAq6rqL6a5P0nuk2S/9v6+NDs1X5jGvgCs8Pk5lf2ZchOZh2bp/7fXLL73V9hvmdo+tZbbX5z2fnWti+33CzRn9AC3fcFe8ms015QadQzHAf8wAfnsdnGMeXs8KMkDaN7Tf083+zMDYxj3+4LmQvZfAx7MjHwezJCJ3DeCDe0fdWID+zud2MA+TJfWu1/StXXlsg7im3QTkYeG9Z1slDEO6/vViGMcyvelEYY4lO89I4yv1zD3k9anRnjhu0m90QzB8Fng88D7gf2Bk4CT2vkB3gD8M3AZsHXAOhZoLzY4SbfN9G3Qsl33Z4h9+1OaL2iXA28D7tR1f9bZt/vRVMu/DXyrvX+Pdt4xwD+1/f4vXffF26qv9US9XsA7aIak+UH7vnoOzbUPPgJ8sf17r572/6WN/WrgKT3Tt7b/X/8MnArNBU+BOwN/A+yg+VB9YM8y/7GdvgP4nSH05edoTrv+PHBpeztmGvsD/DTwj21fLgf+pJ0+dX0Z0LcFfnSx3qnvzzTemLA81MY0M/+/K/RxZt77DN5vmeo+teu+3f7iLPRrXDcmY5/iFpp95d4Y3kbz3eDzNF+wDxxxDDuZjHy2XBzj3B5fBr5Hc82JrvZnloth3O+L2/63maHPg1m5MYH7Rm1c694/6vq21vd3h/E9gnXsw3Qc67r2S8YY11A+773dbrt2noc2knO6fH03+3k64tjWlWvGHeN688s44htWbmGZ/aT13pZ2riRJkiRJkiRJkqS5NXfDM0qSJEmSJEmSJEn9LJpJkiRJkiRJkiRp7lk0kyRJkiRJkiRJ0tyzaCZJkiRJkiRJkqS5Z9FMEklOT3JjksvX0PYvk1za3v4pybfGEKIkSZIkSZIkSSOVquo6BkkdS/ILwC7gzKp62DqWewHwyKr6jyMLTpIkSZIkSZKkMfBMM0lU1ceBm3qnJfnxJH+b5OIkn0jykwMWfSbwjrEEKUmSJEmSJEnSCO3ddQCSJtZ24KSq+mKSo4A3Ao9fmpnkAcBhwEc7ik+SJEmSJEmSpKGxaCbpdpLcDfhZ4G+SLE2+U1+z44B3V9Wt44xNkiRJkiRJkqRRsGgmaZA7AN+qqkes0OY44HnjCUeSJEmSJEmSpNHymmaSbqeqvg1ck+QZAGk8fGl+kgcD+wOf6ihESZIkSZIkSZKGyqKZJJK8g6YA9uAkO5M8BzgeeE6SzwFXAMf2LPJM4OyqqvFHK0mSJEmSJEnS8MVj3pIkSZIkSZIkSZp3nmkmSZIkSZIkSZKkuWfRTJIkSZIkSZIkSXPPopkkSZIkSZIkSZLmnkUzSZIkSZIkSZIkzT2LZpIkSZIkSZIkSZp7Fs0kSZIkSZIkSZI09yyaSZIkSZIkSZIkae5ZNJMkSZIkSZIkSdLcs2gmSZIk/f/Zu/N4W8u6/v+vt6CIAwKiJyY9lugvBccTYlodowSn8PtNC0NFpUi/jkUlWIlpFFY4T5ESYAISTqQ4kLojC1EwjEHJE5AcQVEZZKOihz6/P+5r42Kzh7X32Xuv6fV8PNZjr3Xdw/pc997rs+91Xfd13ZIkSZIkaeLZaSZJkiRJkiRJkqSJZ6eZJEmSJEmSJEmSJp6dZpIkSZIkSZIkSZp4dppJkiRJkiRJkiRp4tlpJkmSJEmSJEmSpIlnp5kkSZIkSZIkSZImnp1mkiRJkiRJkiRJmnh2mkmSJEmSJEmSJGni2WkmSZIkSZIkSZKkiWenmSRJkiRJkiRJkiaenWaSJEmSJEmSJEmaeHaa6TZJNibZ3PP6yiS/ssx9vSvJn8613znWPTHJn7fnv5DksuW850pJsn2Sf0pyY5J/HGQs0qQxD90Wj3lImnBrlf8kabmSrE9SSbZdhX2/Jsk/9LnubflyKdtJWh1bcw6zyH77PqeZ9f3OcyFpmVbj87ya5w9bI8lUkt9ehf32Xd/e85hhPU6Twk4zrYqqemFVvW4Z2/1rVT14NWJagmcA64B7V9Uze0+2ZiR5SZLzk9yS5MRZyw5JMt3z+H5Lco9ewzpIE2+c81CS7ZK8J8n/JLkpyX8keVLvDpLcLck7knyndb6ds9aVkDQYC+W/1WrIkkbJaneszPX9YYX3byPKkFmthjZpqfw/PzzsxJf64//Q4ZLkeUk+N+g4Bs1OswnlF6wF3R/4r6rassA6VwN/Dpwwe0FVva+q7jHzAP4fcDnwpVWJVhpR5qEFLZaHtgWuAn4JuBfwp8DpSdb3rHM8sDPws+3n761atJJWjLlRGrx0BvZd2TwwGEm2GXQMkoaL+VjSKDJ3bT07zUZMkj9M8oFZZW9N8qYkz0/ylTbq4PIkv9uzzsYkm5O8Msk3gb9v03+dmOT6JJcCPzfHW/5ckkvbOn+f5K5tf3fodW5XOz6wPZ/36sokj0zypRbn+4G7zo6z5/WVSf4gyX+2kRLvn4mhLf+jJNckuTrJb8+K4ckt9puSfCPJH8w6jjPbvWBmuyR/Brwa+M02Sux3gUOAP2qv/wmgqj5YVR8GvrvQ76s5FDi5qqqPdaWhZx4afB6qqpur6jVVdWVV/W9VfRS4Anh02/eDgV8DDq+qb1fVrVV1wfy/VUn9WCT/3SvdCNBr2uf9z9MaYJP8TJLPJPluutGf70uyY88+rmy58T+Bm/OTLzkrmv+SvBe4H/BPLZ/80QoeHmkotc/WN9r/4suSPAV4FT/5P/vltt5UkmOS/BvwfeCnM2vERmaNGkjy+CT/nuSGJFe1z+bhzPH9ofcz2l7fYeqwWedId0pyZJL/brnj9CQ7t81nRo/f0N7jsQvlmbbsuiSPaq93a+tsTHJwkvNnHbPfS3Jme/6UdCPav9fq+JoFjvVix2u/nuP15SQbe5Y9IMm/tN/T2cAus/b9a0kuadtOJfnZ+eKYtd0/Jvlm2qj7JA+d9Tt4Z5KzktwMPCHJnkk+mOTb7Vi+ra37vCSfS/I3LSdfkTbKP8kxwC8Ab2u/j7f1E5u00ub6Pz/fZyDJXZJcmOSl7fU2Sf4tyatbjvhBT86Z+f70nSR3XijfzBHT7BkzZn/X2i3JB9pn7ookL+tZtuB3xSQ/2/LBDS0//Fqfx2kmt96U7jzr//Qse147Dm9Mch3wmhbHcelm+bix5YLt85NRv4cm+Xo7Fn/c9nMgc/yvkUZVkn2TnNs+b9ckeVuSu/QsryQvTPK19pl9e5K0Zdu0/5/fSXI58JSe7eb8H5rkzenOO76X5IIkv9CzzVlJjut5/f4kJ6SbkeeGJHv3LLtPy2f3TbJTko+2fHN9e77HPPWdfQ4z83nftr1e6LvfvPVty3dLcma6c7NNSX6nz9/B87O09rZtkryqJ99dkGTPhX5f6c6v3gU8tv0+bugntnFkp9no+QfgwPzkC9C2wG8C7wWuBZ4K7AA8H3hj2hej5qfoRhvcHzgcOBr4mfY4gK5zZ7ZD2rKfAR4E/MnWBN8S6odbvDsD/wj8+iKb/QZwIPAA4GHA89q+DgR+H/gV4IF0Iy56vQf43aq6J7A38Jme7f4A+FVgr7Y9AFV1NPAXwPvbSLG/Bd4H/FV7/bQl1vf+wC8CJy9lO2nImYeGLA8lWUd3bC5pRY8B/gf4s3aidlGSxeooaXEL5b+TgC10ueCRwBOBmWlGAvwlsBvd6M89gdfM2vez6L5Q7dgzynRF819VPQf4OvC0lk/+amv2Jw27dBeRvAT4ufa/+ADgq9z+/+zDezZ5Dt35yT3p/o8utO/7AR8H3grcB3gEcGFVHc/yvj/MPkd6GfB0unOL3YDrgbe3dX+x/dyxvce5LJBnquq/gVcC70tyN+DvgROrago4E3hwkr16Yvkt4JT2/GbgucCOdDnqRUme3medbpNkd+BjdLN17Ex3HvSBJPdpq5wCXEDXWfY6es4JkzwIOBV4Bd2xPouuU+C2xroFfJzuXOu+dDN/vG/W8t8CjqH7nZ8LfJTud78e2B04rWfdxwCXtRj/CnhPklTVHwP/Cryk/T5e0kdc0oqb5//8nJ+BqvoR8Gzgta2R9EhgG+CYqrqa7vPQ+/3ht4AzqurH9Hdes6h0I3r/Cfgy3edtf+AVSQ5oq8z7XTHJndu2n2p1eyldjutnmv3/pmukvxfwZ8A/JNm1Z/lj6GYLui9dfvgbuosTf54uf/0R8L896z8eeHCL/9VJfraqPsH8/2ukUXQr3ew1uwCPpft7/3+z1nkqXef2w+naUGY+y7/Tlj0S2EB3OwoAFvgf+kW6c6ud6c4R/jE/uXj5BcBzkvxykkPae768qm4BPkj3vWrGbwD/UlXX0vWD/D3dudb9gB8Ay73QZaHvfvPWtzkV2EyXQ58B/EWS/ft4z6W2t/0+3bF4ctvmBXQXhs24w++rqr4CvBA4t/0+duwjrrFkp9mIqapr6K4sfGYrOhD4TlVdUFUfq6r/rs6/0J08/ELP5v8LHF1Vt1TVD+g+EMdU1XVVdRXwljne8m1VdVVVXUd3svCsOdZZiv2AOwNvqqofV9UZdIlwIW+pqqtbDP9ElzRp8f99VV1SVd+nO9np9WPgIUl2qKrrq+pLs7a7uKpuZhknd0vwXOBfq+qKVXwPaU2Zh4YrD7UvjO8DTqqqr7biPeg66W6kOxF7CXBS+rwqW+OjXXF3bZKL+1z/N9JdcXtJklMW32KyzJf/6L70PAl4RRsJei3wRuDgtt2mqjq75b5vA2/gjp3sb2m57gc9ZSud/6Q1N+A8dCuwHd3/4ju3Edr/vcD6J7b/6Vtaw/BCDgH+uapObecT362qC7ci1tnnSL8L/HFVbW6NQK8BnpF5pttZLM9U1d8BXwPOA3YF/riVfx/4CC2/tM6z/4+uM42qmqqqi6ob2f6fdA09s/NXP54NnFVVZ7V9nQ2cDzy5dUD+HPCnLf5z6M63Zvwm8LFWvx/TNWBvT9eAvaCqOqGqbuo5hg9Pcq+eVT5SVf9WVf9Ld2HUbsAftlz+w6rqHdX7P1X1d1V1K11j2a5096DVkJvk86GFPgNVdTFdR/aH6Dqyn9P+vqFrpJ7JC6E7pzmlbdfPeU0/fg64T1W9tqp+VFWXA3/X3gsW/q64H3AP4Ni27WfoOr0XPVeqqn9s3+3+t6reT5cb9+1Z5eqqemt1FzHdQtfQ/PKq+kZ1M3j8ezueM/6sqn5QVV+m6wC0g0x3MOp5qLX3fL6dI10J/C13/NwfW1U3VNXXgc9y+3aTN/V8r/nLPt7vH9q51ZaqOo7ufO7Bbdk36Tp2TgLeDDy3qm5qm96Wu5rbLgRq+/tAVX2/rX/MHHVYVLtoed7vfgvVt430ejzwynaecSHwbroLtxY7Jkttb/tt4E+q6rK2zZerqnfGtPl+X8JOs1F1Et2XDtrP9wIkeVKSz7fhnTfQ9ST3Tmvx7ar6Yc/r3ejuiTNjrqspZy/fbStj3w34RtXtpipc8CpO4Js9z79Pd2I0s6/e+HqfQ3dV1JOB/0k31cdj59lusfffGs+l+31J48Y89JN9DSwPtasz3wv8iK5jbMYP6Drs/rx9ifwXupOgJy7nfTTSTqTr2FlUayg9CnhcVT2U7op+3dFc+e/+dJ3x16SbEuQGui+S9wVINx3Iaemm7vge3Yi1XWbtd3b+mF22EvlPGoQTGVAeqqpNbR+vAa5tn8OFPkdzfQ7nsyfdaIWVMvsc6f7Ah3pyylfoOgHn7KTpM8/8Hd1FNW+d1eDb28D0W8CHW2caSR6T5LPppjK6ka6RavZ++3F/4Jkz9Wl1ejxdx9NuwPXVXUg0o/fcaLfe162D6yq6kSnzSjct0bHppiX6HnBlW9Qbf+/vfE+6jrH57il72/ngzPHhJ+eEGm4nMoHnQ31+Bk6iG1l5VlV9raf8DLrpuXajG91adKNB+s03/bg/sNusvPAqfpLnFvrOtBtwVcsHvcsXzAst/uemm5py5j33Zv68sAvdVP4L5fv5vitKvU5khPNQkgelm87wm+1z/xfc8XPfb7vJou0fSY5INxXhje1zeq9Z7/dRutGxl826wOUzwPbt/OX+dB1BH2r7vFuSv0031er36C6G3DFLv6fpgt/9FqnvbsB1PZ18M8v7yV1LbW9b7FzV3LUAO81G04eBh6Wbo/WpdEPQtwM+QHfV3brqhk+eRTdsfsbse2pdQ/cBmnG/Od5r9vKr2/ObgbvNLEjyU33Gfg2we7tSaaH37XdfvXPP9sZKVX2xqg6iS1ofBk7v2W6xet9uV8sJLsnj6JLhGcvZXhpyH8Y8NLOvgeShFv976L5U/nrd/or4/+wneI2/drX+db1l6e5D8Yl0c5r/a5L/ry36HeDtVXV92/baNQ53VHyYWfmP7kvRLcAuVbVje+zQvuRCd3VhAQ+rqh3oOtsya79znW+sdP6b732kVTPoPFRVp1TV4+kaOAp4PfN/DmaX3+6zRjftzYyr6KYM62c/0DVGzLevuba5CnhST07ZsaruWlXfmGf/C+aZJPcA3kR37vCa9NyriO5K5V2SPIKu86z3ivZT6Ead7VlV96K7z8Xs/DVjseP13ln1uXtVHUt3XrRTkrv3rN97bnQ13e9vpi6hy4/fmCeOGb8FHEQ3Dfa96DoGYP5z06uA+2We0XyLMLcOsUHnoTXW+7fYz2fgHXSNzwckefxtO6m6gS43/Ebbz6k9Fx32c14zY7G8cMWsvHDPqnpyW77Qd6argT3bRYS9yxfMC60R/e/oLji8d/vOejHz54XvAD9k/ny/EPOCbjMGeeiddFNc79U+969i/s/9bIu1f9zus5Lu/mWvpMs/O7XP6Y2z3u8YuguKdk1y28iy1pF+Ot35zG8BH+3poDqCbrTaY1odZqa7nqsei+Wuhb77LZa7dk5yz1nLF8tdy2lvW+hcdSHmLuw0G0mt1/gMui8wX2jDKO9CN1T128CWdDclXmxEwenAUeluhLgH3RzQs704yR7tS9WrgPe38i8DD03yiHRzyr6mz/DPpZvz9WVJtk3yf7n9MPilOB14frqbv94NePXMgnQ3tT0kyb1aQ/L36K7MnNnueUke0rY7epH3+Rbw070FLfa70l3VsE2Su87x5epQ4AOzrh6QxoJ56HbxDyQP0Z20/izdPQt+MGvZOXT3Mziq1fFxwEbgk0uvosbQ8cBLq+rRdFPxvKOVPwh4ULqbn38+3b33NMtc+a+6aRs/BRyXZIckd2pfgmem+7gnMA3ckO6+Pn/Y59utdP6DufOJtNbWJA8leXC6+11sR9fo+QO6/8XfAtbPamydy4XAwUnunGT2PSneB/xKuumTtk1y79bpBHN/zi4EfivdyI8DWXw6oHcBx7QG3pkb2R/Uln2bbgqe3vdYLM+8Gbigqn6b7t5i75pZ0EZWnQH8Nd29MM6etd/rquqHSfala4Caz4XMf7z+AXhakgPaMbhrupvW71FV/0M3VeOftfOnxwO994I7HXhKkv3TTUt9BF1j1b8vEMtM7LcA36Vr+PqLRdb/Al1D17FJ7t5ifNwi28wwt46ecT0f6v1bXPAzkOQ5dPfqeh7dfRRPah3sM06hmz3n17l9Z/pSzmsupJuGded2kc8repZ9Afheklcm2b7lhr2T/FxbvtB3xfPoGrX/qOWcjXR5o/c+hHO5O12D8LfbMXg+3UizObUG+BOANyTZrcX42PZ/ZTH9/q/R5BqlPHRPujaN6da596IlbHs6XfvLHkl2oruHYq/Z/0PvSddm821g2ySvprsnFwBJfpHunl7PbY+3tlw04xS6qZ0P4Y656wd0uWtnFm6HuRD4xST3Szel7VEzC/r47jdvfaubavbfgb9s5xkPAw7jjvdcnW057W3vBl6XZK90Hpbk3otsA93vY4/0d+/YsWXiHl0nAfvQpkRrHTMvo/tgXk/3ZebMRfbxZ3RDQK+g+7C/d451TmnLLm+PP2/v91/Aa4F/ppv/+XNzbHsH1d1s9v/SnZRdT5fEPtjPtnPs6+N0c1p/FthE1xAO3UkhdPPBXpluyO0LaVMpte3eRDdkd1P7uZD30N0H4YYkH25lf0KXaI9s+/1BKwOgNWD9Bk7NqPFmHhpQHmoNaL9LN9XAN5NMt8chbf8/pruq9Ml0V2T9Hd0831+de/eaFK0h5OfpbqR8Id00EjM3Pt+W7kbxG+muzHt3kh3XPsqRcLv81zyX7svMpXS55Qx+cmz/DHgU3efxY/Sfc1Y0/zV/CfxJyyd/sITtpBWxxnloO+BYupEC36Qb+f0q4B/b8u8m+dI82wL8Kd0VutfTfY5va3hpFww9ma4D5zq6xpWZ+9jM9f3h5XQNujfQNeLMlM/nzXTnUZ9KchPweeAx7b2/T3eF9b+199iPBfJM62w7kO5cBLobwz9q5ryhOYVuNMo/zpqe8P8Br20xvJqfjJqfy0LH6yq6c5NX0TX4XEXX0D7TJvFbrX7X0TVindyz7WV051BvpftdPo3uoqEfLRALbR//Q3f19qV0x3Be1d3L6WnAA+kuPtpMd57YjzfT3XPu+iRz3aNXQ2TMz4du+z9P1wk+52cg3b0E30T3HWG6qk6h67x+Y8++zqQ7Ft+q7n5dM5ZyXvNeuot9rqQ7p5m5AKj3M/cIuu+D36Fr5J257+C83xXb5//X6O4r9B26zoZFv+9U1aXAcXTf275Fdz73bwttQ9eZcRHdfbCvoxux3E97ar//azSBRjAP/QHd/+qb6NoX3r/w6rfzd3QX8H4Z+BJ3zBmz/4d+Evg48F90OeCHtOkOk+xA9//9JdXdZ/BzdOddf590swlV1Uyn+m5tPzPeRHdP1O/Q5cNPzBdwdfdefT/dLD4X0I3I7bXQd7/F6vssupG/V9NNHXl0e795LbO97Q1t/U/RdXi+h67+i/kMcAldW9N3+lh/LKVud0sXjYp2gvNV4Keq6nuDjmcYJPlZumH12y0wD/182xbdEONNqxKcNIbMQ3dkHtIwSrKeblqKvduXjMuqatc51nsX8PmqOrG9/jRwZFV9cS3jHQXmP2lpzEOSBs08JGnQzEPS6HCk2QhqQ7t/Hzht0htqkvyfdFN47ER3tc8/LbWhWtLSmYd+wjykUdI+r1ckeSZ094VJMjMy4sPAE1r5LnTTglw+iDiHmflP2jrmIUmDZh6SNGjmIWm42Wk2YtLdHPl7wK+y+D1wJsHv0k3v8d909ydYypy6kpbBPHQH5iENrSSn0k0/8+Akm5McRjct2GFJvkw37cLMPXI+STd9zKV0U47+YVV9dxBxDyvzn7R05iFJg2YekjRo5iFptDg9oyRJkiRJkiRJkiaeI80kja0kVya5KMmFSc5vZTsnOTvJ19rPnXrWPyrJpiSXJTmgp/zRbT+bkrxl5uaikiRJkiRJkqTxYaeZpHH3hKp6RFVtaK+PBD5dVXsBn26vSfIQ4GDgocCBwDuSbNO2eSdwOLBXexy4hvFLkiRJkiRJktbAtoMOYKXtsssutX79+kXXu/nmm7n73e+++gGtAesynCaxLhdccMF3quo+axDS1jgI2NienwRMAa9s5adV1S10N2PdBOyb5Epgh6o6FyDJycDTgY8v9Cb95KJh+RsZhjiMwRhWKoYRyUNrYpTy0HIY++CMcvxrEbt56CfG5buZ8S3fMMcG4xufeegnxiUPrbZJrv8k1x1Wr/7moZ8Y1zw0SvEa6+oY9li3Kg9V1YIP4ATgWuDiWeUvBS6ju1HhX/WUHwVsassO6Cl/NHBRW/YWfnI/te2A97fy84D1PdscCnytPQ5dLNaq4tGPfnT147Of/Wxf640C6zKcJrEuwPnVx+d0rR7AFcCXgAuAw1vZDbPWub79fBvw7J7y9wDPADYA/9xT/gvARxd7735y0bD8jQxDHMZgDCsVw7DloUE+RikPLYexD84ox78WsZuHlpaHqob/b8r4lm+YY6sa3/jMQ+OXh1bbJNd/kutetXr1H/c8BOwInAF8FfgK8Nj51h3XPDRK8Rrr6hj2WLcmD/Uz0uzE1ph88kxBkifQjcp4WFXdkuS+rbx3erPdgH9O8qCqupWfTG/2eeAsuunNPg4c1hqtH5jkYOD1wG8m2Rk4ujVYF3BBkjOr6vo+YpYkgMdV1dUtR52d5KsLrDvXfcpqgfI77iA5nC7PsW7dOqamphYMbnp6etF11sIwxGEMxjBsMUiS7ijJjsC7gb3pzodeUG00viStBfOQpCHxZuATVfWMJHcB7jbogCStnEU7zarqnCTrZxW/CDi2umnMqKprW/lypjc7CHhN2/4M4G1JAhwAnF1V17VtzqbraDt1WTWVNHGq6ur289okHwL2Bb6VZNequibJrnQjaQE2A3v2bL4HcHUr32OO8rne73jgeIANGzbUxo0bF4xvamqKxdZZC8MQhzEYw7DFIEmakw1EkgbNPCRpoJLsAPwi8DyAqvoR8KNBxiRpZS33nmYPAn4hyTHAD4E/qKovArvTjSSbsbmV/bg9n11O+3kVQFVtSXIjcO/e8jm2kaQFJbk7cKequqk9fyLwWuBMuqlfj20/P9I2ORM4Jckb6EbK7gV8oapuTXJTkv3oppB9LvDWta2NJEnSYNlAJGnQzEOShsRPA98G/j7Jw+luCfLyqrp5ZoWlzkQEozfjyijFa6yrY5RiXarldpptC+wE7Af8HHB6kp9medObrfmUaDBev1TrMpysy8CtAz7UDVxlW+CUqvpEki/S5azDgK8DzwSoqkuSnA5cCmwBXtymloVudO2JwPZ0I2Q/vpYVkSRJGgKLNhDBeH43M77lG+bYwPhGUF95SJJW2bbAo4CXVtV5Sd4MHAn86cwKS52JCEZvxpVRitdYV8coxbpUy+002wx8sN1Q7QtJ/hfYheVNbzazzeYk2wL3Aq5r5RtnbTM1VzCTkIgWYl2Gk3UZrKq6HHj4HOXfBfafZ5tjgGPmKD+fbs58SZKkSbVoAxGM53cz41u+YY4NjG8E9ZWHxrHzfrVNcv0nue5g/ZdpM7C5qs5rr8+gy0WSxsRyO80+DPwyMJXkQcBdgO+wvOnNZqZKOxd4BvCZqqoknwT+IslObb0nAkctM15JkiRJ0vLZQCRp0PrKQ+PYeb/aJrn+k1x3sP7LUVXfTHJVkgdX1WV0F2ZfOui4JK2cRTvNkpxKN+JrlySbgaOBE4ATklxMN3/0oW3U2XKmN3sP8N4km+hGmB0MUFXXJXkd8MW23mur6rqtq64kSZIkaalsIJI0aOYhSUPkpcD7ktwFuBx4/oDjkbSCFu00q6pnzbPo2fOsv6Tpzarqh7R7Cs2x7AS6DjpJkqSRlWRP4GTgp4D/BY6vqjfPWifAm4EnA98HnldVX2rLDmzLtgHeXVXHrmH4kjTDBiJJg2YekjRwVXUhsGHQcUhaHcudnnEsrD/yYyu6vyuPfcqK7k/S+DMPSRNjC3BEVX0pyT2BC5KcXVW9V0c/iW5q672AxwDvBB6TZBvg7cCv0k1L9MUkZ87aVlpT/v+aTKvVQHTRN27keSv4N+XfkzS+RiUPgblI0tKYh6ThcadBByBJkjTuquqamVFjVXUT8BVg91mrHQScXJ3PAzsm2RXYF9hUVZdX1Y+A09q6kiRJkiRJWkF2mkmSJK2hJOuBRwLnzVq0O3BVz+vNrWy+ckmSJEmSJK2giZ6eUZIkaS0luQfwAeAVVfW92Yvn2KQWKJ9r/4cDhwOsW7eOqampBeOZnp5edJ1hZeyDMz09zRH73Lqi+1yr4zHqx16SJEmStLrsNJMkSVoDSe5M12H2vqr64ByrbAb27Hm9B3A1cJd5yu+gqo4HjgfYsGFDbdy4ccGYpqamWGydYWXsgzM1NcVxn7t5Rfd55SEbV3R/8xn1Yy9JkiRJWl1OzyhJkrTKkgR4D/CVqnrDPKudCTw3nf2AG6vqGuCLwF5JHpDkLsDBbV1JkiRJkiStIEeaSZIkrb7HAc8BLkpyYSt7FXA/gKp6F3AW8GRgE/B94Plt2ZYkLwE+CWwDnFBVl6xp9JIkSZIkSRPATjNJkqRVVlWfY+57k/WuU8CL51l2Fl2nmiRJkiRJklaJ0zNKkiRJkiRJkiRp4tlpJkmSJEmSJEmSpIlnp5kkSZIkSZIkSZImnp1mkiRJkiRJkiRJmnh2mkmSJEmSJEmSJGni2WkmSZIkSZIkSZKkiWenmSRJkiRJkiRJkiaenWaSJEmSJEmSJEmaeHaaSZIkSZIkSZIkaeLZaSZJkiRJkiRJkqSJt2inWZITklyb5OI5lv1BkkqyS0/ZUUk2JbksyQE95Y9OclFb9pYkaeXbJXl/Kz8vyfqebQ5N8rX2OHSraytJkiRJkiRJkiTNoZ+RZicCB84uTLIn8KvA13vKHgIcDDy0bfOOJNu0xe8EDgf2ao+ZfR4GXF9VDwTeCLy+7Wtn4GjgMcC+wNFJdlpa9SRJkiRJkiRJkqTFLdppVlXnANfNseiNwB8B1VN2EHBaVd1SVVcAm4B9k+wK7FBV51ZVAScDT+/Z5qT2/Axg/zYK7QDg7Kq6rqquB85mjs47SZIkSZIkSZIkaWst655mSX4N+EZVfXnWot2Bq3peb25lu7fns8tvt01VbQFuBO69wL4kSZIkSZIkSZKkFbXtUjdIcjfgj4EnzrV4jrJaoHy528yO6XC6qR9Zt24dU1NTc612O9PT0xyxz62LrrcU/bzvapienh7Ye6806zKcxqkukiRJkiRJkiTNZcmdZsDPAA8AvtzNosgewJeS7Es3GmzPnnX3AK5u5XvMUU7PNpuTbAvci246yM3AxlnbTM0VUFUdDxwPsGHDhtq4ceNcq93O1NQUx33u5kXXW4orD1n8fVfD1NQU/dR5FFiX4TROdZEkSZIkSZIkaS5Lnp6xqi6qqvtW1fqqWk/XufWoqvomcCZwcJLtkjwA2Av4QlVdA9yUZL92v7LnAh9puzwTOLQ9fwbwmXbfs08CT0yyU5Kd6Ea2fXL5VZUkSZIkSZIkSZLmtuhIsySn0o342iXJZuDoqnrPXOtW1SVJTgcuBbYAL66qmTkQXwScCGwPfLw9AN4DvDfJJroRZge3fV2X5HXAF9t6r62q65ZcQ0mSpCGQ5ATgqcC1VbX3HMv/EDikvdwW+FngPu2c6ErgJuBWYEtVbVibqCVJkiRJkibHop1mVfWsRZavn/X6GOCYOdY7H7hDA1FV/RB45jz7PgE4YbEYJUmSRsCJwNuAk+daWFV/Dfw1QJKnAb8364KhJ1TVd1Y7SEmSJEnS/LyoURpvy7mnmSRJkpaoqs5Jsr7P1Z8FnLqK4UjSktlAJEmSdBsvapTGlJ1mkiRJQyTJ3YADgZf0FBfwqSQF/G1VHT+Q4CTJBiJJA2YHviRJWk12mkmSJA2XpwH/NmtqxsdV1dVJ7gucneSrVXXO7A2THA4cDrBu3TqmpqYWfKPp6elF1xlWxj4409PTHLHPrYuvuARrdTxG/dhLkm5jB76kQfKiRmmM2WkmSZI0XA5m1tSMVXV1+3ltkg8B+wJ36DRrX9aOB9iwYUNt3LhxwTeamppisXWGlbEPztTUFMd97uYV3eeVh2xc0f3NZ9SP/RCwgUiSJGmRixqXejEjwLrt4Yh9tqxokKt5sdgoXYxmrKtjlGJdKjvNJI21JNsA5wPfqKqnJtkZeD+wHrgS+I2qur6texRwGN00Hy+rqk+28kcDJwLbA2cBL6+qWtuaSJoESe4F/BLw7J6yuwN3qqqb2vMnAq8dUIiSJtuio16HoZFopb+8D3uDwDDHN8yxgfGNsAU78IchD8HajaJeCZP8tzbJdQfrv1yLXdS41IsZAd76vo9w3EUr21S/mhemjdLFaMa6OkYp1qWy00zSuHs58BVgh/b6SODTVXVskiPb61cmeQjd6I6HArsB/5zkQVV1K/BOui9dn6frNDsQ+PjaVkPSqEtyKrAR2CXJZuBo4M4AVfWuttr/AT5VVb3DeNYBH0oC3bnbKVX1ibWKW5Jm9DPqdRgaiVa6gWjYGwSGOb5hjg2Mb4Qt2IE/DHkI1m4U9UqY5L+1Sa47WP/l8KJGafzZaSZpbCXZA3gKcAzw+634ILpGa4CTgCngla38tKq6BbgiySZg33aT6R2q6ty2z5OBp2OnmaQlqqpn9bHOiXQjW3vLLgcevjpRSVJ/bCCSNCz6nbZaklaJFzVKY85OM0nj7E3AHwH37ClbV1XXAFTVNe3qRIDd6UaSzdjcyn7cns8ulyRJmiQ2EEkaODvwJQ2aFzVK489OM0ljKclTgWur6oIkG/vZZI6yWqB8rvdc0tz509PTHLHPrX2E1r/lzEU+DHOYG4MxDFsMkqTbs4FI0pCwA1+SJK0qO80kjavHAb+W5MnAXYEdkvwD8K0ku7ZRZrsC17b1NwN79my/B3B1K99jjvI7WOrc+VNTUxz3uZsXXGepljNv/jDMYW4MxjBsMUiSJGn42IEvSZJW250GHYAkrYaqOqqq9qiq9cDBwGeq6tnAmcChbbVDgY+052cCByfZLskDgL2AL7SpHG9Ksl+6yxmf27ONJEmSJEmSJGlMONJM0qQ5Fjg9yWHA14FnAlTVJUlOBy4FtgAvrqqZuRNfBJwIbA98vD0kSZIkSZIkSWPETjNJY6+qpoCp9vy7wP7zrHcMcMwc5ecDe69ehJIkSZIkSZKkQXN6RkmSJEmSJEmSJE08O80kSZIkSZIkSZI08ew0kyRJkiRJkiRJ0sSz00ySJEmSJEmSJEkTb9FOsyQnJLk2ycU9ZX+d5KtJ/jPJh5Ls2LPsqCSbklyW5ICe8kcnuagte0uStPLtkry/lZ+XZH3PNocm+Vp7HLpSlZYkSZIkSZIkSZJ69TPS7ETgwFllZwN7V9XDgP8CjgJI8hDgYOChbZt3JNmmbfNO4HBgr/aY2edhwPVV9UDgjcDr2752Bo4GHgPsCxydZKelV1GSJEmSJEmSJEla2KKdZlV1DnDdrLJPVdWW9vLzwB7t+UHAaVV1S1VdAWwC9k2yK7BDVZ1bVQWcDDy9Z5uT2vMzgP3bKLQDgLOr6rqqup6uo252550kSZIkSZIkSZK01VbinmYvAD7enu8OXNWzbHMr2709n11+u21aR9yNwL0X2JckSZIkSZIkSZK0orbdmo2T/DGwBXjfTNEcq9UC5cvdZnYch9NN/ci6deuYmpqaP+hmenqaI/a5ddH1lqKf910N09PTA3vvlWZdhtM41UWSJEmSJEmSpLksu9MsyaHAU4H925SL0I0G27NntT2Aq1v5HnOU926zOcm2wL3opoPcDGyctc3UXLFU1fHA8QAbNmyojRs3zrXa7UxNTXHc525edL2luPKQxd93NUxNTdFPnUeBdRlO41QXSZI0fNYf+bEV3+eVxz5lxfcpSZIkSRpvy5qeMcmBwCuBX6uq7/csOhM4OMl2SR4A7AV8oaquAW5Ksl+7X9lzgY/0bHNoe/4M4DOtE+6TwBOT7JRkJ+CJrUySJGnkJDkhybVJLp5n+cYkNya5sD1e3bPswCSXJdmU5Mi1i1qSJEmSJGlyLDrSLMmpdCO+dkmyGTgaOArYDji76wPj81X1wqq6JMnpwKV00za+uKpm5kB8EXAisD3dPdBm7oP2HuC9STbRjTA7GKCqrkvyOuCLbb3XVtV1W1ddSZKkgTkReBtw8gLr/GtVPbW3IMk2wNuBX6Ubif/FJGdW1aWrFagkSZIkSdIkWrTTrKqeNUfxexZY/xjgmDnKzwf2nqP8h8Az59nXCcAJi8UoSZI07KrqnCTrl7HpvsCmqrocIMlpwEF0FylJkiRJkiRphSxrekZJkiStiscm+XKSjyd5aCvbHbiqZ53NrUySJEmSJEkraNGRZpIkSVoTXwLuX1XTSZ4MfJju/rCZY92aawdJDgcOB1i3bh1TU1MLvuH09PSi6wwrYx+c6elpjtjn1sVXHLC5jvGoH3tJkiRJ0uqy00ySJGkIVNX3ep6fleQdSXahG1m2Z8+qewBXz7OP44HjATZs2FAbN25c8D2npqZYbJ1hZeyDMzU1xXGfu3nQYSzqykM23qFs1I+9JEmSJGl1OT2jJEnSEEjyU0nSnu9Ld572XeCLwF5JHpDkLsDBwJmDi1SSJEmSJGk8OdJMkiRpDSQ5FdgI7JJkM3A0cGeAqnoX8AzgRUm2AD8ADq6qArYkeQnwSWAb4ISqumQAVZAkSZIkSRprdppJkiStgap61iLL3wa8bZ5lZwFnrUZckiRJkiRJ6jg9oyRJkiRJkiRJkiaenWaSJEmSJEmSJEmaeHaaSZIkSZIkSZIkaeLZaSZJkiRJ6kuSbZL8R5KPDjoWSZKkQfB8SBpvdppJkiRJkvr1cuArgw5C0uSysVrSEPB8SBpjdppJkiRJkhaVZA/gKcC7Bx2LpIlmY7WkgfF8SBp/2w46AEmSJEnSSHgT8EfAPQcch6QJ1dNYfQzw+wMOR9JkehOLnA8lORw4HGDdunVMTU0tutN128MR+2xZmQibft53uaanp1d1/yvJWFfHKMW6VHaaSZIkSZIWlOSpwLVVdUGSjQusN/BGopX+8j7sDQLDHN8wxwbGN6LehJ33kgak3/OhqjoeOB5gw4YNtXHjvKve5q3v+wjHXbSyTfVXHrL4+y7X1NQU/dRrGBjr6hilWJfKTjNJkiRJ0mIeB/xakicDdwV2SPIPVfXs3pWGoZFopRuIhr1BYJjjG+bYwPhGzSh13sPqjvBYaZPcQTvJdQfrvwx9nQ9JGm12mkmSJEmSFlRVRwFHAbTG6j+wgUjSGhuZzntY3REeK22SO2gnue5g/ZfK8yFpMtxp0AFIkiRJkiRJC6mqo6pqj6paDxwMfMbGakmStNIcaSZJkiRJ6ltVTQFTAw5DkiRpYDwfksbXoiPNkpyQ5NokF/eU7Zzk7CRfaz936ll2VJJNSS5LckBP+aOTXNSWvSVJWvl2Sd7fys9Lsr5nm0Pbe3wtyaErVmtJkiRJkiSNpKqaqqqnDjoOSZI0fvqZnvFE4MBZZUcCn66qvYBPt9ckeQjdEPmHtm3ekWSbts076W7Euld7zOzzMOD6qnog8Ebg9W1fOwNHA48B9gWO7u2ckyRJkiRJkiRJklbKop1mVXUOcN2s4oOAk9rzk4Cn95SfVlW3VNUVwCZg3yS7AjtU1blVVcDJs7aZ2dcZwP5tFNoBwNlVdV1VXQ+czR077yRJkiRJkiRJkqSt1s9Is7msq6prANrP+7by3YGretbb3Mp2b89nl99um6raAtwI3HuBfUnSopLcNckXknw5ySVJ/qyVr9j0spIkSZIkSZKk8bHtCu9vrobkWqB8udvc/k2Tw+mmfmTdunVMTU0tGuj09DRH7HProustRT/vuxqmp6cH9t4rzboMpxGtyy3AL1fVdJI7A59L8nHg/9JNL3tskiPpppd95azpZXcD/jnJg6rqVn4yvezngbPoRr1+fO2rJEmSJEmSJElaLcvtNPtWkl2r6po29eK1rXwzsGfPensAV7fyPeYo791mc5JtgXvRTQe5Gdg4a5upuYKpquOB4wE2bNhQGzdunGu125mamuK4z9286HpLceUhi7/vapiamqKfOo8C6zKcRrEubSrY6fbyzu1RdFPCbmzlJ9HllVfSM70scEWSmellr6RNLwuQZGZ6WTvNJC1JkhOApwLXVtXecyw/hC4fQZe/XlRVX27LrgRuAm4FtlTVhjUJWpIkSZIkaYIsd3rGM4FD2/NDgY/0lB+cZLskDwD2Ar7QpnC8Kcl+bVqz587aZmZfzwA+0xq7Pwk8MclObfq0J7YySepLkm2SXEjXsX92VZ3Hyk4vK0lLcSIL35/1CuCXquphwOtoFwT1eEJVPcIOM0mSJEmSpNWx6EizJKfSjcrYJclm4GjgWOD0JIcBXweeCVBVlyQ5HbgU2AK8uE1tBvAiusai7elGaMyM0ngP8N42quM6uunRqKrrkrwO+GJb77VVdd1W1VbSRGn55xFJdgQ+lOQOIzt6rPlUscMyTewwTL9pDMYwbDGshqo6J8n6BZb/e8/Lz3P7UfqSJEmSJElaZYt2mlXVs+ZZtP886x8DHDNH+fnAHRqsq+qHtE63OZadAJywWIyStJCquiHJFN0Ij5WcXnb2+yxpqthhmSZ2GKbfNAZjGLYYhsBh3H4a2AI+laSAv235RpIkSZIkSStoufc0k6ShluQ+wI9bh9n2wK8Ar+cnU8Ieyx2nlz0lyRuA3fjJ9LK3JrkpyX7AeXTTy751bWsjaZIkeQJdp9nje4ofV1VXJ7kvcHaSr1bVOXNsu+QRr6M6qs/YB2c1RkqvhrmO8agfe0mSJEnS6rLTTNK42hU4Kck2dPdvPL2qPprkXFZuellJWlFJHga8G3hSVX13pryqrm4/r03yIWBf4A6dZssZ8Tqqo/qMfXBWY6T0aphr9PWoH3tJkiRJ0uqy00zSWKqq/wQeOUf5d1mh6WUlaSUluR/wQeA5VfVfPeV3B+5UVTe1508EXjugMCVJkiRJksaWnWaSJElrIMmpwEZglySbgaOBOwNU1buAVwP3Bt6RBGBLVW0A1gEfamXbAqdU1SfWvAKSJEmSJEljzk4zSZKkNVBVz1pk+W8Dvz1H+eXAw1crLkmSJEmSJHXuNOgAJEmSJEmSJEmSpEGz00ySJEmSJEmSJEkTz04zSZIkSZIkSZIkTTw7zSRJkiRJkiRJkjTx7DSTJEmSJEmSJEnSxLPTTJIkSZIkSZIkSRPPTjNJkiRJkiRJkiRNPDvNJEmSJEmSJEmSNPHsNJMkSZIkSZIkSdLEs9NMkiRJkiRJkiRJE89OM0mSJEmSJEmSJE08O80kSZIkSZIkSZI08baq0yzJ7yW5JMnFSU5NctckOyc5O8nX2s+detY/KsmmJJclOaCn/NFJLmrL3pIkrXy7JO9v5eclWb818UqSJEmSlq591/tCki+374B/NuiYJEmS1prnRNL4W3anWZLdgZcBG6pqb2Ab4GDgSODTVbUX8On2miQPacsfChwIvCPJNm137wQOB/ZqjwNb+WHA9VX1QOCNwOuXG68kSZIkadluAX65qh4OPAI4MMl+gw1J0iSxoVrSkPCcSBpzWzs947bA9km2Be4GXA0cBJzUlp8EPL09Pwg4rapuqaorgE3Avkl2BXaoqnOrqoCTZ20zs68zgP1nRqFJkiRJktZGdabbyzu3Rw0wJEmTx4ZqSQPnOZE0/rZd7oZV9Y0kfwN8HfgB8Kmq+lSSdVV1TVvnmiT3bZvsDny+ZxebW9mP2/PZ5TPbXNX2tSXJjcC9ge8sN25JkiRp0qw/8mMrtq8j9tnCVnyN0AhrM4VcADwQeHtVnTfgkCRNkHahtQ3VkgbOcyJpvC372267V9lBwAOAG4B/TPLshTaZo6wWKF9om9mxHE43vSPr1q1jampqgTA609PTHLHPrYuutxT9vO9qmJ6eHth7rzTrMpzGqS6SNChJTgCeClzbpraevTzAm4EnA98HnldVX2rLDmzLtgHeXVXHrlngktRU1a3AI5LsCHwoyd5VdXHvOsv5brZu+5nO2JWx0uetw34uPMzxDXNsYHyjyIZqScNgsXOiYTgfgtVtqx6l/1HGujpGKdal2ppLRH8FuKKqvg2Q5IPAzwPfSrJrG2W2K3BtW38zsGfP9nvQTee4uT2fXd67zeY2BeS9gOtmB1JVxwPHA2zYsKE2bty4aPBTU1Mc97mb+6tpn648ZPH3XQ1TU1P0U+dRYF2G0zjVRZIG6ETgbXRTUc/lSfzk/q6Pobvn62Na49DbgV+lOzf6YpIzq+rSVY9YkuZQVTckmaK7F/XFs5Yt+bvZW9/3EY67aOVGL67097JhPxce5viGOTYwvlE0Kp33MLgLq5djnBs+FzPJdQfrv7XmOycahvMhWN226lH6H2Wsq2OUYl2qrfkkfh3YL8nd6KZn3B84H7gZOBQ4tv38SFv/TOCUJG8AdqNrEPpCVd2a5KY2D/V5wHOBt/ZscyhwLvAM4DNtOL4kSdJIqapzkqxfYJWDgJPbuc7nk+zYLkBaD2yqqssBkpzW1rXTTNKaSXIf4MetcWh7uosoXz/gsCRNqGHvvIfBXVi9HOPc8LmYSa47WP/l8JxIGn9bc0+z85KcAXwJ2AL8B91JyT2A05McRtex9sy2/iVJTqdr4NkCvLhdIQTwIrqrr7cHPt4eAO8B3ptkE90Is4OXG68kSdKQu+1ers3MfV7nKn/MGsYlSQC7Aie10a93Ak6vqo8OOCZJE8SGaklDwnMiacxt1WU0VXU0cPSs4lvoRp3Ntf4xwDFzlJ8P3OHeHlX1Q1qnmyRJ0phbzv1fb7+DJU5HNMrTsRj70qzklFOrMYXVapjrGI/y382gVdV/Ao8cdBySJpoN1ZIGznMiafyt7NhzSZIkLdd893+9yzzld7DU6YhGeToWY1+a5x35sRXb1xH7bFnxKaxWw1zTYo3y340kTTobqiVJ0lq406ADkCRJEtDdy/W56ewH3FhV1wBfBPZK8oAkd6GbrvrMQQYqSZIkSZI0job/ElFJkqQxkORUYCOwS5LNdFNc3xmgqt4FnAU8GdgEfB94flu2JclLgE8C2wAnVNUla14BSZIkSZKkMWenmSRJ0hqoqmctsryAF8+z7Cy6TjVJkiRJkiStEqdnlCRJkiRJkiRJ0sSz00ySJEmSJEmSJEkTz04zSZIkSZIkSZIkTTw7zSRJkiRJkiRJkjTx7DSTJEmSJEmSJEnSxLPTTJIkSZIkSZIkSRPPTjNJkiRJkiRJkiRNPDvNJI2lJHsm+WySryS5JMnLW/nOSc5O8rX2c6eebY5KsinJZUkO6Cl/dJKL2rK3JMkg6iRJkiRJkiRJWj12mkkaV1uAI6rqZ4H9gBcneQhwJPDpqtoL+HR7TVt2MPBQ4EDgHUm2aft6J3A4sFd7HLiWFZEkSZIkSZIkrT47zSSNpaq6pqq+1J7fBHwF2B04CDiprXYS8PT2/CDgtKq6paquADYB+ybZFdihqs6tqgJO7tlGkiRJkiRJkjQm7DSTNPaSrAceCZwHrKuqa6DrWAPu21bbHbiqZ7PNrWz39nx2uSRJkiRJkiRpjGw76AAkaTUluQfwAeAVVfW9BW5HNteCWqB8rvc6nG4aR9atW8fU1NSCsU1PT3PEPrcuuM5SLfae88WxnO1WkjEYw7DFIEmSJEmSpMljp5mksZXkznQdZu+rqg+24m8l2bWqrmlTL17byjcDe/ZsvgdwdSvfY47yO6iq44HjATZs2FAbN25cML6pqSmO+9zNS6rTYq48ZOH3nC+OxWJdbcZgDMMWgyRJkiRJkiaP0zNKGkvphpS9B/hKVb2hZ9GZwKHt+aHAR3rKD06yXZIHAHsBX2hTON6UZL+2z+f2bCNJkiRJkiRJGhNb1WmWZMckZyT5apKvJHlskp2TnJ3ka+3nTj3rH5VkU5LLkhzQU/7oJBe1ZW9pDdO0xuv3t/Lz2n2JJKkfjwOeA/xykgvb48nAscCvJvka8KvtNVV1CXA6cCnwCeDFVTUzd+KLgHcDm4D/Bj6+pjWRJEmSJEmSJK26rZ2e8c3AJ6rqGUnuAtwNeBXw6ao6NsmRwJHAK5M8BDgYeCiwG/DPSR7UGqXfSXcfoM8DZwEH0jVKHwZcX1UPTHIw8HrgN7cyZkkToKo+x9z3IwPYf55tjgGOmaP8fGDvlYtOkiRJkiRJkjRslj3SLMkOwC/STX9GVf2oqm4ADgJOaqudBDy9PT8IOK2qbqmqK+hGbOzb7im0Q1WdW1UFnDxrm5l9nQHsPzMKTZIkSZIkSZIkSVopWzM9408D3wb+Psl/JHl3krsD69o9gGg/79vW3x24qmf7za1s9/Z8dvnttqmqLcCNwL23ImZJkqSBSHJgm6J6UxuNP3v5H/ZMJ3txkluT7NyWXdmmsr4wyflrH70kSZIkSdL425rpGbcFHgW8tKrOS/JmuqkY5zPXCLFaoHyhbW6/4+RwuukdWbduHVNTUwuE0ZmenuaIfW5ddL2l6Od9V8P09PTA3nulWZfhNE51kaRBSLIN8Ha6eyluBr6Y5MyqunRmnar6a+Cv2/pPA36vqq7r2c0Tquo7axi2JEmSJEnSRNmaTrPNwOaqOq+9PoOu0+xbSXatqmva1IvX9qy/Z8/2ewBXt/I95ijv3WZzkm2BewG9jUcAVNXxwPEAGzZsqI0bNy4a/NTUFMd97uY+qtm/Kw9Z/H1Xw9TUFP3UeRRYl+E0TnWRpAHZF9hUVZcDJDmNbhrqS+dZ/1nAqWsUmyRJkiRJktiK6Rmr6pvAVUke3Ir2p2v4ORM4tJUdCnykPT8TODjJdkkeAOwFfKFN4XhTkv3a/cqeO2ubmX09A/hMu++ZJEnSKJlvmuo7SHI34EDgAz3FBXwqyQVthL0kSZIkSZJW2NaMNAN4KfC+JHcBLgeeT9cRd3qSw4CvA88EqKpLkpxO17G2BXhxVc3Mj/gi4ERge+Dj7QHwHuC9STbRjTA7eCvjlSRJGoS+ppxungb826ypGR9XVVcnuS9wdpKvVtU5d3iTJU5ZPcrT7xr70hyxz5YV29e67Vd2f6tlrmM8yn83kiRJkqTVt1WdZlV1IbBhjkX7z7P+McAxc5SfD+w9R/kPaZ1ukiRJI2y+aarncjCzpmasqqvbz2uTfIhuusc7dJotdcrqUZ5+19iX5nlHfmzF9nXEPls47qKtvfZu9c01dfoo/91IkiRp8JLsCZwM/BTwv8DxVfXmwUYlaSUte3pGSZIk9e2LwF5JHtBG6B9MNw317SS5F/BL/GSqapLcPck9Z54DTwQuXpOoJalJsmeSzyb5SpJLkrx80DFJmizmIUlDYgtwRFX9LLAf8OIkDxlwTJJW0PBfIipJkjTiqmpLkpcAnwS2AU5oU1e/sC1/V1v1/wCfqqqbezZfB3you/Ur2wKnVNUn1i56SQJ+0kD0pdaRf0GSs6vq0kEHJmlimIckDVxVXQNc057flOQrdPerNhdJY8JOM0mSpDVQVWcBZ80qe9es1yfS3ee1t+xy4OGrHJ4kLcgGIkmDZh6SNGySrAceCZw34FAkrSA7zSRJkiRJfbOBSNKgmYckDVqSewAfAF5RVd+btexw4HCAdevWMTU1tej+1m3f3Tt4JfXzvss1PT29qvtfSca6OkYp1qWy00ySJEmS1JeFGoja8oE3Eq30l/dhbxAY5viGOTYwvlE1CnkIVrexeqVN8t/aJNcdrP9yJbkzXR56X1V9cPbyqjoeOB5gw4YNtXHjxkX3+db3fYTjLlrZpvorD1n8fZdramqKfuo1DIx1dYxSrEtlp5kkSZIkaVGLNRDBcDQSrXQD0bA3CAxzfMMcGxjfKBqVPASr21i90ib5b22S6w7WfznS3Wz6PcBXquoNg45H0sq706ADkCRJkiQNNxuIJA2aeUjSkHgc8Bzgl5Nc2B5PHnRQklaOI80kSZIkjZ31R37sDmVH7LOF581R3o8rj33K1oY06mYaiC5KcmEre1VVnTW4kCRNGPOQpIGrqs8BGXQcklaPnWaSJEmSpAXZQCRp0MxDkiRpLTg9oyRJkiRJkiRJkiaenWaSJEmSJEmSJEmaeHaaSZIkSZIkSZIkaeLZaSZJkiRJkiRJkqSJZ6eZJEmSJEmSJEmSJp6dZpIkSZIkSZIkSZp4dppJkiRJkiRJkiRp4tlpJkmSJEmSJEmSpIm31Z1mSbZJ8h9JPtpe75zk7CRfaz936ln3qCSbklyW5ICe8kcnuagte0uStPLtkry/lZ+XZP3WxitJkiRJkiRJkiTNthIjzV4OfKXn9ZHAp6tqL+DT7TVJHgIcDDwUOBB4R5Jt2jbvBA4H9mqPA1v5YcD1VfVA4I3A61cgXkmSJEmSJEmSJOl2tqrTLMkewFOAd/cUHwSc1J6fBDy9p/y0qrqlqq4ANgH7JtkV2KGqzq2qAk6etc3Mvs4A9p8ZhSZJkjRKkhzYRttvSnLkHMs3JrkxyYXt8ep+t5UkSZIkSdLW23Yrt38T8EfAPXvK1lXVNQBVdU2S+7by3YHP96y3uZX9uD2fXT6zzVVtX1uS3AjcG/jOVsYtSZK0Ztro+rcDv0p3rvPFJGdW1aWzVv3XqnrqMrfVmFh/5McGHYIkSZIkSRNp2Z1mSZ4KXFtVFyTZ2M8mc5TVAuULbTM7lsPppndk3bp1TE1NLRrM9PQ0R+xz66LrLUU/77sapqenB/beK826DKdxqoskDci+wKaquhwgyWl0I+r76fjamm0lSZIkSZLUp60ZafY44NeSPBm4K7BDkn8AvpVk1zbKbFfg2rb+ZmDPnu33AK5u5XvMUd67zeYk2wL3Aq6bHUhVHQ8cD7Bhw4bauHHjosFPTU1x3Odu7rOq/bnykMXfdzVMTU3RT51HgXUZTuNUF0kakNtGzzebgcfMsd5jk3yZ7lzoD6rqkiVsK0mSJEmSpK2w7E6zqjoKOAq6e3DQNew8O8lfA4cCx7afH2mbnAmckuQNwG7AXsAXqurWJDcl2Q84D3gu8NaebQ4FzgWeAXym3fdMkiRplPQzev5LwP2rarpdlPRhuvOlvkbew9JH34/ySOJxjv2IfbasXTDLsG774Y9xPlsT+6j+vUmSJEmS+re19zSby7HA6UkOA74OPBOgqi5JcjrdVEJbgBdX1cz8iC8CTgS2Bz7eHgDvAd6bZBPdCLODVyFeSZKk1TbfiPvbVNX3ep6fleQdSXbpZ9ue7ZY0+n6URxKPc+zPG/J7mh2xzxaOu2g1vkasvq2JfVCzSkiSJEmS1s6KfNutqilgqj3/LrD/POsdAxwzR/n5wN5zlP+Q1ukmSZI0wr4I7JXkAcA36C4E+q3eFZL8FPCtqqok+wJ3Ar4L3LDYtpIkSZIkSdp6o3mJqCRJ0gipqi1JXgJ8EtgGOKGNwn9hW/4uuqmoX5RkC/AD4OA2LfWc2w6kIpIkSZIkSWPMTjNJYynJCcBTgWurau9WtjPwfmA9cCXwG1V1fVt2FHAYcCvwsqr6ZCt/ND+ZPvYs4OXeW1HSclTVWXR5pLfsXT3P3wa8rd9tJUmSJEmStLLuNOgAJGmVnAgcOKvsSODTVbUX8On2miQPoZvu7KFtm3ck2aZt807gcGCv9pi9T0mSJEmSJEnSGLDTTNJYqqpzgOtmFR8EnNSenwQ8vaf8tKq6paquADYB+ybZFdihqs5to8tO7tlGkiRJkiRJkjRG7DSTNEnWVdU1AO3nfVv57sBVPettbmW7t+ezyyVJkiRJkiRJY8Z7mkkSZI6yWqB87p0kh9NN5ci6deuYmppa8E2np6c5Yp9b+4+yD4u953xxLGe7lWQMxjBsMUiSJEmSJGny2GkmaZJ8K8muVXVNm3rx2la+GdizZ709gKtb+R5zlM+pqo4HjgfYsGFDbdy4ccFgpqamOO5zNy+1Dgu68pCF33O+OBaLdbUZgzEMWwySJEmSJEmaPE7PKGmSnAkc2p4fCnykp/zgJNsleQCwF/CFNoXjTUn2SxLguT3bSJIkSZIkSZLGiCPNJI2lJKcCG4FdkmwGjgaOBU5PchjwdeCZAFV1SZLTgUuBLcCLq2pm3sQXAScC2wMfbw9JkiRJkiRJ0pix00zSWKqqZ82zaP951j8GOGaO8vOBvVcwNEmSpJGU5ATgqcC1VeX5kaQ1Zx6SNGjmIWn8OT2jJEmSJKkfJwIHDjoISRPtRMxDkgbrRMxD0liz00ySJEmStKiqOge4btBxSJpc5iFJg2YeksafnWaSJEmSJEmSJEmaeN7TTJIkSZK0IpIcDhwOsG7dOqamphbdZt32cMQ+W1Yshn7ecymmp6dXfJ8raZjjG+bYwPjG1TDkIVj5XLSaJvlvbZLrDtZ/tUxCHhqlvx1jXR2jFOtS2WkmSZIkSVoRVXU8cDzAhg0bauPGjYtu89b3fYTjLlq5r6ZXHrL4ey7F1NQU/dRjUIY5vmGODYxvXA1DHoKVz0WraZL/1ia57mD9V8sk5KFR+tsx1tUxSrEuldMzSpIkSZIkSZIkaeLZaSZJkiRJWlSSU4FzgQcn2ZzksEHHJGmymIckDZp5SBp/Ts8oSZIkSVpUVT1r0DFImmzmIUmDZh6Sxt+yR5ol2TPJZ5N8JcklSV7eyndOcnaSr7WfO/Vsc1SSTUkuS3JAT/mjk1zUlr0lSVr5dkne38rPS7J+K+oqSZIkSZIkSZIkzWlrpmfcAhxRVT8L7Ae8OMlDgCOBT1fVXsCn22vasoOBhwIHAu9Isk3b1zuBw4G92uPAVn4YcH1VPRB4I/D6rYhXkiRpYJIc2C4c2pTkyDmWH5LkP9vj35M8vGfZle0CowuTnL+2kUuSJEmSJE2GZXeaVdU1VfWl9vwm4CvA7sBBwElttZOAp7fnBwGnVdUtVXUFsAnYN8muwA5VdW5VFXDyrG1m9nUGsP/MKDRJkqRR0S4UejvwJOAhwLPaBUW9rgB+qaoeBrwOOH7W8idU1SOqasOqByxJkiRJkjSBtmak2W3atImPBM4D1lXVNdB1rAH3bavtDlzVs9nmVrZ7ez67/HbbVNUW4Ebg3isRsyRJ0hraF9hUVZdX1Y+A0+guDrpNVf17VV3fXn4e2GONY5QkSZIkSZpo227tDpLcA/gA8Iqq+t4CA8HmWlALlC+0zewYDqeb3pF169YxNTW1SNQwPT3NEfvcuuh6S9HP+66G6enpgb33SrMuw2mc6iJJAzLXxUOPWWD9w4CP97wu4FNJCvjbqpo9Ck2SJEmSJElbaas6zZLcma7D7H1V9cFW/K0ku1bVNW3qxWtb+WZgz57N9wCubuV7zFHeu83mJNsC9wKumx1Hazg6HmDDhg21cePGRWOfmpriuM/d3E81+3blIYu/72qYmpqinzqPAusynMapLpI0IH1dCASQ5Al0nWaP7yl+XFVdneS+wNlJvlpV58yx7ZIuJBrliyLGOfYj9tmydsEsw7rthz/G+WxN7KP69yZJkiRJ6t+yO83avcXeA3ylqt7Qs+hM4FDg2PbzIz3lpyR5A7AbsBfwhaq6NclNSfajm97xucBbZ+3rXOAZwGfafc8kSZJGyXwXD91OkocB7waeVFXfnSmvqqvbz2uTfIhuusc7dJot9UKiUb4oYpxjf96RH1u7YJbhiH22cNxFWz1hxUBsTeyDukBOkiRJkrR2tubb7uOA5wAXJbmwlb2KrrPs9CSHAV8HnglQVZckOR24FNgCvLiqZuZHfBFwIrA93VREM9MRvQd4b5JNdCPMDt6KeCVJkgbli8BeSR4AfIPunOa3eldIcj/gg8Bzquq/esrvDtypqm5qz58IvHbNIpckSZIkSZoQy+40q6rPMfdUQwD7z7PNMcAxc5SfD+w9R/kPaZ1ukiRJo6qqtiR5CfBJYBvghHZB0Qvb8ncBrwbuDbyj3SN2S1VtANYBH2pl2wKnVNUnBlANSZIkSZKksTaa86pIkiSNmKo6CzhrVtm7ep7/NvDbc2x3OfDwVQ9QkiRJkiRpwt1p0AFIkiRJkiRJkiRJg2anmSRJkiRJkiRJkiaenWaSJEmSJEmSJEmaeHaaSZIkSZIkSZIkaeLZaSZJkiRJkiRJkqSJt+2gA5AkrZz1R35sydscsc8WnjfPdlce+5StDUmSJEmSJEmSRoIjzSRJkiRJkiRJkjTx7DSTJEmSJEmSJEnSxLPTTJIkSZIkSZIkSRPPTjNJkiRJkiRJkiRNPDvNJEmSJEmSJEmSNPHsNJMkSZIkSZIkSdLEs9NMkiRJkiRJkiRJE89OM0mSJEmSJEmSJE28bQcdwDhZf+THVnR/Vx77lBXdnyRJkiRJkiRJkuZmp5kkSZK0FZZ64dQR+2zheSt8sZUkSZIkSdp6IzE9Y5IDk1yWZFOSIwcdj6TJYx6StLUWyyPpvKUt/88kj+p3W0laC+YiSYNmHpI0DMxF0ngb+pFmSbYB3g78KrAZ+GKSM6vq0sFGJmlSTHIeWulpZ8GpZzWZ+swjTwL2ao/HAO8EHjPJOUjS8DAXSRo085CkYWAuksbfKIw02xfYVFWXV9WPgNOAgwYck6TJYh6StLX6ySMHASdX5/PAjkl27XNbSVpt5iJJg2YekjQMzEXSmBv6kWbA7sBVPa830119LUlrxTy0guYavTYM9/eZicGRcFol/eSRudbZvc9tJWm1mYskDZp5SNIwMBdJY24UOs0yR1ndboXkcODw9nI6yWV97HcX4DtbGduqyuv7XnXo67IE1mU49VuX+692IAOyaB6CZeWiofgbedkQxDFMMSwh966GgR+HMYhhWPNQP3lkvnX6ykEwunlomUY29mHIeVtjlOPfmtiX8P9hWPPQ1lqt8yFY4b+pVfhfPux/88Mc3zDHBuMbn3loCNqIBvy9YqmG/bOwmia57rB69R/XPAQj1Fa9ynlolD47xro6hj3WZeehUeg02wzs2fN6D+Dq3hWq6njg+KXsNMn5VbVh68MbPOsynKzLWFk0D8HSc9GwHNdhiMMYjGHYYlgF/eSR+da5Sx/bAqObh5bD2AdnlOMf5diHwKqcD8Hw/16Mb/mGOTYwvhE0sXlotU1y/Se57mD9l8m2akYrXmNdHaMU61KNwj3NvgjsleQBSe4CHAycOeCYJE0W85CkrdVPHjkTeG46+wE3VtU1fW4rSavNXCRp0MxDkoaBuUgac0M/0qyqtiR5CfBJYBvghKq6ZMBhSZog5iFJW2u+PJLkhW35u4CzgCcDm4DvA89faNsBVEPSBDMXSRo085CkYWAuksbf0HeaAVTVWXQNSStpSUNkh5x1GU7WZYyMeR4ahjiMoWMMnWGIYcXNlUdaZ9nM8wJe3O+2K2SUj7WxD84oxz/KsQ/cBOci41u+YY4NjG/kTHAeWm2TXP9JrjtY/2UZ8zaifo1SvMa6OkYp1iVJ1z4jSZIkSZIkSZIkTa5RuKeZJEmSJEmSJEmStKomstMsyYFJLkuyKcmRg45ntiR7Jvlskq8kuSTJy1v5zknOTvK19nOnnm2OavW5LMkBPeWPTnJRW/aWJBlQnbZJ8h9JPjrKdUmyY5Izkny1/X4eO8J1+b3293VxklOT3HVU6zKKVjMPJTkhybVJLu4pW9Pf7TDksfY3/YUkX24x/NmAjsXA81+SK9v2FyY5f0DHYWzy5zCb5/P/iCSfn/n9J9m3Z9lQHeelxJ9kfZIftPILk7yrZ5s1j3+e2B+e5NwWyz8l2aFn2dAc+6XEPoTHfeD/b9S/LHL+k85b2vL/TPKoIYptY5Ibe/72X71WsbX3v8PndNbyQR67xWIb9LGbM0/MWmeQx6+f+AZ6DMfdYp//cbbY53ec9fPZG1eZ57uyBmdU8tAofm4yq01mWGWONpNBx7SQzNGuPOiYVlRVTdSD7gaN/w38NHAX4MvAQwYd16wYdwUe1Z7fE/gv4CHAXwFHtvIjgde35w9p9dgOeECr3zZt2ReAxwIBPg48aUB1+n3gFOCj7fVI1gU4Cfjt9vwuwI6jWBdgd+AKYPv2+nTgeaNYl1F8rHYeAn4ReBRwcU/Zmv5uGYI81ta/R3t+Z+A8YL8BHIuB5z/gSmCXWWVrfRzGIn8O+4O5P/+fmjlOwJOBqWE9zkuMf33verP2s+bxzxP7F4Ffas9fALxuGI/9EmMftuM+8P83Pvr+XS16/tM+4x9vv4P9gPOGKLaNtP/lAzp+d/icDsOx6zO2QR+7OfPEEB2/fuIb6DEc50c/n/9xfiz2+R3nRz+fvXF9MM935UHHNamPUcpDo/i5YVabzLA+mKPNZNAxLRDrnO3Kg45rJR+TONJsX2BTVV1eVT8CTgMOGnBMt1NV11TVl9rzm4Cv0P0xHkT3AaL9fHp7fhBwWlXdUlVXAJuAfZPsCuxQVedW9xd8cs82aybJHsBTgHf3FI9cXdJdYf2LwHsAqupHVXUDI1iXZltg+yTbAncDrmZ06zJqVjUPVdU5wHWzitf0dzsMeaw60+3lnduj1jKGIc9/a3kcxi1/Dq15Pv8FzIxwuhddvochPM5LjH9Og4p/ntgfDJzTnp8N/Hp7PlTHfomxz2mAsQ/8/4361s/5z0HAye1/+OeBHdvvZhhiG6h5Pqe9BnXs+oltoBbIE70Gefz6iU+rZ+g//6tp2D+/q2mSP3sLfFfWYIxMHhq1z808bTJDZ4E2k2E2V7vy2JjETrPdgat6Xm9muD/c64FH0l31sa6qroEuSQH3bavNV6fd2/PZ5WvtTcAfAf/bUzaKdflp4NvA37dhve9OcndGsC5V9Q3gb4CvA9cAN1bVpxjBuoyoQeShgf1uB5nH2jD8C4FrgbOraq1jeBPDkf8K+FSSC5IcPoA4xiZ/jqhXAH+d5Cq63H9UKx+V4/wK5o4f4AHtb+pfkvxCKxum+C8Gfq09fyawZ3s+Csd+vthhSI/7mJw3j7N+zn8G9V2t3/d9bJvK6uNJHroGcS3FsH/PHYpjNytP9BqK47dAfDAkx3AMDcXvXoO1yGdvLM3zXVmDMZJ5aEQ+N2/ijm0yw2i+NpOhtEC78tiYxE6zue5NMJRXUyS5B/AB4BVV9b2FVp2jrBYoXzNJngpcW1UX9LvJHGVDURe6HvRHAe+sqkcCN9NN9zOfoa1Luvt6HEQ3LdFuwN2TPHuhTeYoG4q6jKhhOm6r+rsddB6rqlur6hHAHnQjCfZeqxiGLP89rqoeBTwJeHGSX1zjOMYmf46oFwG/V1V7Ar9Hu3qN0TnO88V/DXC/9jf1+8Ap7Qq9YYr/BXSfuQvopi/5USsfhWM/X+xDedwH/f9GfennGA/q99DP+34JuH9VPRx4K/Dh1Q5qiYb5b3gojt0ieWLgx2+R+IbiGI6pgf/uNVhLOIcYK0v8rqzVNXJ5aBQ+N8tokxmkpbaZDNQy2pVHziR2mm3m9lfK7sEQDh9Mcme65PO+qvpgK/7WzBQR7ee1rXy+Om1uz2eXr6XHAb+W5Eq64cW/nOQfGM26bAY291x9cwZdQhvFuvwKcEVVfbuqfgx8EPh5RrMuo2gQeWjNf7fDlMfasPYp4MA1jGFo8l9VXd1+Xgt8iG76h7WMY5zy5yg6lC7PA/wj3e8fRuc4zxl/m17vu+35BXT3AXgQQxR/VX21qp5YVY8GTqWLEUbg2M8X+zAe92H6f6MF9XP+M6jvaou+b1V9b2Yqq6o6C7hzkl3WILZ+De333GE4dvPkiV4DPX6LxTcMx3CMDe1nR6uvj9ww9mZ9V9ZgjFQeGqHPzXxtMsNovjaTYTVfu/LYmMROsy8CeyV5QJK7AAcDZw44pttJErqrqL9SVW/oWXQmXcMR7edHesoPTrJdkgcAewFfaFPR3JRkv7bP5/Zssyaq6qiq2qOq1tMd689U1bNHtC7fBK5K8uBWtD9wKSNYF7rhs/sluVuLYX+6eYhHsS6jaBB5aE1/t8OQx5LcJ8mO7fn2dP/Uv7pWMQxL/kty9yT3nHkOPJFu2rU1i2PM8ucouhr4pfb8l4GvteejcpznjL99xrdpz3+aLv7Lhyn+JPdtP+8E/AnwrrZo6I/9fLEP23Efhv836ls/5z9nAs9NZz+6qV6uGYbYkvxU+9sgyb5036W/uwax9WtQx25Rgz52C+SJXgM7fv3EN+hjOOaGvo1Iq6PP3DCWFviurMEYmTw0Sp+bBdpkhs4CbSbDar525fFRVRP3AJ4M/BfdlbF/POh45ojv8XTDcP8TuLA9ngzcG/g0XWPRp4Gde7b541afy4An9ZRvoGsc/W/gbUAGWK+NwEfb85GsC/AI4Pz2u/kwsNMI1+XP6E6KLgbeC2w3qnUZxcdq5iG6EQHXAD+mu1rlsLX+3Q5DHgMeBvxHi+Fi4NWtfM3/zhlg/qObG/vL7XHJzN/bAOJ4BGOSP4f5wdyf/8cDF7S/gfOARw/rcV5K/MCvt7/pL9NNW/W0QcY/T+wvp8v1/wUc2xvHMB37pcQ+hMd94P9vfCzp93WH8x/ghcAL2/MAb2/LLwI2DFFsL+n52/888PNrfOzm+pwOy7FbLLZBH7v58sSwHL9+4hvoMRz3x1yf/0l5zPX5HXRMa1j3OT97g45rjeo+53dlHwP9nYxEHhrVzw09bTLD+mCONpNBx7RIvHdoVx50TCv5mPnyK0mSJEmSJEmSJE2sSZyeUZIkSZIkSZIkSbodO80kSZIkSZIkSZI08ew0kyRJkiRJkiRJ0sSz00ySJEmSJEmSJEkTz04zSZIkSZKkAUtyQpJrk1zcx7r3S/LZJP+R5D+TPHktYpQ0/sxFkgZtiXnojUkubI//SnLDVr9/VW3tPiRJkiRJkrQVkvwiMA2cXFV7L7Lu8cB/VNU7kzwEOKuq1q9BmJLGnLlI0qAtJQ/N2u6lwCOr6gVb8/6ONJMkSZIkSRqwqjoHuK63LMnPJPlEkguS/GuS/29mdWCH9vxewNVrGKqkMWYukjRoS8xDvZ4FnLq177/t1u5AkiRJkiRJq+J44IVV9bUkjwHeAfwy8BrgU+2K6rsDvzK4ECVNAHORpEGbLw8BkOT+wAOAz2ztG9lpJkmSJEmSNGSS3AP4eeAfk8wUb9d+Pgs4saqOS/JY4L1J9q6q/x1AqJLGmLlI0qAtkodmHAycUVW3bu372WkmSZIkSZI0fO4E3FBVj5hj2WHAgQBVdW6SuwK7ANeuXXiSJoS5SNKgLZSHZhwMvHil3kySJEmSJElDpKq+B1yR5JkA6Ty8Lf46sH8r/1ngrsC3BxKopLFmLpI0aIvkIZI8GNgJOHcl3s9OM0mSJEmSpAFLcipdY8+Dk2xOchhwCHBYki8DlwAHtdWPAH6nlZ8KPK+qahBxSxov5iJJg7bEPATdVLGnrVT+iXlMkiRJkiRJkiRJk86RZpIkSZIkSZIkSZp4dppJkiRJkiRJkiRp4tlpJkmSJEmSJEmSpIlnp5kkSZIkSZIkSZImnp1mkiRJkiRJkiRJmnh2mkmSJEmSJEmSJGni2WkmSZIkSZIkSZKkiWenmSRJkiRJkiRJkiaenWaSJEmSJEmSJEmaeHaaSZIkSZIkSZIkaeLZaSZJkiRJkiRJkqSJZ6eZJEmSJEmSJEmSJp6dZpIkSZIkSZIkSZp4dppJkiRJkiRJkiRp4tlpJkmSJEmSJEmSpIlnp5kkSZIkSZIkSZImnp1mkiRJkiRJkiRJmnh2mkmSJEmSJEmSJGni2WkmSZIkSZIkSZKkiWenmSRJkiRJkiRJkiaenWaSJEmSJEmSJEmaeHaaSZIGKsnGJJv7XPc1Sf6hPb9fkukk26xuhJI03pKsT1JJtm2vp5L8dj/rbsV7VpIHbs0+JEmSJElaaXaa6XaSXJnkVwYdx1pZqFFI0u0NW36oqq9X1T2q6tZBxyJJq9kJtJSLCyRJkiRJ0vLZaSZJ0oTa2pEikvrn502SJEmSpOFnp5luk+S9wP2Af2pTnv1Rkn9M8s0kNyY5J8lD27p3SXJhkpe219sk+bckr26v901ybpIbklyT5G1J7tLzXpXk/yX5WpKbkrwuyc+0bb6X5PRZ6/9Okk1JrktyZpLdWvkdpgjqHT2W5HlJPpfkb5Jcn+SKJE9qy44BfgF4W6vv21b7GEujaoXzw/ZJTmyfyUuBn5v1Xrsl+UCSb7fP7MvmiWmu6cRe197rpiSfSrJLz/rPSfI/Sb6b5I97R861eP68Z93bjepYKKY2ZeTpSU5u73tJkg09y/dM8sG27XdbPtyu5bN9eta7b5IfJLlPkouTPK1n2Z2TfCfJI9rr/ZL8e8uxX06ysWfd5yf5Sovl8iS/O7teSV6Z5JvA3y/4i5eGxFyfo1b+gvb3fn2STya5f882leSF7Vzj+iRvT5K27IFJ/qXlr+8keX8rX+y8Yr7tzmmrf7nlyN+c6/OWZKckH231uL4936PnvXZO8vdJrm7LP5zk7sDHgd3avqdbTurnXGu++m+T7tzoO0kuB54yx2H/mSRfaHX9SJKd5/nd7Jbu3Oy6dOdqv9OzbJskr0ry3y0nXZBkzzn28fgkVyV5wsJ/CZIkSZIkrS47zXSbqnoO8HXgaW3Ks7+ia6TZC7gv8CXgfW3dHwHPBl6b5GeBI4FtgGPa7m4Ffg/YBXgssD/w/2a95YHAo4H9gD8CjgcOAfYE9gaeBZDkl4G/BH4D2BX4H+C0JVTtMcBlLZa/At6TJFX1x8C/Ai9p9X3JEvYpTZQVzg9HAz/THgcAh868T5I7Af8EfBnYnS53vCLJAX2G+lvA81tMdwH+oO33IcA7gecAuwH3BvaYZx+302dMv0aXl3YEzgRmGvS3AT5Kl7fWt+1Pq6pb2vrP7tnHs4B/rqpvAyfPWvZk4JqqujDJ7sDHgD8Hdm51/ECS+7R1rwWeCuzQjsUbkzyqZ18/1ba7P3B4P8dAGqT5PkdJng68Cvi/wH3o/qefOmvzp9J1zD+c7jxi5nP7OuBTwE50ueCtfYYz53ZV9Ytt+cNbjnx/ez3783Ynus7q+9NdiPADWr5o3gvcDXgoXR57Y1XdDDwJuLrt+x5VdTX9nWvNV//facseCWwAnjFHXZ8LvIAuZ24B3jLPMTkV2NzWewbwF0n2b8t+ny63PZkuJ70A+H7vxi2Xngr8elV9dp73kCRJkiRpTYxtp1mSE5Jcm+TiPtf/jSSXphshcMpqxzcqquqEqrqpNfC+Bnh4knu1ZRfTNdp+iK7R9jkz9xaqqguq6vNVtaWqrgT+FvilWbt/fVV9r6ouAS4GPlVVl1fVjXSN8Y9s6x0CnFBVX2pxHAU8Nsn6PqvxP1X1dy22k+g63tYt/WhI6rXc/EDXcHtMVV1XVVdx+4bYnwPuU1WvraofVdXlwN8BB/cZ1t9X1X9V1Q+A04FHtPJnAB+tqnNavH8K/G+f++wnps9V1Vmtju+la6AG2JeuIfkPq+rmqvphVX2uLTsJ+K3WKQddh9572/N/AJ6cZIc5lj0bOKu93/9W1dnA+XSN0lTVx6rqv6vzL3QN/L/QE+v/AkdX1S3tOGmMjcn50Hyfo98F/rKqvlJVW4C/AB7RO9oMOLaqbqiqrwOf5Sc54cd0HVe7zfpcLmap293u81ZV362qD1TV96vqJrqLCX4JIMmudJ1jL6yq66vqx+0zPKc+z7Xmq/9vAG+qqquq6jq6i5Nme29VXdw67f4U+I3WgXmbNmrs8cAr2/G4EHg3Xc4C+G3gT6rqspaTvlxV3+3ZxTPpLpp6clV9Yb66SpIkSZK0Vsa20ww4kW4k06KS7EXXEfO4qnoo8IrVC2t0tCl1jm1T6nwPuLIt2qVntZPorvo+q6q+1rPtg9qUQ99s2/7FrO0AvtXz/AdzvL5He74b3dXlAFTVNPBduivN+/HNnm1nrm6+xzzrSurD1uQHus/0VT2v/6fn+f3ppiC7YeZBN5Kk347ub/Y8/z63zyO3vWdrBO5tuF1IPzHNft+7ppvebU+6jvsts3daVecBNwO/lOT/Ax5IN0qNNork34BfT7IjXUP6+3rieeaseB5Pd0EASZ6U5PNtqrQb6DrTen8v366qH/ZZd42+Exn986H5Pkf3B97c8zm4Dgi3Pz+YLyf8UVv3C62D8AV9xrLU7W73eUtytyR/m26q2O8B5wA7ts6oPYHrqur6fgLp81yrr5zI7fPwjNnL7zzH/ndrMd80a92Z38GewH8vUI1XAKdX1UULrCNJkiRJ0poZ206zqjqHrvHkNunumfWJdj+Ff22NlNBNUfP2mUaKqrp2jcMdJtXz/LeAg4BfAe5F1/gNXWPRjHfQTZl0QJLH95S/E/gqsFdV7UDXwNy73VJcTdcw1r15d2+PewPfoGtwhm4qoxk/tYR91+KrSGpWKj9cQ9eQOuN+Pc+vAq6oqh17HvesqidvZey3e88kd6PLIzNuZv48sjUxXQXcLz33R5rlJLqRY88BzpjVmTWz7JnAuVX1jZ59vndWPHevqmOTbAd8APgbYF1V7Qicxe1/L+a9CTIm50PzfY6uAn531mdh+6r698V2WFXfrKrfqard6EasvSPJA1nkvGKB7eZ9q1mvjwAeDDymnR/NTOuYVp+dW0f5YvuBrTvXWigPz5i9/MfAd2atc3WL+Z6z1u3NVz+zQBzPBJ6e5BV9xCxJkiRJ0qob206zeRwPvLSqHk03Xdg7WvmDgAcl+bd2dX5fV2SPqW8BP92e3xO4hW40xt3ormC+TZLn0N2T7HnAy4CTktyjZ9vvAdOtMe5FWxHTKcDzkzyiNQj/BXBeVV1Z3b1/vgE8u418eQELN87M1ltfSQtbqfxwOnBUkp2S7AG8tGfTLwDfS/LKJNu3z/XeSX5uK2M/A3hqkscnuQvwWm7/P/BCuukQd07yU9x+hM3WxPQFusbpY5PcPcldkzyuZ/l7gf9D1zl28qxtPww8Cnj5rGX/ADwtyQEtlrsm2diO5V2A7YBvA1uSPAl4Yh9xarKM2vnQfJ+jd9HlkocCJLlXkmf2s8Mkz2yfGYDr6Tqlbl3svGK+7drrfs4p7kk3mv6GJDvT3eMRgKq6hm566ne0/HjnJDOdat8C7p02BW7PvpZ7rnU68LIkeyTZie7ek7M9O8lD2kUGr6Xr2L+1d4Xqptj9d+Av2+/lYcBh/GRk7LuB1yXZK52HJem9YOFqunuxvSzJ7PuxSZIkSZK05iam06w11v488I9JLqS778OubfG2wF7ARrqblb97nqt8J8FfAn/SpjnamW6KnW8AlwKfn1kpyf2ANwHPrarpqjqF7p46b2yr/AHdSJSb6O798/7lBlRVn6a7l8YH6BrNfobb30vod4A/pGu8fyhd402/3gw8I8n1Sea7wb2kzkrlhz9r215Bd7+tmXt10Rpkn0Z3350r6EY1vJtuNNuyVXfvxBfTdcJfQ9fYvblnlfcCX6abZvJT9OSsrYmpZ9sHAl9v7/mbPcs3A1+ia3j/11nb/oAu7z0A+GBP+VV0o/xeRdc5dhVdDrxTmyLtZXQN4tfT5eEzF4tTk2MUz4fm+xxV1YeA1wOntekJL6abyrQfPwecl2Sa7jPy8qq6oi1b6Lxioe1eQ3eBwA1JfmOe930TsD1dHvk88IlZy59DN6Lrq8C1tA78qvoqcCpwedv/bmzdudbfAZ+ky3tfoifH9Hgv3fSe3wTuSpdb5vIsutHGV9Pdx/Lo6u61CPAGunz0KboOvvfQ1f827X5r+wOvTPLbS6iDJEmSJEkrLlXjO0tTkvXAR6tq7yQ7AJdV1a5zrPcu4PNVdWJ7/WngyKr64lrGK0laO0muBH67qv55wHGcAFxdVX8yx7JXAw+qqmevfWQaF54PSZIkSZIk9WdiRppV1feAK2am7WlTxDy8Lf4w8IRWvgvd9ESXDyJOSdLkaJ0Z/5du9MXsZTvTTXN2/BqHpTHm+ZAkSZIkSdL8xrbTLMmpwLnAg5NsTnIYcAhwWJIvA5fQTW8F3fQ0301yKfBZ4A+r6ruDiFuSNBmSvI5uOrm/7pnebWbZ79BNu/jxqjpnEPFpPHg+JEmSJEmS1L+xnp5RkiRJkiRJkiRJ6sfYjjSTJEmSJEmSJEmS+rXtoANYabvsskutX7++r3Vvvvlm7n73u69uQGvMOo2Gca3TV7/61e9U1X0GHcsw6DcXjcLfgjGuDGNcGYvFeMEFF5iHmnHKQ2CcK804V1ZvnOYhSZIkSRpdY9dptn79es4///y+1p2ammLjxo2rG9Aas06jYVzr9IQnPOF/Bh3HsOg3F43C34IxrgxjXBmLxZhkbPNQkh2BdwN7AwW8oKrOnW/9ccpDYJwrzThXVm+c45yHJEmSJGncjV2nmSRJ0ph6M/CJqnpGkrsAdxt0QJIkSZIkSePETjNJkqQhl2QH4BeB5wFU1Y+AHw0yJkmSJEmSpHEzEp1mS52OSJIkacz8NPBt4O+TPBy4AHh5Vd3cu1KSw4HDAdatW8fU1NSiO56enu5rvUEzzpVlnCtrVOKUJEmSJC1sJDrNcDoiSZI02bYFHgW8tKrOS/Jm4EjgT3tXqqrjgeMBNmzYUP3cC2oU7xk1zIxzZRmnJEmSJGkt3WnQASymZzqi90A3HVFV3TDQoCRJktbWZmBzVZ3XXp9B14kmSZIkSZKkFTL0nWbcfjqi/0jy7iR3H3RQkiRJa6WqvglcleTBrWh/4NIBhiRJkiRJkjR2RmF6xkWnI1rO/TtgPO89YJ1Gw7jWSZK0ql4KvK9NVX058PwBxyNJkiRJkjRWRqHTbK7piI7sXWE59++A8bz3gHUaDeNaJ0nS6qmqC4ENg45DkiRJkiRpXA399IxORyRJkiRJkiRJkqTVNgojzWCVpiO66Bs38rwjP7YSuwLgymOfsmL7kqTlWL+COQ3Ma5KWzjwkSZIkSZJG1Uh0mjkdkSRJkiRJkiRJklbT0E/PKEmSJEmSJEmSJK02O80kSZIkSZIkSZI08ew0kyRJkiRJkiRJ0sSz00ySJEmSJEmSJEkTz04zSZIkSZIkSZIkTTw7zSRJkiRJkiRJkjTx7DSTJEmSJEmSJEnSxLPTTJIkSZIkSZIkSRPPTjNJkiRJkiRJkiRNPDvNJEmSJEmSJEmSNPHsNJMkSZIkSZIkSdLEs9NM0tBLckKSa5Nc3FO2c5Kzk3yt/dypZ9lRSTYluSzJAT3lj05yUVv2liRp5dsleX8rPy/J+p5tDm3v8bUkh65RlSVJkiRJkiRJa8xOM0mj4ETgwFllRwKfrqq9gE+31yR5CHAw8NC2zTuSbNO2eSdwOLBXe8zs8zDg+qp6IPBG4PVtXzsDRwOPAfYFju7tnJMkSZIkSZIkjQ87zSQNvao6B7huVvFBwEnt+UnA03vKT6uqW6rqCmATsG+SXYEdqurcqirg5FnbzOzrDGD/NgrtAODsqrquqq4HzuaOnXeSJEmSJEmSpDFgp5mkUbWuqq4BaD/v28p3B67qWW9zK9u9PZ9dfrttqmoLcCNw7wX2JUmSJEmSJEkaM9sOOgBJWmGZo6wWKF/uNnd84+RwuukfWbduHVNTUwsGCjA9Pd3Xev06Yp8tK7YvgKmpqRWPcTUY48owRkmSJEmSJE0yO80kjapvJdm1qq5pUy9e28o3A3v2rLcHcHUr32OO8t5tNifZFrgX3XSQm4GNs7aZmi+gqjoeOB5gw4YNtXHjxvlWvc3U1BT9rNev5x35sRXbF8CVh2xc8RhXgzGuDGOUJEmSJEnSJHN6Rkmj6kzg0Pb8UOAjPeUHJ9kuyQOAvYAvtCkcb0qyX7tf2XNnbTOzr2cAn2n3Pfsk8MQkOyXZCXhiK5MkSZIkSZIkjRlHmkkaeklOpRvxtUuSzcDRwLHA6UkOA74OPBOgqi5JcjpwKbAFeHFV3dp29SLgRGB74OPtAfAe4L1JNtGNMDu47eu6JK8DvtjWe21VXbeKVZUkSZIkSZIkDYidZpKGXlU9a55F+8+z/jHAMXOUnw/sPUf5D2mdbnMsOwE4oe9gJUmSJEmSJEkjyekZJUmSJEmSJEmSNPHsNJMkSZIkSZIkSdLEs9NMkiRJkiRJkiRJE897mkmSJI2AJFcCNwG3AluqasNgI5IkSZIkSRovdppJkiSNjidU1XcGHYQkSZIkSdI4cnpGSZIkSZIkSZIkTbyRGGnmdESSJEkU8KkkBfxtVR0/6IAkSZIkSZLGyUh0mjVORyRJkibZ46rq6iT3Bc5O8tWqOqd3hSSHA4cDrFu3jqmpqUV3Oj093dd6/Tpiny0rti/gtthWOs7VYpwryzglSZIkSWtplDrNJEmSJlZVXd1+XpvkQ8C+wDmz1jkeOB5gw4YNtXHjxkX3OzU1RT/r9et5R35sxfYFcOUhG4GVj3O1GOfKMk5JkiRJ0loalXuazUxHdEG7glqSJGliJLl7knvOPAeeCFw82KgkSZIkSZLGy6iMNFtwOqLlTEUEsG77lZ1CaBimZBnHqWGs02iYnp4edAiSNM7WAR9KAt352ylV9YnBhiRJkiRJkjReRqLTbLHpiJYzFRHAW9/3EY67aOUOwcz0QYM0jlPDWKfRMG6dgJI0TKrqcuDhg45DkiRJkiRpnA399IxORyRJkiRJkiRJkqTVNgojzZyOSJIkSZIkSZIkSatq6DvNnI5IkiRJkiRJkiRJq23op2eUJEmSJEmSJEmSVpudZpIkSZIkSZIkSZp4dppJkiRJkiRJkiRp4tlpJkmSJEmSJEmSpIlnp5kkSZIkSZIkSZImnp1mkiRJkiRJkiRJmnh2mkmSJEmSJEmSJGni2WkmSZIkSZIkSZKkiWenmSRJkiRJkiRJkiaenWaSJEmSJEmSJEmaeHaaSZIkSZIkSZIkaeLZaSZJkiRJkiRJkqSJZ6eZJEmSJEmSJEmSJp6dZpIkSZIkSZIkSZp4dppJGmlJfi/JJUkuTnJqkrsm2TnJ2Um+1n7u1LP+UUk2JbksyQE95Y9OclFb9pYkaeXbJXl/Kz8vyfoBVFOSJEmSJEmStMrsNJM0spLsDrwM2FBVewPbAAcDRwKfrqq9gE+31yR5SFv+UOBA4B1Jtmm7eydwOLBXexzYyg8Drq+qBwJvBF6/BlWTJEmSJEmSJK0xO80kjbptge2TbAvcDbgaOAg4qS0/CXh6e34QcFpV3VJVVwCbgH2T7ArsUFXnVlUBJ8/aZmZfZwD7z4xCkyRJkiRJkiSNDzvNJI2sqvoG8DfA14FrgBur6lPAuqq6pq1zDXDftsnuwFU9u9jcynZvz2eX326bqtoC3AjcezXqI0mSJEmSJEkanG0HHYAkLVe7V9lBwAOAG4B/TPLshTaZo6wWKF9om7niOZxuikfWrVvH1NTUAqF0pqen+1qvX0fss2XF9gUwNTW14jGuBmNcGcYoSZIkSZKkSWanmaRR9ivAFVX1bYAkHwR+HvhWkl2r6po29eK1bf3NwJ492///7d1/jGVneR/w74MXjAMxP0LYOl63JmVDYoyAsEVOrKIJjoqToJg/oNrICXZkdSXiUGidROv8g6LKUpAKgRBAXWFiAwbiOqS2MJBYhilJRUz4lRjbsViBhTc4OA0/pxImC0//mLMwXmZ3x7t35tw75/ORRnPve99z9ntm733nzHnOec+urE7neGh4fHT72mUODVNAPiHJV9YL090HkhxIkj179vTS0tIJN2B5eTkb6bdRl++/dWbrSpL7Ll2aecbNIONsyAgAAADAlJmeEVhkX0xyQVX90HCfsYuS3JPkliSXDX0uS3Lz8PiWJHur6vSqelqS3Uk+Pkzh+M2qumBYz8uPWubIul6a5MPDfc8AAAAAANhGXGkGLKzuvqOqbkryqSSHk3w6q1d6PT7JjVV1RVYLay8b+t9VVTcmuXvof2V3f2dY3SuSXJfkjCQfHL6S5Nok76yqg1m9wmzvFmwaAAAAAABbTNEMWGjd/Zokrzmq+aGsXnW2Xv9rklyzTvsnkpy/Tvu3MhTdAAAAAADYvkzPCAAAAAAAwOQpmgEALIiqOq2qPl1V7x87CwAAAMB2o2gGALA4XpXknrFDAAAAAGxHC1M0c2Y1ADBlVbUryS8ledvYWQAAAAC2ox1jB3gEjpxZfebYQQAARvCGJL+T5IeP1aGq9iXZlyQ7d+7M8vLyCVe6srKyoX4bddWzDs9sXUm+l23WOTeLnLMlJwAAAFtpIYpma86svibJfx05DgDAlqqqFyd5sLs/WVVLx+rX3QeSHEiSPXv29NLSMbt+z/LycjbSb6Mu33/rzNaVJPddupRk9jk3i5yzJScAAABbaVGmZ3xDVs+s/u7IOQAAxnBhkl+uqvuSvDfJC6vqXeNGAgAAANhe5v5Ks42cWX0yUxElyc4zZjuF0DxMybIdp4axTYthZWVl7AgA21Z3X53k6iQZ9od+q7t/dcxMAAAAANvN3BfN8v0zq38xyWOTnFlV71p7oOhkpiJKkjfdcHNed+fsfgRHpg8a03acGsY2LYbtVgQEAAAAAGBa5n56xu6+urt3dfe5SfYm+bAzqwGAqeru5e5+8dg5AAAAALabuS+aAQAAAAAAwGZbhOkZv6e7l5MsjxwDAAAAAACAbcaVZgAAAAAAAEyeohkAAAAAAACTp2gGAAAAAADA5CmaAQAAAAAAMHmKZgAAAAAAAEyeohkAAAAAAACTp2gGAAAAAADA5CmaAQAAAAAAMHmKZgAAAAAAAEyeohkAAAAAAACTp2gGAAAAAADA5CmaAQAAAAAAMHmKZgAAAAAAAEyeohkAAAAAAACTp2gGAAAAAADA5CmaAQAAAAAAMHmKZgAAAAAAAEyeohmw0KrqiVV1U1X9fVXdU1U/U1VPrqrbqupzw/cnrel/dVUdrKp7q+pFa9qfV1V3Dq/9YVXV0H56Vf3J0H5HVZ07wmYCAAAAALDJFM2ARffGJB/q7p9M8uwk9yTZn+T27t6d5PbhearqvCR7kzwzycVJ3lJVpw3reWuSfUl2D18XD+1XJPlqdz89yR8kee1WbBQAAAAAAFtL0QxYWFV1ZpIXJLk2Sbr72939tSSXJLl+6HZ9kpcMjy9J8t7ufqi7v5DkYJLnV9VZSc7s7o91dyd5x1HLHFnXTUkuOnIVGgAAAAAA24eiGbDIfjzJPyX546r6dFW9raoel2Rndz+QJMP3pw79z05y/5rlDw1tZw+Pj25/2DLdfTjJ15P8yOZsDgAAAAAAY9kxdgCAU7AjyU8neWV331FVb8wwFeMxrHeFWB+n/XjL/ODKq/ZldYrH7Ny5M8vLy8eJsmplZWVD/Tbqqmcdntm6kmR5eXnmGTeDjLMhIwAAAABTpmgGLLJDSQ519x3D85uyWjT7clWd1d0PDFMvPrim/zlrlt+V5EtD+6512tcuc6iqdiR5QpKvrBemuw8kOZAke/bs6aWlpRNuwPLycjbSb6Mu33/rzNaVJPddujTzjJtBxtmQEQAAAIApMz0jsLC6+x+T3F9VzxiaLkpyd5Jbklw2tF2W5Obh8S1J9lbV6VX1tCS7k3x8mMLxm1V1wXC/spcftcyRdb00yYeH+54BAAAAALCNuNIMWHSvTHJDVT0myeeT/HpWTwi4saquSPLFJC9Lku6+q6puzGph7XCSK7v7O8N6XpHkuiRnJPng8JUk1yZ5Z1UdzOoVZnu3YqMA1qqqxyb5aJLTs7r/dlN3v2bcVAAAAADbi6IZsNC6+zNJ9qzz0kXH6H9NkmvWaf9EkvPXaf9WhqIbwIgeSvLC7l6pqkcn+auq+mB3//XYwQAAAAC2i7mfnrGqHltVH6+qv62qu6rq98bOBACwlXrVyvD00cOXqWIBAAAAZmjui2b5/pnVz07ynCQXV9UF40YCANhaVXVaVX0myYNJbuvuO0aOBAAAALCtzP30jN3dSZxZDQBM2nAPxudU1ROT/FlVnd/dn13bp6r2JdmXJDt37szy8vIJ17uysrKhfht11bMOz2xdSb6XbdY5N4ucsyUnAAAAW2nui2bJ6pnVST6Z5OlJ3uzMagBgqrr7a1W1nOTiJJ896rUDSQ4kyZ49e3ppaemE61teXs5G+m3U5ftvndm6kuS+S5eSzD7nZpFztuQEAABgKy1E0exEZ1afzFnVSbLzjNmeDT0PZ5dux7NcbdNiWFlZOXEnAE5KVf1okn8ZCmZnJPn5JK8dORYAAADAtrIQRbMjjnVm9cmcVZ0kb7rh5rzuztn9CI6cCT2m7XiWq21aDNutCAgwZ85Kcv1w9f2jktzY3e8fORMAAADAtjL3RTNnVgMAU9fdf5fkuWPnAAAAANjO5r5oFmdWAwAAAAAAsMnmvmjmzGoAAAAAAAA226PGDgAAAAAAAABjUzQDAAAAAABg8hTNAAAAAAAAmDxFMwAAAAAAACZP0QwAAAAAAIDJUzQDAAAAAABg8hTNAAAAAAAAmDxFMwAAAAAAACZP0QwAAAAAAIDJUzQDAAAAAABg8hTNAAAAAAAAmDxFMwAAAAAAACZP0QwAAAAAAIDJUzQDAAAAAABg8hTNAAAAAAAAmDxFMwAAAAAAACZP0QwAAAAAAIDJUzQDAAAAAABg8hTNAAAAAAAAmDxFMwAAAAAAACZP0QxYeFV1WlV9uqrePzx/clXdVlWfG74/aU3fq6vqYFXdW1UvWtP+vKq6c3jtD6uqhvbTq+pPhvY7qurcLd9AAAAAAAA2naIZsB28Ksk9a57vT3J7d+9OcvvwPFV1XpK9SZ6Z5OIkb6mq04Zl3ppkX5Ldw9fFQ/sVSb7a3U9P8gdJXru5mwIAAAAAwBgUzYCFVlW7kvxSkretab4kyfXD4+uTvGRN+3u7+6Hu/kKSg0meX1VnJTmzuz/W3Z3kHUctc2RdNyW56MhVaAAAAAAAbB+KZsCie0OS30ny3TVtO7v7gSQZvj91aD87yf1r+h0a2s4eHh/d/rBluvtwkq8n+ZGZbgEAAAAAAKPbMXYAgJNVVS9O8mB3f7KqljayyDptfZz24y2zXp59WZ3iMTt37szy8vIJA62srGyo30Zd9azDM1tXkiwvL88842aQcTZkBAAAAGDKFM2ARXZhkl+uql9M8tgkZ1bVu5J8uarO6u4HhqkXHxz6H0pyzprldyX50tC+a532tcscqqodSZ6Q5CvrhenuA0kOJMmePXt6aWnphBuwvLycjfTbqMv33zqzdSXJfZcuzTzjZpBxNmScX1V1Tlanjv1XWb2y9kB3v3HcVAAAAADby9xPz1hV51TVR6rqnqq6q6peNXYmYD5099Xdvau7z02yN8mHu/tXk9yS5LKh22VJbh4e35Jkb1WdXlVPS7I7yceHKRy/WVUXDPcre/lRyxxZ10uHf2PdK80ANtHhJFd1908luSDJlVV13siZAAAAALaVRbjS7MhBok9V1Q8n+WRV3dbdd48dDJhbv5/kxqq6IskXk7wsSbr7rqq6McndWR1bruzu7wzLvCLJdUnOSPLB4StJrk3yzqo6mNUrzPZu1UYAHDEU94/cq/GbVXVPVu+5aH8IAAAAYEbmvmjmIBGwEd29nGR5ePzPSS46Rr9rklyzTvsnkpy/Tvu3MhTdAOZBVZ2b5LlJ7hg5CgAAAMC2MvdFs7UcJAIApqyqHp/kT5O8uru/sc7r+5LsS5KdO3dmeXn5hOtcWVnZUL+NuupZh2e2riTfyzbrnJtFztmSEwAAgK20MEWz4x0kOpkDREmy84zZHtiZhz+Ut+Mf7LZpMaysrIwdAWBbq6pHZ3Vf6Ibuft96fbr7QJIDSbJnz55eWlo64XqXl5ezkX4bdfn+W2e2riS579KlJLPPuVnknC05AQAA2EoLUTQ70UGikzlAlCRvuuHmvO7O2f0IjhzUGdN2/IPdNi2G7VYEBJgnVVVZvcfiPd39+rHzAAAAAGxHjxo7wIk4SAQAkAuT/FqSF1bVZ4avXxw7FAAAAMB2sghXmh05SHRnVX1maPvd7v7AeJEAALZOd/9Vkho7BwAAAMB2NvdFMweJAAAAAAAA2GxzPz0jAAAAAAAAbDZFMwAAAAAAACZP0QwAAAAAAIDJUzQDAAAAAABg8hTNAAAAAAAAmDxFMwAAAAAAACZP0QwAAAAAAIDJUzQDAAAAAABg8hTNAAAAAAAAmDxFMwAAAAAAACZP0QwAAAAAAIDJUzQDAAAAAABg8hTNAAAAAAAAmDxFMwAAAAAAACZP0QwAAAAAAIDJUzQDAAAAAABg8hTNAAAAAAAAmDxFMwAAAAAAACZP0QwAAAAAAIDJUzQDAAAAAABg8hTNAAAAAAAAmDxFMwAAAAAAACZP0QwAAAAAAIDJUzQDFlZVnVNVH6mqe6rqrqp61dD+5Kq6rao+N3x/0pplrq6qg1V1b1W9aE3786rqzuG1P6yqGtpPr6o/GdrvqKpzt3xDAQAAAADYdIpmwCI7nOSq7v6pJBckubKqzkuyP8nt3b07ye3D8wyv7U3yzCQXJ3lLVZ02rOutSfYl2T18XTy0X5Hkq9399CR/kOS1W7FhAAAAAABsLUUzYGF19wPd/anh8TeT3JPk7CSXJLl+6HZ9kpcMjy9J8t7ufqi7v5DkYJLnV9VZSc7s7o91dyd5x1HLHFnXTUkuOnIVGsBWqqq3V9WDVfXZsbMAAAAAbEdzXzRzgAjYiGHaxOcmuSPJzu5+IFktrCV56tDt7CT3r1ns0NB29vD46PaHLdPdh5N8PcmPbMpGABzfdfn+VbAAAAAAzNiOsQNswHVJ/iirV34A/ICqenySP03y6u7+xnEuBFvvhT5O+/GWWS/HvqxO8ZidO3dmeXn5OKlXraysbKjfRl31rMMzW1eSLC8vzzzjZpBxNmScb939UfdVBAAAANg8c180c4AIOJ6qenRWC2Y3dPf7huYvV9VZ3f3AMPXig0P7oSTnrFl8V5IvDe271mlfu8yhqtqR5AlJvrJelu4+kORAkuzZs6eXlpZOmH95eTkb6bdRl++/dWbrSpL7Ll2aecbNIONsyAgAAADAlM190QzgWIZ7i12b5J7ufv2al25JclmS3x++37ym/d1V9fokP5Zkd5KPd/d3quqbVXVBVqd3fHmSNx21ro8leWmSDw/3PQOYO9v1itdkca4ylHO25AQAAGArbYui2ckcIEqSnWfM9sDOPPyhvB3/YLdNi2FlZWWMf/bCJL+W5M6q+szQ9rtZLZbdWFVXJPlikpclSXffVVU3Jrk7yeEkV3b3d4blXpHV6WDPSPLB4StZLcq9s6oOZvUKs72bvE0AJ227XvGaLM5VhnLOlpwAAABspW1RNDuZA0RJ8qYbbs7r7pzdj+DIQZ0xbcc/2G3TYhijCNjdf5X17zmWJBcdY5lrklyzTvsnkpy/Tvu3MhTdAAAAAADYvh41dgAAAE6sqt6T1alin1FVh4araQEAAACYkbm/0mw4QLSU5ClVdSjJa7r72nFTAQBsre7+lbEzAAAAAGxnc180c4AIAAAAAACAzWZ6RgAAAAAAACZP0QwAAAAAAIDJUzQDAAAAAABg8hTNAAAAAAAAmDxFMwAAAAAAACZP0QwAAAAAAIDJUzQDAAAAAABg8hTNAAAAAAAAmDxFMwAAAAAAACZP0QwAAAAAAIDJUzQDAAAAAABg8hTNAAAAAAAAmDxFMwAAAAAAACZP0QwAAAAAAIDJUzQDAAAAAABg8hTNAAAAAAAAmDxFMwAAAAAAACZP0QwAAAAAAIDJUzQDAAAAAABg8hTNAAAAAAAAmDxFMwAAAAAAACZP0QwAAAAAAIDJUzQDAAAAAABg8naMHQBgyu78h6/n8v23jh0DAAAAAGDyXGkGAAAAAADA5CmaAQAAAAAAMHkLUTSrqour6t6qOlhV+8fOA0yPcQgYm3EIAAAAYHPNfdGsqk5L8uYkv5DkvCS/UlXnjZsKmBLjEDA24xAAAADA5pv7olmS5yc52N2f7+5vJ3lvkktGzgRMi3EIGJtxCAAAAGCTLULR7Owk9695fmhoA9gqxiFgbMYhAAAAgE22Y+wAG1DrtPXDOlTtS7JveLpSVfducN1PSfJ/TyHbw9RrZ7WmUzLTbZoTtmkxPCXJvxk7xCY54TiUnPRYNNfvhWFcm+uMAxlnYztkNA5tz3EomfOca8g5W4uYc7uOQwAAANveIhTNDiU5Z83zXUm+tLZDdx9IcuCRrriqPtHde04t3nyxTYthG2/TuWPn2CQnHIeSkxuLFuG9IONsyDgbi5Bxk0x6HErknDU5Z2tRcgIAAHB8izA9498k2V1VT6uqxyTZm+SWkTMB02IcAsZmHAIAAADYZHN/pVl3H66q30zy50lOS/L27r5r5FjAhBiHgLEZhwAAAAA239wXzZKkuz+Q5AObsOpHPKXjArBNi8E2LZiJj0MyzoaMs7EIGTfFxMehRM5Zk3O2FiUnAAAAx1HdP3APeQAAAAAAAJiURbinGQAAAAAAAGyqSRbNquriqrq3qg5W1f6x88xCVb29qh6sqs+OnWUWquqcqvpIVd1TVXdV1avGznSqquqxVfXxqvrbYZt+b+xMs1JVp1XVp6vq/WNnWRTzOA4d63NXVU+uqtuq6nPD9yfNQdaHvefmLWNVPbGqbqqqvx9+nj8zhxn/y/D//Nmqes8wRo2acb3fZcfLVFVXD5+he6vqRVuZdTuYx3FoPYuwj7Mo+y2Lti+yKPsXVXVfVd1ZVZ+pqk+MnQcAAICTN7miWVWdluTNSX4hyXlJfqWqzhs31Uxcl+TisUPM0OEkV3X3TyW5IMmV2+D/6aEkL+zuZyd5TpKLq+qCcSPNzKuS3DN2iEUxx+PQsT53+5Pc3t27k9w+PB/b0e+5ecv4xiQf6u6fTPLsrGadm4xVdXaS/5xkT3efn+S0JHvnION1+cHfZetmGt6be5M8c1jmLcNniw2Y43FoPddl/vdxFmW/ZdH2RRZp/+Lnuvs53b1n7CAAAACcvMkVzZI8P8nB7v58d387yXuTXDJyplPW3R9N8pWxc8xKdz/Q3Z8aHn8zqwdMzh431anpVSvD00cPXwt/U8Gq2pXkl5K8bewsC2Qux6HjfO4uSXL90O36JC8ZJeDgGO+5uclYVWcmeUGSa5Oku7/d3V/LHGUc7EhyRlXtSPJDSb6UkTMe43fZsTJdkuS93f1Qd38hycGsfrbYmLkch9azCPs4i7Lfskj7IvYvAAAAGMMUi2ZnJ7l/zfNDmcODGnxfVZ2b5LlJ7hg5yikbphn6TJIHk9zW3Qu/TUnekOR3knx35ByLZO7HoaM+dzu7+4Fk9cBwkqeOGC1Z/z03Txl/PMk/JfnjYVqxt1XV4+YpY3f/Q5L/nuSLSR5I8vXu/ot5yrjGsTLN/edozvn5bZJ5329ZoH2RN2Rx9i86yV9U1Serat/YYQAAADh5Uyya1Tptc3mGLUlVPT7JnyZ5dXd/Y+w8p6q7v9Pdz0myK8nzq+r8kSOdkqp6cZIHu/uTY2dZMHM9Ds3z525B3nM7kvx0krd293OT/L+MP13kwwz3BbskydOS/FiSx1XVr46b6hGb68/RAvDz2wTzPH4esQj7Igsy1q91YXf/dFanO72yql4wdiAAAABOzhSLZoeSnLPm+a6sTknFnKmqR2f1wNMN3f2+sfPM0jBV23Lm/x4tJ3Jhkl+uqvuyOrXXC6vqXeNGWghzOw4d43P35ao6a3j9rKxenTCWY73n5injoSSH1ly9cVNWi2jzlPHnk3yhu/+pu/8lyfuS/OycZTziWJnm9nO0IPz8ZmzR9lvmfF9kofYvuvtLw/cHk/xZTBULAACwsKZYNPubJLur6mlV9Zgke5PcMnImjlJVldX7Ad3T3a8fO88sVNWPVtUTh8dnZPWg9d+PGuoUdffV3b2ru8/N6mfpw929aFerjGEux6HjfO5uSXLZ8PiyJDdvdbYjjvOem6eM/5jk/qp6xtB0UZK7M0cZszot4wVV9UPD//tFWb0H0zxlPOJYmW5JsreqTq+qpyXZneTjI+RbVHM5Di2qRdlvWZR9kUXav6iqx1XVDx95nOQ/JPnsuKkAAAA4WTvGDrDVuvtwVf1mkj9PclqSt3f3XSPHOmVV9Z4kS0meUlWHkrymu68dN9UpuTDJryW5c7jvRpL8bnd/YLxIp+ysJNdX1WlZLVjf2N3vHzkTI5jjcWjdz12S309yY1VdkdViy8vGiXdc85bxlUluGIoRn0/y6xk+9/OQsbvvqKqbknwqyeEkn05yIMnjx8y43u+yHOP/trvvqqobs1qQPJzkyu7+zlbmXWRzPA79gAXZx1mU/Rb7IrO3M8mfrdZNsyPJu7v7Q+NGAgAA4GRVt9tXAAAAAAAAMG1TnJ4RAAAAAAAAHkbRDAAAAAAAgMlTNAMAAAAAAGDyFM0AAAAAAACYPEUzIFX19qp6sKo+u8H+/7Gq7q6qu6rq3ZudDwBgVh7Jfk9VvaCqPlVVh6vqpeu8fmZV/UNV/dHmpAUAAGArKZoBSXJdkos30rGqdie5OsmF3f3MJK/evFgAADN3XTa435Pki0kuT3Ksk4T+W5L/feqRAAAAmAeKZkC6+6NJvrK2rar+bVV9qKo+WVV/WVU/Obz0n5K8ubu/Oiz74BbHBQA4aY9kv6e77+vuv0vy3aPXU1XPS7IzyV9sRW4AAAA2n6IZcCwHkryyu5+X5LeSvGVo/4kkP1FV/6eq/rqqNnqmNgDAvDrWfs+6qupRSV6X5Le3IBsAAABbZMfYAYD5U1WPT/KzSf5nVR1pPn34viPJ7iRLSXYl+cuqOr+7v7bFMQEATtkJ9nuO5TeSfKC771+zDAAAAAtO0QxYz6OSfK27n7POa4eS/HV3/0uSL1TVvVktov3NFuYDAJiV4+33HMvPJPn3VfUbSR6f5DFVtdLd+zcjIAAAAFvD9IzAD+jub2S1IPayJKlVzx5e/l9Jfm5of0pWp2v8/Bg5AQBO1Qn2e461zKXd/a+7+9ysTuf4DgUzAACAxadoBqSq3pPkY0meUVWHquqKJJcmuaKq/jbJXUkuGbr/eZJ/rqq7k3wkyW939z+PkRsA4JF6JPs9VfXvqupQkpcl+R9VdddYuQEAANh81d1jZwAAAAAAAIBRudIMAAAAAACAyVM0AwAAAAAAYPIUzQAAAAAAAJg8RTMAAAAAAAAmT9EMAAAAAACAyVM0AwAAAAAAYPIUzQAAAAAAAJg8RTMAAAAAAAAm7/8D08IjMlRrghcAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 2160x2160 with 36 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Distribution of continuous variables\n",
    "df_eda[cont_vars].hist(figsize=(30,30))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "94d0f744-3e7a-4bf0-830a-64c2573d0ddd",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAR8klEQVR4nO3df2xdZ33H8fe3Tmu3pSPpD9xpATIYYk1wCtsVFBDULYzSpHWHxiQYbKPSkllMBhZKU8FQ2SZFWKIBFgEmnoBq5cdGh4nXiFC09Y79wloCbdKmYXTQUKgyWtKEpqWmSb77496kThrX18Qn97H9fklXuT4995yPo5589Dzn3HMiM5EkqTSntTuAJEknYkFJkopkQUmSimRBSZKKZEFJkoq0oIqNnn/++blkyZIqNi3NWo899hhnn312u2NIxdm2bdvDmXnB8csrKaglS5awdevWKjYtzVr1ep3e3t52x5CKExG7T7TcKT5JUpEsKElSkSwoSVKRLChJUpEsKElSkSwoSVKRLChJUpEsKKliAwMDdHV1cdlll9HV1cXAwEC7I0mzQktf1I2IdwOrgACGM/NjVYaS5oqBgQGGhoYYHBxk6dKl7Ny5k7Vr1wKwYcOGNqeTyjblCCoiXkKjnF4OXAxcFREvqjqYNBcMDw8zODjImjVr6OrqYs2aNQwODjI8PNzuaFLxWpniuwj4VmY+npkHgX8F3lRtLGluGB8fp7+//5hl/f39jI+PtymRNHu0UlB3A6+NiPMi4ixgBfDcamNJc0NnZydDQ0PHLBsaGqKzs7NNiaTZY8pzUJl5b0QMAt8ADgB3AQePXy8iVgOrAbq7u6nX6zObVJqFrrzySq6//nruu+8+Lr/8ct75zneyceNGrr76ao8RaQqRmdP7QMQ64EeZ+cnJ1qnVaundzKWGgYEBhoeHGR8fp7Ozk1WrVnmBhDRBRGzLzNrTlrdSUBHxnMz8SUQ8D7gdeGVmPjLZ+haU9HQ+bkM6sckKqtXnQf1jRJwHPAn82TOVkyRJM6GlgsrM11QdRJKkibyThCSpSBaUJKlIFpQkqUgWlCSpSBaUJKlIFpQkqUgWlCSpSBaUJKlIFpQkqUgWlCSpSBaUJKlIFpQkqUgWlCSpSBaUJKlIFpQkqUgWlCSpSBaUJKlIFpQkqUgWlCSpSBaUJKlIC9odQJpNIqJt+87Mtu1bagdHUNI0ZOYv/Xr+2ttO6vPSfGNBSZKKZEFJkopkQUmSimRBSZKKZEFJkopkQUmSimRBSZKK1FJBRcSfR8Q9EXF3RHwxIrqqDiZJmt+mLKiI+DXgXUAtM18CdABvqTqYJGl+a3WKbwFwZkQsAM4CHqwukiRJLdyLLzN/HBEfAX4I/By4PTNvP369iFgNrAbo7u6mXq/PcFRp9vO4kFo3ZUFFxCLgGuDXgX3AlyPi7Zl5y8T1MnMjsBGgVqtlb2/vjIeVZrUtm/G4kFrXyhTf64EfZOZDmfkk8BXgVdXGkiTNd60U1A+BSyLirGg8a+B1wL3VxpIkzXdTFlRmjgG3At8GdjQ/s7HiXJKkea6lBxZm5o3AjRVnkSTpKO8kIUkqkgUlSSqSBSVJKpIFJUkqkgUlSSqSBSVJKpIFJUkqkgUlSSqSBSVJKpIFJUkqkgUlSSqSBSVJKpIFJUkqkgUlSSqSBSVJKpIFJUkqkgUlSSqSBSVJKpIFJUkqkgUlSSqSBSVJKpIFJUkqkgUlSSqSBSVJKpIFJUkqkgUlSSqSBSVJKpIFJUkq0pQFFREvjog7J7x+FhHvOQXZJEnz2IKpVsjM7wIvBYiIDuDHwEi1sSRJ8910p/heB/xvZu6uIowkSUdMOYI6zluAL57oP0TEamA1QHd3N/V6/eSSSXOQx4XUusjM1laMOAN4EFiWmf/3TOvWarXcunXrDMST5o4lN2zm/g+vbHcMqTgRsS0za8cvn84U35XAt6cqJ0mSZsJ0CuqtTDK9J0nSTGupoCLiLOB3gK9UG0eSpIaWLpLIzMeB8yrOIknSUd5JQpJUJAtKklQkC0qSVCQLSpJUpOneSUKa9S7+y9vZ//Mn27LvJTdsPuX7fPaZp3PXjW845fuVTpYFpXln/8+fbMsdHer1Or29vad8v+0oRWkmOMUnSSqSBSVJKpIFJUkqkgUlSSqSBSVJKpIFJUkqkgUlSSqSBSVJKpIFJUkqkgUlSSqSBSVJKpIFJUkqkgUlSSqSBSVJKpIFJUkqks+D0rxzzkU30HPzDe3Z+c2nfpfnXARw6p9/JZ0sC0rzzqP3ftgHFkqzgFN8kqQiWVCSpCJZUJKkIllQkqQiWVCSpCJZUJKkIrVUUBGxMCJujYhdEXFvRLyy6mCSpPmt1e9BfRzYkplvjogzgLMqzCRJ0tQFFRG/ArwWeAdAZv4C+EW1sSRJ810rI6gXAA8Bn42Ii4FtwLsz87GJK0XEamA1QHd3N/V6fYajSjOnHf9/HjhwoG3HhcejZqNWCmoB8FvAQGaORcTHgRuAD05cKTM3AhsBarVatuOWLlJLtmxuyy2H2nWro3b9vtLJauUiiR8BP8rMsebPt9IoLEmSKjNlQWXmHuCBiHhxc9HrgJ2VppIkzXutXsU3AHy+eQXf94Frq4skSVKLBZWZdwK1aqNIkvQU7yQhSSqSBSVJKpIFJUkqkgUlSSqSBSVJKpIFJUkqkgUlSSqSBSVJKpIFJUkqUqu3OpLmlCU3bG7Pjrec+v0++8zTT/k+pZlgQWneuf/DK9uy3yU3bG7bvqXZyCk+SVKRLChJUpEsKElSkSwoSVKRLChJUpEsKElSkSwoSVKRLChJUpEsKElSkSwoSVKRLChJUpEsKElSkSwoSVKRLChJUpEsKElSkSwoSVKRLChJUpFaeqJuRNwPPAocAg5mZq3KUJIkTeeR75dl5sOVJZEkaQKn+CRJRWp1BJXA7RGRwKczc+PxK0TEamA1QHd3N/V6fcZCSnOFx4XUulYL6tWZ+WBEPAf4RkTsysxvTlyhWVobAWq1Wvb29s5sUmm227IZjwupdS1N8WXmg80/fwKMAC+vMpQkSVMWVEScHRHnHHkPvAG4u+pgkqT5rZUpvm5gJCKOrP+FzNxSaSpJ0rw3ZUFl5veBi09BFkmSjvIyc0lSkSwoSVKRLChJUpEsKElSkSwoSVKRLChJUpEsKElSkSwoSVKRLChJUpEsKElSkSwoSVKRLChJUpEsKElSkSwoSVKRLChJUpEsKElSkSwoSVKRLChJUpEsKElSkSwoSVKRLChJUpEsKElSkSwoSVKRLChJUpEsKElSkSwoSVKRLChJUpEsKElSkSwoSVKRWi6oiOiIiO9ExG1VBpIkCaY3gno3cG9VQSRJmqilgoqIxcBK4G+rjSNJUsOCFtf7GHA9cM5kK0TEamA1QHd3N/V6/WSzSXOOx4XUuikLKiKuAn6Smdsioney9TJzI7ARoFarZW/vpKtK89OWzXhcSK1rZYrv1UBfRNwPfAm4PCJuqTSVNIcMDAzQ1dXF7sGr6OrqYmBgoN2RpFkhMrP1lRsjqOsy86pnWq9Wq+XWrVtPLplUoIho276nc6xKs0lEbMvM2vHL/R6UNA2ZOa1XZ2cnF1544THbuPDCC+ns7Jz2tqT5ZloFlZn1qUZPkp4yPj7Onj176OvrY2RkhL6+Pvbs2cP4+Hi7o0nFcwQlVWzZsmVs2rSJhQsXsmnTJpYtW9buSNKs0Opl5pJ+Sffccw8LFizg0KFDdHR0cOjQoXZHkmYFR1CSpCJZUFKFjlz1t3LlSkZGRli5cuUxyyVNzik+qUKZyaJFixgdHWV0dBSARYsW8cgjj7Q5mVQ+C0qqUEdHB/v37+emm25i6dKl7Ny5k/e97310dHS0O5pUPAtKqlBmcvjwYd773vces/y005xdl6biUSJV6PDhw8BThXTkzyPLJU3OgpIqtmzZMg4dOsQdd9zBoUOH/B6U1CILSqrYrl27WL9+PU888QTr169n165d7Y4kzQqeg5IqtnjxYt7//vczPj5OZ2cnixcvZvfu3e2OJRXPEZRUoZ6eHnbv3s0VV1zByMgIV1xxBbt376anp6fd0aTiTetxG63ycRvSU5YvX86OHTuO/tzT08P27dvbmEgqi4/bkNrk0ksvpbOzE4DOzk4uvfTSNieSZgfPQUkVGhgYYGhoiMHBwaNf1F27di0AGzZsaHM6qWyOoKQKDQ8PMzg4yJo1a+jq6mLNmjUMDg4yPDzc7mhS8SwoqULj4+P09/cfs6y/v98HFkotsKCkCnV2djI0NHTMsqGhoaPnpCRNznNQUoVWrVp19JzT0qVLWb9+PWvXrn3aqErS01lQUoWOXAgx8Yu6/f39XiAhtcApPklSkRxBSRXyMnPpl+edJKQKdXV1UavV2Lp169EpviM/P/HEE+2OJxVhsjtJOIKSKjQ+Ps7Y2NjTRlAHDx5sdzSpeJ6Dkiq2YsWKY76ou2LFinZHkmYFC0qq2ObNm495HtTmzZvbHUmaFZzikyp05JzTxMvML7nkEjxHK03NEZRUoVWrVjE2Nsa6dev42te+xrp16xgbG2PVqlXtjiYVb8qr+CKiC/gm0EljxHVrZt74TJ/xKj7pKeeddx579+49+vO5557LT3/60zYmkspyMs+DGgcuz8yLgZcCb4yIS2Y4nzQnLV++nL1799LX18fIyAh9fX3s3buX5cuXtzuaVLwpCyobDjR/PL35mvkvT0lz0I4dO+jr62PTpk0sXLiQTZs20dfXd8wTdiWdWEsXSUREB7AN+A3gE5k5doJ1VgOrAbq7u6nX6zMYU5q9rr32Wur1OgcOHKBer3PttdcyOjrqMSJNYVp3koiIhcAIMJCZd0+2nuegpIaIODqCqtfr9Pb2cs011zA6OkoVd3GRZqOTOQd1VGbuA+rAG2cmljS39fT0MDo6yjXXXMO+ffuOllNPT0+7o0nFm3KKLyIuAJ7MzH0RcSbwemCw8mTSHLB9+3aWL1/O6Ogoo6OjQKO0tm/f3uZkUvlaOQf1q8DNzfNQpwH/kJm3VRtLmjuOlNGRKT5JrZmyoDJzO/CyU5BFkqSjvJOEJKlIFpQkqUgWlCSpSBaUJKlIFpQkqUgWlCSpSNO61VHLG414CNg94xuWZrfzgYfbHUIq0PMz84LjF1ZSUJKeLiK2nuh+Y5JOzCk+SVKRLChJUpEsKOnU2djuANJs4jkoSVKRHEFJkopkQUmSimRBaV6IiCURcXe7c0xHM/MfnMTnfzMi7oyI70TEC09mW1I7WFBSuZYAJ1MqvwtsysyXAc89yW1Jp5wXSWheiIglwBZgjMYDOP8H+CPgOuBq4EzgP4E/zcyMiHcB/cBBYGdmviUizgY2AD00Hvb5oczcFBHvoFEGHcBLgJuAM4A/BMaBFZm5NyJeCHwCuAB4HFiVmbsi4nPAz4AacCFwfWbeGhHfAi4CfgDcDNwOfLa57dOA38vM70XEB5q/ywPAQ8A2YCfwGeBQ83c9c+K2MvOjM/aXK1UlM335mvMvGqORBF7d/PkzNMrp3Anr/B1wdfP9g0Bn8/3C5p/rgLcfWUbjH/6zgXcA9wHn0Cif/UB/c72PAu9pvv9n4EXN968A/qX5/nPAl2mUzlLgvubyXuC2Cfk2AG9rvj+DRun8NrADOAv4lWaO65rrfGjC+2O25cvXbHhN+ch3aQ55IDP/o/n+FuBdwA8i4noa/8CfC9wD/BOwHfh8RHwV+GrzM28A+iLiuubPXcDzmu/vyMxHgUcjYn9zG9Aoj+UR8SzgVcCXI+JIns4J2b6amYeBnRHRPUn+/wI+EBGLga9kY/T0GmAkMx8HiIjRaf2NSAWzoDSfHD+fncAngVpmPhARH6JROgArgdcCfcAHI2IZEDSm1b47cSMR8QoaU3lHHJ7w82Eax9lpwL7MfOkk2SZ+Pk60QmZ+ISLGmtm+HhF/MsnvJc0JXiSh+eR5EfHK5vu3Av/efP9wc4TzZoCIOA14bmbeAVxPYzrvWcDXgYFoDoEi4mWt7jgzf0ZjtPb7zc9GRFw8xccepTFtSPMzLwC+n5l/A4wCy4FvAm+KiDMj4hwa59Om3JY0GziC0nxyL/DHEfFp4HvAp4BFNKbh7gf+u7leB3BLRDybxmjmo5m5LyL+GvgYsL1ZUvcDV01j/28DPhURfwGcDnwJuOsZ1t8OHIyIu2icp+oC3h4RTwJ7gL/KxsUXfw/cSeMRN//WyrbSiyQ0C3gVnzSHNKcpD2TmR9qdRTpZTvFJkorkCEqSVCRHUJKkIllQkqQiWVCSpCJZUJKkIllQkqQi/T9PEYkvA+xD5wAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n",
      "\n",
      "\n",
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAVAklEQVR4nO3df4wd5X3v8ffXP8NNaGhi7kKJgyvFavFSNWmPCNBWXZM0gEsFlVCLlZQScrUhDdxWwmoRSDi0Qk3UxFISCI4jECFBbpImsSi4Bkp8EuBeKLavDV673FipCS5WWkjjxEAMxt/+ccZwfHx293h9vPN49/2SRp6Z88zM14bxx/M8z86JzESSpNLMqLsASZK6MaAkSUUyoCRJRTKgJElFMqAkSUWaVdeF582blwsWLKjr8lIRXnzxRd785jfXXYZUq40bNz6fmSd37q8toBYsWMCGDRvqurxUhGazydDQUN1lSLWKiGe67beLT5JUJANKklQkA0qSVCQDSpJUpHEDKiLeFBH/EhFbImIkIm7q0mYoIvZExOZqufHYlCtJmi56mcW3DzgvM/dGxGzgkYj4p8x8rKPdw5l5Uf9LlCRNR+MGVLZed7632pxdLb4CXZJ0TPX0c1ARMRPYCLwLuDUzH+/S7JyI2AI8ByzLzJEu5xkGhgEGBgZoNpsTrVs6rj300EN89atf5Yc//CHvfOc7+dCHPsT73ve+usuSitJTQGXma8C7I+Ik4NsRcWZmbm1rsgk4veoGXAKsARZ2Oc8qYBVAo9FIf0BR09Hq1au5++67ueOOO3jttdeYOXMmH/nIR1i0aBFLly6tuzypGHGkX1gYEcuBFzPz02O02Qk0MvP50do0Go30TRKajs4880wuueQS1qxZw/bt2znjjDNe3966dev4J5CmmIjYmJmNzv3jPkFFxMnAq5n5k4g4AXg/8KmONqcAP8rMjIizaM0OfKE/pUtTy7Zt23jppZe4/fbbD3mC2rlzZ92lSUXp5eegTgXWR8STwBPAg5l5b0RcFRFXVW0uBbZWY1CfAy5Lv0te6mrOnDlcffXVLF68mFmzZrF48WKuvvpq5syZU3dpUlGOuIuvX+zi03Q1Y8YMTj/99EPGoK688kqeeeYZDhw4UHd50qSbcBefpP5atGgRl1xyCddcc83rY1Af/OAHWbNmTd2lSUUxoKRJdsMNN3DDDTccNgZ18803112aVBQDSppkB6eStz9B3XzzzU4xlzo4BiXVyC8slEYfg/Jt5pKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQizRqvQUS8CfgeMLdq/w+ZubyjTQCfBZYALwFXZOam/pcrTQ2tW+ZQmVlDJVK5enmC2gecl5m/DrwbuCAizu5ocyGwsFqGgdv6WaQ0lbSH0/Lly7vul9RDQGXL3mpzdrV0/lPvYuCuqu1jwEkRcWp/S5Wmlrlz53LTTTcxd+7cukuRijRuFx9ARMwENgLvAm7NzMc7mpwGPNu2vavat7vjPMO0nrAYGBig2WxOrGppCrjyyis577zz+M53vsNtt7U6HbwnpDfEkfR7R8RJwLeBazJza9v++4C/zcxHqu2HgL/MzI2jnavRaOSGDRsmWrd03BqrK89xKE1HEbExMxud+49oFl9m/gRoAhd0fLQLmN+2/Q7guSMrUZp+rrnmmrpLkIo1bkBFxMnVkxMRcQLwfuBfO5rdA1weLWcDezJzN5LG9PnPf77uEqRi9TIGdSrw5Wocagbw9cy8NyKuAsjMlcBaWlPMd9CaZv7hY1SvJGmaGDegMvNJ4D1d9q9sW0/g4/0tTZI0nfkmCakmg4ODrF69msHBwbpLkYrU0zRzSf01Z84cRkZGWLp06evbr7zySs1VSWXxCUqqQWcYGU7S4QwoqUYf+MAH6i5BKpYBJdXogQceqLsEqVgGlFSDdevWkZmsX7+ezGTdunV1lyQVx0kSUg0uuKDzZSySOvkEJdVowYIFdZcgFcuAkmq0c+fOukuQimVASTU4//zzDxmDOv/88+suSSqOY1BSDe6//36/QVcah09QUo1mzPAWlEbj3SHV6MCBA3WXIBXLgJJqsHDhQubOnQvA3LlzWbhwYc0VSeVxDEqqwfe//30+85nPsGjRIrZt28a1115bd0lScQwoqSaGkjQ2u/gkSUUyoKSatP8clKTDGVBSTVasWMHPf/5zVqxYUXcpUpEcg5Jqcv3117Nv377XZ/NJOpRPUFJN9u3bd8ivkg5lQEmSimRASZKKZEBJNTn33HP5xje+wbnnnlt3KVKRxg2oiJgfEesjYntEjETEn3dpMxQReyJic7XceGzKlaaORx99lHnz5vHoo4/WXYpUpF5m8e0Hrs3MTRFxIrAxIh7MzG0d7R7OzIv6X6I0Nfl1G9LYxn2CyszdmbmpWv8ZsB047VgXJkma3o7o56AiYgHwHuDxLh+fExFbgOeAZZk50uX4YWAYYGBggGazeaT1SlPG4OAgy5Yt49Of/jQjI63bxXtCekP0+pqViHgL8F3g5sz8VsdnvwAcyMy9EbEE+Gxmjvn9AY1GIzds2DDBsqXj11hde772SNNRRGzMzEbn/p5m8UXEbOCbwN2d4QSQmT/NzL3V+lpgdkTMO8qapSltcHCQ1atXMzg4WHcpUpHG7eKL1j/3bge2Z2bXl4ZFxCnAjzIzI+IsWsH3Ql8rlaaYkZERli5dWncZUrF6GYP6LeBPgKciYnO173rgnQCZuRK4FPhYROwHXgYuS/sqJElHYdyAysxHgDHnw2bmLcAt/SpKkiTfJCHVxDEoaWx+3YZUE8egpLH5BCXV6KMf/WjdJUjFMqCkGn3xi1+suwSpWAaUJKlIBpQkqUgGlFQjx6Ck0RlQUo0cg5JGZ0BJNZoxw1tQGo13h1SjAwcO1F2CVCwDSpJUJANKklQkA0qSVCQDSpJUJANKklQkA0qSVCQDSpJUJANKklQkA0qSVCQDSpJUJANKklQkA0qqSWayfv16MrPuUqQiGVCSpCLNGq9BRMwH7gJOAQ4AqzLzsx1tAvgssAR4CbgiMzf1v1xp6mjdNpJGM25AAfuBazNzU0ScCGyMiAczc1tbmwuBhdXyXuC26ldJkiZk3C6+zNx98GkoM38GbAdO62h2MXBXtjwGnBQRp/a9WmkKcQxKGtsRjUFFxALgPcDjHR+dBjzbtr2Lw0NMkqSe9dLFB0BEvAX4JvAXmfnTzo+7HHLYPwsjYhgYBhgYGKDZbPZeqTTFdBuD8p6Q3hC9dC9ExGzgXuD+zFzR5fMvAs3MXF1tPw0MZebu0c7ZaDRyw4YNEy5cOl6NNTnC7j5NRxGxMTMbnfvH7eKrZujdDmzvFk6Ve4DLo+VsYM9Y4SRJ0nh6GYP6LeBPgPMiYnO1LImIqyLiqqrNWuAHwA7gS8CfHZtypanDSRLS2MYdg8rMR+g+xtTeJoGP96soSZJ6niQhqb/8QV1pbL7qSJJUJANKqoljUNLYDChJUpEMKElSkZwkIdXESRLS2HyCkiQVyYCSauIkCWlsBpQkqUgGlCSpSE6SkGriJAlpbD5BSZNstDEnx6KkQxlQUg0y85BJEoaTdDgDSpJUJMegpKNU11iST12a6nyCko7SwS66iSyn/9W9Ez5WmuoMKElSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpEMKElSkQwoSVKRxg2oiLgjIv4jIraO8vlQROyJiM3VcmP/y5QkTTe9vOroTuAW4K4x2jycmRf1pSJJkujhCSozvwf8eBJqkSTpdf16Wew5EbEFeA5Ylpkj3RpFxDAwDDAwMECz2ezT5aXjl/eB1F0/AmoTcHpm7o2IJcAaYGG3hpm5ClgF0Gg0cmhoqA+Xl45j6+7D+0Dq7qhn8WXmTzNzb7W+FpgdEfOOujJJ0rR21AEVEadE9YU4EXFWdc4Xjva8kqTpbdwuvohYDQwB8yJiF7AcmA2QmSuBS4GPRcR+4GXgsvTLaiRJR2ncgMrMpeN8fgutaeiSJPWNb5KQJBXJgJIkFcmAkiQVyYCSJBXJgJIkFcmAkiQVyYCSJBXJgJIkFcmAkiQVyYCSJBXJgJIkFcmAkiQVyYCSJBXJgJIkFcmAkiQVyYCSJBXJgJIkFcmAkiQVyYCSJBXJgJIkFcmAkiQVaVbdBUil+PWbHmDPy69O+nUXXHffpF7vrSfMZsvyD0zqNaWJMKCkyp6XX2XnJ39/Uq/ZbDYZGhqa1GtOdiBKE2UXnySpSOMGVETcERH/ERFbR/k8IuJzEbEjIp6MiN/of5mSpOmmlyeoO4ELxvj8QmBhtQwDtx19WZKk6W7cgMrM7wE/HqPJxcBd2fIYcFJEnNqvAiVJ01M/JkmcBjzbtr2r2re7s2FEDNN6ymJgYIBms9mHy0v9M9n/T+7du7eW+8B7T8eDfgRUdNmX3Rpm5ipgFUCj0cjJnr0kjWndfZM+o66OWXx1/D6liejHLL5dwPy27XcAz/XhvJKkaawfAXUPcHk1m+9sYE9mHta9J0nSkRi3iy8iVgNDwLyI2AUsB2YDZOZKYC2wBNgBvAR8+FgVK0maPsYNqMxcOs7nCXy8bxVJkoRvkpAkFcqAkiQVyYCSJBXJgJIkFcmAkiQVyYCSJBXJgJIkFcmAkiQVyYCSJBXJgJIkFcmAkiQVyYCSJBXJgJIkFcmAkiQVqR9f+S5NCSeecR2/9uXrJv/CX57cy514BsDvT+5FpQkwoKTKU3/61KRfc8F197Hzk4aF1I1dfJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQi9RRQEXFBRDwdETsi4rAftY+IoYjYExGbq+XG/pcqSZpOxn2TRETMBG4Ffg/YBTwREfdk5raOpg9n5kXHoEZJ0jTUyxPUWcCOzPxBZr4C/D1w8bEtS5I03fXyLr7TgGfbtncB7+3S7pyI2AI8ByzLzJHOBhExDAwDDAwM0Gw2j7hgaarxPpC66yWgosu+7NjeBJyemXsjYgmwBlh42EGZq4BVAI1GI4eGho6oWGnKWXcf3gdSd7108e0C5rdtv4PWU9LrMvOnmbm3Wl8LzI6IeX2rUpI07fQSUE8ACyPilyNiDnAZcE97g4g4JSKiWj+rOu8L/S5WkjR9jNvFl5n7I+Jq4H5gJnBHZo5ExFXV5yuBS4GPRcR+4GXgsszs7AaUJKlnPX1hYdVtt7Zj38q29VuAW/pbmiRpOvMbdaWjVPVuT/z4T03sODspNNX5qiPpKGXmhJf169dP+FhpqjOgJElFMqAkSUUyoCRJRTKgJElFMqAkSUUyoCRJRTKgJElFMqAkSUUyoCRJRTKgJElFMqAkSUUyoCRJRTKgJElFMqAkSUUyoCRJRTKgJElFMqAkSUUyoCRJRTKgJElFMqAkSUUyoCRJReopoCLigoh4OiJ2RMR1XT6PiPhc9fmTEfEb/S9Vmjoigohg8eLFr69LOtS4ARURM4FbgQuBRcDSiFjU0exCYGG1DAO39blOacoYLYwMKelQvTxBnQXsyMwfZOYrwN8DF3e0uRi4K1seA06KiFP7XKs0pWQm69evJzPrLkUq0qwe2pwGPNu2vQt4bw9tTgN2tzeKiGFaT1gMDAzQbDaPsFxp6mg2m+zdu/eQ+8B7QnpDLwHVrd+h8598vbQhM1cBqwAajUYODQ31cHlpahoaGqLZbNJ+H3hPSG/oJaB2AfPbtt8BPDeBNpLaOOYkja2XMagngIUR8csRMQe4DLino809wOXVbL6zgT2ZubvzRJIYdczJsSjpUOMGVGbuB64G7ge2A1/PzJGIuCoirqqarQV+AOwAvgT82TGqV5oSMvOQSRKGk3S4Xrr4yMy1tEKofd/KtvUEPt7f0iRJ05lvkpAkFcmAkiQVyYCSJBXJgJIkFcmAkiQVyYCSJBUp6vr5i4j4T+CZWi4ulWMe8HzdRUg1Oz0zT+7cWVtASYKI2JCZjbrrkEpkF58kqUgGlCSpSAaUVK9VdRcglcoxKElSkXyCkiQVyYCSJBXJgNK0FBFXRMQtEzz2zoi4tIfz/9IRnndBRGydaH0RcX23cx3B8RP+M5GOBQNKOjauAI4ooPrg+vGbHJ3qW7P9e0OTwv/RNKVExOUR8WREbImIr0TEH0TE4xHx/yLinyNioMsxAxHx7eqYLRFxbucTSEQsi4hPdDn2xoh4IiK2RsSq6i/wS4EGcHdEbI6IEyLiNyPiuxGxMSLuj4hTq+N/s7rm/+XwL/2cHxHrIuLpiFjeds011XlGImK42vdJ4ITqendXTWdGxJeqdg9ExAlV22ZEfCoi/iUi/n9E/M5Y16z+LLZHxBeATcD8I/3vIk3Iwa+bdnE53hdgEHgamFdtvw34Rd6Yrfq/gM9U61cAt1TrXwP+olqfCbwVWABsbTv3MuAT1fqdwKUHr9HW5ivAH1TrTaBRrc8G/g9wcrX9x8Ad1fqTwO9W63938JpVfbuBtwMnAFvbzve26teD+99ebe9tq2UBsB94d7X9deBDbbUd/HNYAvzzWNesznUAOLvu/8Yu02vp6SvfpePEecA/ZObzAJn544j4NeBr1RPLHODfRjnu8uqY14A9EfGLPV5zcUT8JfA/aAXiCPCPHW1+BTgTeDAioBWCuyPircBJmfndqt1XgAvbjnswM18AiIhvAb8NbAD+d0T8YdVmPrAQeKFLbf+WmZur9Y20guagb42yv9s11wDPZOZjo/0hSMeCAaWpJIDOH+z7PLAiM++JiCHgEz2eaz+HdoG/6bCLRbwJ+AKtJ5tnqy7Aw9pVdY1k5jkdx5/Upd52nZ9l9Xt4P3BOZr4UEc1Rrgmwr239NVpPRZ2fvcahfw8cds3q1xfHqFM6JhyD0lTyEPBHEfF2gIh4G63uun+vPv/TMY77WHXMzIj4BeBHwP+MiLdHxFzgoi7HHQyG5yPiLUD7zL6fASdW608DJ0fEOdU1ZkfEYGb+hNbT2m9X7T7Ycf7fi4i3VWNHlwCPVr+f/6rC6VeBs9vavxoRs0f5Pfaq2zWlWhhQmjIycwS4GfhuRGwBVtB6YvpGRDzM6F9r8ee0uuqeotXlNZiZrwJ/DTwO3Av8a5fr/QT4EvAUrW6wJ9o+vhNYGRGbaXXpXQp8qqprM3Bu1e7DwK3VJImXOy7xCK1uv83ANzNzA7AOmBURTwJ/A7R3u60CnmybJDER3a4p1cJXHUmSiuQTlCSpSAaUJKlIBpQkqUgGlCSpSAaUJKlIBpQkqUgGlCSpSP8N+5OpbhzLNU8AAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAATCElEQVR4nO3df5DcdX3H8dcrF9xDQKAFDwsxBx1Sc00VdVutjLhnWrDiSGfEEYpWaJrzqpzUYDlK2kHrRDntZNSzzsEZftQf8QeKzVDHYPEWWguRPYiR5PzBQMAoQZyiGMYs5Hj3j/1evFzucpfdZfdzt8/HzM5997Pf7/fz3r3svfL5fr77XUeEAABIzaJmFwAAwHQIKABAkggoAECSCCgAQJIIKABAkhY3srMTTjghOjs7G9klkKSnnnpKRx11VLPLAJIwOjr6i4g4cWp7QwOqs7NTpVKpkV0CSSoWiyoUCs0uA0iC7Yena+cQHwAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQU0EB9fX1qb29Xd3e32tvb1dfX1+ySgGQ19IO6QCvr6+vT0NCQBgYG1NXVpR07dqi/v1+SNDg42OTqgPQwggIaZHh4WAMDA1qzZo3a29u1Zs0aDQwMaHh4uNmlAUkioIAGKZfL6u3tPaCtt7dX5XK5SRUBaSOggAbJ5XIaGho6oG1oaEi5XK5JFQFpYw4KaJDVq1fvn3Pq6urS+vXr1d/ff9CoCkAFAQU0yMSJEFdddZXK5bJyuZx6e3s5QQKYwayH+Gxfb/vntu+f1PY7tr9l+8fZz+Of2zKBhWFwcFB79+7VyMiI9u7dSzgBhzCXOagbJb1hStuVkm6PiNMl3Z7dBwCgbmYNqIi4U9L/TWk+T9JN2fJNkv6yvmUBAFpdtXNQHRHxqCRFxKO2XzjTirZ7JPVIUkdHh4rFYpVdAgvHnj17eC8As3jOT5KIiOskXSdJ+Xw++JprgK98B+ai2s9BPWb7RZKU/fx5/UoCFi6uxQfMXbUjqE2S3inpmuznf9StImCB4lp8wOFxRBx6BXujpIKkEyQ9JulqSV+X9GVJL5b0iKS3RsTUEykOks/no1Qq1VYxME+1t7crn8+rVCrt/xzUxP29e/c2uzygaWyPRkR+avusI6iIuHCGh1bWXBXQQsrlsu6++2599KMf3T+CuuKKKzQ+Pt7s0oAkcSUJoIGWLFlywJUklixZop07dza7LCBJXCwWaKCdO3fqnHPO0S233KJzzjmHcAIOgREU0EBLly7V5s2btWnTJuVyOS1dulQPP/xws8sCkkRAAQ20a9eug+agAEyPgAIaJJfL6fjjj9fll1++v+2kk07SE0880cSqgHQxBwU0yLJly7R7924tWlR52y1atEi7d+/WsmXLmlwZkCYCCmiQ7du3S5KeffbZA35OtAM4EAEFNMhEIM21HWh1BBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJNQWU7ffZ3m77ftsbbbfXqzAAQGurOqBsnyzpvZLyEbFCUpukC+pVGDAf2J7zrV77mW1fwEKxuA7bH2n7GUnPl/Sz2ksC5o+ImPO6hwqWw9kP0CqqDqiI+Kntf5X0iKTfSLotIm6bup7tHkk9ktTR0aFisVhtl8CCxfsCOJir/Z+b7eMlfVXS2yT9UtJXJN0cEZ+baZt8Ph+lUqmq/oCFYLpRFKMntDrboxGRn9pey0kSfybpoYh4PCKekfQ1Sa+pYX/AghcRiggt7b91/zKA6dUSUI9IerXt57vy38KVksbqUxYAoNVVHVARsUXSzZLulfT9bF/X1akuAECLq+ksvoi4WtLVdaoFAID9uJIEACBJBBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEm1ft0GMO+97IO36Ve/eabh/XZe+Z8N7/PYI4/Q964+u+H9AtUgoNDyfvWbZ7TzmnMb2mexWFShUGhon1JzQhGoFof4AABJIqAAAEkioAAASWIOCi3vmOVX6o9uurLxHd/U+C6PWS5JjZ1vA6pFQKHl/XrsGk6SABLEIT4AQJIIKABAkggoAECSmIMC1KS5mW8250oSwHxBQKHlNfoECakSiM3oF5hPOMQHAEgSAQUASBIBBQBIEgEFAEgSAQUASBIBBQBIEgEFAEgSAQUASBIBBQBIEgEFAEgSlzoCGsj2b5cHKj8joknVAGljBAU0yORwmks70OoIKABAkjjEB9SgXqOfw90PhwXRCggooAaHExSHCiECBzhYTYf4bB9n+2bbP7A9ZvtP61UYAKC11TqC+oSkb0bE+bafJ+n5dagJAIDqA8r2CySdJeliSYqIpyU9XZ+yAACtrpYR1GmSHpd0g+2XSRqVdFlEPDV5Jds9knokqaOjQ8VisYYugYWJ9wVwMFc7OWs7L+luSWdGxBbbn5D0ZET880zb5PP5KJVK1VUKzHOcJAFMz/ZoROSnttdyksQuSbsiYkt2/2ZJr6hhfwAA7Fd1QEXEbkk/sf0HWdNKSTvqUhUAoOXVehZfn6TPZ2fwPSjpktpLAgCgxoCKiK2SDjpuCABArbgWHwAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBTQYB0dHbrhhhvU0dHR7FKApC1udgFAq3nsscd0ySWXNLsMIHmMoAAASSKgAABJIqAAAEkioAAASao5oGy32b7P9q31KAgAAKk+I6jLJI3VYT8AAOxXU0DZPkXSuZI+U59yAACoqPVzUB+XdIWkY2ZawXaPpB6p8gHFYrFYY5fAwsP7AjiYI6K6De03SXpjRLzbdkHS+yPiTYfaJp/PR6lUqqo/YL6zPeNj1b4PgYXA9mhE5Ke213KI70xJb7a9U9IXJb3e9udq2B8AAPtVHVAR8Y8RcUpEdEq6QNK3I+LtdasMANDS+BwU0AQnnXRSs0sAkleXi8VGRFFSsR77AlrB7t27m10CkDxGUACAJBFQAIAkEVBAg1x66aWH1Q60Or6wEGiQwcFBSdLw8LDK5bJyuZxWr169vx3Agar+oG41+KAuUFEsFlUoFJpdBpCE5+KDugAAPGcIKABAkggoAECSCCgAQJIIKABAkggoAECSCCgAQJIIKABAkggoAECSCCgAQJIIKABAkggoAECSCCgAQJIIKABAkggoAECSCCgAQJIIKABAkggoAECSCCgAQJIIKABAkggoAECSCCgAQJIIKABAkggoAECSCCgAQJIIKABAkggoAECSCCgAQJIIKABAkqoOKNtLbI/YHrO93fZl9SwMANDaFtew7T5Jl0fEvbaPkTRq+1sRsaNOtQEAWljVI6iIeDQi7s2Wfy1pTNLJ9SoMANDa6jIHZbtT0sslbanH/gAAqOUQnyTJ9tGSvirp7yPiyWke75HUI0kdHR0qFou1dgnMe3v27OG9AMzCEVH9xvYRkm6VtDki1s+2fj6fj1KpVHV/wEJRLBZVKBSaXQaQBNujEZGf2l7LWXyWtEHS2FzCCQCAw1HLHNSZkt4h6fW2t2a3N9apLgBAi6t6Dioi/keS61gLAAD7cSUJAECSCCgAQJIIKABAkggoAECSCCgAQJIIKABAkggoAECSCCgAQJIIKABAkggoAECSCCgAQJIIKKCB+vr61N7eru7ubrW3t6uvr6/ZJQHJqvkLCwHMTV9fn4aGhjQwMKCuri7t2LFD/f39kqTBwcEmVwekhxEU0CDDw8MaGBjQmjVr1N7erjVr1mhgYEDDw8PNLg1IEgEFNEi5XFZvb+8Bbb29vSqXy02qCEgbAQU0SC6X09DQ0AFtQ0NDyuVyTaoISBtzUECDrF69ev+cU1dXl9avX6/+/v6DRlUAKggooEEmToS46qqrVC6Xlcvl1NvbywkSwAwcEQ3rLJ/PR6lUalh/QKqKxaIKhUKzywCSYHs0IvJT25mDAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkqaaAsv0G2z+0/YDtK+tVFLBQbdy4UStWrNDKlSu1YsUKbdy4sdklAcmq+ivfbbdJ+jdJfy5pl6R7bG+KiB31Kg5YSDZu3Ki1a9dqw4YNGh8fV1tbm1atWiVJuvDCC5tcHZCeWkZQfyLpgYh4MCKelvRFSefVpyxg4Vm3bp02bNig7u5uLV68WN3d3dqwYYPWrVvX7NKAJFU9gpJ0sqSfTLq/S9Krpq5ku0dSjyR1dHSoWCzW0CUwf42NjWl8fFzFYlF79uxRsVjU+Pi4xsbGeF8A06gloDxNWxzUEHGdpOskKZ/PR6FQqKFLYP5avny52traVCgUVCwWVSgUNDIyouXLl4v3BXCwWg7x7ZK0ZNL9UyT9rLZygIVr7dq1WrVqlUZGRrRv3z6NjIxo1apVWrt2bbNLA5JUywjqHkmn2z5V0k8lXSDpr+pSFbAATZwI0dfXp7GxMS1fvlzr1q3jBAlgBo446Kjc3De23yjp45LaJF0fEYec7c3n81EqlaruD1goJg7xAZBsj0ZEfmp7LSMoRcQ3JH2jln0AADAdriQBAEgSAQUASBIBBQBIEgEFAEgSAQUASBIBBQBIUk2fgzrszuzHJT3csA6BdJ0g6RfNLgJIxNKIOHFqY0MDCkCF7dJ0H0wE8Fsc4gMAJImAAgAkiYACmuO6ZhcApI45KABAkhhBAQCSREABAJJEQCEJtt9re8z2E7avnGXd37N98yEe77R9f4317Jm0/DHb27OfH7D9/lr2PWm/Z9m+1/Y+2+fXY5/1Nun38vkqtu20zZeYomo1fR8UUEfvlvQXEfHQbCtGxM8kNfIP+rsknRgRZdsfqMcObS+W9IikiyXVJfCqrMOqzEU/O8Mqc/69TKNTlW/Z/kKV5aHFMYJC09keknSapE2232f7U1n7jbY/aft/bT84McqYPEKy/Ye2v2t7q+1ttk/Pdttmezgb+dxm+8hs/d+3/U3bo7b/2/ZLsvZTbd9l+x7bH5pU2yZJR0naYvttU+o+w/bdWb+32D5+lvai7Q/bvkPSZRGxMyK2SXp2yn5fZPvO7Dndb/u1Wfsltn9k+47suU1+nc6ftP2e7OfRtm/PRmnft33epNdvzPanJd0raYntf8ie+zbbH5zh93KU7euz9e6btL+2bHQ5sf27slKukfTa7Hm8r4p/Gmh1EcGNW9NvknaqcvmfiyV9Kmu7UdJXVPmPVJekB7L2Tkn3Z8uDki7Klp8n6cjs8X2Szsjavyzp7dny7ZJOz5ZfJenb2fImSX+dLb9H0p5JtU1e/oCk92fL2yS9Llv+F0kfn6W9KOnT0zz3GyWdP+n+5ZLWZsttko6R9CJVRlwnZs/zO1Nep/On1qvKEZIXZMsnSHpAkrPX51lJr84eO1uV096dvda3Sjpr8u8lW/7wpNfxOEk/UiW8eyT9U9aek1SSdKqkgqRbm/1vi9v8vXGID6n7elQOP+2w3THN43dJWmv7FElfi4gfV45a6aGI2JqtMyqp0/bRkl4j6SvZOlLlD6oknSnpLdnyZyUNHKoo28dKOi4i7siabsr2O237pE2/NNsTlnSPpOttH6HK899qe6WkYkQ8nvX/JUnLZtmPJX3Y9lmqBNLJkiZew4cj4u5s+ezsdl92/2hJp0u6c8r+zpb05klzcO2SXpy1v3TSKO7YbPun5/BcgRkRUEhdedKypz4YEV+wvUXSuZI22/5bSQ9O2W5clZHVIkm/jIgzZuirER8KfGq2FSLizixUzpX0Wdsfk/SkZq5vn7LD9dmc0vOy9otUGXG9MiKesb1TlVCZWoclfSQirp2lNEt6S0T88IDGSp99EbF5Snthlv0Bh8QcFOY126dJejAiPqnKYbqXzrRuRDwp6SHbb822te2XZQ9/R9IF2fJFs/UbEb+S9MTE/JCkd0i6Y6b2w3xOSyX9PCKGJW2Q9ApJWyQVbP9uNrJ666RNdkp6ZbZ8nqQjsuVjs/08Y7tb0tIZutws6W+yEaZsn2z7hTOs15cFkmy/fFL732V1yfYy20dJ+rUqhyeBqhBQmO/eJul+21slvUTSv8+y/kWSVtn+nqTtqvxBl6TLJL3H9j2q/GGfi3dK+pjtbZLOUGW+6VDtB7D9x7Z3qRI219renj1UkLTV9n2qHHb8REQ8qsr8112S/kuVkxsmDEt6ne3vqjKvNjE6+rykvO1S9rx/MF0dEXGbKmfa3WX7+5Ju1vTB8iFVwm+bKyepTJxM8hlJOyTdm7Vfq8rRmW2S9tn+HidJoBpc6giYh2xfLCkfEZc2uxbgucIICgCQJEZQAIAkMYICACSJgAIAJImAAgAkiYACACSJgAIAJOn/AcbMkxtP+QjUAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAWe0lEQVR4nO3dfZRcdX3H8c8nm7ALSQA1cUWgLLUUXINinQMlPiQUpNhoqBpbOT4hMXuS1vUhcBIeTlX0pBBs0nLAyiFC4ailSoQ2hKBQzAQlwGHDU0KC0iMoCEpyKJhQspDl2z/mbjLZzD5kZnLnl53365w9e++de+/vO5PMfvZ357e/64gQAACpGdPoAgAAqISAAgAkiYACACSJgAIAJImAAgAkaWyejU2aNCk6OjrybBJI0ksvvaTx48c3ugwgCevWrdsSEZMHbs81oDo6OtTT05Nnk0CSisWipk+f3ugygCTY/nWl7VziAwAkiYACACSJgAIAJGnYgLJ9re3nbG+o8Nh5tsP2pH1THgCgWY2kB3WdpDMGbrR9pKT3S/pNnWsCAGD4gIqIuyQ9X+Ghf5a0QBKzzQIA6q6qYea2Z0r6bUQ8bHu4fbskdUlSe3u7isViNU0Co8q2bdt4LwDD2OuAsn2QpIsknT6S/SPiaklXS1KhUAj+9gPNrLu7W8uWLVNvb69aW1s1Z84cXXHFFY0uC0hSNT2ot0g6WlJ/7+kISQ/YPjEiflfP4oDRpLu7W1dddZUWL16szs5Obdy4UQsXLpQkQgqowCO5YaHtDkkrI2JKhceelFSIiC3DnadQKAQzSaBZtbW1qVAoqKenZ2cPqn99+/btjS4PaBjb6yKiMHD7sD0o2zdImi5pku2nJX01Iq6pf4nA6Nbb26t7771Xl1122c4e1IIFC9TX19fo0oAkjWQU31kRcVhEjIuIIwaGU0R0jKT3BECaMWOG5s+fr7a2Ns2fP18zZsxodElAsphJAsjRqlWrtHTpUm3fvl1Lly7VqlWrGl0SkKxcZzMHmln/Z04XXnjhzs+gTjrpJGb4BwZBDwrIyZw5c7R27Vrt2LFDkrRjxw6tXbtWc+bMaXBlQJoIKABAkggoICfLli3T1KlTNXZs6cr62LFjNXXqVC1btqzBlQFp4jMoICe9vb26++67B10HsDt6UACAJBFQQM7a2tp2+w6gMgIKyFn/tEZMbwQMjYACcjZv3jzdcsstmjdvXqNLAZI2osli64XJYtHMhrp3Wp7vQyA1g00WSw8KAJAkAgrIyWA9qOHuSg00KwIKyMlgl/G4vAdURkABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJI0bEDZvtb2c7Y3lG37pu3HbD9i+2bbh+7TKgEATWckPajrJJ0xYNsdkqZExNsl/VLSBXWuCwDQ5IYNqIi4S9LzA7bdHhE7stV7JR2xD2oDADSxsXU4xzmSfjDYg7a7JHVJUnt7u4rFYh2aBEYX3hfAnkZ0R13bHZJWRsSUAdsvklSQ9JEYwYm4oy6aGXfUBSob7I66VfegbH9G0gclnTqScAIAYG9UFVC2z5C0UNK0iPi/+pYEAMDIhpnfIOkeScfaftr2bElXSpoo6Q7bD9m+ah/XCQBoMsP2oCLirAqbr9kHtQAAsBMzSQAAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJI0bEDZvtb2c7Y3lG17ve07bD+efX/dvi0TANBsRtKDuk7SGQO2nS/pzog4RtKd2ToAAHUzbEBFxF2Snh+w+UxJ12fL10v66/qWBQBodmOrPK49Ip6VpIh41vYbB9vRdpekLklqb29XsVissklg9OJ9AezJETH8TnaHpJURMSVbfyEiDi17/H8jYtjPoQqFQvT09FRfLbAfsz3oYyN5HwKjle11EVEYuL3aUXy/t31YduLDJD1XS3EAAAxUbUCtkPSZbPkzkv6rPuUAAFAykmHmN0i6R9Kxtp+2PVvSpZLeb/txSe/P1gEAqJthB0lExFmDPHRqnWsBAGAnZpIAACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkadhbvgMYnO2GnCci6tIukDICCqjB3gTFUCFE4AB74hIfACBJBBSQk8F6SfSegMoIKCBHEaGI0FELV+5cBlAZAQUASFJNAWX7y7Yftb3B9g222+pVGACguVUdULYPl/QFSYWImCKpRdLH61UYAKC51XqJb6ykA22PlXSQpGdqLwkAgBr+Dioifmv7nyT9RtLLkm6PiNsH7me7S1KXJLW3t6tYLFbbJDCq8F4AhlZ1QNl+naQzJR0t6QVJN9r+ZER8r3y/iLha0tWSVCgUYvr06VUXC4waP75VvBeAodVyie80SU9ExOaIeFXSTZKm1qcsAECzqyWgfiPpz20f5NIcLqdK2lSfsgAAza7qgIqI+yQtl/SApPXZua6uU10AgCbnPP+SvVAoRE9PT27tASPxjotv14svv9roMnJxyIHj9PBXT290GcBubK+LiMLA7cxmjqb34suv6slLZ+TaZrFYbMggiY7zb829TaBaTHUEAEgSAQUASBIBBQBIEp9BoelNfOv5Ov768/Nv+Pr8m5z4VknK9/M2oFoEFJre1k2XMkgCSBCX+AAASSKgAABJIqAAAEniMyhADfps5sf5t3nIgeNybxOoFgGFppf3AAmpFIiNaBfYn3CJDwCQJAIKAJAkAgoAkCQCCgCQJAIKAJAkAgoAkCQCCgCQJAIKAJAkAgoAkCRmkgByZHvX8uLS94hoUDVA2uhBATkpD6eRbAeaHT0ooAb1Cpe9PQ+9LjSDmnpQtg+1vdz2Y7Y32T65XoUB+4OIGPFXvc5DOKFZ1NqDulzSjyNilu0DJB1Uh5oAAKg+oGwfLOl9ks6WpIh4RdIr9SkLANDsarnE98eSNkv6N9sP2v6O7fF1qgsA0ORqucQ3VtKfSeqOiPtsXy7pfEn/UL6T7S5JXZLU3t6uYrFYQ5PA6MT7AtiTq/3A1fabJN0bER3Z+nslnR8Rg94mtFAoRE9PT1XtAfu7oUbqMfABzcz2uogoDNxe9SW+iPidpKdsH5ttOlXSxmrPBwBAuVpH8XVL+n42gu9Xkj5be0kAANQYUBHxkKQ9umUAANSKqY4AAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJqjmgbLfYftD2ynoUBACAVJ8e1BclbarDeQAA2KmmgLJ9hKQZkr5Tn3IAACiptQf1L5IWSHqt9lIAANhlbLUH2v6gpOciYp3t6UPs1yWpS5La29tVLBarbRIYtXhfAHtyRFR3oH2JpE9J2iGpTdLBkm6KiE8OdkyhUIienp6q2gP2d7YHfaza9yEwGtheFxGFgdurvsQXERdExBER0SHp45J+OlQ4AQCwN/g7KABAkqr+DKpcRBQlFetxLgAAJHpQAIBEEVAAgCQRUACAJBFQAIAkEVBAzpYsWaLbbrtNS5YsaXQpQNLqMooPwMgtWLBAfX19amlpaXQpQNLoQQE56+vr2+07gMoIKABAkggoAECSCCgAQJIIKCBnjOIDRoZRfECOJk+erAsvvFC9vb1qbW3V5MmTtXnz5kaXBSSJHhSQo82bN2v79u1avXq1tm/fTjgBQ6AHBeTMtqZMmaINGzY0uhQgafSggJyU3zW3PJy4my5QGQEF5CgiFBFavXr1zmUAlRFQAIAkEVBAjrq7u9XW1qZTTjlFbW1t6u7ubnRJQLIYJAHkpLu7W1dddZUWL16szs5Obdy4UQsXLpQkXXHFFQ2uDkgPPSggJ8uWLdPixYs1f/58tbW1af78+Vq8eLGWLVvW6NKAJNGDAnLS29ura665Rueee+7ObZ2dnert7W1gVUC66EEBOdq4caNmzpypm2++WTNnztTGjRsbXRKQLAIKyNm0adPU1tamadOmNboUIGlc4gNydM455+w2F98555yja6+9ttFlAUmqugdl+0jbq21vsv2o7S/WszBgNNqyZctuc/Ft2bKl0SUByaqlB7VD0rkR8YDtiZLW2b4jIrioDlRw/PHHa8WKFbK9x3YAe6q6BxURz0bEA9nyVkmbJB1er8IAAM2tLoMkbHdIeqek++pxPmA0Wr9+vWbOnLnbXHwzZ87U+vXrG10akKSaB0nYniDpR5K+FBF/qPB4l6QuSWpvb1exWKy1SWC/9corr+iAAw7Qq6++qnHjxunUU0+VJN4XQAWuZTZl2+MkrZT0k4hYOtz+hUIhenp6qm4P2J/1f/a0ZMmSnVMd9f/RLrOao5nZXhcRhYHbaxnFZ0nXSNo0knACULJmzRpt375da9asaXQpQNJqucT3bkmfkrTe9kPZtgsjYlXNVQGjVGdnp1asWKEVK1bsXGc2CaCyWkbx/TwiHBFvj4gTsi/CCRhEa2urZs+evdsgidmzZ6u1tbXRpQFJYiYJICdz5szZeXuNzs5OLV26VAsXLtTcuXMbXBmQJgIKyEn/PZ/KpzqaO3cu94ICBsFksQCAJNGDAnLCHXWBvUMPCsgJd9QF9g4BBeSkt7d3jwERc+fO5Y66wCC4xAfkpLW1VePHj6+4HcCe6EEBOSnvKV1wwQUVtwPYhYACctba2qpLLrmEnhMwDAIKyNHy5ct3u6Pu8uXLG10SkCwCCsjRrFmzhlwHsAsBBeTMtorF4h63fgewOwIKyEn5PZ8uvvjiitsB7EJAATkaM2bMkOsAduHdAeSkpaVFr732miZMmKBvf/vbmjBhgl577TW1tLQ0ujQgSQQUkJP+cNq6dauOO+44bd26dWdIAdgTAQXkaOBt3rntOzA4AgrI0bRp04ZcB7ALAQXkZMyYMdq2bZsmTpyoxx57TBMnTtS2bdsYKAEMgsligZz09fWppaVF27Zt07x58ySVQquvr6/BlQFp4lc3IEd9fX2KCK1evVoRQTgBQyCgAABJIqAAAEkioAAASappkITtMyRdLqlF0nci4tK6VAWMUpUmiGUuPqCyqntQtlskfUvSByR1SjrLdme9CgNGm/JwWrRoUcXtAHap5RLfiZL+JyJ+FRGvSPoPSWfWpyxg9IoITZ06lZ4TMIxaLvEdLumpsvWnJZ00cCfbXZK6JKm9vV3FYrGGJoH926JFi1QsFrVt2zYVi0UtWrRIF110Ee8LoAJX+1uc7Y9J+suI+Fy2/ilJJ0ZE92DHFAqF6Onpqao9YH/XfykvIlQsFjV9+vTdtgHNyva6iCgM3F7LJb6nJR1Ztn6EpGdqOB/QFGxr7dq1fPYEDKOWgLpf0jG2j7Z9gKSPS1pRn7KA0ae8l3TRRRdV3A5gl6oDKiJ2SPq8pJ9I2iTphxHxaL0KA0ajiNhtqiPCCRhcTX8HFRGrJK2qUy0AAOzETBIAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkVT3VUVWN2Zsl/Tq3BoF0TZK0pdFFAIk4KiImD9yYa0ABKLHdU2nuMQC7cIkPAJAkAgoAkCQCCmiMqxtdAJA6PoMCACSJHhQAIEkEFAAgSQQUGs722bavrPLY62zPGsH537yX5+2wvaFs/Qbbj9j+su2v2z5tmONn2j5/mJqqes7Z8dNtr6z2+H2l/HWq4tjptqfui7qwf6rpflDAfuJsSRskPVPNwbbfJGlqRBw10mMiYoVG4R2mbY/NblZa6bG9fp0GmC5pm6S1VR6PUYYeFPYZ25/Ofpt+2PZ3bX/I9n22H7T937bbKxzTbvvm7JiHbU+t0Js5z/bXKhz7Fdv3295g+2qXzJJUkPR92w/ZPtD2u2yvsb3O9k9sH5Yd/66szXsk/X3ZqW+X9Mbs+PeW99psP2n7YtsP2F5v+7hs+84eku2PZTU9bPuusvO+2faPbT9u+7Ky53G67Xuyc95oe0K2/Qzbj9n+uaSPlO0/Lavtoey1nZg99yttb7R9q+1VA2qelC0XbBez5RNtr83Osdb2sWXP5Ubbt0i63fZ429dmr/WDts8c5HV6S/b81tn+WdlrM9n2j7Lj77f9btsdkuZK+nL/8UP+50Jz6L/tNF981fNL0tsk/ULSpGz99ZJep10jRz8naUm2fLakK7PlH0j6UrbcIukQSR2SNpSd+zxJX8uWr5M0q7+Nsn2+K+lD2XJRUiFbHqfSb+iTs/W/lXRttvyIpGnZ8jf726zQfnmbT0rqzpb/TtJ3Kjyn9ZIOz5YPLXv8V9nza1NpCrAjVZoC6S5J47P9Fkr6SrbPU5KOkWRJP5S0MtvnFknvzpYnqHRl5COS7shewzdLemFAzf3/LgVJxWz5YEljs+XTJP2orNan+19fSf8o6ZP9z0fSLyWNr/A63SnpmGz5JEk/zZb/XdJ7suU/krQpW/6apPMa/X+Xr3S+uMSHfeUvJC2PiC2SFBHP2z5e0g+yHssBkp4Y5LhPZ8f0SXrR9utG2OYpthdIOkilQHxUpR/e5Y6VNEXSHbal0g/wZ20folJ4rMn2+66kD4yw3Zuy7+tU1rMpc7ek62z/sGxfSbozIl6UJNsbJR2l0g/8Tkl3Z/UdIOkeScdJeiIiHs/2/56krrLzL7X9fUk3RcTTtt8n6YbsNXzG9k9H8DwOkXS97WMkhUph3u+OiHg+Wz5d0kzb52XrbSoFzcv9O2e9vqmSbsyehyS1Zt9Pk9RZtv1g2xNHUB+aDAGFfcUq/ZArd4WkpRGxwvZ0lX5jHokd2v1ydNsejdltkv5VpZ7SU9klwD32y+p6NCJOHnD8oRXqHane7HufKrynImKu7ZMkzZD0kO0TBhxXfqxVCoOzBtR3wmD1RcSltm+V9FeS7vWuARyDPZ/y17P8NfqGpNUR8eHsklux7LGXysuR9NGI+MWAGjvKVsdIeiEiTqjQ/hhJJ0fEy+UbywILkMRnUNh37pT0N7bfIEm2X6/Sb+i/zR7/zBDHzcuOabF9sKTfq/TZxhtst0r6YIXj+n/Qbsl+ey8f2bdVUv9v6L+QNNn2yVkb42y/LSJeUKm39p5sv0/s1bMdgu23RMR9EfEVlWYwP3KI3e+V9G7bf5Ide5DtP5X0mKSjbb8l229ngGXnXx8RiyX1qNTbukvSx7PX8DBJp5S18aSkd2XLHy3bXv7vc/YQNf5EUrezRLH9zoE7RMQfJD1h+2PZPrb9juzh2yV9vqz+E7LF8n8ngIDCvhERj0paJGmN7YclLVWpx3Sj7Z9p8FtNfFGlS3XrVbpk9raIeFXS1yXdJ2mlSj+sB7b3gqRlKn3e85+S7i97+DpJV9l+SKVLerMkLc7qekilS1GS9FlJ33JpkMRuv93X6JsuDaDYoFJwPDzYjhGxWaVwuMH2IyoF1nERsV2lS3q3ZoMkym9b8yVngzCyum+TdLOkx1V6Pb4taU3Z/hdLujz7d+gr236ZpEts363S6zSYb6h0+e+R7Dl9Y5D9PiFpdlbXo5L6B1N8QVLBpQE0G1UaHCGVLsd+mEES6MdUR0ATsH2dSoMqlje6FmCk6EEBAJJEDwoAkCR6UACAJBFQAIAkEVAAgCQRUACAJBFQAIAk/T/xfjhtHwRk2wAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAWA0lEQVR4nO3df5RcZX3H8c8nG8wqiQZK2FIQFnpQSJP4a49aRLM5FIJiieeIpyJalZU0UaIGagG3HsuxKQQNR8SSlZAUajGtodpGFCHFTCgG0Q0gCVn8USBKpSY5JJqNZiHZb/+Ym83sZnY3mZnceXb3/Tpnztz7zJ37fHdh8tnn3jvPdUQIAIDUjKt3AQAAlENAAQCSREABAJJEQAEAkkRAAQCSND7Pzo477rhobm7Os0sgSbt379bRRx9d7zKAJGzYsGF7REwZ2J5rQDU3N6uzszPPLoEkFQoFtba21rsMIAm2t5Rr5xAfACBJBBQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBSQo5UrV2ratGk655xzNG3aNK1cubLeJQHJyvWLusBYtnLlSrW3t2v58uXat2+fGhoa1NbWJkm6+OKL61wdkB5GUEBOFi1apOXLl2vWrFkaP368Zs2apeXLl2vRokX1Lg1IEgEF5KSrq0tnn312v7azzz5bXV1ddaoISBsBBeTkzDPP1IMPPtiv7cEHH9SZZ55Zp4qAtBFQQE7a29vV1tamtWvXau/evVq7dq3a2trU3t5e79KAJDkicuuspaUlmM0cY9mMGTO0cePGvvXp06fr8ccfr2NFQP3Z3hARLQPbGUEBOZk9e7Y2btyo+fPn61vf+pbmz5+vjRs3avbs2fUuDUgSl5kDOVmzZo3mz5+vW265RYVCQbfccoskqaOjo86VAWliBAXkJCJ03XXX9Wu77rrrlOdhdmAkIaCAnNjWNddc06/tmmuuke06VQSkjUN8QE7OPfdcLV26VJL0jne8Qx/96Ee1dOlSnXfeeXWuDEgTV/EBOZo9e7bWrFmjiJBtnXvuubr33nvrXRZQV4NdxccICsjR/jAqFApqbW2tbzFA4jgHBQBIEgEF5IjbbQCHbthDfLZXSHqnpK0RMS1r+7ykP5f0gqT/kfThiNh5BOsERjxutwEcnkMZQd0u6fwBbWskTYuIGZJ+KumagW8C0B+32wAOz7ABFREPSHp+QNt9EbE3W/2BpJOOQG3AqNLV1aVVq1apsbFRs2bNUmNjo1atWsXtNoBB1OIc1KWS7qnBfoBRbfLkyero6NAxxxyjcePG6ZhjjlFHR4cmT55c79KAJFV1mbntdkl7Jd05xDZzJc2VpKamJhUKhWq6BEasHTt2KCK0detW9fb2auvWrYoI7dixg88FUMYhfVHXdrOku/dfJJG1fVDSPEnnRMTvDqUzvqiLscy2bKupqUlbt27V8ccfr1//+teKCObjw5hW09tt2D5f0lWSLjzUcAIgnXzyydqxY4d6e3u1Y8cOnXzyyfUuCUjWsAFle6WkhyS92vazttskfVnSJElrbD9mm/sFAIdgy5Yt6unpkST19PRoy5Ytda4ISNew56AiotwXNJYfgVoAAOjDTBJAziZOnCjbmjhxYr1LAZJGQAE5OvbYY7V7925FhHbv3q1jjz223iUBySKggBw9//zzfTcotK3nn39+mHcAYxcBBeSst7e33zOA8ggoAECSCCggRwOnNWKaI2BwBBSQo507dw65DuAAAgrI2VFHHdXvGUB5BBSQsxdffLHfM4DyCCgAQJIIKABAkggoAECSCCgAQJIIKABAkggoAECSCCgAQJIIKABAkggoAECSCCgAQJIIKABAkggoAECSCCgAQJIIKABAkggoAECSCCgAQJIIKABAkoYNKNsrbG+1vamk7Vjba2z/LHs+5siWCQAYaw5lBHW7pPMHtF0t6f6IOF3S/dk6AAA1M2xARcQDkp4f0DxH0h3Z8h2S3lXbsgAAY12l56CaIuI5Scqej69dSQAASOOPdAe250qaK0lNTU0qFApHuktgxOFzARzMETH8RnazpLsjYlq2/hNJrRHxnO0TJBUi4tXD7aelpSU6OzurLBkYmWwP+tqhfA6B0cr2hohoGdhe6SG+1ZI+mC1/UNJ/VloYAADlHMpl5islPSTp1baftd0m6XpJ59r+maRzs3UAAGpm2HNQEXHxIC+dU+NaAADow0wSAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkja93AcBIZrsu+4mImvQLpIyAAqpwOEExVAgROMDBOMQHAEgSAQXkZLBREqMnoLyqAsr2QttP2N5ke6XtxloVBoxGEaGI0ClX3d23DKC8igPK9omSPi6pJSKmSWqQ9N5aFQYAGNuqPcQ3XtJLbY+X9DJJv6q+JAAAqgioiPhfSV+Q9AtJz0n6TUTcV6vCAABjW8WXmds+RtIcSadK2ilple33R8S/DNhurqS5ktTU1KRCoVBxscBowmcBGFo134P6M0lPR8Q2SbL9DUlnSeoXUBFxq6RbJamlpSVaW1ur6BIYJb77bfFZAIZWzTmoX0h6s+2XufgNxHMkddWmLADAWFfNOaiHJd0l6RFJG7N93VqjugAAY1xVUx1FxGclfbZGtQAA0IeZJAAASWKyWIx5r7n2Pv3m9y/m3m/z1d/Ovc9XvPQo/fiz5+XeL1AJAgpj3m9+/6Keuf6CXPssFAp1uYqvHqEIVIpDfACAJBFQAIAkEVAAgCRxDgpj3qQzr9b0O67Ov+M78u9y0pmSlO/5NqBSBBTGvF1d13ORBJAgDvEBAJJEQAEAkkRAAQCSxDkoQHU6N/Pd+swkAYwUBBTGvLwvkJCKgViPfoGRhEN8AIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJDGTBJAj2weWFxefI6JO1QBpYwQF5KQ0nA6lHRjrCCgAQJI4xAdUoVajn8PdD4cFMRZUNYKyPdn2XbaftN1l+09rVRgwEkTEIT9qtR/CCWNFtSOomyR9NyIusv0SSS+rQU0AAFQeULZfLultkj4kSRHxgqQXalMWAGCsq+YQ32mStkn6J9uP2r7N9tE1qgsAMMZVc4hvvKTXS1oQEQ/bvknS1ZI+U7qR7bmS5kpSU1OTCoVCFV0CoxOfC+BgrvSEq+0/lPSDiGjO1t8q6eqIGPQ+1i0tLdHZ2VlRf8BIN9SVelz4gLHM9oaIaBnYXvEhvoj4P0m/tP3qrOkcSZsr3R8AAKWqvYpvgaQ7syv4npL04epLAgCgyoCKiMckHTQsAwCgWkx1BABIEgEFAEgSAQUASBIBBQBIEgEFAEgSAQUASBIBBQBIEgEFAEgSAQUASBIBBQBIEgEFAEgSAQUASBIBBQBIEgEFAEgSAQUASBIBBQBIEgEFAEgSAQUASBIBBQBIEgEFAEgSAQUASBIBBQBIEgEFAEgSAQUASBIBBQBIEgEFAEhS1QFlu8H2o7bvrkVBAABItRlBfUJSVw32AwBAn6oCyvZJki6QdFttygEAoKjaEdQXJf2NpN7qSwEA4IDxlb7R9jslbY2IDbZbh9hurqS5ktTU1KRCoVBpl8CoxecCOJgjorI32tdJ+oCkvZIaJb1c0jci4v2DvaelpSU6Ozsr6g8Y6WwP+lqln0NgNLC9ISJaBrZXfIgvIq6JiJMiolnSeyV9b6hwAgDgcPA9KABAkio+B1UqIgqSCrXYFwAAEiMoAECiCCgAQJIIKABAkggoAECSCCggZw0NDf2eAZRHQAE5u+GGG3TPPffohhtuqHcpQNJqcpk5gEP3qU99Sr29vRo3jr8PgaHwCQFy1tvb2+8ZQHkEFAAgSQQUACBJBBQAIEkEFJCzJUuW6J577tGSJUvqXQqQNK7iA3I0ZcoUffrTn1ZPT48mTJigKVOmaNu2bfUuC0gSIyggR9u2bdOePXu0du1a7dmzh3AChsAICsiZbU2bNk2bNm2qdylA0hhBATkpva17aThxu3egPAIKyFFEKCK0du3avmUA5RFQAIAkEVBAjhYsWKDGxkbNmjVLjY2NWrBgQb1LApLFRRJAThYsWKCOjg4tXrxYU6dO1ebNm3XVVVdJkm6++eY6VwekhxEUkJNly5Zp8eLFuuKKK9TY2KgrrrhCixcv1rJly+pdGpAkRlBATnp6erR8+XJdeeWVfW1Tp05VT09PHasC0sUICsjR5s2bdeGFF+qb3/ymLrzwQm3evLneJQHJIqCAnM2cOVONjY2aOXNmvUsBksYhPiBHl156ab+5+C699FKtWLGi3mUBSWIEBeRo+/bt/ebi2759e71LApJVcUDZfqXttba7bD9h+xO1LAwYbaZPn67Vq1drzpw52rlzp+bMmaPVq1dr+vTp9S4NSJIrnWrF9gmSToiIR2xPkrRB0rsiYtCzvi0tLdHZ2VlZpcAo0NjY2O+qvQkTJmjPnj11rAioP9sbIqJlYHvFI6iIeC4iHsmWd0nqknRi5SUCo9uMGTPU09PT7yq+np4ezZgxo96lAUmqyUUStpslvU7Sw2VemytpriQ1NTWpUCjUoktgxNm4caPOOussLVy4UN3d3Vq4cKG2b9+u9evX87kAyqg6oGxPlPTvkj4ZEb8d+HpE3CrpVql4iK+1tbXaLoER64wzztD555/fdxXfJZdcovXr14vPBXCwqgLK9lEqhtOdEfGN2pQEjF4rVqzQkiVL+ubiK51VAkB/1VzFZ0nLJXVFxI21KwkY3datW6c9e/Zo3bp19S4FSFo1I6i3SPqApI22H8vaPh0R36m6KmCUmjp1qlavXq3Vq1f3rTPdEVBeNVfxPRgRjogZEfHa7EE4AYOYMGGC2tra+t1Rt62tTRMmTKh3aUCSmOoIyMlll13Wd/+nqVOn6sYbb9RVV12lefPm1bkyIE0EFJCT/TclLJ2Lb968edysEBgEc/EBAJLECArICbd8Bw4PIyggJ9zyHTg8BBSQk56enoMuiJg3bx63fAcGQUABOZkwYYI6Ojr6tXV0dHCZOTAIzkEBObnssst05ZVXHjS90eWXX16nioC0VXw/qEpwPyiMZcXZwcrL83MIpKbm94MCUJnSmSQADI6AAnJ01113DbkO4AACCsjRRRddNOQ6gAMIKCBntlUoFIY8JwWAgAJyU3rO6dprry3bDuAAAgrI0bhx44ZcB3AAnw4gJw0NDert7dXEiRO1dOlSTZw4Ub29vWpoaKh3aUCSCCggJ/vDadeuXTrjjDO0a9euvpACcDACCsjRunXrhlwHcAABBeRo5syZQ64DOICAAnIybtw4dXd3a9KkSXryySc1adIkdXd3c6EEMAgmiwVysm/fPjU0NKi7u1vz58+XVAytffv21bkyIE386QbkaN++ff3m4iOcgMERUACAJBFQAIAkEVAAgCQRUACAJFV1FZ/t8yXdJKlB0m0RcX1NqgJGqXIzmDNZLFBexSMo2w2S/lHS2yVNlXSx7am1KgwYbUrDadGiRWXbARxQzSG+N0r6eUQ8FREvSPpXSXNqUxYwekWEzjrrLEZOwDCqOcR3oqRflqw/K+lNAzeyPVfSXElqampSoVCooktgZFu0aJEKhYK6u7tVKBS0aNEitbe387kAynClf8XZfo+k2RHxkWz9A5LeGBELBntPS0tLdHZ2VtQfMNLtP5QXESoUCmptbe3XBoxVtjdERMvA9moO8T0r6ZUl6ydJ+lUV+wPGBNtav349556AYVQTUD+SdLrtU22/RNJ7Ja2uTVnA6FM6Smpvby/bDuCAigMqIvZKulzSvZK6JH09Ip6oVWHAaBQR/ebiI5yAwVX1PaiI+I6k79SoFgAA+jCTBAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEkVT3VUUWf2NklbcusQSNdxkrbXuwggEadExJSBjbkGFIAi253l5h4DcACH+AAASSKgAABJIqCA+ri13gUAqeMcFAAgSYygAABJIqAAAEkioDAi2P647S7bO2xfPcy2f2T7riFeb7a9qcp6uqt5/5FQ8ju6s4L3Ntt+X8n6H9hea7vb9pdL2l9m+9u2n7T9hO3ra1U/MFBV94MCcvRRSW+PiKeH2zAifiXpoiNfUr5cvEe8I6J3kE0O+XdURrOk90n6Wra+R9JnJE3LHqW+EBFrsztp32/77RFxTwV9AkNiBIXk2e6QdJqk1bYX7v+L3vbttr9ke73tp2xflLX3jZBs/4ntH9p+zPbjtk/Pdttge1k2CrjP9kuz7f/Y9ndtb7D937bPyNpPtf2Q7R/Z/lxJbSfYfiDb/ybbb83aP2z7p7bXZf2U1nxRyfu7s+eJtu+3/YjtjbbnlPwsXbZvkfSIpFfa/lRWx+O2rx3kd3S07RXZdo+W7K/B9udL3v9XWSnXS3pr9nMsjIjdEfGgikHVJyJ+FxFrs+UXsppOqva/MVDW/ttO8+CR8kPSMypOD/QhSV/O2m6XtErFP7SmSvp51t4saVO2fLOkS7Lll0h6afb6Xkmvzdq/Lun92fL9kk7Plt8k6XvZ8mpJf5ktf0xSd7Z8paT2bLlB0iRJJ0j6haQpWZ/fH1DzRSU/1/79jJf08mz5OEk/l+Ss1l5Jb85eO0/FS9Sd/dx3S3pb6e8oW/6Hkp9psqSfSjpa0lxJf5u1T5DUKelUSa2S7i7ze+/7fZd5bbKkpySdVu//P3iMzgeH+DDS/UcUD3lttt1U5vWHJLXbPknSNyLiZ8UjZXo6Ih7Lttkgqdn2RElnSVqVbSMV/xGXpLdIene2/FVJi7PlH0laYfuorJbHbJ8jqRAR2yTJ9r9JetUwP4cl/YPtt6kYSCdK2v/zbImIH2TL52WPR7P1iZJOl/TAgP2dJ+lC23+drTdKOjlrn1EyintF9v4Xhqmvf7H2eEkrJX0pIp46nPcCh4qAwkjXU7LsgS9GxNdsPyzpAkn32v6Iin/1l75vn4ojq3GSdkbEawfp66AvDUbEA1moXCDpq7Y/L+m35bbN7M362X9O6SVZ+yUqjrjeEBEv2n5GxVCRpN0DfsbrIuIrg+y/dLt3R8RP+jUW+1wQEfcOaG8dZn8D3SrpZxHxxcN8H3DIOAeFUc32aZKeiogvqXiYbsZg20bEbyU9bfs92Xtt+zXZy9+X9N5s+ZKS/Z8iaWtELJO0XNLrJT0sqTW7Eu4oSe8p6eYZSW/IludIOipbfkW2nxdtz5J0yiBl3ivp0my0J9sn2j5+kO0WZIEk268raZ+f1SXbr7J9tKRdKh6eHJbtv8/q/eShbA9UioDCaPcXkjbZfkzSGZL+eZjtL5HUZvvHkp5QMUQk6ROSPmb7Ryr+47xfq6THbD+q4iHAmyLiOUl/p+Lhxf9S8UKC/ZZJmmn7hyqe49o/OrpTUovtzqyGJ8sVFxH3qXil3UO2N0q6S+WD5XMqht/j2QUj+y/suE3SZkmPZO1fUfFIyuOS9tr+se2FkpSN4m6U9CHbz9qemh0qbVfxnN8j2UUVHyn7mwSqxFRHwBFm+0OSWiLi8nrXAowkjKAAAEliBAUASBIjKABAkggoAECSCCgAQJIIKABAkggoAECS/h/7mIwwA5NGNwAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAXkElEQVR4nO3df5BlZX3n8fd3ftAtMyoBtGHRpdUimm5GCelSjKU7EzZsgRbzR0gtYFZlDWMLdoyt1MyCMb+crF0EqhBq7dAmMWaFbKSCTIlB3Oz06maF0IPD4AyQneAoCIOCAWx+tPz47h/3DN650z39Y5p7Hvq+X1W3+tznPPecb49cP/0857nnRmYiSVJpltVdgCRJ0zGgJElFMqAkSUUyoCRJRTKgJElFMqAkSUVaMZdOEfEx4LeBBO4EzsvMp5v2B3AFcAbwJPCBzLz9YMc8+uijs7e3d4FlS0vDE088wapVq+ouQ6rVtm3bHs7MV7W2zxpQEXEc8DtAX2Y+FRF/C5wNfKGp2+nACdXjbcDnqp8z6u3tZWJiYs6/gLQUjY+Ps3bt2rrLkGoVEd+frn2uU3wrgJdFxArgcOCBlv3rgS9mwy3AERFx7IKrlSR1vFkDKjN/CPwp8APgQeCxzLy5pdtxwH1Nz++v2iRJWpC5TPH9Ao0R0uuAR4EvR8RvZeZ/b+42zUsPuIdSRGwANgD09PQwPj6+gJKlpWNyctL3gTSDuSyS+PfA9zLzxwAR8XfArwLNAXU/8Nqm56/hwGlAMvNq4GqAgYGBdO5dnc5rUNLM5nIN6gfAKRFxeLVa71TgrpY+W4D3RcMpNKYBH1zkWiVJHWTWEVRm3hoR1wG3A88C3wGujojBav8o8DUaS8x301hmft6LVrEkqSPMaRVfZv5+Zr4pM0/MzP+UmVOZOVqFE9XqvQsz8w2ZuSYzXT8uHcTQ0BDd3d2sW7eO7u5uhoaG6i5JKs6cPqgrafEMDQ0xOjrKyMgIfX197Nq1i40bNwJw5ZVX1lydVA5vdSS12djYGCMjIwwPD9Pd3c3w8DAjIyOMjY3VXZpUFANKarOpqSkGBwf3axscHGRqaqqmiqQyGVBSm3V1dTE6Orpf2+joKF1dXTVVJJXJa1BSm51//vkvXHPq6+vj8ssvZ+PGjQeMqqROZ0BJbbZvIcTFF1/M1NQUXV1dDA4OukBCahGZB9yRqC0GBgbSu5mr03knCQkiYltmDrS2ew1KklQkA0qSVCQDSpJUJANKklQkV/FJh6hxk//2q2uBk9QujqCkQ5SZC34cv/GrC36ttNQZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCLNGlAR8caI2N70eDwifrelz9qIeKypz6detIolSR1h1m/Uzcx7gJMAImI58EPg+mm6fisz37Oo1UmSOtZ8p/hOBf4lM7//YhQjSdI+s46gWpwNXDvDvrdHxB3AA8AnMnNna4eI2ABsAOjp6WF8fHyep5eWHt8H0vQiM+fWMeIwGuHTn5kPtex7BfB8Zk5GxBnAFZl5wsGONzAwkBMTEwssW1oaejfdyJ7PvLvuMqRaRcS2zBxobZ/PFN/pwO2t4QSQmY9n5mS1/TVgZUQcveBqJUkdbz4BdQ4zTO9FxDEREdX2W6vjPnLo5UmSOtWcrkFFxOHArwMfamobBMjMUeAs4MMR8SzwFHB2znXuUJKkacwpoDLzSeColrbRpu2rgKsWtzRJUifzThKSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIs33CwulJestf3gzjz31TNvP27vpxrae75UvW8kdv39aW88pLYQBJVUee+qZtn954Pj4OGvXrm3rOdsdiNJCOcUnSSqSASVJKpIBJUkqkgElSSqSASVJKpKr+KTKy39pE2v+alP7T/xX7T3dy38JoL2rFaWFMKCkyk/v+ozLzKWCOMUnSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSrSrAEVEW+MiO1Nj8cj4ndb+kREfDYidkfEjog4+UWrWJLUEWb9oG5m3gOcBBARy4EfAte3dDsdOKF6vA34XPVTkqQFme8U36nAv2Tm91va1wNfzIZbgCMi4thFqVCS1JHmG1BnA9dO034ccF/T8/urNkmSFmTO9+KLiMOAM4H/Mt3uadpymmNsADYA9PT0MD4+PtfTS23R7v8mJycna3kf+N7TS8F8bhZ7OnB7Zj40zb77gdc2PX8N8EBrp8y8GrgaYGBgINt9k0zpoG66se03bq3jZrF1/J7SQsxniu8cpp/eA9gCvK9azXcK8FhmPnjI1UmSOtacRlARcTjw68CHmtoGATJzFPgacAawG3gSOG/RK5UkdZQ5BVRmPgkc1dI22rSdwIWLW5okqZN5JwlJUpEMKElSkQwoSVKRDChJUpHm8zkoacnr3XRj+096U3vP+cqXrWzr+aSFMqCkyp7PvLvt5+zddGMt55VeCpzikyQVyYCSJBXJgJIkFcmAkiQVyYCSJBXJgJIkFcmAkiQVyYCSJBXJgJIkFcmAkiQVyYCSJBXJgJIkFcmAkiQVyYCSJBXJgJIkFcmAkiQVyYCSJBXJgJIkFcmAkiQVyYCSJBXJgJIkFWlOARURR0TEdRFxd0TcFRFvb9m/NiIei4jt1eNTL065kqROsWKO/a4AbsrMsyLiMODwafp8KzPfs3ilSZI62awBFRGvAN4FfAAgM38G/OzFLUuS1OnmMsX3euDHwF9GxHci4vMRsWqafm+PiDsi4u8jon9xy5QkdZq5TPGtAE4GhjLz1oi4AtgE/F5Tn9uB4zNzMiLOAL4CnNB6oIjYAGwA6OnpYXx8/NCql5YA3wfS9CIzD94h4hjglszsrZ6/E9iUme8+yGv2AAOZ+fBMfQYGBnJiYmIhNUtLRu+mG9nzmRnfSlJHiIhtmTnQ2j7rFF9m7gXui4g3Vk2nArtaDn5MRES1/dbquI8cctWSpI4111V8Q8CXqhV89wLnRcQgQGaOAmcBH46IZ4GngLNztqGZJEkHMaeAysztQOvwa7Rp/1XAVYtXliSp03knCUlSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpEMKElSkeZ6Lz5Ji6i6t3Jje6Tx09tXSvtzBCW1WXM4zaVd6lSOoKRDtJjBMp9jOeLSUmdASYdovkFxsBAydKSfc4pPklQkA0qq0THHHFN3CVKxDCipRnv37q27BKlYBpQkqUgGlCSpSAaUJKlIBpQkqUgGlCSpSAaUJKlIBpQkqUgGlCSpSAaUJKlIBpQkqUgGlCSpSHMKqIg4IiKui4i7I+KuiHh7y/6IiM9GxO6I2BERJ7845UqSOsVcvw/qCuCmzDwrIg4DDm/ZfzpwQvV4G/C56qckSQsy6wgqIl4BvAv4c4DM/FlmPtrSbT3wxWy4BTgiIo5d7GIlSZ1jLiOo1wM/Bv4yIt4CbAM+mplPNPU5Driv6fn9VduDzQeKiA3ABoCenh7Gx8cXXrm0BPmekH5uLgG1AjgZGMrMWyPiCmAT8HtNfab7DusDvrs6M68GrgYYGBjItWvXzrtgaSnzPSH93FwWSdwP3J+Zt1bPr6MRWK19Xtv0/DXAA4deniSpU80aUJm5F7gvIt5YNZ0K7GrptgV4X7Wa7xTgscx8EEkHdfTRR9ddglSsua7iGwK+VK3guxc4LyIGATJzFPgacAawG3gSOO9FqFVach5++OG6S5CKNaeAysztwEBL82jT/gQuXLyyJEmdzjtJSJKKZEBJkopkQEmSimRASZKKZEBJkopkQEmSimRASZKKZEBJNTnzzDO5/vrrOfPMM+suRSpSND5j234DAwM5MTFRy7mlOkVMd2/lhrrej1KdImJbZrbeDMIRlCSpTAaUJKlIBpQkqUgGlFSTrq6u/X5K2p8BJdVkampqv5+S9mdASW32kY98ZF7tUqdymbnUZsuXLwfg0ksvpa+vj127dnHRRRcB8Nxzz9VZmlQLl5lLhXj++efZvHkzw8PDdHd3Mzw8zObNm3n++efrLk0qigElSSqSU3xSm61YsWLaqbzly5fz7LPP1lCRVC+n+KRCzHSdyetP0v4MKElSkQwoqSbezVw6OANKqskNN9zAEUccwQ033FB3KVKRDCipJhHB2NjYQb9+Q+pkBpRUo2uuuabuEqRiGVBSjTZv3lx3CVKxDCipRpdcckndJUjFmlNARcSeiLgzIrZHxAGfro2ItRHxWLV/e0R8avFLlZaO1atXH/S5pPmNoNZl5knTfdq38q1q/0mZ+UeLUZy0VE1OTtLf38+1115Lf38/k5OTdZckFWdF3QVInWrnzp2cc845dZchFWuuI6gEbo6IbRGxYYY+b4+IOyLi7yOif5HqkyR1qLmOoN6RmQ9ExKuBb0TE3Zn5zab9twPHZ+ZkRJwBfAU4ofUgVbhtAOjp6WF8fPyQipdeyrZu3crk5CSrV69m3bp1AL4npCbzvpt5RPwBMJmZf3qQPnuAgcx8eKY+3s1cnWrfB3P7+/v55Cc/yac//Wl27twJQF3fLiDVaaa7mc86goqIVcCyzPxptX0a8EctfY4BHsrMjIi30pg6fGRxSpeWJq9BSQc3lym+HuD66q++FcA1mXlTRAwCZOYocBbw4Yh4FngKODv9U1Ca1rJly6b99txly/xYotRs1oDKzHuBt0zTPtq0fRVw1eKWJi1NF1xwAVdddeDb5YILLqihGqlc/skmtdnY2BiXXXYZmcnWrVvJTC677DLGxsbqLk0qip+DktpsamqKe+65h+7ubqampujq6uL9738/U1NTdZcmFcWAktps2bJlfP7zn+fSSy+lr6+PXbt2cdFFF3kNSmphQEltFhE899xzfPzjH9+vffny5TVVJJXJP9mkNnvuuefm1S51KgNKqkFPT89+iyR6enrqLkkqjlN8Ug0eeughv+pdmoUjKElSkQwoqSbN3wcl6UBO8Uk18V580sE5gpJqtHnz5rpLkIplQEk1WblyJatXr2blypV1lyIVySk+qSbPPPMMH/3oR+suQyqWIyipJi6SkA7OgJJqsnPnTu69994Xvk1X0v4MKKlGl1xySd0lSMUyoKQa9Pf373erI6f5pAMZUFKbHXnkkezcuZMTTzyRvXv3cuKJJ7Jz506OPPLIukuTiuIqPqnNHnnkEY466qj9Pqh75JFH8sgjj9RcmVQWR1BSDc4991y6uroA6Orq4txzz625Iqk8jqCkNhsaGmJ0dJSRkZEXvlF348aNAFx55ZU1VyeVwxGU1GZjY2OMjIwwPDxMd3c3w8PDjIyMMDY2VndpUlEMKKnNpqamGBwc3K9tcHCQqampmiqSymRASW3W1dXF6Ojofm2jo6MvXJOS1OA1KKnNzj///BeuOfX19XH55ZezcePGA0ZVUqczoKQ227cQ4uKLL2Zqaoquri4GBwddICG1iMys5cQDAwM5MTFRy7mlUoyPj7N27dq6y5BqFRHbMnOgtX1O16AiYk9E3BkR2yPigFSJhs9GxO6I2BERJy9G0ZKkzjWfKb51mfnwDPtOB06oHm8DPlf9lCRpQRZrFd964IvZcAtwREQcu0jHliR1oLkGVAI3R8S2iNgwzf7jgPuant9ftUmStCBzneJ7R2Y+EBGvBr4REXdn5jeb9sc0rzlg9UUVbhsAenp6GB8fn2+90pIyOTnp+0CawZwCKjMfqH7+KCKuB94KNAfU/cBrm56/BnhgmuNcDVwNjVV8rl5Sp3MVnzSzWaf4ImJVRLx83zZwGvDdlm5bgPdVq/lOAR7LzAcXvVpJUseYywiqB7g+Ivb1vyYzb4qIQYDMHAW+BpwB7AaeBM57ccqVJHWKWQMqM+8F3jJN+2jTdgIXLm5pkqRO5s1iJUlFMqAkSUUyoCRJRTKgpBoMDQ3R3d3NunXr6O7uZmhoqO6SpOL4dRtSmw0NDTE6OsrIyAh9fX3s2rXrhe+H8is3pJ9zBCW12djYGCMjIwwPD9Pd3c3w8DAjIyOMjY3VXZpUFANKarOpqakDvj13cHCQqampmiqSyuQUn9RmXV1dvOENb2Dv3r0vtB1zzDF0dXXVWJVUHkdQUputWrWKvXv30t/fz7XXXkt/fz979+5l1apVdZcmFcURlNRmP/nJT+jt7WX37t2cc845dHV10dvby549e+ouTSqKIyipBrfddhtPP/00W7du5emnn+a2226ruySpOAaUVIMPfvCDB30uyYCS2m7NmjVs2bKF9evX8+ijj7J+/Xq2bNnCmjVr6i5NKko0bkTefgMDAzkxMVHLuaW6vfnNb+bOO+984fmaNWvYsWNHjRVJ9YmIbZk50NruCEqqwY4dO8hMtm7dSmYaTtI0DChJUpEMKElSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpFq+6BuRPwY+H4tJ5fKcTTwcN1FSDU7PjNf1dpYW0BJgoiYmO4T9JKc4pMkFcqAkiQVyYCS6nV13QVIpfIalCSpSI6gJElFMqD0khIRvxMRd0XEv0bEpln6/puIuO4g+3sj4ruHWM/kobz+xdD0b/SlBby2NyLObXp+VERsjYjJiLiqpe9NEXFHROyMiNGIWL4Y9Uv7OMWnl5SIuBs4PTO/twjH6gW+mpknHsIxJjNz9aHWMs9zBo337vMz7F/wv1FErAU+kZnvqZ6vAn4ZOBE4MTM/0tT3FZn5eFXPdcCXM/Nv5ntOaSaOoPSSERGjwOuBLRHxsX1/0UfEFyLisxHxfyPi3og4q2p/YYQUEf0R8U8RsT0idkTECdVhl0fEWDUKuDkiXlb1f0M1QtgWEd+KiDdV7a+LiG9HxG0R8cdNtR0bEd+sjv/diHhn1X5eRPxzRPzv6jzNNZ/V9PrJ6ufqiPiHiLg9Iu6MiPVNv8tdEfHfgNuB10bERVUdOyLiD2f4N1oVEX9R9ftO0/GWR8SlTa//UFXKZ4B3Vr/HxzLzicz8P8DTrf97ZObj1eYK4DDAv3a1uDLTh4+XzAPYQ+PuCx8ArqravgB8mcYfXH3A7qq9F/hutX0l8N5q+zDgZdX+Z4GTqva/BX6r2v4H4IRq+23A/6q2twDvq7YvBCar7Y8Dl1Tby4GXA8cCPwBeVZ3zH1tqPqvp99p3nBXAK6rto4HdQFS1Pg+cUu07jcYKwKh+768C72r+N6q2/6TpdzoC+GdgFbAB+GTV3gVMAK8D1tIYVbb+u7/w793S/nXgX4FrgOV1//fhY2k9VkybWtJLz1eyMeW1KyJ6ptn/beCSiHgN8HeZ+f8aM1N8LzO3V322Ab0RsRr4VeDLVR9o/J84wDuA36i2/xoYqbZvA/4iIlZWtWyPiFOB8cz8MUBE/A/gF2f5PQL4k4h4F41AOg7Y9/t8PzNvqbZPqx7fqZ6vBk4AvtlyvNOAMyPiE9XzbuDfVu1vbhrFvbJ6/c9mqW8/mfkfIqIb+BLwa8A35vN66WAMKC0VU03b0bozM6+JiFuBdwNfj4jfBu5ted1zNEZWy4BHM/OkGc51wFRWZn6zCpV3A38dEZcCj0/Xt/JsdZ5915QOq9rfS2PE9SuZ+UxE7KERKgBPtPyO/zUz/2yG4zf3+43MvGe/xsY5hzLz6y3ta2c53gEy8+mI2AKsx4DSIvIalDpCRLweuDczP0tjmu7NM/XNxrWV70XEb1avjYh4S7X7H4Gzq+33Nh3/eOBHmTkG/DlwMnArsLZaCbcS+M2m0+wBfqXaXg+srLZfWR3nmYhYBxw/Q5lfB/5zNdojIo6LiFfP0G+oCiQi4peb2j9c1UVE/GK1IOKnNKYnD6q6VnZstb0COAO4e7bXSfNhQKlT/EfguxGxHXgT8MVZ+r8X+GBE3AHspBEiAB8FLoyI22iEyT5rge0R8R0aU4BXZOaDwB/QmF78nzQWN+wzBvy7iPgnGte49o2OvgQMRMREVcO0/6efmTfTuO7z7Yi4k8YquumC5Y9phN+OasHIvoUdnwd2AbdX7X9GY0ZlB/BsNJaPfwygGsVdDnwgIu6PiD4a17G2RMQO4A7gR8DodLVKC+Uyc6lNIuIDwEA2LdWWNDNHUJKkIjmCkiQVyRGUJKlIBpQkqUgGlCSpSAaUJKlIBpQkqUgGlCSpSP8foV7j9AYPFFoAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAASqUlEQVR4nO3df5DcdX3H8dfrLuddchIvbfSGqmWjg+glVas71ppRjwEZOop0Kk7VSEXppZfC6ZAARyGO7ThYDpx0NLacXKVQihkr2KI4A7E0W1qD6AUjhESjQyIBseAIjTnMae7e/WO/d95d7nI/du+7n8s9HzM7+93P97vf73tvsnnt5/P97HcdEQIAIDV1tS4AAIDJEFAAgCQRUACAJBFQAIAkEVAAgCQRUACAJE0bULZvtv207T2TrLvcdtheOT/lAQAWq5n0oG6RdO7ERtsvl/QOSY9XuSYAALRkug0i4n7bhUlW/Z2kKyXdNdODrVy5MgqFyXYFLC4DAwNqbm6udRlAEnbt2vWziHjxxPZpA2oytt8t6cmI+J7tGT+vUCiov79/LocETiqlUknt7e21LgNIgu0fT9Y+64CyvUzSNZLOmeH26yWtl6TW1laVSqXZHhI46Rw5coT3AjCNufSgXilplaSR3tPLJD1k+00R8dOJG0fETZJukqRisRh8agToQQEzMeuAiohHJL1k5LHtg5KKEfGzKtYFAFjkZjLNfJukBySdYfsJ2xfPf1kAgMVuJrP43j/N+kLVqgEAIMOVJAAASSKggBx1dXWpqalJZ555ppqamtTV1VXrkoBkzel7UABmr6urS729verp6VFbW5v27t2r7u5uSdLWrVtrXB2QHnpQQE76+vrU09OjjRs3qqmpSRs3blRPT4/6+vpqXRqQJAIKyMng4KA6OzvHtXV2dmpwcLBGFQFpI6CAnDQ2Nqq3t3dcW29vrxobG2tUEZA2zkEBOeno6Bg959TW1qYtW7aou7v7uF4VgDICCsjJyESIq6++WoODg2psbFRnZycTJIApMMQHAEgSPSggJ0wzB2aHHhSQE6aZA7NDQAE5YZo5MDsEFJATppkDs8M5KCAnHR0d2rRpkzZt2jSu/dJLL61RRUDa6EEBObnrrrskSdkvUY/ej7QDGI+AAnJy6NAhLV++fFzb8uXLdejQoRpVBKSNgAJydPjwYbW0tEiSWlpadPjw4doWBCSMc1BAjurq6nTnnXdqaGhI9fX1OvvsszU8PFzrsoAk0YMCcjQ8PKzNmzfrueee0+bNmwkn4AToQQE5am5u1s6dO7Vz587RxwMDAzWuCkgTPSggJyNhtGHDBn3ta1/Thg0bNDAwoObm5lqXBiSJgAJy0tfXp4aGBt14440677zzdOONN6qhoYFLHQFTIKCAHLW0tKhQKKiurk6FQmF0Rh+A4zkicjtYsViM/v7+3I4HpGTNmjV6/vnndeDAgdG2VatWadmyZdqzZ08NKwNqy/auiChObGeSBJCTRx99VFL5ChIRIdvjwgrAeAzxATkbGbXIc/QCWIgIKCBnq1ev1rZt27R69epalwIkjYACcrR06VJJ0rp168Y9BnA8zkEBOfrlL3+pffv2aXh4ePQewOToQQE5Gwklwgk4MQIKAJAkAgoAkCQCCgCQJAIKAJAkAgoAkCQCCgCQJAIKyFmhUNBtt92mQqFQ61KApPFFXSBnBw8e1IUXXljrMoDk0YMCACSJgAIAJImAAgAkiYACACRp2oCyfbPtp23vGdN2g+3v237Y9r/ZbpnXKgEAi85MelC3SDp3Qts3JK2JiNdK2i/pr6pcFwBgkZs2oCLifkk/n9C2PSKOZQ+/Jell81AbAGARq8b3oD4i6UtTrbS9XtJ6SWptbVWpVKrCIYGTC+8L4HiOiOk3sguS7o6INRPar5FUlPQnMYMdFYvF6O/vn2OpwMJme8p1M3kfAicr27siojixfc49KNsfkvQuSWfNJJwAAJiNOQWU7XMldUt6e0Q8X92SAACY2TTzbZIekHSG7SdsXyzpc5JOkfQN27tt985znQCARWbaHlREvH+S5i/MQy0AAIziShIAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJE0bULZvtv207T1j2n7L9jds/zC7XzG/ZQIAFpuZ9KBukXTuhLarJN0XEadLui97DABA1UwbUBFxv6SfT2g+X9Kt2fKtkv64umUBABa7JXN8XmtEPCVJEfGU7ZdMtaHt9ZLWS1Jra6tKpdIcDwmcvHhfAMdzREy/kV2QdHdErMkePxcRLWPWPxsR056HKhaL0d/fP/dqgQXM9pTrZvI+BE5WtndFRHFi+1xn8f2v7VOzHZ8q6elKigMAYKK5BtRXJX0oW/6QpLuqUw4AAGUzmWa+TdIDks6w/YTtiyVdJ+kdtn8o6R3ZYwAAqmbaSRIR8f4pVp1V5VoAABjFlSQAAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJWlLrAoCFzHZN9hMRVTkukDICCqjAbILiRCFE4ADHY4gPAJAkAgrIyVS9JHpPwOQIKCBHEaGI0Gndd48uA5gcAQUASBIBBQBIEgEFAEgSAQUASBIBBQBIEgEFAEgSAQUASBIBBQBIEgEFAEgSAQUASBIBBQBIEgEFAEhSRQFl+zLbj9reY3ub7aZqFQYAWNzmHFC2Xyrpo5KKEbFGUr2k91WrMADA4lbpEN8SSUttL5G0TNJPKi8JAIAKAioinpT0aUmPS3pK0v9FxPZqFQYAWNyWzPWJtldIOl/SKknPSfqy7Q9GxL9M2G69pPWS1NraqlKpNOdigflwyX0DGvh1/sctXPX13I/Z3CD9/VnNuR8XmIs5B5SksyUdiIhnJMn2VyS9RdK4gIqImyTdJEnFYjHa29srOCRQfQP3fF0Hr3tnrscslUqqxXuhcNXXa3JcYC4qOQf1uKQ3215m25LOkrSvOmUBABa7Ss5BPSjpDkkPSXok29dNVaoLALDIVTLEp4j4hKRPVKkWAABGcSUJAECSCCgAQJIIKABAkio6BwWcDE55zVX6vVuvyv/At+Z/yFNeI0n5TqkH5oqAwqL3i33XLarvQQELBUN8AIAkEVAAgCQRUACAJBFQAIAkMUkCUI0mD9yT/zFftLQh92MCc0VAYdHLewafVA7EWhwXWEgY4gMAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJIkfLARyZPs3yz3l+4ioUTVA2uhBATkZG04zaQcWOwIKAJAkhviAClSr9zPb/TAsiMWAgAIqMJugOFEIETjA8RjiAwAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkqaKAst1i+w7b37e9z/YfVqswAMDiVum1+D4j6Z6IuMD2CyQtq0JNAADMPaBsL5f0NkkXSVJE/ErSr6pTFgBgsaukB/UKSc9I+ifbr5O0S9LHImJg7Ea210taL0mtra0qlUoVHBI4OfG+AI7nuV7m33ZR0rckrY2IB21/RtLhiPj4VM8pFovR398/t0qBBY6f2wAmZ3tXRBQntlcySeIJSU9ExIPZ4zskvaGC/QEAMGrOARURP5V0yPYZWdNZkvZWpSoAwKJX6Sy+Lkm3ZzP4HpP04cpLAgCgwoCKiN2Sjhs3BACgUlxJAgCQJAIKyNmKFSvU19enFStW1LoUIGmVnoMCMEvPPvusOjo6al0GkDx6UACAJBFQAIAkEVAAgCQRUACAJBFQQM7q6urG3QOYHO8QIGfDw8Pj7gFMjoACACSJgAJyVF9ff8LHAH6DgAJy0tjYqOuvv14RoR07digidP3116uxsbHWpQFJ4koSQE46Ojp0+eWX68orr9TQ0JDq6+s1PDysSy65pNalAUkioIAcRYSGhoYkafQewOTm/JPvc8FPvmMxW7JkiSJCN9xwg9ra2rR3715dccUVsq1jx47VujygZubjJ98BzMLQ0JCuvfZabdy4UU1NTdq4caOuvfZaelLAFAgoAECSGOIDcjIyKaK+vn50ksTQ0JDq6uroRWFRY4gPqLHVq1dLKk+UGHs/0g5gPAIKyMn+/fu1du1aNTQ0SJIaGhq0du1a7d+/v8aVAWkioICcDA4Oavv27Tp69Kh27Niho0ePavv27RocHKx1aUCSCCggJ42Njert7R3X1tvby5UkgCnwRV0gJx0dHeru7pYktbW1acuWLeru7lZnZ2eNKwPSREABOdm6dask6eqrr9bg4KAaGxvV2dk52g5gPIb4AABJogcF5KSrq0u9vb3q6ekZvdTRyJAfvSjgePSggJz09fWpp6dn3KWOenp61NfXV+vSgCQRUEBOBgcHj5sQ0dnZyTRzYAoEFJATppkDs8M5KCAnTDMHZoeAAnLCNHNgdriaOVADpVJJ7e3ttS4DSAJXMwcALCgEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJuU4zt/2MpB/ndkAgXSsl/azWRQCJOC0iXjyxMdeAAlBmu3+y730A+A2G+AAASSKgAABJIqCA2rip1gUAqeMcFAAgSfSgAABJIqCwINj+qO19tp+1fdU02/6O7TtOsL5ge0+F9Ryp5PnzYczf6PY5PLdg+wNjHv+27R22j9j+3IRtS7Z/YHt3dntJNeoHJuL3oLBQ/KWkP4qIA9NtGBE/kXTB/JeUL9tWeVh+eIpNZvw3mkRB0gckfTF7fFTSxyWtyW4TrYsIfjsH84oeFJJnu1fSKyR91fZlI5/obd9i+7O2d9p+zPYFWftoD8n2atvfzj7pP2z79Gy39bb7bD9qe7vtpdn2r7R9j+1dtv/b9quz9lW2H7D9HdufHFPbqbbvz/a/x/Zbs/YP295v+7+y44yt+YIxzz+S3b/Q9n22H7L9iO3zx7yWfbb/QdJDkl5u+4qsjodt/80Uf6Nm2zdn2313zP7qbd8w5vl/kZVynaS3Zq/jsogYiIj/UTmogNqICG7ckr9JOqjy1RcukvS5rO0WSV9W+YNWm6QfZe0FSXuy5a0qf9qXpBdIWpqtPybp9Vn7v0r6YLZ8n6TTs+U/kPSf2fJXJf1ZtnyJpCPZ8iZJ12TL9ZJOkXSqpMclvTg75jcn1HzBmNc1sp8lkpZnyysl/UiSs1qHJb05W3eOyjMAnb3uuyW9bezfKFv+1JjX1CJpv6RmSeslbc7aGyX1S1olqV3S3ZP83Uf/3mPaSpIekbRb5V6Wa/3vg9vJeWOIDwvdv0d5yGuv7dZJ1j8g6RrbL5P0lYj4YXmkTAciYne2zS5JBdsvlPQWSV/OtpHK/4lL0lpJ78mWb5PUky1/R9LNthuyWnbbPktSKSKekSTbX5L0qmlehyV9yvbbVA6kl0oaeT0/johvZcvnZLfvZo9fKOl0SfdP2N85kt5t+/LscZOk383aXzumF/ei7Pm/mqa+sdZFxJO2T5F0p6QLJf3zLJ4PzAgBhYVucMyyJ66MiC/aflDSOyXda/vPJT024XlDKves6iQ9FxGvn+JYx30nIyLuz0LlnZJus32DpMOTbZs5lh1n5JzSC7L2dSr3uN4YEb+2fVDlUJGkgQmv8W8j4vNT7H/sdu+JiB+Maywfsysi7p3Q3j7N/kZFxJPZ/S9sf1HSm0RAYR5wDgonNduvkPRYRHxW5WG61061bUQclnTA9nuz59r267LV35T0vmx53Zj9nybp6Yjok/QFSW+Q9KCk9mwmXIOk9445zEFJb8yWz5fUkC2/KNvPr22fKem0Kcq8V9JHst6ebL90ill090rqygJJtn9/TPuGrC7ZfpXtZkm/UHl48oRsL7G9MltukPQuSRXNiASmQkDhZPenkvbY3i3p1Zr+k/46SRfb/p6kR1UOEUn6mKRLbH9H5TAZ0S5pt+3vqjwE+JmIeErSX6s8vPgfKk9uGNEn6e22v63yOa6R3tHtkoq2+7Mavj9ZcRGxXeWZdg/YfkTSHZo8WD6pcvg9nE0YGZnY8Y+S9kp6KGv/vMojKQ9LOmb7e7Yvk6SsF7dF0kW2n7DdpvKQ5722H1b5HNST2WsCqo4rSQDzzPZFkooRcWmtawEWEnpQAIAk0YMCACSJHhQAIEkEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJ/w9S1LX0p8E3TgAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAASQklEQVR4nO3df5BdZX3H8c8nP9xFAmILrhTUlQ7axFRR7kytQtglFW1xahxxqoK/mrJdW9aMa20oa4eqE3THTkaMU9KsoVgb0yoyIqQVaJKrjUF0l0TErEoEpGIQrL8I6pof3/5xT+LNZjdL7r2597vZ92vmzj373HPO872b2f3kec5zzzoiBABANrNaXQAAABMhoAAAKRFQAICUCCgAQEoEFAAgpTnN7OzUU0+Nzs7OZnYJpPTEE0/oxBNPbHUZQAojIyM/iojTxrc3NaA6Ozs1PDzczC6BlMrlsrq6ulpdBpCC7e9N1M4UHwAgJQIKAJASAQUASImAAgCkREABAFIioAAAKRFQAICUCCigidavX6+FCxdq8eLFWrhwodavX9/qkoC0mvpBXWAmW79+vQYGBrR27Vrt27dPs2fP1tKlSyVJb3zjG1tcHZAPIyigSVasWKG1a9equ7tbc+bMUXd3t9auXasVK1a0ujQgJQIKaJLR0VGdd955h7Sdd955Gh0dbVFFQG4EFNAk8+fP15YtWw5p27Jli+bPn9+iioDcCCigSQYGBrR06VJt3rxZe/fu1ebNm7V06VINDAy0ujQgJRZJAE1yYCFEX1+fRkdHNX/+fK1YsYIFEsAkGEEBTbR161bt3LlT+/fv186dO7V169ZWlwSkxQgKaJK+vj6tXr1ag4ODWrBggXbs2KHly5dLklatWtXi6oB8GEEBTTI0NKTBwUH19/ervb1d/f39Ghwc1NDQUKtLA1IioIAmGRsbU29v7yFtvb29Ghsba1FFQG5M8QFN0tbWpp6eHm3fvv3gIolzzjlHbW1trS4NSIkRFNAkF1xwgdatW6dFixbp5ptv1qJFi7Ru3TpdcMEFrS4NSIkRFNAkDz/8sJYsWaLrr79e1113ndra2rRkyRLdd999rS4NSImAAppkdHRU27Zt09y5c1Uul9XV1aU9e/aovb291aUBKU05xWf7etuP2r63qu23bN9h+77i+enHtkxg+uNWR8DReTLXoG6Q9KpxbVdK2hgRZ0vaWHwN4Ai41RFwdBwRU+9kd0q6NSIWFl9/W1JXROyyfbqkckQ8f6rzlEqlGB4errNkYPp65StfqTvuuEMRIdt6xSteodtuu63VZQEtZXskIkrj22u9BtUREbskqQipZxyh4x5JPZLU0dGhcrlcY5fA9Hbttddq48aN6u3t1YUXXqhNmzZpzZo1eu1rX6tly5a1ujwgnVpHUD+NiFOqXv9JREx5HYoRFGay9vZ2XXPNNerv7z+4SGLlypW66qqr9Ktf/arV5QEtM9kIqtbPQf2wmNpT8fxoPcUBM8HY2Jg2bNigWbNmqbu7W7NmzdKGDRu4kwQwiVoD6vOS3lpsv1XSzY0pBzh+2damTZvU29urW265Rb29vdq0aZNst7o0IKUpp/hsr5fUJelUST+UdLWkz0n6tKRnS3pI0usj4sdTdcYUH2ayA0E0e/Zs7du37+CzJD2ZqXbgeFXzIomImOyvqS2uuypgBjoQSgeeAUyMe/EBAFIioAAAKRFQAICUCCigyebNm3fIM4CJEVBAk/3iF7845BnAxAgooMn2799/yDOAiRFQAICUCCgAQEoEFAAgJQIKAJASAQUASImAAgCkREABAFIioAAAKRFQAICUCCgAQEoEFAAgJQIKAJASAQUASImAAgCkREABAFIioAAAKRFQAICUCCgAQEoEFAAgJQIKAJASAQUASImAAgCkREABAFIioAAAKRFQAICUCCgAQEoEFAAgJQIKAJASAQUASImAAgCkREABAFIioAAAKdUVULbfZfubtu+1vd52e6MKAwDMbDUHlO0zJL1TUikiFkqaLekNjSoMmA5sP+lHo84z1bmA48WcBhx/gu09kp4q6Qf1lwRMHxHxpPc9UrAczXmAmaLmEVREPCzpHyU9JGmXpJ9FxO2NKgwAMLPVPIKy/XRJr5H0XEk/lfQZ25dFxL+N269HUo8kdXR0qFwu11wsMJ1t3rxZ3d3dE7bzcwEcrp4pvj+S9EBEPCZJtm+S9DJJhwRURKyRtEaSSqVSdHV11dElML0dmMrrvHKDHvzQxS2uBsitnlV8D0l6qe2nujK5vljSaGPKAgDMdPVcg7pL0o2S7pb0jeJcaxpUFwBghqtrFV9EXC3p6gbVAgDAQdxJAgCQEgEFAEiJgAIApERAAQBSIqAAACkRUACAlAgoAEBKBBQAICUCCgCQUr1/DwqY9l70vtv1s1/uaXq/nVduaHqfTzthrr5+9UVN7xeoBQGFGe9nv9zT9DuLl8tlteLO/q0IRaBWTPEBAFIioAAAKRFQAICUCCgAQEosksCMd9L8K/X7n7iy+R1/ovldnjRfkvhT85geCCjMeI+PfohVfEBCTPEBAFIioAAAKRFQAICUuAYFqEXXZr7QmlsdAdMFAYUZr9kLJKRKILaiX2A6YYoPAJASAQUASImAAgCkREABAFIioAAAKRFQAICUCCgAQEoEFAAgJQIKAJASAQUASImAAgCkREABAFLiZrFAE9n+zfZg5TkiWlQNkBsjKKBJqsPpybQDMx0BBQBIiSk+oA6NGv0c7XmYFsRMQEABdTiaoDhSCBE4wOHqmuKzfYrtG21/y/ao7T9sVGEAgJmt3hHUtZK+EBGX2H6KpKc2oCYAAGoPKNsnS1ok6W2SFBG/lvTrxpQFAJjp6hlBnSXpMUn/YvtFkkYkLYuIJ6p3st0jqUeSOjo6VC6X6+gSOD7xcwEczrVenLVdkvQVSS+PiLtsXyvp5xHx95MdUyqVYnh4uLZKgWmORRLAxGyPRERpfHs9iyS+L+n7EXFX8fWNkl5Sx/kAADio5oCKiEck/a/t5xdNiyXtaEhVAIAZr95VfH2S1hUr+O6X9Pb6SwIAoM6Aiojtkg6bNwQAoF7ciw8AkBIBBQBIiYACAKREQAEAUiKgAAApEVAAgJQIKABASgQUACAlAgoAkBIBBQBIiYACAKREQAEAUiKgAAApEVAAgJQIKABASgQUACAlAgoAkBIBBQBIiYACAKREQAEAUiKgAAApEVAAgJQIKABASgQUACAlAgoAkBIBBQBIiYACAKREQAEAUiKgAAApEVAAgJQIKABASgQUACAlAgpogRUrVrS6BCA9AgpogYGBgVaXAKRHQAEAUiKgAAApEVAAgJQIKABASnUHlO3ZtrfZvrURBQEAIDVmBLVM0mgDzgMAwEF1BZTtMyVdLOnjjSkHAICKekdQH5H0t5L2118KAAC/MafWA22/WtKjETFiu+sI+/VI6pGkjo4OlcvlWrsEjlv8XACHc0TUdqD9QUlvlrRXUrukkyXdFBGXTXZMqVSK4eHhmvoDpjvbk75W688hcDywPRIRpfHtNU/xRcTfRcSZEdEp6Q2SNh0pnAAAOBp8DgoAkFJDAioiyhHx6kacC5gJnvnMZ7a6BCA9RlBACzzyyCOtLgFIj4ACAKREQAEAUiKggCa54oorjqodmOlq/qAugKOzatUqSdLQ0JDGxsbU1tamyy+//GA7gEPV/EHdWvBBXaCiXC6rq6ur1WUAKTT8g7oAABxLBBQAICUCCgCQEgEFAEiJgAIApERAAQBSIqAAACkRUACAlAgoAEBKBBQAICUCCgCQEgEFAEiJgAIApERAAQBSIqAAACkRUACAlAgoAEBKBBQAICUCCgCQEgEFAEiJgAIApERAAQBSIqAAACkRUACAlAgoAEBKBBQAICUCCgCQEgEFAEiJgAIApERAAQBSIqAAACkRUACAlAgoAEBKNQeU7WfZ3mx71PY3bS9rZGEAgJltTh3H7pX07oi42/ZJkkZs3xEROxpUGwBgBqt5BBURuyLi7mL7cUmjks5oVGEAgJmtnhHUQbY7Jb1Y0l0TvNYjqUeSOjo6VC6XG9ElMK3t3r2bnwVgCo6I+k5gz5P0RUkrIuKmI+1bKpVieHi4rv6A40G5XFZXV1erywBSsD0SEaXx7XWt4rM9V9JnJa2bKpwAADga9azis6S1kkYjYmXjSgIAoL4R1MslvVnShba3F48/aVBdAIAZruZFEhGxRZIbWAsAAAdxJwkAQEoEFAAgJQIKAJASAQUASImAAgCkREABAFIioAAAKRFQAICUCCgAQEoEFAAgJQIKAJASAQUASImAApqor69P7e3t6u7uVnt7u/r6+lpdEpBWQ/7kO4Cp9fX1afXq1RocHNSCBQu0Y8cOLV++XJK0atWqFlcH5MMICmiSoaEhDQ4Oqr+/X+3t7erv79fg4KCGhoZaXRqQEgEFNMnY2Jh6e3sPaevt7dXY2FiLKgJyI6CAJmlra9Pq1asPaVu9erXa2tpaVBGQG9eggCa5/PLLD15zWrBggVauXKnly5cfNqoCUEFAAU1yYCHEVVddpbGxMbW1tam3t5cFEsAkHBFN66xUKsXw8HDT+gOyKpfL6urqanUZQAq2RyKiNL6da1AAgJQIKABASgQUACAlAgoAkBIBBQBIiYACAKREQAEAUmrq56BsPybpe03rEMjrVEk/anURQBLPiYjTxjc2NaAAVNgenuiDiQB+gyk+AEBKBBQAICUCCmiNNa0uAMiOa1AAgJQYQQEAUiKgAAApEVCYFmy/0/ao7Z/YvnKKfX/H9o1HeL3T9r111rO7nuOPharv0boaju20/aZxX//S9vbisbrqtXNtf8P2Ttsfte1GvQegGn9RF9PFX0n644h4YKodI+IHki459iU1VxEEjoj9k+zypL9HE+iU9CZJn6pq+25EnDPBvtdJ6pH0FUn/KelVkv6rhj6BI2IEhfSK/72fJenztt9l+2NF+w3F/+C32r7f9iVF+8ERku0X2P5qMQq4x/bZxWln2x6y/U3bt9s+odj/d21/wfaI7f+x/XtF+3Nt32n7a7Y/UFXb6ba/VJz/XtvnF+1vt/0d218s+qmu+ZKq43cXz/Nsb7R9dzE6eU3Vexm1/U+S7pb0LNvvKeq4x/b7JvkenWj7+mK/bVXnm237w1XH/2VRyocknV+8j3cd4d/idEknR8SdUVlh9a+SltT0DwtMJSJ48Ej/kPSgKrcHepukjxVtN0j6jCr/0VogaWfR3inp3mJ7laRLi+2nSDqheH2vpHOK9k9LuqzY3ijp7GL7DyRtKrY/L+ktxfZfS9pdbL9b0kCxPVvSSZJOl/SQpNOKPr88ruZLqt7XgfPMUeUXv4r3uVOSi1r3S3pp8dpFqixRd/G+b5W0qPp7VGxfU/WeTpH0HUknqjLyeW/R3iZpWNJzJXVJurWqrk5JT0jaJumLks4v2kuS/rtqv/Orj+PBo5EPpvgw3X0uKlNeO2x3TPD6nZIGbJ8p6aaIuK+4ZPJARGwv9hmR1Gl7nqSXSfpM1WWVtuL55ZJeV2x/UtJgsf01SdfbnlvUst32YknliHhMkmz/h6TnTfE+LOka24tUCaQzJB14P9+LiK8U2xcVj23F1/MknS3pS+POd5GkP7X9N8XX7ZKeXbS/sGoU97Ti+F+PO36XpGdHxP/ZPlfS52y/oKhzPD6rgmOCgMJ0N1a1fdgvz4j4lO27JF0s6TbbfyHp/nHH7VNlZDVL0k9j4usu0gS/iCPiS0WoXCzpk7Y/LOnnE+1b2Fv0c+Ca0lOK9ktVGXGdGxF7bD+oSqhIlZFM9Xv8YET88yTnr97vdRHx7UMaK332RcRt49q7xr2vMRXfo4gYsf1dVUL2+5LOrNr1TEk/mKIWoCZcg8JxzfZZku6PiI+qMk33wsn2jYifS3rA9uuLY237RcXLX5b0hmL70qrzP0fSoxExJGmtpJdIuktSl+3fLkZWr6/q5kFJ5xbbr5E0t9h+WnGePba7JT1nkjJvk/TnxWhPts+w/YxJ9us7sMLO9our2t9R1CXbz7N9oqTHVZmePPC+TrM9u9g+S5VR1v0RsUvS47ZfWpz7LZJunqRWoC6MoHC8+zNJl9neI+kRSe+XdPIR9r9U0nW236tKePy7pK9LWibpU7aXSfps1f5dkt5TnH+3Ktepdtn+B1WmF3epsrhhdrH/kKSbbX9VletdB0ZH6yTdYntY0nZJ35qouIi43fZ8SXcW2bNb0mWSHh236wckfUTSPUWQPCjp1ZI+rsr1pbuL9sdUWeRwj6S9tr+uynWyhyS93/ZeVUaYvRHx4+Lc7yj2OUGV1Xus4MMxwa2OgGPM9tsklSLiilbXAkwnTPEBAFJiBAUASIkRFAAgJQIKAJASAQUASImAAgCkREABAFL6f30nbGCH9UCMAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAATmElEQVR4nO3df5DcdX3H8dcrubjLL5EkelChHFpELxStuU6tVr1oZfih0qk4lWLrz7tuNKtytbn0oEMzMpQdyrUa01lzlUKtMipjWwfKD0uz0rGIJphAElAZCDRWByT+SiRrknv3j/1euFxyud/f/dzd8zGzs9/97I/ve2/YvPh8v599ryNCAACkZkGzCwAA4GgIKABAkggoAECSCCgAQJIIKABAklry3NnSpUujra0tz10CSdq7d69OOOGEZpcBJGHz5s0/jogXjhzPNaDa2tq0adOmPHcJJKlWq6mzs7PZZQBJsP3E0cY5xAcASNKYAWX7RttP2d52lPs+bjtsL52Z8gAA89V4ZlA3Sbpg5KDtMyS9RdKT01wTAABjB1RE3Ctp91Hu+jtJqyXRKwkAMO0mtUjC9tsl/SAittoe67HdkrolqbW1VbVabTK7BOaUPXv28FkAxjDhgLJ9vKQrJZ0/nsdHxAZJGySpo6MjWLkEsIoPGI/JrOJ7qaSzJG21vVPS6ZIesH3qdBYGAJjfJhxQEfFQRLwoItoiok3SLkmvjogfTXt1wBxTLpdVLBa1YsUKFYtFlcvlZpcEJGvMQ3y2b5HUKWmp7V2Sro6Iz850YcBcUy6XVa1WValU1N7erh07dqi3t1eStG7duiZXB6THef5gYUdHR9BJAvNVsVjUtddeq56enkPnoPr7+9XX16d9+/Y1uzygaWxvjoiOkeN0kgByUq/XVSqVDhsrlUqq1+tNqghIGwEF5KRQKKharR42Vq1WVSgUmlQRkLZcm8UC81lXV9ehc07t7e3q7+9Xb2/vEbMqAA0EFJCToYUQfX19qtfrKhQKKpVKLJAARsEiCaAJ+KIu8BwWSQAAZhUCCgCQJAIKAJAkAgoAkCQCCgCQJAIKAJAkAgrIEd3MgfHji7pATuhmDkwMMyggJwMDA6pUKurp6VGxWFRPT48qlYoGBgaaXRqQJAIKyAndzIGJIaCAnNDNHJgYzkEBOaGbOTAxBBSQE7qZAxNDN3OgCehmDjyHbuYAgFmFgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAnJULpdVLBa1YsUKFYtFlcvlZpcEJKul2QUA80W5XFa1WlWlUlF7e7t27Nih3t5eSdK6deuaXB2QnjFnULZvtP2U7W3Dxq63/YjtB23/q+0XzGiVwBwwMDCgSqWinp4eFYtF9fT0qFKpaGBgoNmlAUkazyG+myRdMGLsa5LOjYjzJH1P0l9Oc13AnFOv11UqlQ4bK5VKqtfrTaoISNuYARUR90raPWLs7og4kN38pqTTZ6A2YE4pFAqqVquHjVWrVRUKhSZVBKRtOs5BvV/SF0e703a3pG5Jam1tVa1Wm4ZdArPPhRdeqNWrV+vRRx/Vm970Jn3oQx/Shg0b9La3vY3PBXAUjoixH2S3SbotIs4dMX6lpA5JfxjjeKGOjo7YtGnTJEsFZr9yuayBgQHV63UVCgV1dXWxQALznu3NEdExcnzSy8xtv0fSWyVdPp5wAtBYrbdv3z5t3LhR+/btI5yAY5jUIT7bF0jqlfTGiPjl9JYEAMD4lpnfIuk+SefY3mX7A5I+LekkSV+zvcV29ZgvAgDABI1nFd9lEXFaRCyKiNMj4rMR8RsRcUZEvCq7lMZ6HQB0kgAmgk4SQE7oJAFMDL34gJzQSQKYGAIKyAmdJICJIaCAnNBJApgYzkEBOenq6jp0zqm9vV39/f3q7e09YlYFoIGAAnIytBCir6/vUCeJUqnEAglgFONqdTRdaHUENNRqNXV2dja7DCAJ097qCACAmURAAQCSREABAJJEQAE5otURMH6s4gNyQqsjYGKYQQE5odURMDEEFJATWh0BE0NAATmh1REwMZyDAnJCqyNgYggoICe0OgImhlZHQBPQ6gh4Dq2OAACzCof4gBwtWbJEu3fvPnR78eLFeuaZZ5pYEZAuZlBATobCadmyZbrlllu0bNky7d69W0uWLGl2aUCSCCggJ0PhtG3bNp166qnatm3boZACcCQCCsjR8uXLD+vFt3z58maXBCSLVXxATmxLkhYuXKiDBw8eupakPD+HQGpYxQckYiiUhq4BHB0BBQBIEgEF5Oiiiy5SRGjjxo2KCF100UXNLglIFgEF5OiOO+5Qf3+/9u3bp/7+ft1xxx3NLglIFl/UBXIytChi9erVhxZJRIQWLlzY7NKAJDGDAnKycuXKQyv5htjWypUrm1QRkDZmUEBOhrqWDwwM6ODBg2ppaVFXVxfdzIFRMIMCACSJGRSQk3K5rPXr12vBgsb/Fx44cEDr16+XJGZRwFHQSQLIycKFCzU4OHhEJ4kFCxbwpV3Ma3SSAJpscHBwQuPAfEdAAQCSREABOaMXHzA+BBQAIEkEFJCzoS/rjvzSLoDDEVBAzoaWmQ9dAzg6PiEAgCSNGVC2b7T9lO1tw8YW2/6a7e9n16fMbJnA3MEiCWB8xjODuknSBSPG1ki6JyLOlnRPdhvAMSxevHhC48B8N2ZARcS9knaPGL5E0s3Z9s2S/mB6ywLmnt27Gx+joZ/XGLoeGgdwuMmeg2qNiB9KUnb9oukrCZi71q5dqwMHDmjjxo06cOCA1q5d2+ySgGTNeLNY292SuiWptbVVtVptpncJJGv9+vW65pprtH//fi1atEinnNI4fcvnAjjSuJrF2m6TdFtEnJvd/q6kzoj4oe3TJNUi4pyxXodmsZjPhr73tGzZMl111VW65pprtH37dklSnk2bgdSM1ix2sjOor0p6j6Trsut/n0JtwLywYMECDQ4Oavv27brssssOGwdwpPEsM79F0n2SzrG9y/YH1Aimt9j+vqS3ZLcBHMPg4KC6u7tVKBQkSYVCQd3d3XQzB0bB70EBU9CsdkUcEsRcwu9BATMgIsZ9WbVqlVpaWnTDDTfojCtu1Q033KCWlhatWrVqQq9DOGG+4CffgZwM/ax7X1+f6vW6+goFlUolfu4dGAWH+IAmaFtzu3Zed3GzywCSwCE+AMCsQkABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAktQylSfbvkLSByWFpIckvS8i9k1HYUBeXrn2bv3s2f2577dtze257/Pk4xZp69Xn575fYDImHVC2XyzpI5LaI+JZ21+S9C5JN01TbUAufvbsfu287uJc91mr1dTZ2ZnrPqXmhCIwWVM9xNci6TjbLZKOl/R/Uy8JAIApzKAi4ge2/1bSk5KelXR3RNw98nG2uyV1S1Jra6tqtdpkdwnMmLz/u9yzZ0/TPgt8BjFbTOUQ3ymSLpF0lqSfSvqy7XdHxL8Mf1xEbJC0QZI6OjqiGYc1gGO68/bcD7c16xBfM94rMFlTOcT3+5Iej4inI2K/pK9Ieu30lAUAmO+mElBPSnqN7eNtW9KbJT08PWUBAOa7SQdURNwv6VZJD6ixxHyBskN5AABM1ZS+BxURV0u6eppqAQDgEDpJAACSREABAJJEQAEAkkRAAQCSNKVFEsBccNIr1ug3b16T/45vzn+XJ71CkvLtOwhMFgGFee8XD19Hs1ggQRziAwAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJn9sA1KSfobgz/32efNyi3PcJTBYBhXkv79+CkhqB2Iz9ArMJh/gAAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJIqAAAEkioAAASSKgAABJIqAAAEmaUkDZfoHtW20/Yvth2787XYUBAOa3lik+/5OS7oyIS20/T9Lx01ATAACTDyjbz5f0BknvlaSI+JWkX01PWQCA+W4qM6iXSHpa0j/ZfqWkzZI+GhF7hz/IdrekbklqbW1VrVabwi6BuYPPAnBsjojJPdHukPRNSa+LiPttf1LSzyPir0Z7TkdHR2zatGlylQJzSNua27XzuoubXQaQBNubI6Jj5PhUFknskrQrIu7Pbt8q6dVTeD0AAA6ZdEBFxI8k/a/tc7KhN0vaMS1VAQDmvamu4itL+ny2gu8xSe+bekkAAEwxoCJii6QjjhsCADBVdJIAACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAJyVC6XVSwW9UTlrSoWiyqXy80uCUjWVJvFAhincrmsarWqSqWiv991pj52+hPq7e2VJK1bt67J1QHpmfQPFk4GP1iIucZ2U/ab5+cWmGkz8YOFwLwXEeO+SNLevXsVEdq4caMiQnv37p3w6xBOmC8IKCAnhUJB1Wr1sLFqtapCodCkioC0cQ4KyElXV9ehc07t7e3q7+9Xb2+vSqVSkysD0kRAATkZWgjR19ener2uQqGgUqnEAglgFCySAJqgVqups7Oz2WUASWCRBABgViGgAABJIqAAAEkioAAASSKggBwN9eJbsWIFvfiAMbDMHMjJ8F587e3t2rFjB734gGNgBgXkZGBgQJVKRT09PSoWi+rp6VGlUtHAwECzSwOSREABOanX60d0jSiVSqrX602qCEgbAQXkhF58wMRwDgrICb34gIkhoICc0IsPmBh68QFNQC8+4Dn04gMAzCoEFAAgSQQUACBJBBQAIEkEFAAgSQQUACBJBBQAIEm5fg/K9tOSnshth0C6lkr6cbOLABJxZkS8cORgrgEFoMH2pqN9MRHAczjEBwBIEgEFAEgSAQU0x4ZmFwCkjnNQAIAkMYMCACSJgAIAJImAwqxj+yO2H7b9E9trxnjsr9m+9Rj3t9neNsV69kzl+TNh2N/o85N4bpvtPx4xdp7t+2xvt/2Q7eL0VQscHeegMOvYfkTShRHx+DS8Vpuk2yLi3Cm8xp6IOHGqtUxwn1bj8zs4yv2T/hvZ7pT08Yh4a3a7RdIDkv4kIrbaXiLppxFxcLL1A+PBDAqziu2qpJdI+qrtK2x/Ohu/yfanbP+P7cdsX5qNH5oh2V5m+1u2t9h+0PbZ2csutD2QzQ7utn1c9viX2r7T9mbb/2375dn4Wdls4tu2PzGsttNs35u9/jbbr8/G32f7e7a/nu1neM2XDnv+nuz6RNv32H4gm61cMuy9PGz7H9QIjDNs/0VWx4O2147yNzrB9o3Z474z7PUW2r5+2PP/LCvlOkmvz97HFZLOl/RgRGyVpIh4hnBCLiKCC5dZdZG0U41WQe+V9Ols7CZJX1bjf7raJT2ajbdJ2pZtr5N0ebb9PEnHZfcfkPSqbPxLkt6dbd8j6exs+3ck/Ve2/VVJf5ptf1jSnmz7zyVdmW0vlHSSpNMkPSnphdk+vzGi5kuHva+h12mR9Pxse6mkRyU5q3VQ0muy+85XY7m6s/d9m6Q3DP8bZdvXDntPL5D0PUknSOqWdFU2XpC0SdJZkjrVmFUO1fUxSZ+TdJcawbi62f8NcJkfl5ZRcguYjf4tGoe8dthuPcr990m60vbpkr4SEd9vHCnT4xGxJXvMZklttk+U9FpJX84eIzX+EZek10l6R7b9OUmVbPvbkm60vSirZYvtN0uqRcTTkmT7i5JeNsb7sKRrbb9BjUB6saSh9/NERHwz2z4/u3wnu32ipLMl3Tvi9c6X9HbbH89uFyX9ejZ+3rBZ3MnZ83814vktkn5P0m9L+qWke2xvjoh7xngfwJQQUJhL6sO2PfLOiPiC7fslXSzpLtsflPTYiOcdVGNmtUCN8yyvGmVfR5y8jYh7s1C5WNLnbF8v6edHe2zmQLafoXNKz8vGL1djxrU8Ivbb3qlGqEjS3hHv8W8i4jOjvP7wx70jIr572GBjn+WIuGvEeOeI5++S9PWI+HF2/39IerUaM0xgxnAOCvOG7ZdIeiwiPqXGYbrzRntsRPxc0uO235k917Zfmd39DUnvyrYvH/b6Z0p6KiIGJH1WjX/E75fUaXtJNrN657Dd7JS0PNu+RNKibPvk7HX2214h6cxRyrxL0vuz2Z5sv9j2i0Z5XDkLJNn+rWHjK7O6ZPtltk+Q9As1Dk8Of/55to/PFky8UdKOUWoCpg0BhfnkjyRts71F0ssl/fMYj79c0gdsb5W0XY0QkaSPSvqw7W+rESZDOiVtsf0dNQ4BfjIifijpr9U4vPifapzDGTIg6Y22v6XGOa6h2dHnJXXY3pTV8MjRiouIuyV9QdJ9th+SdKsOD5Yhn1Aj/B7MFowMLez4RzWC5oFs/DNqHFV5UNIB21ttXxERP5HUr8YhzC2SHoiI20f7owHThWXmQI5sv1dSR0SsanYtQOqYQQEAksQMCgCQJGZQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCT9P7QHhYp2baS8AAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAATSUlEQVR4nO3dfYxc1XnH8e+DbRI30KDUaKHGeNPGUnGpAnREoPSP2bzw4iBhqVQCkUAg0QaCoZGCWgQSFqpQU6lKBQJhnIAKDSKKQmJZ4Bgo2mmgFQTbssHGobJSEyxbpCGNyWI3wfD0jx3I7Hh3Zrwe7z3e/X6kkefee+beZxGj35xzz5yJzESSpNIcU3UBkiRNxICSJBXJgJIkFcmAkiQVyYCSJBVpblUXXrBgQQ4ODlZ1eakIb731Fh/60IeqLkOq1MaNG3+RmSe2768soAYHB9mwYUNVl5eK0Gg0qNfrVZchVSoiXp1ov0N8kqQiGVCSpCIZUJKkIhlQkqQidQ2oiPhgRPw4IrZExLaIuH2CNvWI2BsRm5uP245MuZKk2aKXWXy/AT6ZmaMRMQ94NiJ+mJnPtbV7JjMv7n+JkqTZqGtA5dhy56PNzXnNh0ugS5KOqJ6+BxURc4CNwMeAezLz+QmanRsRW4DdwE2ZuW2C8wwDwwADAwM0Go2p1i0d1Z5++mm+/e1v87Of/YxTTz2Vz33uc3zqU5+quiypKD0FVGa+A5wREScAP4iI0zNza0uTTcDi5jDgMmANsGSC86wGVgPUarX0C4qajR555BEefvhhHnjgAd555x3mzJnDF7/4RZYuXcrll19edXlSMeJQf7AwIlYCb2XmP3VosxOoZeYvJmtTq9XSlSQ0G51++uksX76cNWvWsH37dk477bT3t7du3dr9BNIMExEbM7PWvr9rDyoiTgTezsxfRcR84NPAP7a1OQl4PTMzIs5mbHbgG/0pXZpZXn75Zfbt28f9998/rge1c+fOqkuTitLL96BOBkYi4kXgBeCpzHwsIq6NiGubbS4FtjbvQd0FXJb+lrw0oWOPPZYVK1YwNDTE3LlzGRoaYsWKFRx77LFVlyYV5ZCH+PrFIT7NVscccwyLFy8edw/qmmuu4dVXX+Xdd9+tujxp2k15iE9Sfy1dupTly5dzww03vH8P6oorrmDNmjVVlyYVxYCSptmtt97KrbfeetA9qDvuuKPq0qSiGFDSNHtvKnlrD+qOO+5wirnUxntQUoX8wUJp8ntQrmYuSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSqSASVJKtLcbg0i4oPAj4APNNt/LzNXtrUJ4E5gGbAP+EJmbup/udLMMPaWGS8zK6hEKlcvPajfAJ/MzI8DZwAXRsQ5bW0uApY0H8PAvf0sUppJWsNp5cqVE+6X1ENA5ZjR5ua85qP9o94lwEPNts8BJ0TEyf0tVZpZMpN6vW7PSZpE1yE+gIiYA2wEPgbck5nPtzVZCLzWsr2ruW9P23mGGethMTAwQKPRmFrV0lFu5cqVNBoNRkdHaTQarFy5kttvv933hNQiDuXTW0ScAPwAuCEzt7bsfxz4h8x8trn9NPC3mblxsnPVarXcsGHDVOuWjlqdhvLsTWk2ioiNmVlr339Is/gy81dAA7iw7dAuYFHL9inA7kMrUZp9vvzlL1ddglSsrgEVESc2e05ExHzg08BP2pqtBa6MMecAezNzD5I6uu+++6ouQSpWL/egTgYebN6HOgb4bmY+FhHXAmTmKmAdY1PMdzA2zfzqI1SvJGmW6BpQmfkicOYE+1e1PE/g+v6WJs18X/rSl/jWt75VdRlSkVxJQqrIwMAA5513HgMDA1WXIhWpp2nmkvrv9ddf5+qrHQ2XJmMPSpJUJANKqtD5559fdQlSsQwoqUJPPvlk1SVIxTKgpAqsX7+ezGRkZITMZP369VWXJBXHSRJSBS68sH0xFknt7EFJFRocHKy6BKlYBpRUoZ07d1ZdglQsA0qqwAUXXDDuHtQFF1xQdUlScbwHJVXgiSee8Bd0pS7sQUkVmjvXz4jSZAwoqUIHDhyougSpWAaUVIGrrrpq3D2oq666quqSpOIYUFIFHnzwwY7bkpwkIVXGSRJSZ/agJElFMqCkirTeg5J0MANKqkD7EkcueSQdzICSKtC+xJFLHkkHc5KEVBEnSUid2YOSJBXJgJIq4iQJqTMDSqrA/PnzO25L6iGgImJRRIxExPaI2BYRfzNBm3pE7I2Izc3HbUemXGlm2L9/f8dtSb1NkjgAfC0zN0XE8cDGiHgqM19ua/dMZl7c/xKlmclJElJnXXtQmbknMzc1n/8a2A4sPNKFSZJmt0OaZh4Rg8CZwPMTHD43IrYAu4GbMnPbBK8fBoYBBgYGaDQah1qvNGOMjIwwOjrKcccdx9DQEIDvCalF9DqDKCKOA/4duCMzv9927PeBdzNzNCKWAXdm5pJO56vVarlhw4Ypli0dvd4b2stMGo0G9Xp93D5ptomIjZlZa9/f0yy+iJgHPAo83B5OAJn5ZmaONp+vA+ZFxILDrFmSNIt1HeKLsY929wPbM/Mbk7Q5CXg9MzMizmYs+N7oa6XSDOMkCamzXu5BnQd8HngpIjY3990CnAqQmauAS4HrIuIAsB+4LB2rkCaUmROGk28ZabyuAZWZzwIdP+pl5t3A3f0qSprJJus5RYQhJbVwJQmpIi51JHVmQEmSimRASZKK5O9BSRVxFp/UmT0oaZpNds/Je1HSeAaUNM06zeKT9DsGlFQRZ/FJnRlQkqQiGVCSpCI5i0+qiPecpM7sQUnTzFl8Um8MKElSkQwoaZo5zVzqjQElVcRp5lJnBpQkqUgGlCSpSE4zlyriPSepM3tQ0jRzmrnUGwNKklQkA0qaZk4zl3pjQEkVcZq51JkBJUkqkgElSSpS12nmEbEIeAg4CXgXWJ2Zd7a1CeBOYBmwD/hCZm7qf7nSzOE9J6mzXr4HdQD4WmZuiojjgY0R8VRmvtzS5iJgSfPxCeDe5r+S2mTmhOHkvShpvK5DfJm5573eUGb+GtgOLGxrdgnwUI55DjghIk7ue7XSDJGZ4yZJGE7SwQ7pHlREDAJnAs+3HVoIvNayvYuDQ0ySpJ71vNRRRBwHPAp8NTPfbD88wUsO+kgYEcPAMMDAwACNRqP3SqUZZGho6KB9IyMjFVQilSt6GVqIiHnAY8ATmfmNCY7fBzQy85Hm9itAPTP3THbOWq2WGzZsmHLh0tGq0+QIh/o0G0XExsyste/vOsTXnKF3P7B9onBqWgtcGWPOAfZ2CidJflFX6qaXIb7zgM8DL0XE5ua+W4BTATJzFbCOsSnmOxibZn513yuVJM0qXQMqM59l4ntMrW0SuL5fRUmS5O9BSRXxi7pSZy51JE0zfw9K6o0BJVXAL+pK3RlQkqQiGVCSpCI5SUKqgIvFSt3Zg5KmmT/5LvXGgJIq4koSUmcGlCSpSAaUJKlITpKQKuI9J6kze1DSNHMlCak3BpRUAVeSkLozoCRJRTKgJElFMqAkSUUyoCRJRTKgJElFMqAkSUUyoCRJRXIlCekwVbUihN+d0kxnD0o6TO990XYqj8V/99iUXyvNdAaUJKlIBpQkqUhdAyoiHoiIn0fE1kmO1yNib0Rsbj5u63+ZkqTZppdJEv8C3A081KHNM5l5cV8qkiSJHnpQmfkj4JfTUIskSe/r1zTzcyNiC7AbuCkzt03UKCKGgWGAgYEBGo1Gny4vHb18H0gT60dAbQIWZ+ZoRCwD1gBLJmqYmauB1QC1Wi3r9XofLi8dxdY/ju8DaWKHPYsvM9/MzNHm83XAvIhYcNiVSZJmtcMOqIg4KZpfpY+Is5vnfONwzytJmt26DvFFxCNAHVgQEbuAlcA8gMxcBVwKXBcRB4D9wGXp19wlSYepa0Bl5uVdjt/N2DR0SZL6xpUkJElFMqAkSUUyoCRJRTKgJElFMqAkSUUyoCRJRTKgJElFMqAkSUUyoCRJRTKgJElFMqAkSUUyoCRJRTKgJElFMqAkSUUyoCRJRTKgJElFMqAkSUUyoCRJRTKgJElFMqAkSUUyoCRJRZpbdQFSKT5++5Ps3f/2tF938ObHp/V6H54/jy0rz5/Wa0pTYUBJTXv3v83Or392Wq/ZaDSo1+vTes3pDkRpqhzikyQVqWtARcQDEfHziNg6yfGIiLsiYkdEvBgRZ/W/TEnSbNNLD+pfgAs7HL8IWNJ8DAP3Hn5ZkqTZrmtAZeaPgF92aHIJ8FCOeQ44ISJO7leBkqTZqR+TJBYCr7Vs72ru29PeMCKGGetlMTAwQKPR6MPlpf6Z7v8nR0dHK3kf+N7T0aAfARUT7MuJGmbmamA1QK1Wy+mevSR1tP7xaZ9RV8Usvir+Tmkq+jGLbxewqGX7FGB3H84rSZrF+hFQa4Erm7P5zgH2ZuZBw3uSJB2KrkN8EfEIUAcWRMQuYCUwDyAzVwHrgGXADmAfcPWRKlaSNHt0DajMvLzL8QSu71tFkiThShKSpEIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIvXjJ9+lGeH4027mzx68efov/OD0Xu740wA+O70XlabAgJKaXrrqpWm/ZqPRoF6vT/t1paOBQ3ySpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIvUUUBFxYUS8EhE7IuKgr9pHRD0i9kbE5ubjtv6XKkmaTbquJBERc4B7gM8Au4AXImJtZr7c1vSZzLz4CNQoSZqFeulBnQ3syMyfZuZvge8AlxzZsiRJs10va/EtBF5r2d4FfGKCdudGxBZgN3BTZm5rbxARw8AwwMDAAI1G45ALlmaS0dFR3wfSJHoJqJhgX7ZtbwIWZ+ZoRCwD1gBLDnpR5mpgNUCtVksXydRs52Kx0uR6GeLbBSxq2T6FsV7S+zLzzcwcbT5fB8yLiAV9q1KSNOv0ElAvAEsi4qMRcSxwGbC2tUFEnBQR0Xx+dvO8b/S7WEnS7NF1iC8zD0TECuAJYA7wQGZui4hrm8dXAZcC10XEAWA/cFlmtg8DSpLUs55+sLA5bLeubd+qlud3A3f3tzRJ0mzmShKSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIvUUUBFxYUS8EhE7IuLmCY5HRNzVPP5iRJzV/1KlmSMiiAiGhobefy5pvK4BFRFzgHuAi4ClwOURsbSt2UXAkuZjGLi3z3VKM8ZkYWRISeP10oM6G9iRmT/NzN8C3wEuaWtzCfBQjnkOOCEiTu5zrdKMkpmMjIyQmVWXIhVpbg9tFgKvtWzvAj7RQ5uFwJ7WRhExzFgPi4GBARqNxiGWK80cjUaD0dHRce8D3xPS7/QSUBONO7R/5OulDZm5GlgNUKvVsl6v93B5aWaq1+s0Gg1a3we+J6Tf6SWgdgGLWrZPAXZPoY2kFt5zkjrr5R7UC8CSiPhoRBwLXAasbWuzFriyOZvvHGBvZu5pP5EkJr3n5L0oabyuAZWZB4AVwBPAduC7mbktIq6NiGubzdYBPwV2AN8EvnKE6pVmhMwcN0nCcJIO1ssQH5m5jrEQat23quV5Atf3tzRJ0mzmShKSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiRVXfv4iI/wFereTiUjkWAL+ougipYosz88T2nZUFlCSIiA2ZWau6DqlEDvFJkopkQEmSimRASdVaXXUBUqm8ByVJKpI9KElSkQwoSVKRDChpAhFxY0Rsj4iHO7QZbf47GBFbm8+/EBF3H+K1bml5/v65DldEfDUifq8f55KqYEBJE/sKsCwzr5iGa93SvcmUfBUwoHTUMqCkNhGxCvgjYG1E7I2Im1qObY2IwS6nWBQR6yPilYhY2fLaNRGxMSK2RcRwc9/XgfkRsbmltzYnIr7ZbPdkRMxvtv1YRPxbRGyJiE0R8ccRUY+IRkR8LyJ+EhEPx5gbgT8ERiJipH//daTpY0BJbTLzWmA3MAT88xROcTZwBXAG8NcR8d5KEddk5p8DNeDGiPiDzLwZ2J+ZZ7T01pYA92TmnwK/Av6quf/h5v6PA38B7GnuP5Ox3tJSxoL1vMy8672/ITOHpvA3SJUzoKT+eyoz38jM/cD3gb9s7r8xIrYAzwGLGAuiifx3Zm5uPt8IDEbE8cDCzPwBQGb+X2bua7b5cWbuysx3gc3AYL//IKkKc6suQCrcAcZ/kPtgD69p/3JhRkQd+DRwbmbui4hGh3P9puX5O8B8IDpcr72972vNCPagpM52AmcBRMRZwEd7eM1nIuIjzXtHy4H/AD4M/G8znP4EOKel/dsRMa/TCTPzTWBXRCxv1vKBHmbo/Ro4vod6pSIZUFJnjwIfiYjNwHXAf/XwmmeBf2VsuO3RzNwArAfmRsSLwN8zNsz3ntXAi52mtDd9nrFhwheB/wRO6tJ+NfBDJ0noaOVSR5KkItmDkiQVyYCSJBXJgJIkFcmAkiQVyYCSJBXJgJIkFcmAkiQV6f8BjZwX2KHTkv4AAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n",
      "\n",
      "\n",
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAQtElEQVR4nO3df2xdZ33H8feHJjhRqeimBq8dEwY6OqqOpuBtaFK3NOtQ1DIQ3dYfYoOKiihMRNM0tASQgA4xBbrAH7Sj6qSqILEs2yDTlJSKbspVYeovu0pCqhWKtjCFaq061oIZmFK++8PHnWtuasdOrh/b75d0pXuf55x7nis5fffce3ydqkKSpNa8aKkXIElSPwZKktQkAyVJapKBkiQ1yUBJkpq0ZqkXcDLOOeecGhkZWeplSKfd97//fc4888ylXoY0EOPj409W1YbZ48sqUCMjI4yNjS31MqTTrtfrsWnTpqVehjQQSb7Vb9y3+CRJTTJQkqQmGShJUpMMlCSpSQZKktQkAyVJapKBkiQ1aVn9HpS00iX5qTH/JI5WK8+gpEb0i9MLjUsrnYGSJDXJQEmSmmSgJElNMlCSpCYZKElSkwyUJKlJBkqS1CQDJUlqkoGSJDXJQEmSmmSgJElNMlCSpCYZKElSkwyUJKlJBkqS1CQDJUlqkoGSJDXJQEmSmmSgJElNMlCSpCYZKElSkwyUJKlJBkqS1KQ1S70AaSVLMvDnqapTckxpqRko6TQ6mVi8UISMjlYj3+KTJDXJQEmNONFZkmdPWq3mDFSS25M8keTojLG9SQ51t2NJDvXZb12SB5IcTvJwkhtnzW9P8vVu7hOn5NVIy1xVUVW8Ysf+5+5Lq9V8PoO6A7gZ+Nz0QFVdM30/yW7g6T77TQKbq2oiyVrgq0m+VFX3JbkMeCvwuqqaTPKyxbwISdLKM2egquqeJCP95jL1qe7VwOY++xUw0T1c292m/3fwPcCuqprstn3ipFcuSVrRFnsV36XA41X1aL/JJGcA48D5wC1VdX839Rrg0iQfA34IvK+qHjzBc2wFtgIMDw/T6/UWuWRpefBnXavdYgN1HbDnRJNV9SywMcnZwL4kF1XV0e64PwO8EfgV4O+SvKr6vOFeVbcBtwGMjo7Wpk2bFrlkaRm46wD+rGu1W/BVfEnWAFcBe+fatqqeAnrAlm7oOPDFmvIA8BPgnIWuRZK08izmMvPLgUeq6ni/ySQbujMnkqyf3r6b/ke6z62SvAZ4MfDkItYiSVph5nOZ+R7gXuCCJMeT3NBNXcust/eSnJfkzu7hucDBJEeAB4G7q2p/N3c78Kru0vW/Bd7Z7+09SdLqNZ+r+K47wfj1fcYeA67o7h8BLjnBvj8C/uBkFipJWl38JglJUpMMlCSpSQZKktQkAyVJapKBkiQ1yUBJkppkoCRJTTJQkqQmGShJUpMMlCSpSQZKktQkAyVJapKBkiQ1yUBJkppkoCRJTTJQkqQmGShJUpMMlCSpSQZKktQkAyVJapKBkiQ1yUBJkppkoCRJTTJQkqQmGShJUpMMlCSpSQZKktQkAyVJapKBkiQ1yUBJkppkoCRJTTJQkqQmGShJUpMMlCSpSQZKktQkAyVJapKBkiQ1yUBJkppkoCRJTTJQkqQmGShJUpMMlCSpSQZKktSkOQOV5PYkTyQ5OmNsb5JD3e1YkkN99luX5IEkh5M8nOTGGXMfSfLtGc9xxSl7RZKkFWHNPLa5A7gZ+Nz0QFVdM30/yW7g6T77TQKbq2oiyVrgq0m+VFX3dfOfqqq/XPDKJUkr2pyBqqp7koz0m0sS4Gpgc5/9CpjoHq7tbrXglUqSVpX5nEG9kEuBx6vq0X6TSc4AxoHzgVuq6v4Z0+9N8g5gDPjTqvqfEzzHVmArwPDwML1eb5FLlpYHf9a12mXqRGeOjabOoPZX1UWzxj8DfLOqds+x/9nAPmB7VR1NMgw8ydQZ1UeBc6vqXXOtY3R0tMbGxuZcr7Tcjew8wLFdVy71MqSBSDJeVaOzxxd8FV+SNcBVwN65tq2qp4AesKV7/HhVPVtVPwH+GvjVha5DkrQyLeYy88uBR6rqeL/JJBu6MyeSrJ/evnt87oxN3wYc/aknkCStavO5zHwPcC9wQZLjSW7opq4F9sza9rwkd3YPzwUOJjkCPAjcXVX7u7lPJPlaN3cZ8Cen4LVIklaQ+VzFd90Jxq/vM/YYcEV3/whwyQn2/cOTWqUkadXxmyQkSU0yUJKkJhkoSVKTDJQkqUkGSpLUJAMlSWqSgZIkNclASZKaZKAkSU0yUJKkJhkoSVKTFvsHC6VV4eIbv8zTP3hmoMcc2XlgYMd66fq1HP7wmwZ2PGk+DJQ0D0//4JmB/gHBXq/Hpk2bBna8QcZQmi/f4pMkNclASZKaZKAkSU0yUJKkJhkoSVKTDJQkqUkGSpLUJAMlSWqSgZIkNclASZKaZKAkSU0yUJKkJhkoSVKTDJQkqUkGSpLUJAMlSWqSgZIkNclASZKaZKAkSU0yUJKkJhkoSVKTDJQkqUlrlnoB0nJw1mt38suf3TnYg352cIc667UAVw7ugNI8GChpHr73b7s4tmtw/wHv9Xps2rRpYMcb2XlgYMeS5su3+CRJTTJQkqQmGShJUpMMlCSpSQZKktSkOQOV5PYkTyQ5OmNsb5JD3e1YkkN99luX5IEkh5M8nOTGPtu8L0klOWfRr0SStKLM5zLzO4Cbgc9ND1TVNdP3k+wGnu6z3ySwuaomkqwFvprkS1V1X7ffLwC/DfznwpcvSVqp5jyDqqp7gO/0m0sS4GpgT5/9qqomuodru1vN2ORTwJ/NGpMkCVj8L+peCjxeVY/2m0xyBjAOnA/cUlX3d+NvAb5dVYenGndiSbYCWwGGh4fp9XqLXLK0MIP82ZuYmBj4z7r/ttSaxQbqOvqcPU2rqmeBjUnOBvYluQj4d+CDwJvmc4Cqug24DWB0dLQG+dv10nPuOjDQb3YY9DdJDPr1SfOx4Kv4kqwBrgL2zrVtVT0F9IAtwKuBVwKHkxwDXg48lOTnFroWSdLKs5jLzC8HHqmq4/0mk2zozpxIsn7G9l+rqpdV1UhVjQDHgddX1X8tYi2SpBVmPpeZ7wHuBS5IcjzJDd3Utcx6ey/JeUnu7B6eCxxMcgR4ELi7qvafuqVLklayOT+DqqrrTjB+fZ+xx4AruvtHgEvm8fwjc20jSVp9/CYJSVKTDJQkqUkGSpLUJAMlSWqSgZIkNclASZKaZKAkSU0yUJKkJhkoSVKTDJQkqUkGSpLUpMX+PShp1RjZeWCwB7xrcMd76fq1AzuWNF8GSpqHY7uuHOjxRnYeGPgxpdb4Fp8kqUkGSpLUJAMlSWqSgZIkNclASZKaZKAkSU0yUJKkJhkoSVKTDJQkqUkGSpLUJAMlSWqSgZIkNclASZKaZKAkSU0yUJKkJhkoSVKTDJQkqUkGSpLUJAMlSWqSgZIkNclASZKaZKAkSU0yUJKkJhkoSVKTDJQkqUkGSpLUJAMlSWqSgZIkNclASZKaZKAkSU2aM1BJbk/yRJKjM8b2JjnU3Y4lOdRnv3VJHkhyOMnDSW6cMffRJEe6/b+c5LxT9ookSSvCfM6g7gC2zByoqmuqamNVbQS+AHyxz36TwOaquhjYCGxJ8sZu7qaqel23/37gQwtavSRpxVoz1wZVdU+SkX5zSQJcDWzus18BE93Dtd2turnvztj0zOlxSZKmzRmoOVwKPF5Vj/abTHIGMA6cD9xSVffPmPsY8A7gaeCyEx0gyVZgK8Dw8DC9Xm+RS5aWB3/Wtdpl6kRnjo2mzqD2V9VFs8Y/A3yzqnbPsf/ZwD5ge1UdnTX3fmBdVX14rnWMjo7W2NjYnOuVlruRnQc4tuvKpV6GNBBJxqtqdPb4gq/iS7IGuArYO9e2VfUU0GPWZ1mdvwF+d6HrkCStTIu5zPxy4JGqOt5vMsmG7syJJOunt+8e/+KMTd8yPS5J0rT5XGa+B7gXuCDJ8SQ3dFPXAntmbXtekju7h+cCB5McAR4E7q6q/d3criRHu7k3AX98Cl6LJGkFmc9VfNedYPz6PmOPAVd0948Al5xgX9/SkyS9IL9JQpLUJAMlSWqSgZIkNclASZKaZKAkSU0yUJKkJhkoSVKTDJQkqUkGSpLUJAMlSWqSgZIkNclASZKaZKAkSU0yUJKkJhkoSVKTDJQkqUkGSpLUJAMlSWqSgZIkNclASZKaZKAkSU0yUJKkJhkoSVKTDJQkqUkGSpLUJAMlSWqSgZIkNclASZKaZKAkSU0yUJKkJhkoSVKTDJQkqUkGSpLUJAMlNWT79u2sW7eOb338zaxbt47t27cv9ZKkJZOqWuo1zNvo6GiNjY0t9TKkeUsy8GMup3/TEkCS8aoanT3uGZR0GlXVvG9DQ0Ps3r2bquLgwYNUFbt372ZoaOiknkdaKQyU1IjJyUm2bdv2vLFt27YxOTm5RCuSlpaBkhoxNDTErbfe+ryxW2+9laGhoSVakbS01iz1AiRNefe7382OHTsAuPDCC/nkJz/Jjh07fuqsSlotDJTUiE9/+tMAfOADH2BycpKhoSG2bdv23Li02ngVn9SgXq/Hpk2blnoZ0kB4FZ8kaVkxUJKkJs0ZqCS3J3kiydEZY3uTHOpux5Ic6rPfuiQPJDmc5OEkN86YuynJI0mOJNmX5OxT9YIkSSvDfM6g7gC2zByoqmuqamNVbQS+AHyxz36TwOaquhjYCGxJ8sZu7m7goqp6HfAN4P0LWr0kacWaM1BVdQ/wnX5zmfoel6uBPX32q6qa6B6u7W7VzX25qn7czd0HvPzkly5JWskWe5n5pcDjVfVov8kkZwDjwPnALVV1f5/N3gXsPdEBkmwFtgIMDw/T6/UWuWSpfRMTE/6sa9VbbKCuo8/Z07SqehbY2H3GtC/JRVU187OsDwI/Bj7/As9xG3AbTF1m7qW3Wg28zFxaRKCSrAGuAt4w17ZV9VSSHlOfZR3t9n8n8Gbgt2qev4w1Pj7+ZJJvLXTN0jJyDvDkUi9CGpBX9BtczBnU5cAjVXW832SSDcAzXZzWd9t/vJvbAuwAfrOq/ne+B6yqDYtYr7RsJBnr94uL0moyn8vM9wD3AhckOZ7khm7qWma9vZfkvCR3dg/PBQ4mOQI8CNxdVfu7uZuBs4C7u0vVn/8NmZKkVW9ZfdWRtFp4BiX5TRJSq25b6gVIS80zKElSkzyDkiQ1yUBJkppkoKRTKMnEHPNnJ/mjGY/PS/IP3f2NSa5YwDE/kuR9J79aqW0GShqss4HnAlVVj1XV73UPNwInHShppTJQ0mmQ5CVJ/iXJQ0m+luSt3dQu4NXd7//dlGQkydEkLwb+HLimm7tm9plRt91Id/+DSb6e5J+BC2Zs8+okdyUZT/KVJL80uFctnVqL/S4+Sf39EHhbVX03yTnAfUn+CdjJ1J+a2QgwHZyq+lGSDwGjVfXebu4j/Z44yRuY+kX5S5j6N/wQU1/KDFOXp2+rqkeT/BrwV8Dm0/IKpdPMQEmnR4C/SPIbwE+AnweGT9FzXwrsm/6asC58JHkJ8OvA30/9JRwAhk7RMaWBM1DS6fF2YAPwhqp6JskxYN1JPsePef7b8DP37/cLjC8Cnpo+O5OWOz+Dkk6PlwJPdHG6jP//tubvMfU9lP3MnjsGvB4gyeuBV3bj9wBvS7I+yVnA7wBU1XeB/0jy+90+SXLxqXtJ0mAZKOn0+DwwmmSMqbOpRwCq6r+Bf+0ueLhp1j4HgQunL5IAvgD8bJJDwHuAb3TP8RBTf+TzULfNV2Y8x9uBG5IcBh4G3oq0TPlVR5KkJnkGJUlqkoGSJDXJQEmSmmSgJElNMlCSpCYZKElSkwyUJKlJ/wcvyHI9fhansgAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "C:\\Users\\sboeh\\anaconda3\\lib\\site-packages\\pandas\\core\\arraylike.py:358: RuntimeWarning: invalid value encountered in log\n",
      "  result = getattr(ufunc, method)(*inputs, **kwargs)\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAOeUlEQVR4nO3cfayed13H8ffH1kZgxEEKh9I2njlqtJIp87gtYsiJMLJOsi7GhC0qAxMbcEswPowi/uVfSzBiFpbVRklGJCsPSmhI4xzDW8LDZA+MYjPmmgGutoyHPwZnMy4lX/841+Lh5G53uvvuzvec834lJ70eftd1/U5zN+9c1333TlUhSVI3P7HaE5AkaRwDJUlqyUBJkloyUJKklgyUJKmlzas9gedj69atNTs7u9rTkM6rp556ipe85CWrPQ3pvHvggQe+V1WvWL59TQZqdnaW+++/f7WnIZ1Xo9GI+fn51Z6GdN4l+da47T7ikyS1ZKAkSS0ZKElSSwZKktSSgZIktWSgJEktGShJUksGSpLUkoGSJLVkoCRJLRkoSVJLBkqS1JKBkiS1ZKAkSS0ZKElSSwZKktSSgZIktWSgJEktGShJUksGSpLUkoGSJLVkoCRJLRkoSVJLBkqS1JKBkiS1ZKAkSS0ZKElSSwZKktSSgZIktTSVQCW5KskjSY4n2T9mf5LcOuw/muTSZfs3JflKkk9PYz6SpLVv4kAl2QTcBuwBdgPXJ9m9bNgeYNfwsw+4fdn+dwMPTzoXSdL6MY07qMuA41X1WFU9AxwC9i4bsxf4cC26F7gwyTaAJDuA3wT+bgpzkSStE5uncI7twONL1k8Al69gzHbgFPA3wM3AS892kST7WLz7YmZmhtFoNMmcpfYWFhZ8nWtDm0agMmZbrWRMkrcA36mqB5LMn+0iVXUQOAgwNzdX8/NnHS6teaPRCF/n2sim8YjvBLBzyfoO4OQKx7weuCbJN1l8NPgbSf5hCnOSJK1x0wjUfcCuJBcl2QJcBxxeNuYw8Lbh03xXAE9W1amqem9V7aiq2eG4z1bV705hTpKkNW7iR3xVdTrJTcBdwCbgQ1V1LMk7h/0HgCPA1cBx4GngHZNeV5K0vk3jPSiq6giLEVq67cCS5QJufI5zjIDRNOYjSVr7/CYJSVJLBkqS1JKBkiS1ZKAkSS0ZKElSSwZKktSSgZIktWSgJEktGShJUksGSpLUkoGSJLVkoCRJLRkoSVJLBkqS1JKBkiS1ZKAkSS0ZKElSSwZKktSSgZIktWSgJEktGShJUksGSpLUkoGSJLVkoCRJLRkoSVJLBkqS1JKBkiS1ZKAkSS0ZKElSSwZKktSSgZIktWSgJEktGShJUksGSpLUkoGSJLVkoCRJLRkoSVJLUwlUkquSPJLkeJL9Y/Ynya3D/qNJLh2270zyr0keTnIsybunMR9J0to3caCSbAJuA/YAu4Hrk+xeNmwPsGv42QfcPmw/DfxJVf0CcAVw45hjJUkb0DTuoC4DjlfVY1X1DHAI2LtszF7gw7XoXuDCJNuq6lRVPQhQVT8EHga2T2FOkqQ1bvMUzrEdeHzJ+gng8hWM2Q6cenZDklngdcC/j7tIkn0s3n0xMzPDaDSacNpSbwsLC77OtaFNI1AZs63OZUySC4B/BP6oqn4w7iJVdRA4CDA3N1fz8/PPa7LSWjEajfB1ro1sGo/4TgA7l6zvAE6udEySn2QxTh+pqn+awnwkSevANAJ1H7AryUVJtgDXAYeXjTkMvG34NN8VwJNVdSpJgL8HHq6qv57CXCRJ68TEj/iq6nSSm4C7gE3Ah6rqWJJ3DvsPAEeAq4HjwNPAO4bDXw/8HvC1JA8N2/68qo5MOi9J0to2jfegGIJyZNm2A0uWC7hxzHGfZ/z7U5KkDc5vkpAktWSgJEktGShJUksGSpLUkoGSJLVkoCRJLRkoSVJLBkqS1JKBkiS1ZKAkSS0ZKElSSwZKktSSgZIktWSgJEktGShJUksGSpLUkoGSJLVkoCRJLRkoSVJLBkqS1JKBkiS1ZKAkSS0ZKElSSwZKktSSgZIktWSgJEktGShJUksGSpLUkoGSJLVkoCRJLRkoSVJLBkqS1JKBkiS1ZKAkSS0ZKElSSwZKktTSVAKV5KokjyQ5nmT/mP1Jcuuw/2iSS1d6rCRpY5o4UEk2AbcBe4DdwPVJdi8btgfYNfzsA24/h2MlSRvQNO6gLgOOV9VjVfUMcAjYu2zMXuDDtehe4MIk21Z4rCRpA9o8hXNsBx5fsn4CuHwFY7av8FgAkuxj8e6LmZkZRqPRRJOWultYWPB1rg1tGoHKmG21wjErOXZxY9VB4CDA3Nxczc/Pn8MUpbVnNBrh61wb2TQCdQLYuWR9B3ByhWO2rOBYSdIGNI33oO4DdiW5KMkW4Drg8LIxh4G3DZ/muwJ4sqpOrfBYSdIGNPEdVFWdTnITcBewCfhQVR1L8s5h/wHgCHA1cBx4GnjH2Y6ddE6SpLVvGo/4qKojLEZo6bYDS5YLuHGlx0qS5DdJSJJaMlCSpJYMlCSpJQMlSWrJQEmSWjJQkqSWDJQkqSUDJUlqyUBJkloyUJKklgyUJKklAyVJaslASZJaMlCSpJYMlCSpJQMlSWrJQEmSWjJQkqSWDJQkqSUDJUlqyUBJkloyUJKklgyUJKklAyVJaslASZJaMlCSpJYMlCSpJQMlSWrJQEmSWjJQkqSWDJQkqSUDJUlqyUBJkloyUJKklgyUJKklAyVJammiQCV5eZK7kzw6/PmyM4y7KskjSY4n2b9k+/uTfD3J0SSfTHLhJPORJK0fk95B7QfuqapdwD3D+o9Jsgm4DdgD7AauT7J72H038NqqugT4T+C9E85HkrROTBqovcAdw/IdwLVjxlwGHK+qx6rqGeDQcBxV9S9VdXoYdy+wY8L5SJLWic0THj9TVacAqupUkleOGbMdeHzJ+gng8jHjfh/46JkulGQfsA9gZmaG0Wj0fOcsrQkLCwu+zrWhPWegknwGeNWYXe9b4TUyZlstu8b7gNPAR850kqo6CBwEmJubq/n5+RVeXlqbRqMRvs61kT1noKrqTWfal+SJJNuGu6dtwHfGDDsB7FyyvgM4ueQcNwBvAd5YVYUkSUz+HtRh4IZh+QbgU2PG3AfsSnJRki3AdcNxJLkKeA9wTVU9PeFcJEnryKSBugW4MsmjwJXDOkleneQIwPAhiJuAu4CHgY9V1bHh+A8CLwXuTvJQkgMTzkeStE5M9CGJqvo+8MYx208CVy9ZPwIcGTPuNZNcX5K0fvlNEpKklgyUJKklAyVJaslASZJaMlCSpJYMlCSpJQMlSWrJQEmSWjJQkqSWDJQkqSUDJUlqyUBJkloyUJKklgyUJKklAyVJaslASZJaMlCSpJYMlCSpJQMlSWrJQEmSWjJQkqSWDJQkqSUDJUlqyUBJkloyUJKklgyUJKklAyVJaslASZJaMlCSpJYMlCSpJQMlSWrJQEmSWjJQkqSWDJQkqSUDJUlqyUBJkloyUJKkliYKVJKXJ7k7yaPDny87w7irkjyS5HiS/WP2/2mSSrJ1kvlIktaPSe+g9gP3VNUu4J5h/cck2QTcBuwBdgPXJ9m9ZP9O4ErgvyaciyRpHZk0UHuBO4blO4Brx4y5DDheVY9V1TPAoeG4Z30AuBmoCeciSVpHNk94/ExVnQKoqlNJXjlmzHbg8SXrJ4DLAZJcA/x3VX01yVkvlGQfsA9gZmaG0Wg04dSl3hYWFnyda0N7zkAl+QzwqjG73rfCa4wrTyV58XCON6/kJFV1EDgIMDc3V/Pz8yu8vLQ2jUYjfJ1rI3vOQFXVm860L8kTSbYNd0/bgO+MGXYC2LlkfQdwErgYuAh49u5pB/Bgksuq6tvn8DtIktahSd+DOgzcMCzfAHxqzJj7gF1JLkqyBbgOOFxVX6uqV1bVbFXNshiyS42TJAkmD9QtwJVJHmXxk3i3ACR5dZIjAFV1GrgJuAt4GPhYVR2b8LqSpHVuog9JVNX3gTeO2X4SuHrJ+hHgyHOca3aSuUiS1he/SUKS1JKBkiS1ZKAkSS0ZKElSSwZKktSSgZIktWSgJEktGShJUksGSpLUkoGSJLVkoCRJLRkoSVJLBkqS1JKBkiS1ZKAkSS0ZKElSSwZKktSSgZIktWSgJEktGShJUksGSpLUkoGSJLVkoCRJLRkoSVJLBkqS1JKBkiS1ZKAkSS0ZKElSS6mq1Z7DOUvyXeBbqz0P6TzbCnxvtSchvQB+pqpesXzjmgyUtBEkub+q5lZ7HtJq8RGfJKklAyVJaslASX0dXO0JSKvJ96AkSS15ByVJaslASZJaMlDShJIsnIdzXpNk/7B8bZLdz+McoyR+TF1rloGSGqqqw1V1y7B6LXDOgZLWOgMlTUkWvT/JfyT5WpK3Dtvnh7uZTyT5epKPJMmw7+ph2+eT3Jrk08P2tyf5YJJfA64B3p/koSQXL70zSrI1yTeH5RclOZTkaJKPAi9aMrc3J/lSkgeTfDzJBS/s34507jav9gSkdeS3gF8GfonFrym6L8nnhn2vA34ROAl8AXh9kvuBvwXeUFXfSHLn8hNW1ReTHAY+XVWfABjaNs67gKer6pIklwAPDuO3An8BvKmqnkryHuCPgb+cwu8snTcGSpqeXwfurKofAU8k+TfgV4EfAF+uqhMASR4CZoEF4LGq+sZw/J3Avgmu/wbgVoCqOprk6LD9ChYfEX5hiNsW4EsTXEd6QRgoaXrOeGsD/O+S5R+x+G/vbOPP5jT//3j+p5btG/cfGwPcXVXXP8/rSavC96Ck6fkc8NYkm5K8gsU7mi+fZfzXgZ9NMjusv/UM434IvHTJ+jeBXxmWf3vZ9X8HIMlrgUuG7fey+EjxNcO+Fyf5uZX8QtJqMlDS9HwSOAp8FfgscHNVfftMg6vqf4A/BP45yeeBJ4Anxww9BPxZkq8kuRj4K+BdSb7I4ntdz7oduGB4tHczQxyr6rvA24E7h333Aj8/yS8qvRD8qiNpFSW5oKoWhk/13QY8WlUfWO15SR14ByWtrj8YPjRxDPhpFj/VJwnvoCRJTXkHJUlqyUBJkloyUJKklgyUJKklAyVJaun/AAQpVboT/6rPAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAUbElEQVR4nO3df5BdZ33f8fdHkpEMRhjGsKXAWGFqMzIKoWFhSByCHIJrD0zJTDGpixNIlC5t001AgHFQAnVmNHExdkqNW/AEY0/ragqBtFSMjVx3L564BrICG2SZ2AzF1PyIIS4OAq3QSt/+sUdivVp5V7tXdx/tvl8zd+45z/nxfO9orj73Offsc1NVSJLUmlVLXYAkSbMxoCRJTTKgJElNMqAkSU0yoCRJTVozyM7OOuus2rBhwyC7lJr0ox/9iKc85SlLXYbUhN27d3+/qp45s32gAbVhwwbGx8cH2aXUpF6vx+bNm5e6DKkJSR6ard1LfJKkJhlQkqQmGVCSpCbNGVBJnpdkLMn9Se5L8vtd+zOS3J7kwe756Se/XEnSSjGfEdQk8Paq2gi8HPjdJOcBVwB3VNU5wB3duiRJfTFnQFXVd6rqi93yD4H7gecArwNu7na7Gfi1k1SjJGkFOqHbzJNsAP4h8HlgqKq+A1MhluRZxzlmBBgBGBoaotfrLaZeaVnYt2+f7wVpDvMOqCRnAJ8A3lpVf5dkXsdV1Q3ADQDDw8Pl335oJduxYwfbt2/n/vvvZ+PGjWzbto1LL710qcuSmjSvgEpyGlPhdEtVfbJr/pskz+5GT88GHjlZRUrLwY4dO9i2bRsf+chHOHToEKtXr2bLli0AhpQ0i8z1g4WZGirdDDxaVW+d1n418LdVdVWSK4BnVNXlT3Su4eHhciYJrVSbNm3inHPO4dZbb+XAgQOsXbuWiy++mAcffJA9e/YsdXnSkkmyu6qGZ7bPZwR1PvAbwFeS3NO1vRu4CvhYki3AN4FL+lSrtCzt3buXr371q7zvfe/jvPPOY+/evVx++eUcPnx4qUuTmjRnQFXVXwLH+8LpVf0tR1reRkZG2Lp1K71ej61bt/K1r32ND33oQ0tdltSkgU4WK61kVcXHP/5xbr31Vh566CHOPvts9u3bx1yX2aWVyqmOpAFZs2YN+/fvB+DIXbD79+9nzRo/J0qzMaCkAVm/fj0TExOMjo7y6U9/mtHRUSYmJli/fv1SlyY1yY9u0oD84Ac/4C1veQvvfve7j97FNzIywoc//OGlLk1qkiMoaUA2btzIJZdcwsTEBGNjY0xMTHDJJZewcePGpS5NapIBJQ3Itm3b2LJlC2NjY0xOTjI2NsaWLVvYtm3bUpcmNclLfNKAHJktYnR09OhUR9u3b3cWCek45pxJop+cSUKa0uv1cF5KacrxZpLwEp8kqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJcwZUkhuTPJJkz7S2Fyf5XJJ7kownednJLVOStNLMZwR1E3DRjLb3AVdW1YuB93TrkiT1zZwBVVV3Ao/ObAbWd8tPA77d57okSSvcQn+w8K3AZ5K8n6mQ+8W+VSRJEgsPqH8JvK2qPpHkDcBHgF+dbcckI8AIwNDQEL1eb4FdSsvHvn37fC9Ic5jXL+om2QDsrKpN3fpjwJlVVUkCPFZV65/oHOAv6kpH+Iu60k/1+xd1vw28slv+FeDBhRYmSdJs5rzEl2QHsBk4K8nDwHuBfw58IMkaYILuEp4kSf0yZ0BV1aXH2fSSPtciSdJRziQhSWqSASVJapIBJUlqkgElSWqSASUN0OjoKOvWreOCCy5g3bp1jI6OLnVJUrMWOpOEpBM0OjrK9ddfz6pVU58LJycnuf766wG47rrrlrI0qUnzmkmiX5xJQivZmjVrqCquvvpqzjvvPPbu3cs73/lOkjA5ObnU5UlLpt8zSUg6QYcOHWL79u1s3bqVdevWsXXrVrZv386hQ4eWujSpSQaUJKlJXuKTBmT16tUcPnyY1atXc+jQoaPPq1atchSlFc1LfNISe+ELXwjAkQ+FR56PtEt6PANKGpAHHniA888/n9NOOw2A0047jfPPP58HHnhgiSuT2mRASQNy4MABdu3axcTEBGNjY0xMTLBr1y4OHDiw1KVJTfLvoKQBWbt2LRdeeCHj4+McOHCAtWvXMjw8zNq1a5e6NKlJjqCkATn33HO56667OHjwIAAHDx7krrvu4txzz13iyqQ2GVDSgNx3330AHD58+HHPR9olPZ4BJQ3I4cOHScI111zDrbfeyjXXXEOSo0El6fEMKGmALr744sfNJHHxxRcvdUlSswwoaYBuu+02rr32WiYmJrj22mu57bbblrokqVnexScNyKpVqzh8+DBvf/vbj2mXdCzfGdKAnHnmmSfULq10cwZUkhuTPJJkz4z20SR/neS+JO87eSVKy8Ojjz4KTM3JN/35SLukx5vPCOom4KLpDUkuAF4HvKiqXgi8v/+lScvPlVdeyeTkJGNjY0xOTnLllVcudUlSs+Y1m3mSDcDOqtrUrX8MuKGq/ueJdOZs5lrJknDGGWewb9++o21H1gf5qwJSa/o9m/m5wCuSfD7JZ5O8dHHlSSvD9HCabV3STy30Lr41wNOBlwMvBT6W5Pk1y8fAJCPACMDQ0BC9Xm+BXUrLl+8L6VgLDaiHgU92gfSFJIeBs4Dvzdyxqm4AboCpS3ybN29eYJfS8uX7QjrWQi/x/TfgVwCSnAs8Cfh+n2qSJGnuEVSSHcBm4KwkDwPvBW4EbuxuPf8J8KbZLu9JkrRQcwZUVV16nE2X9bkWSZKOciYJSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSk+YMqCQ3JnkkyZ5Ztr0jSSU56+SUJ0laqeYzgroJuGhmY5LnAa8GvtnnmiRJmjugqupO4NFZNv0pcDlQ/S5KkqQFfQeV5B8D36qqe/tcjyRJAKw50QOSPBnYBlw4z/1HgBGAoaEher3eiXYpLXu+L6RjpWruK3RJNgA7q2pTkp8F7gB+3G1+LvBt4GVV9d0nOs/w8HCNj48vrmLpFJXkuNvm8z6Ulqsku6tqeGb7CY+gquorwLOmnfgbwHBVfX9RFUqSNM18bjPfAdwNvCDJw0m2nPyyJEkr3ZwjqKq6dI7tG/pWjSRJHWeSkCQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNWnOgEpyY5JHkuyZ1nZ1kq8m+XKSv0hy5kmtUpK04sxnBHUTcNGMttuBTVX1IuAB4A/6XJckaYWbM6Cq6k7g0Rltu6pqslv9HPDck1CbJGkF68d3UL8N3NqH80iSdNSaxRycZBswCdzyBPuMACMAQ0ND9Hq9xXQpLUu+L6Rjparm3inZAOysqk3T2t4E/AvgVVX14/l0Njw8XOPj4wssVTq1JTnutvm8D6XlKsnuqhqe2b6gEVSSi4B3Aa+cbzhJknQi5nOb+Q7gbuAFSR5OsgX4IPBU4PYk9yT50EmuU5K0wsw5gqqqS2dp/shJqEWSpKOcSUKS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktSkOQMqyY1JHkmyZ1rbM5LcnuTB7vnpJ7dMSdJKM58R1E3ARTPargDuqKpzgDu6dUmS+mbOgKqqO4FHZzS/Dri5W74Z+LX+liVJWunWLPC4oar6DkBVfSfJs/pYk3TKSLIk56mqvvQrtWyhATVvSUaAEYChoSF6vd7J7lIamLGxsXnve8EFF/TlPIDvI60Imc8nsSQbgJ1Vtalb/2tgczd6ejbQq6oXzHWe4eHhGh8fX2TJ0qnpiUZJjoi0kiXZXVXDM9sXepv5p4A3dctvAv77QguTVorjhZDhJM1uPreZ7wDuBl6Q5OEkW4CrgFcneRB4dbcuaQ5VRVVx9rt2Hl2WNLs5v4OqqkuPs+lVfa5FkqSjnElCktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktSkkz4Xn9S6n7tyF4/tPzjwfjdc8emB9/m000/j3vdeOPB+pYUwoLTiPbb/IN+46jUD7bPX67F58+aB9glLE4rSQnmJT5LUJANKktQkA0qS1CS/g9KK99SNV/CzN18x+I5vHnyXT90IMNjv26SFMqC04v3w/qu8SUJqkJf4JElNcgQlsUQji9uW5u+gpFOFAaUVb9CX92AqEJeiX+lU4iU+SVKTDChJUpMMKElSkwwoSVKTFhVQSd6W5L4ke5LsSLKuX4VJkla2BQdUkucAvwcMV9UmYDXwT/tVmCRpZVvsJb41wOlJ1gBPBr69+JIkSVrE30FV1beSvB/4JrAf2FVVu/pWmbQMJfnp8r+deq6qJapGatuCAyrJ04HXAT8D/AD4eJLLquo/z9hvBBgBGBoaotfrLbhY6VR2wQUXzNqehLGxsQFXI7UvC/30luQS4KKq2tKt/ybw8qr6V8c7Znh4uMbHxxfUn3Sqmz56mslRlFayJLuranhm+2KmOvom8PIkT2bqEt+rANNHK8oThc7JPI+BppVgMd9BfT7JnwNfBCaBLwE39Ksw6VRwIkHhCEo6MYuaLLaq3gu8t0+1SJJ0lDNJSJKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmrSogEpyZpI/T/LVJPcn+YV+FSZJWtnWLPL4DwC3VdXrkzwJeHIfapIkaeEBlWQ98MvAmwGq6ifAT/pTliRppVvMJb7nA98DPprkS0n+LMlT+lSXJGmFW8wlvjXAzwOjVfX5JB8ArgD+aPpOSUaAEYChoSF6vd4iupSWJ98X0rFSVQs7MPl7wOeqakO3/grgiqp6zfGOGR4ervHx8QX1J53qkhx320Lfh9JykGR3VQ3PbF/wJb6q+i7wf5O8oGt6FbB3oeeTJGm6xd7FNwrc0t3B93XgtxZfkiRJiwyoqroHOGZYJknSYjmThCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlDQgVXVC7dJKZ0BJA1RVVBVjY2NHlyXNzoCSJDVp0QGVZHWSLyXZ2Y+CJEmC/oygfh+4vw/nkSTpqEUFVJLnAq8B/qw/5UiSNGWxI6h/B1wOHF58KZIk/dSahR6Y5LXAI1W1O8nmJ9hvBBgBGBoaotfrLbRLadnYt2+f7wVpDlnoba5J/gT4DWASWAesBz5ZVZcd75jh4eEaHx9fUH/SctLr9di8efNSlyE1Icnuqho+pr0ff4fRjaDeUVWvnWO/7wEPLbpD6dR3FvD9pS5CasTZVfXMmY0LvsS3ELMVIK1EScZn+8Qo6af6MoKSdGIMKGluziQhSWqSASUtjRuWugCpdV7ikyQ1yRGUJKlJBpQkqUkGlJaVJPvm2L4hyT+bx3n+d/+qGrwkr0hyX5J7kpy+gOPffTLqkk6EAaWVZgMwZ0BV1S+e/FIWJ8nqJ9j8RuD9VfXiqtq/gNMbUFpyBpSWpUy5OsmeJF9J8uvdpquAV3Qji7cleWGSL3TrX05yTnf8vu75j7tt9yT5VpKPdu2XTTvuw93voq1OctO0Pt/W7fuSJPcmuftITV37m5N8cFrNO4/Ma5nkPyYZ70ZBV07b5xtJ3pPkL4FLklzYnfeLST6e5IwkvwO8AXhPklu6496Z5K+61zj9fLO9jquA07u2W07Ov5A0D0d+dtqHj+XwAPZ1z/8EuB1YDQwB3wSeDWwGdk7b/zrgjd3yk4DTp59n2n5PA74MvATYCPwP4LRu238AfrPbdvu0Y87snr8MvLJbvhrY0y2/GfjgtP13Apu75Wd0z6uBHvCibv0bwOXd8lnAncBTuvV3Ae/plm8CXt8tX8jUbe1h6kPpTuCXj/c6Znv9PnwsxWOgUx1JA/RLwI6qOgT8TZLPAi8F/m7GfncD27rfNvtkVT0480RJAtwC/GlNzd7/r5kKo7+a2sTpwCNM/Wf//CTXAZ8GdiV5GlNB9dnudP8JuHge9b+h+yWANUwF63lMBR3Af+2eX96139XV8aTu9cx0Yff4Urd+BnAO8KLjvA6pCQaUlqvMZ6eq+i9JPs/UD29+JsnvVNX/mrHbvwEerqqPTjv3zVX1B8d0mvwc8I+A32XqMttW4Hh/bDjJ4y+zr+vO8TPAO4CXVtX/S3LTkW2dH02r4/aqunSOlxngT6rqwzNqHT3e65Ba4HdQWq7uBH69+07lmUxd0voC8EPgqUd2SvJ84OtV9e+BTzE1qmDa9tcCrwZ+b1rzHcDrkzyr2+cZSc5Ochawqqo+AfwR8PNV9QPgsSS/1B37xmnn+Qbw4iSrkjwPeFnXvp6pEHosyRDHH3F9Djg/yT/o6nhyknNn2e8zwG8nOaPb7zld7bO+ju6Yg0lOO06/0kA4gtJy9RfALwD3MjWCubyqvpvkb4HJJPcy9T3NOuCyJAeB7wJ/POM8bwf+PvCF7jLYp6rqPUn+kKlLeKuAg0yNmPYDH+3aAI6MTH4LuDHJj5kKiyPuAv4P8BVgD/BFgKq6N8mXgPuAr3f7HaOqvpfkzcCOJGu75j8EHpix364kG4G7u9ewD7isqvYe53U8xNR3Vl9O8sWqmh6q0sA41ZE0QEk2MHWTxqalrkVqnZf4JElNcgQlSWqSIyhJUpMMKElSkwwoSVKTDChJUpMMKElSk/4/nZ2rHfRggkcAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAATfElEQVR4nO3df2zcd33H8dfLdrmjP6AdhUsHgQyGOpxCM3bqwqZVbkuBhcqFCRho/MqQreuKVbCaukQDBJq6uiHRUCZ6xBNtJjY2KESZulIVlh4daC27QAvpjw2tBCglKRUNrUt7reP3/riLGzt24txdvvex7/mQrDt/7nvf7zuRvnr58+P7/ToiBABAano6XQAAAPMhoAAASSKgAABJIqAAAEkioAAASSKgAABJ6jvWBra/IOkSSY9ExDmNtt+S9K+SVknaK+ldEfHYsfZ15plnxqpVq1ooF1gennzySZ1yyimdLgNIwu7dux+NiBfPbfexroOyfb6kSUn/eFhAXSfpVxFxre2rJZ0REWPHKqJYLEa1Wm3qHwAsJ5VKRQMDA50uA0iC7d0RUZzbfswhvoi4Q9Kv5jRfKml74/12SW9rtUAAAA7X7BxUISJ+IUmN15e0ryQAABYxB9Uq28OShiWpUCioUqmc6EMCyZucnORcAI6h2YDab/usiPiF7bMkPbLQhhGxTdI2qT4Hxbg7wBwUsBjNDvH9m6QPNN5/QNLO9pQDAEDdMQPK9pck/Zeks20/ZPtDkq6VdLHtH0m6uPE7AABtc8whvoh4zwIfXdTmWgAAmMGdJIAMjYyMKJ/P64ILLlA+n9fIyEinSwKSdcJX8QGoGxkZUblc1vj4uPr7+3XfffdpbKx+ffvWrVs7XB2QHnpQQEYmJiY0Pj6u0dFR5fN5jY6Oanx8XBMTE50uDUgSAQVkpFarqVQqzWorlUqq1WodqghIGwEFZCSXy6lcLs9qK5fLyuVyHaoISBtzUEBGhoaGZuac+vv7tWXLFo2NjR3RqwJQR0ABGTm0EGLjxo2q1WrK5XIqlUoskAAWcMzHbbQTj9sA6rjVEfCcph+3AQBAJxBQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJLUUULavsL3H9r22P9KmmgAAaD6gbJ8jaUjSeZLOlXSJ7Ve3qzAAQHdrpQf1Gkl3RsRvImJK0rckvb09ZQEAul0rAbVH0vm2X2T7ZEnrJK1sT1kAgG7X1+wXI+J+2+OSviFpUtI9kqbmbmd7WNKwJBUKBVUqlWYPCSwbk5OTnAvAMTgi2rMj+xpJD0XE5xbaplgsRrVabcvxgKWsUqloYGCg02UASbC9OyKKc9ub7kE1dvqSiHjE9ssl/ZmkN7SyPwAADmkpoCR91faLJD0r6fKIeKwNNQEA0FpARcSftKsQAAAOx50kAABJIqAAAEkioIAMjYyMKJ/P64ILLlA+n9fIyEinSwKS1eoiCQCLNDIyonK5rPHxcfX39+u+++7T2NiYJGnr1q0drg5IDz0oICMTExMaHx/X6Oio8vm8RkdHNT4+romJiU6XBiSJgAIyUqvVVCqVZrWVSiXVarUOVQSkjYACMpLL5VQul2e1lctl5XK5DlUEpI05KCAjQ0NDM3NO/f392rJli8bGxo7oVQGoI6CAjBxaCLFx40bVajXlcjmVSiUWSAALaNvNYheDm8UCddwsFnjOQjeLZQ4KAJAkAgoAkCQCCgCQJAIKAJAkAgoAkCQCCsgQN4sFFo/roICMcLNY4PhwHRSQkXw+r2KxqGq1OnOh7qHfn3766U6XB3TMQtdB0YMCMlKr1XTnnXfquuuum+lBXXXVVTp48GCnSwOSREABGVq5cuWsWx2tXLlSe/fu7XRZQJIIKCBDe/fulW1J0jPPPEM4AUfBKj4gYz09PbNeAcyPMwQAkCQCCsjYoUURLI4Ajq6lgLL9Udv32t5j+0u28+0qDFiuBgcHtWPHDg0ODna6FCBpTV8HZfulkr4tqT8inrL9ZUm3RMSNC32H66DQzQ4tjujt7dXBgwdnXiUpy+sRgdScqOdB9Ul6vu0+SSdLerjF/QHL2tq1a9XXV18829fXp7Vr13a4IiBdTS8zj4if2/6MpJ9KekrSbRFxW9sqA5aZnp4effe739WmTZtmLtTdsGEDq/mABTQdULbPkHSppN+RdEDSV2y/NyK+OGe7YUnDklQoFFSpVJouFljKBgcHtXPnTm3YsEHT09Pq6elRROjSSy/lvADm0coc1DslvSUiPtT4/f2S1kbEXy30Heag0O1GRkY0MTExcyeJoaEhbhSLrnci5qB+Kmmt7ZNdn/29SNL9LewPAIAZrcxB3WX7JknfkzQl6fuStrWrMGC54XEbwPHhcRtARvL5vK655hqNjo6qUqloYGBAW7Zs0caNG3ncBrraiVpmDmCRarWaSqXSrLZSqaRardahioC0EVBARnK5nMrl8qy2crmsXC7XoYqAtPG4DSAjQ0NDuvLKK2ceUtjb26vp6WldfvnlnS4NSBI9KABAklgkAWQkn8/rjDPO0L59+2baVqxYoccee4xFEuhqLJIAOqxWq2nfvn2z7ma+b98+FkkACyCggAytXr1aO3fu1Omnn66dO3dq9erVnS4JSBaLJIAM3Xvvverr6zvicRsAjkQPCgCQJAIKyBiPfAcWh4ACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkqemAsn227bsP+3nc9kfaWBsAoIs1/cj3iPgfSWskyXavpJ9L2tGesgAA3a5dQ3wXSfq/iPhJm/YHAOhyTfeg5ni3pC/N94HtYUnDklQoFFSpVNp0SGD54LwAjuSIaG0H9vMkPSxpdUTsP9q2xWIxqtVqS8cDlirbC37W6nkILGW2d0dEcW57O4b4/lTS944VTgAAHI92BNR7tMDwHgAAzWopoGyfLOliSV9rTzkAANS1tEgiIn4j6UVtqgVYco42r3Qi98OcFbpBu1bxAV3peIKCRRLA8eFWR0BGFgohwgmYHwEFZCgiFBF6xdjNM+8BzI+AAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJgAIAJImAAgAkiYACACSJu5mj6537qdv066eezfy4q67+98yP+cLnn6R7PvmmzI8LNIOAQtf79VPPau+1b830mJVKRQMDA5keU+pMKALNYogPAJAkAgoAkCQCCgCQJAIKAJAkAgoAkCRW8aHrnfaaq/Xa7Vdnf+Dt2R/ytNdIUrYrFoFmEVDoek/cfy3LzIEEMcQHAEhSSwFl+3TbN9l+wPb9tt/QrsIAAN2t1SG+z0q6NSLeYft5kk5uQ01A5joy9HVrZ251BCwVTQeU7RdIOl/SByUpIp6R9Ex7ygKyk/X8k1QPxE4cF1hKWulBvVLSLyXdYPtcSbslXRERTx6+ke1hScOSVCgUVKlUWjgksHxwLgBH54ho7ot2UdKdkv44Iu6y/VlJj0fExxf6TrFYjGq12lylwDJCDwp4ju3dEVGc297KIomHJD0UEXc1fr9J0utb2B8AADOaDqiI2CfpZ7bPbjRdJOm+tlQFAOh6ra7iG5H0T40VfA9KWt96SQAAtBhQEXG3pCPGDQEAaBV3kgAAJImAAgAkiYACACSJgAIAJImAAgAkiedBARmy/dz78fprs3dzAZY7elBARg4Pp8W0A92OgAIAJIkhPqAF7er9HO9+GBZENyCggBYcT1AcLYQIHOBIDPEBAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAktTS4zZs75X0hKSDkqYiotiOogAAaMfzoC6IiEfbsB8AAGYwxAcASFKrPaiQdJvtkPT5iNg2dwPbw5KGJalQKKhSqbR4SGD54bwAjuRWHjVt+7cj4mHbL5H0DUkjEXHHQtsXi8WoVqtNHw9YynjkOzA/27vnW8PQ0hBfRDzceH1E0g5J57WyPwAADmk6oGyfYvu0Q+8lvUnSnnYVBgDobq3MQRUk7WgMW/RJ+ueIuLUtVQEAul7TARURD0o6t421AAAwg2XmAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUEDGenp6tGnTJvX0cPoBR9P0I98BNGd6elobNmzodBlA8vgTDgCQJAIKAJAkAgoAkCQCCgCQJAIKyNjg4KB27NihwcHBTpcCJM0R0doO7F5JVUk/j4hLjrZtsViMarXa0vGApcr2gp+1eh4CS5nt3RFRnNvejh7UFZLub8N+AACY0VJA2X6ZpLdK+of2lAMAQF2rPai/k3SVpOnWSwEA4DlN30nC9iWSHomI3bYHjrLdsKRhSSoUCqpUKs0eEli2OC+AIzW9SML230p6n6QpSXlJL5D0tYh470LfYZEEuhmLJID5tX2RRER8LCJeFhGrJL1b0q6jhRMAAMeD66CAjOVyuVmvAObXloCKiMqxroECUFer1XThhReqVqt1uhQgafSggA7YtWtXp0sAkkdAAQCSREABAJJEQAEAkkRAAQCSREABAJJEQAEAkkRAARkrFAq64YYbVCgUOl0KkLSmbxYLoDn79+/X+vXrO10GkDx6UACAJBFQAIAkEVBARhZ63MbRHsMBdDMCCsjIoWc+9fb2znrlWVDA/AgoIEPr1q3T1NSUbr/9dk1NTWndunWdLglIFqv4gAzdcsstDOkBi0QPCgCQJAIKyNjcOSgA8yOggAwVi0X19dVH1vv6+lQsFjtcEZAu5qCADFWrVUWEKpWKBgYGmI8CjoKAAjJGKAGLwxAfkJEPf/jDx9UOdDsCCsjIxMSENm/erIjQ7bffrojQ5s2bNTEx0enSgCQRUEBGarWaSqXSrLZSqaRardahioC0EVBARnK5nMrl8qy2crmsXC7XoYqAtDW9SMJ2XtIdknKN/dwUEZ9sV2HAcjM0NKSxsTFJUn9/v7Zs2aKxsbEjelUA6tzsjSpdX4p0SkRM2j5J0rclXRERdy70nWKxGNVqtblKgWWgt7dX09PTM7/39PTo4MGDHawI6DzbuyPiiIsCmx7ii7rJxq8nNX64LTOwgHw+r+np6VmPfJ+enlY+n+90aUCSWpqDst1r+25Jj0j6RkTc1ZaqgGWoVqspn8/rwIEDWr9+vQ4cOKB8Ps8iCWABLV2oGxEHJa2xfbqkHbbPiYg9h29je1jSsCQVCgVVKpVWDgksaU8//bQuu+wyXXjhhdq1a5euv/56SeK8AObR9BzUETuyPynpyYj4zELbMAeFbmZbp556qp544omZWx2ddtppmpyc5KGF6Gptn4Oy/eJGz0m2ny/pjZIeaLpCoAtMTk5qxYoV2rt3r1asWKHJycljfwnoUq0M8Z0labvtXtWD7ssRcXN7ygKWn1wup6mpKe3fv1/r16+XVF/Vd+ju5gBma2UV3w8i4vcj4nURcU5EfLqdhQHLzdDQkGxr8+bN+vrXv67NmzfLtoaGhjpdGpAk/nQDMrJ161ZJ0saNG1Wr1ZTL5VQqlWbaAczWtkUSi8EiCaDu0CIJACdgkQQAACcSAQUASBIBBQBIEgEFAEgSAQUASBIBBQBIUqbLzG3/UtJPMjsgkK4zJT3a6SKARLwiIl48tzHTgAJQZ7s633UfAJ7DEB8AIEkEFAAgSQQU0BnbOl0AkDrmoAAASaIHBQBIEgEFtIHtiu0FV+XZ/rTtN2ZZE7DU8TwoIAMR8YlO1wAsNfSg0NVsr7L9gO3ttn9g+ybbJ9u+yPb3bf/Q9hds5xrbz9t+2P56bd9oe09jm4822m+0/Q7bRdt3N35+aDsan7/K9q22d9v+T9u/12h/Z2Nf99i+o9H2Qdt/f9gxb7Y90Hg/aXu8sZ9v2j6v0bt70PZgFv+nQLsQUIB0tqRtEfE6SY9LGpV0o6Q/j4jXqj7ScJnt/Hztc/a1RtJLI+KcxjY3HP5hRFQjYk1ErJF0q6TPND7aJmkkIv5A0pWSPtdo/4SkN0fEuZIWEzCnSKo09vOEpL+RdLGkt0v69CK+DySDgAKkn0XEdxrvvyjpIkk/joj/bbRtl3S+6kE2X/vhHpT0Sttbbb9F9cA7gu13SXq9pKttnyrpjyR9xfbdkj4v6azGpt+RdKPtIUm9i/i3PKN68EnSDyV9KyKebbxftYjvA8lgDgqQFnuthY+5o4jHbJ8r6c2SLpf0Lkl/OWsn9mpJn5J0fkQctN0j6UCjVzV3fyXbfyjprZLutr1G0pRm/3GZP+z9s/HctSPTkmqN/Uzb5nzHkkIPCpBebvsNjffvkfRNSats/26j7X2SviXpgQXaZ9g+U1JPRHxV0sdV7yUd/vkLJf2LpPdHxC8lKSIel/Rj2+9sbONGyMn2qyLirsYii0clrZS0V9Ia2z22V0o6r03/D0BS+IsKkO6X9AHbn5f0I0lXSLpT9SG3Pkn/LakcETXb6+e2z9nXSyXd0OgVSdLH5nz+NkmvkDRh1ztkjZ7TX0i63vZfSzpJ9RC7R9Im269Wvff2H402Sfqx6sN2eyR9r9X/ACBF3EkCXc32Kkk3R8Q5na4FwGwM8QEAkkQPCgCQJHpQAIAkEVAAgCQRUACAJBFQAIAkEVAAgCQRUACAJP0/X1KxGWPCtecAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAYj0lEQVR4nO3df5CdVZ3n8fdnjSM/V0aUbCFirAUZlB8p7GF112DLMGwGMzowCkRnBgqKzGyxK1jrLlLUurorM4KO/oOWGxfEGdiYWQO7y48RokuD6xLlh0nokEhcRSeFBYMwOE1QCX73j/t0eae5ne50Cznd/X5V3br3fs85z/Oc5KY//Tz35N5UFZIkteYf7e0DkCRpEANKktQkA0qS1CQDSpLUJANKktSkRXv7AH4VXvnKV9aSJUv29mFIL4inn36a/ffff28fhvSCue+++x6vqldNrM+LgFqyZAn33nvv3j4M6QUxMjLC8PDw3j4M6QWT5AeD6l7ikyQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1aV78PyhpPkryvJpfj6OFxDMoqUGDwml3dWk+MqCkhlUVd9xxh2dOWpCmDKgk1yR5LMloX21tko3d7eEkGycZe1GS0SRbklzcV/9Ekm1JNie5MclBXf2lSb6Y5IEkW5NcOusZSpLmpOmcQV0LLO8vVNVZVbW0qpYC64AbJg5KcgxwAXAicDywIsmRXfN64JiqOg54CBgPovcAL6uqY4E3AX+cZMkezkmSNA9MuUiiqu6aLCTSuyB+JnDygOajgQ1VtbPreydwOnBlVd3e128D8O7x3QH7J1kE7Av8HPjJ9KYizT++56SFbLar+JYBj1bV9gFto8DlSQ4GngFOAwZ95Ph5wNru8ZeBdwE/AvYDPlBVTwzacZJVwCqAxYsXMzIyMotpSG254447ePvb3z6w7mtdC8VsA2olsGZQQ1VtTXIFvct5Y8AmYFd/nySXdbXru9KJwHPAocCvA19P8tWq+t6A7a8GVgMMDQ2VX0eg+WZ8YYRft6GFasar+LrLcGfwy7Of56mqq6vqhKo6CXgC2N43/hxgBfC++uUSpfcCX6mqZ6vqMeAbwNBMj1GSNHfNZpn5KcC2qtoxWYckh3T3h9MLszXd8+XAJcA7x9+j6vwQODk9+wNvBrbN4hglSXPUdJaZrwHuBo5KsiPJ+V3T2Uy4vJfk0CS39pXWJXkQuAm4sKqe7OpXAQcC67ul6p/r6p8BDqD3/tU9wBeqavMM5yZJmsOms4pv5ST1cwfUHqG3GGL8+bJJxh4xSX2M3lJzSdIC5ydJSJKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaNGVAJbkmyWNJRvtqa5Ns7G4PJ9k4ydiLkowm2ZLk4r76J5JsS7I5yY1JDuprOy7J3d2YB5LsM6sZSpLmpOmcQV0LLO8vVNVZVbW0qpYC64AbJg5KcgxwAXAicDywIsmRXfN64JiqOg54CLi0G7MIuA74k6p6IzAMPLvHs5IkzXlTBlRV3QU8MagtSYAzgTUDmo8GNlTVzqraBdwJnN5t8/auBrABOKx7fCqwuao2df1+XFXP7cF8JEnzxKJZjl8GPFpV2we0jQKXJzkYeAY4Dbh3QL/zgLXd49cDleQ24FXAl6rqykE7TrIKWAWwePFiRkZGZjMPqVljY2O+vrUgzTagVjL47Imq2prkCnqX88aATcCu/j5JLutq1/cdz1uB3wR2Al9Lcl9VfW3A9lcDqwGGhoZqeHh4llOR2jQyMoKvby1EM17F171fdAa/PPt5nqq6uqpOqKqT6F0m3N43/hxgBfC+qqquvAO4s6oer6qdwK3ACTM9RknS3DWbZeanANuqasdkHZIc0t0fTi/M1nTPlwOXAO/sgmjcbcBxSfbrAvBtwIOzOEZJ0hw1nWXma4C7gaOS7Ehyftd0NhMu7yU5NMmtfaV1SR4EbgIurKonu/pVwIHA+m6p+ucAuvZPAfcAG4H7q+qWGc9OkjRnTfkeVFWtnKR+7oDaI/QWQ4w/XzbJ2CN2s7/r6C01lyQtYH6ShCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJUwZUkmuSPJZktK+2NsnG7vZwko2TjL0oyWiSLUku7qt/Ism2JJuT3JjkoAnjDk8yluSDM56ZJGlOm84Z1LXA8v5CVZ1VVUuraimwDrhh4qAkxwAXACcCxwMrkhzZNa8Hjqmq44CHgEsnDP808NfTn4Ykab6ZMqCq6i7giUFtSQKcCawZ0Hw0sKGqdlbVLuBO4PRum7d3NYANwGF92/w94HvAlulPQ5I03yya5fhlwKNVtX1A2yhweZKDgWeA04B7B/Q7D1gLkGR/4BLgt4HdXt5LsgpYBbB48WJGRkZmOAWpbWNjY76+tSDNNqBWMvjsiaramuQKepfzxoBNwK7+Pkku62rXd6WPAp+uqrHeydnkqmo1sBpgaGiohoeHZz4LqWEjIyP4+tZCNOOASrIIOAN402R9qupq4Oqu/58CO/rGnwOsAH6rqqor/zPg3UmuBA4CfpHkp1V11UyPU5I0N83mDOoUYFtV7ZisQ5JDquqxJIfTC7O3dPXl9C7lva2qdo73r6plfWM/AowZTpK0ME1nmfka4G7gqCQ7kpzfNZ3NhMt7SQ5NcmtfaV2SB4GbgAur6smufhVwILC+W6r+udlORJI0v0x5BlVVKyepnzug9gi9xRDjz5dN7NPVj5jGfj8yVR9J0vzlJ0lIkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkpo0ZUAluSbJY0lG+2prk2zsbg8n2TjJ2IuSjCbZkuTivvonkmxLsjnJjUkO6uq/neS+JA909yfPeoaSpDlpOmdQ1wLL+wtVdVZVLa2qpcA64IaJg5IcA1wAnAgcD6xIcmTXvB44pqqOAx4CLu3qjwO/W1XHAucAf7mnE5IkzQ9TBlRV3QU8MagtSYAzgTUDmo8GNlTVzqraBdwJnN5t8/auBrABOKyrf7uqHunqW4B9krxsD+YjSZonFs1y/DLg0araPqBtFLg8ycHAM8BpwL0D+p0HrB1Q/33g21X1s0E7TrIKWAWwePFiRkZG9vzopTlgbGzM17cWpNkG1EoGnz1RVVuTXEHvct4YsAnY1d8nyWVd7foJ9TcCVwCnTrbjqloNrAYYGhqq4eHhGU9CatnIyAi+vrUQzXgVX5JFwBkMPvsBoKqurqoTquokepcJt/eNPwdYAbyvqqqvfhhwI/BHVfX/Znp8kqS5bTZnUKcA26pqx2QdkhxSVY8lOZxemL2lqy8HLgHeVlU7+/ofBNwCXFpV35jFsUmS5rjpLDNfA9wNHJVkR5Lzu6azmXB5L8mhSW7tK61L8iBwE3BhVT3Z1a8CDgTWd0vVP9fV/zVwBPAf+paxHzLj2UmS5qwpz6CqauUk9XMH1B6htxhi/PmyScYeMUn9Y8DHpjomSdL85ydJSJKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaNGVAJbkmyWNJRvtqa5Ns7G4PJ9k4ydiLkowm2ZLk4r76J5JsS7I5yY1JDupruzTJd5N8J8m/nNXsJElz1nTOoK4FlvcXquqsqlpaVUuBdcANEwclOQa4ADgROB5YkeTIrnk9cExVHQc8BFzajXkDcDbwxm6fn03ykj2fliRprpsyoKrqLuCJQW1JApwJrBnQfDSwoap2VtUu4E7g9G6bt3c1gA3AYd3jdwFfqqqfVdX3ge/SCzhJ0gKzaJbjlwGPVtX2AW2jwOVJDgaeAU4D7h3Q7zxgbff41fQCa9yOrvY8SVYBqwAWL17MyMjITI5fat7Y2Jivby1Isw2olQw+e6Kqtia5gt7lvDFgE7Crv0+Sy7ra9eOlQZuaZPurgdUAQ0NDNTw8PIPDl9o3MjKCr28tRDNexZdkEXAGvzz7eZ6qurqqTqiqk+hdJtzeN/4cYAXwvqoaD6EdwGv6NnEY8MhMj1GSNHfNZpn5KcC2qtoxWYckh3T3h9MLszXd8+XAJcA7q2pn35D/BZyd5GVJXgccCXxrFscoSZqjprPMfA1wN3BUkh1Jzu+azmbC5b0khya5ta+0LsmDwE3AhVX1ZFe/CjgQWN8tVf8cQFVtAf4KeBD4SjfmuZlPT5I0V035HlRVrZykfu6A2iP0FkOMP182ydgjdrO/y4HLpzouSdL85idJSJKaNNtVfNKCdPxHb+epZ57d43E/uGLFC3A0k3vtJTfv8ZiX7/tSNv3HU1+Ao5H2jAElzcBTzzzLwx9/x54P/PjA/zWxWy/2MvMlH7rlRduXtDte4pMkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNWnKgEpyTZLHkoz21dYm2djdHk6ycZKxFyUZTbIlycV99fd0tV8kGeqrvzTJF5M8kGRrkktnNz1J0lw1nTOoa4Hl/YWqOquqllbVUmAdcMPEQUmOAS4ATgSOB1YkObJrHgXOAO6aMOw9wMuq6ljgTcAfJ1ky3clIkuaPKQOqqu4CnhjUliTAmcCaAc1HAxuqamdV7QLuBE7vtrm1qr4zaHfA/kkWAfsCPwd+Mp2JSJLml9m+B7UMeLSqtg9oGwVOSnJwkv2A04DXTLG9LwNPAz8Cfgh8sqoGhqMkaX5bNMvxKxl89kRVbU1yBbAeGAM2Abum2N6JwHPAocCvA19P8tWq+t7EjklWAasAFi9ezMjIyEznIM3Ii/WaGxsbe9Ff3/57UgtmHFDdZbgz6L1XNFBVXQ1c3fX/U2DHFJt9L/CVqnoWeCzJN4Ah4HkBVVWrgdUAQ0NDNTw8PINZSDP0lVt4sV5zIyMjL9q+gBd1btLuzOYS3ynAtqqaNHSSHNLdH04vzAaebfX5IXByevYH3gxsm8UxSpLmqOksM18D3A0clWRHkvO7prOZEDhJDk1ya19pXZIHgZuAC6vqya7f6Ul2AG8BbklyW9f/M8AB9N6/ugf4QlVtnvn0JElz1ZSX+Kpq5ST1cwfUHqG3GGL8+bJJxt4I3DigPkZvqbkkaYHzkyQkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTZoyoJJck+SxJKN9tbVJNna3h5NsnGTsRUlGk2xJcnFf/T1d7RdJhiaMOS7J3V37A0n2mfn0JElz1aJp9LkWuAr4i/FCVZ01/jjJnwNPTRyU5BjgAuBE4OfAV5LcUlXbgVHgDOC/TBizCLgO+MOq2pTkYODZPZyT9II78OgPcewXP/Ti7fCLL96uDjwa4B0v3g6lSUwZUFV1V5Ilg9qSBDgTOHlA89HAhqra2fW9EzgduLKqtna1iWNOBTZX1aZu3z+e3jSkF9ffb/34jMb94IoVv+Ij2b3XXnLzHo95+b4vfQGORNpz0zmD2p1lwKPdWdFEo8Dl3VnQM8BpwL1TbO/1QCW5DXgV8KWqunJQxySrgFUAixcvZmRkZGYzkGbg2uX7z2zg8jv2eMjY2BgHHHDAzPY3Q/57UgtmG1ArgTWDGqpqa5IrgPXAGLAJ2DWN43kr8JvATuBrSe6rqq8N2P5qYDXA0NBQDQ8Pz3QOUtNGRkbw9a2FaMar+Lr3i84A1k7Wp6qurqoTquok4Alg0JlWvx3AnVX1eHdp8FbghJkeoyRp7prNMvNTgG1VtWOyDkkO6e4PpxdmA8+2+twGHJdkvy4A3wY8OItjlCTNUdNZZr4GuBs4KsmOJOd3TWczIXCSHJrk1r7SuiQPAjcBF1bVk12/05PsAN4C3NK950TX/ingHmAjcH9V3TKbCUqS5qbprOJbOUn93AG1R+gthhh/vmySsTcCN07Sdh29peaSpAXMT5KQJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNSlVtbePYdaS/C3wg719HNIL5JXA43v7IKQX0Gur6lUTi/MioKT5LMm9VTU0dU9pfvESnySpSQaUJKlJBpTUvtV7+wCkvcH3oCRJTfIMSpLUJANKktQkA0pqQJKLk+z3K9rWkiTv/VVsq9vecJKbu8cfSfLBSfY5OsPtP5zklbM9Ts0/BpTmtPTMh9fxxcDAgErykj3c1hLgVxZQ0t4yH/5ha4HpflvfmuSzwP3A1UnuTbIlyUe7PicmuaF7/K4kzyT5tST7JPleVz8iyVeTbEpyf5J/2tX/XZJ7kmzu2974Pj/f7ef2JPt2be9P8mDX/0td7R+caSQZ7baxf5Jbun2OJjkryfuBQ4E7ktzR9R9L8p+SfBN4S5IPd8c0mmR1kuxmDh8HliXZmOQD3X6/3rXfn+Sfd2OHk4wk+XKSbUmu79vu8q72f+h9G3a/45P87yTbk1ww4O9nnyRfSPJAkm8neXtXf0mST3b1zUn+zYRx+yb5yqBtaoGqKm/e5tSN3hnCL4A3d89f0d2/BBgBjqP3ZZzf7+qfpPctzf8CeBuwpqt/Ezi9e7wPvTOYU+kt6w69X+BuBk7q9rkLWNr1/yvgD7rHjwAv6x4f1N1/BPhg3zGPdtv4feDzffWXd/cPA6/sqxdwZt/zV/Q9/kvgd3czh2Hg5r7++wH7dI+PBO7tHg8DTwGHdXO9G3hrt52/6fqmm+vNffPaBOxL7yOY/oZeuC4BRrs+/xb4Qvf4N4Afdtv8V8A6YNGEv7eHu/FfBf5ob7++vLVz8wxKc9UPqmpD9/jMJPcD3wbeCLyhqnYB301yNHAi8Cl6QbMM+HqSA4FXV+/bnamqn1bVTnoBdWq3rfvp/YA9stvP96tqY/f4Pno/VAE2A9cn+QN6IbY7DwCnJLkiybKqemqSfs/R+2E+7u1JvpnkAeBk4I27mcNELwU+343978Ab+tq+VVU7quoXwMZuTr/RzXV7VRXP/4br/1lVz1TV48Ad9P58+72VXohSVdvofU7m64FTgM91fzdU1RP926QXan8xyZ+HFiADSnPV0wBJXgd8EPitqjoOuIXeb+sAXwd+B3iW3m/nb+1ud9E7MxgkwJ9V1dLudkRVXd21/ayv33P0ztIA3gF8BngTcF+SRfSCqv/f1z4AVfVQ1+8B4M+SfHiS4/hpVT3XzXEf4LPAu6vqWODz3fYmm8NEHwAeBY4HhoBf62ubbE67+w+SE9smPt/dn+1k2/0G8DvjlxglMKA09/1jemH1VJLF9AJp3F30Fh/cXVV/CxxM7+xgS1X9BNiR5PcAkrwsvVV0twHnJTmgq786ySGT7bxboPGaqroD+PfAQcAB9C5bndD1OQF4Xff4UGBnVV1H79LjCd2m/h44cJLdjAfu491xvRtgN3OYuK2XAz/qzpL+kN6l0N3ZBrxu/D05YOWE9nd17zMdTO8y4T0T2u8C3tcd0+uBw4HvALcDf9IFOEle0Tfmw8CP6QWxBBhQmuOqahO9y3FbgGvo/SY+7pvAYno/MKF3KW5zd9kKej+s359kM/B/gX9SVbcD/w24u7sk9mUmDw7o/bC/ruv7beDTVfV39C7PvSLJRnrvvTzU9T8W+FZXvwz4WFdfDfz1+CKJCXP8O3pnTQ8A/4N/GAjPm0M3z13dwokP0Puhf06SDfQutT29m/lQVT8FVgG3dIskJn6VzbfonaluAP5zVT0yof2zwEu6P5O1wLlV9TPgv9J7P2pzkk08f6XhxcA+Sa7c3fFp4fCjjiRJTfIMSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUpP8PSh4Cd6zUX5cAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAANkklEQVR4nO3de4yld13H8c9XtxFaGi4WJoLKmNgUknIRRxQvOKRAKkXkooFqFKWy0ZBwSYgsGiRIiEWMREMEN1JaL1TlFrAbyza4h0ZSDNtCYUkb+octqaKFFCtbqmnt1z/2dDtOZ3d2z5yZ+e3O65VM5pzfOc/5/XbT03ee5zz7nOruAMBovmO7FwAAaxEoAIYkUAAMSaAAGJJAATCkXVs52TnnnNOLi4tbOSUM6e67785ZZ5213cuAIVx//fXf6O7Hrh7f0kAtLi7m4MGDWzklDGkymWR5eXm7lwFDqKrb1hp3iA+AIa0bqKq6rKruqKpDK8beXlVfrKovVNX+qnr85i4TgJ3mRPagLk9y4aqxd3X3U7v76UmuSvK7c14XADvcuoHq7muT3Llq7L9W3D0rieslATBXM58kUVXvSPIrSe5K8pzjPG93kt1JsrCwkMlkMuuUcNo4fPiw9wKso07kYrFVtZjkqu4+f43H3pzkYd391vVeZ2lpqZ3FB87ig5Wq6vruXlo9Po+z+D6Y5GVzeB0AOGqmQ3xVdW533zK9+6IkN89vSXD6qqqHjPnKG1jbuoGqqiuTLCc5p6puT/LWJC+oqvOS3J/ktiS/sZmLhNPBWnF6YFyk4KHWDVR3X7zG8Ps3YS2wI3T30c+gjhUtwJUkABiUQAEwpC29WCxw7M+igP/PHhRskWOdCOEECVibQMEW6u50dw4cOHD0NrA2gQJgSAIFwJAECoAhCRQAQxIoAIYkUAAMSaAAGJJAATAkgQJgSAIFwJAECoAhCRQAQxIoAIbk+6BgA7bru51cBZ2dwB4UbMADX5lxsj9PfNNVM28rTuwUAgXAkAQKgCEJFABDEigAhiRQAAxJoAAYkkABMCSBAmBIAgXAkAQKgCEJFABDEigAhiRQAAxJoAAYkkABMCSBAmBIAgXAkAQKgCEJFABDWjdQVXVZVd1RVYdWjL2rqm6uqi9W1ceq6lGbukoAdpwT2YO6PMmFq8auSXJ+dz81yVeSvHnO6wJgh1s3UN19bZI7V43t7+77pnc/m+R7N2FtAOxgu+bwGq9K8rfHerCqdifZnSQLCwuZTCZzmBJOfd4LcHwbClRV/U6S+5L89bGe0917k+xNkqWlpV5eXt7IlHB6uHpfvBfg+GYOVFW9MskLk1zQ3T2/JQHAjIGqqguTvCnJT3f3t+e7JAA4sdPMr0xyXZLzqur2qrokyXuSnJ3kmqr6QlW9b5PXCcAOs+4eVHdfvMbw+zdhLQBwlCtJADAkgQJgSAIFwJAECoAhCRQAQxIoAIYkUAAMSaAAGJJAATAkgQJgSAIFwJAECoAhCRQAQxIoAIYkUAAMSaAAGJJAATAkgQJgSAIFwJAECoAhCRQAQxIoAIYkUAAMSaAAGJJAATAkgQJgSAIFwJAECoAhCRQAQxIoAIYkUAAMSaAAGJJAATAkgQJgSAIFwJAECoAhCRQAQxIoAIYkUAAMad1AVdVlVXVHVR1aMfYLVfXlqrq/qpY2d4kA7EQnsgd1eZILV40dSvLSJNfOe0EAkCS71ntCd19bVYurxm5KkqrapGUBsNOtG6iNqqrdSXYnycLCQiaTyWZPCacE7wU4vk0PVHfvTbI3SZaWlnp5eXmzp4TxXb0v3gtwfM7iA2BIAgXAkE7kNPMrk1yX5Lyqur2qLqmql1TV7UmelWRfVX1ysxcKwM5yImfxXXyMhz4257UAwFEO8QEwJIECYEgCBcCQBAqAIQkUAEMSKACGJFAADEmgABjSpl8sFkb3tLftz1333Lvl8y7u2bflcz7y4Wfkxrc+f8vnhVkIFDveXffcm1svvWhL55xMJttyNfPtiCLMyiE+AIYkUAAMSaAAGJJAATAkgQJgSAIFwJAECoAhCRQAQxIoAIYkUAAMSaAAGJJAATAkgQJgSAIFwJAECoAhCRQAQxIoAIYkUAAMSaAAGJJAATAkgQJgSAIFwJAECoAhCRQAQxIoAIYkUAAMadd2LwC229lP3pOnXLFn6ye+YuunPPvJSXLR1k8MMxAodrxv3XRpbr10a/+nPZlMsry8vKVzJsninn1bPifMyiE+AIa0bqCq6rKquqOqDq0Ye0xVXVNVt0x/P3pzlwnATnMie1CXJ7lw1dieJJ/q7nOTfGp6HwDmZt1Adfe1Se5cNfxzefAj3iuSvHi+ywJgp5v1JImF7v5aknT316rqccd6YlXtTrI7SRYWFjKZTGacEjbPVv93efjw4W17L3gPcqrY9LP4untvkr1JsrS01Ntx5hIc19X7tvyMuu06i287/qwwq1nP4vuPqvqeJJn+vmN+SwKA2QP1iSSvnN5+ZZKPz2c5AHDEiZxmfmWS65KcV1W3V9UlSS5N8ryquiXJ86b3AWBu1v0MqrsvPsZDF8x5LQBwlCtJADAkgQJgSAIFwJAECoAhCRQAQxIoAIYkUAAMSaAAGJJAATAkgQJgSAIFwJAECoAhCRQAQxIoAIYkUAAMSaAAGJJAATAkgQJgSAIFwJAECoAhCRQAQxIoAIa0a7sXACNY3LNv6ye9euvnfOTDz9jyOWFWAsWOd+ulF235nIt79m3LvHAqcYgPgCEJFABDEigAhiRQAAxJoAAYkkABMCSBAmBIAgXAkAQKgCEJFABDEigAhiRQAAxJoAAYkkABMCSBAmBIGwpUVb2uqg5V1Zer6vVzWhMAzB6oqjo/yauTPDPJ05K8sKrOndfCANjZNrIH9eQkn+3ub3f3fUk+neQl81kWADvdRr7y/VCSd1TVdye5J8kLkhxc/aSq2p1kd5IsLCxkMplsYEo4fXgvwPHNHKjuvqmq3pnkmiSHk9yY5L41nrc3yd4kWVpa6uXl5VmnhNPH1fvivQDHt6GTJLr7/d39jO5+dpI7k9wyn2UBsNNt5BBfqupx3X1HVX1/kpcmedZ8lgXATrehQCX5yPQzqHuTvKa7vzmHNQHAxgLV3T81r4UAwEquJAHAkAQKgCEJFABDEigAhiRQAAxJoAAYkkABMCSBAmBIAgXAkAQKgCEJFABDEigAhiRQAAxJoAAYkkABMCSBAmBIAgXAkAQKgCEJFABDEigAhiRQAAxJoAAYkkABMCSBAmBIAgXAkAQKgCEJFABDEigAhiRQAAxJoAAYkkABMCSBAmBIAgXAkAQKgCHt2u4FwKmsqmbf9p2zz9vds28Mpwh7ULAB3T3Tz4EDB2beVpzYKQQKgCEJFABDEigAhrShQFXVG6rqy1V1qKqurKqHzWthAOxsMweqqp6Q5LVJlrr7/CTfmeQV81oYADvbRg/x7Ury8KraleTMJP+28SUBwAb+HVR3/2tV/WGSrya5J8n+7t6/+nlVtTvJ7iRZWFjIZDKZdUo4bRw+fNh7AdZRs/6biqp6dJKPJHl5kv9M8qEkH+7uvzrWNktLS33w4MGZ5oPTyWQyyfLy8nYvA4ZQVdd399Lq8Y0c4ntukn/p7q93971JPprkxzfwegBw1EYC9dUkP1ZVZ9aR671ckOSm+SwLgJ1u5kN8SVJVb8uRQ3z3Jfl8kl/v7v85zvO/nuS2mSeE08c5Sb6x3YuAQTyxux+7enBDgQJmU1UH1zrmDjzIlSQAGJJAATAkgYLtsXe7FwCj8xkUAEOyBwXAkAQKgCEJFMxBVf1eVT13xm1fVFV7jvHY4ROdt6peX1VnzrIGGJHPoGCV6ZVRqrvvH2Ath7v7ESf43Ftz5Otv/ANgTgv2oCBJVS1W1U1V9adJbkjylqr6XFV9cXrFlAee95aqurmqrpl+Secbp+OXV9XPT29fUFWfr6ovVdVlVfVd0/Fbq+ptVXXD9LEnTcd/tareM739A1V13XTut69a429Nt7uxqi5dOW9VvTbJ45McqKoDVXVJVb17xbavrqo/2sy/Q5g3gYIHnZfkL5K8KckTkjwzydOT/HBVPbuqlpK8LMkPJXlpkodcCWL6rdKXJ3l5dz8lR77S5jdXPOUb3f2MJO9N8sY11vDHSd7b3T+S5N9XvO7PJHlxkh/t7qcl+YOVG3X3n+TI97E9p7ufk+Rvkryoqs6YPuXXknzgRP8iYAQCBQ+6rbs/m+T505/P58je1JOSnJvkJ5N8vLvv6e5vJfn7NV7jvBy5yv9XpvevSPLsFY9/dPr7+iSLa2z/E0munN7+yxXjz03yge7+dpJ0953H+4N0991J/jHJC6d7amd095eOtw2MZuYvLITT0N3T35Xk97v7z1Y+WFVvOIHXqHUef+Biyv+bY7//1vpguI4xfjx/nuS3k9wce0+cguxBwUN9MsmrquoRSVJVT6iqxyX5pyQ/W1UPmz520Rrb3pxksap+cHr/l5N8+iTm/kySV0xv/9KK8f3TNZ05XdNj1tj2W0nOfuBOd/9zku9L8ot5cK8MThkCBat09/4kH0xyXVV9KcmHk5zd3Z9L8okkN+bIobqDSe5ate1/58jnPR+abnt/kvedxPSvS/KaqvpckkeueN2rp3MfrKovZO3Pr/Ym+YeqOrBi7O+SfKa7v3kSa4AhOM0cTkJVPaK7D0/3ZK5Nsru7b9judR1LVV2V5N3d/antXgucLHtQcHL2TvdgbkjykVHjVFWPqqqvJLlHnDhV2YMCYEj2oAAYkkABMCSBAmBIAgXAkAQKgCH9H3u1cs19ZOHGAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAOCklEQVR4nO3df4xlZ13H8c9XtxEspAKFCUJgNDZtsZRfI0lDrWMQLBRRfgUbf1QhbBSjYGLCokKphFjCHyqiwCbUVoWVCCK1m5Q2yNCUtIFtKXWbVppAq03Q2pQUthSk8vjH3jaTYXZneufOzHd3Xq9kM/c+957zPGeY23fOmcudGmMEALr5ge1eAACsRqAAaEmgAGhJoABoSaAAaGnXVk528sknj/n5+a2cElq6//77c+KJJ273MqCFG2644Z4xxhNXjm9poObn53PgwIGtnBJaWlpayuLi4nYvA1qoqjtXG3eJD4CWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWtvSjjuB4U1XbMq+/hM1O4AwKNmCMMdW/p7/liqm3FSd2CoECoCWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWBAqAlgQKgpTUDVVWXVNXdVXVw2dg7q+rmqrqpqq6qqh/d3GUCsNOs5wzq0iTnrhh7zxjjzDHGs5NckeTtM14XADvcmoEaY1yT5N4VY99YdvfEJGPG6wJgh9s17YZV9a4kv57kviQ/e5Tn7U6yO0nm5uaytLQ07ZRwXPFagKOrMdY++amq+SRXjDHOWOWxtyZ51BjjwrX2s7CwMA4cODDNOuG4Mr9nf+64+LztXga0UFU3jDEWVo7P4l18H0nyqhnsBwAeNlWgquqUZXdfnuS22SwHAA5b83dQVbUvyWKSk6vqriQXJnlpVZ2a5HtJ7kzyW5u5SAB2njUDNcY4f5XhD23CWgDgYT5JAoCWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWBAqAltb8i7pwvHvWRVflvge+u+Xzzu/Zv+VznvToE/KlC1+85fPCNASKHe++B76bOy4+b0vnXFpayuLi4pbOmWxPFGFaLvEB0JJAAdCSQAHQkkAB0JJAAdCSQAHQkkAB0JJAAdCSQAHQkkAB0JJAAdCSQAHQkkAB0JJAAdCSQAHQkkAB0JJAAdCSQAHQkkAB0JJAAdDSmoGqqkuq6u6qOrhs7D1VdVtV3VxVn6iqH9nUVQKw46znDOrSJOeuGLs6yRljjDOTfDnJW2e8LgB2uDUDNca4Jsm9K8auGmM8OLl7fZKnbsLaANjBds1gH69L8tEjPVhVu5PsTpK5ubksLS3NYEqYra3+uTx06NC2vRa8BjlWbChQVfVHSR5M8uEjPWeMsTfJ3iRZWFgYi4uLG5kSZu/K/dnqn8ulpaUtnzPJthwrTGvqQFXVBUleluSFY4wxuyUBwJSBqqpzk7wlyc+MMb412yUBwPreZr4vyXVJTq2qu6rq9Unel+SxSa6uqpuq6gObvE4Adpg1z6DGGOevMvyhTVgLADzMJ0kA0JJAAdCSQAHQkkAB0JJAAdCSQAHQkkAB0JJAAdCSQAHQ0iz+3AYc0x57+p4887I9Wz/xZVs/5WNPT5Lztn5imIJAseN989aLc8fFW/sf7e36cxvze/Zv+ZwwLZf4AGhJoABoSaAAaEmgAGhJoABoSaAAaEmgAGhJoABoSaAAaEmgAGhJoABoSaAAaEmgAGhJoABoSaAAaEmgAGhJoABoSaAAaEmgAGhJoABoSaAAaEmgAGhJoABoSaAAaEmgAGhJoABoSaAAaEmgAGhpzUBV1SVVdXdVHVw29pqquqWqvldVC5u7RAB2ovWcQV2a5NwVYweTvDLJNbNeEAAkya61njDGuKaq5leM3ZokVbVJywJgp/M7KABaWvMMaqOqaneS3UkyNzeXpaWlzZ4SHrGt/rk8dOjQtr0WvAY5Vmx6oMYYe5PsTZKFhYWxuLi42VPCI3Pl/mz1z+XS0tKWz5lkW44VpuUSHwAtredt5vuSXJfk1Kq6q6peX1WvqKq7kpyVZH9VfWqzFwrAzrKed/Gdf4SHPjHjtQDAw1ziA6AlgQKgJYECoCWBAqAlgQKgJYECoCWBAqClTf+oIzgWzO/Zv/WTXrn1c5706BO2fE6YlkCx491x8XlbPuf8nv3bMi8cS1ziA6AlgQKgJYECoCWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWBAqAlgQKgJYECoCWBAqClNQNVVZdU1d1VdXDZ2OOr6uqqun3y9XGbu0wAdpr1nEFdmuTcFWN7knx6jHFKkk9P7gPAzKwZqDHGNUnuXTH8i0kum9y+LMkvzXZZAOx0u6bcbm6M8bUkGWN8raqedKQnVtXuJLuTZG5uLktLS1NOCccXrwU4umkDtW5jjL1J9ibJwsLCWFxc3Owpob8r98drAY5u2nfx/XdVPTlJJl/vnt2SAGD6QF2e5ILJ7QuSfHI2ywGAw9bzNvN9Sa5LcmpV3VVVr09ycZIXVdXtSV40uQ8AM7Pm76DGGOcf4aEXzngtAPAwnyQBQEsCBUBLAgVASwIFQEsCBUBLAgVASwIFQEsCBUBLAgVASwIFQEsCBUBLAgVASwIFQEsCBUBLAgVASwIFQEsCBUBLAgVASwIFQEsCBUBLAgVASwIFQEsCBUBLAgVASwIFQEsCBUBLAgVASwIFQEsCBUBLAgVASwIFQEsCBUBLAgVASwIFQEsCBUBLAgVASwIFQEsCBUBLAgVASxsKVFW9qaoOVtUtVfXmGa0JAKYPVFWdkeQNSZ6f5FlJXlZVp8xqYQDsbBs5gzo9yfVjjG+NMR5M8tkkr5jNsgDY6XZtYNuDSd5VVU9I8kCSlyY5sPJJVbU7ye4kmZuby9LS0gamhOOH1wIc3dSBGmPcWlXvTnJ1kkNJvpTkwVWetzfJ3iRZWFgYi4uL004Jx48r98drAY5uQ2+SGGN8aIzx3DHGOUnuTXL7bJYFwE63kUt8qaonjTHurqqnJXllkrNmsywAdroNBSrJxye/g/pukt8ZY3x9BmsCgI0Faozx07NaCAAs55MkAGhpo5f4YEerqum3fff0844xpt8YjhHOoGADxhhT/fvMZz4z9bbixE4hUAC0JFAAtCRQALQkUAC0JFAAtCRQALQkUAC0JFAAtCRQALQkUAC0VFv5sSlV9T9J7tyyCaGvk5Pcs92LgCaePsZ44srBLQ0UcFhVHRhjLGz3OqAzl/gAaEmgAGhJoGB77N3uBUB3fgcFQEvOoABoSaAAaEmgOK5V1Z9U1c9Nue3Lq2rPER47tJlrqqp3VNUfrDI+X1UHp517so87qurkjexjK/fLzrVruxcA61VVlcO/N/3eercZY7x92vnGGJcnuXza7Y+y36nXtFFV9YPbNTc8Us6gaG1yxnBrVf11khuTvK2qvlBVN1fVRcue97aquq2qrq6qfQ+dfVTVpVX16sntF1bVF6vq36rqkqr6ocn4HVV1UVXdOHnstMn4b1TV+ya3f6yqrpvM/c5l8y5W1VJVfWwy/4cnIU1VPa+qPltVN1TVp6rqyaus6aWT7a6tqvdW1RXLDv8Zk31/pap+b9n4rqq6bPI9+FhV/fA6ju/tVXVtktdM9vG7qxzv46vqnyf7vb6qzlxj/AlVddVkzg8mqQ3/Dw7LCBTHglOT/G2StyR5SpLnJ3l2kudV1TlVtZDkVUmek+SVSb7vExqq6lFJLk3y2jHGM3P46sFvL3vKPWOM5yZ5f5Lvu7SW5C+SvH+M8VNJ/mvFY89J8uYkz0jy40leUFUnJPnLJK8eYzwvySVJ3rXKmj6Y5CVjjLOTrPyol9OS/PzkeC+c7POh78feMcaZSb6R5I3rOL5vjzHOHmP8w1GO96IkX5zs9w9z+Ht+tPELk1w7xnhODp9pPm2V7xtMTaA4Ftw5xrg+yYsn/76Yw2dTpyU5JcnZST45xnhgjPHNJP+yyj5OTfLVMcaXJ/cvS3LOssf/afL1hiTzq2z/giT7Jrf/bsVjnx9j3DW59HjTZPtTk5yR5OqquinJHyd56ortTkvylTHGVyf39614fP8Y4ztjjHuS3J1kbjL+n2OMz01u/30OH/9ax/fRFfte7XjPfujYxhj/muQJVXXSUcbPmcyfMcb+JF8PzJDfQXEsuH/ytZL86Rjjg8sfrKrfX8c+1rr89J3J1//LkV8XR/o/DX5n2e2Htq8kt4wxzprBmlaua+U6xjr2df+K+6sd72r7ONK+x4qvMHPOoDiWfCrJ66rqMUlSVU+pqicluTbJL1TVoyaPnbfKtrclma+qn5jc/7Ukn30Ec38uyS9Pbv/KOp7/70meWFVnTdZ6QlX95Cpr+vGqmp/cf+061/K0h/ab5PwcPv6NHl+SXJPJsVXVYg5fBvzGOsdfkuRxj3A+OCpnUBwzxhhXVdXpSa6bvA/hUJJfHWN8oaouT/KlHP5zLgeS3Ldi229X1W8m+ceq2pXkC0k+8Aimf1OSj1TVm5J8fB1r/d/JGyHeO7kctivJnye5ZdlzHqiqNya5sqruSfL5da7l1iQXTN6YcHsO/25so8eXJO9I8jdVdXOSbyW5YI3xi5Lsq6obcziG//EI54Oj8lFHHBeq6jFjjEOTd7Rdk2T3GOPG7V7XWpatu5L8VZLbxxh/tt3rgg5c4uN4sXfyZoQbk3z8WIjTxBsm674lyUk5/K4+IM6gAGjKGRQALQkUAC0JFAAtCRQALQkUAC39P+IRpPnrPcCqAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAQ/UlEQVR4nO3df6zddX3H8edL6oYIignuJqVmxc2UkQrozoiIwZvVma52GNmMNGpwkDRZnDKDGRLCmC5xSzDbSNhkN6NUZ9cxGc2cdtIm46xxK0iLtRTbESMTr50pjvjjAhtU3vujx3B3Obfncn/0fnrP85Hc3PN9fz+f7/d9ku/tK9/z/fZ7UlVIktSalyx2A5Ik9WNASZKaZEBJkppkQEmSmmRASZKatGyxG+jnrLPOqpUrVy52G9IJ9eSTT/Lyl798sduQTri9e/d+v6pePbXeZECtXLmSPXv2LHYb0gnV7XYZHR1d7DakEy7Jt/vV/YhPktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktSkJv8flDRMkryg5tfgSJ5BSYuqXzgdry4NEwNKakBVce+993rmJE1iQEmSmmRASZKa5E0SUgO85iS9kGdQ0iKa7pqT16IkA0padFX1/26SMJykYwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTBgZUkk1JjiQ5MKl2c5JDSfYn2ZbkzGnmfiTJw0kOJNma5NR57F2StITN5AxqM7B2Sm0nsLqqzgceAa6fOinJ2cCHgU5VrQZOAa6YU7eSpKExMKCqahfwxJTajqo62lu8D1gxzfRlwMuSLANOAw7PoVdJ0hCZj4fFXgXcObVYVd9N8ingMeBpYEdV7ZhuI0k2AhsBRkZG6Ha789CadPKYmJjwuJcmmVNAJbkBOAps6bPuVcA7gXOAHwCfT/K+qvpcv21V1RgwBtDpdGp0dHQurUknnW63i8e99LxZ38WX5EpgPfDe6v90y7cBj1bV41X1LHA38ObZ7k+SNFxmFVBJ1gLXAZdV1VPTDHsMeFOS03Lsy27WAAdn16YkadjM5DbzrcBuYFWS8SRXA7cCZwA7k+xLcltv7PIk2wGq6n7gLuBB4KHevsYW5m1IkpaagdegqmpDn/Lt04w9DKybtHwTcNOsu5MkDS2fJCFJapIBJUlqkgElSWqSASVJapIBJUlqkgElSWqSASVJapIBJUlqkgElSWqSASVJapIBJUlqkgElSWqSASVJapIBJUlqkgElSWqSASVJapIBJUlqkgElSWqSASVJapIBJUlqkgElSWqSASVJapIBJUlq0sCASrIpyZEkBybVbk5yKMn+JNuSnDnN3DOT3NUbezDJxfPYuyRpCZvJGdRmYO2U2k5gdVWdDzwCXD/N3FuAL1fVucAFwMFZ9ilJGjIDA6qqdgFPTKntqKqjvcX7gBVT5yV5BXApcHtvzjNV9YO5NixJGg7L5mEbVwF39qm/FngcuCPJBcBe4JqqerLfRpJsBDYCjIyM0O1256E16eQxMTHhcS9NkqoaPChZCXyxqlZPqd8AdIDLa8qGknQ4dnZ1SVXdn+QW4EdVdeOg/XU6ndqzZ8/M34W0BHS7XUZHRxe7DemES7K3qjpT67O+iy/JlcB64L1Tw6lnHBivqvt7y3cBb5zt/iRJw2VWAZVkLXAdcFlVPdVvTFV9D/hOklW90hrgG7PqUpI0dGZym/lWYDewKsl4kquBW4EzgJ1J9iW5rTd2eZLtk6Z/CNiSZD9wIfDJ+X4DkqSlaeBNElW1oU/59mnGHgbWTVrex7FrVJIkvSg+SUKS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUpIEBlWRTkiNJDkyq3ZzkUJL9SbYlOfM4809J8rUkX5ynniVJQ2AmZ1CbgbVTajuB1VV1PvAIcP1x5l8DHJxVd5KkoTUwoKpqF/DElNqOqjraW7wPWNFvbpIVwDuAv55jn5KkIbNsHrZxFXDnNOv+HPh94IxBG0myEdgIMDIyQrfbnYfWpJPHxMSEx700yZwCKskNwFFgS59164EjVbU3yeigbVXVGDAG0Ol0anR04BRpSel2u3jcS8+bdUAluRJYD6ypquoz5BLgsiTrgFOBVyT5XFW9b7b7lCQNj1ndZp5kLXAdcFlVPdVvTFVdX1UrqmolcAXwL4aTJGmmZnKb+VZgN7AqyXiSq4FbOXZdaWeSfUlu641dnmT7gnYsSRoKAz/iq6oNfcq3TzP2MLCuT70LdF9kb5KkIeaTJCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNGhhQSTYlOZLkwKTazUkOJdmfZFuSM/vMe02Se5McTPJwkmvmuXdJ0hI2kzOozcDaKbWdwOqqOh94BLi+z7yjwLVV9UvAm4APJjlvDr1KkobIwICqql3AE1NqO6rqaG/xPmBFn3n/VVUP9l7/GDgInD3njiVJQ2HZPGzjKuDO4w1IshJ4A3D/ccZsBDYCjIyM0O1256E16eQxMTHhcS9NMqeASnIDxz7K23KcMacD/wD8XlX9aLpxVTUGjAF0Op0aHR2dS2vSSafb7eJxLz1v1gGV5EpgPbCmqmqaMS/lWDhtqaq7Z7svSdLwmVVAJVkLXAe8taqemmZMgNuBg1X1p7NvUZI0jGZym/lWYDewKsl4kquBW4EzgJ1J9iW5rTd2eZLtvamXAO8HfrU3Zl+SdQvzNiRJS83AM6iq2tCnfPs0Yw8D63qvvwJkTt1JkoaWT5KQJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDVpYEAl2ZTkSJIDk2o3JzmUZH+SbUnOnGbu2iT/keSbST42j31Lkpa4mZxBbQbWTqntBFZX1fnAI8D1UyclOQX4C+DXgfOADUnOm1O3kqShMTCgqmoX8MSU2o6qOtpbvA9Y0WfqRcA3q+pbVfUM8HfAO+fYryRpSMzHNairgH/uUz8b+M6k5fFeTZKkgZbNZXKSG4CjwJZ+q/vU6jjb2ghsBBgZGaHb7c6lNemkMzEx4XEvTTLrgEpyJbAeWFNV/YJnHHjNpOUVwOHptldVY8AYQKfTqdHR0dm2Jp2Uut0uHvfS82b1EV+StcB1wGVV9dQ0wx4AXpfknCQ/A1wBfGF2bUqShs1MbjPfCuwGViUZT3I1cCtwBrAzyb4kt/XGLk+yHaB3E8XvAvcAB4G/r6qHF+h9SJKWmIEf8VXVhj7l26cZexhYN2l5O7B91t1JkoaWT5KQJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNcmAkiQ1yYCSJDXJgJIkNWlgQCXZlORIkgOTau9O8nCS55J0jjP3I71xB5JsTXLqfDUuSVraZnIGtRlYO6V2ALgc2DXdpCRnAx8GOlW1GjgFuGJ2bUqShs2yQQOqaleSlVNqBwGSzGT7L0vyLHAacHh2bUqShs3AgJqtqvpukk8BjwFPAzuqasd045NsBDYCjIyM0O12F6o1qUkTExMe99IkCxZQSV4FvBM4B/gB8Pkk76uqz/UbX1VjwBhAp9Op0dHRhWpNalK328XjXnreQt7F9zbg0ap6vKqeBe4G3ryA+5MkLSELGVCPAW9KclqOXaxaAxxcwP1JkpaQmdxmvhXYDaxKMp7k6iTvSjIOXAx8Kck9vbHLk2wHqKr7gbuAB4GHevsaW6D3IUlaYmZyF9+GaVZt6zP2MLBu0vJNwE2z7k6SNLR8koQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQMDKsmmJEeSHJhUe3eSh5M8l6RznLlnJrkryaEkB5NcPF+NS5KWtpmcQW0G1k6pHQAuB3YNmHsL8OWqOhe4ADj4YhuUJA2nZYMGVNWuJCun1A4CJJl2XpJXAJcCH+jNeQZ4ZvatSpKGycCAmoPXAo8DdyS5ANgLXFNVT/YbnGQjsBFgZGSEbre7gK1J7ZmYmPC4lyZZyIBaBrwR+FBV3Z/kFuBjwI39BlfVGDAG0Ol0anR0dAFbk9rT7XbxuJeet5B38Y0D41V1f2/5Lo4FliRJAy1YQFXV94DvJFnVK60BvrFQ+5MkLS0zuc18K7AbWJVkPMnVSd6VZBy4GPhSknt6Y5cn2T5p+oeALUn2AxcCn5z3dyBJWpJmchffhmlWbesz9jCwbtLyPmDa/yclSdJ0fJKEJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQv5hYXSUEpywvdZVSd8n9JCS4sHdqfTqT179ix2GxpyF3x8Bz98+tnFbmNBvfJlL+XrN719sdvQkEuyt6pe8M0XnkFJ03hu5bWcsdhNLLDnAHhokbuQ+jOgpGk8dOWJ/Ye72+0yOjp6QvcptcybJCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTWryYbFJHge+vdh9SCfYWcD3F7sJaRH8fFW9emqxyYCShlGSPf2e6CwNKz/ikyQ1yYCSJDXJgJLaMbbYDUgt8RqUJKlJnkFJkppkQEmSmmRASQsoySeSvG2Wcy9L8rFp1k0MmPvvs9mn1BKvQUkzlCQc+5t5roFeJqrq9MXuQ1pInkFJx5FkZZKDSf4SeBC4MckDSfYn+fikcTcmOZRkZ5KtST7aq29O8lu912uSfC3JQ0k2JfnZXv0/k3w8yYO9def26h9Icmvv9TlJdvf2/UeT9vuJJPt6P99NckevPtH7PZpkV5JtSb6R5LYk/t3rpOCBKg22CvgscB1wNnARcCHwy0kuTdIBfhN4A3A58IKnQSQ5FdgMvKeqXg8sA35n0pDvV9UbgU8DH+3Twy3Ap6vqV4Dv/bRYVX9QVRcCbwX+G7i1z9yLgGuB1wO/0OtRap4BJQ327aq6D3h77+drHDubOhd4HfAW4B+r6umq+jHwT322sQp4tKoe6S1/Brh00vq7e7/3Aiv7zL8E2Np7/TeTV/Q+etwC/FlV7e0z96tV9a2q+klvG285znuVmrFssRuQTgJP9n4H+OOq+qvJK5N8ZAbbyID1/9v7/ROm/7uc7oLxHwLjVXXHDOd54VknBc+gpJm7B7gqyekASc5O8nPAV4DfSHJqb907+sw9BKxM8ou95fcD//oi9v1vwBW91+/9aTHJeuDXgA8fZ+5FvWtYLwHe0+tXap4BJc1QVe0A/hbYneQh4C7gjKp6APgC8HWOfVS3B/jhlLn/A/w28Pne3OeA217E7q8BPpjkAeCVk+rXAsuBr/ZulPhEn7m7gT8BDgCPAttexH6lReNt5tI8SHJ6VU0kOQ3YBWysqgcb6GsU+GhVrV/kVqQXzWtQ0vwYS3IecCrwmRbCSTrZeQYlSWqS16AkSU0yoCRJTTKgJElNMqAkSU0yoCRJTfo/SqkkTmltubgAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n",
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAQ9ElEQVR4nO3df2xdZ33H8ffXduqWtgilaS6BIjIYJXJaFaorNoI2GdimZqWJpv3TdqtEhWRpE1GgPyaYNE2b1L8Q60pBYxGjII21mhhR18JG0ZbLD6Vkc5Ju+clEGRGlNDStOtrKcZz4uz/sBDuNc89NfO59rv1+SdH1OT73+tM/jj59nvOccyMzkSSpNAO9DiBJ0rlYUJKkIllQkqQiWVCSpCJZUJKkIg3V8aGrVq3KtWvX1vHRUt969dVXufzyy3sdQyrO7t27j2Xm1Wfvr6Wg1q5dy/j4eB0fLfWtVqvF6Ohor2NIxYmII+fa7xSfJKlIFpQkqUgWlCSpSG0LKiLeGRFPzfn3i4j4WBeySZKWsbaLJDLzB8C7ACJiEPgpsL3eWJKk5a7TKb4PAk9n5jlXXEiStFg6LahbgYfrCCJJ0lyV74OKiEuATcAnF/j9GDAG0Gg0aLVai5FP6nsPPPAAX//615mammLFihXcfPPNbN26tdexpOJ1cqPuRmBPZh491y8zcxuwDaDZbKY3JEqwZcsWHn30UQYGZiYrpqenefTRR7nmmmt48MEHe5xOKltU/cLCiHgE+GZmPtTu2GazmT5JQoKhoSEyk0996lOMjIxw8OBB7r33XiKCkydP9jqeVISI2J2ZzbP3V7oGFRGvA34b+NpiB5OWslOnTnHfffdx1113cemll3LXXXdx3333cerUqV5Hk4pXeQTVCUdQ0oyI4LLLLmNiYuLMvtPbdZx7Uj+6qBGUpAs3MTFBo9HgoYceotFozCsrSQur5WnmkuY7duwYd955J4ODg72OIvUNR1BSze644w6Ghmb+X3BoaIg77rijx4mk/mBBSTXbs2cPx48fZ8eOHRw/fpw9e/b0OpLUFywoqUYrV67kwIEDXHfddTz33HNcd911HDhwgJUrV/Y6mlQ8r0FJNXrhhRe46qqrOHDgALfddhswU1ovvPBCj5NJ5XMEJdXs9ttvZ3h4GIDh4WFuv/32HieS+oMjKKlGW7Zs4bOf/eyZ7cnJyTPbPupIOj9HUFKNTpfR+vXrefjhh1m/fv28/ZIWZkFJNVu3bh379+/njW98I/v372fdunW9jiT1Baf4pJodO3aMiDizvWrVqh6mkfqHIyipZseOHTvvtqRzs6AkSUWyoCRJRbKgpJqtW7eOzGTHjh1kposkpIosKKlmhw8fZvPmzbz00kts3ryZw4cP9zqS1Bf8wkKpRkNDQ+f89tzBwUG/8l2a5RcWSj0wMjLS0X5Jv+R9UFKNDh06xPDwMNPT00xNTbFixQoGBgY4dOhQr6NJxXMEJdXo5MmTbN26lWuvvZaBgQGuvfZatm7d6vSeVIHXoKQaRcQ5R1CTk5PUce5J/chrUFKPTE5OsnHjRrZv387GjRuZnJzsdSSpLziCkmoUEVxyySVk5pkRVERw4sQJR1DSLEdQUo+sWbOGqakpAKamplizZk2PE0n9oVJBRcQbIuKrEXE4Ig5FxHvrDiYtFUeOHGHTpk1s376dTZs2ceTIkV5HkvpCpSm+iPgy8N3M/EJEXAK8LjNfWuh4p/ikGXO/ZuNsTvFJMxaa4mt7H1REvB74TeDDAJl5Ajix2AElSZqryo26bwOeBx6KiBuA3cDWzHx17kERMQaMATQaDVqt1iJHlfpXRJCZZ14BzxGpjbZTfBHRBL4PvC8zd0XEA8AvMvPPFnqPU3zSjNNTfBs2bODjH/84999/Pzt37gSc4pNOu+ApPuAZ4JnM3DW7/VXgE4sZTlrKVq5cyZNPPsnOnTuJCFauXMmLL77Y61hS8dqu4svM54CfRMQ7Z3d9EDhYayppCXnxxRdZvXo1EcHq1astJ6miqg+L3QJ8ZXYF34+AO+uLJC09R48enfcqqb1KBZWZTwGvmR+UdH7Dw8PnfLTR8PBwD9JI/cUnSUg1mpycZMOGDWcKaXh4mA0bNvg8PqkCC0qq2cTEBCdOzNw6eOLECSYmJnqcSOoPFpRUs71793LLLbewfft2brnlFvbu3dvrSFJf8Bt1pS7YtWsXjz32GKtXr+51FKlvOIKSajYyMsLRo0fJTI4ePcrIyEivI0l9wRGUVLNDhw7x6U9/mpGREQ4ePMg999zT60hSX7CgpBqdfvbevffey/T0NAMDA2eeySfp/Jzik2p0+nl709PT8159Dp/UngUldcHg4OC8V0ntWVBSF5w6dWreq6T2LChJUpEsKKkLBgYG5r1Kas+zReqCsxdJSGrPgpIkFcmCkiQVyYKSJBXJgpIkFcmCkiQVyYKSJBXJgpIkFcmCkiQVyYKSanb11Vefd1vSufl9UFLNnn/++XlfWHj33Xf3OpLUFywoqUbXX389+/bte00pXX/99T1KJPWPSlN8EfHjiNgXEU9FxHjdoaSlYs2aNR3tl/RLnYyg3p+Zx2pLIi1BTzzxBPDLr34//Xp6v6SFuUhC6gK/bkPqXNURVAJPREQCf5uZ284+ICLGgDGARqNBq9VatJBSv8vMea+A54jURsw9YRY8KOJNmflsRKwGvgVsyczvLHR8s9nM8XEvVUkRseDvqpx70nIQEbszs3n2/krzDZn57Ozrz4HtwHsWN54kSfO1LaiIuDwirjz9M/A7wP66g0mSlrcq16AawPbZqYoh4B8y819rTSVJWvbaFlRm/gi4oQtZJEk6wzWvkqQiWVCSpCJZUJKkIllQkqQiWVCSpCJZUJKkIllQkqQiWVCSpCJZUJKkIllQkqQiWVCSpCJZUJKkIllQkqQiWVCSpCJZUJKkIllQkqQiWVCSpCJZUJKkIllQkqQiWVCSpCJZUJKkIllQkqQiWVCSpCJZUJKkIlUuqIgYjIi9EfF4nYEkSYLORlBbgUN1BZEkaa5KBRUR1wA3A1+oN44kSTOGKh7318CfAFcudEBEjAFjAI1Gg1ardbHZpCXNc0Q6v8jM8x8Q8SHgdzPzjyNiFLgnMz90vvc0m80cHx9ftJBSv4qIBX/X7tyTlouI2J2ZzbP3V5niex+wKSJ+DDwCfCAi/n6R80mSNE/bgsrMT2bmNZm5FrgV+PfM/MPak0mSljXvg5IkFanqIgkAMrMFtGpJIknSHI6gJElFsqAkSUWyoCRJRbKgJElFsqAkSUWyoCRJRbKgJElFsqAkSUWyoCRJRbKgJElFsqAkSUWyoCRJRbKgJElFsqAkSUWyoCRJRbKgJElFsqAkSUWyoCRJRbKgJElFsqAkSUWyoCRJRbKgJElFsqAkSUWyoCRJRWpbUBFxaUT8R0T8V0QciIi/6EYwSdLyVmUENQl8IDNvAN4F3BQRv15rKmkJuvHGG3sdQeorQ+0OyMwEXpndXDH7L+sMJS1Fe/bs6XUEqa+0LSiAiBgEdgO/CnwuM3ed45gxYAyg0WjQarUWMaa09HiOSOcXMwOkigdHvAHYDmzJzP0LHddsNnN8fPzi00l9LiIW/F0n5560lEXE7sxsnr2/o1V8mfkS0AJuWpxYkiSdW5VVfFfPjpyIiMuA3wIO15xLkrTMVbkGtQb48ux1qAHgHzPz8XpjSZKWuyqr+P4beHcXskiSdIZPkpC65NZbb+11BKmvWFBSlzzyyCO9jiD1FQtKklQkC0qSVCQLSpJUJAtKklQkC0rqkoEBTzepE54xUpdMT0/3OoLUVywoSVKRLChJUpEsKKlL3v72t/c6gtRXLCipS55++uleR5D6igUldcmVV17Z6whSX7GgpC45fvx4ryNIfcWCkrpkamqq1xGkvmJBSZKKZEFJXeI1KKkzFpTUJS+//HKvI0h9xYKSJBXJgpIkFcmCkiQVyYKSJBXJgpIkFcmCkiQVqW1BRcRbImJHRByKiAMRsbUbwaSlJDPZsWMHmdnrKFLfGKpwzEng7szcExFXArsj4luZebDmbJKkZaxtQWXmz4Cfzf78ckQcAt4MWFBSRRHR6whS36kygjojItYC7wZ2neN3Y8AYQKPRoNVqLUI8aenyHJHOL6rOiUfEFcC3gfsy82vnO7bZbOb4+PgixJP62+mRU2bSarUYHR2dt08SRMTuzGyevb/SKr6IWAH8E/CVduUkSdJiqLKKL4C/Aw5l5l/VH0mSpGrXoN4H3AHsi4inZvf9aWZ+o7ZU0hLjIgmpc1VW8X0P8OySJHWVT5KQusAbdaXOWVCSpCJZUJKkInV0o66kC+MiCalzjqCkGi10zclrUVJ7FpRUo4VGTo6opPYsKKkLXMUndc6CkiQVyYKSJBXJVXxSF3jNSeqcIyipRq7iky6cBSVJKpIFJdXIZebShbOgpC5wmbnUOQtKklQkC0qSVCSXmUtd4DUnqXOOoKQaucxcunAWlFSzzJy3SMJykqqxoCRJRfIalFSzc11/chQltecISqqRN+pKF86CkrrAG3WlzrUtqIj4YkT8PCL2dyOQJElQbQT1JeCmmnNIkjRP20USmfmdiFjbhSzSkuU1J6lzXoOSauSNutKFW7Rl5hExBowBNBoNWq3WYn201Nd27NgBwCuvvMIVV1wB4PkhVbBoBZWZ24BtAM1mM0dHRxfro6UlodVq4XkhVecUnySpSFWWmT8MPAm8MyKeiYiP1B9LkrTcVVnFd1s3gkiSNJdTfJKkIllQkqQi+TRzqWY+zVy6MI6gpBr5NHPpwllQUhf4NHOpcxaUJKlIFpQkqUgukpC6wGtOUuccQUk18mnm0oWzoKSaZea8RRKWk1SNBSVJKpIFJUkqkgUlSSqSBSVJKpIFJUkqkgUlSSqSBSVJKpIFJUkqkgUlSSqSBSVJKpIFJUkqkgUlSSqSBSVJKpIFJUkqUqWCioibIuIHEfHDiPhE3aEkSWpbUBExCHwO2AiMALdFxEjdwSRJy1uVEdR7gB9m5o8y8wTwCLC53liSpOVuqMIxbwZ+Mmf7GeDXzj4oIsaAMYBGo0Gr1VqMfNKi23JkS+/++Jd782cffOuDvfnD0kWoUlBxjn2v+c7qzNwGbANoNps5Ojp6ccmkmuxjX0/+bqvVwvNCqq7KFN8zwFvmbF8DPFtPHEmSZlQpqP8E3hERvxIRlwC3Av9cbyxJ0nLXdoovM09GxEeBbwKDwBcz80DtySRJy1qVa1Bk5jeAb9ScRZKkM3yShCSpSBaUJKlIFpQkqUgWlCSpSBaUJKlIFpQkqUiR+ZqnFl38h0Y8DxxZ9A+W+tsq4FivQ0gFemtmXn32zloKStJrRcR4ZjZ7nUPqF07xSZKKZEFJkopkQUnds63XAaR+4jUoSVKRHEFJkopkQUmSimRBSTWIiGZEfGb259GI2HARn/XhiHjT4qWT+kOl74OS1JnMHAfGZzdHgVeAnRf4cR8G9gPPXnQwqY+4SEKqICLWAo9n5nWz2/cAVzBTPruA9wNvAD6Smd+NiFHgHuCjwPeBU8DzwBbgf4DPA2+b/fg/YqZ8/gX4HrAB+CmwGbgZ+NLs9gTw3sycqO+/VCqHU3zSxRvKzPcAHwP+fO4vMvPHzJTR/Zn5rsz8LvAZ4NuZeQNwI3Bg9vB3AJ/LzPXAS8DvZ+ZXmRmJ/cHs+y0nLRsWlHTxvjb7uhtYW+H4DwB/A5CZpzLz/2b3/29mPtXhZ0lLlgUlVXOS+efLpXN+npx9PcXFXdednPPzxX6W1PcsKKmao8DqiLgqIoaBD3Xw3peBK+ds/xsz152IiMGIeH2H75eWBQtKqiAzp4C/ZGZBxOPA4Q7e/hjwexHxVET8BrAVeH9E7GNmKm99m/d/Cfj87Psv6zi81KdcxSdJKpIjKElSkSwoSVKRLChJUpEsKElSkSwoSVKRLChJUpEsKElSkf4fwhWqCKdHfL4AAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAASl0lEQVR4nO3df5RcdX3G8efZzZpfBu0BnHrUmtoCNUtA4sjRpHgStGAJTVRafljb2mr2WD0xeMBCj8eDtqUHRIpobQlCqT1q5OfaKVrA2owoEXQ3xpBfYqvBIjVKNcgmGOLm0z/mZtks2Z07ZO7Md3ber3P27OydOzPPAHMevne+93sdEQIAIDU97Q4AAMDhUFAAgCRRUACAJFFQAIAkUVAAgCTNKOJJjznmmJg/f34RTw10rD179mju3LntjgEkZ3h4+LGIOHbi9kIKav78+RoaGiriqYGOVa1WtXTp0nbHAJJj++HDbecQHwAgSRQUACBJFBQAIEkUFAAgSRQUACBJFBQAIEm5Csr2GttbbG+1fWHBmQAAqF9Qtk+UtErSqZJOlnS27eOKDgZMF6tXr9asWbO0bNkyzZo1S6tXr253JKAj5DlR9+WS7o+IvZJk+yuS3iTpw0UGA6aD1atX67rrrtOVV16pBQsWaNu2bbrkkkskSR//+MfbnA5IW56C2iLpcttHS3pS0lmSnrFMhO0BSQOSVCqVVK1WmxgT6Exr167VqlWrtGjRIo2MjGjRokVatWqV1q5dq3POOafd8YCkOc8VdW2/XdK7JY1I2ibpyYh472T7l8vlYKkjQLKtPXv2aM6cOWNLHe3du1dz584VV7MGamwPR0R54vZca/FFxI2Sbsye6G8lPdLceMD0NHPmTJ1xxhkaGhrSvn37NHPmTJXLZc2cObPd0YDk5Z3F94Ls969JerOkdUWGAqaL448/Xvfdd5/OPPNMDQ4O6swzz9R9992n448/vt3RgOTlXc389uw7qP2S3h0RPyswEzBtPPTQQ1qyZInuvvtuVSoVzZw5U0uWLGG1fyCHXCOoiDgtIhZExMkR8eWiQwHTxb59+9Tf33/Itv7+fu3bt69NiYDOUcj1oADU9Pb26oYbbtBVV101Ns38fe97n3p7e9sdDUgeSx0BBZpsph4z+ID6GEEBBTpw4IAWLFigiy66aGzbwZEUgKlRUECBenp6tGPHDl199dWHHOLr6eHgBVAPnxKgQLYb2g7gaYyggAKNjo6qv7//kEN8/f392rp1axtTAZ2BggIK1Nvbq+3btz/jEB+z+ID6OMQHFIhZfMCzxwgKKBCz+IBnjxEUUKDe3l5t27Zt7JDexL8BTI6CAgo0OjoqSVq+fLkGBwe1fPnyQ7YDmByH+ICClUolVSoVVSqVsb937drV5lRA+hhBAQWbWEaUE5APBQW0wOLFi3Xrrbdq8eLF7Y4CdAwO8QEtsGHDBm3YsKHdMYCOwggKAJAkCgoAkCQKCgCQJAoKAJAkCgoomG319fVJkvr6+rjUBpATBQUULCLGSsk2C8UCOVFQQAs89dRTh/wGUB8FBQBIEgUFFGiyVctZzRyoj4ICCjTZquWsZg7Ul6ugbL/X9lbbW2yvsz2r6GAAgO5Wt6Bsv0jSeySVI+JESb2Szi86GACgu+U9xDdD0mzbMyTNkfRocZEAAMixmnlE/ND2RyT9QNKTku6JiHsm7md7QNKAVLsgW7VabXJUYHrhMwJMzfVOGrT9K5Jul3SepN2SbpV0W0R8erLHlMvlGBoaamJMoDNNtWoEJ+wCNbaHI6I8cXueQ3yvl/T9iPhJROyXdIckrroGAChUnoL6gaRX257j2v8Ovk7S9mJjAQC6Xd2CiogHJN0maaOkB7PHXF9wLgBAl8t1yfeIuEzSZQVnAQBgDCtJAACSREEBAJJEQQEAkkRBAQCSREEBAJJEQQEAkkRBAQCSREEBAJJEQQEAkkRBAQCSREEBAJJEQQEAkkRBAQCSREEBAJJEQQEAkkRBAQCSREEBAJJEQQEAkkRBAQCSREEBAJJEQQEAkkRBAQCSREEBAJJEQQEAkkRBAQCSVLegbJ9ge9O4n5/bvrAF2QAAXWxGvR0i4juSXiFJtnsl/VDSYLGxAADdrm5BTfA6Sf8dEQ8XEQZIne22PVdENO21gU7QaEGdL2nd4e6wPSBpQJJKpZKq1eqRJQMStH79+ob2X7ZsWdOei88Uuo3z/l+Z7edIelRSf0TsmmrfcrkcQ0NDTYgHdLbe3l4dOHDgGdt7eno0OjrahkRAemwPR0R54vZGZvH9rqSN9coJwNNGR0fV03Pox4xyAvJppKAu0CSH9wBMbnR0VBGhl15ypyKCcgJyylVQtudI+h1JdxQbBwCAmlyTJCJir6SjC84CAMAYVpIAACSJggIAJImCAgAkiYICACSJggIAJImCAgAkiYICACSJggIAJImCAgAkiYICACSJggIAJImCAgAkiYICACSJggIAJImCAgAkiYICACSJggIAJImCAgAkiYICACSJggIAJImCAgAkiYICACSJggIAJImCAgAkiYICACQpV0HZfr7t22zvsL3d9muKDgYA6G4zcu53raS7IuL3bT9H0pwCMwEAUL+gbB8l6bWS3iZJEfGUpKeKjQUA6HZ5RlAvk/QTSTfZPlnSsKQ1EbFn/E62ByQNSFKpVFK1Wm1yVKDz8bkA8nNETL2DXZZ0v6QlEfGA7Wsl/TwiPjDZY8rlcgwNDTU3KdAkJ3/oHj3+5P52x2iZ583u07cvO6PdMYBJ2R6OiPLE7XlGUI9IeiQiHsj+vk3Spc0MB7TS40/u184rlrf8davVqpYuXdry151/6Rda/ppAM9SdxRcRP5L0P7ZPyDa9TtK2QlMBALpe3ll8qyV9JpvB9z1Jf1pcJAAAchZURGyS9IzjgwAAFIWVJAAASaKgAABJoqAAAEmioAAASaKgAABJoqAAAEmioAAASaKgAABJyruSBDBtzHv5pVr4qTYtJ/mp1r/kvJdLUuvXHgSOFAWFrvPE9itYLBboABziAwAkiYICACSJggIAJImCAgAkiYICACSJggIAJImCAgAkiYICACSJggIAJImCAgAkiaWO0JXatvzPXa1/3efN7mv5awLNQEGh67RjHT6pVortem2gE3GIDwCQJAoKAJAkCgoAkKRc30HZ3inpCUmjkn4ZEeUiQwEA0MgkiWUR8VhhSQAAGIdDfACAJOUdQYWke2yHpLURcf3EHWwPSBqQpFKppGq12rSQwHTB5wLIL29BLYmIR22/QNKXbO+IiHvH75CV1vWSVC6XY+nSpc1NCnS6u74gPhdAfrkO8UXEo9nvH0salHRqkaEAAKhbULbn2p538LakMyRtKToYAKC75TnEV5I0aPvg/p+NiLsKTQUA6Hp1Cyoivifp5BZkAQBgDNPMAQBJoqAAAEmioAAASaKgAABJoqAAAEmioAAASaKgAABJoqAAAEmioAAASaKgAABJoqAAAEmioAAASaKgAABJoqAAAEmioAAASaKgAABJoqAAAEmioAAASaKgAABJmtHuAMB0Z/vp21fWfkdEm9IAnYMRFFCg8eWUZzuAp1FQAIAkcYgPaEAzRz6NPheHBdFtKCigAY2WxFQlROEAU+MQHwAgSbkLynav7W/ZvrPIQAAASI2NoNZI2l5UEAAAxstVULZfLGm5pBuKjQMAQE3eSRIflfQXkuZNtoPtAUkDklQqlVStVo80GzCt8RkBpuZ6M4lsny3prIh4l+2lki6OiLOneky5XI6hoaGmhQQ6FbP4gPpsD0dEeeL2PIf4lkhaYXunpM9JOt32p5ucDwCAQ9QtqIj4y4h4cUTMl3S+pP+MiLcWngwA0NU4DwoAkKSGVpKIiKqkaiFJAAAYhxEUACBJFBQAIEkUFAAgSRQUACBJFBQAIEkUFAAgSRQUACBJFBQAIEkUFAAgSRQUACBJFBQAIEkUFAAgSRQUACBJFBTQAv39/Vq3bp36+/vbHQXoGA1dbgPAs7N161ZdcMEF7Y4BdBRGUACAJFFQAIAkUVAAgCRRUACAJFFQAIAkUVAAgCRRUEAL2D7kN4D6KCigBSLikN8A6qOgAABJoqCAFlmxYkW7IwAdpW5B2Z5l+xu2v217q+0PtSIYMN1UKpV2RwA6Sp61+PZJOj0iRmz3Sfqa7X+PiPsLzgYA6GJ1Cypq3+qOZH/2ZT980wsAKFSu1cxt90oalvSbkj4REQ8cZp8BSQOSVCqVVK1WmxgTmH74jABTcyPTXm0/X9KgpNURsWWy/crlcgwNDR15OqDDTXXeE1POgRrbwxFRnri9oVl8EbFbUlXSG5oTCwCAw8szi+/YbOQk27MlvV7SjoJzAQC6XJ4R1Aslrbe9WdI3JX0pIu4sNhYwvaxYsUKDg4OcCwU0oKHvoPLiOyighu+ggPqa8h0UgGfv3HPPbXcEoKNQUECL3HLLLe2OAHQUCgoAkCQKCgCQJAoKaIG+vj5de+216uvra3cUoGPkWuoIwJHZv3+/1qxZ0+4YQEdhBAUASBIFBQBIEgUFAEgSBQUASBIFBRSsVCopIrR+/XpFhEqlUrsjAR2BWXxAwXbt2jXlmnwADo8RFAAgSRQU0AJcbgNoHJfbAArE5TaA+rjcBtBGjKCAxjFJAmiBSqWinp4eVSqVdkcBOgYjKKBFPv/5z7c7AtBRKCigYCtWrDjkPCgO8wH5UFBAgRYuXKhKpaKVK1dq9+7dWrlypSqVihYuXNjuaEDymMUHFOykk07Sgw8+OPb3woULtXnz5jYmAtLCLD6gTTZv3nzIIT7KCciHggIAJImCAgAkiYICACSpbkHZfont9ba3295qe00rggEAuluelSR+KemiiNhoe56kYdtfiohtBWcDAHSxuiOoiPjfiNiY3X5C0nZJLyo6GACguzW0Fp/t+ZJOkfTAYe4bkDQg1a4gWq1WmxAPmD5GRkb4XAANyH2iru3nSvqKpMsj4o46+/5E0sNHHg+YVo6R9Fi7QwAJemlEHDtxY66Cst0n6U5Jd0fE3xUQDpj2bA8d7mx5AIeXZxafJd0oaTvlBABolTznQS2R9EeSTre9Kfs5q+BcAIAuV3eSRER8TdLk160GkNf17Q4AdJJCVjMHAOBIsdQRACBJFBQAIEkUFLqW7Q/avji7XbXd0BRw2xuy3/Ntb5lkn7Hntf1F288/wth5s/1WNqHpW7Z/w/Zbxt13dLa+5ojtvx+3fd64iVCbbD9m+6OtyAscDgWFruCapv73HhGLG9z/rIjY3cwMU3ijpH+NiFMkvUTSW8bd9wtJH5B08YR8T0TEKw7+qHay/ZQn5QNFoqDQUWz/9fgV9W1fbvs9tr9se6PtB22vzO6bn63C/w+SNkp6ie332/6O7f+QdMKEp3+r7Q22t9g+NXuOsVFW9veWbMkv2R45TL7Ztj9ne7PtmyXNHnffTtvHjMv1yewKAffYnp3t86rssV+3fdXBkZntftvfyEY2m20fl20fez+219m+ODsN5EJJ77C9XtIVkk7LHvveiNiTzc79xRT/nI+T9AJJX833bwZoPgoKneZGSX8iSdmI6HxJN0t6U0QskrRM0tXZCeZSrYT+JRtJHJPtf4qkN0t61YTnnpuNit4l6Z+eZb4/l7Q3Ik6SdLmkV06y33GSPhER/ZJ2Szon236TpHdGxGskjY7b/52Srs1GNmVJj9h+5eHeT0R8UdJ1kq6JiGWSLpX01WxkdE3O93GBpJuDab5oo4YWiwXaLSJ22v4/26dIKkn6lqSfSrrG9mslHVBttf1S9pCHI+L+7PZpkgYjYq8k2a5MePp12Wvca/uoZ/l90WslfSx7ns22N0+y3/cjYlN2e1jS/Oz15kXEhmz7ZyWdnd3+uqT3236xpDsi4ru2672fI3G+aifoA21DQaET3SDpbZJ+VbWRzh9KOlbSKyNiv+2dkmZl++6Z8NipRgQT7wvVroc2/kjDLNWXZ9Sxb9ztUdUOBU56QnxEfNb2A5KWS7rb9jsaeK2G2D5Z0oyIGG72cwON4BAfOtGgpDeodkjrbknPk/TjrJyWSXrpJI+7V9Kbsu+J5kn6vQn3nydJtn9b0uMR8biknZIWZdsXSfr1OtnuVa0wZftESSflfVMR8TNJT9h+dbbp/IP32X6ZpO9FxMckVbLnrfd+DnpC0ry8OVQ7vLeugf2BQjCCQseJiKeyL/93R8So7c9I+jfbQ5I2SdoxyeM2ZhMXNqk2Q23iBICfZVPHj5L0Z9m22yX9se1Nkr4p6aE68f5R0k3Zob1Nkr7R2LvT2yV90vYeSVVJj2fbz1NtEsd+ST+S9FcR8dM67+egzZJ+afvbkv45Iq7JRplHSXqO7TdKOmPcVbLPlcR6m2g7ljpCx8kmR2yU9AcR8d1252km28+NiJHs9qWSXhgRa+o87OBjPyhpJCI+UmBEoGU4xIeOYnuBpP+S9OXpVk6Z5dl08C2qTer4m3YHAtqFERQAIEmMoAAASaKgAABJoqAAAEmioAAASaKgAABJ+n+NB/Gx+gNntgAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAUAklEQVR4nO3df5BdZX3H8fd3E9zEJCojcseR1q21VRMDoreOYusEraBiifbHCNZSEdiGdlIsapPaOrE/0iHFaK1DTUNbav1BrdYM0Qhi21x/YZANQoAElNqlVYtIMZiFZCW73/6xN+lm2c29m+zZ++zu+zWzc+8997nnfG8mZz/7nPM850RmIklSabo6XYAkSeMxoCRJRTKgJElFMqAkSUUyoCRJRTKgJElFaiugIuKyiLgzIu6KiLdVXJMkSa0DKiKeD1wCvBg4DXhdRPxM1YVJkua2+W20eR6wIzMfBYiILwJvAP5iog+cdNJJ2dPTMyUFSrPFI488wqJFizpdhlScnTt3PpiZTxu7vJ2AuhNYHxFPBfYDrwX6jvaBnp4e+vqO2kSacxqNBitWrOh0GVJxIuK+8Za3DKjM3BMRG4AvAAPA7cDBcTbQC/QC1Go1Go3G8dQrzToDAwPuF9IkxGSvxRcRfw58JzP/eqI29Xo97UFJR7IHJY0vInZmZn3s8nYO8RERJ2fmAxHxk8AvAy+d6gIlSRqtrYAC/qV5Duox4Hcy84cV1iRJUnsBlZm/UHUhkiSN5pUkJElFMqCkiq1evZoFCxZw5plnsmDBAlavXt3pkqQZod1zUJKOwerVq9m0aRMbNmxg6dKl7N69mzVr1gDwwQ9+sMPVSWWzByVV6Oqrr2bDhg1cfvnlLFiwgMsvv5wNGzZw9dVXd7o0qXgGlFShwcFBVq1adcSyVatWMTg42KGKpJnDgJIq1N3dzaZNm45YtmnTJrq7uztUkTRzeA5KqtAll1xy+JzT0qVLed/73seaNWse16uS9HgGlFShQwMh3vWudzE4OEh3dzerVq1ygITUhklfi68dXotPejyvxSeNb6Jr8XkOSpJUJANKklQkA0qSVCQDSpJUJANKklQkA0qSVCQDSqqYVzOXjo0TdaUKrV69mquuuoqurpG/BQ8ePMhVV10FeDVzqRUn6koVmj9/PpnJlVdeefh2G+985zuJCA4ePNjp8qQiOFFX6oChoSHWr19/xO021q9fz9DQUKdLk4pnQEkVu+666444B3Xdddd1uiRpRjCgpIrt2LGDs88+my1btnD22WezY8eOTpckzQgOkpAqNG/ePIaGhti2bRtbt25l3rx5h5dLOjp7UFKFhoaGWLZs2eFzTmNfS5qYPSipQvPmzWPPnj1s3LjxiFF89qCk1uxBSRWaaBpHFdM7pNnGHpRUoeHhYXp7e4+4o+7FF1/M5s2bO12aVLy2JupGxO8BFwMJ3AFcmJkHJmrvRF1pxIIFC+jq6mL//v2Hly1cuJDh4WEOHJhwF5LmlGOeqBsRzwB+F6hn5vOBecB5U1+iNDvt37+fWq3GNddcQ61WOyKsJE2s3XNQ84GFETEfeCLwvepKkmaPwcFBTjzxRPbu3cuFF17I3r17OfHEExkcHOx0aVLxWgZUZn4XeC/wX8D/AA9n5o1VFybNFieffPLhQBocHOTkk0/ucEXSzNBykEREnAisBH4K2At8MiLenJkfHdOuF+gFqNVqNBqNKS9WmonuuecezjjjDC699FI+9KEPcdNNNwG4j0gttBwkERG/Brw6My9qvr4AeElm/vZEn3GQhDQiIiZ8z6Hm0ojjuZr5fwEviYgnxsje9kpgz1QXKEnSaO2cg7oZ+BRwKyNDzLsAJ3FIk3DohoWHHiW11tZE3cxcB6yruBZp1hoeHj7iUVJr/jknSSqSASVNg40bN3L99dezcePGTpcizRhei0+aBu94xzvIzKOO6pN0JHtQ0jQ4NKTcoeVS+wwoqULLly8H/n8+1KHHQ8slTcyAkiq0a9cu4PE9qEPLJU3MgJIqdKjH1NXVxZVXXnl4HpTnoqTWDCipYl1dXQwNDVGv1xkaGnKyrtQm9xSpYjfeeONRX0sanwElVeyss8466mtJ43MelDQJx3LuaHh4eNzPTXZdDlHXXGNASZNwLCExXhAZNlJrHuKTKpaZZCbPXPPZw88ltWZASZKKZEBJkopkQEmSimRASZKKZEBJkopkQEmSimRASZKKZEBJkopkQEmSimRASZKKZEBJkopkQEmSimRASZKK1DKgIuI5EXHbqJ8fRcTbpqE2SdIc1vJ+UJl5D/ACgIiYB3wX2FJtWZKkuW6yh/heCfxHZt5XRTGSJB0y2TvqngdcO94bEdEL9ALUajUajcbxVSbNQu4XUvui3bt7RsQTgO8ByzLz+0drW6/Xs6+vbwrKk2aPnrXb6L/inE6XIRUnInZmZn3s8skc4nsNcGurcJIkaSpMJqDOZ4LDe5IkTbW2Aioingi8Cvh0teVIkjSirUESmfko8NSKa5Ek6TCvJCFJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkqUlsBFRFPiYhPRcTdEbEnIl5adWGSpLltfpvtPgDckJm/GhFPAJ5YYU2SJLUOqIh4EvBy4C0Amflj4MfVliVJmuva6UE9C/gBcE1EnAbsBC7LzEdGN4qIXqAXoFar0Wg0prhUaeZzv5DaF5l59AYRdWAH8LLMvDkiPgD8KDPfPdFn6vV69vX1TW2l0gzXs3Yb/Vec0+kypOJExM7MrI9d3s4gie8A38nMm5uvPwW8cCqLkyRprJYBlZn3A/8dEc9pLnolsLvSqiRJc167o/hWAx9rjuD7NnBhdSVJktRmQGXmbcDjjg9KklQVryQhSSqSASVJKlK756CkWeO0P76Rh/c/1pFt96zdNu3bfPLCE7h93VnTvl3peBlQmnMe3v9YR+YjNRoNVqxYMe3b7UQoSlPBQ3ySpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQize90AdJ0W/K8tSz/8NrObPzD07/JJc8DOGf6Nywdp7YCKiL6gX3AEHAwM+tVFiVVad+eK+i/Yvp/YTcaDVasWDHt2+1Zu23atylNhcn0oM7MzAcrq0SSpFE8ByVJKlK7PagEboyIBP4mMzePbRARvUAvQK1Wo9FoTFmR0lTrxP/PgYGBju0X7o+aidoNqJdl5vci4mTgCxFxd2Z+aXSDZmhtBqjX69mJY+1SW27Y1pFzQZ06B9Wp7ysdr7YO8WXm95qPDwBbgBdXWZQkSS0DKiIWRcSSQ8+Bs4A7qy5MkjS3tXOIrwZsiYhD7T+emTdUWpUkac5rGVCZ+W3gtGmoRZKkwxxmLkkqkgElSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkq0mTuqCvNGh27DfoN07/dJy88Ydq3KU0FA0pzTv8V53Rkuz1rt3Vs29JM5CE+SVKRDChJUpEMKElSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpHaDqiImBcR34iIz1ZZkCRJMLke1GXAnqoKkSRptLYCKiJOAc4B/rbaciRJGtHuHXX/Evh9YMlEDSKiF+gFqNVqNBqN461NmnXcL6T2tQyoiHgd8EBm7oyIFRO1y8zNwGaAer2eK1ZM2FSam27YhvuF1L52DvG9DDg3IvqBfwJeEREfrbQqSdKc1zKgMvMPMvOUzOwBzgP+PTPfXHllkqQ5zXlQkqQitTtIAoDMbACNSiqRJGkUe1CSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIrUMqIhYEBFfj4jbI+KuiPjj6ShMkjS3zW+jzSDwiswciIgTgK9ExPWZuaPi2iRJc1jLgMrMBAaaL09o/mSVRUmS1E4PioiYB+wEng1clZk3j9OmF+gFqNVqNBqNKSxTmh3cL6T2xUgHqc3GEU8BtgCrM/POidrV6/Xs6+s7/uqkWaRn7Tb6rzin02VIxYmInZlZH7t8UqP4MnMv0ABePTVlSZI0vnZG8T2t2XMiIhYCvwjcXXFdkqQ5rp1zUE8HPtw8D9UF/HNmfrbasiRJc107o/h2AadPQy2SJB3mlSQkSUVqa5i5pGM3b948hoeHAYgN0NXVxdDQUIerkspnQEmTEBHHvY7h4eFjWs9kpoRIs4GH+KRJyMxJ/QAsXryYzGT79u1kJosXLz7mdUlziT0oqWIRcUSPacmSJR2sRpo57EFJFdu3bx/Lli3j2muvZdmyZezbt6/TJUkzgj0oaRrcddddnH/++Z0uQ5pR7EFJkopkQEkVm2iQhKSj8xCfVLGBgYEpGZ4uzTX2oCRJRTKgpGlw7rnnsmXLFs4999xOlyLNGB7ikyq2ZMkStm7dytatWw+/dqi51JoBJVVs3759bNy4kaVLl7J7927e/va3d7okaUbwEJ80DdatW0d/fz/r1q3rdCnSjGEPSqpYRDAwMMCll156+LXX1pNaswclVai7u5tarXbEslqtRnd3d4cqkmYOA0qq0KJFi7j//vuPuBbf/fffz6JFizpdmlQ8D/FJFXrooYfo6enh3nvv5fzzz6e7u5uenh76+/s7XZpUPHtQUsVuueUWDhw4wPbt2zlw4AC33HJLp0uSZgQDSqrYRRdddNTXksZnQEkVWr58OVu3bmXlypXs3buXlStXsnXrVpYvX97p0qTiRRXDXev1evb19U35eqWZ6NRTT+WOO+44/Hr58uXs2rWrgxVJZYmInZlZH7vcHpRUsV27dh1xuw3DSWqPASVJKpIBJUkqUsuAioifiIjtEbEnIu6KiMumozBJ0tzWzkTdg8DbM/PWiFgC7IyIL2Tm7oprkyTNYS17UJn5P5l5a/P5PmAP8IyqC5MkzW2TutRRRPQApwM3j/NeL9ALIxfDbDQaU1CeNHsMDAy4X0iT0PY8qIhYDHwRWJ+Zn27R9gfAfcdfnjSrnAQ82OkipAI9MzOfNnZhWwEVEScAnwU+n5nvq6A4adaLiL7xJiNKGl87o/gC+Dtgj+EkSZou7cyDehnwG8ArIuK25s9rK65LkjTHtRwkkZlfAWIaapFmu82dLkCaSSq5WKwkScfLSx1JkopkQGnOioj3RMQ7ms8bETGpEXYRcVPzsSci7pygzeH1RsTnIuIpx1l2u7U9t3m++BsR8dMR8aZR770qInZGxB3Nx1eMeu8JEbE5Ir4ZEXdHxK9MR73SeAwozQkxYkr/v2fmGZNs/9rM3DuVNRzF64HrMvN04CeAN41670HglzJzOfCbwEdGvfeHwAOZ+bPAUkbmPkodMakrSUidFhF/CjyYmR9ovl4PfB9YCZwInAD8UWZe17zyyfXAduClwOsj4s3ABcB/Az8Ado5a/Zsj4q+AJwFvzcyvR8R7gIHMfG9ze3cCr8vM/ogYyMzFY+pbCFzDyC/3PcDCUe/1A3VgcbOurwBnAN8FVmbm/oj4OUamdTzSfP81mfn8iFjWXO8TGPnD8lcy81sR8YfjfJ/dwNuAoYh4ebOG50XEbcCHM/P9o0q+C1gQEd2ZOQi8FXguQGYO48RidZA9KM00f8fIX/00e0TnAZ8A3pCZLwTOBDY25+8BPAf4x2ZP4qRm+9OBXwZ+bsy6FzV7Rb8N/P0x1ncp8GhmngqsB140QbufAa7KzGXAXuDQobRrgFWZ+VJgaFT7VcAHMvMFjITcdyLiReN9n8z8HLAJeH9mngmsBb6cmS8YE040t/uNzBwcdfjxTyPi1oj4ZETUjuUfQZoKBpRmlMzsB/43Ik4HzgK+ATwE/HlE7AL+lZGLGR/6xXpfZu5oPv8FYEtmPpqZPwK2jln9tc1tfAl40jGeL3o58NHmenYBE90+9z8z87bm851AT3N7SzLzpubyj49q/zXgXRGxhpHLwuxv4/scVbNXtgH4reai+cApwFebYf814L2TWac0lQwozUR/C7wFuJCRns6vA08DXtTsYXwfWNBs+8iYzx5tXsXY95KR282M3k8W0Fo7czcGRz0fYiQcJpxvmJkfB84F9gOfHzWw4ZjmiUTEKcAW4ILM/I/m4v8FHm0uB/gk8MJjWb80FQwozURbgFczckjr88CTGTmx/1hEnAk8c4LPfQl4Q0QsbN7b7JfGvP9GgIj4eeDhzHwY6Kf5SzoiXgj8VIvavsRIYBIRzwdObfdLZeYPgX0R8ZLmovMOvRcRzwK+nZl/xUhP6dQ2vs8h+4Alo9b1FGAb8AeZ+dVR20/gM8CK5qJXMnI+S+oIB0loxsnMH0fEdmBvZg5FxMeAz0REH3AbcPcEn7s1Ij7RbHMf8OUxTX7YHDr+JEYGCwD8C3BBc4DBLcA3W5T3IeCa5uHG24CvT+7bcRFwdUQ8AjSAh5vL38jIII7HgPuBP8nMh1p8n0N2AQcj4nbgH4BFwLOBd0fEu5ttzsrMB4A1wEci4i8ZGXRx4STrl6aMV5LQjNMcHHEr8GuZ+a1O1zOVImJxZg40n68Fnp6Zl7X52fcwasShNNN5iE8zSkQsBe4F/m22hVPTOc0JtncyMgjizzpdkNQp9qAkSUWyByVJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSrS/wFPiN5qGzTNUQAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAVm0lEQVR4nO3df4xd5X3n8fd3/GOwYyfUuAxaEtlpDNS2UGgy6xZC0YBxgx1klMhV7Cql3UbyjxUkVIFdkFii7srarNhsm43BrhtCK7Vy0kIIITgQBDMtyAnFdnCCPWutyTIEyBLHLDgDZvxjvvvHvXYuw4xnrn/MeTzzfklX98xznnvO91i++szznDPnRGYiSVJpWqouQJKkwRhQkqQiGVCSpCIZUJKkIhlQkqQiTay6gMHMnDkzZ8+eXXUZ0qh68803ec973lN1GdKo27Zt2y8z8zcHthcZULNnz2br1q1VlyGNqq6uLjo6OqouQxp1EdEzWLtTfJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhV5qyPpTBYRo75Pn4ytscgRlHSKZeYJvWb9x++e8GelsciAkiQVyYCSJBXJgJIkFcmAkiQVyYCSJBXJgJIkFcmAkiQVyYCSJBXJgJIkFcmAkiQVyYCSJBXJgJIkFWnYgIqIiyLi2YbX/oi4aZB+HfX1OyPinxvar4mI3RGxJyJuPcX1S5LGqGEft5GZu4FLACJiAvAy8EBjn4g4G7gbuCYzX4yIcxv63wUsAl4CnomI72TmrlN4DJKkMajZKb6FwPOZ2TOg/Y+Ab2XmiwCZ+Yt6+wJgT2b+NDMPAt8ArjuZgiVJ40OzAbUc2DRI+4XAb0REV0Rsi4jr6+3nAz9r6PdSvU2SpOMa8RN1I2IysBS4bYjtfJTaCGsK8IOI+CEw2KNFB326WkSsBFYCtLW10dXVNdLSpDHD//fSrzXzyPfFwPbMfHWQdS8Bv8zMN4E3I+JfgA/X2z/Q0O/9wCuDbTwzNwIbAdrb27Ojo6OJ0qQx4JGH8f+99GvNTPGtYPDpPYAHgd+PiIkRMRX4XaAbeAa4ICI+WB+BLQe+czIFS5LGhxGNoOqhswhY1dC2GiAzN2Rmd0Q8AvwY6Ae+lpnP1fvdADwKTAC+npk7T+0hSJLGohEFVGa+BZwzoG3DgJ/vBO4c5LObgc0nUaMkaRzyThKSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiNfM8KGlc+fBffJ83Dhwa1X3OvvXhUd3f+6ZMYscX/2BU9ymNlAElDeGNA4d44UufGLX9dXV1jfoDC0c7EKVmOMUnSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSqSASVJKpIBJUkqkgElSSqSDyyUhjB97q1c/He3ju5O/250dzd9LsDoPZRRaoYBJQ3hV91f8om6UoWGneKLiIsi4tmG1/6IuGlAn46IeKOhzx0N6/48InZGxHMRsSkizjoNxyFJGmOGHUFl5m7gEoCImAC8DDwwSNcnM/PaxoaIOB/4HDAvMw9ExD8Cy4G/PbmyJUljXbMXSSwEns/MniY+MxGYEhETganAK03uU5I0DjV7Dmo5sGmIdZdGxA5qAXRzZu7MzJcj4r8DLwIHgO9n5vcH+3BErARWArS1tdHV1dVkadKpN5r/D3t7eyv5f+93TaUacUBFxGRgKXDbIKu3A7MyszcilgDfBi6IiN8ArgM+CLwO/FNEfCYz/37gBjJzI7ARoL29PUf7ZLH0Lo88PKoXLVRxkcRoH6PUjGam+BYD2zPz1YErMnN/ZvbWlzcDkyJiJnA18H8yc29mHgK+BVx2CuqWJI1xzQTUCoaY3ouI8yIi6ssL6tvdR21q7/ciYmp9/UKg++RKliSNByOa4ouIqcAiYFVD22qAzNwALAPWRMRhauealmdmAk9HxH3UpgAPAz+iPo0nSdLxjCigMvMt4JwBbRsaltcB64b47BeBL55EjZKkcch78UmSimRASZKKZEBJkorkzWKl4xj1m6k+Mrr7e9+USaO6P6kZBpQ0hNG8kznUwnC09ymVzCk+SVKRDChJUpEMKElSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpGGDaiIuCginm147Y+Imwb06YiINxr63NGw7uyIuC8i/ldEdEfEpafhOCRJY8zE4Tpk5m7gEoCImAC8DDwwSNcnM/PaQdq/AjySmcsiYjIw9cTLlSSNF8MG1AALgeczs2cknSPivcAVwJ8CZOZB4GCT+5QkjUPNBtRyYNMQ6y6NiB3AK8DNmbkT+C1gL3BvRHwY2AZ8PjPfHPjhiFgJrARoa2ujq6urydKkM5//76Vfi8wcWcfa9NwrwPzMfHXAuvcC/ZnZGxFLgK9k5gUR0Q78EPhYZj4dEV8B9mfmfzrevtrb23Pr1q0ncjzSGWv2rQ/zwpc+UXUZ0qiLiG2Z2T6wvZmr+BYD2weGE0Bm7s/M3vryZmBSRMwEXgJeysyn613vAz7SdPWSpHGnmYBawRDTexFxXkREfXlBfbv7MvP/Aj+LiIvqXRcCu06iXknSODGic1ARMRVYBKxqaFsNkJkbgGXAmog4DBwAluev5w5vBP6hPkX4U+DfnbryJUlj1YgCKjPfAs4Z0LahYXkdsG6Izz4LvGtuUZKk4/FOEpKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiNXs3c0mnWP0uYbXl/1Z7H+lNnKWxzBGUVKHGcBpJuzSeOIKSTrFTFS7NbMcRl8YiA0o6xZoJi+OFkKGj8c4pPklSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpEMKElSkQwoSVKRDChJUpEMKElSkYYNqIi4KCKebXjtj4ibBvTpiIg3GvrcMWD9hIj4UUR89xTXL0kaoyYO1yEzdwOXQC1ogJeBBwbp+mRmXjvEZj4PdAPvPbEyJUnjTbNTfAuB5zOzZ6QfiIj3A58AvtbkviRJ49iwI6gBlgObhlh3aUTsAF4Bbs7MnfX2vwL+AzD9eBuOiJXASoC2tja6urqaLE0aW/wOaLyLzBxZx4jJ1MJnfma+OmDde4H+zOyNiCXAVzLzgoi4FliSmf8+IjqoBddQ04DHtLe359atW5s8FOnMExFDrhvpd1M600XEtsxsH9jezBTfYmD7wHACyMz9mdlbX94MTIqImcDHgKUR8QLwDeCqiPj7EzkASdL40kxArWCI6b2IOC/qvwpGxIL6dvdl5m2Z+f7MnE1tevCJzPzMSdYsSRoHRnQOKiKmAouAVQ1tqwEycwOwDFgTEYeBA8DydH5CknQSRnwOajR5DkrjheegpFNzDkqSpFFjQEmSimRASZKKZEBJkopkQEmSimRASZKKZEBJkorU7M1iJZ1iM2bM4L777uPIkSNMmDCBZcuW8dprr1VdllQ5A0qq2Ouvv87ixYvp6+ujtbWVQ4cOVV2SVAQDSqpYf38/fX19AMfeJXkOSqrUxImD/444VLs0nhhQUoUOHz7MrFmzaG1tBaC1tZVZs2Zx+PDhiiuTquevaVLFenp6ji339fW942dpPHMEJRVg/vz5bNq0ifnz51ddilQMR1BSAbq7u1mxYgUtLf7OKB3lt0GqWEtLC/39/UDtij5DSqrxmyBV7Gg4DfWzNF4ZUJKkIhlQUsUGTuk5xSfVeJGEVLFzzjmHb37zm8fuxffpT3+avXv3Vl2WVDkDSqrY3r17ueqqq6ouQyqOcwlShSKiqXZpPDGgpAplZlPt0nhiQEkFmD59Oi0tLUyfPr3qUqRieA5KqlhLSwsPPvjgsYskrr76av8WSsIRlFS5/v5+br/9dl5//XVuv/12w0mqcwQlFWDLli1s2bKl6jKkojiCkip09DlQ06ZNe8f70XZpPDOgpArde++9TJo0id7eXgB6e3uZNGkS9957b8WVSdUzoKQKrVixglWrVr3jibqrVq1ixYoVFVcmVc9zUFKFNm3axMMPP8z3vve9Y1fxffazn+Wyyy4zpDTuDTuCioiLIuLZhtf+iLhpQJ+OiHijoc8d9fYPRERnRHRHxM6I+PxpOg7pjLR27VruuecerrzySiZOnMiVV17JPffcw9q1a6suTarcsCOozNwNXAIQEROAl4EHBun6ZGZeO6DtMPCFzNweEdOBbRHxWGbuOrmypbGhu7ubyy+//B1tl19+Od3d3RVVJJWj2XNQC4HnM7NnJJ0z8+eZub2+/CugGzi/yX1KY9bcuXN56qmn3tH21FNPMXfu3IoqksrR7Dmo5cCmIdZdGhE7gFeAmzNzZ+PKiJgN/A7w9GAfjoiVwEqAtrY2urq6mixNOvN88pOf5FOf+hStra384he/4Nxzz6Wvr48bbrjB74DGvREHVERMBpYCtw2yejswKzN7I2IJ8G3ggobPTgPuB27KzP2DbT8zNwIbAdrb27Ojo2OkpUlnrJ///OdMmjSJKVOmEBFMmTKF/v5+5s2bh98BjXfNTPEtBrZn5qsDV2Tm/szsrS9vBiZFxEyAiJhELZz+ITO/dQpqlsaMtWvXMmvWLHp6eujv76enp4dZs2Z5kYREc1N8Kxhiei8izgNezcyMiAXUgm9f1B5qcw/QnZn/46SrlcaYnTtrM+Fr1qxhyZIlbN68mfXr11dclVSGEY2gImIqsAj4VkPb6ohYXf9xGfBc/RzU/wSWZ+2BNh8D/hi4quES9CWn9AikM9zSpUu5++67mTZtGnfffTdLly6tuiSpCCMaQWXmW8A5A9o2NCyvA9YN8rmnAB8NKh3Hjh076Ozs5MiRI3R2drJjx46qS5KK4J0kpApFBHPmzOHGG2+ku7ubuXPnMmfOHF588cWqS5Mq5734pAotWrSIxx9/nCuuuIIHH3yQK664gscff5xFixZVXZpUuaidKipLe3t7bt26teoypFHx8Y9/nMcee4zMJCJYtGgRjz76aNVlSaMmIrZlZvvAdkdQUsUuvPBCJk+eDMDkyZO58MILK65IKoPnoKQK3Xjjjdx11120tNR+Vzx8+DB33XUXAF/96lerLE2qnFN8UoUmTJgAwJ133sm8efPYtWsXt9xyCwBHjhypsjRp1Aw1xecISqpQf38/c+bM4eabbz52DupDH/oQe/bsqbo0qXKeg5IqtmfPHlavXs1DDz3E6tWrDSepzik+qUK1u4HVpvqOPlH36NReid9N6XTwKj6pYP39/e94l2RASZWbMmXKsdFSZjJlypSKK5LKYEBJFTtw4MCxqb6I4MCBAxVXJJXBgJIK0DiCklRjQEkFOPqHukffJRlQUhG8SEJ6NwNKklQkA0qSVCQDSpJUJANKKkDjZeaSagwoqQBeZi69mwElSSqSASVJKpIBJRVgzZo1PPTQQ6xZs6bqUqRi+LgNqUIRQUtLyzv+QPfozyV+N6XTwcdtSAW6+OKL33X3iP7+fi6++OKKKpLKYUBJkoo0seoCpPHsJz/5CR/5yEfo6+uju7ubuXPn0trayvbt26suTaqcASVVrDGMdu7cWWElUlmc4pMKcNZZZ7Fu3TrOOuusqkuRiuEISipAX18fN9xwg7c6kho4gpIK4K2OpHcbNqAi4qKIeLbhtT8ibhrQpyMi3mjoc0fDumsiYndE7ImIW0/DMUhnvCeeeILHHnuMJ554oupSpGIMO8WXmbuBSwAiYgLwMvDAIF2fzMxrGxvq/e8CFgEvAc9ExHcyc9dJ1i2NKVdddVXVJUjFaXaKbyHwfGb2jLD/AmBPZv40Mw8C3wCua3KfkqRxqNmAWg5sGmLdpRGxIyK+FxHz623nAz9r6PNSvU2SpOMa8VV8ETEZWArcNsjq7cCszOyNiCXAt4ELgMEuSRr0LHBErARWArS1tdHV1TXS0qQxye+AxrtmLjNfDGzPzFcHrsjM/Q3LmyPi7oiYSW3E9IGGru8HXhls45m5EdgItZvFdnR0NFGadGb78pe/zLx589i1axdf+MIXAPA7oPGumSm+FQwxvRcR50X9DzgiYkF9u/uAZ4ALIuKD9RHYcuA7J1eyNPbcf//99Pb2cv/991ddilSMEY2gImIqtSvxVjW0rQbIzA3AMmBNRBwGDgDLs/YHHYcj4gbgUWAC8PXM9F4u0gBbtmxhy5YtVZchFcXnQUkVOt6dI0r8bkqng8+DkiSdUQwoqWJnn302ra2tALS2tnL22WdXW5BUCANKqtj69et5++236ezs5O2332b9+vVVlyQVwbuZSxW7/vrraWtr48iRI3R2dnL99ddXXZJUBANKqtCMGTN47bXXuOaaazh48CCTJ0/m0KFDzJgxo+rSpMo5xSdVaN++fcyYMYODBw8CcPDgQWbMmMG+ffsqrkyqngElVWzfvn1kJp2dnWSm4STVGVCSpCIZUJKkIhlQkqQiGVCSpCIZUJKkIhlQkqQiGVCSpCIV+biNiNgL9FRdhzTKZgK/rLoIqQKzMvM3BzYWGVDSeBQRWwd7Jo40XjnFJ0kqkgElSSqSASWVY2PVBUgl8RyUJKlIjqAkSUUyoCRJRTKgpEJExAsRMbOJ/v8mIu6rL3dExHcbli87XXVKo8WAkgoQEROa/UxmvpKZywZZ1QEYUDrjGVBSkyLiv0TE5xt+XhsRn4uIWyLimYj4cUT8RcP6b0fEtojYGRErG9p7I+I/R8TTwKX15lsi4l/rrzn1fn8bEcsaP1d/nx0Rzw2obTawGvjziHg2In7/NPwTSKPCgJKadw/wJwAR0QIsB14FLgAWAJcAH42IK+r9/ywzPwq0A5+LiHPq7e8BnsvM383Mp+pt+zNzAbAO+KtmC8vMF4ANwF9m5iWZ+WTzhyeVYWLVBUhnmsx8ISL2RcTvAG3Aj4B/C/xBfRlgGrXA+hdqofTJevsH6u37gCPA/QM2v6nh/S9P20FIZwADSjoxXwP+FDgP+DqwEPivmfnXjZ0iogO4Grg0M9+KiC7grPrqtzPzyIDt5iDLh6nPdkREAJNP1UFIJXOKTzoxDwDXUBs5PVp//VlETAOIiPMj4lzgfcD/q4fTbwO/N8x2P93w/oP68gvAR+vL1wGThtnGr4DpIz8UqUyOoKQTkJkHI6ITeL0+Cvp+RMwFflAb5NALfAZ4BFgdET8GdgM/HGbTrfWLJlqAFfW2vwEejIh/BR4H3hxmGw8B90XEdcCNnofSmcpbHUknoH5xxHbgDzPzf1ddjzQWOcUnNSki5gF7gMcNJ+n0cQQlSSqSIyhJUpEMKElSkQwoSVKRDChJUpEMKElSkf4/wgZ5FpIi/XIAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAZfElEQVR4nO3df5TddX3n8efLJNgWsKjRKRIq/sG6oURoncVaac+k/ihQCpV6LLHbyjZnU6iy2NXT0uYcf3SbLbrSXS1FGoVFuzLqWUJBRYG6M0V2q5BQCKHxB8vSJYYDa6HURAtJeO8f9xu4Ge7MhLmT3G/mPh/n3HM/38/38/1+Pndm7rzm+/l+53tTVUiS1DbPG/QAJEnqxYCSJLWSASVJaiUDSpLUSgaUJKmVDChJUistnq1BkmOBTwE/BjwFrK+qjyR5EfBZ4DjgAeCtVfVYj+1PAz4CLAI+UVWXzNbn0qVL67jjjtv/VyEtADt37uTwww8f9DCkg27Tpk3fraqXTK3PbP8HleRo4OiqujPJkcAm4JeB84BHq+qSJBcDL6yq35uy7SLgW8AbgW3AHcCqqvq7mfocHR2tjRs37u9rkxaEyclJxsbGBj0M6aBLsqmqRqfWzzrFV1UPVdWdTfl7wFbgGOBs4JNNs0/SCa2pTgHuq6r7q+pJ4DPNdpIkzeg5nYNKchzwk8DXgZGqegg6IQa8tMcmxwAPdi1va+okSZrRrOeg9kpyBHAt8K6q+qck+7VZj7qec4pJ1gBrAEZGRpicnNzfoUkLwo4dO/y5l7rsV0AlWUInnD5dVRua6oeTHF1VDzXnqR7psek24Niu5WXA9l59VNV6YD10zkE5F69h4zkoaV+zTvGlc6h0JbC1qv6ka9UNwNub8tuB63tsfgdwfJJXJDkMOLfZTpKkGe3POajXAb8O/HySu5rHGcAlwBuTfJvOVXqXACR5WZIbAapqN/BO4CY6F1d8rqruPQCvQ5K0wMw6xVdVt9H7XBLA63u03w6c0bV8I3DjXAcoLXTj4+OsW7eOrVu3snz5ctauXcuqVasGPSxp4LyThDRA4+PjXHTRRezcuZOqYufOnVx00UWMj48PemjSwM36j7qD4D/qalgce+yxPPzww+zatevpuiVLljAyMsKDDz44w5bSwjHnf9SVdOBs27aNXbt2ccEFF/D5z3+eCy64gF27drFt27ZBD00aOANKGrAzzzyTyy+/nCOOOILLL7+cM888c9BDklphv/9RV9KBcfvttzMxMcGePXuYmJjg9ttvH/SQpFYwoKQBe+yxx/iFX/gFdu3axZIlSwY9HKk1nOKTBmjFihXs2rXr6Ysk9pZXrFgx4JFJg2dASQP0ne985znVS8PEKT5pgB599FEOO+wwqurpKb4kPProo4MemjRwBpQ0YE8++SSLFi0C4KmnnmLPnj0DHpHUDk7xSS3wghe8YJ9nSQaU1AqPPfbYPs+SDChJUksZUJKkVjKgJEmtZEBJklrJgJIktZIBJUlqJQNKktRKBpQkqZVmvdVRkquAM4FHqurEpu6zwCubJkcB/1hVJ/fY9gHge8AeYHevj/SVJKmX/bkX39XAZcCn9lZU1a/uLSe5FHh8hu1XVtV35zpASdJwmjWgqurWJMf1WpckwFuBn5/ncUmShly/56B+Fni4qr49zfoCbk6yKcmaPvuSJA2Rfj9uYxUwPsP611XV9iQvBW5J8o2qurVXwybA1gCMjIwwOTnZ59CkQ5vvAQ27VNXsjTpTfF/Ye5FEU7cY+A7w6qrath/7eD+wo6o+PFvb0dHR2rhx46zjkg51nVny3vbnvSktBEk29bqIrp8pvjcA35gunJIcnuTIvWXgTcCWPvqTJA2RWQMqyTjwN8Ark2xLsrpZdS5TpveSvCzJjc3iCHBbkruB24EvVtWX52/okqSFbH+u4ls1Tf15Peq2A2c05fuBk/ocnyRpSHknCUlSKxlQkqRWMqAkSa1kQEmSWsmAkiS1kgElSWolA0qS1EoGlCSplQwoSVIrGVCSpFYyoCRJrWRASZJayYCSJLWSASVJaiUDSpLUSgaUJKmVDChJUisZUJKkVjKgJEmtZEBJklrJgJIktdKsAZXkqiSPJNnSVff+JN9JclfzOGOabU9L8s0k9yW5eD4HLkla2PbnCOpq4LQe9f+5qk5uHjdOXZlkEfBnwOnACcCqJCf0M1hJ0vCYNaCq6lbg0Tns+xTgvqq6v6qeBD4DnD2H/UiShtDiPrZ9Z5LfADYC766qx6asPwZ4sGt5G/Ca6XaWZA2wBmBkZITJyck+hiYd+nwPaNjNNaA+BvwHoJrnS4HfnNImPbar6XZYVeuB9QCjo6M1NjY2x6FJC4PvAQ27OV3FV1UPV9WeqnoK+Did6byptgHHdi0vA7bPpT9J0vCZU0AlObpr8c3Alh7N7gCOT/KKJIcB5wI3zKU/SdLwmXWKL8k4MAYsTbINeB8wluRkOlN2DwC/1bR9GfCJqjqjqnYneSdwE7AIuKqq7j0QL0KStPCkatrTQgMzOjpaGzduHPQwpAMu6XWqtqON703pQEiyqapGp9Z7JwlJUisZUJKkVjKgJEmtZEBJklrJgJIktZIBJUlqJQNKktRKBpQkqZUMKElSKxlQkqRW6ufzoCT1MNPtiw7UfrwtkhYiA0qaZ88lLLwXnzQ9p/gkSa1kQEkDNN1RkkdPkgElDVxVUVW8/Pe+8HRZkgElSWopA0qS1EoGlCSplQwoSVIrGVCSpFaaNaCSXJXkkSRbuur+U5JvJNmc5LokR02z7QNJ7klyV5KN8zhuSdICtz9HUFcDp02puwU4sapeBXwL+P0Ztl9ZVSdX1ejchihJGkazBlRV3Qo8OqXu5qra3Sx+DVh2AMYmSRpi83Evvt8EPjvNugJuTlLAn1fV+ul2kmQNsAZgZGSEycnJeRiadGjx5156Rl8BlWQtsBv49DRNXldV25O8FLglyTeaI7JnacJrPcDo6GiNjY31MzTp0PPlL+LPvfSMOV/Fl+TtwJnAr9U092apqu3N8yPAdcApc+1PkjRc5hRQSU4Dfg84q6q+P02bw5McubcMvAnY0qutJElTzTrFl2QcGAOWJtkGvI/OVXvPpzNtB/C1qjo/ycuAT1TVGcAIcF2zfjFwTVV9+YC8CukAOOkDN/P4D3Yd1D6Pu/iLB7W/H/3hJdz9vjcd1D6l/TVrQFXVqh7VV07TdjtwRlO+Hzipr9FJA/T4D3bxwCW/eND6m5ycPOjnoA52IErPhXeSkCS1kgElSWolA0qS1Erz8Y+60oJ05PKLWfHJiw9up588uN0duRzg4J1nk54LA0qaxve2XuJFEtIAOcUnSWolA0qS1EoGlCSplQwoSVIreZGENIODfhHBlw/+rY6ktjKgpGkczCv4oBOGB7tPqc2c4pMktZIBJUlqJQNKktRKBpQkqZUMKElSKxlQkqRWMqAkSa1kQEmSWsmAkiS10qwBleSqJI8k2dJV96IktyT5dvP8wmm2PS3JN5Pcl+Qgf/KbJOlQtj9HUFcDp02puxj4SlUdD3ylWd5HkkXAnwGnAycAq5Kc0NdoJUlDY9aAqqpbgUenVJ/NMx9O/Ungl3tsegpwX1XdX1VPAp9ptpMkaVZzvVnsSFU9BFBVDyV5aY82xwAPdi1vA14zx/6kBSvJM+UPdp6rakCjkdrjQN7NPD3qpn3XJVkDrAEYGRlhcnLyAA1Lao+VK1f2rE/CxMTEQR6N1C5zDaiHkxzdHD0dDTzSo8024Niu5WXA9ul2WFXrgfUAo6OjNTY2NsehSQuD7wENu7leZn4D8Pam/Hbg+h5t7gCOT/KKJIcB5zbbSQtakv1+HIz9SIeq/bnMfBz4G+CVSbYlWQ1cArwxybeBNzbLJHlZkhsBqmo38E7gJmAr8LmquvfAvAypPapqvx8HYz/SoWrWKb6qWjXNqtf3aLsdOKNr+UbgxjmPTpI0tLyThCSplQwoSVIrGVCSpFYyoCRJrWRASZJayYCSJLWSASVJaiUDSpLUSgaUJKmVDChJUisZUJKkVjKgJEmtZEBJklrJgJIktZIBJUlqJQNKktRKBpQkqZUMKElSKxlQkqRWMqAkSa0054BK8sokd3U9/inJu6a0GUvyeFeb9/Y9YknSUFg81w2r6pvAyQBJFgHfAa7r0fSrVXXmXPuRJA2n+Zriez3wv6vq7+dpf5KkITdfAXUuMD7NutcmuTvJl5L8xDz1J0la4OY8xbdXksOAs4Df77H6TuDlVbUjyRnAXwLHT7OfNcAagJGRESYnJ/sdmnRI8z2gYZeq6m8HydnAO6rqTfvR9gFgtKq+O1O70dHR2rhxY1/jkg4FSaZd1+97UzpUJNlUVaNT6+djim8V00zvJfmxNO/AJKc0/f3DPPQpSVrg+priS/IjwBuB3+qqOx+gqq4A3gJckGQ38APg3PLPQknSfugroKrq+8CLp9Rd0VW+DLisnz4kScPJO0lIklrJgJIktZIBJUlqJQNKktRKBpQkqZUMKElSKxlQkqRWMqAkSa1kQEmSWsmAkiS1kgElSWolA0qS1EoGlCSplQwoSVIrGVCSpFYyoCRJrWRASZJayYCSJLWSASVJaiUDSpLUSn0FVJIHktyT5K4kG3usT5KPJrkvyeYkP9VPf5Kk4bF4Hvaxsqq+O82604Hjm8drgI81z5IkzehAT/GdDXyqOr4GHJXk6APcpyRpAeg3oAq4OcmmJGt6rD8GeLBreVtTJ0nSjPqd4ntdVW1P8lLgliTfqKpbu9anxzbVa0dNwK0BGBkZYXJyss+hSYc23wMadqnqmRfPfUfJ+4EdVfXhrro/ByararxZ/iYwVlUPzbSv0dHR2rjxWddcSAtO0utvuI75em9KbZdkU1WNTq2f8xRfksOTHLm3DLwJ2DKl2Q3AbzRX8/008Phs4SRJEvQ3xTcCXNf8BbgYuKaqvpzkfICqugK4ETgDuA/4PvBv+huuJGlYzDmgqup+4KQe9Vd0lQt4x1z7kCQNL+8kIUlqJQNKktRKBpQkqZUMKElSKxlQUgs873nP2+dZkgEltcJTTz21z7MkA0qS1FIGlCSplQwoSVIrGVCSpFYyoCRJrWRASZJayYCSJLWSASVJaiUDSpLUSgaU1AKXXnopX/rSl7j00ksHPRSpNfr5RF1J8+Td7373oIcgtY5HUNIALVu2jMWL9/07cfHixSxbtmxAI5LawyMoacCWLl3KNddcw549e1i0aBFve9vbBj0kqRUMKGmAtm/fztVXX82FF17I1q1bWb58OR/60Ic477zzBj00aeCc4pMGaPny5SxbtowtW7bwla98hS1btrBs2TKWL18+6KFJAzfngEpybJKJJFuT3Jvkoh5txpI8nuSu5vHe/oYrLSxr165l9erVTExMsHv3biYmJli9ejVr164d9NCkgetnim838O6qujPJkcCmJLdU1d9NaffVqjqzj36kBWvVqlUA+0zxrVu37ul6aZjNOaCq6iHgoab8vSRbgWOAqQElaQarVq1i1apVTE5OMjY2NujhSK0xLxdJJDkO+Eng6z1WvzbJ3cB24D1Vde80+1gDrAEYGRlhcnJyPoYmtd7KlSufVTcxMTGAkUjtkqrqbwfJEcBfA+uqasOUdS8AnqqqHUnOAD5SVcfPts/R0dHauHFjX+OSDgVJpl3X73tTOlQk2VRVo1Pr+7qKL8kS4Frg01PDCaCq/qmqdjTlG4ElSZb206ckaTj0cxVfgCuBrVX1J9O0+bGmHUlOafr7h7n2KS1UZ511Ftdddx1nnXXWoIcitUY/56BeB/w6cE+Su5q6PwB+HKCqrgDeAlyQZDfwA+Dcct5CepabbrqJG264gec///mDHorUGv1cxXcbMP0EeqfNZcBlc+1DGhZPPPEEJ510EnffffeghyK1hneSkFrCcJL2ZUBJklrJgJIGaLpzTp6LkgwoaaCeeOKJp8vdd5HorpeGlQEltYR3T5H2ZUBJLeD/QUnPZkBJLXD99ddz1FFHcf311w96KFJrGFBSCyRh3bp1M96bTxo2BpTUEn/1V3816CFIrWJASQO0YsUK4NnnoPbWS8Os74/bOBD8uA0Nk1e96lXcc889Ty+vWLGCzZs3D3BE0sF1QD5uQ1L/Nm/eTFUxMTFBVRlOUsOAkgZsfHycE088kde//vWceOKJjI+PD3pIUivMy0e+S5qb8fFx1q5dy5VXXsmePXtYtGgRq1evBmDVqlUDHp00WB5BSQO0bt06rrzySlauXMnixYtZuXIlV155JevWrRv00KSBM6CkAdq6dSunnnrqPnWnnnoqW7duHdCIpPYwoKQBWr58Obfddts+dbfddhvLly8f0Iik9jCgpAFau3Ytq1evZmJigt27dzMxMcHq1atZu3btoIcmDZwXSUgDtPdCiAsvvJCtW7eyfPly1q1b5wUSEv6jrtQak5OT+3wmlDQsDsg/6iY5Lck3k9yX5OIe65Pko836zUl+qp/+pIUoCUlYuXLl02VJfQRUkkXAnwGnAycAq5KcMKXZ6cDxzWMN8LG59ictRN1hdM455/Ssl4ZVP0dQpwD3VdX9VfUk8Bng7CltzgY+VR1fA45KcnQffUoLUlVx4YUX0sYpd2lQ+rlI4hjgwa7lbcBr9qPNMcBDU3eWZA2doyxGRkb8+GsNjXPOOYfJyUl27NjB5OQk55xzDhs2bPA9oKHXT0D1moOY+uff/rTpVFatB9ZD5yIJTxZrWGzYsIFrr7326YskVq5cCeAFExp6/UzxbQOO7VpeBmyfQxtp6CXhT//0Tz33JHXpJ6DuAI5P8ookhwHnAjdMaXMD8BvN1Xw/DTxeVc+a3pOGVfc5pw0bNvSsl4bVnAOqqnYD7wRuArYCn6uqe5Ocn+T8ptmNwP3AfcDHgd/uc7zSglNV+3welOEkdfR1J4mqupFOCHXXXdFVLuAd/fQhSRpO3otPktRKBpQkqZUMKElSKxlQkqRWMqAkSa1kQEmSWqmVnweV5P8Bfz/ocUgH2VLgu4MehDQAL6+ql0ytbGVAScMoycZeH9omDSun+CRJrWRASZJayYCS2mP9oAcgtYnnoCRJreQRlCSplQwotVKSdyX5kXnc3x/M176a/Z2X5GXzuc9Z+ptMMi9X+CV5IMnSWdrsaJ6PS7JlPvqdoa+xJD9zIPvQocmAUlu9C+gZUEkWzWF/zzmgksz0cTTnAQctoA4Vc/zejAEGlJ7FgNLAJTk8yReT3J1kS5L30fnlP5FkommzI8kfJvk68Nruo4Ako0kmm/IRSf5rknuSbE7yK0kuAX44yV1JPj31qCDJe5K8vylPJvmPSf4auCjJq5P8dZJNSW5KcnSStwCjwKebff5wkvcmuaMZ//rmU6QXN3Vjzb7/OMm6JKcn+VxX/2NJPt+UP5ZkY5J7k3xgmq/Xjq7yW5Jc3ZRfkuTaps87kryuqX9xkpuT/G2SPwfStf2/b8a8Jcm7Zvk+HZfkq0nubB4/0zX+iSTXAPckWZTkw13fgwubdg8k+UCz7T1J/mWS44Dzgd9pvpY/O9MYNGT2foKnDx+DegC/Any8a/lHgQeApV11Bby1a/np9XTCYrIpfxD4L13tXtg87+iqOw7Y0rX8HuD9TXkSuLwpLwH+F/CSZvlXgau62o127eNFXeW/AH6pKf8EnU+cfiPwt8BhdD4o9P8ChzdtPgb86+79AIuaPl41tb8pr+UtwNVN+Rrg1Kb848DWpvxR4L1N+Rebr+VS4NXAPcDhwBHAvcBPdvfR/bWic0T7Q035eGBjUx4DdgKvaJYvAK4FFk95TQ8AFzbl3wY+0ZTfD7xn0D+HPtr36OsTdaV5cg/w4SQfBL5QVV9NMrXNHjq/9GbzBuDcvQtV9dgcxvPZ5vmVwInALc14FgEPTbPNyiS/S+eX+Ivo/LL/fFXdm+QvgM8Dr62qJwGSfBn4pST/nU5o/G6zn7cmWUMnxI4GTgA27+e43wCc0PW1e0GSI4GfA84BqKovJtn7NTkVuK6qdjZj2gD8LJ0g7WUJcFmSk+l8P/5F17rbq+r/dI3jiqra3fT5aFe7Dc3zpr1jkqZjQGngqupbSV4NnAH8cZKbezT756ra07W8m2emqH+oqz50jhBm0r3t1O2hczSwd1/3VtVrZ9pZkh8CLqdzhPNgM13Yvc8VwD8CI111nwXeATwK3FFV30vyCjpHc/+qqh5rpu6mjg32fX3d659HJwR/MGV8U7d5etVMr6uH3wEeBk5q+vrnrnU7u8ozfQ+eaJ734O8fzcJzUBq4dK6G+35V/Tfgw8BPAd8DjpxhswfoTFFBZ4pwr5uBd3bt+4VNcVeSJU35YeClzbmZ5wNnTtPHN4GXJHlts68lSX6iWdc9vr0h8d0kR9CZdtvb/znAi+kcxXw0yVHNqsnmdf5bnjliewGdX/SPJxkBTp9mXA8nWZ7kecCbZ3jtJzfFW4Ffa+pOB17YVf/LSX4kyeHNvr46TZ/QmXp9qKqeAn6dzhFlLzcD56e5yCTJi2bYJ8z+vdaQMqDUBiuA25PcBawF/ojOXRW+lOYiiR4+AHwkyVfp/DW+1x8BL2xO+t8NrGzq1wObk3y6qnYBfwh8HfgC8I1eHTTTcW8BPtjs6y6eudrsauCKZsxPAB+nM1X5l8AdAOlcxHEJsLqqvgVcBnyk2feepu/Tm2eq6m4602v3AlcB/3Oa135xs83/YN8px38HjDYXJvwdnYsP9n6tfi7JncCb6Jz/oqrubF7H7c3X4hNVNd30HnSOEt+e5Gt0pvd2TtPuE00fm5uv29tm2Cd0pj/f7EUSmso7SUiSWskjKElSKxlQkqRWMqAkSa1kQEmSWsmAkiS1kgElSWolA0qS1EoGlCSplf4/4flTiMspGb4AAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAXgUlEQVR4nO3df5RfdX3n8ecrCWCBHAGRMSA1HpuqERS7U7TYygCVX6uN2+oWTg9iyyElq2vduhxp3Va7W/fopto9Li4YLMcf1aCuxeYoKlQzoq5YJiwEMIBZGtc0KAUEE1BDMu/9Y25wGL6Tmcx3Mt+b+T4f58z5fu/nfu79vL8cbl7zud8796aqkCSpbRb0ugBJkjoxoCRJrWRASZJayYCSJLWSASVJaqVFvS6gk6OPPrqWLl3a6zKkOfXoo49y2GGH9boMac5t2LDhgap65sT2VgbU0qVLGRkZ6XUZ0pwaHh5maGio12VIcy7J9zq1e4pPktRKBpQkqZWmDKgkxydZn2RTkjuT/FHTflSSG5J8t3k9cpLtz05yd5LNSS6b7Q8gSZqfpjOD2gW8rapeCLwceFOS5cBlwFeqahnwlWb5SZIsBD4InAMsB85vtpUkaa+mDKiquq+qbmnebwc2AccBK4CPNt0+Cry2w+YnA5ur6t6q2glc02wnSdJe7dNVfEmWAi8Fvg0MVNV9MBZiSY7psMlxwPfHLW8FXjbJvlcCKwEGBgYYHh7el9KkA96OHTv8/14aZ9oBleRw4LPAW6vqx0mmtVmHto63T6+qNcAagMHBwfJyW/UbLzOXnmxaV/ElOYixcPpEVf1d0/zDJEua9UuA+ztsuhU4ftzys4FtMy9Xmn/Wrl3LCSecwBlnnMEJJ5zA2rVre12S1ArTuYovwN8Am6rq/eNWrQMubN5fCPx9h81vBpYleW6Sg4Hzmu0kMRZOl1xyCffccw+jo6Pcc889XHLJJYaUBGSqBxYm+XXg68DtwGjT/KeMfQ/1aeAXgf8HvL6qHkpyLPDhqjq32f5c4L8DC4Grq+rdUxU1ODhY3klC/eAZz3gGDz30EAsXLmT37t1PvB511FE8+OCDvS5PmhNJNlTV4FPa2/hEXQNK/SIJCxYsYPXq1SxfvpzvfOc7XHrppYyOjtLGY1PaHyYLqFbei0/qJ6eeeipXX301mzZt4oUvfCGnnnoq69ev73VZUs8ZUFKPfe1rX3vKDEqSASX13OjoKKtXr+b+++/nmGOOYXR0dOqNpD5gQEk9lISq4gc/+AHAE6/T/DtDaV7zbuZSDx15ZMd7LE/aLvUTA0rqoYcffnif2qV+YkBJPTTZ901+DyUZUJKkljKgpBbYc1GEF0dIP2dASS2w564R3j1C+jkDSpLUSgaUJKmVDChJUisZUJKkVjKgJEmtZEBJklrJgJIktZIBJUlqJQNKktRKBpQkqZWmfGBhkquBVwP3V9UJTdungOc3XY4AHq6qkzpsuwXYDuwGdlXV4KxULUma96bzRN2PAJcDH9vTUFW/u+d9kvcBj+xl+9Oq6oGZFihJ6k9TBlRV3Zhkaad1Gbv18r8FTp/luiRJfW46M6i9+Q3gh1X13UnWF3B9kgI+VFVrJttRkpXASoCBgQGGh4e7LE06sHkMqN9lOrf3b2ZQn9/zHdS49iuAzVX1vkm2O7aqtiU5BrgB+PdVdeNU4w0ODtbIyMh06pcOaHt7/pOP3lC/SLKh0zUKM76KL8ki4LeBT03Wp6q2Na/3A9cCJ890PElSf+nmMvPfBO6qqq2dViY5LMniPe+BM4E7uhhPktRHpgyoJGuBbwHPT7I1yUXNqvOAtRP6HpvkumZxAPhGktuAfwS+UFVfmr3SJUnz2XSu4jt/kvY3dmjbBpzbvL8XeEmX9UmS+pR3kpAktZIBJUlqJQNKktRKBpQkqZUMKElSKxlQkqRWMqAkSa1kQEmSWsmAkiS1kgElSWolA0qS1EoGlCSplQwoSVIrGVCSpFYyoCRJrWRASZJayYCSJLWSASVJaiUDSpLUSgaUJKmVpgyoJFcnuT/JHePa3pXkn5Pc2vycO8m2Zye5O8nmJJfNZuGSpPltOjOojwBnd2j/66o6qfm5buLKJAuBDwLnAMuB85Ms76ZYSVL/mDKgqupG4KEZ7PtkYHNV3VtVO4FrgBUz2I8kqQ8t6mLbNyd5AzACvK2qfjRh/XHA98ctbwVeNtnOkqwEVgIMDAwwPDzcRWnSgc9jQP1upgF1BfBfgGpe3wf8wYQ+6bBdTbbDqloDrAEYHBysoaGhGZYmzQ8eA+p3M7qKr6p+WFW7q2oUuIqx03kTbQWOH7f8bGDbTMaTJPWfGQVUkiXjFv8NcEeHbjcDy5I8N8nBwHnAupmMJ0nqP1Oe4kuyFhgCjk6yFXgnMJTkJMZO2W0B/rDpeyzw4ao6t6p2JXkz8GVgIXB1Vd25Pz6E1CZJp7Pb+3c/VZOePZcOWGnj/9iDg4M1MjLS6zKk/W5vIdTGY1PaH5JsqKrBie3eSUKS1EoGlNRDk82SnD1JBpTUc1VFVfGct3/+ifeSDChJUksZUJKkVjKgJEmtZEBJklrJgJIktZIBJUlqJQNKktRKBpQkqZUMKElSKxlQkqRWMqAkSa1kQEmSWsmAkiS10pRP1JX61Uv+4noe+cnjczrm0su+MKfjPf0XDuK2d545p2NK02VASZN45CePs+U9/3rOxhseHmZoaGjOxoO5D0RpX3iKT5LUSgaUJKmVpgyoJFcnuT/JHePaVie5K8nGJNcmOWKSbbckuT3JrUlGZrFuSdI8l6keL53klcAO4GNVdULTdibw1araleS9AFX19g7bbgEGq+qBfSlqcHCwRkbMM/XWiR89sdclzInbL7y91yWozyXZUFWDE9unvEiiqm5MsnRC2/XjFm8CXtd1hVLLbN/0Hi+SkHpoNq7i+wPgU5OsK+D6JAV8qKrWTLaTJCuBlQADAwMMDw/PQmlSd+by/8MdO3b05P97jzW1VVcBleQdwC7gE5N0eUVVbUtyDHBDkruq6sZOHZvwWgNjp/jm+jdJ6Sm+9IU5ndH0YgY1159R2hczvoovyYXAq4Hfq0m+yKqqbc3r/cC1wMkzHU+S1F9mFFBJzgbeDvxWVT02SZ/Dkize8x44E7ijU19Jkiaa8hRfkrXAEHB0kq3AO4E/AQ5h7LQdwE1VdUmSY4EPV9W5wABwbbN+EfDJqvrSfvkU0n4y5xcRfGnub3UktdWUl5n3gpeZqx8tvewLc3rVoNQWk11m7p0kJEmtZEBJklrJgJIktZIBJUlqJQNKktRKBpQkqZUMKElSKxlQkqRWMqAkSa1kQEmSWsmAkiS1kgElSWolA0qS1Eqz8ch3SV1oHkkz9v69Y69tfMqANNecQUk9ND6cptMu9RMDSpLUSp7ik2bZbM1+9mU/nhLUfGRASbNsX8JibyFk6KjfeYpPktRKUwZUkquT3J/kjnFtRyW5Icl3m9cjJ9n27CR3J9mc5LLZLFySNL9NZwb1EeDsCW2XAV+pqmXAV5rlJ0myEPggcA6wHDg/yfKuqpUk9Y0pA6qqbgQemtC8Avho8/6jwGs7bHoysLmq7q2qncA1zXaSJE1ppt9BDVTVfQDN6zEd+hwHfH/c8tamTZKkKe3Pq/g6XZ406WVJSVYCKwEGBgYYHh7eT2VJBwaPAfW7mQbUD5Msqar7kiwB7u/QZytw/LjlZwPbJtthVa0B1gAMDg7W0NDQDEuT5gePAfW7mZ7iWwdc2Ly/EPj7Dn1uBpYleW6Sg4Hzmu0kSZrSdC4zXwt8C3h+kq1JLgLeA7wqyXeBVzXLJDk2yXUAVbULeDPwZWAT8OmqunP/fAxJ0nwz5Sm+qjp/klVndOi7DTh33PJ1wHUzrk6S1Le8k4QkqZUMKElSKxlQkqRWMqAkSa1kQEmSWsmAkiS1kgElSWolA0qS1EoGlCSplQwoSVIrGVCSpFYyoCRJrWRASZJayYCSJLWSASVJaiUDSpLUSgaUJKmVDChJUisZUJKkVjKgJEmtNOOASvL8JLeO+/lxkrdO6DOU5JFxff6864olSX1h0Uw3rKq7gZMAkiwE/hm4tkPXr1fVq2c6jiSpP83WKb4zgP9bVd+bpf1JkvrcbAXUecDaSdb9WpLbknwxyYtmaTxJ0jw341N8eyQ5GPgt4E86rL4FeE5V7UhyLvA5YNkk+1kJrAQYGBhgeHi429KkA5rHgPpdqqq7HSQrgDdV1ZnT6LsFGKyqB/bWb3BwsEZGRrqqSzoQJJl0XbfHpnSgSLKhqgYnts/GKb7zmeT0XpJnpTkCk5zcjPfgLIwpSZrnujrFl+RQ4FXAH45ruwSgqq4EXgesSrIL+AlwXvlroSRpGroKqKp6DHjGhLYrx72/HLi8mzEkSf3JO0lIklrJgJIktZIBJUlqJQNKktRKBpQkqZUMKElSKxlQkqRWMqAkSa1kQEmSWsmAkiS1kgElSWolA0qS1EoGlCSplQwoSVIrGVCSpFYyoCRJrWRASZJayYCSJLWSASVJaiUDSpLUSl0FVJItSW5PcmuSkQ7rk+QDSTYn2ZjkV7oZT5LUPxbNwj5Oq6oHJll3DrCs+XkZcEXzKknSXu3vU3wrgI/VmJuAI5Is2c9jSpLmgW4DqoDrk2xIsrLD+uOA749b3tq0SZK0V92e4ntFVW1LcgxwQ5K7qurGcevTYZvqtKMm4FYCDAwMMDw83GVp0oHNY0D9LlUd82Lfd5S8C9hRVX81ru1DwHBVrW2W7waGquq+ve1rcHCwRkaecs2FNO8knX6HGzNbx6bUdkk2VNXgxPYZn+JLcliSxXveA2cCd0zotg54Q3M138uBR6YKJ0mSoLtTfAPAtc1vgIuAT1bVl5JcAlBVVwLXAecCm4HHgN/vrlxJUr+YcUBV1b3ASzq0XznufQFvmukYkqT+5Z0kJEmtZEBJklrJgJIktZIBJUlqJQNK6qGjjjpqn9qlfmJAST300EMP7VO71E9m427mkrr01a9+ld27d7Nw4UJOP/30XpcjtYIBJfXY0qVLOeecc/jZz37GIYccwtKlS9myZUuvy5J6zlN8Uo9t2bKFs846i2uvvZazzjrLcJIas3az2NnkzWLVL7xZrLQfbhYrSdL+ZEBJklrJgJIktZIBJUlqJQNKktRKBpQkqZUMKElSKxlQkqRWMqAkSa1kQEmSWmnGAZXk+CTrk2xKcmeSP+rQZyjJI0lubX7+vLtyJUn9opu7me8C3lZVtyRZDGxIckNVfWdCv69X1au7GEeS1IdmPIOqqvuq6pbm/XZgE3DcbBUm9YMFCzofgpO1S/1kVo6CJEuBlwLf7rD615LcluSLSV40G+NJ88Xo6Og+tUv9pOsHFiY5HPgs8Naq+vGE1bcAz6mqHUnOBT4HLJtkPyuBlQADAwMMDw93W5p0QPMYUL/r6nlQSQ4CPg98uareP43+W4DBqnpgb/18HpT6hc+DkvbD86AydmT9DbBpsnBK8qymH0lObsZ7cKZjSvPZu9/97l6XILVKN6f4XgFcANye5Nam7U+BXwSoqiuB1wGrkuwCfgKcV/5aKHX0jne8o9clSK0y44Cqqm8Ak5+fGOtzOXD5TMeQJPUvr2WVJLWSASVJaiUDSpLUSgaU1AJHHnkkV111FUceeWSvS5Fao+s/1JXUvR/96EdcfPHFvS5DahVnUFILnHLKKXzmM5/hlFNO6XUpUms4g5J6bNGiRWzYsIHXv/71HHLIISxatIhdu3b1uiyp55xBST22a9culixZwsc//nGWLFliOEkNZ1BSDyWhqtiyZQsXXHDBk9qlfucMSuqhhQsXAk+9im9Pu9TPnEFJPbRr1y4OPfRQHn74YS6++GKScOihh/LYY4/1ujSp55xBST22adMmRkdHWb9+PaOjo2zatKnXJUmt4AxK6rFly5axc+fOJ5YPPvjgHlYjtYczKKmHFixYwM6dOzn88MO54oorOPzww9m5cycLFnhoSs6gpB4aHR3loIMOYseOHaxatQqAgw46iMcff7zHlUm9569pUo9t27aNqmL9+vVUFdu2bet1SVIrGFBSj1100UV7XZb6lQEl9dCJJ57IunXrWLFiBQ8//DArVqxg3bp1nHjiib0uTeq5VFWva3iKwcHBGhkZ6XUZ0px48YtfzO233/7E8oknnsjGjRt7WJE0t5JsqKrBie3OoKQe27hx45O+gzKcpDFdBVSSs5PcnWRzkss6rE+SDzTrNyb5lW7Gk+ajhQsXkoTTTjuNJN7mSGrMOKCSLAQ+CJwDLAfOT7J8QrdzgGXNz0rgipmOJ81HCxcuZHR09El/BzU6OmpISXQ3gzoZ2FxV91bVTuAaYMWEPiuAj9WYm4AjkizpYkxpXtkTTtu3b+cFL3gB27dvfyKkpH7XzR/qHgd8f9zyVuBl0+hzHHDfxJ0lWcnYLIuBgQGGh4e7KE06cKxevZrh4WF27NjB8PAwq1evZtWqVR4D6nvdBFSnB9ZMvCRwOn3GGqvWAGtg7Cq+oaGhLkqTDhyXXnop27dvZ3h4mKGhIV7zmtcA4DGgftfNKb6twPHjlp8NTPwT+On0kfrWggUL2LFjB4sXL+auu+5i8eLF7Nixw3vxSXQXUDcDy5I8N8nBwHnAugl91gFvaK7meznwSFU95fSe1K927979REitWrXqiXDavXt3r0uTem7GAVVVu4A3A18GNgGfrqo7k1yS5JKm23XAvcBm4Crg33VZrzTv7N69+0l/B2U4SWO6upt5VV3HWAiNb7ty3PsC3tTNGJKk/uSJbklSKxlQkqRWMqAkSa1kQEmSWsmAkiS1kgElSWqlVj6wMMm/AN/rdR3SHDsaeKDXRUg98JyqeubExlYGlNSPkox0eqqo1K88xSdJaiUDSpLUSgaU1B5rel2A1CZ+ByVJaiVnUJKkVjKgJEmtZEBpXktyRJI5eQ5ZkqVJ7pilfQ0l+fwUfd6Y5PLm/buS/MfZGHsv4701yaH7cwxpPANK890R+KDMp0gyk2fBvRUwoDRnDCjNd+8Bnpfk1iR/neQrSW5JcnuSFQBJfjXJxiRPS3JYkjuTnJDkU0nO3bOjJB9J8jvNTOnrzX5uSXLKxEHHz26a5c8nGWren5nkW822n0lyeNN+dpK7knwD+O1x2x6V5HNNjTclefHePnCSi5PcnOS2JJ/dM+tp6n9/kvXAe5P8UpJ/aPrdkuR5zcxtOMn/amr5RMa8BTgWWN9sL+13XT1RVzoAXAacUFUnNbOGQ6vqx0mOBm5Ksq6qbk6yDvhL4BeAv62qO5JcA/wucF2Sg4EzgFVAgFdV1U+TLAPWAtO6A0Qz7n8CfrOqHk3yduCPk/w34CrgdGAz8Klxm/0F8H+q6rVJTgc+Bpy0l2H+rqquasb7S+Ai4H806365GXt3km8D76mqa5M8jbFfWI8HXgq8CNgGfBN4RVV9IMkfA6dVlbdj0pwwoNRPAvzXJK8ERoHjgAHgB8B/Bm4Gfgq8pen/ReADSQ4BzgZurKqfJHk6cHmSk4DdjP2jP10vB5YD30wCcDDwLeAFwD9V1XcBkvwtsLLZ5teB3wGoqq8meUZTw2ROaILpCOBw4Mvj1n2mCafFwHFVdW2z35824wL8Y1VtbZZvBZYC39iHzyjNCgNK/eT3gGcC/6qqHk+yBXhas+4oxv4xP6hpe7SZIQ0DZzE2k1rb9P0PwA+BlzA26/hph7F28eRT6HvGCXBDVZ0/vnMTdpP9UWI6tO3tDxg/Ary2qm5L8kZgaNy6R/eyzz1+Nu79bvx3Qj3id1Ca77YDi5v3Twfub8LpNOA54/qtAf4M+ATw3nHt1wC/D/wGP5+JPB24r6pGgQuAhR3G3QKclGRBkuOBk5v2m4BXJPklgCSHJvll4C7guUme1/QbH2A3MhauNN9jPVBVP97LZ14M3JfkoD3bTdRsvzXJa5v9HjKNK/TG/7eU9jsDSvNaVT3I2Om0Oxj73mYwyQhj/3DfBZDkDcCuqvokYxdV/GrzXQ/A9cArgX+oqp1N2/8ELkxyE2On9/bMSsb7JvBPwO3AXwG3NPX8C/BGYG2SjYwF1guaU2wrgS80F0mMf9zMu5q6Nzb1XTjFx/4z4NvADXs+4yQuAN7S7Pd/A8+aYr9rgC96kYTmirc6kiS1kjMoSVIrGVCSpFYyoCRJrWRASZJayYCSJLWSASVJaiUDSpLUSv8f2QTIp6bjiPUAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAVt0lEQVR4nO3df5BdZ33f8fdHMq7BGBvHsLGFjTyNx4ODwA07hkDjrEvw2CqRkg4N9tDwyx1BGtpmAhRPNcOPZNw49TgpqSlG1C6mkxqTSdSo2BgD8Vb8MIGVkSW5hqIaGQkxKIZgEL8lffvHHuGr9V392Lva+6zu+zVz557znOec57nXOv7s89xzz01VIUlSa5YMuwOSJPVjQEmSmmRASZKaZEBJkppkQEmSmnTSsDvQz1lnnVXLly8fdjekBfX973+fU089ddjdkBbcpk2bHq2qZ8wsbzKgli9fztTU1LC7IS2oyclJJiYmht0NacEleaRfuVN8kqQmGVCSpCYZUJKkJhlQkqQmGVCSpCYZUJKkJhlQkqQmNfk9KGmUnHfeeezcufNn6+eeey5f+9rXhtgjqQ2OoKQhOhhOp5xyCgCnnHIKO3fu5Lzzzhtyz6ThM6CkITo4cvrRj350yHPviEoaVQaU1IBVq1axfv16Vq1aNeyuSM1Iiz/5Pj4+Xt6LT6MgCUuXLuXAgQNUFUlYsmQJ+/fvp8VzUzoekmyqqvGZ5V4kIQ3Z/v37f7ZcVYesS6PMKT5JUpMMKElSkwwoSVKTDChJUpOOeJFEkluBlwN7quq5XdkdwIVdlTOA71TVxX323QF8D9gP7Ot3lYYkSf0czVV8HwBuAj54sKCqXnlwOcmNwGOH2f+yqnp0rh2UJI2mIwZUVW1MsrzftiQBfgv4J/PcL0nSiBv0e1C/Anyzqr4yy/YC7klSwPuqat1sB0qyBlgDMDY2xuTk5IBdkxY3zwGNuqO6k0Q3gvrIwc+gesrfC2yvqhtn2e+cqtqd5JnAx4F/XVUbj9Sed5LQqJiehOjPO0loVMx2J4k5X8WX5CTgnwF3zFanqnZ3z3uA9cAlc21PkjRaBrnM/NeAL1XVrn4bk5ya5LSDy8DlwLYB2pMkjZAjBlSS24H7gAuT7EpyTbfpKuD2GXXPSXJXtzoGfDrJA8DngTur6u7567ok6UR2NFfxXT1L+Wv7lO0GVnbLDwPPH7B/kqQR5Z0kJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElNOmJAJbk1yZ4k23rK3pnk60k2d4+Vs+x7RZIvJ9me5Nr57Lgk6cR2NCOoDwBX9Cn/06q6uHvcNXNjkqXAe4ArgYuAq5NcNEhnJUmj44gBVVUbgW/P4diXANur6uGq+gnwIWD1HI4jSRpBJw2w75uSvBqYAt5cVX8/Y/syYGfP+i7ghbMdLMkaYA3A2NgYk5OTA3RNWvw8BzTq5hpQ7wX+EKju+Ubg9TPqpM9+NdsBq2odsA5gfHy8JiYm5tg16cTgOaBRN6er+Krqm1W1v6oOAO9nejpvpl3AuT3rzwJ2z6U9SdLomVNAJTm7Z/U3gW19qn0BuCDJ+UlOBq4CNsylPWkxSXLUj4U4jrRYHXGKL8ntwARwVpJdwDuAiSQXMz1ltwN4Q1f3HOC/VtXKqtqX5E3Ax4ClwK1V9eDxeBFSS6pmncl+gsOFy7EcRzoRpcWTYHx8vKampobdDem4M6AkSLKpqsZnlnsnCWmIZgshw0kyoKShqyqqime/7SM/W5ZkQEmSGmVASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKadMSASnJrkj1JtvWU3ZDkS0m2JFmf5IxZ9t2RZGuSzUmm5rHfkqQTXKrq8BWSS4G9wAer6rld2eXA31TVviR/DFBVb+uz7w5gvKoePZZOjY+P19SUeabhev677uGxH/502N04rk5/8pN44B2XD7sbGnFJNlXV+Mzyk460Y1VtTLJ8Rtk9PaufA14xcA+lxjz2w5+y4/p/umDtTU5OMjExsWDtASy/9s4FbU86FkcMqKPweuCOWbYVcE+SAt5XVetmO0iSNcAagLGxMSYnJ+eha9JgFvLf4d69e4fy795zTa0aKKCSrAX2AX8+S5WXVNXuJM8EPp7kS1W1sV/FLrzWwfQU30L/JSk9wd13LuiIZhgjqIV+jdKxmPNVfEleA7wceFXN8kFWVe3unvcA64FL5tqeJGm0zCmgklwBvA1YVVU/mKXOqUlOO7gMXA5s61dXkqSZjuYy89uB+4ALk+xKcg1wE3Aa09N2m5Pc3NU9J8ld3a5jwKeTPAB8Hrizqu4+Lq9CknTCOZqr+K7uU3zLLHV3Ayu75YeB5w/UO0nSyJqPq/ikE9Jpz7mWFbddu7CN3rawzZ32HICFu5ReOhYGlDSL7z10vd+DkobIe/FJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkprk96Ckw1jw7wndvbDtnf7kJy1oe9KxMKCkWSzkl3RhOgwXuk2pZU7xSZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJkpp0xIBKcmuSPUm29ZSdmeTjSb7SPT99ln2vSPLlJNuTXDufHZckndiOZgT1AeCKGWXXAp+sqguAT3brh0iyFHgPcCVwEXB1kosG6q0kaWQcMaCqaiPw7RnFq4HbuuXbgN/os+slwPaqeriqfgJ8qNtPkqQjmuvPbYxV1TcAquobSZ7Zp84yYGfP+i7ghbMdMMkaYA3A2NgYk5OTc+yatHj571563PH8Paj0KavZKlfVOmAdwPj4eE1MTBynbkmNuvtO/HcvPW6uV/F9M8nZAN3znj51dgHn9qw/C9g9x/YkSSNmrgG1AXhNt/wa4K/71PkCcEGS85OcDFzV7SdJ0hEdzWXmtwP3ARcm2ZXkGuB64GVJvgK8rFsnyTlJ7gKoqn3Am4CPAQ8BH66qB4/Py5AknWiO+BlUVV09y6aX9qm7G1jZs34XcNeceydJGlneSUKS1CQDSpLUpON5mbmko5A8/o2M/PH0c9Ws38iQRoYjKGmIesPpaMqlUWJASZKa5BSfNM/ma/RzLMdxSlAnIgNKmmfHEhaHCyFDR6POKT5JUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKT5hxQSS5Msrnn8d0kvzejzkSSx3rqvH3gHkuSRsKcf7Cwqr4MXAyQZCnwdWB9n6qfqqqXz7UdSdJomq8pvpcC/6+qHpmn40mSRtx8/eT7VcDts2z75SQPALuBt1TVg/0qJVkDrAEYGxtjcnJynromLU6eAxp1qarBDpCczHT4/GJVfXPGtqcBB6pqb5KVwLur6oIjHXN8fLympqYG6pe0GCSZddug56a0WCTZVFXjM8vnY4rvSuD+meEEUFXfraq93fJdwJOSnDUPbUqSTnDzEVBXM8v0XpKfT/cnYpJLuva+NQ9tSpJOcAN9BpXkKcDLgDf0lL0RoKpuBl4B/E6SfcAPgavKeQtJ0lEYKKCq6gfAz80ou7ln+SbgpkHakCSNJu8kIUlqkgElSWqSASVJapIBJUlqkgElSWqSASVJapIBJUlqkgElSWqSASVJapIBJUlqkgElSWqSASVJapIBJUlqkgElSWqSASVJapIBJUlqkgElSWqSASVJapIBJUlqkgElSWqSASVJapIBJUlq0kABlWRHkq1JNieZ6rM9Sf4syfYkW5L80iDtSZJGx0nzcIzLqurRWbZdCVzQPV4IvLd7liTpsI73FN9q4IM17XPAGUnOPs5tSpJOAIOOoAq4J0kB76uqdTO2LwN29qzv6sq+MfNASdYAawDGxsaYnJwcsGvS4uY5oFE3aEC9pKp2J3km8PEkX6qqjT3b02ef6negLtzWAYyPj9fExMSAXZMWN88BjbqBpviqanf3vAdYD1wyo8ou4Nye9WcBuwdpU5I0GuYcUElOTXLawWXgcmDbjGobgFd3V/O9CHisqp4wvSdJ0kyDTPGNAeuTHDzO/6iqu5O8EaCqbgbuAlYC24EfAK8brLuSpFEx54CqqoeB5/cpv7lnuYDfnWsbkqTR5Z0kJElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgpAYsWbKEG264gSVLPCWlg+bj5zYkDejAgQO89a1vHXY3pKb455rUgIMjJ0dQ0uM8G6QGHDhw4JBnSQaUJKlRBpTUgBtvvJGPfvSj3HjjjcPuitQML5KQGvDmN7952F2QmuMISpLUJANKktQkA0qS1CQDSmrA0qVLD3mWZEBJTdi/f/8hz5IMKElSowwoSVKTDChJUpPmHFBJzk1yb5KHkjyY5N/2qTOR5LEkm7vH2wfrriRpVAxyJ4l9wJur6v4kpwGbkny8qv7PjHqfqqqXD9COJGkEzXkEVVXfqKr7u+XvAQ8By+arY5Kk0TYvn0ElWQ78I+Bv+2z+5SQPJPlokl+cj/YkSSe+gW8Wm+SpwF8Cv1dV352x+X7g2VW1N8lK4H8CF8xynDXAGoCxsTEmJycH7Zq0qHkOaNSlqua+c/Ik4CPAx6rqT46i/g5gvKoePVy98fHxmpqamnO/pMUiyazbBjk3pcUkyaaqGp9ZPshVfAFuAR6aLZyS/HxXjySXdO19a65tSpJGxyBTfC8BfhvYmmRzV/bvgfMAqupm4BXA7yTZB/wQuKr8s1CSdBTmHFBV9Wlg9vmJ6To3ATfNtQ1J0ujyThKSpCYZUJKkJhlQUiPe8Y53DLsLUlMMKKkR73rXu4bdBakpBpQkqUkGlCSpSQaUJKlJBpTUgKri3nvv9fZGUo+BbxYraXBJuPTSS9m4ceOwuyI1wxGU1AjDSTqUASU1IAnXX3/9Ye9uLo0ap/ikBlQV11577bC7ITXFEZQ0ZI888sghF0k88sgjw+6S1AQDShqylStXHnZdGlVO8UlDdOaZZ/Lggw8+4bOnM888c0g9ktrhCEoaomXLlh1TuTRKDChpiLZu3cqqVasO+Qxq1apVbN26ddhdk4bOgJKG7JZbbjnsujSqDChpyK655prDrkujyoCShmjFihVs2LCB1atX853vfIfVq1ezYcMGVqxYMeyuSUOXFm9OOT4+XlNTU8PuhrQgnve85x3ymdOKFSvYsmXLEHskLawkm6pqfGa5IyhpyLZs2XLIRRKGkzRtoIBKckWSLyfZnuQJ92nJtD/rtm9J8kuDtCediJKQhMsuu+xny5IGCKgkS4H3AFcCFwFXJ7loRrUrgQu6xxrgvXNtTzoR9YbRdddd17dcGlWDjKAuAbZX1cNV9RPgQ8DqGXVWAx+saZ8Dzkhy9gBtSiekquLFL36xP1go9RjkVkfLgJ0967uAFx5FnWXAN2YeLMkapkdZjI2NMTk5OUDXpMXjuuuuY3Jykr179zI5Ocl1113H2rVrPQc08gYJqH5zEDP//DuaOtOFVeuAdTB9Fd/ExMQAXZMWj7Vr11JVTE5OMjExwWWXXQaA54BG3SBTfLuAc3vWnwXsnkMdaeQl4bOf/ayfPUk9BgmoLwAXJDk/ycnAVcCGGXU2AK/uruZ7EfBYVT1hek8aVb2fOa1du7ZvuTSq5hxQVbUPeBPwMeAh4MNV9WCSNyZ5Y1ftLuBhYDvwfuBfDdhf6YRTVYd8D8pwkqYN9HtQVXUX0yHUW3Zzz3IBvztIG5Kk0eSdJCRJTTKgJElNMqAkSU0yoCRJTTKgJElNMqAkSU1q8gcLk/wd8Miw+yEtsLOAR4fdCWkInl1Vz5hZ2GRASaMoyVS/XxWVRpVTfJKkJhlQkqQmGVBSO9YNuwNSS/wMSpLUJEdQkqQmGVCSpCYZUFqUkuydp+MsT7KtW744ycr5OO5Rtv3aJDfN07HemeQtR6jzgSSv6JYnkxy3S9qTnJHE33/TQAwo6XEXAwsWUItJkqXHuMsZ+AOlGpABpUUtyVOTfDLJ/Um2JlndlS9P8lCS9yd5MMk9SZ7cbXtBkgeS3Ef3g5pJTgb+AHhlks1JXpnkkiSfTfLF7vnCru7vJ7m1W16RZFuSpyTZkeSMnr5tTzKW5NeT/G13nE8kGevzOn42uunW9/YsvzXJF5JsSfKunvK1Sb6c5BPAhT3lFyf5XFd/fZKnH+E9fG+Sqe596j3+jiRvT/Jp4J8nuaJ7nx9I8smuzjuT3NqNyB5O8m+63a8H/mH3Xt5w+P+K0iwO/sS0Dx+L6QHs7Z5PAp7WLZ8FbAcCLAf2ARd32z4M/ItueQvwq93yDcC2bvm1wE09bTwNOKlb/jXgL7vlJcBG4DeBKeAlXfm7gdd1yy8EPtEtP53Hr5j9l8CNM9sDPgC8os/ru5zpy8/TtfsR4FLgBcBW4CldP7cDb+nz+v4A+E8z2wAmgfFu+czueWlX/rxufQfw77rlZwA7gfNn7PNO4LPAP+je/28BT+re/23D/nfiY3E/BvrJd6kBAf5DkkuBA8Ay4OAI5atVtblb3gQsT3I6cEZV/e+u/L8DV85y7NOB25JcABTT/+Olqg4keS3TQfC+qvpMV/8O4O3AfwOu6tYBngXckeRs4GTgq8fw+i7vHl/s1p8KXACcBqyvqh8AJNnQPc98fbcBf3GENn4ryRqmw/5s4KLutR18TQAvAjZW1Ve79+DbPfvfWVU/Bn6cZA+Pv//SQJzi02L3Kqb/un9BVV0MfBM4pdv24556+5n+H3CYDpuj8YfAvVX1XODXe44L0yGxFzinp+w+4BeSPAP4DeCvuvL/zPRIaQXwhhnHOWgf3fmYJEwHGV1//6iqLu4ev1BVt3TbBv4SY5LzgbcAL62q5wF3zujf93v6MVt7/d5naWAGlBa704E9VfXTJJcBzz5c5ar6DvBYkn/cFb2qZ/P3mB6Z9B77693yaw8WdqOUdzM91fZzBz87qqoC1gN/AjxUVd/qc5zXzNK1HUxP2wGsphutAR8DXp/kqV3by5I8k26KMcmTk5zGdIBSVY8Bf5/kV7r9fxs4OJrq52lMh9Bj3Wdjs40m7wN+tQs0kpx5mGPCE99L6Zj5l44Wuz8H/leSKWAz8KWj2Od1wK1JfsB0ABx0L3Btks3AHwH/kekpvt8H/qan3p8C/6Wq/m+Sa4B7k2ysqj1MT4l9gZ5AY/pzmr9I8nXgc8D5ffr0fuCvk3we+CTdyKWq7knyHOC+6YEVe5n+LO3+JHd0r/kR4FM9x3oNcHOSpwAPd6+3r6p6IMkXgQe7up+Zpd7fddOAf5VkCbAHeNlhjvutJJ/J9CX8H62qt85WV5qNtzqSJDXJKT5JUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpP+P1lI+GxfTTN7AAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAAUCElEQVR4nO3df5Bd5X3f8fdnJSwcxC8HewPY9TodxhWoMXG3OJ4wIJWaAvUAbd0ZNJngOFtUaFA7nXgmtJqmJK1apzb5VTwQqIjtqbKOZ4iLahgbxtWWqDWUlS1+yGuCivFYQE1xGIxAqEj69o+94NV6V1r2Lnsf7X2/ZnbuOc957nm+q9Gdz57nPPfeVBWSJLVmoNcFSJI0EwNKktQkA0qS1CQDSpLUJANKktSk5b0uYCannXZaDQ0N9boMaVG9/PLLnHDCCb0uQ1p0O3bseL6q3jm9vcmAGhoaYnx8vNdlSItqbGyMNWvW9LoMadEl+d5M7U7xSZKaZEBJkppkQEmSmmRASZKaZEBJkppkQEmSmmRASZKaZEBJPTY6Osrq1au56KKLWL16NaOjo70uSWpCk2/UlfrF6OgoIyMj7Nu3D4Bdu3YxMjICwLp163pZmtRzR72CSnJHkueSPDal7cYkTyfZ2fm5bJbnXpLk8SS7k9ywkIVLS8E111zDvn37OPXUUxkYGODUU09l3759XHPNNb0uTeq5uVxBfQ64GfjCtPbfq6rPzPakJMuAzwIfAfYADyXZWlXfnmet0pLz8ssvc+KJJ3LnnXdy8OBBli1bxhVXXMFLL73U69KknjtqQFXV/UmG5nHu84DdVfUkQJIvAlcABpQ0xdlnn82ll17K/v37WbFiBeeeey4PPvhgr8uSeq6be1DXJ7kaGAd+vapemHb8TOD7U/b3AB+a7WRJ1gPrAQYHBxkbG+uiNOnY8eCDD3L55Zezbt06RkdH2bp1K4CvAfW9VNXRO01eQX2lqlZ39geB54EC/g1welX96rTn/EPg71TVP+rs/zJwXlVtONp4w8PD5aeZqx8kAeDUU0/lxRdf5OSTT+aFFyb/1pvLa1NaCpLsqKrh6e3zWmZeVT+oqoNVdQi4ncnpvOn2AO+Zsv9u4Jn5jCctZcuXL+eFF17g0KFDvPDCCyxf7uJaCeYZUElOn7L794DHZuj2EHBWkvcleRtwFbB1PuNJS9Xy5ctZsWIFQ0NDDAwMMDQ0xIoVKwwpibktMx8FvgG8P8meJCPAf0jyaJJHgLXAP+/0PSPJPQBVdQC4HvgaMAF8qap2vUW/h3RMOumkk3j11VfZsGEDd999Nxs2bODVV1/lpJNO6nVpUs/N6R7UYvMelPrFsmXLOHTo0E+0DwwMcPDgwR5UJC2+Bb0HJWlhvB5OK1eu5JZbbmHlypWHtUv9zIluqQF79+7luuuu63UZUlO8gpIasGzZssMeJRlQUhNev9/kfSfpxwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSk44aUEnuSPJcksemtH06yXeSPJLky0lOmeW5TyV5NMnOJOMLWLckaYmbyxXU54BLprXdB6yuqp8D/gL4F0d4/tqqOreqhudXoiSpHx01oKrqfuAvp7XdW1UHOrsPAO9+C2qTJPWx5Qtwjl8F/nSWYwXcm6SAP6qq22Y7SZL1wHqAwcFBxsbGFqA06djla0D9LlV19E7JEPCVqlo9rX0jMAz8/ZrhREnOqKpnkryLyWnBDZ0rsiMaHh6u8XFvWWnpSzLrsbm8NqWlIMmOmW4DzXsVX5KPAx8FfmmmcAKoqmc6j88BXwbOm+94kqT+Mq+ASnIJ8BvA5VX1yix9Tkhy4uvbwMXAYzP1lSRpurksMx8FvgG8P8meJCPAzcCJwH2dJeS3dvqekeSezlMHge1JHgb+F3B3VX31LfktJElLzlEXSVTVuhmaN8/S9xngss72k8AHuqpOktS3/CQJSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTjhpQSe5I8lySx6a0vSPJfUme6DyeOstzL0nyeJLdSW5YyMIlSUvbXK6gPgdcMq3tBuDrVXUW8PXO/mGSLAM+C1wKnA2sS3J2V9VKkvrGUQOqqu4H/nJa8xXA5zvbnweunOGp5wG7q+rJqvp/wBc7z5OWtCRz/lmM80jHquXzfN5gVT0LUFXPJnnXDH3OBL4/ZX8P8KHZTphkPbAeYHBwkLGxsXmWJvXWtm3b5tx37dq1C3IeXy9aiuYbUHMx0591NVvnqroNuA1geHi41qxZ8xaVJR0bfA2o3813Fd8PkpwO0Hl8boY+e4D3TNl/N/DMPMeTlqSqmf9mm61d6ifzDaitwMc72x8H7pqhz0PAWUnel+RtwFWd50maoqqoKt77G195Y1vS3JaZjwLfAN6fZE+SEeBTwEeSPAF8pLNPkjOS3ANQVQeA64GvARPAl6pq11vza0iSlpqj3oOqqnWzHLpohr7PAJdN2b8HuGfe1UmS+pafJCFJapIBJUlqkgElSWqSASVJapIBJUlqkgElSWqSASVJapIBJUlqkgElSWqSASVJapIBJUlqkgElSWrSW/mFhdIx7QO/dS8v7nttUcccuuHuRR3v5Lcfx8P/+uJFHVOaKwNKmsWL+17jqU/93UUbb2xsbNG/RXexA1F6M5zikyQ1yYCSJDXJgJIkNcmAkiQ1yUUS0ixOXHUDf/3zNyzuoJ9f3OFOXAWweAtBpDfDgJJm8dLEp1zFJ/WQU3ySpCYZUJKkJhlQkqQmGVCSpCa5SEI6gkVfRPDVxf8sPqlV8w6oJO8H/nRK088Cv1lVvz+lzxrgLuC7naY/q6rfnu+Y0mJazBV8MBmGiz2m1LJ5B1RVPQ6cC5BkGfA08OUZuv55VX10vuNIkvrTQt2Dugj431X1vQU6nySpzy3UPairgNFZjn04ycPAM8Anq2rXTJ2SrAfWAwwODjI2NrZApUnHDv/fSz+WquruBMnbmAyfc6rqB9OOnQQcqqq9SS4D/qCqzjraOYeHh2t8fLyruqRjjfeg1K+S7Kiq4entCzHFdynwzenhBFBVP6qqvZ3te4Djkpy2AGNKkpa4hQiodcwyvZfkZ5Kks31eZ7wfLsCYkqQlrqt7UEl+CvgI8I+ntF0LUFW3Ah8DrktyANgHXFXdzilKkvpCVwFVVa8APz2t7dYp2zcDN3czhiSpP/lRR5KkJhlQkqQmGVCSpCYZUJKkJhlQkqQmGVCSpCb5fVBSj3Xeyz65/TuTj75dUPIKSuqpqeE0l3apnxhQkqQmOcUnLbCFuvp5M+dxSlBLkQElLbA3ExZHCiFDR/3OKT5JUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSkwwoSVKTDChJUpMMKElSk7oKqCRPJXk0yc4k4zMcT5I/TLI7ySNJPtjNeJKk/rEQ3we1tqqen+XYpcBZnZ8PAbd0HiVJOqK3eorvCuALNekB4JQkp7/FY0qSloBuA6qAe5PsSLJ+huNnAt+fsr+n0yZJ0hF1O8X3i1X1TJJ3Afcl+U5V3T/l+EzfZz3j91h3Am49wODgIGNjY12WJh3bfA2o36Vqxrx48ydKbgT2VtVnprT9ETBWVaOd/ceBNVX17JHONTw8XOPjP7HmQlpykpn+hpu0UK9NqXVJdlTV8PT2eU/xJTkhyYmvbwMXA49N67YVuLqzmu8XgBePFk6SJEF3U3yDwJc7fwEuB/6kqr6a5FqAqroVuAe4DNgNvAJ8ortyJUn9Yt4BVVVPAh+Yof3WKdsF/Np8x5Ak9S8/SUKS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUJANKktQkA0qS1CQDSpLUpHkHVJL3JNmWZCLJriT/bIY+a5K8mGRn5+c3uytXktQvlnfx3APAr1fVN5OcCOxIcl9VfXtavz+vqo92MY4kqQ/N+wqqqp6tqm92tl8CJoAzF6owSVJ/6+YK6g1JhoCfBx6c4fCHkzwMPAN8sqp2zXKO9cB6gMHBQcbGxhaiNOmY5WtA/S5V1d0JkpXAfwc2VdWfTTt2EnCoqvYmuQz4g6o662jnHB4ervHx8a7qko4FSWY91u1rUzpWJNlRVcPT27taxZfkOOBOYMv0cAKoqh9V1d7O9j3AcUlO62ZMSVJ/6GYVX4DNwERV/e4sfX6m048k53XG++F8x5Qk9Y9u7kH9IvDLwKNJdnba/iXwVwCq6lbgY8B1SQ4A+4CrynkLSdIczDugqmo7MPsE+mSfm4Gb5zuGJKl/+UkSkqQmGVCSpCYZUJKkJhlQkqQmGVCSpCYZUJKkJhlQkqQmGVBSA8455xwGBgY455xzel2K1IwF+TRzSd3ZtWvXYY+SvIKSemq2T/7yE8EkA0rquaqiqti2bdsb25IMKElSo7wHJfXYTF9a6FWU5BWU1FOvh9PAwACf/vSnGRgYOKxd6mcGlNRjAwMDHDx4kOHhYQ4ePPhGSEn9zleC1GP33nvvEfelfmVAST128cUXH3Ff6lcukpB67NChQ95zkmbgFZTUQ9dff/2bapf6iQEl9dDtt9/OTTfddNgbdW+66SZuv/32Xpcm9ZwBJfXQ/v37ufbaaw9ru/baa9m/f3+PKpLa4T0oqYdWrFjB+vXr2blzJxMTE6xatYpzzz2XFStW9Lo0qee8gpJ66MILL2TLli1ccMEF3HXXXVxwwQVs2bKFCy+8sNelST3nFZTUQ08//TRXXnkld9xxB7fccgsrVqzgyiuv5Iknnuh1aVLPGVBSD01MTPCtb32L4447jrGxMdasWcNrr73G8ccf3+vSpJ7raoovySVJHk+yO8kNMxxPkj/sHH8kyQe7GU9aalatWsX27dsPa9u+fTurVq3qUUVSO+YdUEmWAZ8FLgXOBtYlOXtat0uBszo/64Fb5juetBRt3LiRkZERtm3bxoEDB9i2bRsjIyNs3Lix16VJPdfNFN95wO6qehIgyReBK4BvT+lzBfCFmvzugAeSnJLk9Kp6totxpSVj3bp1AGzYsOGNVXybNm16o13qZ90E1JnA96fs7wE+NIc+ZwI/EVBJ1jN5lcXg4CBjY2NdlCYdO04//XRuvvlm9u7dy8qVKwH8/y/RXUDN9OFh079lbS59JhurbgNuAxgeHq41a9Z0UZp07Hl9kYSkSd0sktgDvGfK/ruBZ+bRR5Kkn9BNQD0EnJXkfUneBlwFbJ3WZytwdWc13y8AL3r/SZI0F/Oe4quqA0muB74GLAPuqKpdSa7tHL8VuAe4DNgNvAJ8ovuSJUn9oKs36lbVPUyG0NS2W6dsF/Br3YwhSepPfhafJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpQkqUkGlCSpSQaUJKlJBpTUY6Ojo6xevZqLLrqI1atXMzo62uuSpCZ09Vl8krozOjrKxo0b2bx5MwcPHmTZsmWMjIwA+K266nteQUk9tGnTJjZv3szatWtZvnw5a9euZfPmzWzatKnXpUk9Z0BJPTQxMcH5559/WNv555/PxMREjyqS2mFAST20atUqtm/ffljb9u3bWbVqVY8qktphQEk9tHHjRkZGRti2bRsHDhxg27ZtjIyMsHHjxl6XJvWciySkHnp9IcSGDRuYmJhg1apVbNq0yQUSEpDJL71ty/DwcI2Pj/e6DGlRjY2NsWbNml6XIS26JDuqanh6u1N8kqQmGVCSpCYZUJKkJhlQkqQmGVCSpCYZUJKkJhlQkqQmNfk+qCT/F/her+uQFtlpwPO9LkLqgfdW1TunNzYZUFI/SjI+05sVpX7lFJ8kqUkGlCSpSQaU1I7bel2A1BLvQUmSmuQVlCSpSQaUJKlJBpR0BElOSfJPel1Ht5L8SpIzel2H9GYYUNKRnQIc8wEF/ApgQOmYYkBJR/Yp4K8m2Znk95J8Pck3kzya5AqAJH8zySNJjk9yQpJdSVYnWTlL/6Ek30nyn5I8lmRLkr+d5H8keSLJeZ1+70jyXzrnfiDJz3Xab0zyydcL7JxjqPMzkeT2Tg33Jnl7ko8Bw8CWzu/x9kX/V5TmwVV80hEkGQK+UlWrkywHfqqqfpTkNOAB4KyqqiT/FjgeeDuwp6r+/Wz9gfcCu4GfB3YBDwEPAyPA5cAnqurKJP8ReL6qfivJ3wJ+t6rOTXIjsLeqPtOp8THgo52SdwPDVbUzyZeArVX1n5OMAZ+sqvG39l9MWjjLe12AdAwJ8O+SXAAcAs4EBoH/A/w2k0HzKvBPj9If4LtV9ShAkl3A1ztB9ygw1OlzPvAPAKrqvyX56SQnH6XG71bVzs72jinnko45BpQ0d78EvBP4G1X1WpKnmLxqAngHsBI4rtP28lH6759y3kNT9g/x49dlZqihgAMcPj1//JTtqec9yOQVnXRM8h6UdGQvASd2tk8GnuuEzVomp+pedxvwr4AtwO/Mof9c3M9kyJFkDZPTfT8CngI+2Gn/IPC+N/l7SMcEr6CkI6iqH3YWLzzG5BTeX0syDuwEvgOQ5GrgQFX9SZJlwP/s3DPaAvzX6f3fhBuBP07yCPAK8PFO+53A1Ul2dmr6izmc63PArUn2AR+uqn1vshZp0blIQpLUJKf4JElNMqAkSU0yoCRJTTKgJElNMqAkSU0yoCRJTTKgJElN+v/LWm2WTZwhHgAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n",
      "\n",
      "\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "C:\\Users\\sboeh\\anaconda3\\lib\\site-packages\\pandas\\core\\arraylike.py:358: RuntimeWarning: invalid value encountered in log\n",
      "  result = getattr(ufunc, method)(*inputs, **kwargs)\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAagAAAEYCAYAAAAJeGK1AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAARDElEQVR4nO3dfZBddX3H8ffHBLMYUKrEHR7UWGG0KQ+hbpVqtRvKODGtYKutWkdBp2TSGeK0jJ0ITAV8aM3A0E4tMymhDkwLUtSmowGV1LrQqQVNFMJDI0MZGJk4Uqsoi2M04ds/9qxel7vZDYTcX3bfr5k7Ofd3zzn3d5Yl75xzTzapKiRJas2zBj0BSZL6MVCSpCYZKElSkwyUJKlJBkqS1KSFg55AP0ceeWQtXbp00NOQDqjHH3+cxYsXD3oa0gG3bdu271bVkqnjTQZq6dKlbN26ddDTkA6osbExRkdHBz0N6YBL8lC/cS/xSZKaZKAkSU0yUJKkJhkoSVKTDJQkqUkGSpLUJAMlSWqSgZIGbO3atQwNDbFixQqGhoZYu3btoKckNaHJv6grzRdr165lw4YNrF+/nmXLlnHvvfeybt06AD7+8Y8PeHbSYHkGJQ3Qxo0bWb9+Peeddx5DQ0Ocd955rF+/no0bNw56atLAGShpgHbt2sWaNWt+YWzNmjXs2rVrQDOS2mGgpAFatGgRGzZs+IWxDRs2sGjRogHNSGqHn0FJA3TOOef87DOnZcuWcfnll7Nu3bonnVVJ85GBkgZo8kaICy64gF27drFo0SLWrFnjDRISkKoa9ByeZGRkpPznNjTf+M9taL5Ksq2qRqaO+xmUJKlJBkqS1CQDJUlqkoGSJDXJQEmSmmSgJElNMlCSpCYZKElSkwyUJKlJBkqS1CQDJUlqkoGSJDXJQEmSmmSgJElNMlCSpCYZKElSkwyUJKlJBkqS1CQDJUlqkoGSJDXJQEmSmmSgJElNMlCSpCYZKElSkwyUJKlJC2daIckQcCuwqFv/01V1UZIPA2cCTwCPAGdX1c4+2z8IPAbsAXZX1cj+m74kaa6azRnULuC0qjoZWA6sTHIqcGlVnVRVy4HNwAf3so8VVbXcOEmSZmvGM6iqKmC8e3pI96iq+mHPaouB2v/TkyTNV7P6DCrJgiR3MHEpb0tV3d6NfzTJt4B3Mv0ZVAE3J9mWZPV+mLMkaR7IxAnSLFdOjgA2AWur6u6e8fOBoaq6qM82R1fVziQvBLZ0297aZ73VwGqA4eHhV15//fX7eizSQW18fJzDDjts0NOQDrgVK1Zs6/cR0D4FCiDJRcDjVXVZz9hLgBur6oQZtr0YGO/dtp+RkZHaunXrPs1LOtiNjY0xOjo66GlIB1ySvoGa8RJfkiXdmRNJDgVOB3YkOb5ntTOAHX22XZzk8Mll4A3A3VPXkyRpqhlvkgCOAq5JsoCJoN1QVZuTfCbJy5m4zfwhYA1MXNIDrqqqVcAwsCnJ5HtdV1VfeAaOQ5I0x8zmLr7twCl9xt8yzfo7gVXd8gPAyU9zjpKkecifJCFJapKBkiQ1yUBJkppkoCRJTTJQkqQmGShJUpMMlCSpSQZKktQkAyVJapKBkiQ1yUBJkppkoCRJTTJQkqQmGShJUpMMlCSpSQZKktQkAyVJapKBkiQ1yUBJkppkoCRJTTJQkqQmGShJUpMMlCSpSQZKktQkAyVJapKBkiQ1yUBJkppkoCRJTTJQkqQmGShJUpMMlCSpSQZKktQkAyVJapKBkiQ1yUBJkppkoCRJTTJQkqQmGShJUpMMlCSpSQZKktQkAyVJapKBkiQ1yUBJkppkoCRJTTJQkqQmGShJUpNmDFSSoSRfTXJnknuSXNKNfzjJ9iR3JLk5ydHTbL8yyTeT3J/kA/v7ACRJc9NszqB2AadV1cnAcmBlklOBS6vqpKpaDmwGPjh1wyQLgCuANwLLgHckWbaf5i5JmsNmDFRNGO+eHtI9qqp+2LPaYqD6bP4q4P6qeqCqfgJcD5z5NOcsSZoHFs5mpe5MaBtwHHBFVd3ejX8UeDfwA2BFn02PAb7V8/xh4NXTvMdqYDXA8PAwY2NjszsCaY4YHx/3+17qMatAVdUeYHmSI4BNSU6oqrur6kLgwiTnA+cCF03ZNP12N817XAlcCTAyMlKjo6OzOwJpjhgbG8Pve+nn9ukuvqp6FBgDVk556TrgLX02eRh4Uc/zY4Gd+/KekqT5aTZ38S3pzpxIcihwOrAjyfE9q50B7Oiz+deA45O8NMmzgbcDn33as5YkzXmzucR3FHBN9znUs4Abqmpzks8keTnwBPAQsAagu938qqpaVVW7k5wLfBFYAHyiqu55Ro5EkjSnzBioqtoOnNJnvN8lPapqJ7Cq5/lNwE1PY46SpHnInyQhSWqSgZIkNclASZKaZKAkSU0yUJKkJhkoSVKTDJQkqUkGSpLUJAMlSWqSgZIkNclASZKaZKAkSU0yUJKkJhkoSVKTDJQkqUkGSpLUJAMlSWqSgZIkNclASZKaZKAkSU0yUJKkJhkoSVKTDJQkqUkGSpLUJAMlSWqSgZIkNclASZKaZKAkSU0yUJKkJhkoSVKTDJQkqUkGSpLUJAMlSWqSgZIkNclASZKaZKAkSU0yUJKkJhkoSVKTDJQkqUkGSpLUJAMlSWqSgZIkNclASZKaZKAkSU0yUJKkJi2caYUkQ8CtwKJu/U9X1UVJLgXeBPwE+B/gPVX1aJ/tHwQeA/YAu6tqZL/NXpI0Z83mDGoXcFpVnQwsB1YmORXYApxQVScB9wHn72UfK6pquXGSJM3WjIGqCePd00O6R1XVzVW1uxu/DTj2GZqjJGkemtVnUEkWJLkDeATYUlW3T1nlvcDnp9m8gJuTbEuy+inPVJI0r8z4GRRAVe0Blic5AtiU5ISquhsgyYXAbuDaaTZ/bVXtTPJCYEuSHVV169SVunitBhgeHmZsbGyfD0Y6mI2Pj/t9L/VIVe3bBslFwONVdVmSs4A1wG9X1Y9mse3FwHhVXba39UZGRmrr1q37NC/pYDc2Nsbo6OigpyEdcEm29btHYcZLfEmWdGdOJDkUOB3YkWQlsA44Y7o4JVmc5PDJZeANwN1P+SgkSfPGbC7xHQVck2QBE0G7oao2J7mfiVvPtyQBuK2q1iQ5GriqqlYBw0xcEpx8r+uq6gvPxIFIkuaWGQNVVduBU/qMHzfN+juBVd3yA8DJT3OOkqR5yJ8kIUlqkoGSJDXJQEmSmmSgJElNMlCSpCYZKElSkwyUJKlJBkqS1CQDJUlqkoGSJDXJQEmSmmSgJElNMlCSpCYZKElSkwyUJKlJBkqS1CQDJUlqkoGSJDXJQEmSmmSgJElNMlCSpCYZKElSkwyUJKlJBkqS1CQDJUlqkoGSJDXJQEmSmmSgJElNMlCSpCYZKElSkwyUJKlJBkqS1CQDJUlqkoGSJDXJQEmSmmSgJElNMlCSpCYZKElSkwyUJKlJBkqS1CQDJUlqkoGSJDXJQEmSmmSgJElNMlCSpCYZKElSk2YMVJKhJF9NcmeSe5Jc0o1fmmRHku1JNiU5YprtVyb5ZpL7k3xgP89fkjRHzeYMahdwWlWdDCwHViY5FdgCnFBVJwH3AedP3TDJAuAK4I3AMuAdSZbtp7lLkuawGQNVE8a7p4d0j6qqm6tqdzd+G3Bsn81fBdxfVQ9U1U+A64Ez98O8JUlz3MLZrNSdCW0DjgOuqKrbp6zyXuCf+2x6DPCtnucPA6+e5j1WA6sBhoeHGRsbm83UpDljfHzc73upx6wCVVV7gOXd50ybkpxQVXcDJLkQ2A1c22fT9NvdNO9xJXAlwMjISI2Ojs5matKcMTY2ht/30s/t0118VfUoMAasBEhyFvC7wDurql94HgZe1PP8WGDnU5moJGl+mc1dfEsm79BLcihwOrAjyUpgHXBGVf1oms2/Bhyf5KVJng28Hfjsfpm5JGlOm80lvqOAa7rPoZ4F3FBVm5PcDywCtiQBuK2q1iQ5GriqqlZV1e4k5wJfBBYAn6iqe56ZQ5EkzSUzBqqqtgOn9Bk/bpr1dwKrep7fBNz0NOYoSZqH/EkSkqQmGShJUpMMlCSpSQZKktQkAyVJapKBkiQ1yUBJkppkoCRJTTJQkqQmGShJUpMMlCSpSQZKktQkAyVJapKBkiQ1yUBJkppkoCRJTTJQkqQmGShJUpMMlCSpSQsHPQFpvkvypLGqGsBMpLZ4BiUNUL847W1cmk8MlNSAquLLX/6yZ05SDwMlSWpSWvwT28jISG3dunXQ09A8d+I1Jw56CgfEXWfdNegpaJ5Lsq2qRqaOe5OENI0D8Rv33j5ravEPj9KB5CU+aYCmi5BxkgyUNHBV9Qs3SRgnaYKBkiQ1yUBJkppkoCRJTTJQkqQmGShJUpMMlCSpSQZKktQkAyVJalKTP4svyf8CDw16HtIBdiTw3UFPQhqAl1TVkqmDTQZKmo+SbO33AzOl+cpLfJKkJhkoSVKTDJTUjisHPQGpJX4GJUlqkmdQkqQmGShJUpMMlLQfJFma5I/24/7enGTZftzf1Une2i0/mOTIPutcnOT9T2Hfo0k27495Sr0MlLR/LAX6BirJwqewvzcD+y1Q0sHIQOmgl+TdSbYnuTPJPyZZkuQzSb7WPV7brXdxkk8kGUvyQJL3deOLk9zYbX93krd14z8700gykmSsW/6tJHd0j28kORz4GPC6buzPkpyd5FNJPgfcnOSwJF9K8vUkdyU5cy/zfw1wBnBpt7+XJTmnO5Y7u2N7Trft1Un+NslXumOaPEtKkr9Lcm+SG4EXTvmy/XmSr3aP4/p8TZcnua2b16Ykv9SNH5fk37p5fD3Jy6Zs9+vd1+SXn+5/V4mq8uHjoH0Avwp8Eziye/584DrgN7vnLwb+u1u+GPgKsIiJHyv0f8AhwFuAjT37fF7364M9+x0BxrrlzwGv7ZYPAxYCo8Dmnn2cDTwMPL97vhB4brd8JHA/kH7z7369Gnhrz/5e0LP8EWBtz3qfYuIPm8uA+7vx3we2AAuAo4FHJ/fXHdeF3fK7J+fdfX3e3y1vB36rW/4Q8Dfd8u3A73XLQ8BzJo8deA2wDXjxoL8vfMyNx1O59CC15DTg01X1XYCq+l6S04FlSSbXeW53lgNwY1XtAnYleQQYBu4CLkuynonfrP9jhvf8T+DyJNcC/1JVD/e8V68tVfW9bjnAXyZ5PfAEcEz33k+a/zTveUKSjwBHMBHFL/a89q9V9QRwb5Lhbuz1wCerag+wM8m/T9nfJ3t+/eveF5I8Dziiqm7phq4BPtV9DY+pqk3dXH/crQ/wK0z8Pa43VNXOaY5B2ide4tPBLsDUv8z3LOA3qmp59zimqh7rXtvVs94eYGFV3Qe8kolQ/VWSD3av7+bn/48MTW5UVR8D/hg4FLgtySummdvjPcvvBJYAr6yq5cB3un32m38/VwPnVtWJwCW985lyTL2l3Nt+a5rlvelb4c63gR8Dp8xyX9KMDJQOdl8C/jDJCwCSPB+4GTh3coUky/e2gyRHAz+qqn8CLgN+rXvpQSbCBROXASfXf1lV3VVV64GtwCuAx4DDmd7zgEeq6qdJVgAv2cv86bO/w4FvJzmEidjN5Fbg7UkWJDkKWDHl9bf1/PpfvS9U1Q+A7yd5XTf0LuCWqvoh8HCSN3dzXTT5WRgTlxB/h4mzxNFZzE+akZf4dFCrqnuSfBS4Jcke4BvA+4Arkmxn4nv8VmDNXnZzIhM3JDwB/BT4k278EuAfklzAxGcvk/60i8we4F7g80xcttud5E4mzna+P+U9rgU+l2QrcAewYy/zPxu4HtjY3cjxVuAvujk8xMSZ3t5iCLCJicuHdwH3AbdMeX1RktuZ+EPqO/psfxawoQvQA8B7uvF3AX+f5ENMfK3+YHKDqvpOkjcBn0/y3qq6fepOpX3hjzqSJDXJS3ySpCYZKElSkwyUJKlJBkqS1CQDJUlqkoGSJDXJQEmSmvT/LFBNHyTa4gEAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    }
   ],
   "source": [
    "# Outliers continuous variables\n",
    "\n",
    "def analyze_outliers(df,var):\n",
    "    \n",
    "    new_df = df.copy()\n",
    "    \n",
    "    if 0 in new_df[var].unique():\n",
    "        pass\n",
    "    \n",
    "    else:\n",
    "        new_df[var] = np.log(df[var])\n",
    "        new_df.boxplot(column=var)\n",
    "        plt.tight_layout()\n",
    "        plt.show()\n",
    "        \n",
    "for var in cont_vars:\n",
    "    analyze_outliers(df_eda,var)\n",
    "    print('\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "54a9efda-c0f6-4632-9e8d-7fa79d0c7b53",
   "metadata": {},
   "source": [
    "### Duplicates\n",
    "Lets first check for duplicates and drop duplicates with the same parcel id."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "e6008cd5-750a-4b10-836e-32cf80eeb17d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Total rows: 5970434\n",
      "Duplicated rows: 2985217\n",
      "Duplicated rows in %: 50.0\n"
     ]
    }
   ],
   "source": [
    "# Create duplicate df and caluclate precentage\n",
    "duplicate = df.index.duplicated()\n",
    "print('Total rows:', len(df))\n",
    "print('Duplicated rows:', duplicate.sum())\n",
    "print('Duplicated rows in %:', (duplicate.sum()/df.shape[0])*100)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "abba48d2-e400-4937-a5ac-7406c406280c",
   "metadata": {},
   "source": [
    "As the shown above it seems like the total dataset has 50% of duplicated row, which we will drop in the next step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "f22ab4b0-c400-489d-86ff-94552d1a1431",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Drop duplicates\n",
    "df = df[~df.index.duplicated(keep='first')]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e9cf69ee-a003-41a6-9cdd-0b9ff735fd22",
   "metadata": {},
   "source": [
    "### NA values\n",
    "Since we have a lot of NA values 85,129,239 to be exact. We decided to use to approaches to handle them. In the first data frame we will train the models with we will keep all NA values and try to replace them as accurate as possiple. For the second dataframe we will drop all columns which have more than 50% of NA values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "494aa29a-7f75-49c2-a96c-a42b15c6571c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define columsn to keep with more than 50% of NA values\n",
    "columns = pd.DataFrame(df.isnull().mean())\n",
    "columns = columns[columns[0] <= 0.5].index\n",
    "columns = [i for i in columns]\n",
    "\n",
    "# Create second dataframe\n",
    "df2 = df.copy()\n",
    "df2 = df2[columns]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f00c9395-41a2-4230-ae2b-2c006c45d08d",
   "metadata": {},
   "source": [
    "### Pools\n",
    "\n",
    "The dataset contains different data about pools that come with the property.\n",
    "\n",
    "- **pooltypeid10**, which defines if the pool is a spa or hot tub with around 99% of NA values\n",
    "- **poolsizesum**, which describes the square footage of all pools on the property with also 99% of NA values\n",
    "- **pooltypeid2**, which says if the its a pool with a Spa/Hot Tub and has around 98% of NA values\n",
    "- **hashottuborspa**, is a categorical varibale does the home have a hot tub or spa with around 98% NA values\n",
    "- **pooltypeid7**, which determines if its a pool without a hot tub with around 83% of NA values\n",
    "- **poolcnt**, describes the numberof pools on the property with 82% of na values\n",
    "\n",
    "In case of *pooltypeid2*, *pooltypeid7* and *pooltypeid10* we can simply replace the NA values with 0 since it will only contain non-null information when *poolcnt* or *hashottuborspa* contains non-null information. The feature *poolsizesum* reflects the total square footage of pools on the property. Here again we can replace the NA values with 0. Next is *hashottuborspa* which contains if the property has a hot tub or spa. The NA value means that it has no spa or hot tub so we could also replace it with 0. For *poolcnt* we can also replace the NA values with 0, because NA pools of course means no pools. Also *pooltypeid10* and *hashottuborspa* tells us the same information which means we can drop one of them. Since the columns are not in the second dataframe we dont need to adjust it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "0bbb768f-834a-48eb-b210-c4f98141aa6b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Fill NA values for pooltype features with 0\n",
    "df.pooltypeid2.fillna(0, inplace=True)\n",
    "df.pooltypeid7.fillna(0, inplace=True)\n",
    "df.pooltypeid10.fillna(0, inplace=True)\n",
    "\n",
    "# Fill NA values for poolsizesum with 0\n",
    "df.poolsizesum.fillna(0, inplace=True)\n",
    "\n",
    "# Fill NA values for poolcnt with 0\n",
    "df.poolcnt.fillna(0, inplace=True)\n",
    "\n",
    "# Dropping redundant feature\n",
    "df.drop('hashottuborspa', axis=1, inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "0288a486-ae6c-497b-a551-ab4320ba4a59",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "pooltypeid2     0\n",
       "pooltypeid7     0\n",
       "pooltypeid10    0\n",
       "poolcnt         0\n",
       "poolsizesum     0\n",
       "dtype: int64"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Checking for NA values\n",
    "df[['pooltypeid2', 'pooltypeid7', 'pooltypeid10', 'poolcnt', 'poolsizesum']].isnull().sum()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68cdbba5-489f-4209-b31c-3d51177de5dc",
   "metadata": {},
   "source": [
    "### Fireplaces\n",
    "\n",
    "The dataset contains different data about the fireplaces that come with the property.\n",
    "\n",
    "- **fireplaceflag** describes if the property has a fireplace and cointains around 99% of NA values\n",
    "- **fireplacecnt** counts the fireplaces on the property and has around 89% of NA values\n",
    "\n",
    "The feautre *fireplaceflag* contains the value True if the property has a fireplace, which we will change to 1 and NA to 0 if it has not. For *fireplacecnt* we switch the NA value to 0, because NA means just no fireplace. The columns are also not in the second dataframe we dont need to adjust it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "042fbe78-25ee-4021-b108-ea2542ab0276",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Replace True value with 1 and replace NA values with 0 for fireplaceflag\n",
    "df.fireplaceflag.replace(to_replace=True, value=1, inplace=True)\n",
    "df.fireplaceflag.fillna(0, inplace=True)\n",
    "\n",
    "# Replace NA values for fireplacecnt with 0\n",
    "df.fireplacecnt.fillna(0, inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "id": "80ec0068-3091-4837-818c-7f370c504e87",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "NA-values fireplaceflag: 0\n",
      "NA-values fireplacecnt: 0\n"
     ]
    }
   ],
   "source": [
    "print('NA-values fireplaceflag:', df['fireplaceflag'].isnull().sum())\n",
    "print('NA-values fireplacecnt:', df['fireplacecnt'].isnull().sum())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bcdc8cd0-7ba5-4d44-a9a5-3abd106c58b9",
   "metadata": {},
   "source": [
    "### Garages\n",
    "\n",
    "The dataset contains different data about the garages that come with the property.\n",
    "\n",
    "- **garagetotalsqft** measures the square foot of the garage with around 70% NA values\n",
    "- **garagecarcnt** counts the gargaes on the property and has also around 70% of NA values\n",
    "\n",
    "Here again we will switch the NA values for 0 since NA means no garage. The columns above are also not in df2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "id": "bac4dd36-81d2-41b8-aba7-2a5277beb2b7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Replace NA values with 0 for the feautres garagetotalsqft and garagecarcnt\n",
    "df.garagetotalsqft.fillna(0, inplace=True)\n",
    "df.garagecarcnt.fillna(0, inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "id": "c1cdc672-b861-48b1-8954-f1acfad013a7",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "NA-values garagetotalsqft: 0\n",
      "NA-values garagecarcnt: 0\n"
     ]
    }
   ],
   "source": [
    "# Checking for NA values\n",
    "print('NA-values garagetotalsqft:', df['garagetotalsqft'].isnull().sum())\n",
    "print('NA-values garagecarcnt:', df['garagecarcnt'].isnull().sum())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ba1ad070-33e3-4c62-a10f-713d824339d9",
   "metadata": {},
   "source": [
    "### Tax Delinquency\n",
    "\n",
    "The dataset contains different data about the tax delinquency that come with the property.\n",
    "\n",
    "- **taxdelinquencyflag** means that property taxes for the property are past due as of 2015 with around 98% NA values.\n",
    "- **taxdelinquencyyear** describes the year in which the unpaid taxes were due with also around 98% NA values.\n",
    "\n",
    "The feature *taxdelinquencyflag* cotains if the taxes are due past as 2015 Y which we replace with 1 and NA for not, which we replace with 0. For the feature *taxdelinquencyyear* will be dropped since we dont consider that for our model. The two features are also not in df2 so no replacement needed in that case."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "id": "16017c18-86d2-4f93-aca7-7a74b88da51f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Replace Y with 1 and fill NA with 0 for the feature taxdelinquencyflag\n",
    "df.taxdelinquencyflag.replace(to_replace='T', value=1, inplace=True)\n",
    "df.taxdelinquencyflag.fillna(0, inplace=True)\n",
    "\n",
    "# Drop taxdelinquencyyear\n",
    "df.drop('taxdelinquencyyear', axis=1, inplace=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3d40f120-f63f-4c72-8fd7-bfe5e22ab9f6",
   "metadata": {},
   "source": [
    "### Squarefeet\n",
    "\n",
    "The dataset contains different data about the squarefeet that come with the property.\n",
    "\n",
    "- **finishedsquarefeet6** is the base unfinished and finished area with 99% NA values\n",
    "- **finishedsquarefeet12** describes the finished living area and has around 9% NA values\n",
    "- **finishedsquarefeet13** is the perimeter of the living area with 99% NA values\n",
    "- **finishedsquarefeet15** means the total finished area in square feet with 93% of NA values\n",
    "- **finishedsquarefeet50** is the size of the first floor in square feet with 93% NA values\n",
    "- **finishedfloor1squarefeet** is also the size of the first floor in square feet with 93% NA values as well\n",
    "- **calculatedfinishedsquarefeet** describes the calculated total square feet of the living area with 2% of NA values\n",
    "- **lotsizesquarefeet** is the area of the lot in square feet with around 9% of NA values\n",
    "Area of lot in square feet. Fill \"NaN\" with average.\n",
    "\n",
    "For the feature *lotsizesquarefeet* we will use the avergae to fill the NA values. Before deciding on what to do with the other features we will look at them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "id": "28c3cb51-d0ba-424c-99b0-e99e6339e016",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "finishedsquarefeet6 False\n",
      "finishedsquarefeet12 True\n",
      "finishedsquarefeet13 False\n",
      "finishedsquarefeet50 False\n",
      "finishedfloor1squarefeet False\n",
      "calculatedfinishedsquarefeet True\n",
      "lotsizesquarefeet True\n"
     ]
    }
   ],
   "source": [
    "squarefeet = ['finishedsquarefeet6', 'finishedsquarefeet12', 'finishedsquarefeet13',\n",
    "              'finishedsquarefeet50', 'finishedfloor1squarefeet', 'calculatedfinishedsquarefeet', 'lotsizesquarefeet']\n",
    "\n",
    "for i in squarefeet:\n",
    "    print(i, i in columns)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "id": "9fb9e701-3b11-41e5-a139-87d3f8bc3eb7",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>finishedsquarefeet6</th>\n",
       "      <th>finishedsquarefeet12</th>\n",
       "      <th>finishedsquarefeet13</th>\n",
       "      <th>finishedsquarefeet15</th>\n",
       "      <th>finishedsquarefeet50</th>\n",
       "      <th>finishedfloor1squarefeet</th>\n",
       "      <th>calculatedfinishedsquarefeet</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>parcelid</th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>17108365</th>\n",
       "      <td>NaN</td>\n",
       "      <td>1634.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>812.0</td>\n",
       "      <td>812.0</td>\n",
       "      <td>1634.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17108511</th>\n",
       "      <td>NaN</td>\n",
       "      <td>2510.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1736.0</td>\n",
       "      <td>1736.0</td>\n",
       "      <td>2510.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17108803</th>\n",
       "      <td>NaN</td>\n",
       "      <td>1319.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>619.0</td>\n",
       "      <td>619.0</td>\n",
       "      <td>1319.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17108926</th>\n",
       "      <td>NaN</td>\n",
       "      <td>1469.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>608.0</td>\n",
       "      <td>608.0</td>\n",
       "      <td>1469.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17109097</th>\n",
       "      <td>NaN</td>\n",
       "      <td>1110.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>679.0</td>\n",
       "      <td>679.0</td>\n",
       "      <td>1110.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17109335</th>\n",
       "      <td>NaN</td>\n",
       "      <td>1484.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>762.0</td>\n",
       "      <td>762.0</td>\n",
       "      <td>1484.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17109581</th>\n",
       "      <td>NaN</td>\n",
       "      <td>1660.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1660.0</td>\n",
       "      <td>1660.0</td>\n",
       "      <td>1660.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17109604</th>\n",
       "      <td>NaN</td>\n",
       "      <td>2415.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1305.0</td>\n",
       "      <td>1305.0</td>\n",
       "      <td>2415.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17109704</th>\n",
       "      <td>NaN</td>\n",
       "      <td>1430.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1430.0</td>\n",
       "      <td>1430.0</td>\n",
       "      <td>1430.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17109927</th>\n",
       "      <td>NaN</td>\n",
       "      <td>1618.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1618.0</td>\n",
       "      <td>1618.0</td>\n",
       "      <td>1618.0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "          finishedsquarefeet6  finishedsquarefeet12  finishedsquarefeet13  \\\n",
       "parcelid                                                                    \n",
       "17108365                  NaN                1634.0                   NaN   \n",
       "17108511                  NaN                2510.0                   NaN   \n",
       "17108803                  NaN                1319.0                   NaN   \n",
       "17108926                  NaN                1469.0                   NaN   \n",
       "17109097                  NaN                1110.0                   NaN   \n",
       "17109335                  NaN                1484.0                   NaN   \n",
       "17109581                  NaN                1660.0                   NaN   \n",
       "17109604                  NaN                2415.0                   NaN   \n",
       "17109704                  NaN                1430.0                   NaN   \n",
       "17109927                  NaN                1618.0                   NaN   \n",
       "\n",
       "          finishedsquarefeet15  finishedsquarefeet50  \\\n",
       "parcelid                                               \n",
       "17108365                   NaN                 812.0   \n",
       "17108511                   NaN                1736.0   \n",
       "17108803                   NaN                 619.0   \n",
       "17108926                   NaN                 608.0   \n",
       "17109097                   NaN                 679.0   \n",
       "17109335                   NaN                 762.0   \n",
       "17109581                   NaN                1660.0   \n",
       "17109604                   NaN                1305.0   \n",
       "17109704                   NaN                1430.0   \n",
       "17109927                   NaN                1618.0   \n",
       "\n",
       "          finishedfloor1squarefeet  calculatedfinishedsquarefeet  \n",
       "parcelid                                                          \n",
       "17108365                     812.0                        1634.0  \n",
       "17108511                    1736.0                        2510.0  \n",
       "17108803                     619.0                        1319.0  \n",
       "17108926                     608.0                        1469.0  \n",
       "17109097                     679.0                        1110.0  \n",
       "17109335                     762.0                        1484.0  \n",
       "17109581                    1660.0                        1660.0  \n",
       "17109604                    1305.0                        2415.0  \n",
       "17109704                    1430.0                        1430.0  \n",
       "17109927                    1618.0                        1618.0  "
      ]
     },
     "execution_count": 31,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Fill NA values for lotsizesquarefeet with the average.\n",
    "df.lotsizesquarefeet.fillna(df['lotsizesquarefeet'].mean(), inplace=True)\n",
    "df2.lotsizesquarefeet.fillna(df2['lotsizesquarefeet'].mean(), inplace=True)\n",
    "\n",
    "df[['finishedsquarefeet6', 'finishedsquarefeet12', 'finishedsquarefeet13', 'finishedsquarefeet15', 'finishedsquarefeet50', 'finishedfloor1squarefeet', 'calculatedfinishedsquarefeet']][500:510]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f3e5782e-74bf-4023-a0b3-9bd7a11cb0e8",
   "metadata": {},
   "source": [
    "We can see that the features *finishedsquarefeet50* and *finishedfloor1squarefeet* carry the same value. Which means at least one of those features is redundant. Since all features besides *finishedsquarefeet12* have at least 90% of NA values.*finishedsquarefeet12* and *calculatedfinishedsquarefeet* contain the same value, which means we can drop one. Since *calculatedfinishedsquarefeet* has less NA values will keep it and decide on filling the missing values with the mean or the median."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "id": "a46a313a-3412-4ff8-bc45-450195369bae",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Drop the spare features\n",
    "df.drop(columns=['finishedsquarefeet6', 'finishedsquarefeet12', 'finishedsquarefeet13', 'finishedsquarefeet15', 'finishedsquarefeet50', 'finishedfloor1squarefeet'], inplace=True)\n",
    "df2.drop(columns='finishedsquarefeet12', inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "id": "badca4ec-f332-47e2-b490-7a666f3ac40f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Since value distribution for the finishedsquarefeet12 feature is not symmetric we will fill the median value\n",
    "df['calculatedfinishedsquarefeet'].fillna((df['calculatedfinishedsquarefeet'].median()), inplace=True)\n",
    "df2['calculatedfinishedsquarefeet'].fillna((df2['calculatedfinishedsquarefeet'].median()), inplace=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3bd9e489-5e83-4774-b22c-01561d401e3d",
   "metadata": {},
   "source": [
    "### Rooms\n",
    "\n",
    "The dataset contains different data about the bathrooms that come with the property.\n",
    "\n",
    "- **threequarterbathnbr** is the number of 3/4 baths = shower, sink, toilet with around 89% of NA values\n",
    "- **fullbathcnt** counts the number of full bathrooms = tub, sink, toilet with around 4% NA values\n",
    "- **calculatedbathnbr** describes the total number of bathrooms including partials with also around 4% of NA values\n",
    "- **bathroomcnt** contains the number of bathrooms and has around 0.2% of NA values\n",
    "- **bedroomcnt** counts the bedrooms on the property and has around 0.2% NA values <- most common\n",
    "- **roomcnt** counts the rooms on the property and has around 0.2% NA values <- most common\n",
    "\n",
    "Since *bathroomcnt* and *calculatedbathnbr* should contain the same information we can drop one of them, but lets look at them first. For *bedroomcnt* and *roomcnt* we will use the most common value to replace the NA values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "id": "f1170d01-7541-435c-bc84-20f59307e692",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "threequarterbathnbr False\n",
      "fullbathcnt True\n",
      "calculatedbathnbr True\n",
      "bathroomcnt True\n",
      "bedroomcnt True\n",
      "roomcnt True\n"
     ]
    }
   ],
   "source": [
    "rooms = ['threequarterbathnbr', 'fullbathcnt', 'calculatedbathnbr',\n",
    "              'bathroomcnt', 'bedroomcnt', 'roomcnt']\n",
    "\n",
    "for i in rooms:\n",
    "    print(i, i in columns)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "id": "9831b948-94ab-49a2-a9a6-44a8e48ef995",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>threequarterbathnbr</th>\n",
       "      <th>fullbathcnt</th>\n",
       "      <th>calculatedbathnbr</th>\n",
       "      <th>bathroomcnt</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>parcelid</th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>17108365</th>\n",
       "      <td>1.0</td>\n",
       "      <td>2.0</td>\n",
       "      <td>2.5</td>\n",
       "      <td>2.5</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17108511</th>\n",
       "      <td>NaN</td>\n",
       "      <td>3.0</td>\n",
       "      <td>3.0</td>\n",
       "      <td>3.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17108803</th>\n",
       "      <td>1.0</td>\n",
       "      <td>2.0</td>\n",
       "      <td>2.5</td>\n",
       "      <td>2.5</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17108926</th>\n",
       "      <td>1.0</td>\n",
       "      <td>2.0</td>\n",
       "      <td>2.5</td>\n",
       "      <td>2.5</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17109097</th>\n",
       "      <td>NaN</td>\n",
       "      <td>2.0</td>\n",
       "      <td>2.0</td>\n",
       "      <td>2.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17109335</th>\n",
       "      <td>1.0</td>\n",
       "      <td>2.0</td>\n",
       "      <td>2.5</td>\n",
       "      <td>2.5</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17109581</th>\n",
       "      <td>NaN</td>\n",
       "      <td>2.0</td>\n",
       "      <td>2.0</td>\n",
       "      <td>2.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17109604</th>\n",
       "      <td>1.0</td>\n",
       "      <td>2.0</td>\n",
       "      <td>2.5</td>\n",
       "      <td>2.5</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17109704</th>\n",
       "      <td>NaN</td>\n",
       "      <td>2.0</td>\n",
       "      <td>2.0</td>\n",
       "      <td>2.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17109927</th>\n",
       "      <td>NaN</td>\n",
       "      <td>2.0</td>\n",
       "      <td>2.0</td>\n",
       "      <td>2.0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "          threequarterbathnbr  fullbathcnt  calculatedbathnbr  bathroomcnt\n",
       "parcelid                                                                  \n",
       "17108365                  1.0          2.0                2.5          2.5\n",
       "17108511                  NaN          3.0                3.0          3.0\n",
       "17108803                  1.0          2.0                2.5          2.5\n",
       "17108926                  1.0          2.0                2.5          2.5\n",
       "17109097                  NaN          2.0                2.0          2.0\n",
       "17109335                  1.0          2.0                2.5          2.5\n",
       "17109581                  NaN          2.0                2.0          2.0\n",
       "17109604                  1.0          2.0                2.5          2.5\n",
       "17109704                  NaN          2.0                2.0          2.0\n",
       "17109927                  NaN          2.0                2.0          2.0"
      ]
     },
     "execution_count": 35,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Display the bathroom features\n",
    "df[['threequarterbathnbr', 'fullbathcnt', 'calculatedbathnbr', 'bathroomcnt']][500:510]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b9317c9a-ee6e-45f5-b53a-95cc1c2f4cd5",
   "metadata": {},
   "source": [
    "As we can see *threequarterbathnbr* counts as half a bath so we can drop *threequarterbathnbr*, *fullbathcnt* and *calculatedbathnbr*. We keep *bathroomcnt* and fill the NA values with the most common amount of bathrooms, because it has least amount of NA values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "id": "85f5be5a-0733-4a64-a31b-0645bfb09ce0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Drop threequarterbathnbr and fullbathcnt from the dataframe\n",
    "df.drop(columns=['threequarterbathnbr', 'fullbathcnt', 'calculatedbathnbr'], inplace=True)\n",
    "df2.drop(columns=['fullbathcnt', 'calculatedbathnbr'], inplace=True)\n",
    "\n",
    "# Fill the NA values of bathroomcnt with the most common number of baths\n",
    "df['bathroomcnt'].fillna(df['bathroomcnt'].mode()[0], inplace=True)\n",
    "df2['bathroomcnt'].fillna(df2['bathroomcnt'].mode()[0], inplace=True)\n",
    "\n",
    "# Fill the NA values of bedroomcnt with the most common number of bedrooms\n",
    "df['bedroomcnt'].fillna(df['bedroomcnt'].mode()[0], inplace=True)\n",
    "df2['bedroomcnt'].fillna(df2['bedroomcnt'].mode()[0], inplace=True)\n",
    "\n",
    "# Fill the NA values of bedroomcnt with the most common number of rooms\n",
    "df['roomcnt'].fillna(df['roomcnt'].mode()[0], inplace=True)\n",
    "df2['roomcnt'].fillna(df2['roomcnt'].mode()[0], inplace=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ae3f3e15-5dae-430f-a508-f1555b44b790",
   "metadata": {},
   "source": [
    "### Building Attributes\n",
    "\n",
    "The dataset contains different data about the attributes of the buildings on the property.\n",
    "\n",
    "- **numberofstories** contains the number of stories and has around 77% of NA values\n",
    "- **airconditioningtypeid** describes the different types of aircondition of the building and has around 72% of NA values \n",
    "- **heatingorsystemtypeid** shows different types of heatingsystems and has 38% of NA values\n",
    "- **buildingqualitytypeid** describes the overall quality of the building with around 35% of NA values \n",
    "\n",
    "In case of *numberofstories* we can fill the NA values with 1 since a building has at least 1 story. For the NA values of *airconditioningtypeid* we can replace them with the number 5 for None. Same applies for *heatingorsystemtypeid* where we chose the value 13 which resembles None. For the feature of *buildingqualitytypeid* we apply prior used method on filling the NA values with the most common buildingqualitytype."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "id": "1d49c497-a78a-438b-95ca-681fa9ff7cae",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "numberofstories False\n",
      "airconditioningtypeid False\n",
      "heatingorsystemtypeid True\n",
      "buildingqualitytypeid True\n"
     ]
    }
   ],
   "source": [
    "building = ['numberofstories', 'airconditioningtypeid', 'heatingorsystemtypeid',\n",
    "              'buildingqualitytypeid']\n",
    "\n",
    "for i in building:\n",
    "    print(i, i in columns)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "id": "cc89dd65-96d8-4b9b-83c9-31eda0215e82",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Filling the NA values of numberofstories with 1\n",
    "df.numberofstories.fillna(1, inplace=True)\n",
    "\n",
    "# Filling the NA values of airconditioningtypeid with 5 which stands for None\n",
    "df.airconditioningtypeid.fillna(5, inplace=True)\n",
    "\n",
    "# Filling the NA values of heatingorsystemtypeid with 13 which stands for None\n",
    "df.heatingorsystemtypeid.fillna(13, inplace=True)\n",
    "df2.heatingorsystemtypeid.fillna(13, inplace=True)\n",
    "\n",
    "# Filling the NA values of buildingqualitytypeid with most common values\n",
    "df['buildingqualitytypeid'].fillna(df['buildingqualitytypeid'].mode()[0], inplace=True)\n",
    "df2['buildingqualitytypeid'].fillna(df2['buildingqualitytypeid'].mode()[0], inplace=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d46f3de-155f-4df7-bfa3-6b580c64839b",
   "metadata": {},
   "source": [
    "### Location\n",
    "\n",
    "The dataset contains different data about location of the property.\n",
    "\n",
    "- **regionidneighborhood** describes the neighborhood the property is located in with around 61% NA values\n",
    "- **longitude** has around 0.2% of NA values \n",
    "- **latitude** has around 0.2% of NA values\n",
    "- **propertyzoningdesc** contains information about the allowed land uses with 33% of NA values\n",
    "- **regionidzip** is the zip code the property is located in with 0.4% of NA values\n",
    "- **propertycountylandusecode** describes the county land use code and has around 0.2% of NA values\n",
    "- **regionidcounty** is the county the property is located in 0.2% of NA values\n",
    "- **regionidcity** is the City in which the property is located with around 2% of NA values\n",
    "- **fips** is the Federal Information Processing Standard code and has around 0.2% of NA values\n",
    "- **censustractandblock** is the Census tract and block ID combined with 2% of NA values\n",
    "- **rawcensustractandblock** describes Census tract and block ID combined and has around 0.2% of NA values. \n",
    "\n",
    "In case of *regionidneighborhood* and *regionidcity* we can drop this features since longitude and latitude combined provide essentially the same information. For all other features with less than 1% of NA values we will use the most common values. In the case of *propertyzoningdesc* we will investigate further. For the *fips* feature we will use the most common value to replace the NA values. Since *censustractandblock* and *rawcensustractandblock* provide the same information we can drop *censustractandblock* and fill the NA values of *rawcensustractandblock* with the most common value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "id": "38068581-d60e-4f45-a577-8160333a6e5f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "regionidneighborhood False\n",
      "longitude True\n",
      "latitude True\n",
      "propertyzoningdesc True\n",
      "regionidzip True\n",
      "propertycountylandusecode True\n",
      "regionidcounty True\n",
      "regionidcity True\n",
      "fips True\n",
      "censustractandblock True\n",
      "rawcensustractandblock True\n"
     ]
    }
   ],
   "source": [
    "location = ['regionidneighborhood', 'longitude', 'latitude', 'propertyzoningdesc', 'regionidzip', \n",
    "            'propertycountylandusecode', 'regionidcounty', 'regionidcity', 'fips', 'censustractandblock', 'rawcensustractandblock']\n",
    "\n",
    "for i in location:\n",
    "    print(i, i in columns)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "id": "80f8e82a-0625-4b1f-a869-23040b0cb65e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Drop regionidneighborhood, regionidcity and censustractandblock\n",
    "df.drop(columns=['regionidneighborhood', 'regionidcity', 'censustractandblock'], axis=1, inplace=True)\n",
    "df2.drop(columns=['regionidcity', 'censustractandblock'], axis=1, inplace=True)\n",
    "\n",
    "# Fill the other features with the most common values\n",
    "df['longitude'].fillna(df['longitude'].mode()[0], inplace=True)\n",
    "df['latitude'].fillna(df['latitude'].mode()[0], inplace=True)\n",
    "df['regionidzip'].fillna(df['regionidzip'].mode()[0], inplace=True)\n",
    "df['propertycountylandusecode'].fillna(df['propertycountylandusecode'].mode()[0], inplace=True)\n",
    "df['regionidcounty'].fillna(df['regionidcounty'].mode()[0], inplace=True)\n",
    "df['fips'].fillna(df['fips'].mode()[0], inplace=True)\n",
    "df['rawcensustractandblock'].fillna(df['rawcensustractandblock'].mode()[0], inplace=True)\n",
    "\n",
    "df2['longitude'].fillna(df2['longitude'].mode()[0], inplace=True)\n",
    "df2['latitude'].fillna(df2['latitude'].mode()[0], inplace=True)\n",
    "df2['regionidzip'].fillna(df2['regionidzip'].mode()[0], inplace=True)\n",
    "df2['propertycountylandusecode'].fillna(df2['propertycountylandusecode'].mode()[0], inplace=True)\n",
    "df2['regionidcounty'].fillna(df2['regionidcounty'].mode()[0], inplace=True)\n",
    "df2['fips'].fillna(df2['fips'].mode()[0], inplace=True)\n",
    "df2['rawcensustractandblock'].fillna(df2['rawcensustractandblock'].mode()[0], inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "id": "7b3daa65-c0ea-4bb3-9b07-4d3a3896745e",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "LAR1         275029\n",
       "LAR3          67105\n",
       "LARS          54859\n",
       "LBR1N         52750\n",
       "LAR2          48808\n",
       "              ...  \n",
       "SMRVC-R3*         1\n",
       "COR1-A-*          1\n",
       "LBC2*             1\n",
       "LCR2**            1\n",
       "BHR1**            1\n",
       "Name: propertyzoningdesc, Length: 5638, dtype: int64"
      ]
     },
     "execution_count": 41,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Have a look at the values in propertyzoningdesc\n",
    "df.propertyzoningdesc.value_counts()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "657fb959-0d57-4d58-939f-25528b95d066",
   "metadata": {},
   "source": [
    "As we can see the feature *propertyzoningdesc* has a lot of unique values which may provide some valuable information about the property so we also fill this with the most common value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "id": "15d1ee6c-c992-42f6-a4e0-396f273ac617",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Fill the NA values with the most common value\n",
    "df['propertyzoningdesc'].fillna(df['propertyzoningdesc'].mode()[0], inplace=True)\n",
    "df2['propertyzoningdesc'].fillna(df2['propertyzoningdesc'].mode()[0], inplace=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b276904c-e9f7-4de2-9977-6a5797a16244",
   "metadata": {},
   "source": [
    "### Tax Values\n",
    "\n",
    "The dataset contains different data about the tax values of the property.\n",
    "\n",
    "- **landtaxvaluedollarcnt** is the assed value of the land area of the parcel and has around 2% of NA values\n",
    "- **structuretaxvaluedollarcnt** is the assed value of the structure on the property and has around 2% of NA values\n",
    "- **taxvaluedollarcnt** is the total tax assessed value of the parcel with around 1% of NA values\n",
    "- **taxamount** is the total property tax assessed for the property and has around 0.9% of NA values\n",
    "- **assessmentyear** is the year of the property tax assessment with around 0.2% NA values.\n",
    "\n",
    "\n",
    "For the feature *landtaxvaluedollarcnt* we can fill the NA values with 0 because it means the property has no structure value. The opposite counts for *structuretaxvaluedollarcnt* NA values which means it has no land value. In case of *taxvaluedollarcnt* we will replace the NA values with the average, same for *taxamount*. The NA values of the feature *assessmentyear* will be replaced by the most common year"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 43,
   "id": "157afd45-b658-4cd8-9166-247e64e18119",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "landtaxvaluedollarcnt True\n",
      "structuretaxvaluedollarcnt True\n",
      "taxvaluedollarcnt True\n",
      "taxamount True\n",
      "assessmentyear True\n"
     ]
    }
   ],
   "source": [
    "tax = ['landtaxvaluedollarcnt', 'structuretaxvaluedollarcnt', 'taxvaluedollarcnt', 'taxamount', 'assessmentyear']\n",
    "\n",
    "for i in tax:\n",
    "    print(i, i in columns)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 44,
   "id": "564dc74b-ad64-472b-a8ce-aa6a813833a5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Fill NA values for landtaxvaluedollarcnt and structuretaxvaluedollarcnt with 0\n",
    "df.landtaxvaluedollarcnt.fillna(0, inplace=True)\n",
    "df.structuretaxvaluedollarcnt.fillna(0, inplace=True)\n",
    "df2.landtaxvaluedollarcnt.fillna(0, inplace=True)\n",
    "df2.structuretaxvaluedollarcnt.fillna(0, inplace=True)\n",
    "\n",
    "\n",
    "# Fill NA values for taxvaluedollarcnt and taxamount with the average\n",
    "df['taxvaluedollarcnt'].fillna(df['taxvaluedollarcnt'].mean(), inplace=True)\n",
    "df['taxamount'].fillna(df['taxamount'].mean(), inplace=True)\n",
    "df2['taxvaluedollarcnt'].fillna(df2['taxvaluedollarcnt'].mean(), inplace=True)\n",
    "df2['taxamount'].fillna(df2['taxamount'].mean(), inplace=True)\n",
    "\n",
    "# Fill NA values for assessmentyear with the most common\n",
    "df['assessmentyear'].fillna(df['assessmentyear'].mode()[0], inplace=True)\n",
    "df2['assessmentyear'].fillna(df2['assessmentyear'].mode()[0], inplace=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4456de41-986f-4153-8c4e-07f788aff9c3",
   "metadata": {},
   "source": [
    "### Other features\n",
    "\n",
    "- **storytypeid** is a numerical ID that discribes all types of properties and has around 99% NA values\n",
    "- **basementsqft** describe the Square footage of basement with around 99% NA values\n",
    "- **yardbuildingsqft26** means Storage shed square footage with approximately 99% NA values\n",
    "- **yardbuildingsqft17** explains if a patio is in the yard with around 97% NA values\n",
    "- **architecturalstyletypeid** gives information about the architectual style with also 99% NA values\n",
    "- **typeconstructiontypeid** shows out of which material the house is made with 99% NA values\n",
    "- **buildingclasstypeid** describes the internal structure of the home and has 99% NA values\n",
    "- **decktypeid** is the type of deck on the property with 99% of NA values\n",
    "- **propertylandusetypeid** describes the type of land use the property is zoned for and has around 0.2% NA values\n",
    "- **yearbuilt** is the yeat the principal residence was built and has around 2% of NA values\n",
    "- **unitcnt** is the number of units on the property and has around 33% of NA values.\n",
    "\n",
    "Only *basementsqft*, *yardbuildingsqft26* and *yardbuildingsqft17* can gives us information about if the property has a basement or yard and if so how large it is. So we will change the NA values for them to 0. For *decktypeid* we can see the value 66 or NaN, so we keep this feature and switch 66 to 1 for yes and NaN to 0 for no. The rest of the features will be droped. For *propertylandusetypeid* and *yearbuilt* we will fill the NA values with most common values. For *unitcnt* we will fill the NA values with 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "id": "083564c0-cf27-49cc-bd48-4edcbc6b88e1",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "storytypeid False\n",
      "basementsqft False\n",
      "yardbuildingsqft26 False\n",
      "yardbuildingsqft17 False\n",
      "architecturalstyletypeid False\n",
      "typeconstructiontypeid False\n",
      "buildingclasstypeid False\n",
      "decktypeid False\n",
      "propertylandusetypeid True\n",
      "yearbuilt True\n",
      "unitcnt True\n"
     ]
    }
   ],
   "source": [
    "others = ['storytypeid', 'basementsqft', 'yardbuildingsqft26', 'yardbuildingsqft17', 'architecturalstyletypeid', 'typeconstructiontypeid', 'buildingclasstypeid',\n",
    "         'decktypeid', 'propertylandusetypeid', 'yearbuilt', 'unitcnt']\n",
    "\n",
    "for i in others:\n",
    "    print(i, i in columns)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "id": "45f202e7-8d3c-46cf-a531-cb8cb5a9b759",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Replace NA values for basementsqft, yardbuildingsqft26 and yardbuildingsqft17 with 0\n",
    "df.basementsqft.fillna(0, inplace=True)\n",
    "df.yardbuildingsqft26.fillna(0, inplace=True)\n",
    "df.yardbuildingsqft17.fillna(0, inplace=True)\n",
    "\n",
    "# Replace NA values with 0 and 66 with 1 for decktypeid\n",
    "df.decktypeid.fillna(0,inplace = True)\n",
    "df.decktypeid.replace(to_replace = 66.0, value = 1,inplace = True)\n",
    "\n",
    "# Drop the other features\n",
    "df.drop(columns=['storytypeid', 'architecturalstyletypeid', 'typeconstructiontypeid', 'buildingclasstypeid'], inplace=True)\n",
    "\n",
    "# Fill NA values with the most common values for propertylandusetypeid and yearbuilt\n",
    "df.propertylandusetypeid.fillna(df['propertylandusetypeid'].mode()[0], inplace=True)\n",
    "df.yearbuilt.fillna(df['yearbuilt'].mode()[0], inplace=True)\n",
    "df2.propertylandusetypeid.fillna(df2['propertylandusetypeid'].mode()[0], inplace=True)\n",
    "df2.yearbuilt.fillna(df2['yearbuilt'].mode()[0], inplace=True)\n",
    "\n",
    "# Fill NA values with 1 for unitcnt\n",
    "df.unitcnt.fillna(1, inplace=True)\n",
    "df2.unitcnt.fillna(1, inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "id": "89a36883-7c30-4507-83eb-0a03fdcfca61",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "airconditioningtypeid           0.0\n",
       "propertylandusetypeid           0.0\n",
       "propertyzoningdesc              0.0\n",
       "rawcensustractandblock          0.0\n",
       "regionidcounty                  0.0\n",
       "regionidzip                     0.0\n",
       "roomcnt                         0.0\n",
       "unitcnt                         0.0\n",
       "propertycountylandusecode       0.0\n",
       "yardbuildingsqft17              0.0\n",
       "yearbuilt                       0.0\n",
       "numberofstories                 0.0\n",
       "fireplaceflag                   0.0\n",
       "structuretaxvaluedollarcnt      0.0\n",
       "taxvaluedollarcnt               0.0\n",
       "assessmentyear                  0.0\n",
       "landtaxvaluedollarcnt           0.0\n",
       "yardbuildingsqft26              0.0\n",
       "taxamount                       0.0\n",
       "pooltypeid7                     0.0\n",
       "pooltypeid10                    0.0\n",
       "basementsqft                    0.0\n",
       "bathroomcnt                     0.0\n",
       "bedroomcnt                      0.0\n",
       "buildingqualitytypeid           0.0\n",
       "decktypeid                      0.0\n",
       "calculatedfinishedsquarefeet    0.0\n",
       "fips                            0.0\n",
       "pooltypeid2                     0.0\n",
       "fireplacecnt                    0.0\n",
       "garagetotalsqft                 0.0\n",
       "heatingorsystemtypeid           0.0\n",
       "latitude                        0.0\n",
       "longitude                       0.0\n",
       "lotsizesquarefeet               0.0\n",
       "poolcnt                         0.0\n",
       "poolsizesum                     0.0\n",
       "garagecarcnt                    0.0\n",
       "taxdelinquencyflag              0.0\n",
       "dtype: float64"
      ]
     },
     "execution_count": 47,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df.isnull().mean().sort_values()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 48,
   "id": "0f9495d8-5742-4679-8d1d-165a032101ba",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "bathroomcnt                     0.0\n",
       "assessmentyear                  0.0\n",
       "taxvaluedollarcnt               0.0\n",
       "structuretaxvaluedollarcnt      0.0\n",
       "yearbuilt                       0.0\n",
       "unitcnt                         0.0\n",
       "roomcnt                         0.0\n",
       "regionidzip                     0.0\n",
       "regionidcounty                  0.0\n",
       "rawcensustractandblock          0.0\n",
       "landtaxvaluedollarcnt           0.0\n",
       "propertyzoningdesc              0.0\n",
       "propertycountylandusecode       0.0\n",
       "lotsizesquarefeet               0.0\n",
       "longitude                       0.0\n",
       "latitude                        0.0\n",
       "heatingorsystemtypeid           0.0\n",
       "fips                            0.0\n",
       "calculatedfinishedsquarefeet    0.0\n",
       "buildingqualitytypeid           0.0\n",
       "bedroomcnt                      0.0\n",
       "propertylandusetypeid           0.0\n",
       "taxamount                       0.0\n",
       "dtype: float64"
      ]
     },
     "execution_count": 48,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df2.isnull().mean().sort_values()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 49,
   "id": "34ee511c-d95c-4d7a-b580-1a6fd23e403f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Creating target variable\n",
    "df['total_value'] = df['structuretaxvaluedollarcnt'] + df['landtaxvaluedollarcnt']\n",
    "df2['total_value'] = df2['structuretaxvaluedollarcnt'] + df2['landtaxvaluedollarcnt']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 50,
   "id": "a4b7418b-5c65-471a-b06e-5c02fde559aa",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<AxesSubplot:>"
      ]
     },
     "execution_count": 50,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAA5kAAAHYCAYAAADHzSzGAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAEAAElEQVR4nOzdZ1RURx+A8WfpTXpRxNjFiooi9gh2VOw1oib2bqIYsKAoxliwYDcx9oaKIiBWLLF3UaPGhoIICErv7L4feLO6AZUkwgrO75w9xzs7d/Y/Fwd27sydkchkMhmCIAiCIAiCIAiC8AmoKDsAQRAEQRAEQRAEoeQQnUxBEARBEARBEAThkxGdTEEQBEEQBEEQBOGTEZ1MQRAEQRAEQRAE4ZMRnUxBEARBEARBEAThkxGdTEEQBEEQBEEQBOGTEZ1MQRAEQRAEQRCEL1hycjKdO3cmIiIiz3v37t2jR48etG/fnunTp5Odnf3R8kQnUxAEQRAEQRAE4Qt169Yt+vfvT1hYWL7vu7q64uHhwZEjR5DJZPj6+n60TNHJFARBEARBEARBKGESExOJiIjI80pMTFTI5+vry6xZszA3N89TxosXL0hPT6devXoA9OjRg8OHD3/0s9U+SQ0E4R/Iin2i7BAE4YuTfemgskMQhC+Trr6yIxCEL5Z2q++UHUKBFNZ34807g1i5cmWe9HHjxjF+/Hj58bx5895bRkxMDGZmZvJjMzMzoqOjP/rZopMpCIIgCIIgCIJQwgwePJju3bvnSdfXL/jNL6lUikQikR/LZDKF4/cRnUxBEApEJpMx3cubqpUr8O2AXsoORxBKhDN/PGNF0EUys3OoamnC7L4O6GlpKOQJCX3CmiNXkEgkGOho4tGnFeVMDRTy/LDxMGb6urj3bFGU4QtCsXXm9mNW7D+d2/bKmjF7UEf0tDUV8oTc+JM1AWdz256uFh4uHShnZsSUdft5HhMvzxcZG0+Dal+xfGzPIq6FIHyYvr7+P+pQ5qd06dK8evVKfhwbG5vvtNq/E89kCoLwUY/DnjN0gjvHTp1VdiiCUGK8Tk5j1q4QFg9pj7/7AKyM9VkeeFEhT3pmNtN2nMB7SAd8p/ShZa0KLNiv2A43htzgxpOXRRm6IBRrr5NSmbX5EItHdsN/znCsTA1Zvv+0Qp70zCym/RaI96ju+M78lpY2VViw6zgAi0fmpvnO/BYPlw6U0tHCvX9bZVRFKCmkOYXz+gTKli2LpqYm165dA8Df35+WLVt+9DzRyVSSpKQkxo4d+8E87u7uvHjx4oN5XFxcuHTp0ieLa8WKFaxYseKTlSeUDLv2BdKzS3vaOYhREkH4VC48CKdWOXPKmxkC0LtZLYKvP0Qmk8nzSGUykEFyeiYAaRlZaKqpyt+/8ugF5+8/p1fTWkUauyAUZxf+eEqt8qUpb2EMQO+v6xN86a5i25PKQCYjOS0DgLSMTDTVFScAZmXn4LEpCNc+rSltLJ69Ff4DmbRwXv/B8OHDuX37NgCLFy9m/vz5dOjQgdTUVAYNGvTR88V0WSVJSEjg3r17H8xz6dKlj3ZEBaEoTJ88BoDzl68rORJBKDmi45MpbagnP7Yw0CM5PZOUjCz5lFkdTXWm92rJYB8/DHW1yJHK2DQ+9/mamIQUFu0/x6qRndh7/g+l1EEQiqPoN0kKnUILo1K5bS89Uz5lVkdLg+nftGfwwm0Y6mqTI5WyaepAhXL2nwvFzEAPx/rVijR+QSgsISEh8n//8ssv8n9Xr16dvXv3/qOyxEimknh5eRETE8PYsWPZt28fnTt3pkuXLri5uZGSksL69euJiYlhxIgRvHnzhuDgYPr06YOzszMdOnTg+vWCfdmfP38+v/32m/x4/PjxHDt2jD///BMXFxd69uyJg4MDO3fuzHOutbW1/N9+fn64ubkBEBoaSv/+/enevTvfffcd4eHh//FqCIIgfHmkMhn5rZ2g+k7iw8g41h+9it+P/Tg2ezDD2tgyZdMRsnJycN96jCndmmKmr1uEUQtC8SeVyshv2RJVlXfa3otXrA86h9/soRxbOJZhTk2Ysm6/wmjntuNXGObUtAgiFko8qbRwXkokOplKMmPGDMzNzZkwYQJr165l69atBAQEoK2tzcqVKxkxYgTm5uasX78eAwMDdu3axdq1azl48CDDhg1j/fr1Bfqcrl27EhgYCEBycjI3btzg66+/Zs+ePYwZM4Z9+/axZcsWFi5cWKDyMjMzmTFjBt7e3uzfv59vv/2WmTNn/uvrIAiC8KUqY1iKVwmp8uOYhBT0tTXR1lSXp51/EE7diqXlC/30bV6bR1Gvuf0smojXiSz2P0+fxb7svXCXozcf4bn7ZJHXQxCKmzLG+rxKSJYfx8Qnoa+jhbbm20W3zt99Qt3KVpQzMwKgbytbHr2IJT4lDYD7z6PJkUppWK1c0QYvCMWEmC6rZFeuXMHBwQEjo///EuvbF3d3d4U8KioqrFq1ipCQEJ4+fcrly5dRUSnY/YGaNWuSmZnJs2fPuHHjBo6OjmhoaODm5sbvv//OunXr+PPPP0lNTf14YUBYWBjh4eGMHj1anpacnPyBMwRBEIT8NLG2wvvgeZ69iqe8mSF7z9+lVe0KCnlqWJmy6+wd4pJSMSmlw8nbTylrXArbSpYc8Xj7TMyaw1eIT0kXq8sKQgE0qVkB770hPIt+TXkLY/aeuUmrulUU8tT4qjS7Tl0nLjEFE31dTt58SFlTA4z0dAC4+udzGlmXL9BWDoLwMbL/+Pzk50h0MpVM+rehbJlMRnZ2tkJaSkoKvXr1wtnZGTs7O6ytrdm+fXuBP8PZ2ZlDhw5x48YNRowYAcCkSZPQ19fHwcEBJycn+Wjn3/21F85fMUmlUqysrPD39wcgJyeH2NjYAsciCIIg5DIupYNnPwdcNx0lKycHK1MDvPo7cjc8Bs/dp/Cd0odGVa0Y7FCPYav8UVdVRV9Hk6VDOyo7dEEo1oz1dfEc7ITr+gNkZedgZWaE17eduBv2Es+th/Gd+S2NqpdncFt7hnnvQF1NFX0dbZaO6SEv43nMGyxNDD7wKYLwDyh5amthEJ1MJVFTUyM7O5tGjRqxZcsWxowZg6GhIb6+vtjb2wOgqqpKTk4OYWFhSCQSRo0ahUwmY+rUqeTkFHxZ4i5dujBixAgyMzNp0KABAOfOnSM4OBgLCwt5h/XvZRoZGfHw4UOqVq1KSEgIhoaGVKpUiYSEBK5evUrDhg3Zt28fAQEBbN269RNdGeFzNm/GZGWHIAglSoua5WlRs7xCmoGuFr5T+siP+zWvTb/mtT9YzugOdoUSnyCUVC3qVKZFncoKaQa62vjO/FZ+3M/Bln4OtvmeP21Au0KNTxCKO9HJVBITExMsLS2ZN28eI0eOxMXFhaysLGrVqoWnpycArVq1YsSIEfzyyy/UqFGDjh07IpFIaN68uXyvmoIoU6YMRkZG1K9fXz6tY/z48QwYMABNTU2qV69O2bJliYiIUDhv8uTJjBo1ClNTUxo0aMCbN2/Q0NBg+fLlzJs3j4yMDPT09FiwYMGnuzCCIAiCIAiC8CUpgdNlJbJ3l8kShCKQFftE2SEIwhcn+9JBZYcgCF8mXbF/oiAoi3ar75QdQoFkht8qlHI1ytUtlHILQoxklgDPnz9n/Pjx+b7n5eVFnTp1ijgiQRAEQRAEQRAKRFrwx+CKC9HJLAG++uor+UI8glBYZDIZ0728qVq5At8O6KXscAShRDjzxzNWBF0kMzuHqpYmzO7rgJ6WhkKekNAnrDlyBYlEgoGOJh59Wsm3NPnLDxsPY6avK1aXFYQCOnP7MSv2n85te2XNmD2oI3ramgp5Qm78yZqAs7ltT1cLD5cOlDMzYsq6/TyPiZfni4yNp0G1r1g+tmcR10IQPl9f/D6ZSUlJjB07ttA/x9HRMc8zj//GihUrWLFiRYE+y8/PDzc3t//8me8THh7OtGnTCq184fPxOOw5Qye4c+zUWWWHIgglxuvkNGbtCmHxkPb4uw/Aylif5YEXFfKkZ2YzbccJvId0wHdKH1rWqsCC/YrtcGPIDW48eVmUoQtCsfY6KZVZmw+xeGQ3/OcMx8rUkOX7TyvkSc/MYtpvgXiP6o7vzG9paVOFBbuOA7B4ZG6a78xv8XDpQCkdLdz7t1VGVYSSQiYtnJcSffGdzISEBO7du6fsMIqlyMhIwsPDlR2GUAR27QukZ5f2tHMQoySC8KlceBBOrXLmlDczBKB3s1oEX3/Iu0slSGUykEFyeiYAaRlZaKqpyt+/8ugF5+8/p1fTWkUauyAUZxf+eEqt8qUpb2EMQO+v6xN86a5i25PKQCYjOS0DgLSMTDTVFScAZmXn4LEpCNc+rSltLJ69Ff4DqbRwXkr0xU+X9fLyIiYmhrFjx1KlShUuXLhAQkIC5ubmLF26lOjoaEaMGEFAQAAqKip0794dHx8fRo8ezYEDBzA1NSU+Pp7OnTtz8uRJdu/ejb+/P2lpaairq+Pt7U2lSpXkn+fn58fly5f5+eefAXBxcWHcuHHY29uzfv16goODycnJoXnz5ri6uiKRSPj111/x9fXFyMgIfX19bGxsADh58iTLli1DKpVSrlw55syZg6mpab71DA4OZuPGjaSnp5OZmclPP/2Era0tLi4uGBgY8PDhQ5YtW8ajR49Ys2YNEomEOnXqMHfuXNauXUt0dDTPnj3jxYsX9O7dm9GjR+Pl5UVERASenp7MmjWr8H9YgtJMnzwGgPOXrys5EkEoOaLjkyltqCc/tjDQIzk9k5SMLPmUWR1Ndab3aslgHz8MdbXIkcrYNL47ADEJKSzaf45VIzux9/wfSqmDIBRH0W+SFDqFFkalctteeqZ8yqyOlgbTv2nP4IXbMNTVJkcqZdPUgQrl7D8XipmBHo71qxVp/IJQHHzxI5kzZszA3NycqVOn8uTJE3bt2sWRI0coU6YMBw8epFatWvTt25eFCxcyd+5c+vfvT506dejQoQOHDx8G4OjRo7Rt25aMjAyOHz/O1q1bCQwMpFWrVvI9KD/mzJkz3Llzh71793LgwAGio6M5ePAgt2/fZt++fezfv5+NGzcSFRUFQFxcHB4eHqxatYqAgABsbW2ZM2dOvmVLpVJ27drF2rVrOXjwIMOGDWP9+vXy962trTly5AjGxsbMnz+f3377jaCgIHJycjh9Onf6yIMHD9iwYQN79uxh/fr1JCYmMmPGDGrXri06mIIgCP+CVCbj/7tKKVB9J/FhZBzrj17F78d+HJs9mGFtbJmy6QhZOTm4bz3GlG5NMdPXLcKoBaH4k0pl5NP0UFV5p+29eMX6oHP4zR7KsYVjGebUhCnr9iuMdm47foVhTk2LIGKhpJPJpIXyUqYvfiTzL+XLl+fHH39kz549PH36lJs3b/LVV18BMHr0aHr27ImWlhaLFi0CwNnZmfnz5zNw4EACAwP5/vvv0dPTw9vbm6CgIMLCwvj999+pUaNGgT7/woULhIaG0qNHDwDS09OxtLQkNjaWr7/+Gl3d3C8RHTp0QCqVEhoaio2NDVZWVgD07dtXoeP4LhUVFVatWkVISAhPnz7l8uXLqKi8vb/w18jojRs3sLW1pXTp0gDyut67dw97e3s0NDQwMTHB0NCQpKSkf3R9BUEQBEVlDEtx51mM/DgmIQV9bU20NdXlaecfhFO3Ymn5Qj99m9dmsf95bj+LJuJ1Iov9zwMQl5SKVCojMzubWX0dirYiglDMlDHW505YpPw4Jj4JfR0ttDXfLrp1/u4T6la2opyZEQB9W9my2DeE+JQ0jPR0uP88mhyplIbVyhV5/IJQHHzxI5l/uXPnDkOHDkUqldK+fXvatGkjv1uVlJRESkoKcXFxxMfHA7kds4SEBEJDQ4mOjqZ+/fq8fPmSvn37kpSURMuWLenevTt/34ZUIpEopGVlZQGQk5PD4MGD8ff3x9/fnz179jBq1Kg8+dXUcu8LSP82z1omk5GdnZ1v3VJSUujVqxcRERHY2dnh4uKi8L6Wlpa8bMk7d9Bfv37N69evAdDUfLvi2t9jEgRBEP65JtZWhD6L5tmreAD2nr9Lq9oVFPLUsDLl2uOXxCWlAnDy9lPKGpfCtpIlRzwG4TulD75T+tCrSS3a1asiOpiCUABNalYg9Ekkz6Jzv+PsPXOTVnWrKOSp8VVprj18TlxiCgAnbz6krKkBRno6AFz98zmNrMsrfG8ShH+tBD6T+cV3MtXU1MjOzubKlSs0atSI/v37U6FCBU6dOkVOTu6eNZ6engwcOJABAwbg6ekpP7dLly7MmjWLTp06AXD79m3Kly/PkCFDqFOnDsePH5eX8RcjIyMeP36MTCYjPDycBw8eANC4cWP8/f1JSUkhOzubsWPHcuTIEZo0acLJkydJSkoiIyODY8eOAVC3bl1u3bolX7F29+7d2Nvb51vHsLAwJBIJo0aNwt7enmPHjuWJC6BOnTrcvHmTV69eAfDTTz9x4sSJ9147VVXV93ZsBUEQhA8zLqWDZz8HXDcdpfvPO3kY9ZrJzk25Gx5Dn8W+ADSqasVgh3oMW+VPn0W+7Dp7h6VDOyo5ckEo3oz1dfEc7ITr+gN0n/ULD1+8YnJvR+6GvaTP3I0ANKpensFt7RnmvYM+c39j18nrLB3TQ17G85g3WJoYvO8jBOGfKYGry37x02VNTEywtLQkJCSE9PR0unTpAkDt2rWJiIjg0KFDhIeHs2TJEmQyGT179uTQoUM4OTnh7OzM8uXLWbp0KQDNmjVj586dODk5IZPJsLOz4+HDhwqf17RpU/bt20eHDh2oWLEiDRo0AHK3Hbl//z59+vQhJyeHFi1a0L17dyQSCYMHD6ZXr17o6+tjaWkJgKmpKXPmzGHcuHFkZWVhaWnJvHnz8q1j9erVqVGjBh07dkQikdC8eXOuXbuWJ5+FhQXTp0+Xj+jWq1ePHj16sHr16nzLrVy5MklJSbi6usqn1gol27wZk5UdgiCUKC1qlqdFzfIKaQa6WvhO6SM/7te8Nv2a1/5gOaM72BVKfIJQUrWoU5kWdSorpBnoauM781v5cT8HW/o52OZ7/rQB7Qo1PkEo7iQyMe9RKGIZd44pOwRB+OJIr4coOwRB+HKpa3w8jyAIn5x2f8+PZ/oMZNw//fFM/4Jm9a8LpdyC+OKnywqCIAiCIBQa0cEUBOEL9MVPlxUEQRAEQRAEQVAaJT8/WRhEJ1MQBM5cu8PybQfJzM6mWvmyeI4ZgJ6OtkKeHYdOsTP4DFoa6lQsW5rpw/tgUEqXpJQ0Zq3eztMX0chkMpxb2fNd97ZKqokgFC9nHr5kxam7ZGZLqWpuwOzOtui9s4VJQOgztl56JD9OzsgiJimNI+M7oqaqwrzgGzyITkBbXZWudSvQ365yfh8jCMJHnPkzkhXHb5GZI6WqhSGznRuhp6WukCfkXgRrTt5GIpFgoK2Bh7Md5YxLKSlioURR8kqwhUFMl1WypKQkxo4dq+ww/jVfX18CAwOVHYbwH7xOSGLmym0scR1GwAoPrCxMWLbtoEKey7f/5Lf9x/ll9nj2eLvTwrYWnmt3ArBqVyAWJobsXzadHQtc8T3yO7cePFFGVQShWHmdksGswOss7tkY/9HtsDLSZXnIHYU8XWzK4zu8Nb7DW7P9OwdM9bRwa18XEz0tFh0LRUdDDb+Rbdn6rQNnH0dx5uFLJdVGEIqv1ynpzDpwicV9m+M/vlNuWzx+SyFPelY20/wu4N23Ob6jO9DSuiwLgq8rKWJB+PyJTqaSJSQkcO/ePWWH8a9dv36dzMxMZYch/AcXbt2ndpXylLc0B6BP+xYc+v2Kwl6ofzx5TmMba0qb5G5K3bpxXU5fvUNWVjY/fteLyYO7AxD7JpHMrOw8o6CCIOR14Wk0tcoYUt5YD4DethUJvhv+3n2IN134E2MdTXrZVgLgXlQ8nep8haqKBHVVFVpUKc2x+y+KLH5BKCkuPI6iVlljypvkjkr2bliF4NvPFNqiVCoDWe5sAoC0zCw01VSVEq9QAoktTIRPzcvLi5iYGMaOHUuVKlW4cOECCQkJmJubs3TpUqKjoxkxYgQBAQGoqKjQvXt3Vq9ejbGxMdOmTSMpKYmYmBi6d+/OxIkT8fPz49SpU8THxxMTE0O/fv148eIFFy9exNDQkF9//RVNTU327dvHxo0bkUgk1KpVi5kzZ6Krq4u1tbV8704/Pz8uX77Mzz//jKOjI87Ozpw9e5a0tDQWLFhAYmIiISEhXLx4ETMzM1q0aKHkqyn8G1Gxbyhtaig/tjAxJDk1nZS0dHlnsU7VCuw4dJrImNdYmhvjH3KRrOxs4pNTMDMyQE1VFfflmzl24QaOjepSwdJCSbURhOIjOjGN0vo68mMLfW2SM7JJycxWmDIL8CY1gy2XHrLzO0d5Wh1LI4JuP6eelQlZOVJO3I9ETVVsDC8I/1R0Qurf2qIOyRlZpGRky6fM6miqM71zQwZvOI6htgY5MhmbvmujrJAF4bMnRjKVbMaMGZibmzN16lSePHnCrl27OHLkCGXKlOHgwYPUqlWLvn37snDhQubOnUv//v2pUaMGgYGBdO7cGV9fXwICAti8eTOvX78G4Pbt26xevZoNGzYwf/58WrZsSUBAAAC///47Dx48YO3atWzdupWAgAC0tbVZuXLlR2M1NDRk79699OvXj3Xr1tG0aVMcHR2ZMGGC6GAWY1KZDMj7xVRF5e2vhwY1qzCqd0cmLVxPv6kLkKhIMNDTQf2du7jzJw7mzMYFJCansHZPcFGELgjFmlQmQ5JPn1A1n8R9N57SqloZrIx05Wk/tKmDBOi3IYTv91ygcUVz1FXEn3VB+Kdy22Ledqeq8jbtYXQ860/fxW9sR45N6cawFrWY4nvuvTMPBOEfkUoL56VEYiTzM1G+fHl+/PFH9uzZw9OnT7l58yZfffUVAKNHj6Znz55oaWmxaNEiAIYOHcrFixfZsGEDDx8+JCsri7S0NABsbW3R09NDTy93ClaTJk0AKFu2LImJiVy5cgUHBweMjHKnPvbt2xd3d/ePxvhXR7Jq1aocPXr0014AQWnKmBpx+2GY/DgmLgF9PR10tDTlaSlp6TSsVZUebZoCEB0Xz6qdgRjo6XLuxh9ULW+JubEhOtqadGzekGMXbxZxLQSh+Cmjr8OdF2/kxzFJ6ehrqaOtkfdP89E/XjC1nY1CWkpGNpNa18FAO3eLjF/P3afc/6feCoJQcGUMdLnz4rX8OCYpDX0tDYW2eP5RFHW/MpUv9NO3URUWH7lBfGomRrqaecoUhH9CJstRdgifnLjl+Zm4c+cOQ4cORSqV0r59e9q0aSO/O5aUlERKSgpxcXHEx8cD8PPPP7N161YsLS0ZPXo0RkZG8vzq6orTrNTUFL+wSP92Z0Mmk5Gdna1wDCikAWhq5v4Sze9un1B8NalXg9A/w3gWGQPAnqO/42BXRyFPzOsEvvNYTnJq7o2MX/YdoWPzhkgkEo6ev8Fa32BkMhmZWVkcOX8d+zrVirweglDcNKlkTmjka569TgZg7/UntKpWJk++xLRMnr9Jpq6ViUL6nutPWH36DwDiktPZfzOMjrWsCj9wQShhmlQuTWhELM/ikgDYe/URraqXVchTo4wR18JiiEtOB+Dk/ReUNdQVHUxBeA/RyVQyNTU1srOzuXLlCo0aNaJ///5UqFCBU6dOkZOTe1fD09OTgQMHMmDAADw9PQE4d+4cQ4cOpWPHjjx9+pTo6Og8ncf3adSoESEhIfIOq6+vL/b29gAYGRnx8OFDZDIZISEhHy1LVVVVHqdQPJkYlGLu2IFMXryBrhPm8vB5JFMG9+Duo2f0njwfgIplLRjavS3fuC2my/g5ZGVl88OgbgBMHtKd5NQ0enz/E31dF1Kz8ld806mV8iokCMWEsa4Wnp0b4LrvEt3XHuNhTCKT29hwN/INfX45Ic/3/E0yZnpaqKsq/ske2tSa6KQ0eq4/zvDtvzOmZU1qWxoXdTUEodgz1tPCs6s9rr7n6L7yEA+jE5jcrh53X7ymz5rDADSqZMHgZjUYtimEPmsOs+vyQ5b2F48KCZ+IWPhH+NRMTEywtLQkJCSE9PR0unTpAkDt2rWJiIjg0KFDhIeHs2TJEmQyGT179uTQoUOMHDmSqVOnoqWlRenSpeX5C6J69eqMHDkSFxcXsrKyqFWrlrzzOnnyZEaNGoWpqSkNGjTgzZs3HyyradOmLFmyhFKlStGhQ4f/djEEpWnRoBYtGtRSSDMopcse77fTqPs7fU1/p6/znKuvq8PCH74r9BgFoSRqUaU0LaqUVkgz0NbAd3hr+XFtS2MCxrTPc66upjrLejcp9BgF4UvQopolLapZKqQZ6GjiO/rtd5t+jarSr1HVog5NEIoliUw8sSwUsbRTvyk7BEH44jz+zk/ZIQjCF8n6so+yQxCEL5a6aSVlh1Ag6dcPfjzTv6Bl61wo5RaEmC4rCIIgCIIgCIIgfDJiuqwgCJy5/ZgV+0+TmZ1D1bJmzB7UET1txcUMQm78yZqAs0gkEgx0tfBw6UA5MyOmrNvP85h4eb7I2HgaVPuK5WN7FnEtBKH40XNoSGnXwUg01Em/H8YLt+VIk9PyzVuqbWOsvH/gnk2f3AQVFcp4jkK3UW0Akk9dJWq+mCkiCJ+STCZjupc3VStX4NsBvZQdjlBSKfn5ycJQ7EYyL126hIuLy38ux9HRkYiICMLDw5k2bdoniOzjrK2tP0k5bm5u+Pl9eOrbX5+1YsUKVqxY8Uk+Nz+hoaHybVWE4ul1UiqzNh9i8chu+M8ZjpWpIcv3n1bIk56ZxbTfAvEe1R3fmd/S0qYKC3YdB2DxyNw035nf4uHSgVI6Wrj3b6uMqghCsaJqrI/Vgkk8HzOfh21GkRkehcXUIfnm1ahgSWn373h3Y03D7g5oVizLo47jeNRpPDr2tdHv2KyIoheEku9x2HOGTnDn2Kmzyg5FKOmkOYXzUqJi18n81CIjIwkPD1d2GMXWo0ePiIuLU3YYwn9w4Y+n1CpfmvIWuatS9v66PsGX7ipsMC2VykAmIzktA4C0jEw01RUnQmRl5+CxKQjXPq0pbaxfdBUQhGJKr4UtabcfkhkWCcDrbYcw7NoqTz6JliZWSyYTNe9XxXRVFVR0tJBoqKOioY5EXR1ZRlZRhC4IX4Rd+wLp2aU97RzEKrKC8E8V2+myly9fZunSpaSnp5OYmIi7uztt2rTBzc0NPT097t69S3R0NGPHjqVnz57Ex8fj6upKVFQUlStXJiMj98uyl5cXEREReHp6Mn36dGbPns3Dhw+JjY3F2tqaJUuWcO7cORYuXMjBgweJiorCxcWFPXv20KNHD06dOoW6ujp//vknU6ZM4eDBgyxdupQLFy6QkJCAubk5S5cuxdTUVB77XyOL48ePB3JHVbds2UKZMmVYuHAhly9fJicnhx49ejBkyBBkMhk///wzp06dwtzcnJycHBo1agTAvn372LhxIxKJhFq1ajFz5kx0dXXzvWbbtm3D39+ftLQ01NXV8fb2plKlSjg6OmJjY8O9e/fYsWMHAQEB7Ny5E1VVVRwcHHB1dc33urZt2xYfHx9SU1NZs2YNo0ePLswfuVBIot8kKXQKLYxKkZyeSUp6pnzKrI6WBtO/ac/ghdsw1NUmRypl09SBCuXsPxeKmYEejvXFHpmCUBDqZUzJehkrP86KikW1lC4qetoKU2bLzhvL652HSb8fpnD+m70n0O/YnOoXNoOqCslnb5AUcrmowheEEm/65DEAnL98XcmRCCWemC77+di2bRteXl7s378fLy8vli9fLn8vKiqKHTt2sGbNGhYuXAiAj48PNWvWJCAggG+++YbY2Nw/7DNmzKB27drMmjWLGzduoK6uzu7duzl27BhJSUmcPn2a1q1bU69ePdauXYu7uzs//vgjFhYW2NjYcPZs7hSKoKAgnJ2defbsGU+ePGHXrl0cOXKEMmXKcPBgwVaM8vX1BWD//v3s3buXEydOcPXqVY4cOcIff/xBYGAgy5cv5/nz5wA8ePCAtWvXsnXrVgICAtDW1mblypX5lp2cnMzx48fZunUrgYGBtGrViu3bt8vfb9myJUeOHOHFixfs2LGDvXv3cvDgQe7evcudO3fyva76+vpMmDABR0dH0cEsxqRSGZJ80lVV3qY+fPGK9UHn8Js9lGMLxzLMqQlT1u1XGO3cdvwKw5yaFkHEglAySFQk5LfAuyzn7ZcN44FOyHJyiN9zLE8+8wn9yXmdwP1GA3nQbAiqBqUwGdq9UGMWBEEQhIIotiOZixYt4uTJkxw+fJhbt26RkpIif69Zs2ZIJBKqVatGfHw8kDvy6e3tDYCdnR3lypXLU6adnR2GhoZs376dJ0+eEBYWRmpqKgDTp0/HyckJW1tbOnXqBICzszNBQUE4ODgQHBzM1q1bsbCw4Mcff2TPnj08ffqUmzdv8tVXXxWoThcuXODevXtcvHgRgNTUVB48eMDjx49p164d6urqGBsb07JlSwCuXLmCg4MDRkZGAPTt2xd3d/d8y9bT08Pb25ugoCDCwsL4/fffqVGjhvz9unXrKpRZqlQpADZt2vTB6yoUf2WM9bnz/+l6ADHxSejraKGtqSFPO3/3CXUrW1HO7P//11rZstg3hPiUNIz0dLj/PJocqZSG1fK2K0EQ8pf54hXadd8+q69uYUJ2fBKy/09LBzDs2QYVLU0qB/ogUVdDRUuDyoE+PPtuNvrtm/LScy2yrGxkWdnE+51Av2Mz4jbsV0JtBEEQhH9NKkYyPxsDBgwgNDSU2rVrM2rUKIX3NDVzp/hJ3lkgQSJRvGOsqqqap8wTJ04wZcoUtLS06NGjB3Z2dvJzYmNjUVVV5cmTJ/Kptq1bt+bKlStcuXKFMmXKYGFhwZ07dxg6dChSqZT27dvTpk2bPHeq/x5LVlbuMzQ5OTm4urri7++Pv78/u3fvplevXnnyq6nl3huQ/u0/pEwmIzs7O9/r9fLlS/r27UtSUhItW7ake/fuCmX+dc3U1NQUrlt0dDSJiYnvva5C8dekZgVCn0TyLPo1AHvP3KRV3SoKeWp8VZprD58Tl5h7M+fkzYeUNTXASE8HgKt/PqeRdXnxf0MQ/oHkszfQqW+NRoXcDeCNv3Ei6fhFhTxPuv/Ao45jedx5As++m400PZPHnSeQHfOatLuP0e/0/2fF1FQp1caetBsPirgWgiAIwn8mkxbOS4mKZSczPj6esLAwJk6cSMuWLTlx4gQ5OR9eQalJkyb4+/sDuSui/jXlVFVVVd4xu3DhAh07dqRnz57o6+tz6dIlcnJyyMnJwd3dnenTp9OoUSP51FwNDQ1atGjBTz/9hLNz7manV65coVGjRvTv358KFSpw6tSpPLEZGRnx6NEjeSyvXr0CoHHjxvj6+pKVlUVKSgoDBgzg5s2bNGnShODgYDIzM0lISOD3338HoFGjRoSEhMhHFX19fbG3t8+3/rdv36Z8+fIMGTKEOnXqcPz48XyvWcOGDTl9+jQpKSlkZ2czefJk+XTZ/Lx7/YTiyVhfF8/BTriuP0D3Wb/w8MUrJvd25G7YS/rM3QhAo+rlGdzWnmHeO+gz9zd2nbzO0jE95GU8j3mDpYmBsqogCMVSTlwCEVOXU26VO1WOrkHTujxR8zagVacKlQN9Pnp+lNcvqOrrUvXYGqoE+pD9MpbY9fuKIHJBEARB+LBiOV3W0NCQpk2b0qlTJ9TU1GjcuDHp6enyqa35mTBhAm5ubnTq1IlKlSrJp8tWrlyZpKQkXF1dGTZsGFOmTCEoKAh1dXVsbW2JiIjgt99+w8TEhHbt2tG0aVM6d+5Mu3btqFevHl27duXgwYO0b98eACcnJ8aNG0eXLl0AqF27NhEREQqxODk5ceTIEZycnKhVqxY1a9YEoF+/fjx79ozu3buTnZ1Njx495J3G27dv07lzZ0xNTalcuTIA1atXZ+TIkbi4uJCVlUWtWrXw9PTMt/7NmjVj586dODk5IZPJsLOz4+HDh3ny1apVi4EDB9KvXz+kUilt27aladOm732u1MbGhpUrV7J48WKmTJny3usvfN5a1KlMizqVFdIMdLXxnfmt/Lifgy39HGzzPX/agHaFGp8glFTJp66SfOqqQlrO7Uc87jwhT96sFzHcq9P7bb74JCImii2kBKGwzZsxWdkhCCVdCZwuK5Hlt+qAIBSitFNis3BBKGqPv/vw3rqCIBQO68sfH5UWBKFwqJtWUnYIBZJ+bvvHM/0LWs2+KZRyC6JYjmQKxVxKorIjEIQvjviiKwjK8aBR3lFpQRCKRu0ngcoOoWBK4Eim6GQKgiAIgiAIgiAoiUz24bVliiPRyRQEgTN/PGNF0EUys3OoamnC7L4O6GlpKOQJCX3CmiNXkEgkGOho4tGnFeVMFRf7+WHjYcz0dXHv2aIowxeEEk0mkzHdy5uqlSvw7YBeyg5HEEoEPYeGlHYdjERDnfT7YbxwW440OS3fvKXaNsbK+wfu2fTJTVBRoYznKHQb1QZyn62Omi8eBRKEdxXZ6rI+Pj5cvXr14xk/Ijw8nGnTpn2CiCApKYmxY8d+krI+xtHRMc8CQP/GihUrWLFiRYE+y8/PDzc3t//8me/zKX8WgvK8Tk5j1q4QFg9pj7/7AKyM9VkeqLiNQnpmNtN2nMB7SAd8p/ShZa0KLNh/ViHPxpAb3HjysihDF4QS73HYc4ZOcOfYqbMfzywIQoGoGutjtWASz8fM52GbUWSGR2ExdUi+eTUqWFLa/Tt4Z4suw+4OaFYsy6OO43jUaTw69rXR79isiKIXSiSptHBeSlRkncwrV658dJuRgoiMjCQ8PPwTRAQJCQncu3fvk5T1JfqUPwtBeS48CKdWOXPKmxkC0LtZLYKvP1TYR1Uqk4EMktMzAUjLyEJT7e1es1ceveD8/ef0alqrSGMXhJJu175AenZpTzsHMTtAED4VvRa2pN1+SGZYJACvtx3CsGurPPkkWppYLZlM1LxfFdNVVVDR0UKioY6KhjoSdXVkGVlFEbogFBuF0smMiopi4MCB9OjRg169erFy5Uru3LnDjBkzePDgAS4uLowbN4727dtz7949rK2t5ee+O/p2/vx5nJ2d6dKlCyNHjiQ5ORkvLy/u3LmDp6cnly5dwsXFRX6um5sbfn5+RERE0KFDB/r378+3335LTk4O8+fPp3v37jg7O7Np0yYAvLy8iImJkY9mLl26lD59+tC+fXtcXFyIjY3l7t27NGvWjNevXxMfH4+DgwO3b9+mefPmxMbGArn7djZv3pysrCy2bdtG79696dy5M927d+fJkycK1+bvo4suLi5cunQJgPXr18tjXLhwofxL/q+//kq7du3o27cvoaGh8nNPnjxJ165d6dKlC2PGjJHHk5/g4GD69OmDs7MzHTp04Pr16/LPf/dnERAQgJOTE506dcLNzY2srCxWrFjBjBkzcHFxwdHRkTVr1siv318/C6H4io5PprShnvzYwkCP5PRMUt75g6mjqc70Xi0Z7ONH29mb2XX2DhM7NwEgJiGFRfvP8dPANqi8c6dXEIT/bvrkMXRq56DsMAShRFEvY0rWy7ffmbKiYlEtpYuKnrZCvrLzxvJ652HS74cppL/Ze4KchGSqX9iM9cUtZD6LJCnkclGELpRUMmnhvJSoUDqZe/fupVWrVvj5+TFhwgS0tbWpXbs2Xl5e8g6ltbU1R44coUaNGvmWkZmZyZQpU1iwYAEBAQFUq1aN/fv3M2PGDGrXrs2sWbM+GMPTp09ZtGgRGzduxNfXF4D9+/ezd+9eTpw4wdWrV5kxYwbm5uasWrWKZ8+e8eTJE3bt2sWRI0coU6YMBw8epFatWvTt25eFCxcyd+5c+vfvT506dejQoQOHDx8G4OjRo7Rt25aMjAyOHz/O1q1bCQwMpFWrVmzfXrAlic+cOcOdO3fYu3cvBw4cIDo6moMHD3L79m327dvH/v372bhxI1FRUQDExcXh4eHBqlWrCAgIwNbWljlz5uRbtlQqZdeuXaxdu5aDBw8ybNgw1q9fL3//r5+FsbEx8+fP57fffiMoKIicnBxOnz4NwIMHD9iwYQN79uxh/fr1JCYmFvhnIXzepDIZ+fUNVd9JfBgZx/qjV/H7sR/HZg9mWBtbpmw6QlZODu5bjzGlW1PM9HWLMGpBEARB+HckKhLy28FPlvP2S7nxQCdkOTnE7zmWJ5/5hP7kvE7gfqOBPGg2BFWDUpgM7V6oMQtCcVMoC/80adKE8ePHc+/ePb7++msGDhzIqVOnFPLY2Nh8sIwHDx5gYWEh74ROnpy7Ee5fo34fY2JigpWVFQAXLlzg3r17XLyY+5xZamoqDx48oHTp0vL85cuX58cff2TPnj08ffqUmzdv8tVXXwEwevRoevbsiZaWFosW5W587ezszPz58xk4cCCBgYF8//336Onp4e3tTVBQEGFhYfz+++/v7UT/3YULFwgNDaVHjx4ApKenY2lpSWxsLF9//TW6urlf4Dt06IBUKiU0NBQbGxt5Hfv27avQcXyXiooKq1atIiQkhKdPn3L58mVUVN7eX/jrZ3Hjxg1sbW3l1+Wvut67dw97e3s0NDQwMTHB0NCQpKSkAtVL+PyVMSzFnWcx8uOYhBT0tTXR1lSXp51/EE7diqXlC/30bV6bxf7nuf0smojXiSz2Pw9AXFIqUqmMzOxsZvUVoy+CIAjC5yfzxSu0676dRaduYUJ2fBKytAx5mmHPNqhoaVI50AeJuhoqWhpUDvTh2Xez0W/flJeea5FlZSPLyibe7wT6HZsRt2G/EmojlAhiC5OCadCgAUFBQZw6dYpDhw6xf3/eRqelpaVwLJPJkEgkZGdnA6Curo7knZGUpKQkUlJSFM6RSBTvRGVlvZ3e9275OTk5uLq60q5dOwBev36Nrq4ur169kue5c+cOkydPZsiQIbRv3x4VFRV52X99dkpKCvHx8RgbG2NjY0NCQgKhoaFER0dTv359Xr58iYuLCwMHDqRly5aYmprmeebzfTHn5OQwePBgvv32WwASExNRVVVl9+7dCvnV1NTIzMxE+rf/jDKZTH7t/i4lJYVevXrh7OyMnZ0d1tbWCiOsf10rNTU1hWv++vVr+b81NTXfWweheGtibYX3wfM8exVPeTND9p6/S6vaFRTy1LAyZdfZO8QlpWJSSoeTt59S1rgUtpUsOeIxSJ5vzeErxKeki9VlBUEQhM9W8tkblJk+FI0KlmSGRWL8jRNJxxUXvHvS/Qf5v9XLmlPl8Coed87d8zTt7mP0O7Ug5eJtUFOlVBt70m48KNI6CCWMkqe2FoZCmS67cOFCDh48SPfu3fHw8OCPP/5AVVX1vQv/GBkZ8fBh7kIjISEhAFSsWJG4uDgePXoE5D6XuHPnTlRVVeWdKSMjI8LDw8nIyCA+Pp5r167lW37jxo3x9fUlKyuLlJQUBgwYwM2bN1FTU5OXdeXKFRo1akT//v2pUKECp06dksfr6enJwIEDGTBggMLzh126dGHWrFl06tQJgNu3b1O+fHmGDBlCnTp1OH78eJ46GxkZ8fjxY2QyGeHh4Tx48EAeo7+/PykpKWRnZzN27FiOHDlCkyZNOHnyJElJSWRkZHDsWO60jbp163Lr1i35irW7d+/G3t4+3/qHhYUhkUgYNWoU9vb2HDt2LN+fRZ06dbh586a88/3TTz9x4sSJfMsEFH4WQvFlXEoHz34OuG46Svefd/Iw6jWTnZtyNzyGPotzp5o3qmrFYId6DFvlT59Fvuw6e4elQzsqOXJBEARB+Ody4hKImLqccqvcqXJ0DZrW5YmatwGtOlWoHOjz0fOjvH5BVV+XqsfWUCXQh+yXscSu31cEkQtC8VEoI5kuLi5MnjwZPz8/VFVVWbBgAY8fP2bWrFksWLAgT/7JkyczatQoTE1NadCgAW/evEFTU5NFixYxdepUsrKy+Oqrr1i4cCGZmZkkJSXh6urKokWL+Prrr+nUqRNly5alQYMG+cbTr18/nj17Rvfu3cnOzqZHjx7Y29uTlZWFpaUlLi4uLF68mHHjxtGlSxcAateuTUREBIcOHSI8PJwlS5Ygk8no2bMnhw4dwsnJCWdnZ5YvX87SpUsBaNasGTt37sTJyQmZTIadnR0PHz5UiKVp06bs27ePDh06ULFiRXnMjo6O3L9/nz59+pCTk0OLFi3o3r07EomEwYMH06tXL/T19bG0tATA1NSUOXPmMG7cOHk95s2bl2/9q1evTo0aNejYsSMSiYTmzZvn2yG3sLBg+vTpDB06FKlUSr169ejRowerV6/Ot9zKlSsr/CyE4qtFzfK0qFleIc1AVwvfKX3kx/2a16Zf89ofLGd0B7tCiU8QvnTzZkxWdgiCUKIkn7pK8inFrfVybj+Sj1a+K+tFDPfq9H6bLz6JiInie4/wCZXA6bISmZj3KBSxtKBlyg5BEL44avbOyg5BEL5IDxrl7bQIglA0aj8JVHYIBZJ2NP8Bnf9Ku92YQim3IAplJFMQPiRs0lFlhyAIXxwDM39lhyAIX6RTWWWVHYIgfLE+PP/qM1ICn8kUnUxBEARBEARBEARlKYHTZUUnUxAE9FrZYTZ5CBINdTIePOXltGVIk9Pyz9umCZaLJvNn/V4AqBjoUcZzHJo1KiFLSyd+3zHebA0oyvAFodjSbNoY/VHDkKirk/X4CfE/LUKWmqqQR3/8aLQcvkaWmLt1VPbzcN54KO6LbPSTJ9LYOBKWfHzREkEQ8irvWI8mbn1Q1VAn7t5zTrj+Stbf/g7WGdyW2i6tARkJz2I4OXUDaXGJyglYED5zn3R1WTc3N/z8/P7xeStWrGDFihUfzOPj48PVq1c/mOfvrK1z90BKTk6mR48edO7cmS1btrB8+fIPnjd9+nRu37793vcdHR3lq7r+G3/F9bk4ffo0LVq0kO9F+k+EhoaKRX+KOVVjfcr8/D0R4+bxpP0IMsOjMJ/ybb551ctbYuE2VGGrG4tpI5CmpvGk4yie9v4BvZYN0XNoVFThC0KxpWJogOH0qbyeNouY/oPJjnyJ/pgRefJp1KnFG4+5vBoynFdDhufpYOp90w+Nuh/ee1oQhPfTMi5Fa+/hBI9YzvZWriQ8j6Gpe1+FPGZ1KlB/pBP7unuys407CU+jsJ/SS0kRCyWOVFo4LyUqlC1MCsOVK1feuwXKx9y7dw8NDQ0CAwMZNGgQEydO/GD+efPmUadOnX/1WcXR4cOHGTduHN7e3v/43EePHhEXF1cIUQlFRbe5Lem3/yTrWSQA8TuC0Hd2yJNPoqVJWW9Xon/6RSFdq3YVEg6E5P4yy8om+dQVSnVoViSxC0JxptnIjqx7D8iJeAFAqp8/2u1aK2ZSV0e9alX0BvbDbOsGjOZ5omphLn9bo35dNBvbkXrgYFGGLgglylct6xBz6ykJYdEA3Nl6gmrdmirkeXU7jG0tp5CZlIaqpjq6pY1Jj09WRriCUCx8dLqsTCZj8eLFHD9+HFVVVfr27UuNGjVYunQp6enpJCYm4u7uTps2bRTO27Rpk3xfSwcHB1xdXXFzc6NRo0b06NEDyB3R+2ufyL9s27YNf39/0tLSUFdXx9vbm9DQUO7cucOMGTNYuXIlWlpazJ49m/j4eLS0tJg5cyY1a9YkIiICV1dXUlNTqVu3LgBxcXFMmzaN2NhYRo0aRbt27bh8+TI///wzjo6OODs7c/bsWdLS0liwYAG1a9fGxcWFcePGUb58eaZMmUJqaioqKirMmDGDevXqAbBq1Sru3btHWloaCxcupG7dujx79qzAcQFcuHBBPgpoYGCAt7c3xsbG/PLLL+zZswcjIyMqV65MmTJlGD9+vML18vPzk9cjODiYjRs3kp6eTmZmJj/99BO2tra4uLhgYGDAw4cPWbZsGa9evcLHx4fs7GysrKyYO3cux48f58SJE1y4cAEVFRUaNWqUbx1iY2Px8PAgKioKiUTC5MmTqV27Nj4+PqSmprJmzRpGjx79L/8bCsqkXtqMrJex8uOsqFhUS+mioqetMGW2zNzxvNl1iIwHTxXOT7/1AINujqRe/wOJhjql2jdDJvZPFYSPUrUwIyc6Rn6c8+oVKnp6SHR05FNmVU1NyLh2naT1G8h+EobugL4YL/Di1ZARqJiaYDBpPHE/TEW3WxdlVUMQij09SxOSI9/eME9++RpNfR3U9bQVpsxKs3Oo2L4BjguHkZOZxaXFe5URrlASlcCFfz46knn48GGuX79OQEAAe/bswc/Pj9WrV+Pl5cX+/fvx8vLKM/00NDSUHTt2sHfvXg4ePMjdu3e5c+fOR4NJTk7m+PHjbN26lcDAQFq1asX27dvp1q0btWvXxsvLC2tra3788UdcXV3Zv38/c+fO5fvvvwdg7ty59OjRA39/f2xtbQEwMTHBy8uL2rVrs3bt2jyfaWhoyN69e+nXrx/r1q1TeG/v3r20atUKPz8/JkyYoLC3ZJUqVThw4AAuLi5s2LAB4B/FBbB69Wpmz56Nn58fTZs25Y8//iA0NJS9e/fi5+fHb7/99sFpuwBSqZRdu3axdu1aDh48yLBhw1i/fr38fWtra44cOYKFhQXe3t5s2LCBAwcO0Lx5cxYvXkzv3r1xdHRkwoQJ9O7d+711mDdvHj179sTPz481a9bg4eGBiooKEyZMwNHRUXQwizMVCeSzk5Es5+0vPKMBnZDl5JCw91iefNHzfwUZVPRfQbnVM0k5dwNZluhkCsJHSVTybXvvTnHKeRnF6ynuZD8JAyBlx25Uy1qialUWI8+ZJPisQhr3uogCFoSSSaIiQcaH/w7+5emRa2yoO5rLS/xw3vYjvPP4iCD8ayVwuuxHRzKvXLlCx44d0dDQQENDA39/fzIyMjh58iSHDx/m1q1bpKSk5DnHwcGBUqVKAbmjmgWhp6eHt7c3QUFBhIWF8fvvv1OjRg2FPCkpKdy5cwd3d3d5WmpqKm/evOHy5cvyKZ/Ozs7MmDHjo5/ZokULAKpWrcrRo4pbazRp0oTx48dz7949vv76awYOHCh/76+R2ypVqnDkyJF/FVfr1q0ZN24cbdq0oXXr1jRr1oxff/2VVq1aoaenB0CnTp3Iysp6b/wqKiqsWrWKkJAQnj59yuXLl1FReXvvwMYm9zmdW7du8fLlSwYNGgTkdk4NDAwKfG3Pnz/PkydP8PHJXVQiOzub8PDwj15f4fOXFfkK7bpvnxNWszAlJz4JWVqGPM2gRxsk2ppUPLgCibo6Ei0NKh5cQfiwWaCqSvTCDUgTcqcNmYzqQ+b/p94KgvB+OdHRqNd6+zdO1cwMaWIisvR0eZpa5UqoV61M2uF3b/BIUDUxRs2yDAbjc/dAUzExBhUV0NAg4efFRVUFQSgRkl7EYVG/svxYr7QR6fHJZL/7d7CCBTpmBry88icA93afptX879Ay0BXTZgUhHx/tZKqpqSks8hEREcHEiROxt7fH3t6eJk2aMGXKlA+eEx0djba2NhKJBNn/79rm13F6+fIlLi4uDBw4kJYtW2Jqasq9e/cU8kilUnln9y9RUVEYGhoCyMuXSCQKna330dTUlOf/uwYNGhAUFMSpU6c4dOgQ+/fvZ+PGjQCoqqoqnPdv4hoyZAgODg6cPHmSRYsWERoaKu+Y/0VNTU3hWslkMiQSCdn/n46YkpJCr169cHZ2xs7ODmtra7Zv3y7Pr6WlBUBOTg62trby0dyMjIw8Nwc+VAepVMrmzZvl9YmJicHExCTPz0coflLOXsfCfRjq5S3JehaJUX8nkk5cVMgT1ut7+b/Vy5pTKWgNT53HA2D2w2BU9HSInrMGVRNDDPu058XEn4u0DoJQHGVcvor++NGoWpUlJ+IFOt26kP77OcVMMikGk8aTees2OS+j0OnRlezHT8i8dZvo7m8XJik1dDAqBgZidVlB+BfCz9ym+cwBGFSwICEsmtoDW/P06HWFPDrmhrRfOZZd7aeR/iaZat2b8fpBuOhgCp/Glzhd1s7OjqNHj5KVlUVaWhpDhw7l4cOHTJw4kZYtW3LixIk8C/I0bNiQ06dPk5KSQnZ2NpMnT+bOnTsYGhry6NEjAI4fP57ns27fvk358uUZMmQIderU4fjx4/KyVVVVycnJoVSpUlSoUEHeETp37hzffPMNAE2bNuXgwdzFD44ePUpGRkaez/gnFi5cyMGDB+nevTseHh788ccf7837b+Lq3bs3KSkpDBkyhCFDhvDHH3/QpEkTTp06RWJiIpmZmQqjq0ZGRjx8+BCZTEZISAgAYWFhSCQSRo0ahb29PceOHct3gaS6dety8+ZNnj7NfZ5u9erVLFy4sMB1aNy4MTt27AByF/vp0qULaWlpqKqqyju8QvGU8zqBSLelWK2YRqXDa9G0rkD0/F/Qql2Vigc/vOozQNw6X9RLm1IxaDXlt87n1bJtpN9+WASRC0LxJn0TT/y8hRjP88RsxybUK1ciwWcN6tWrYbYpd4Gt7CdhJCz1wXjRT5jt2IR2y+a8mTVXyZELQsmSFpfIicnr6bhuAgNCFmBSvRxn527H3KYifQ/PA+Dl5QdcXeFP9z3T6Xt4HlWdGxM0bJlyAxeEz9hHRzLbtm3LnTt36NGjB1KplMGDB/Ps2TM6deqEmpoajRs3Jj09ndR39vWqVasWAwcOpF+/fkilUtq2bUvTpk2xsrJi0qRJdOnShcaNG2NmZqbwWc2aNWPnzp04OTkhk8mws7Pj4cPcL6stWrRg1qxZLFiwgEWLFjF79mx+/fVX1NXVWbp0KRKJBA8PD1xdXdm9eze1a9dGV1f3P10cFxcXJk+ejJ+fH6qqqixYsOCD+f9pXD/88ANubm6oqamho6ODl5cXFSpUYNSoUQwYMABtbW35tFmAyZMnM2rUKExNTWnQoAFv3ryhevXq1KhRg44dOyKRSGjevLnCs6N/MTMz46effmLSpElIpVIsLCzy3XrkfXWYMWMGHh4edOmSu7jEwoUL0dPTw8bGhpUrV7J48eI8I9pC8ZFy+ipPTytuEZSe8FA+WvmurBcxPKjXU34sTUkjYoz40isI/0bGhUu8unBJIS3rfhKvhgyXH6cdOU7akbw3Zt+VtGFzocQnCF+KZydv8ezkLYW0mPin7O4wXX58Z+sJ7mw9UdShCV8CJT8/WRgkMll+qw4In4u/9g8dPz7vl/3iamPZgR/PJAjCJ/Xqo7cUBUEoLOrim5YgKMX3z7cpO4QCSdtfOI8ZaXd3K5RyC0J87RAEQRAEQSgkooMpCMJHlcBnMkUn8zNXkkYwBUEQBEEQBEH4mxI4XVZ0MgVBwKp1PRq49UFVU53X955zbvKvChtQA9QY0hbrQa1BJiPpWQznXDeQHpeIeiltmnsPx6ByGSQqKjza8zu3VwcqqSaCULxUcqxHy6l9UNNQJ+b+cw5P/ZXMv7W9mt2b0WiEEzIZZKdncGLWVqJuP0WiIqHNnMGUa5y7DcqTkzc5NW+nMqohCMVORcd6NPuxD6oa6sTef84x17xtr3r3ZjQc+f+2l5bBqdlbiQ59iqaBLq1/+hazmuXJSs3gjz2nubkp7z7SgvAl+/geH8K/cunSJVxcXAqc38fHh6tXcxdecXFx4dKlSx854/Pg6+tLYKDoUBRnmsalaL5kOCdHLMevpSvJz2JoMK2vQh6TOhWoNcqJoK6eHGjtTuLTKGyn9gLA1rUXKS9fc6C1OwFOHlgPao1ZgyrKqIogFCvaxqXouGg4/qOW86ujKwnPY/jaTbHtGVcqQ6tp/dkzeBGbnaZzYYU/3dZNBKBWj+YYVy7DxnZubOowjXL2NbB2aqSMqghCsaJtXIp2i4cTOHI5mx1y217zv7U9o0plaDm9P/sHLWJ7x+lcWuFP5/+3vVazBpKVks6W1lPZ1W0WFVrVpWLrekqoiVBiSKWF81Ii0cn8TFy5ciXfrUc+d9evXyczM1PZYQj/Qdmv6xB76ymJT6MBuL/lBJW7N1XIE3c7jH3Np5CVlIaqpjo6pY1Jf5O7N9glj61cmZO7vY22hSGqGupkJqYiCMKHVWxZh6jQp7wJy217N7adoGZXxbaXnZnF4R9/JSUmHoCo0Kfomhmioq6KRFUFdW1NVDXUUdVQQ1VdleyMvHtQC4KgqHzLOkTdekr8/9te6NYTVO+m2PZyMrM4NvVt24t+p+2Z16nAPb9zyKQypFk5PA25SVVxg0cQFIjpsoXozZs3DB06lJiYGGxsbJg1axa+vr74+/uTlpaGuro63t7ehIaGcufOHWbMmMHKlSsB2Lt3Lz///DOJiYlMnz4dR0dH3NzciI+P59mzZ7i6umJsbMy8efPIyMjAyMiIOXPmUL58eZ4+fYqHhwfx8fHo6Ogwffp0bGxscHNzQ1tbmz/++IPExER++OEH/P39uX//Pm3atMHNzY2MjAw8PT25du0a6urqjBkzBicnJxwdHXF2dubs2bOkpaWxYMECEhMTCQkJ4eLFi5iZmdGiRQslX3Hh39C1NCElMk5+nPLyNRr6OqjraStMmZVl5/BV+wY0WzyMnIwsbize+/a9HCktfUZTvpMdzw9fI/HxyyKtgyAUR6XKmJD0TttLevkaTX0dNPS05dP2EiNiSYyIledxmPkNj45fR5qVw509Z7B2smfM5RWoqKrw9PfbPD5xo8jrIQjFTSlLE5Jf/rO29/XMb3jy/7YXdeMxNXo0I/Lqn6hqqFGlox3S7OI3UCB8Rj6DzT4CAgJYs2YN2dnZDB48mG+++Ubh/bt37+Lh4UFWVhZlypRh0aJF6Ovrv7c8MZJZiCIiIpg5cyYHDx4kJSWFnTt3cvz4cbZu3UpgYCCtWrVi+/btdOvWjdq1a+Pl5YW1tTUApUqVYv/+/cyYMYNVq1bJyzQ0NCQ4OJjmzZvzww8/yMvv168fP/zwAwCurq64uLgQEBCAu7s7EydOlI82xsTEsHv3bkaMGIG7uzuenp4cOHAAX19fkpKS2Lp1K6mpqQQHB7Nx40ZWrVolP9fQ0JC9e/fSr18/1q1bR9OmTXF0dGTChAmig1mMSVQk+f5yk+XknWbx/Mg1dtYZzc0lfrTb/iNIJPL3zkxYw846o9E01KXu990LNWZBKAkkKhLy20Usv7anrq2J8+rxGJW34PCPvwLQbFIP0l4nsrLBGFY3noC2oR52wzsWetyCUOxJ8m970nzanpq2Jp3WjMewggXHpua2vTNeO5DJ4JtgL5x//Z7nZ+8gzcou9LCFEkzJ02Wjo6NZunQpO3bs4MCBA+zevZtHjx4p5Jk3bx4TJkzg4MGDVKxYkQ0bNnywTNHJLEQNGzakQoUKSCQSunTpwuXLl/H29iYoKAhvb29OnjxJamr+0wrbtGkDQJUqVXjz5o083cbGBoCwsDD09fXlxx07duT58+ckJSXx/Plz2rVrB0C9evUwMDDgyZMnALRs2RIAS0tLqlatiomJCXp6ehgaGpKQkMCVK1fo0qULKioqmJmZERQUhIaGBoC8I1m1alXi4+M/8dUSlCX5RRw6FkbyY53SRmS8SSY7LUOeVqqCBeZ21eTHD3edRtfKFE1DXSy/roO2hSEA2akZPPG/gEmdCkUVviAUW4mRcei90/ZKlTYiLT6ZrHfaHuSOunzj54EsR8qufvPI+P909KodGnLb9zTSrBwyk9K4s/d3vmpSs0jrIAjFUdLf2p5eaSPS4xX/7kFu2+u3P7ft7en7tu1p6Gnz+0872drWnX0DfgYk8qm3gvA5SUxMJCIiIs8rMTFRId/58+dp3LgxhoaG6Ojo0L59ew4fPqyQRyqVkpKSAkBaWhpaWlof/GzRySxEampvZyPLZDISExPp27cvSUlJtGzZku7du+d7Jw1AVVUVAMk7I0WA/AcqzefuhEwmIykpKd/0v573VFdXzze+d9Pe/cxnz57JRzI1NTXzjUko3iJP38bMtgr6FS0AqO7SmudHryvk0TE3pNWacWga6QFQqUcz4h+Ek/EmmYpd7Kn/Qw8AVDTUqNjZnpfn/ijaSghCMRR25jaW9atgVCG37dX7pjWP/tb2NHS16L97On8evkrA+FUKz1xG3wnDupM9ACpqqlRpa0vkDcU7z4Ig5PXszG1K16+C4f/bns3A1jz+W9tT19Wit+90Hh2+yqFxq8h5p+3ZDGxN08k9AdAx1adO/1bcP3Ch6CoglDyFNJK5efNmWrdunee1efNmhY+PiYnBzMxMfmxubk50tOKNEzc3N2bMmEHz5s05f/48/fr1+2CVxDOZhejatWtERkZSunRpDhw4QMuWLbl48SJDhgwhPT0dHx8fSpcuDeR2Kv/Jwj+VKlUiPj6e0NBQbGxsOHToEJaWllhaWmJlZcXRo0dp164dN2/eJDY2lqpVqxaoXDs7Ow4dOoSDgwOvX79m4MCBBAcHvzf/P41b+PykxyVy9of1OKyfgIq6GknPYjgzcS0mNhVptngYB9tNJ/ryA275+NNx73SkOVLSot5w4rtlAFyZs4MmP39LtxPzAXh2+Bp//HpEiTUShOIhNS6RYNf1dF0zAVUNNeKfxRD0/VpK16lI+wXD2Ow0nfqD26Jf1pSq7RtStX1D+bm7B8wnZM522s4ZzNATC5FJpTw7d5dLa8Vq34LwMWlxiRydsp7Oa3P/7iU8j+HwpLVY2FSkzYJhbO84nXpD2lKqrCmV2zek8jttb1//+VxedZAOy0bhcmw+EomE8977iA59osQaCUL+Bg8eTPfueR9h+vuzlFKpVGEQSSaTKRynp6czffp0Nm3ahI2NDRs3buTHH39k/fr17/1s0cksRFWqVGHatGm8evWKxo0b069fP86fP4+TkxMymQw7OzsePnwI5E5FnTVrFgsWLChQ2RoaGixdupS5c+eSlpaGgYEBS5cuBWDRokXMnj2bFStWoK6uzooVK+RTXj9mwIABeHl54ezsDMDMmTPR09N7b/6mTZuyZMkSSpUqRYcOHQr0GcLnJyLkFhEhtxTS4uKfcrDddPnxgy0neLDlRJ5zMxNTOT1mVZ50QRA+7snJWzw5qdj2om4/ZbNTbtu7tDqAS6sD3nt+wATR9gTh3wg7eYuwv7W96NCnbO+Y2/aurArgyqoPtL3hywozPOFLIyuc7Ub09fU/uDjPX0qXLi3fShHg1atXmJuby4///PNPNDU15Y/p9e3bl+XLl3+wTInsffM1BaGQDCzfQ9khCMIXJx2xKIUgCILwZdn77KCyQyiQtC3uhVKu9qD5BcoXHR1N//792bt3L9ra2vTr14+5c+fKO5UJCQl07NiRbdu2UalSJQICAvD19WXr1q3vLVOMZAqCIAiCIAiCIHyhLCws+P777xk0aBBZWVn06tULGxsbhg8fzoQJE6hTpw7z589n0qRJyGQyTExM+Omnnz5YphjJFIqcGMn8/NRzbECfqd+grqHO8/vP+HXqKtLe2SPzXSO9xxP+4DmH1vvL09q4dKBVvzaoa2kQdvsxv0xdRXamGDn7nIiRzM+TrWNDvpk6CDUNNZ7ff8bqqT7vbXvjvCfx/EEYB9cfyPOe6zp3Xke/ZoPHukKOWBBKhv/S9nRK6TB64XjKVrZCoiLh9N4QDqz1K8LohYIqNiOZm90KpVztwT8XSrkFIVaXLQKXLl3CxcWlyM8tCj4+PgpzuIXip5SxPsMXjWP5qEW4Oo4n5nk0fd3y/p+zrFIW952e2Dk1UUhv2MGetkOcmD9gNm5tJqKupUHHoV2KKnxBKLb0jfUZu2gCi0bNZ6LjGKKfR/GN2+A8+cpWsWLWTi8aOzXNt5yuI3tQ3U5sXSIIBfVf216/yd8Q9zKOH9qNx63LZNoN7Eg1W+uiCl8QigXRyRT+kytXrojVZYu5Oi3r8TT0EdFhLwE4se0wTbu2yJOvzaCOnNp1nMtB5xXSm/doRfAvB0lJSEYmk7Fx2jrO+p0uktgFoTir27I+j0IfEvX/tndkWzAtun6dJ1+HQZ04sesoF4LO5XmvVuPa1Gtly7Hth/O8JwhC/v5r2/tt9i9smfcbAEbmxqhrqpOalP++54JQIIW0hYkyiWcyi8ibN28YOnQoMTEx2NjYMGvWLC5evIiPjw/Z2dlYWVkxd+5cjIyMOHv2LPPnz0dTU5OKFSvKy3BxccHAwICHDx+ybNkyoqKiWLZsGVKplHLlyjFnzhxMTU25efMm8+bNIyMjAyMjI+bMmUP58uVxcXGhZs2aXLt2jYyMDKZMmcKWLVt4/PgxQ4YMYciQIcTHxzN9+nSePHmChoYGbm5uNGnShObNm9O+fXuuXbuGqqoqy5Yt49q1a9y5c4cZM2awcuVKrK3FXbziyKSMCXGRsfLj1y/j0NHXRVtPW2Hq0BaPXwGo06KuwvllKlny5NYjpm6eiaGFEQ+u3GPXT1uKJnhBKMZMypgqtL24l7Ho5tP2/poCW7dFfYXzjcyN+XbWcLwGz6bdALG6tyAU1H9tewDSHCkTlv1A445NuXzkIpGPXxR+4IJQjIiRzCISERHBzJkzOXjwICkpKaxfvx5vb282bNjAgQMHaN68OYsXLyYzMxM3Nzd8fHzw8/NDS0tLoRxra2uOHDmCubk5Hh4erFq1ioCAAGxtbZkzZw6ZmZn88MMP8s/q168fP/zwg/x8mUzG3r17ad++PV5eXqxcuZLt27ezalXuMvjLly/nq6++Ijg4mIULF7Js2TIgdynjJk2acODAAezs7Ni+fTvdunWjdu3aeHl5iQ5mMSZRUSG/R7OlOQW7A6aqpkrtFjasGLuYmV2momegR2/Xbz51mIJQ4qioqJDfqggFaXuqaqpMWjGFTXM3EB/zphCiE4SS67+0vXf5TFrCd/UHomeoR6+JfT9RdMIXqQSOZIpOZhFp2LAhFSpUQCKR0KVLFzZv3szLly8ZNGgQXbt2Zfv27Tx79owHDx5gbm5O5cqVAfJsoPrXUsKhoaHY2NhgZWUF5O5Xc/HiRcLCwtDX15fn69ixI8+fPycpKQmAli1bAmBpaUndunXR1tambNmyJCYmArnTX7t27Qrkdmh3794t/+wWLXKnUFatWpWEhIRCuU5C0YuLfIWRhbH82Ki0CcnxSWSkZRTo/DfRb7hy+BJpyWnkZGVz7sAZqthWK6xwBaHEeBX5CuN32p5xaROSCtj2KttUweIrCwbP+I5Fh5bR9psONO3cnFELxhVmyIJQIvyXtge5022NzHPPT09N5+zBM1SqXblQYhW+EDJp4byUSHQyi4ia2tuZyX+NGtna2uLv74+/vz979+7Fx8cHiUSiMKqkqqqqUM5fI5vSv92dkMlkZGdn50n/672/nptUV1fPN6Z30yQSifz48ePH8jI1NTUB8sQoFG+3z9yiSv1qWFQoA0Drb9px/eiVAp9/OfgCjTs1RV1TA4AG7RrxJPRRocQqCCXJrTM3qFrfmtL/b3vtvunIlaOXCnTun9cfMKrJUFydJuHqNIlj2w9zPvAsa39cWZghC0KJ8F/aHkDTzs3pPakfAGoaajTt3Jw750MLJVZBKK5EJ7OIXLt2jcjISKRSKQcOHGDw4MHcvHmTp0+fArB69WoWLlyItbU1sbGx3L9/H4CgoKB8y6tbty63bt0iIiICgN27d2Nvb0+lSpWIj48nNDT3l92hQ4ewtLTE0NCwQHE2bNhQ/pmPHz9m+PDhCp3Ov1NVVRUL/xRziXEJrHddyYQ1riw44UO56uXZ7rWJinUqM++Q90fPP77lMHfOhuIVtIhFISvQ0tFiz8LtRRC5IBRviXEJrHJdzpQ1biw7sYry1cuzxes3KtepwqJDy5QdniCUWP+17W32+g3dUjosObqChYFLeXL7MUG/BRR+4EKJJZPKCuWlTGKfzCJw6dIlli1bhqamJq9evaJx48ZMmzaN06dPs3z5cqRSKRYWFixatAgjIyOuXLnCnDlzUFNTo2bNmjx//pytW7fi4uLCuHHjsLe3ByAkJAQfHx+ysrKwtLRk3rx5mJubc+PGDX766SfS0tIwMDBgzpw5VK5cWeF8Pz8/Ll++zM8/5+6fY21tzYMHD0hMTGTGjBmEhYWhpqbGtGnTaNiwofx9QOHcDRs2sGvXLhYsWICtrW2BrofYJ1MQip7YJ1MQBEH40hSXfTJT139fKOXqjFhaKOUWhOhkCkVOdDIFoeiJTqYgCILwpSk2ncy1EwulXJ1Rywul3IIQW5gIRS4DMb1WEIpauky0O0FQBlXe/8iJIAgCoPRFegqDeCZTEARBEARBEARB+GTESKYgCNg6NmDA1EGoa6jz7H4Ya6auUNiQ+l1jvSfy/MEzAtYfyPPelHVuvIl+zQaP9YUcsSCUDA0d7Rj842DUNdQJux/Gctdl72173y/5nrD7z9i/3g8ADU0NRnuNplq9aiCR8OeNB6yZsYbMjMyirIIgFEsNHBsy6J22t8J1+Xvb3sQl3/PsfhgH1u8HctveSK/RVK1XDYkE/rzxJ+tE2xP+CyUv0lMYxEimAICjo6N8pdp/wtfXl8DAwEKISCgq+sb6jFk0gcWjfmai4xiin0fxjdugPPnKVrFi1s65NHZqmm85ziO7U8OuZmGHKwglhr6xPpMWT2L+yJ8Y5TCSqOdRDHH7Nk8+qyrlmLfzJ5o5NVdI7zO+L6pqqoxrN47x7cahoaVJ77F9iip8QSi29I31mbB4Ej+PnM8Yh1FEPY9ikNuQPPmsqlgxd+c8mjo1U0jvPb4PqmqqTGw3jontxqOhpUGvsb2LKHpBKB5EJ1P4T65fv05mprhzV5zZtKzP49BHRIW9BODotsO06Pp1nnwdBjlxYtcxLgady/Nezca1qd/KlqPbDxd6vIJQUti2tOXhrYdEhkUCcGhrEK26tcqTr/OgThzddYSzQWcV0u9eusMun13IZDKkUilP7j7G3MqsKEIXhGKtfktbHt16yMv/t73DWw/xdT5tz2lQZ47tOsq5PG3vLr4Kbe8JZlbmRRG6UFJJpYXzUiIxXbaYu3TpEqtXr0ZNTY2IiAhsbGyYN28eAQEBbNy4EYlEQq1atZg5cya6urqcPHmSZcuWIZVKKVeuHHPmzMHU1FReXkZGBp6enly7dg11dXXGjBmDk5MTjo6OODs7c/bsWdLS0liwYAGJiYmEhIRw8eJFzMzMaNGihRKvhPBvmZYxJTYyVn4c9zIWHX1dtPW0FaYO/TUFtm6LegrnG5kb8+2sYcwb7EnbAe2LJGZBKAlMLc2IfflKfhz7MhbdfNreWo+1QO4X43fd+P2G/N9mZc1wHtqVlW4rCjlqQSj+TC1NiX359u/e+9re+v+3vXot6yucfzNP23NmldvKQo5aKNGU3CEsDGIkswS4ceMG06dP5/Dhw2RkZLB+/XrWrl3L1q1bCQgIQFtbm5UrVxIXF4eHhwerVq0iICAAW1tb5syZo1DW1q1bSU1NJTg4mI0bN7Jq1Sr5SKWhoSF79+6lX79+rFu3jqZNm+Lo6MiECRNEB7MYk6hIIJ+djKQ5H/+Fp6qmyqQVk9k8dwPxMW8KIzxBKLEkEgn57SJWkLb3rsp1qrBg70ICNwVy5cSVTxWeIJRYn67tVWb+3gUEbQrkqmh7gqBAdDJLADs7OypVqoREIqFr166sXr0aBwcHjIyMAOjbty8XL14kNDQUGxsbrKysFNLfdeXKFbp06YKKigpmZmYEBQWhoaEBIO9IVq1alfj4+KKroFCoYiNfYWRhLD82Lm1CcnwSGWkZHz23sk0VzL8qzeAZ37Ho0FLafdOBpp2bM2rBuMIMWRBKhFeRrzC2MJEfm5Q2IamAbe8vLbu0xGu7F5t/3sSeVb6FEaYglDi5be/t371/0/ZadGmJ53Yvtvy8mb2r9hRGmMKXRCYrnJcSiU5mCaCqqir/91/PB7xLJpORnZ393vR3qampIZG83dPr2bNn8pFMTU1NAIX3heLv1pmbVK1vTekKZQBo900Hrhy9XKBz/7z+gNFNhuLq9D2uTt9zdPthzgeeZe2PYtqQIHzMjTPXsa5vjWUFSwCcBjpx8ejFj5z1VqM2jRjhOZKZA2dw2v90YYUpCCXOzTM3sK5vTZn/t70OA524/A/anl2bRgzzHMHsgTM5I9qeIORLdDJLgGvXrhEdHY1UKuXAgQO4u7sTEhIiH2309fXF3t6eunXrcuvWLfkqsrt378be3l6hLDs7Ow4dOoRMJiMuLo6BAwd+cGEfVVVVcnLEJu/FWWJcAqtdfZi85keWnljJV9XLs8XrNyrVqcKiQ0uVHZ4glFgJcQksn7IM97XurDmxlvLVK7Bh7q9UsamCT/DHn638bvpQJBIJExZMxCd4BT7BKxg1d3QRRC4IxVtCXAI+U5bz41p3Vp5YQ/nq5flt7gaq2FRhabDPR8//dvp3SCQSxi6YwNJgH5YG+zBy7qgiiFwosUrgwj8SWX6T0oVi49KlS8yePRtzc3Oio6Np1qwZ06ZNw8/Pjy1btpCVlUWtWrXw9PRET0+PkJAQfHx8yMrKwtLSknnz5mFubo6joyNbtmzB3NwcLy8vbtzIfah9/PjxtGvXTv6+lZUVly5dYuXKlWzdupWgoCCWLFmCq6srHTp0KFDMvct3LcxLIghCPtJk2R/PJAjCJ6eKmP0jCMri/7x4bLOXumR4oZSr88MvhVJuQYhOZjH3boevuBCdTEEoeqKTKQjKITqZgqA8xaaTuXhYoZSrM+XXQim3IMQWJkKRy0Hc1xCEovaViq6yQxCEL9KF9BfKDkEQhM+drORtYSI6mcWcvb19nucqBUEQBEEQBEEQlEV0MgVBoIFjQ76ZOgh1DTWe3X/Gqqk+ChtSv2u89ySePwjDf/0BAHRK6TB24XjKVrZCoiLh1N4Q9q/1K8LoBaHkqO1Qn65TB6Cmoc6L+8/Y9uNa0v/WFht1a0GbkV2QySArLQPf2Rt5fvuJkiIWhOKjeZsmjJ82Cg0NDR7ee4Tn9/NJSU4tUB69UrrMWupOhSrlUVGREOAbzKaV26lUrQI/rZ4tP19FVYWqNSoz+btphBwSK88KBSQtebP8xOqyX7CuXXOfjQwNDWXRokX/qgxfX18CA4vHfHchf/rG+oxbNIFFo+Yz3nEM0c+jcHEbnCdf2SpWeO70oolTU4X0/pO/Ie5lHJPajWdql8m0H9iRarbWRRW+IJQYesalcFk0hvWjvfFsPYnY8Bi6/ThAIY95pTJ0nzaQlYN+Yr7TVIJX+DFi7RQlRSwIxYeRiSGey6bjOnQ63Zv3J+JZJBNmjC5wnjE/Dic68hW9W7nwTYdh9B7cHZsGtXjyZxj92gyRvy6evkyw31HRwRS+eKKT+QXz9/cH4NGjR8TFxf2rMq5fv/7BLU6Ez1+9lvV5FPqQl2EvATi8LZgWXb/Ok6/joE4c33WU80HnFNI3zP6FTfN+A8DI3Bh1TXVSk1LznC8IwofVaFGXZ6GPeRUWBcCZbUex69pCIU92Zjbbf1xL4qt4AJ7dfoy+mSGq6qp/L04QhHc0/roRd2/e4/nT3G3c9mzeT8ce7QqcZ+GMZSz1zN0D2szcBHUNdZKTUhTOr29flzadHZg39d/duBe+XDKptFBeyiSmy5Ygf19p1s3NjUaNGrFlyxaqVq3KvXv3MDExYfny5RgaGmJtbc2VK1fw8fEhNTWVNWvW8N133+Hp6cm1a9dQV1dnzJgxODk54ejoiLOzM2fPniUtLY0FCxaQmJhISEgIFy9exMzMjBYtWnwkQuFzZFLGlNjIWPlx3MtYdPV10dbTVpgy+6vHOgDqtqifpwxpjpSJy36gScemXDpykcjHYqELQfinjCxNePPy7Q2/+JdxaOvroKWnLZ8y+zriFa8jXsnz9JoxmNDjV8nJEvsVC8KHlLY0J/pFjPw4JvIVpfT10NXTkU+Z/VienJwcvFZ60KZzK04GnyHs0XOFz5jkMZaV89flmYIrCB8lpssKxdH9+/f59ttvCQwMRF9fn4CAAPl7+vr6TJgwAUdHR0aPHs3WrVtJTU0lODiYjRs3smrVKvlIpaGhIXv37qVfv36sW7eOpk2b4ujoyIQJE0QHsxhTUVEhv42MpDn/7A7Y8klLGFJ/IHqGevSe2PcTRScIXw6JRIX8dhXLry1qaGsybNX3mFWwYLvb2qIITxCKNYmKCrJ8VrfPeWe0pyB5Zoybg0PNTugb6jNi8rfy9LoNa2NkYkiw37FPHLkgFE+ik/kFMDExoWbNmgBUrVqVhISE9+a9cuUKXbp0QUVFBTMzM4KCgtDQ0ACQdySrVq1KfHx8occtFI1Xka8wtjCWH5uUNiEpPomMtIwCnV+vZX2MzHPPT09N5+zBM1SqXblQYhWEkuxNZCwGFkbyY8PSxqTEJ5P5t7ZoZGnCFL+5SKVSlvXzJC1RjJoIwsdEvYjCzMJUfmxexpSEN4mkp6YXKE+TVo3k76WlpnH4wHGq16kmz9uua2sC9wTne6NIED5KJi2clxKJTmYJIpFIFH65ZWVlAaCpqfnePH+npqaGRPJ24+hnz57JRzL/Kufd94Xi79aZG1Srb02ZCmUAaPdNR64cvVTg85t2bk7fSf0AUNNQo2nn5tw+H1oosQpCSfbH77eoWK8qZhVKA9Dim7aEHruikEdTV4vvd83m5uHL/DZ+OVkZWcoIVRCKnQunL1OnQS2+qmgFQK9B3Tl15PcC52nn7CgfuVTXUKedsyNXzl6Xn9ugSX0u/36tKKoiCMWC6GSWIEZGRoSHh5ORkUF8fDzXrhXsl52qqirZ2dkA2NnZcejQIWQyGXFxcQwcOPCDC/uoqqqSkyOeBSrOEuISWOm6HNc1bvicWEX56uXZ5PUbletUwfvQso+ev8nrN3RK6bDs6AoWBy7lye3HBP0W8NHzBEFQlByXyFbXNQxf8wMex5dgaf0V+7y28FWdSrgfWghAq8EdMC5rRt32jXA/tFD+0jXUU3L0gvB5exMbz+xJP7HoVy/2ndlOlRqVWDJ7BTXrVmfX8U0fzAPgPXslpfT12HNqKzuO/sa9Ww/Y8YuvvPyvKlkRGf5SGVUTSgKprHBeSiSRiXH9EsXDw4Pz589TtmxZTE1NadasGStXriQkJASAFStyf1mOHz8ea2trHjx4wNOnTxkxYgTt27dnwoQJeHl5cePGDXm+du3a4ejoyJYtW7CyslJYYCgoKIglS5bg6upKhw4dChRjj/LOhVN5QRDeq7RES9khCMIX6UK6WAhNEJTlRtS5j2f6DKTM7l8o5erO3lko5RaE6GQKRa7LV52VHYIgfHEMJBrKDkEQvlh/Zv27bcIEQfhvLkcWj/1KS2InU2xhIgiCIAiCUEhEB1MQhI8SW5gIgiAIgiAIgiAIwvuJkUxBEGjo2JBBPw5GXUOdsPth+LguJ+3/m7//3aQl3/Psfhj71+8HQENTg1Feo6lWrxoSCTy48SdrZ6whM+P9C0YJgpCrnmMD+kz9BnUNdZ7ff8avU1e9t+2N9B5P+IPnHFrvL09r49KBVv3aoK6lQdjtx/wydRXZmdlFFb4gFCvNWjdmjPsINDTVefTHE7wmLyAlObXAeY7e8Sfm5St53q2rd3Fk/3H0DUsxxWsiFatVQFNLg43LtxG872iR1k0o5pS83UhhECOZXygXFxcuXSr4NhUnTpxg+fLlADg6OhIREUF4eDjTpk0rrBCFIqJvrM/ExZOYP3I+ox1GEfU8iiFuQ/Lks6pihdfOeTRzaqaQ3md8H1TVVBnfbhzj241HQ0uD3mN7F1H0glB8lTLWZ/iicSwftQhXx/HEPI+mr5tLnnyWVcrivtMTO6cmCukNO9jTdogT8wfMxq3NRNS1NOg4tEtRhS8IxYqhsQEzl7rhNnwmvVu48OJ5JGOnjSxwnq8qlyPxTRID2w6Tv47sPw6AxzJ3Yl6+wqXdMMb1nczkuRMwL2NW5HUUhM+J6GQKBdK6dWsmTpyokBYZGUl4eLiSIhI+lfotbXl46yEvwyIBCN56iK+7tcqTr9OgzhzbdZRzQWcV0u9eustun13IZDKkUilP7j7BzMq8KEIXhGKtTst6PA19RHRY7rYHJ7YdpmnXFnnytRnUkVO7jnM56LxCevMerQj+5SApCcnIZDI2TlvHWb/isciFIBQ1+6/t+OPmfcKf5q72u2+zPx16tClwHpuGtcmRSlm/fwXbj//G0O8Ho6Kigr5hKRq1aMgvSzYBEPPyFd91HkVCfGLRVU4o/krgFiaik1kMuLq64uv7di8mFxcXbt26xbfffkv37t3p378/f/zxBwB//vknLi4u9OzZEwcHB3buzF1VasWKFQwdOhQnJyd27NgBgK+vL926daNbt27yUc0VK1bItzmBt6OWfn5+uLm5KcTl5eXFnTt38PT0LNT6C4XLzNKU2Jex8uPYl7Ho6uuiraetkG+dx1pO++f9Anvj9xtEPs3toJqVNcN5qHOejqggCHmZlDEhLvJt23v9Mg6dfNreFo9fueD/+99Pp0wlS/RNDJi6eSY/HV5Cj+/7kpqYUuhxC0JxZFHWnJjIGPlxzMtX6OnroaunU6A8qqqqXPn9GhMGuDKyxwQat7Kjz3c9sKpQlriYOL4Z0Ydf/FeyOXgd1nWqkZGWUaT1E4o3mVRaKC9lEs9kFgM9e/ZkxYoV9OnThxcvXvD69Wvmz5+Ph4cHNWvW5NGjR4wdO5YjR46wZ88exowZQ5MmTQgPD8fZ2Zn+/XOXRc7MzOTQoUMABAcHo6Ojw4EDB7h//z4jRozg+PHj/yiuGTNmsHLlSmbNmvXJ6ywUHYlEQn47GUlz/tkvp8p1KjN9/XSCNgVy5cSVTxWeIJRYEhWV/9T2VNVUqd3ChqXDfiYzI4tR3uPp7foN2+b89qlDFYRiT0WiQn6b9uW8094+lMd/R+DbhDTYsc6XvkN78set+5Qtb0lycirDu47DqkJZ1u9fQfiTCO7f/rMQaiIIxYMYySwG7O3tiYmJISIiggMHDtCxY0fu3LmDu7s7Xbt2ZfLkyaSmpvLmzRvc3NzIyMhg3bp1LFu2jNTUtw+029jYKJTbq1cvAKpXr46JiQlPnjwp0noJn4dXka8wtjCWH5uUNiEpPukf3YVt0aUlc7d7sfnnzexZtacwwhSEEicu8hVG77Q9o9ImJP+Dtvcm+g1XDl8iLTmNnKxszh04QxXbaoUVriAUa1EvojEtbSI/NittSsKbRNLT0guUp2PPdlSpUUn+nkQiITs7m9io3NkIgbtyb+JHhL3g1uXb1Kpfo7CrJJQkYrqsoAwSiYRu3boRFBREcHAw3bt3R0NDA39/f/lrz549GBoaMmnSJI4dO0blypWZNGmSQjlaWloKx6qqqvJ/S6VS1NTU8oxqZWVlFWrdBOW7ceYG1vWtKVPBEoCOA524dPRigc+3a9OIEZ4j8Bg4M9/ptIIg5O/2mVtUqV8NiwplAGj9TTuuHy34LIDLwRdo3Kkp6poaADRo14gnoY8KJVZBKO4unb5CbdualKtYFoAeg5w5c/RcgfNUrl6Rka7foaKigqaWBr2/7c4x/5NEhkdxL/QBnfp0AMDY1Ig6DWvxx60HRVg7Qfj8iOmyxUSPHj0YMGAAVapUoWzZslSoUAF/f3+6du3KuXPn8PDw4Pjx45w7d47g4GAsLCzYvn07ADk5OfmWGRAQQO3atbl9+zYpKSmUL18eIyMj+fOZoaGhvHr1Kt9zIbeTmp0tlsov7hLiElg+ZTnua91RU1cj6vlLlkxaQhWbKoxfMIGJHSd88Pzvpn+HRCJh/IK3+e5d/YO1M9cWduiCUKwlxiWw3nUlE9a4oqahRsyzKNZ+70PFOpUZtmAM050mf/D841sOo2egh1fQIlRUVAi784QdXpuKJnhBKGbexMUz9/uf+Xn9HNQ01HkR9oLZE3+iho01071dGdh22HvzAPyyZBOu8yaxI2QjampqnAg8JZ9CO3XoDKb+9D09B3VFoqLChqWbuXfrvjKrKxQ3Sh51LAwSWX4PhAifpQEDBjBw4ECcnJx4/Pgxs2fPJj4+HnV1dWbPno2NjQ0bN25k27ZtaGpqUr16dW7evMnGjRs5ePAgAOPHjwdyFw+qUKECt2/fRkVFhVmzZlG3bl3evHnDxIkTiY2NpVatWjx+/BgfHx8uX77M5cuX+fnnn3F0dGTLli3o6uri4uJCjRo1WLRoUYHr0eWrzoVyfQRBeD8DiYayQxCEL9KfWXHKDkEQvliXI4vHDKvkKV0LpVy9xf4fz1RIRCezGJDJZMTExODi4kJgYCAaGsX7y2LnrzopOwRB+OKky8SsA0FQBlXxZJIgKM2R8GBlh1AgJbGTKX7zFQNHjhyha9eu/PDDD8W+gykIgiAIgiAIwjvEwj+CMnTo0IGLFy/SoUMHZYcilFANHe1YcWQla0+uw22Ne559+t71/ZLv6T6ih0Karr4uK46spIpNlcIOVRCKPXvHRqw/uoaNp35l5prp6LyzT9/H8pQyLMWM1dPYeOpX1hxaSbchznnOLV3OAr/be6lmU7XQ6yIIJUUjRzvWHF3Nr6d+Yfqaafm2y79MWTKZXiN7FmF0glD8iE5mMeTo6EhERATh4eFMmzbtk5a9c+dOdu7cmSfdz88PNzc34uLi6Nq1q/zl6OhI/fr1P2kMQtHSN9Zn0uJJzB/5E6McRhL1PIohbt/myWdVpRzzdv5EM6fmCukNHRri7b8Eq0pWRRWyIBRbBsYGTPGejOeIuXzbahgvn0cxzP27AucZ7TGStJQ0hjqOYHzXSdg52GHf2l5+rrqmOm7Lf0RdXazrJwgFZWBswGTvH5g7wothrYYT9TyK79zz/h0sV6UcC3bNp0Wn5vmUIgj/nkwqK5SXMolOZjEWGRlJeHj4Jy2zf//+9O/f/73vm5iYyLdN2b9/P2XLlmXOnDmfNAahaNm2tOXhrYdEhkUCcGhrEK26tcqTr/OgThzddYSzQWcV0rt864z3xMW8jnldFOEKQrHWoKUtf956wIv/t7eArYG07uZY4DxVbapyfN8JpFIp2VnZXAq5TMt3vvBO8BrH0T3HSHidUEQ1EoTiz7alLQ9u/Sn/Oxi4NRDHbg558jkP7szhXUc4E/R7UYcolHQlcLqsuNVZhC5dusTq1atRU1MjIiICGxsb5s2bR0BAABs3bkQikVCrVi1mzpyJrq4uJ0+eZNmyZUilUsqVK8ecOXMwNTWVl+fl5UVERASenp4kJydjZ2dHnz59gNzVY6dMmcLixYupXr06V69eJSMjg2nTptG8eXNiY2Px8PAgKioKiUTC5MmTadq0KStWrAByV6E9cOAAa9asQU9Pj7Jly6Kjozh1ZN++fWhra9OlS5eiu4jCJ2dqaUbsy7db1cS+jEVXXxdtPW3SktPk6Ws9crckqd/SVuH8WYM8iiZQQSgBzC3NiImMlR+/evkKXX1ddPR0SE1O/Wie+zfu06Zna+5cvYu6hjotOjYn5/9bSXXs1wE1NTUO7QxmwPh+RVsxQSjGzCxNiY18+3fw1f//Dr7bLgFWzVwD5HZKBUH4MDGSWcRu3LjB9OnTOXz4MBkZGaxfv561a9eydetWAgIC0NbWZuXKlcTFxeHh4cGqVasICAjA1tY2z4jhjBkzqF27NrNmzaJnz574++euIPXixQtev35N3bp1AUhOTmb//v14e3vj5uZGZmYm8+bNo2fPnvj5+bFmzRo8PDxITk6Wlx0dHc3ixYvZvn07u3fvJiUlReGzc3JyWLt2LZMnf3gfN+HzJ5FIyG+RaWmOVAnRCELJJlFRAfJrbzkFyrN27npkMhlrD69mzq+zuf77dbIys6lSuwqdXTqxzN2nEKMXhJJJRUUlnxb3/n3GBeGTk0oL56VEopNZxOzs7KhUqRISiYSuXbuyevVqHBwcMDIyAqBv375cvHiR0NBQbGxssLKyUkh/H3t7e2JiYoiIiODAgQN07fp2KeS/Rjdr1KiBmZkZDx484Pz58/j4+NC1a1eGDx9Odna2wtTbGzduUL9+fUxNTVFTU8szWvn7779ToUIFrK2tP9m1EZTjVeQrjC1M5McmpU1Iik8iIy1DiVEJQskU8yIGk3fam2lpUxLjk0h/p719KI+ung6//LSB4W1GMnWAG0gkRD6LpG3PNujq6bD8wFLWHl6NiYUJ7j4/0qRt4yKtnyAUR7ltzlh+bFraVPwdFIT/SHQyi5iqqqr83zKZDOnf7jLIZDKys7Pfm/4+EomEbt26ERQURHBwsEIn893PlEqlqKmpIZVK2bx5s/z5Sl9fX6pVq6ZQ3rujW2pqijOrjx8/jpOTUwFrLXzObpy5jnV9aywrWALgNNCJi0fff0NDEIR/79qZa9SoX52y/29vXQZ24vzRCwXO09mlM4MnDwLA0NQQp34dCDlwkjWeaxny9VBGdRjDqA5jiIuOY/6EBVw4JtqyIHzMtTPXqV6/uvzvYKeBTlz4W7sUhEJVAp/JFJ3MInbt2jWio6ORSqUcOHAAd3d3QkJCiI+PB8DX1xd7e3vq1q3LrVu3iIiIAGD37t3Y29srlKWqqqrQ8ezRowe7du2iTJkyWFhYyNMPHToEwO3bt0lMTKRatWo0btyYHTt2APDo0SO6dOlCWtrb5+8aNGjAzZs35bH+VcZfbt68ScOGDT/dhRGUJiEugeVTluG+1p01J9ZSvnoFNsz9lSo2VfAJXqHs8AShRImPS2DRZG881s1kQ8gvVKxekXVz11PNpiprD6/+YB6AnSt3YVbGlF+Or2PxroVs8t7Cg1t/KrNKglDsJcQl4D15KTPXTeeXkHVUrF6B9XN/oapNVVYfXqns8AShWJLI8nsYSygUly5dYvbs2ZibmxMdHU2zZs2YNm0afn5+bNmyhaysLGrVqoWnpyd6enqEhITg4+NDVlYWlpaWzJs3D3NzcxwdHdmyZQu6urq4uLhQo0YNFi1aBMCAAQMYOHCgfJTRxcUFfX19IiNzV0ybNWsW9erVIzo6Gg8PD3n6lClT+PrrrxUW/jl8+DDLly9HW1ubKlWqoKKiws8//wxA3bp1uXz5Mpqamv/4OnT+qtN/vpaCIPwz6bL3z4QQBKHwqIr7+YKgNEfCg5UdQoEkjepQKOWWWnu4UMotCNHJLEKXLl1i5cqVbN269ZOXLZPJiImJwcXFhcDAQDQ0NIDcTua4cePyjIIqk+hkCkLRE51MQVAO0ckUBOUpLp3MxJHtC6Vc/XVHCqXcghBbmJQQR44cYfbs2cyePVvewfxcZcnEqqWCUNTE/URBUI5sxAqlgiB8ecRIplDk2pfrqOwQBOGLky0TX3QFQRCEL8uJiKPKDqFAEoe3K5Ry9X9RXv3FSKYgCAoaOdrxrdu3qGuo8/TeU5a6LlPYjPpdU5ZMJuxBGHvX7SviKAWh+LJ3bMQw9+9Q11Dnyb2nLJ6yJE8be18eFRUVxnuNpW5jGwAuhVxmndcvAJQyLMW4uWMpX/UrNLU02b5iB8f3nSjy+glCcfBf2mEpw1JM/Gk8VWpVJj01ncO+Rzmw0V9JNRGEz5N4UKAYcnR0JCIigvDwcKZNm/ZJy965cyc7d+7Mk+7n54ebmxuQu0Jur1696Nq1K4MHD+bFixefNAZBeQyMDZjs/QNzR3gxrNVwop5H8Z37t3nylatSjgW75tOiU3MlRCkIxZeBsQGuS6Ywe8Qchnw9lJfPXzLMfWiB87Tt2ZpylcsxrM1IhrcbhU1jG1p2agHA1CVTiH35ilEdxuDa/0fGeY7BtIxpkddRED53/7Udjpk1ivTUdL5zGM4454k0crCjcevPZ+0LoRgSW5gIn5PIyEjCw8M/aZn9+/enf//+H8zj6uqKl5cX/v7+dOnSBS8vr08ag6A8ti1teXDrTyLDclcdDtwaiGM3hzz5nAd35vCuI5wJ+r2oQxSEYq3h1w14cOsBL57mtrGDWwJp3d2xwHlUVFXR0tFCXUP9/y81sjKyKGVYigYtbdmyZBsAsS9jGddlAklvkoqwdoJQPPzXdli1TlWO7TuOVColOyubSycuyW/2CMK/IZPKCuWlTGK6bBG6dOkSq1evRk1NjYiICGxsbJg3bx4BAQFs3LgRiURCrVq1mDlzJrq6upw8eZJly5YhlUopV64cc+bMwdT07V1pLy8vIiIi8PT0JDk5GTs7O/r06QPkrio7ZcoUFi9eTPXq1bl69SoZGRlMmzaN5s2bExsbi4eHB1FRUUgkEiZPnkzTpk0VtjA5cOAAa9asQU9Pj7Jly6Kjo0NmZiYTJ06kevXqAFhbW7Nt27aiv5hCoTCzNCU28pX8+NXLWHT1ddHR01GYRrRq5hogt1MqCELBmVma8Uqhjb1C729t7EN5jvgepWXnFuy+ugNVNVWunrnGheMXsa5nTVz0a3qN6EkjBzvUNdTZs24vEU/FTBNB+Lv/2g7v37xP255tuHPlLuoa6rRwakF2lljBWxDeJUYyi9iNGzeYPn06hw8fJiMjg/Xr17N27Vq2bt1KQEAA2trarFy5kri4ODw8PFi1ahUBAQHY2toyZ84chbJmzJhB7dq1mTVrFj179sTfP/d5gBcvXvD69Wvq1q0LQHJyMvv378fb2xs3NzcyMzOZN28ePXv2xM/PjzVr1uDh4UFycrK87OjoaBYvXsz27dvZvXs3KSkpAGhoaNC1a1cApFIpK1eupE2bNkVx6YQioKKiQn73vXJyxKIxgvApqEgk+a70K82RFijPoB8GkhCXQK/6felnNwB9w1L0HtETNTVVLMuXITU5lYndv8dr7E+MnjWSqnWqFmp9BKE4+q/tcM2cdchkMtYdXsOcDbO5dua66GQK/42YLiv8V3Z2dlSqVAmJRELXrl1ZvXo1Dg4OGBkZAdC3b18uXrxIaGgoNjY2WFlZKaS/j729PTExMURERHDgwAF5RxCQj27WqFEDMzMzHjx4wPnz5/Hx8aFr164MHz6c7Oxsham3N27coH79+piamqKmpkaXLl0UPi8zM5MpU6aQnZ3NyJEjP9n1EZQr5kUMJhbG8mPT0qYkxSeRkZahxKgEoeSIiXyFiYWJ/Ni0tCmJ8Ymkp6UXKE/zDs05vPsI2VnZpCSlcnTPMeo1rUtc9GsADu/O3RMtMiySO1fuUr2edRHVTBCKj//aDnX1dFg/71eGtRnB1P5uSCTw4v+PmQiCkEt0MouYqqqq/N8ymQypVHHPSJlMRnZ29nvT30cikdCtWzeCgoIIDg5W6GS++5lSqRQ1NTWkUimbN2/G398ff39/fH19qVatmkJ5797BU1N7O7M6JSWFYcOGkZ2dzZo1a1BXV/8HV0D4nF07c53q9atjWcESgE4Dnbhw9IKSoxKEkuPq6WvUtK1B2Yq5bayLS2fOH7lQ4DwP7zzk684tAVBVU6VJuyb8cf0+UeFR/Bn6kHa92wJgZGpIrYY1+TP0z6KqmiAUG/+1HXZx6cyQKYOA3LbWsX9HQg6EFGENhBJHWkgvJRKdzCJ27do1oqOjkUqlHDhwAHd3d0JCQoiPjwfA19cXe3t76taty61bt4iIiABg9+7d2Nsrrlymqqqq0PHs0aMHu3btokyZMlhYWMjTDx06BMDt27dJTEykWrVqNG7cmB07dgDw6NEjunTpQlpamvycBg0acPPmTXmsf5UBuQv/lC9fnmXLlqGhofFpL5CgVAlxCXhPXsrMddP5JWQdFatXYP3cX6hqU5XVh1cqOzxBKPbi4+JZOHkxs9bN5LeTv1KpegXWzl1PNZuqrDuy5oN5ANbMXouevh4bT21g/ZE1vHoZy+41vgDMGjYbu68bsuHEerz3LGbr0u08uCU6mYLwd/+1He5YuQuz0mb8enw9i3cvZLP3FtHWhP+kJC78I5HlN+FcKBSXLl1i9uzZmJubEx0dTbNmzZg2bRp+fn5s2bKFrKwsatWqhaenJ3p6eoSEhODj40NWVhaWlpbMmzcPc3NzHB0d2bJlC7q6uri4uFCjRg0WLVoEwIABAxg4cCBO/2PvzuN6yv4Hjr8+rVQotMi+JkvZs1P2XWUnyxiGERlCKZUUkshuGGuWbCkxdorBlDGyM4ws7SVJZf18+v3RzGekJN+fT4vO8/H4PGbuve977jmfnG7nnnPP6d0byJoAqGzZssTEZA3jcHV1pUmTJsTHx+Pi4iLfb29vT6dOnbJN/HPs2DFWrFhB6dKlqVOnDkpKSowePRpLS0vq1Kkj793U09Nj48aN+f4eelTt9dW+U0EQ8ud9pnivVhAEQShZTkedKOws5EvKSIvPB/0PtHcWXg+7aGQWoLCwMFavXo2fn99XTzszM5OEhARsbGw4fPiwvIfRxsYGW1vbHL2ghUk0MgWh4IlGpiAIglDSFJtG5vCcy8V9Ddq7zyok3fwQS5h8I44fP46bmxtubm5iCKsgCDm8E41MQSgUpZTEvAWCIJQ8oidTKHCiJ1MQCt4r2dvCzoIglEiikSkIhefE02OFnYV8SRmqoJ7MPaInUxCEIqKVRUvGOYxDVU2VyDuRLJ/lK1+c+mP2y2by6N4j9v98oIBzKQjFV+suZvzg8D2q6qr8fechXjOX5qhjn4px3+BK5X9mfwaoVNWAa79fx3HcPMpol2H6Aluq16uOeil1/Fbu5MSBUwVdPEEoslpZtOK7D+5vy2Ytz1H3PhVTRluLqQunUrtBbV5nvObE3hMEbT1EtbrVcFw1R36+krISNevXZP6EBVw4dqGgiygI/7Pg4GDWrVvH+/fvGTNmDCNHjsx2/OHDh7i6uvLixQt0dXVZtmwZ5cqV+2R6YnbZYmblypX88ccfecacPXuWLVu25BkTEBCAg4PDF19fKpUyfvx4evTowalTp5gyZcoXpyEUXeXKl2OmzwwWTPTg+84TiHsSx3eO43LEVa1TFS//RXTo074QcikIxVe58uVwXDaLeRPdGNVxLLGPY/lh7vf5jnGZOJ/x3X9gfPcf8J61jLTUdJY7rQRg7vLZJMYm8X2PScwYNgs7d1t0K1Us8DIKQlFUrnw57H1m4D5xAeM7f0/sk1jGf3R/yytmkssPvE5/zQSLidgNmE5L8xaYdWnFk/tPmNxzivxz5dyfnAk8KxqYwhcp7Nll4+PjWb58Obt27SIwMJA9e/bw4MGD//KXmcnkyZOZMGEChw4dwtjYmA0bNuSZpmhkFjOXL19GKs373aqbN2+SlpamkOvHx8dz7949jh8/Tv369blz545CriMUjmYdm3Hv2l/E/LOo9GG/w1gMzDmEo/+YvhzzP865I+cLOouCUKy16tSCu9fuERUZDUDg9kN0s+zyxTEqqirM9Z3DKtc1JMQkUka7DC06NGfLsu0AJMYm8UM/W1KfvyyAUglC0dc8x/3tCBYDLfIdU9ekLqcOnEYmk/H+3XvCzlymQ58O2c5v1KohHXq3Z6XjqgIokfBNUdA6mampqURFReX4pKamZrv8xYsXad26Ndra2mhoaNCjRw+OHftvqPGtW7fQ0NCgY8esdZonTZqUo6fzY2K47Bfw8fHh+PHj6OjooKuri4WFBY8fP+bSpUu8ePECPT09li9fTsWKFWndujWNGjUiMTGR/fv3M3/+fO7fv09SUhJGRkYsW7aMUqVKsX37dnbs2EGZMmWoVasW1apVY+rUqZw7d46VK1fy/v17qlSpwoIFCwgNDeXmzZs4OzuzevVq1NTUcHFxISUlBQ0NDZycnNDQ0MDf3x8AQ0ND2rdvz9y5c3n58iUJCQlYWlpiZ2eXrVxeXl5cuHABJSUlunbtiq2tLc+fP2f27NnExsZSr149Hjx4wNq1a5k8eTIpKSlYWVmhp6dHQkICU6ZMYc2aNYXxIxG+Ml3DiiTFJMq3E2OT0CyriYaWRrYhRWvmZa0j1qxjswLPoyAUZ3qGuiRkq2OJaJXVylbH8hPTZ3gvkuKTOP9Pb0mVGoY8S3jG0B8GYWbeClU1VfzX7yPqYVQBlk4Qii5dQ10SP6pXH9/f8oq5e/UeXa27cOuPW6iqqdKhVzvev8/+0H+C0/dsXbLtk6+YCEJB27ZtG6tX51zn3NbWlqlTp8q3ExIS0NXVlW/r6elx/fp1+faTJ0+oWLEic+fO5c6dO9SqVYt58+bleW3RyMynM2fOcOXKFQ4fPsyrV6+wtLSkU6dOPHz4EH9/f5SUlJg9ezaHDh3iu+++4/nz50yYMAEzMzMuX76Mqqoqe/bsQSaTMWbMGEJDQ6levTo7d+4kICAAVVVVbGxsqFatGsnJyfj4+LB9+3bKlSuHv78/S5cuxdPTkwMHDmBra4uRkRGDBg1i4sSJdO/enYiICOzs7Dh+/DjDhg0DwNramk2bNtG3b18sLS15+fIlnTp1wsbGRl6u6Ohozp07x5EjR3j16hWOjo68efOGFStWUL9+fTZu3EhYWBijR48GYN26dYwePZqAgACioqIYPXq0aGB+Q5SUlMhtcMXnes8FQcgfiZISuc23J5PKvihmyIRBeM9eJt9WVlHBsLoh6S8zmDLQjso1DFkd4EtUZBR/3bj/lUshCMWPRElCZi53ONkH97e8Yn5esIGJzhNYd2wNyQnP+fP8VRo0N5bHNGhuTLkK5TgTWHgTrQjF15cMbf0SY8aMwdLSMsf+smXLZtuWyWRIJJL/8pOZmW37/fv3hIeHs2PHDho3boyvry+LFy9m8eLFn7y2aGTm08WLF+nVqxdqamqoqanRtWtXlJWVmTNnDvv27SMyMpKIiAiqVasmP8fU1BSAli1boq2tzc6dO3n48CGPHj0iIyODS5cuYW5ujpaWFgB9+vQhNTWVa9euERsbK2/YyWSyHC/Wpqen8+TJE7p37w5AkyZNKFeuHA8fPswWN378eH7//Xc2bdrE/fv3effuHa9evZIf19fXR11dnWHDhmFubo69vT3q6upcvnwZHx8fAMzMzKhRo8bX/UKFIikhOoH6TY3k2xUNKvIy5SVvXr0pxFwJwrcjPjqBBk3ry7crGlQk9Xkqr1+9zndM3YZ1UFZWIuLSNXnMs/hnAPy6J2t4U/SjGK5fvolx0/qikSkIQGJ0IvU/rlcpL3n9wf0trxhdnbL8svAXXqZkvY40zHYoMY9j5LGd+nXi1P7TuT4gEoTCUrZs2RwNytwYGBhkm/MlMTERPT09+bauri7Vq1encePGAPTt25dp06blmaZ4JzOflJSUkMlk2falpKQwfvx4ZDIZPXr0oGvXrtl+uZQqVQqA06dPY29vT6lSpbCysqJly5ZkZmbmmiZk9Ro1a9aMoKAggoKC2L9/PytXrswWk9svsczMzBw9TosXL8bPzw9DQ0MmT56Mjo5OtnNVVFTYt28fdnZ2pKSkMGzYMCIjI1FXV8+WjoqKeB5RElw59yf1m9bH8J/ZK/uM6s2lE5cKOVeC8O24HPoHDZo1oErNygAMsOnHbycuflFMkzYm/HkhIts5sU/juHf9L3oNznrwqFNRh0bNG3Lv2l8KLI0gFB9Xzl3B+IP7W99RfXLc3/KK6WvTh9Ezsx7+a1fUptewnpwJDJGfa9K6MVc/qpeCkG8Keiczv9q2bculS5dITk7m1atXnDhxQv7+JUDTpk1JTk7m7t27QNYIz4YNG+aZpmhk5lPbtm05ceIEb9++JS0tjZCQEDIyMmjVqhXDhw+nRo0ahISE5Dqs8NKlS/Tq1Qtra2vKli1LWFgYUqmUNm3aEBoaSlpaGm/fvuXEiRNIJBJMTU2JiIggMjISgLVr17JkyRIAlJWVkUqlaGlpUaVKFU6cOAFAREQESUlJ1K1bF2VlZd6/fw/AhQsXGD9+PL169SIyMpL4+PhsDdvbt28zatQoWrZsyZw5c6hduzaRkZG0b9+egwcPAlkv+/6blw+pqKjIryN8G148e4HPzOXM+9mJjWd+pmb9GmxYsJG6JnVZeyznmH5BEL5MyrMUFs9YgvsGV/xCNlPLuBZr3NdjZFKPTSd+zjPmX1VqViEuKi5H2k7jXWnZuSXbzmxi5X4ftvn6cffavQIrmyAUZSnPXrB05jLm/ezML2c2UKN+DTYs2EBdk7qsO7YmzxgA/9V70K1UkQ2n1rPE34vtPn789cFDnMo1KxP/NL5QyiYUf5kyxXzyS19fn59++onRo0czcOBA+vbti4mJCRMmTODGjRuUKlWKNWvW4OzsTJ8+fQgLC/vsKhWSTNGvn2/Lly/n1KlTlCtXDiUlJaytrdm1axevX2cNYTI2NkYmk7F06VKMjIy4dy/r5n7v3j3s7e0BUFVVpXLlytSqVYuffvqJnTt3smvXLjQ0NNDR0aFly5ZMmDCBM2fOsGLFCmQyGfr6+nh7e6Ojo8OmTZvw9/fHy8uLcuXK4ebmRkpKCqqqqjg7O9OsWTMuX77MnDlzGDduHDo6Ovj6+lKqVCkMDAx4/vw5M2fOJC4ujvDwcBYvXoyXlxdnz56ldOnSNGvWDEdHR96+fYurqyt3796lWrVqXL16lb179wIwevRozpw5w7t377CxsUFVVRU/P798f489qvb6yj8ZQRA+55XsbWFnQRBKpFJKqoWdBUEosU48Pfb5oCLgWb9OCkm3QnCoQtLND9HIzKerV6/y6NEjLC0teffuHUOHDmXhwoXUr1//8yd/QmRkJKGhoYwdOxaAyZMnM3jwYCwsLPI+sRBYWFiwfft2qlSp8v9Oq3OVrl8hR4IgfInXMjHqQBAKi5pEubCzIAgl0rno04WdhXx51kdBjcwjhdfIFC/a5VPNmjVZvXo1W7ZsITMzk4EDB/6/GpgAlStX5saNG/Tt2xeJREL79u0xN8+5JqEgCIIgCMWTaGAKglASiZ5MocCJnkxBKHiiJ1MQCodoZApC4SkuPZlJvRTTk1nxqOjJFAShgLW2MGOC43hU1VR5eOchS+x9ciwg/amY+T+7UPmf2fcADKpW4trv13D6zoUmbU35cd4klFWUSX2eymrXtfx95+HHlxeEEqttl9b86DgBVXVVHtx+iOfMJTnqXl4xx24GkRD734LxO9f6c/zgKWrUrY6jtz2lNUuTmZnJWs8NhIVeLtCyCUJR1rqLGT84fI+quip/33mI18ylOe97n4hx3+Ca7b5XqaoB136/zvqFv+CyZq58v7KSErWMa+H8vSvnjv5WYGUTirkvmKSnuBA9mcL/y8qVK2nbti0tWrTI9zmiJ7PwlStfjq1nfsHWcjrRkdFMnPs9Gpoa+Dqt/KIYACNTI+b/7MJUy+lkpGXgf2kHrj+48+eFq1SrXRWPze6M7zaRd2/fFXQxhQ+InsyiQbt8OXaFbOWHAbY8jYxmitNENDQ18J7rm6+YarWrsnTrQoZ0sMmR9tr9vvy6/ziH/Y9Sr1Ed1u73pUfDAbnOei4UHNGTWTSUK1+O7Wc3MWWgHVGR0UyaO4HSWqVZPnflF8UA1Dc1wn2DK7aWdiTEJGY7NsVlEuX1yrPAdmGBlEvIW7HpyeyhoJ7M44XXkymWMBH+Xy5fviz+gCmGWnZqzt1rfxEdGQ3Aoe3BdLXs8sUxKqoqOC6fzWq3tSTGJlKlZmXSX6bz54WrADz5+ykZLzNo2LxBAZRKEIo+s04tuRNxl6f/1KuAbYfoYdU13zGNWzREJpOx/uBKdpzaxHc/jUZJKetWrqSsRNlyZQDQ0NTg7Rsxo7Ag/KtVpxbcvXaPqH/qVeD2Q3T76J6WnxgVVRXm+s5hleuaHA1Mk1aN6dSnIz4OvooriPBNKuwlTBRBDJf9ynx8fDh+/Dg6Ojro6upiYWHB48ePuXTpEi9evEBPT4/ly5dTsWJFWrduTaNGjUhMTGT//v3Mnz+f+/fvk5SUhJGREcuWLaNUqVJs376dHTt2UKZMGWrVqkW1atWYOnUq586dY+XKlbx//54qVaqwYMECdHR0uHjxIosXLyYzMxNDQ0N8fHwAmDt3LvHx8SQkJNCmTRs8PT0JDw/H29sbmUxG3bp1cXR0xMnJiYcPH6KmpoaDgwNt2rShffv29OjRgytXrqCsrIyvry9Xrlzh5s2bODs7s3r1aoyMjAr52xfyS89Qj8SYBPl2YmwiWmU10dDSkA8dyk9M72G9eBb/jN+OXQDg6cMoSmmUokXH5vxx7gpGpkbUMKpOeb3yBVg6QSi69CrrEf/BH6YJsYloldXKXvfyiFFRVuby+Sus8dyAiqoKy/wWkf4ygz2/7GfpXF9W71vOsAmD0amozbzJ7uIhoCD8Q89QN1ujMDG3upePmD7De5EUn8T5f+57H5o8byIbvTbnGIIrCCWR6Mn8is6cOcOVK1c4fPgwGzZs4Pbt20ilUh4+fIi/vz/Hjx+nUqVKHDp0CIDnz58zYcIEgoKCiIiIQFVVlT179nDy5ElevnxJaGgod+/eZefOnQQEBLBr1y4eP34MQHJyMj4+PmzatInAwEDat2/P0qVLefv2Lfb29nh5eREcHEy9evU4ePAgISEhGBsbs2fPHo4fP87ly5e5desWAI8ePWLbtm14eXmxYsUKqlWrxtGjR1myZAm+vr4AJCYm0qZNGwIDA2nZsiU7d+5k4MCBNGrUCA8PD9HALGYkEgm5DZSXSWVfFDN4gjV+K3fKtzPSMnAe78qoqSP45cTP9BjUjasXInj/TgzVFAQAJYmE3CrWh/Uqr5igXUfwcV7J61evSUtNY/fP++jUqz1q6mp4rHdlwfTF9G8xmElWdsxZMhM9Q12FlkcQiguJkhK5vSGW7b6Xj5ghEwaxfcXOHDGNWjRAu7w2pw4Wj+GZQtEiejKFPF28eJFevXqhpqaGmpoaXbt2RVlZmTlz5rBv3z4iIyOJiIigWrVq8nNMTU0BaNmyJdra2uzcuZOHDx/y6NEjMjIyuHTpEubm5mhpaQHQp08fUlNTuXbtGrGxsYwePRoAmUxGuXLluHfvHvr6+hgbGwMwc+ZM+bWuX7/O1q1befjwISkpKWRkZD1pq1mzJmXKZA2xunz5MkuXLgXAyMiIPXv2yM/v0KEDAHXr1uWPP/5QyHcoFIyEmASMmxrLtysaVCQ1JZXXr17nO6ZOwzooKysTcemaPEYikfAq/RXTB//3787v3Fb5kFtBKOnioxNo2Oy/eqVrUJEXz7PXvbxielp348Htv3nw72RaEgnS91JqGdVEvbQ6F05dAuDWn7eJvPeIhk0bkBBTeO/kCEJRER+dQIOm/y09V9GgIqm51L28Yuo2rIOyslK2+96/LPqZc3z/iVwbqYLwOYXdIFQE0ZP5FSkpKSGTZf9XkpKSwvjx45HJZPTo0YOuXbtm+wVUqlQpAE6fPo29vT2lSpXCysqKli1bkpmZmWuaAFKplGbNmhEUFERQUBD79+9n5cqVqKqqIpFI5HEvX74kLi4OPz8/lixZQvny5Rk1ahS1a9eW5+PfPACoqKhkO//vv/+WX19dXR34t4dL/BItzi6HXqFBM2Mq16wMQH+bflw4fvGLYpq0NuHqxavZzsnMzGSx30KMTOoBYN6vM2/fvBWzywrCP8JCL9OoWQOq/lOvLEf35/yJC/mOqV2/JhNmfYeSkhLqpdQYPM6SU0FniHoUjVYZLRq3aAhA5eqG1KhXnb9u3i/A0glC0XU59A8aNGtAlX/q1QCbfvx24uP7Xt4xTdqY8OeFiFzTN21jwpXfruZ6TBBKItHI/Iratm3LiRMnePv2LWlpaYSEhJCRkUGrVq0YPnw4NWrUICQkJNd3ZC5dukSvXr2wtrambNmyhIWFIZVKadOmDaGhoaSlpfH27VtOnDiBRCLB1NSUiIgIIiMjAVi7di1LliyhZs2aPHv2jAcPHgDwyy+/sHv3bi5cuMDQoUPp378/b9684e7du7k2Xlu0aMGRI0eArAbmhAkTsjU6P6asrCze+SmGUp6l4DXTm/k/u7Dt7CZq1a/J2gU/Y2RSj1+Or88z5l+Va1Ym7ml8jrQ9bBdiv2QGW07/Qt8RvXEe71pg5RKEou75sxQW/OTFwg3z8Q/dRu36tVg5fy31TYzYfvKXPGMAflm2jdSUVHae2cyOU5u5/sdNgnYdIS01jTnjnfnJfSo7Tm9m4cb5LJ7tQ/TjmMIsriAUGSnPUlg8YwnuG1zxC9lMLeNarHFfj5FJPTad+DnPmH9VqVmFuKi4XNOvUrPyJ48JwmdlShTzKURiCZOvbPny5Zw6dYpy5cqhpKSEtbU1u3bt4vXrrKEWxsbGyGQyli5dipGREffu3QPg3r172NvbA6CqqkrlypWpVasWP/30Ezt37mTXrl1oaGigo6NDy5YtmTBhAmfOnGHFihXIZDL09fXx9vZGR0eH8PBwFi9ezLt376hWrRpLlizh+vXruLm5oaamhpaWFqVKlaJ3795Uq1aN1atX4+fnB0BqairOzs48evQIFRUV5s6dS4sWLbLlNSAgQH6NTZs24e/vj5eXF82aNcvXdySWMBGEgieWMBGEwiGWMBGEwlNcljCJ79xZIenqh4QoJN38EI3Mr+jq1as8evQIS0tL3r17x9ChQ1m4cCH169f//MmfEBkZSWhoKGPHjgVg8uTJDB48GAsLi6+U64LXrFL7ws6CIJQ44le9IAiCUNJcjcs5C3BRFNexs0LSNTgXopB080NM/PMV1axZk9WrV7NlyxYyMzMZOHDg/6uBCVC5cmVu3LhB3759kUgktG/fHnNz86+UY0EQBEEQBEEQhK9L9GQKBU70ZBYN7bu0YercH1BVU+P+nb9xn7GI9I/W9spPzNJNniTGJeHltByABqb1sXefRmmN0igpK7FtzU5+PXCiwMol5E78qi862ndtw9S5k1BTU+P+nQfM/ymXuveJGK0ymrgud6RGneooKUkI3nuUrauzllMoq12GOZ4zqFWvBuql1Nm0YhtH9h8vjCIKQpEk6l7JU1x6MmPbK6YDqdJvZxWSbn6IiX8KmKOjI126dPlq60o6ODgQEBDwVdL6X+zdu5fDhw8X2vWF/412BW3cfOdi/70zVh1GEP04hqlOk784ZsyPI2hqZpJtn/cmT9Yv3czwbuOYOtKeGW5TqVqzisLLJAjFgU4Fbeb7OjFrvBOW7YcT9TiGac6T8x3z45wJxMckMrizDSN7fs/gMZaYNM+aUdZ9hTPxsQkM7zaOSUPsmO0xHb1KYp1MQQBR94Si7VtcJ1M0MgvYwYMHOXr0qHwSneLuzz//5O3bt4WdDeELtenUklsRd3gaGQXAvm0H6WXV7YtimrdpSltzM/ZvD5LvU1NXY4PPZsLPZ62jmhCbyPNnKeiLm60gANC6UytuRdzhSbZ61T3fMUucfVk+fzUAunoVUFVTJe1lOmW1y2DWsSUbfDYDWXXPpvdEUlNSC6poglCkibonCAVLvJNZgCZNmkRmZiZt27bl3bt3XLt2DQcHB1JSUnj8+DGzZs2iYsWKLFq0iNevX6Ojo8P8+fOpWrUqNjY21K9fnz/++IM3b94wd+5c2rfPPux0+fLlXLp0iRcvXqCnp8fy5cupWLEiwcHBrFu3DolEQuPGjVmwYAFv377F3d2d+/fvI5VKmTBhAn379uXNmzfMnz+fK1euoKqqyo8//kjv3r2xsLCgf//+/Pbbb7x69QovLy9SU1M5c+YMv//+O7q6unTo0KGQvlnhS+kb6hMfkyDfTohNpExZLTS1NORDh/KKKa1ZmlkL7LAdMRNrmwHymLdv3hK0+4h822pUfzS0NLjx560CKJUgFH0GhnrER39Qr2Jy1r3PxUilUjxWu9C1b2fOHj3HowdPMDY1IikhiVE/DKOdRWvU1NXYvnYXTx4+LfAyCkJRJOqeUJRlFvJyI4ogejIL0Pr1WWstBQYGUqFCBfl+bW1tjh49Svv27XF2dsbHx4eDBw8ybtw45s2bJ49LS0vj4MGD+Pj44ODgkK0H8fHjxzx8+BB/f3+OHz9OpUqVOHToEPHx8SxatIjNmzdz5MgRpFIpoaGhrFu3joYNGxIQEMDOnTtZv349T58+xc/Pj4yMDI4ePcqWLVtYs2aN/Dra2trs37+fYcOG8fPPP9O2bVssLCyYNm2aaGAWM0pKklzf0ZNKZZ+NkUgkLFrnho/rSpISnn3yGmNtR/GD/XdMHz2HN69Fb7cgAEiUlMgkl7r3wbrF+YlxtnXHvEEfymqXZeLMcaioqFClemXSX6Yzrv9kHH5wYab7NIxNvs6rGYJQ3Im6JwgFS/RkFgEmJlnvtD169IinT58yefJ/7wikpaXJ/3/IkCFA1lqburq62YbcVq9enTlz5rBv3z4iIyOJiIigWrVqXL16lWbNmmFgYACAt7c3AGvXruX169ccOHAAgIyMDO7fv8/ly5cZMmQISkpK6OrqcuTIf71S/zYk69aty4kTYiKX4iwuOp5GTRvIt/UqVeTF81Rev3r92Zha9WpQuZohM9ymAlBBrzzKykqoqauxwN4LVTVV5vs6UateDcb2nUSsWJxaEOTiouNo3CyXupfxOl8xbTq34sGdhyTGJ/Eq4xXHAk/RpU8nDu35FYAg/6z/Pn0UTUT4dRo1NebO9W/j9QxB+P8QdU8oygr7/UlFED2ZRUCpUqUAkMlkVKlShaCgIIKCgggICGDXrl3yOGXl/xZ0lslkqKj894zg5s2bjB8/HplMRo8ePejatSuZmZmoqKggkfzXBZ+cnExycjIymQxvb2/5tfbu3UuHDh1yxD9+/Fjek6murg6Q7bhQPF0KCadx84byCXmsRw8k9Pj5fMVcv3KL3i2sGd5tHMO7jePA9iBOBJ1hgb0XAB6rXdAso8HYfqKBKQgfuxSaVa+q/VOvBo22JOTjupdHTPf+FkycOQ4AVTVVuve34PJvfxLzJJbb1+7Sb2gvAMpX1MG0RWNuXbtbUEUThCJN1D2hKMuUSRTyKUyikVmE1KpVixcvXvDHH1mTphw4cAB7e3v58V9/zXpKduPGDVJTU6lXr5782OXLl2nVqhXDhw+nRo0ahISEIJVKady4MRERESQmJgKwcOFCTp8+TevWrdm9ezcACQkJ9O/fn9jYWFq2bMmvv/5KZmYmz549Y9SoUXlO7KOsrIxUKv3q34WgWM+fpeA2fSHeGz04cG4HdevXYtn81RibGrH75JY8Y/Ji0rwh3fqZU7VGFbYcWsfuk1vYfXILbTq3KohiCUKR9zzpn3r1iwcHzu2kjnEtlrmtooFpffxPbc0zBsDHbTVlymqxL8SPXSc2c+faPXZt3AvAzO/m0qZzK/aH7mBjwGo2LNvC7Qjxh64ggKh7glDQxDqZBczIyIjTp08zevRozpw5g4ODA61atcLKygqAq1ev4unpyZs3b9DS0sLLy4tq1aphY2ND2bJliYmJAcDV1ZUmTZrIz2/Xrh22tra8fp017MPY2BiZTMbSpUs5duwYa9euRSaT0aRJE+bPn8+rV69wc3Pj7t27SKVSJk6ciKWlJW/fvsXDw4OrV68CMHXqVLp3746FhQXbt2+nSpUqhIWFsXr1avz8/Dhy5AjLli1j1qxZ9OzZM1/fgVgnUxAKnvhVLwiCIJQ0xWWdzCctuigk3Wp/nFZIuvkhGpnFhI2NDba2tpiZmRV2Vv7fRCNTEAqe+FUvCIIglDSikVl4jUwx8Y8gCEIJIOUbnFVAEIoBFYny54MEQSjRCvv9SUUQjcxiws/Pr7CzIAiCIAiCIAjCVyYamYIgfDPad2nD1Lk/oKqmxv07f+M+Y5F8QeoviVm6yZPEuCS8nJZn2z9gWB/Me3Vk+pg5Ci+LIBQnHbq2xW7uZNTUVPnrzt+4/uSZo159Kka9lDpzF9nTuKkxSCTc+PM2Cx2XUrlaJRavnS8/X1lZmbrGtfnpOwdO/xpa0EUUhCJJUfe9Fm2bMt1lCioqKrx5/YYlzr7cirhTYOUShKJIzC5bhFlYWBAVFcXTp0+ZO3fuV0179+7d8tllPxQQEICDg0O2fb6+vqxatUq+nZqaysSJE+nVqxcjR46Uz1wrFB/aFbRx852L/ffOWHUYQfTjGKY6Tf7imDE/jqCpmUm2fWW1yzDXyx77BXaI1W4EITudCtos8HVixnhH+rcfRtTjaKY7/5jvmAl2Y1BRUcba3IZB5jaUKqXG+GmjefjXI4Z0HSP/XAwN49eAE6KBKQj/UNR9T0VVhcU/u7PA3othXcfyi+82FqyaVyBlEr4dmZmK+RQm0cgsBmJiYnj69OlXTXP48OEMHz48z5iXL18yd+5ctmzZkm2/r68vLVq04OjRowwePBhPT8+vmjdB8dp0asmtiDs8jYwCYN+2g/Sy6vZFMc3bNKWtuRn7twdlO69bfwsS45LwdV+j4FIIQvHTplMrbkbc4ck/9WrvtgB6W/XId8yV3yPYsHwLmZmZyGQy7t78C8MqBtnOb2ZmSre+5iyY7VUAJRKE4kFR9733797Ts+lA7t28D0Dl6oa8eP5C0cURhCJPDJdVgLCwMNauXYuKigpRUVGYmJjg6elJcHAwW7ZsQSKR0LBhQ+bNm4empiZnz57F19cXmUxG1apVcXd3p2LFivL0PDw8iIqKYv78+aSlpdGyZUuGDBkCZM06a29vz9KlS6lfvz5//PEHb968Ye7cubRv356kpCRcXFyIi4tDIpEwc+ZM2rZtK++ZnDp1KoGBgaxbtw4tLS0qV66MhoYGAKdPn6ZGjRqMGzcuW/lCQkLYuXMnAH379sXd3Z13796hqqpaEF+v8BXoG+oTH5Mg306ITaRMWS00tTTkw4LyiimtWZpZC+ywHTETa5sB2dI+8M/Nt9+QXgVQEkEoXgwM9YmL/q9excfkrHt5xVwKDZfvr1TFgJEThuI+K3tjcoaLLasW/ZxjiJ8glGSKvO+9fy+lfEUddp3YjHb5cjhMci2YQgnfjG/xnUzRk6kgV69excnJiWPHjvHmzRs2bNjA+vXr8fPzIzg4mNKlS7N69WqePXuGi4sLa9asITg4mGbNmuHu7p4tLWdnZxo1aoSrqyvW1tYEBWX9ER8dHU1ycjKmpqYApKWlcfDgQXx8fHBwcODt27d4enpibW1NQEAA69atw8XFhbS0NHna8fHxLF26lJ07d7Jnzx7S09PlxwYOHMjEiRNRVs4+M15CQgK6uroAqKiooKWlRXJyskK+R0ExlJQkuS5pIZXKPhsjkUhYtM4NH9eVJCU8U2g+BeFbo6SkBOSsVzKZ7ItijE2M2Bq4Dv/NBzh38r8p+k1bNEangja/Bpz4qvkWhOJO0fe95KTn9Gxmydh+k3BbPpdqtap+vcwLQjEkejIVpGXLltSqVQuAAQMGMHXqVEaNGoWOjg4AQ4cOxdHRkVatWmFiYkKVKlXk+zds2PDJdM3MzJg3bx5RUVEEBQUxYMB/T9P+7d00NjZGV1eXe/fucfHiRR4+fMjKlSsBeP/+fbaht1evXqVp06byntN+/frx+++/f1FZMzMz//mjSCgu4qLjadS0gXxbr1JFXjxP5fWr15+NqVWvBpWrGTLDbSoAFfTKo6yshJq6GgvsxfA8QchLbHQcjZt9WK90efE8lVcZr/Md03NAV5wWz2LRXB9+PZi9MdlzQBeC9x0V66IKwkcUdd9bPn81Lds35+zRcwDcvfEXf91+QB3jWjx5+HVfdRK+XZmZoidTyKcPe//+fXfmQ5mZmbx///6T+z9FIpEwcOBAjhw5wtGjR7M1Mj+8pkwmQ0VFBZlMxrZt2wgKCiIoKIi9e/dSr169bOl9+MeIisrnnzvo6emRlJQEZDVa09PT0dbW/ux5QtFxKSScxs0bUrVm1sMN69EDCT1+Pl8x16/concLa4Z3G8fwbuM4sD2IE0FnRANTEPLhUmg4Js0bUe2fejV4tCVnj5/Ld0ynbu1x8PyJH4bZ5WhgQtY7Y2Hn/1BwKQSh+FHUfU8qleG6zBHTlo0BqFWvJjXqVOPmn7cLtoBCsZYpU8ynMIlGpoJcuXKF+Ph4ZDIZgYGBODo6cubMGVJSUgDYu3cvZmZmmJqacu3aNaKisl4y37NnD2ZmZtnSUlZWztbwtLKywt/fn0qVKqGvry/f/+uvvwJw48YNUlNTqVevHq1bt2bXrl0APHjwgH79+vHq1Sv5Oc2bNyciIkKe13/TyEunTp0IDAyUX7NFixbifcxi5vmzFNymL8R7owcHzu2gbv1aLJu/GmNTI3af3JJnjCAI/7vkpOfMm+6Bzy8LCTy3m7rGtVnqtooGpvXZe2pbnjEAM11tQSLBzceRvae2sffUNuYuspenX71WVWKexhZK2QShKFPUfe9VxitmfOeIvfs0dp/cgutyR5x+nE9CrJh5XyjZJJliTM1XFxYWhpubG3p6esTHx9OuXTvmzp1LQEAA27dv5927dzRs2JD58+ejpaXFmTNnWLlyJe/evcPQ0BBPT0/09PSwsLBg+/btaGpqYmNjg7GxMd7e3gCMGDGCUaNG0bt3byBrAqCyZcsSExMDgKurK02aNCE+Ph4XFxf5fnt7ezp16pRt4p9jx46xYsUKSpcuTZ06dVBSUmLx4sXy8nwYC5CSkoKDgwNPnz6lTJkyLF26VD7cNz+aVWr///yGBUH4Uu8zpYWdBUEokVQkyp8PEgRBIf6M/a2ws5Avfxn3VEi69e4cU0i6+SEamQoQFhbG6tWr8fPz++ppZ2ZmkpCQgI2NDYcPH0ZNTQ3IamTa2trm6AUtikQjUxAKnmhkCkLhEI1MQSg8opFZeI1MMfFPMXP8+HHc3Nxwc3OTNzCLG/FcQxAKnvhDVxAKxxvZu8LOgiAIRdy3OPGP6MkUClxTg3aFnQVBKHEkkm/vBiYIxYFoZApC4bkVH1bYWciXu/V6KyTd+n99fq4VRRE9mYJQQrXv2oapcyehpqbG/TsPmP/TohyLt38qRquMJq7LHalRpzpKShKC9x5l6+qdAJTVLsMczxnUqlcD9VLqbFqxjSP7jxdGEQWhSGrfpQ1T5/6Aqpoa9+/8jfuMXOpePmKWbvIkMS4JL6flADQwrY+9+zRKa5RGSVmJbWt28usBsV6mIPyrY9d2THeajJqaGn/dfsC8nzxJT0vPV4x6KXWcF8+icdMGSJBw/eotPBy8efP6jfxcy+H96Nq7E1Ns7D++tCCUOGJ22WLAwsJCPvtsbj5cxkQQ8kOngjbzfZ2YNd4Jy/bDiXocwzTnyfmO+XHOBOJjEhnc2YaRPb9n8BhLTJo3BMB9hTPxsQkM7zaOSUPsmO0xHb1KugVeRkEoirQraOPmOxf7752x6jCC6McxTHWa/MUxY34cQVMzk2z7vDd5sn7pZoZ3G8fUkfbMcJsqX4pBEEo6nQraeKxwZvp3jvRtN4Sox9HMcP4x3zE/TB+LsrIylp1HYmk+klKl1JkwbQwA5bTL4rJkDo4eP4EYNSL8DzIzFfMpTKKR+Q0ICgoq7CwIxUzrTq24FXGHJ5FZDy/2bTtIL6vu+Y5Z4uzL8n+mddfVq4CqmippL9Mpq10Gs44t2eCzGYCE2ERsek8kNSW1oIomCEVam04tuRVxh6fZ6lW3L4pp3qYpbc3N2L/9v9/9aupqbPDZTPg/a2QmxCby/FkK+uIBjyAA0LazGTev3uFJ5FMA/LcF0Me6Z75j/rgUwc/LN8vXPr9z4x6GVQwA6NG/C4lxSXj/s9SQIAhiuKxChYWFsXbtWlRUVIiKisLExARPT0+Cg4PZsmULEomEhg0bMm/ePDQ1NTl79iy+vr7IZDKqVq2Ku7s7FStWlKd39+5dXFxceP/+Perq6ixatIgaNWpgZGTEvXv3mD17Nvfu3QMgOTmZcuXKcfjwYc6dO8fKlSt5//49VapUYcGCBejo6ODl5cWFCxdQUlKia9eu2Nra5liu5N9lVMLDwwkJCSElJYWEhASGDRtGdHQ0v//+O9ra2vzyyy+oq6sX/Jcs/E8MDPWIj06QbyfEJFKmrBaaWhryIXmfi5FKpXisdqFr386cPXqORw+eYGxqRFJCEqN+GEY7i9aoqauxfe0unjx8WuBlFISiSN9Qn/iYD+pVbM66l1dMac3SzFpgh+2ImVjb/DeK5e2btwTtPiLfthrVHw0tDW78easASiUIRV8lQ33iYuLl2/ExCf/UK035kNm8Yi6G/vduX6UqBthMHIab/SIA9m4/CMDAoX0KoijCNyhT9u31gIueTAW7evUqTk5OHDt2jDdv3rBhwwbWr1+Pn58fwcHBlC5dmtWrV/Ps2TNcXFxYs2YNwcHBNGvWDHd392xpbdu2jXHjxhEQEMCQIUOIiIjIdnzJkiUEBQWxbds2tLS0mD9/PsnJyfj4+LBp0yYCAwNp3749S5cuJTo6mnPnznHo0CF2797NgwcPePPmDXm5ceMGa9euZdOmTSxatIiOHTsSHBwMwPnz57/q9yYolkRJiUxyjqOQymRfFONs6455gz6U1S7LxJnjUFFRoUr1yqS/TGdc/8k4/ODCTPdpGJsYKaYgglDMKClJcp1hWyqVfTZGIpGwaJ0bPq4rSUp49slrjLUdxQ/23zF99BzevH77dTIuCMWc5BP1SiaTflFMA5P6+AX9zK7N+wg9eUExmRVKHFmmRCGfwiR6MhWsZcuW1KpVC8h6d3Lq1KmMGjUKHR0dAIYOHYqjoyOtWrXCxMSEKlWqyPdv2LAhW1qdOnXC3d2d8+fPY2Fhgbm5eY7rvX//Hjs7O0aPHk3z5s05e/YssbGxjB49GgCZTEa5cuXQ19dHXV2dYcOGYW5ujr29/Wd7Ips1a4aWlhZaWloAtGnTBoDKlSuTmiqGQxYncdFxNG7WQL6tV6kiL56n8jrjdb5i2nRuxYM7D0mMT+JVxiuOBZ6iS59OHNqTNYtZkH/Wf58+iiYi/DqNmhpz5/q9AiqdIBRdcdHxNGqaS7169fqzMbXq1aByNUNmuGWNNKmgVx5lZSXU1NVYYO+Fqpoq832dqFWvBmP7TiI2Kq7gCiYIRVxsVDwmzRrJt/Uq6fLi+QtefXDf+1xMr4HdmLd4Fp5zl3IkQEyqJQh5ET2ZCqas/N/adP+O4/9QZmYm79+//+T+D/Xs2ZODBw9iYmLC1q1bcXV1zXG9hQsXUrVqVYYPHw6AVCqlWbNmBAUFERQUxP79+1m5ciUqKirs27cPOzs7UlJSGDZsGJGRkUgk2Z/ivXv339Trqqqq2a6loiKeURRXl0LDady8IdX+mRRk0GhLQo6fz3dM9/4WTJw5DgBVNVW697fg8m9/EvMkltvX7tJvaC8AylfUwbRFY25du1tQRROEIu1SSFa9+ndCHuvRAwn9uO59Iub6lVv0bmHN8G7jGN5tHAe2B3Ei6AwL7L0A8FjtgmYZDcb2Ew1MQfjYxdAwTJo3olrNqgAMHWPFmWPn8x3TuXt7HD1nMGHoNNHAFL66zEyJQj6FSTQyFezKlSvEx8cjk8kIDAzE0dGRM2fOkJKSAsDevXsxMzPD1NSUa9euyWeR3bNnD2ZmZtnSmj59Ojdu3GDYsGHY2dlx+/btbMf37t3L7du3cXFxke8zNTUlIiKCyMhIANauXcuSJUu4ffs2o0aNomXLlsyZM4fatWsTGRmJjo4ODx48AOD69eskJiYq6qsRCtHzpBTcpi/E+xcPDpzbSR3jWixzW0UD0/r4n9qaZwyAj9tqypTVYl+IH7tObObOtXvs2rgXgJnfzaVN51bsD93BxoDVbFi2hdsRopEpCADPn/1TrzZ6cODcDurWr8Wy+asxNjVi98ktecbkxaR5Q7r1M6dqjSpsObSO3Se3sPvkFtp0blUQxRKEIi856TnOdgvw3bSIQ+f9qWtcG2+3FTQ0rc+B0355xgDYu05DggT3ZU4cOO3HgdN+OC+aVZhFEoQiTZKZ2+Bz4asICwvDzc0NPT094uPjadeuHXPnziUgIIDt27fz7t07GjZsyPz589HS0uLMmTOsXLmSd+/eYWhoiKenJ3p6evLJd9LS0nByckImk6GqqoqzszMmJibyiX8aNWpE1apVKVWqlLxndM+ePVy8eJEVK1Ygk8nQ19fH29tbPvHP2bNnKV26NM2aNcPR0ZGXL19iZ2dHUlISDRs25O+//2blypWEh4cTHh7O4sWLAeTXBHBwcKBVq1ZYWVnl63tpatBOMV+4IAifJBHT6gtCoXgje/f5IEEQFOJWfNjng4qA6zX6KSRdk0fBCkk3P0QjU4HCwsJYvXo1fn5+hZ2VIkU0MgWhcLzNfP/5IEEQvjp1JdXPBwmC8NX9GftbYWchX77FRqZ4qU4QBKEEEA1MQSgcooEpCMLnFPZMsIogGpkKZGZmluO9SkEQBEEQBEEQhH8V9iQ9iiAm/hGEEqp91zbsObONg7/tZsnGBWhqaeQ7RquMJt6/eLAvxI8D53Yw1nak/JyO3doRcuco/qe2yj8amjnTFoSSqmPXdgSc3cHhC3tZtnEhmlqaXxxjYKjHmYhgtMuXk+9r1a45+05uI+DsDrYErMWoQV2Fl0UQiqv2Xdqw5/RWAs7vwmvDJ+6B+YhZusmTOZ4/FUSWBaFYEY3MAmJjY0NY2Nd/+TgtLQ0rKyv69u0rn0H2Y46OjkRHR39RugMGDPga2ROKKJ0K2sz3dWLWeCcs2w8n6nEM05wn5zvmxzkTiI9JZHBnG0b2/J7BYywxad4QANOWjdm+bjfDuo6VfzLSMwq8jIJQFOlU0MZjhTPTv3Okb7shRD2OZobzj18U039wL7YF/Yx+JT35Pq0ymvhuXsxS91VYmY/CfbYXPhs9UVUTQzUF4WPaFbRx852L/ffOWHUYQfTjGKY6Tf7imDE/jqCpmUlBZl34RmVmKuZTmEQjs5i7c+cOampqHD58mJo1a+YaExYWxpfO7xQUFPQ1sicUUa07teJWxB2eRGYtmbNv20F6WXXPd8wSZ1+W/7Okgq5eBVTVVEl7mQ6AactGtGrfjD2nt7IpcC3NWpsWVLEEochr29mMm1fv8CTyKQD+2wLoY90z3zG6+hWx6NWJiUOnZTuneq2qpKWmEXb+DwAiHzwm/WU6TVo0VnSRBKHYadOpJbci7vA02/2t2xfFNG/TlLbmZuzfLv5eEoTciEZmLsLCwvjuu+/48ccf6dGjB9OmTSMyMhILCwt5zKpVq1i1KmvNwHbt2uHi4sLAgQP5/vvvOXr0KCNGjMDCwoLw8HD5OXv37mXgwIEMHDhQ3quZnp7OnDlzsLKyYsCAARw+fBiAgIAAbGxs6NevH8uWLSMpKYkffviBfv36YWlpyblz53j27Blz587l3r17TJo0ibt37zJkyBCsrKwYPnw4jx49YsOGDSQkJDBx4kSeP39OREQEgwcPpn///owZM4bHjx8DWT2ttra29OjRgzt37mBkZJRn/nK7llB8GBjqER+dIN9OiEmkTFmtbEOBPhcjlUrxWO3CvhA/rly8yqMHTwBISU5l//ZAhnYZy6qF6/HZvAi9SroFVDJBKNoqGeoTFxMv346PSfinXmnmKyYxPonp3znw+OHTbOk++vsppTVK07ZT1jwAjZoYU9uoFrr6FRVcIkEofvQN9YmP+eD+FpvzHphXTEX9CsxaYIfTFHdkUlmB5l34NskyJQr5FCbRyPyEq1ev4uLiwtGjR4mJieG33z49BXJSUhIdO3YkMDCQN2/ecOrUKXbt2sXUqVPZtm2bPE5DQ4PAwEAWL17MrFmzePv2LevWraNhw4YEBASwc+dO1q9fz9OnWX88xMfHc/DgQWbMmMGCBQto3bo1wcHBrFy5krlz55KZmYmHhweNGjVi/fr1bNu2jXHjxhEQEMCQIUOIiIhg4sSJ6OnpsWHDBjQ1NZkxYwbz5s3j0KFDDBs2jBkzZsjzZ2RkxPHjxzE2Npbv+1T+cruWUHxIlJTIJGfvtlQm+6IYZ1t3zBv0oax2WSbOHAeA/fi5nDocAkBE+HWu/XGD1p1afuUSCELxJFGS5DqyRCaTflHMx9LT0pk2djYT7MYQcGYH/Yf0JuzCH7x7K9ZoFISPKX2ijkk/aDB+KkYikbBonRs+ritJSnim0HwKJUdmpkQhn8IkZpf9hLp162JgYABA7dq1efHiRZ7xHTt2BKBy5co0b94cAENDQ1JTU+UxgwYNAqB+/fpUqFCBhw8fcvHiRV6/fs2BAwcAyMjI4P79+wA0aNAAFZWsH9Hvv/+Oh4cHAFWrVsXU1JRr166hpaUlT79Tp064u7tz/vx5LCwsMDc3z5bHR48eUbZsWUxMst4f6NWrFy4uLrx8+RJAvv9Dn8rf564lFG1x0XE0btZAvq1XqSIvnqfyOuN1vmLadG7FgzsPSYxP4lXGK44FnqJLn05oldViyFgrNq/cLj9PIpHw/p1YPkMQAGKj4jFp1ki+rVdJlxfPX/Dqg7qXn5iPSSQSMtJfMc7qv3c3j1zcJx/uLgjCf+Ki42nUNJf726vXn42pVa8GlasZMsNtKgAV9MqjrKyEmroaC+y9Cq4QglDEiZ7MT1BXV5f/v0SS9STgwyda799n/6NZTU1N/v/Kysq5pvnhfplMhoqKCjKZDG9vb4KCgggKCmLv3r106NABgFKlSsnjP36alpmZiVSa/al2z549OXjwICYmJmzduhVXV9dsx2WynEM6Pkznw+t9eE5u+fvctYSi7VJoOI2bN6RazSoADBptScjx8/mO6d7fQt5zqaqmSvf+Flz+7U8y0jIYOs6KLn06A2DUqC6Nmjbg4tmvP+mVIBRHF0PDMGneiGo1qwIwdIwVZ46d/+KYj2VmZrJu1zIamtYHoOeArrx584Z7t+8roBSCULxdCsm6v1X95/5mPXogoR/fAz8Rc/3KLXq3sGZ4t3EM7zaOA9uDOBF0RjQwhf8XMVy2BCtTpgwpKSkkJyfz9u1bzp/P+4afm+DgYABu3LhBeno61atXp3Xr1uzevRuAhIQE+vfvT2xsbI5zW7duzf79+wF4+vQpf/75J02aNMkWM336dG7cuMGwYcOws7Pj9u3bQFbjViqVUqtWLVJSUrh+/ToAv/76K4aGhmhra38yz5/K36euJRQPz5NScJu+EO9fPDhwbid1jGuxzG0VDUzr439qa54xAD5uqylTVot9IX7sOrGZO9fusWvjXmQyGT+NdWD05OHsC/Fjvq8Tc35wISU575EAglBSJCc9x9luAb6bFnHovD91jWvj7baChqb1OXDaL8+Yz5k92YX5PnMJCt3NoFEDmDZ2tqKLIwjF0vNn/9zfNnpw4NwO6tavxbL5qzE2NWL3yS15xgiCkD+SzC+ddrQECAsLY/Xq1fj5Zd3wHRwcaNWqFbGxsRw4cAADAwPq1q1LxYoVmTp1KkZGRty7dy9brJWVVbZ0bGxsqFGjBjdu3EBJSQlXV1dMTU1JS0vDzc2Nu3fvIpVKmThxIpaWlgQEBBAeHs7ixYuBrPczXVxciImJAcDOzo6uXbtmu8bdu3dxcnJCJpOhqqqKs7MzJiYmeHp6cu7cOX755ReSkpJYuHAhr169oly5cri7u1O7dm35xD9mZlmTRvxbpk/l71PXyo+mBu2+9o9MEITPeJsphiwLQmFQVxLLyAhCYfkz9tNzqhQlvxtaKSTd1jEBCkk3P0QjUyhwjfRbF3YWBEEQBKFA5DaBmiAIBeNWfPF4XediJWuFpNs29oBC0s0PMVxWEARBEARBEARB+GpEI1MQSqiOXdsScHYHwRf24LPRM9v6YJ+LUS+lzgJfJw6G7iQwdBcLfJ1QL6We7dzK1Spx4e5x+UQkgiBkEXVPEApHx67tCDi7g8MX9rJs48Js69PmN8bAUI8zEcFoly8n31e7Xk38Dm3gwGk/9p/aTrvOZgovi/Bt+RaXMBGNzCLEwcGBgIAvGzu9atUqVq1alWO/o6Mj0dHRXytrxMfHM2HChFyPGRkZfbXrCAVDp4I2C1Y4M/07R/q1G0rU4xh+cp6S75iJ08eirKyMVedRWJmPQr2UOt9PGy0/V01djcVr5qOqJt5FEoQPibonCIVDp4I2Hv/Uq77thhD1OJoZzj9+UUz/wb3YFvQz+pX0sp3nvHgWAbuDse5ig/N0D3w2LvzkSgOCUFKIRuY3KiwsLNdFhP9X+vr6bNy48aulJxSutp3NuHX1Dk8inwKwZ1sAfax75DvmyqWr/Lx8C5mZmchkMu7c+AvDKgbyc50X2xO45wjPn4lZZQXhQ6LuCULhaNvZjJsf1Cv/bQH0se6Z7xhd/YpY9OrExKHTcqStrKxM2XJlANDU0uDNm7eKLIrwDZIp6FOYVAr5+iVaZmYmixcvJiQkBD09PaRSKa1atSIwMJBt27Yhk8lo2LAhrq6uqKurExwczLp165BIJDRu3JgFCxbI05JKpfz0009UqVIFbW1tEhISmDhxInZ2dmzZsgV/f38AAgICuHbtGqampoSEhPDs2TMSExMxNzfHwcEBiUTChg0bOHr0KFKplPbt2zNr1iyio6MZPXo0Z86cISoqilmzZpGRkYGpqWlhfX3C/4OBoR5xMfHy7fiYBMqU1UJTS4P0tIzPxlwMDZfvr1TFAJuJQ5lvnzUTsvXI/qioqHBgRxAT7cYWTIEEoZgQdU8QCkclQ/1P1CtN0tPSPxuTGJ/E9O8cck3bw9GbzfvXMPqH4VSoqIP9D8451jIXhJJG9GQWouPHj3P79m0OHz7MihUrePLkCa9evWLv3r34+/sTFBREhQoV2LRpE/Hx8SxatIjNmzdz5MgRpFIpoaGhQFZj1dnZGQMDA2bPns3EiRPR09Njw4YNdO/encTERJ48eQJAYGAgVlZZ0yRfuXKFFStWcPjwYa5du8bJkyc5d+4cN2/eZP/+/QQGBhIfH8+hQ4ey5XvBggVYWVkRFBREs2bNCvZLE74KJSUlcuvolslkXxTTwMSI7UHr2b15P6EnL2Dc2Ighoy1xny0WpRaE3Ii6JwiFQ6IkyXWEl0wm/aKYj6mpq7H0Zw+c7Nzp0rQfowdOwtXbAQNDvU+eIwgfy0SikE9hEo3MQhQeHk737t1RVVWlfPnydOzYkczMTB4/fsyQIUMYMGAAp0+f5uHDh1y9epVmzZphYJA1LMrb25uuXbsC4O/vz+HDh/n+++9zXEMikWBpacmhQ4eIiYnh2bNn8t7HLl26ULFiRdTU1Ojduze///47ly5d4vr161hZWWFpacnNmzd58OBBjnz36tULgP79+6OqKt79KW5io+LRNago39arpMuL5y94lfE63zG9BnZl496VLPdcy8YV2wDoP6QXmmU02XF4I/tPb0fPoCKL186nc48OBVQyQSjaRN0ThMIRGxWPnoGufPtTde9zMR+rW78WpUuXIvTkBQCuX7nJg3sPMWnWSAGlEL5VskzFfAqTGC5biCSS7E/MVFRUkEql9OrVC2dnZwDS09ORSqWEh4cjkfz3RCI5OVn+/02bNqVBgwZ4eHiwcuXKHNextLTk+++/R01NjQEDBsj3f/hSukwmQ1lZGalUypgxYxg3bhwAqampKCsr8/z582xp/ptviUSCkpJ4VlHcXAwNY9b8aVSrWZUnkU8ZOsaSM8fO5zumU/f2OHjOYOJQO25duys/x2ueL17zfOXbxy8fxOFH12wxglCSibonCIUjq17ZfVCvrD5R9/KO+diTyCi0ymrRpEVjIv64QdXqlaltVJM7N+4psjiCUOSJ1kEhatOmDUePHuXt27e8ePGC8+ezfpGdPHmSZ8+ekZmZiZubG9u2baNx48ZERESQmJgIwMKFCzl9+jQA9evXZ8KECdy/f58zZ84AyBuMAJUrV8bAwAB/f/9sjczz58/z8uVL3rx5w5EjR+jYsSOtW7cmKCiI9PR03r9/z5QpUzh+/Hi2fLdt21Y+hPbEiRO8efNGsV+U8NUlJz3H2W4Byzct5NB5f+oa18bbbSUNTeuz//T2PGMA7F2nIkHC/GVz2X96O/tPb8dpkX1hFkkQigVR9wShcPxbr3w3LfqgXq2goWl9Dpz2yzMmLy9T07AbNwdHjxkcDNnJ8k2LcLNfxNPHX2+Gf+HbJ0OikE9hkmR+zSlIhS+2fPlyjh49SsWKFdHQ0KB3795IpVL5xD/GxsYsXLgQdXV1jh07xtq1a5HJZDRp0oT58+ezdu1aAKZOnUpYWBgODg4cPnwYX19fzp07xy+//ELVqlXZt28fJ06ckM8QGxAQgL+/P0pKSjx//pz+/fszZUrWFPlr166Vv/fZoUMH5s6dm23in/j4eGbNmkVKSgqNGjXi2LFj/Pnnn/kucyP91l//ixQEQRCEIigT8WeWIBSWW/FhhZ2FfDmjP0Qh6VrE71VIuvkhGpklwPv375k9ezY9e/ake/fuQFYjMzw8nMWLFxd4fkQjUxAEQSgpRCNTEApPcWlkntYfqpB0u8TvyXfsv6tYvH//njFjxjBy5Mhc40JCQnB3d5ePnvwU8U7mNy4zM5MOHTrQtm1b+URBhU3ccAWh4KlIxMLgglAYMqTilRJBEPJW2GtaxsfHs3z5cgICAlBTU2PYsGGYmZlRp06dbHFJSUl4eeVvFnPRyPzGSSQSLl26lGO/lZWVfCkTQRAEQRAEQRC+LampqaSmpubYX7ZsWcqWLSvfvnjxIq1bt0ZbWxuAHj16cOzYMWxtbbOd5+zsjK2tLT4+Pp+9tpj4RxBKqI5d2xFwdgeHL+xl2caFaGppfnGMgaEeZyKC0S5fTr6vdr2a+B3awIHTfuw/tZ12nc0UXhZBKE46dG3LvjPbCfptN94bPdDU0sh3jFYZTZb+4smBkB0EnNvJONtROc4dOLwPK7cvUXg5BKE46NytPcEh/hy/dICVm7zQyuVe96kYJSUlnDxmcuziAU6FBzJ8jHWOcweN6M/PO5Zn27d6yxJOhQdy6OwuDp3dxdwFMxRTOOGboah1Mrdt20aXLl1yfLZt25bt+gkJCejqfrB8j54e8fHx2WK2b99OgwYN5Eshfo5oZH6BsLAwbGxsALCxsSEsLP/jvE+fPs2KFVkzlFlYWBAVFZUj5t80b9y4gZOT09fJ9Gfs3buXDh064OXlxcqVK/njjz+yHd+3bx8ODg7y7UmTJjFgwAAGDBhAv379MDIy4saNGwWSV+Hr0amgjccKZ6Z/50jfdkOIehzNDOcfvyim/+BebAv6Gf1K2Recdl48i4DdwVh3scF5ugc+GxdmWy5HEEoynQrauPs6MXP8XAa0H0704xjscql7n4qZMmci8TEJWHcexcie4xk8xhKT5lnr8ZXVLoOz1yxmL5iebckrQSipylfQZvEKV2y/m0WPNtY8fRSF/byp+Y4ZNsaaGrWr0afDEKy62TDmhxGYNG0IQDntsrh7O+LkaZ+jvjVpYcKIft/T33wE/c1HsHDesoIpsCB8ZMyYMZw+fTrHZ8yYMdniZDJZtn/HmZmZ2bb/+usvTpw4wY8/Zr9f5UU0MgtIly5dsLOzy1ds48aN8fT0VHCOshw+fJhFixYxZ84cLl++LF/25M2bNyxdupSFCxdmi1+/fj1BQUEEBQXRtWtXhgwZQuPGjQskr8LX07azGTev3uFJ5FMA/LcF0Me6Z75jdPUrYtGrExOHTsuRtrKyMmXLlQFAU0uDN2/eKrIoglCstOnUipsRd3gSmfWgce+2AHpbdc93jJfzcpbNXw1ARb0KqKmpkvYyDYAe/buQGJ8kPy4IJV37zm24EXGbxw+z7mO7tu6n/6Be+Y7p3rszB3YHI5VKSX3xkiMHjzNgcG8Aeg/oRkJcEl6uvtnSq1LNEE0tDTyXz+Nw6B4Wr3SlnHZZBCEvMgV9ypYtS5UqVXJ8PhwqC2BgYCBfJhEgMTERPb3/OhGOHTtGYmIi1tbWTJw4kYSEBEaMGJFnmUrEO5mzZs2iZcuWDBmSNT2wjY0NU6dOZfny5bx+/ZrU1FQcHR3p2rUrDg4OpKSk8PjxY2bNmoWamhqLFi1CXV2dmjVrZkt37969LFq0CABHR0fMzMxYtWoVkLWkCGT1Wm7fvp3w8PAcs7m+ffsWJycnbt68SeXKlXn+/DmQ1WO6evVq/Pz8sLGxoXHjxly5coXk5GScnZ3p1KkTcXFx2Nvb8+LFC+rVq8fly5c5d+4cly5dwtvbG4By5crh4+ND+fLl2bhxI/v27UNHR4fatWtTqVIlJBIJN27cYP78+UyZMoWbN2/i7OzM6tWrSUxMRCaTMWvWLK5fv57jO3348CGBgYEEBwd/5Z+WUBAqGeoTF/PfMIj4mATKlNVCU0uT9LT0z8Ykxicx/TuHHOkCeDh6s3n/Gkb/MJwKFXWw/8FZ/vBCEEo6A0N94qM/rFeJ/9QrDdLTMvIVI5VKWbjala59O3Pm6DkePXgCwL7tgQD0H9q74AokCEWYQWV9YqPj5Ntx/9zHtLQ0SfvnXpdXjEFlA+I+PBabQP0GdQHYve0AAFbD+mW7ZoWK5bkYGs6Cud4kxCXi5DmTRStc+XHMTIWVUyj+Cnvin7Zt27Jq1SqSk5MpXbo0J06cYMGCBfLj06ZNY9q0rI6FqKgoRo8eza5du/JMs0T0ZFpbWxMUFARAdHQ0ycnJ7NixAw8PDw4ePIiHh4d8KCuAtrY2R48epX379jg4OLBy5UoCAgIoVapUtnQ1NDQIDAxk8eLFzJo1i7dvv6zHxs8va/Hfo0eP4uzszJMnT3KNe/fuHXv27MHR0VGeT09PT3r16kVwcDA9e/aUj5teu3Ytbm5uBAQE0LZtW27fvs3169fZv38/AQEBbN68WT681dbWlkaNGuHh4cHAgQPl/29kZET79u2ZPXt2jjL/a+3atYwfPx4tLa0vKrNQNEiUJOS2epFMJv2imI+pqaux9GcPnOzc6dK0H6MHTsLV2wEDQ71PniMIJYlESZLrDNsymeyLYubazqdTg96U0y7LDzPHKSazglDMKSlJyG2hPukH97G8Yj4+JkGCVJZ3c+DanzeZMtaeuJh4ZDIZq5ZsoHO39qiqloh+HaGY0tfX56effmL06NEMHDiQvn37YmJiwoQJE/7n1+JKRCPTzMyMhIQEoqKiCAwMZMCAAXh7e3P//n3WrFnDli1bSE9Pl8ebmJgAcO/ePfT09KhduzYAlpaW2dIdNGgQAPXr16dChQo8fPjwi/IVHh5Or15ZQzJq1KhB06ZNc43r0KEDAHXr1iUlJQWACxcuMGDAAAC6desm7/bu0qULtra2uLu706BBA9q3b094eDidO3dGS0sLTU1N+vTp80X5/NiLFy+4cOECgwcP/n+lIxSe2Kh49Aw+eMG7ki4vnr/gVcbrL4r5WN36tShduhShJy8AcP3KTR7ce4hJs0YKKIUgFD9x0fHo6n9cr1Kz1au8Ytp2NkNXvyIArzJecTTwJMaNjQquAIJQjMRExaFnUFG+rV9Jl5SP7mN5xWQd+6AuGlTMNsInNy1aN8GiR0f5tkQiIVMmQyot7L4qoShT1MQ/X6Jfv34cPnyY48ePM2HCBAA2btyY47W4KlWqfHaNTCghjUyJRMLAgQM5cuQIR48eZcCAAYwYMYLr16/TqFEjJk2alC3+3947iSR7T87Hk5d8uC2TyVBRUclxzrt37/LM14exKiq5P+VSV1eXx3947dx6mcaOHYufnx/VqlXD29ubdevW5eiN/NR18is0NJSOHTvK8yUUPxdDwzBp3ohqNasCMHSMFWeOnf/imI89iYxCq6wWTVpk/UKqWr0ytY1qcufGPQWUQhCKn0uh4Zg0b0i1mlUAGDx6ICHHz+c7pnt/CybN/A4AVTVVuve3IPy3KwVYAkEoPn4L+Z0mzRtTvVbWfWz42EGcPhaa75jTx0IZNKI/ysrKlCmrRV/LHpw6GpLnNTU0NXBZNFv+Hub3U2w4Fnw620gEQSgJSkQjE7LWhfT396dSpUqoq6vz6NEj7Ozs6NixI6dPn871nTEjIyOSkpK4e/cuAEeOHMl2/N/3EW/cuEF6ejrVq1dHR0eHBw8eAHD9+vVsL9F+rE2bNgQHByOTyYiOjubPP//Md3n+PReyGn3/roEzePBg0tPTGTt2LGPHjuX27du0adOGkJAQUlNTefv2LSdOnMg1TWVl5Xy9OxcREUGLFi3ynVeh6ElOeo6z3QJ8Ny3i0Hl/6hrXxtttBQ1N63PgtF+eMXl5mZqG3bg5OHrM4GDITpZvWoSb/SKePo4uiGIJQpGXnPQcl+meLP3Fk4PndlHXuDZL3VbSwLQ+e05tzTMGwMdtFVpltTgQsgP/E1u4c+0eOzfuLcQSCULRlZz0HAe7+azatIRjF/ZjZFyHRS7LaWRqzKGzu/KMAdi1ZT9PHkURHLKbgJN+7NsZRPjFvP9WO3f6Its3+rPnyGaOXzpAtZpVmO+Qv8XrhZJLJlHMpzCVmAHilSpVolKlSlhaWqKtrc2gQYPo06cPKioqtG7dmtevX5ORkZHtHFVVVZYtW8asWbNQUVGhQYMG2Y5nZGQwcOBAlJSU8PHxQVVVld69e3P8+HF69+5Nw4YNc5zzoREjRnD//n169epF5cqVqVevXr7L4+TkxJw5c9i7dy/169eXD5edMWMGDg4OqKiooKGhgYeHBzVq1GDSpEmMGDGC0qVLf/I9yg4dOuDq6oqXlxfNmjX75LWfPn1K586d851XoWg6f/oi509fzLbvRUoq1l1s8oz5WEP97Otghl+4wtCe4h0xQfiU305f4rfTl7Ltu51yl6Fdx+YZA1kPcuZMcskz/UN7fuXQnl+/Sl4FobgLPXWB0FMXsu17kZJKf/MRecYASKVSPJ3zXnQ+wD+YAP/skyBuXreDzet2/D9yLQjFnyQztzGX35jMzEwSEhKwsbHh8OHDqKmpFXaW/t+2b99O27ZtqVOnDrdu3WLevHkEBATk69yPZ8AtaB83SgRBUDwViVirVBAKQ4b0TWFnQRBKrPuJxeN1giCDvJcD+V8NiMt7BlhFKhE9mcePH8fNzQ03N7dvooEJUL16dWbMmIGSkhLq6urZphku6mTf/nMNQShy3ma+L+wsCEKJpCwpMW8mCYLwP/oW/zIuET2ZQtFirNeqsLMgCIIgCAUit+VoBEEoGHcTLhd2FvIlUEE9mQMLsSdTPF4ThBKqU9d2BIbs5NeL+1j+yyI0tTS/OMbAUI+Qa4fRLl9Ovq9z9/ZcuneSgDM75B8NTQ2Fl0cQigtR9wSh8HTq2o6gkF0cvbgf3zzqX14xBob6hF47kq3+AbTrbMbBMzsVmn/h2yRT0KcwiUbmVxIWFoaNjc3nA4uQlStX8scff/xP5zo6OtKlSxcOHz5c7MotgE4FbTxXzMNunAO92w4m6nE0M+dN+aKYAUN64xe0Af1KetnOa9rShC1rd2JlMUr+yUjPPqmWIJRUou4JQuHRqaDNwhUuTBs3h15tB/H0cTQz59l+UcyAIb3ZEfRztvqnXkodO4dJLNuwMMdyd4JQUolGZgl2+fLlfC1ZkpuDBw9y9OhR+vbtS3h4+FfOmaBo7TqbcTPiNo8jnwKwe+sB+lr3zHeMrn5FuvTqxISh03Kk3bSlCa07tODg2Z34HdpAi9ZNFVwaQSg+RN0ThMLTrnNrbnxQt/y3HqBfjvr36Rg9/Yp06dWZ7z+qf+3NW1NaozQOU+cXQCmEb5FMIlHIpzCViIl/Csrz588ZP348CQkJmJiY4Orqyt69ewkKCuLVq1eoqqri4+NDrVq18PLy4sKFCygpKdG1a1dsbW1JT0/H3d2d+/fvI5VKmTBhAn379iUgIICQkBBSUlJISEhg2LBhREdH8/vvv6Otrc0vv/yCuro6gYGBbNu2DZlMRsOGDXF1dUVdXZ327dvTo0cPrly5grKyMr6+vly5coWbN2/i7OzM6tWruXjxIgcPHkRJSQkTExPc3d158+YNzs7O3Lhxg2rVqvH27VsmT57Mli1byMzMZPDgwTRtmvVHzODBg9m3b18h/wSE/DIw1Cc2JkG+HR+TQJmyWmhqaZKelv7ZmMT4JKaNm5Nr2inPX3D4wHFOHD5DMzNT1mxbykDzkcTHJuQaLwgliah7glB4KhnqExcTL9+Oy6X+5RWTEJ/EtHGzc6R7+mgop4+G0qrtp5d/E4S8fItvbouezK8oKiqKefPmcejQIdLT09m9ezenTp3Cz8+Pw4cP07lzZ3bu3El0dDTnzp3j0KFD7N69mwcPHvDmzRvWrVtHw4YNCQgIYOfOnaxfv56nT7OepN24cYO1a9eyadMmFi1aRMeOHQkOzlqX6fz589y/f5+9e/fi7+9PUFAQFSpUYNOmTQAkJibSpk0bAgMDadmyJTt37mTgwIE0atQIDw8P6tSpw88//8yBAwcICAjg3bt3xMfHs337dmQyGUePHsXBwYErV7KmgV6/fj0AQUFBuLm5AYgGZjGjpKQEucz5JZNJvygmN9PGzeHE4TMA/Bl2jauXr9O2k5jsSRBA1D1BKExKShJym+8ye/37fIwgCJ8nejK/ohYtWlCjRg0A+vXrR0BAAD4+Phw5coRHjx5x/vx5jI2N0dfXR11dnWHDhmFubo69vT3q6upcvHiR169fc+DAAQAyMjK4f/8+AM2aNUNLSwstLS0A2rRpA0DlypVJTU0lLCyMx48fM2TIEADevXtHgwYN5Hnr0KEDAHXr1s3xHqaysjJNmzZl0KBBdOnShXHjxqGvr8/ly5cZOnQoEomEWrVq0bJlS8V9eUKBio2Ow6RZQ/m2fiVdUp6/4FXG6y+K+ViZsloMHzeIDSu2yvdJJBLevxfLZwgCiLonCIUpJjoek2aN5Nu51a38xAjC11bYk/QogujJ/IpUVP5rs2dmZpKamsrQoUN5+fIlHTt2xNLSkszMTFRUVNi3bx92dnakpKQwbNgwIiMjkclkeHt7ExQURFBQEHv37pU3DlVVVT95LQCpVEqvXr3k5+7btw8XFxf5cXV1dSDrj47cntCtXbsWNzc3MjMz+f777wkPD6dUqVJ5XlMovi6EhGHaohHVa1YFYOgYK84cO/fFMR9LT8tgxHeD6NbXHADjRvVo3LQB589cUkApBKH4EXVPEArPhZDfs9WtYWOsc6l/n48RBOHzRCPzK7py5QoxMTHIZDICAwPp2LEj1atXZ+zYsTRu3JhTp04hlUq5ffs2o0aNomXLlsyZM4fatWsTGRlJ69at2b17NwAJCQn079+f2NjYfF3bzMyMkydP8uzZMzIzM3Fzc2Pbtm15nqOsrIxUKiU5OZnevXtTr1497OzsaNeuHffu3aNdu3YEBQUhk8mIjY395Ey0ysrK4ml5MZOc9BynaQvw3byYw7/toZ5xHZa4rqChqTEBZ3bkGZMXmUzGlNGz+O7HURwK3Y3nShdmTHQiJflFQRRLEIo8UfcEofAkJz1n7jR3VmxezJHf9lLPuDZerr40MjWWLz3yqRhBUCSZRDGfwiTJzK1bS/hiYWFh+Pr6oq6uTmJiIq1bt2b69OlMmzaN+Ph4MjMzadmyJffv32f37t14eXlx9uxZSpcuTbNmzXB0dOT169e4ublx9+5dpFIpEydOxNLSkoCAAMLDw1m8eDEARkZG3Lt3DwAHBwdatWqFlZUV+/btk0/8Y2xszMKFC1FXV88W/2FamzZtwt/fHy8vL65fv86ePXsoXbo0NWvWZMGCBairq+Pl5cXFixfR1dUlPj4eV1dXzMzMsqU5depUHj58SEBAgLzHNC/GeuIdIUEQBKFkyPwmp/QQhOLhbsLlws5Cvuw0HKWQdEfG7FBIuvkhGplCvtnY2GBra4uZmdn/Kx3RyBSEwiH+2BWEwiGhkLsUBKGEupNQPJbZ+xYbmeIlO0EQhBJANDAFoXCIBqYgCJ/zLd6hRSNTyDc/P7/CzoIgCIIgCIIgCEWcmPhHEEqoTl3bERiyk18v7mP5L4vQ1NL84hgDQz1Crh1Gu3y5HOdWrmbIpXsnaWhqrLAyCEJx1KlrO4JCdnH04n5886h7ecUYGOoTeu1IjrrXrrOZfAITQRCyU9R9r1W75uw7sY2DZ3fi/+smGjdt8HGygpCnb3HiH9HILIZu3LiBk5PT/3Tu7t275TPYfiggIAAHB4dPnnf69GlWrMh7dkOh+NCpoI3ninnYjXOgd9vBRD2OZua8KV8UM2BIb/yCNqBfSS9H+mrqaixZOx9VNdUcxwShJNOpoM3CFS5MGzeHXm0H8fRxNDPn2X5RzIAhvdkR9HO2uqdeSh07h0ks27AQZWXlAiuPIBQXirrvqaqqsGyDJy4zPLE0H8n65VvwWjO/wMolCEWVaGQWQ40bN8bT0/N/Onf48OEMHz78i8/r0qULdnZ2/9M1haKnXWczbkbc5nHkUwB2bz1AX+ue+Y7R1a9Il16dmDB0Wq7puyyeTaD/YVKepSiuEIJQDLXr3JobH9Qr/60H6Jej7n06Rk+/Il16deb7j+pee/PWlNYojcNU8cetIORGUfe9d+/e09m0D3du/gVAleqGpDwXSwcJX0amoE9hEu9kFpKwsDC8vb2RyWRUrlwZDQ0N7t+/j1QqZcKECfTt25d3797h6urKlStX0NfXRyKR8OOPPwKwevVq/Pz8iIyMxMXFhZSUFDQ0NHBycsLExAQHBwe0tLS4desW8fHxTJkyBWtra1atWgVkLTsSGBjIunXr0NLSkuchKiqKKVP+e2oXGRmJnZ0dOjo68qVPLCws6NmzJxcvXgRg4cKFNGgghoYUJwaG+sTGJMi342MSKFNWC00tTdLT0j8bkxifxLRxc3JNe9DIAaioqrBvRxA/TB+n2IIIQjFTyVCfuJh4+XZcLnUvr5iE+CSmjZudI93TR0M5fTSUVm2bKb4QglAMKfK+9/69lAq65Tlwajs65bWZMfF/G20mlFxi4h/hq3r06BFnz57l559/Rk9PDy8vL9LS0hg2bBimpqaEhITw6tUrjh07RkxMDP369cuRxqxZs5g4cSLdu3cnIiICOzs7jh8/DkBcXBy7du3ir7/+YvTo0VhbW8vPi4+PZ+nSpQQGBqKtrc0PP/yAhoYGVapUISgoCICTJ0+yfv16Ro0axZEjR7JdV0NDg8DAQM6cOcOcOXMIDg5W4DclfG1KSkqQy+pFMpn0i2I+1qCxEUPHWGEzYOLXyaggfGOUlCTktnJY9rr3+RhBEL6Mou57/3qWmExn0740aGzE5gNr+PteJI8ePvn/ZVoQijExXLYQ1axZkzJlynDx4kX8/f0ZMGAAI0eOJCMjg/v373PhwgX69euHRCKhcuXKtGnTJtv56enpPHnyhO7duwPQpEkTypUrx8OHDwFo164dEomEevXqkZKSku3cq1ev0rRpUypWrIiKikqOBuzdu3dZvHgxq1atQl1dPUfehwwZAoCFhQXx8fEkJyd/ra9FKACx0XHo6leUb+tX0iXl+QteZbz+opiPDRjSG60ymuw6somAMzvQNdDFe5075j06KKYgglDMxETHo6evK9/OrV7lJ0YQhC+jqPueVhlNuvbuLN++feMe927dp26D2l+3AMI3TUz8I3xVpUqVAkAmk+Ht7U1QUBBBQUHs3buXDh06oKysjEz26RHVuT3pzszMRCrNeuL2b+NQIsn5r0wiyf6kXEXlv07t5ORkpk2bxsKFCzE0NMz12h/Gy2QyMdFEMXMhJAzTFo2oXrMqAEPHWHHm2LkvjvnYonnL6dVmEFYWo7CyGEViXCKzJrtw9vh5xRREEIqZCyG/Z6tXw8ZY51L3Ph8jCMKXUdR9TyaT4eHrTNNWJgDUMapFzbo1uH7llgJKIQjFh2hkFgGtW7eWz/iakJBA//79iY2NpW3btvz6669kZmYSHx9PeHh4tgajlpYWVapU4cSJEwBERESQlJRE3bp1P3vN5s2bExERQXx8PDKZjF9//RWAd+/eYWdnh42NDWZmZp88/9/hsydPnqR27dqUK5dzCQuh6EpOeo7TtAX4bl7M4d/2UM+4DktcV9DQ1JiAMzvyjBEE4X+XnPScudPcWbF5MUd+20s949p4ufrSyNRYvvTIp2IEQfjfKeq+l5H+iqljZuG4YAYBZ3bg4evMrEnziI9NyPM8QfjQtzjxjyQzt+4wQeHCwsLkk/ekpaXh5ubG3bt3kUqlTJw4EUtLS969e4e7uztXr15FV1eX5ORkFixYwKtXr+Tn/v3337i5uZGSkoKqqirOzs40a9YMBwcHWrVqhZWVFQBGRkbcu3cv28Q/x44dY8WKFZQuXZo6deqgpKREu3btcHR0pH79+rx//57MzEzatm1L3bp1s038Y2pqysOHDyldujSLFi2iZs2a+S67sV4rhXyngiB8WuY3Oa2AIBR9Egp5zJoglGB3EsILOwv58nOVUQpJ94eoHQpJNz9EI7MICwkJITMzE3Nzc16+fMnAgQM5cOAA2trahZovCwsLtm/fTpUqVf6n8+vptvjKORIEQRAEQRCE7P5K/KOws5Av32IjU8wuW4TVrl2b2bNn4+vrC8C0adMKvYEpCIIgCIIgCMLXk/kNDngQjcwirGrVqvJ3NYuSM2fOFHYWhP9B527tmOFki5q6Gvdu32eu3QL52mCfi1FSUsLBfTodLdqirKLMpjU78N92AIDa9WriscwJDc3SZGbC0gWr+O3s79nSHfPDcAaPHEjfjkMLrLyCUFQVZl0UhJJGUfWtnHZZ5i2aRR2jWpQqpc665ZsJ2pc1v8WqLUuo37AuGekZAPz+2xUWzVtWsAUXhEImJv75AmFhYdjY2ABgY2NDWFhYvs89ffo0K1ZkvTxuYWFBVFRUjph/07xx4wZOTgWzkO+/M9l6eXmxcuVK/vgja1jB33//zciRIxkwYABDhw7lzp07ALx9+xYPDw8GDhxInz59+O233wokn8L/j04FbRatcGXqd7Pp2caap4+isZ9nm++YYWOsqFm7On06DMW622jG/jAck6YNAXBb4sD+XYcYYD4SRzt3VvyyONtsw81amfK97eiCK6wgFGGFWRcFoaRRZH1bvMqNuJgEBlqMZIz1jzgvtEe/kh4ATVo0ZmS/CQwwH8kA85GigSl81rc48Y9oZBaQLl26YGdnl6/Yxo0b4+npqeAcZTl8+DCLFi1izpw5XL58Wb78ibOzMxMmTCAoKIjp06czZ84cAH755ReeP3/OwYMH8fX1xdHRMdelVISipX3n1tyIuM3jh08B2L11P/0H9cp3TLfe5hzYfQipVErqi5ccOXiC/oOzjikrK1GuXBkANLU0ePP6jTzNCrrlcVk8myVuYlZaQYDCq4uCUBIpqr6V0y5Lu06tWL10AwDxsQkM7jGWFykvqFLNEE0tDTyWOxMc6s+ilS6U0y5bgKUWiqNvsZFZIobLzpo1i5YtWzJkyBAgq8dw6tSpLF++nNevX5OamoqjoyNdu3bFwcGBlJQUHj9+zKxZs1BTU2PRokWoq6vnmEF17969LFq0CABHR0fMzMyyzd4K/02SEx4eLp+d9V9v377FycmJmzdvUrlyZZ4/fw5kn3nWxsaGxo0bc+XKFZKTk3F2dqZTp07ExcVhb2/PixcvqFevHpcvX+bcuXNcunQJb29vAMqVK4ePjw/ly5dn48aN7Nu3Dx0dHWrXrk2lSpWQSCTcuHGD+fPnM2XKFG7evImzszOrV69m8ODBdOjQAciamTY2NhaAo0eP4u3tjUQioW7dumzZsoXMzMxc1+IUio5KlfWJjY6Xb8fFJFCmrBaaWpryYUN5xeQ4FhuPUYM6AMyf48X2gPWMnTSC8hXLM2PiXKRSKUpKSixb78GS+St5/+59AZVUEIq2wqiLglBSKaq+Va9ZlcT4JMZNHkXHLm1RU1Nl85odPHr4hPIVdbgUGs6CuUtJiEvEyXMmi1a48OMY+4IruCAUASWikWltbc2qVasYMmQI0dHRJCcns2PHDjw8PKhduzaXLl1i4cKFdO3aFQBtbW3Wr1/P27dvsbCwYNu2bdSuXTvHEFYNDQ0CAwO5e/cuEydO5NSpU1+ULz8/PyCr4fbo0SP69++fa9y7d+/Ys2cPZ86cYcWKFXTq1AlPT0969erFyJEjOXnyJIcPHwZg7dq1uLm5YWJiwsaNG7l9+zZly5Zl//79BAQEIJFIGDZsGJUqVcLW1pawsDBsbW0xMzPjwIED2NraYmRkhJGRkfz6K1eulH83jx8/5vLly7i7uyOVSvnpp5+oU6fOF5VbKHhKSkq59jjLZNJ8xUiUJNmOSZAglclQU1fDd+Mi5kx1I+Tkb5g2b8TPO5Zz/eptbL4fyuVLV7kYGkarts0VUzBBKGYKoy7GxcTnSEsQSgJF1TcVVRWq1qhC2ss0hvcZT7WaVdgV/AuPHj7h+p+3mDJ2lvycVUs2cOHWcVRVVXgnHrgKn/AtjgksEcNlzczMSEhIICoqisDAQAYMGIC3tzf3799nzZo1bNmyhfT0/14CNzExAeDevXvo6elRu3ZtACwtLbOlO2jQIADq169PhQoVePjw4RflKzw8nF69soZk1KhRg6ZNm+Ya92+PYt26dUlJSQHgwoULDBgwAIBu3bpRtmzWUIwuXbpga2uLu7s7DRo0oH379oSHh9O5c2e0tLTQ1NSkT58++cpfZmYmXl5eXLt2jblz5wIglUqJi4tj586dzJ8/H3t7e16+fPlF5RYKXkxUHHoGuvJt/Uq6pDx/wauM1/mKiY2KQ/+DY3oGusTFJFCvfm1KlS5FyMmsd3OvXbnJ/XsPMW3eiAFDetO9jzlBZ3fi6etMtRqVCTq7swBKKwhFV2HURUEoqRRV3xLiEgEI2BUMwJPIKK6ERWDSrCEtWjfBokdH+TkSCWTKZEilhT14URAKVoloZEokEgYOHMiRI0c4evQoAwYMYMSIEVy/fp1GjRoxadKkbPGlSpWSn/fhE6yPJ1D4cFsmk6GiopLjnHfv3uWZrw9jVVRy71hWV1eXx3947dyevI0dOxY/Pz+qVauGt7c369atk5fnc9f50Pv377G3t+fGjRts376dMmWy3vOpWLEiffr0QSKRUL9+fQwMDIiMjPxsekLh+i3kd5o0b0T1WlUBGD7WmtPHQvMdc/rYOaxH9EdZWZkyZbXoY9mdU0dDeBz5lDJltWjaMuvBTNUalalTrya3b9ylfaOe9DcfwQDzkThN9+DJo2gGmI8swFILQtFTGHVREEoqRdW3qCcx3Lx2B8thfYGs+QeatTThZsQdNDQ1mLdolvw9zPFTRnMs+DQymWhkCp8mkyjmU5hKxHBZACsrK0aMGEGdOnVQV1fn0aNH7Nq1CzU1NZYuXZrreytGRkYkJSVx9+5d6tevz5EjR7IdDw4OplGjRty4cYP09HSqV6+Ojo6OfNbZ69evk5iY+Mk8tWnThuDgYMzNzYmNjeXPP//Md3n+PXfEiBGEhoaSmpoKwODBg5k/fz5jx45FW1ub06dPM336dPz8/JgyZQqlSpXixIkT8t7RDykrK8u/By8vL9LS0ti8eTNqamryGHNzc3799VcaNGjA06dPiY2NzfGuqlD0JCc9x9HOnVWbvFBVU+XJoyhmT3Glkakxnr7ODDAf+ckYgF1b9lO1RmUOhexCVU0V/20BXL6Y9e91yhh7nD1noqaujlQqZd5MT54+ii7M4gpCkSXqoiAUHEXXN1evOQwfa42SkhKrl/7CjYjbAPht9Mf/yCYkSkr8decBzj95FNp3IAiFpcQ0MitVqkSlSpWwtLREW1ubQYMG0adPH1RUVGjdujWvX78mIyMj2zmqqqosW7aMWbNmoaKiQoMGDbIdz8jIYODAgSgpKeHj44Oqqiq9e/fm+PHj9O7dm4YNG+Y450MjRozg/v379OrVi8qVK1OvXr18l8fJyYk5c+awd+9e6tevLx8uO2PGDBwcHFBRUUFDQwMPDw9q1KjBpEmTGDFiBKVLl0ZLSyvXNDt06ICrqyuenp7s3LmTKlWqMHjwYPnxoKAg7O3tcXd3lw+59fDwkPdyCkVb6KkLhJ66kG3fi5TUbL2LucVA1jDphc65T8EeduEK1t3H5Hnt8ItXxBqZgvCPwqyLglDSKKq+xUbHM2nUjFyPbV63k83rxOshQv59i/3ckswSsP5EZmYmCQkJ2NjYcPjw4Ww9c8XV9u3badu2LXXq1OHWrVvMmzePgICAfJ378Qy4Ba2ebotCua4gCIIgCIJQcvyV+EdhZyFffKqNUki6M5/sUEi6+VEiejKPHz+Om5sbbm5u30QDE6B69erMmDEDJSUl1NXVWbBgQWFnSRCEIuyd7NPvhwuCoDiaKqULOwuCIAgFrkT0ZApFi+jJFISCJxqZglA4RCNTEArPzfjfCzsL+bJUQT2Z9oXYk1kiZpcVBCGnzt3acShkN8cuHWDFpsVoamnmO0ZJSYm5HjM4dnE/J8MPMmyMNQC169Uk6OxO+Sc41J+/Ev+gex/zAi2bIBR15t06cPTcPk6HBbFmszdaZXLWv/zEVTLU59LNk+iU15bvM2nakH2/buVIyB6Ont/PwMH5W7ZKEL51Hbu2JeDsDoIv7MFnoyeaWhr5jlEvpc4CXycOhu4kMHQXC3ydUC+VNft/oybG+AVvYP/p7QSE7KCvdc8CLZcgFEWikSn8vzx9+lS+hqZQfOhU0GbRClemfjebnm2sefooGvt5tvmOGTbGipq1q9Onw1Csu41m7A/DMWnakL//imSA+Uj550LI7wQfOMaJI2cLo5iCUCSVr6DDklXuTB47ky5mA3jyOJrZLnZfHGc1tC97Dm/BoJJetvPWbvXB12sdfToPZdzQH3FaYE+NWtUUXi5BKMp0KmizYIUz079zpF+7oUQ9juEn5yn5jpk4fSzKyspYdR6Flfko1Eup8/200QAs37SINd4bGdRlNJOH/8Qs92lUq1m1wMsoFF/f4hImopEp/L/ExMTw9OnTws6G8IXad27NjYjbPH6Y9bPbvXU//Qf1yndMt97mHNh9CKlUSuqLlxw5eIL+g7Of36J1E3r064KL/aICKJEgFB8dzNtw/epNHj18AsCOzXsZMKj3F8XpGejSrbcFYwZPznaOmroaK5f8zIXQrKW04mISSH72HANDfUUWSRCKvLadzbh19Q5PIrPuaXu2BdDHuke+Y65cusrPy7eQmZmJTCbjzo2/MKxigJq6Gut8NvH7ucsAxMcm8jwpBX1D3QIsnVDcyRT0KUwlYuKfb0lYWBje3t7IZDKqVKmCqqoq9+7dQyKRMH78eAYOHIhMJmPhwoVcunQJiURC//79mThxImFhYaxfvx5VVVWioqKwsLBAQ0ODU6dOAbBhwwYqVqxIcHAw69atQyKR0LhxYxYsWMD69euJj4/n8ePHREdHM3jwYCZPnoyHhwdRUVHMnz8fV1fXQv52hPyqVFmf2Oh4+XZcTAJlymqhqaVJelr6Z2NyHIuNx6hBnWzXmO1qx/KFa+XpCYKQpVJlg4/qVjxly5ZBq4wmaS/T8xWXEJfI5DE5l094++Yte3celG8PH22NppYGV/+4rqDSCELxYGCoR1zMf/UpXn5P0yA9LeOzMRdDw+X7K1UxwGbiUObbL+btm7cE7AqWHxtkMwBNLQ2uX7lVAKUShKJLNDKLoUePHnH27FnWrVvH27dvOXz4MMnJyQwePJj69etz5coVYmNjOXToEG/fvsXGxoZ69epRunRprl27xpEjR9DW1qZt27bMmTOHgIAAHB0dOXLkCD179mTRokUEBARgYGDArFmzCA0NBeDevXvs3LmTly9f0rVrV0aOHImzszOrV68WDcxiRklJidzm/JLJpPmKkShJsh2TIEEq+++ZWdOWJpSvoEPwgWNfOeeCUPwpKUnIJGfdkkpl/1Pcp0yy+45xE0cwdsiPvHn95n/LrCB8I7LuaTn3yz64d+UnpoGJESu2eLF7835CT2ZfW3P8VBtGTRjKpGHTRZ0Tvsi3OAuraGQWQzVr1qRMmTL8/vvvLFy4EIDy5cvTpUsXwsPD+eOPP7C0tERZWZnSpUvTr18/Ll26hIWFBfXq1aNSpUoA6Ojo0KZNGwAMDQ1JTU3l6tWrNGvWDAMDAwC8vb0BuHPnDmZmZqipqVGhQgW0tbV5+fJlIZRe+BpiouIwadZIvq1fSZeU5y94lfE6XzGxUXHoG/w3FEjPQJe4mAT5du+B3QjceyTXRqoglEQ/OfxI156dANAqo8W9O/flxwwq6f1Tt15lOycmKo4mzRt/Nu5jamqqeK9eQF2jWlj1HE3005ivWBJBKJ5io+Jp3KyhfFuvki4vPrrvfS6m18CuOC+ehedcH34NOCGPU1VTxXPlPGrXq8nIPhOIeRpbACUShKJNvJNZDJUqVQogxx/wmZmZSKXSbE/cPtwPoKqqmu2YsrJytm0VFRUkkv/eFE5OTiY5ORkAdXV1+X6JRCIaEMXYbyG/06R5I6rXypqYYPhYa04fC813zOlj57Ae0R9lZWXKlNWij2V3Th0NkZ/bqm0zLp0PRxCELMsXr6VP56H06TwUqx42NG1uIp+MZ8S4wZz8oP786/zZS/mKy3Gt9QvRKqOJda8xooEpCP+4GBqGafNG8gl5ho6x5Myx8/mO6dS9PQ6eM5g41C5bAxNg8Ro3tLQ0GdVXNDCF/42MTIV8CpPoySzGWrduzf79+3F2diY5OZnTp0+zatUqVFVVCQwMxNzcnLdv3xIcHMykSZPylWbjxo2ZP38+iYmJ6OrqsnDhQszMzD4Zr6yszPv3779WkYQCkpz0HEc7d1Zt8kJVTZUnj6KYPcWVRqbGePo6M8B85CdjAHZt2U/VGpU5FLILVTVV/LcFcPnin/L0q9esRtQTcaMVhNw8S0pm1lQX1m5ZiqqaKo8jo5j5oxMAjZs0YLGvK306D80z7lOatjCh94DuPHzwiP2/bpXv95q/gnNnLyqyWIJQpCUnPcfZbgHLNy1EVVWVp4+jcLR1p6FpfeYvm8ugLqM/GQNg7zoVCRLmL/tvRv2r4dc5fOAYPfp3IfLBY/yCN8iPLVuwhoshYQVeTqF4KuxJehRBkim6o4qVsLAwVq9ejZ+fH2lpabi5uXHv3j2kUiljx45lyJAhvHv3Di8vL37//XfevXtHv379sLW1zXYugIWFBdu3b6dKlSqsWrUKgKlTp3Ls2DHWrl2LTCajSZMmzJ8/n7Vr18qPf3iupqYmNjY2GBsby4fWfk493RYK+GYEQcjLO9m7ws6CIJRImiqlCzsLglBi3Yz/vbCzkC8Lqo9USLrzHu9USLr5IRqZQoETjUxBKHiikSkIhUM0MgWh8BSXRqa7ghqZLoXYyBTDZYUCl9tsiYIgKJaqkurngwRB+OpeSd8WdhYEQRAKnGhkCoIgCIIgCIIgFJJv8Z1MMbusIJQQnbu1JzjEn+OXDrBykxdaWpr5jlFSUsLJYybHLh7gVHggw8dYy88xa9eCgJN+HDq7m31Ht2LStGG2NNXUVNm6bw09+3VRbAEFoQjr3K0dh0J2c+zSAVZsWoxmrvUv9xglJSXmeszg2MX9nAw/yLAP6p959w6E/3WaoLM75R9NTQ0ApjtO5kRYAEFnd+LqNQc1dbWCKawgFLLO3dpzJHQPJ38PYNUn7nd5xSkpKeHsYc+JSwc4Ex7E8LH/1bkataqyO/gXjl3YT8CJ7dSqU0N+bPyPozj62z4Oh/iz/cA6qtWoAkDlqpXYsmc1xy8e4OBJP3oP6Ka4wgtCEVHiG5lhYWHY2Nh88vjZs2fZsmXLJ4/Hx8czYcIERWStQEilUsaPH0+PHj0IC/vyWdAcHR2Jjo5WQM6Er6l8BW0Wr3DF9rtZ9GhjzdNHUdjPm5rvmGFjrKlRuxp9OgzBqpsNY34YgUnThqiqqrBi4yKcZ3jQ33w4a5dtwnutuzzNJi0as/foVpq1alKQxRWEIkWngjaLVrgy9bvZ9GxjzdNH0djPs813zLAxVtSsXZ0+HYZi3W00Y38YLn+Y06yVCZvX7mCA+Uj5Jz09A6vh/TDv3h7rbqMZYD6SxPgkfnKcXOBlF4SCVr6CNktWujFlnD3dWlvx9HE0s1ymflHc8DHW1KxdjV7thzCw2yjG/XPPA1i23pNdW/fTs90gVnitZ82WJQC07diKwSMHMrjnWPp2Hsbxw2fwWuUGgPcad65euUGPttaMsvyBiVPHUL9h3YL4OoRiQiZRzKcwlfhG5ufcvHmTtLS0Tx7X19dn48aNBZijrys+Pp579+5x/PjxPJcq+ZSwsDCxXmYx0L5zG25E3Obxw6cA7Nq6n/6DeuU7pnvvzhzYHYxUKiX1xUuOHDzOgMG9effuPe1NenH7xj0AqtaoTEryC3maYyYMY+mCVVy/erMgiikIRVL7zq2z1a3duda/T8d0623Ogd2HPqh/J+g/OOtY05YmtG7fgkNnd7EreCMt2jQFoJGpMad+DeVlatb968SRs/QQowmEEqC9eRuuR9zi0T91aeeWfQz4qL59Lq57H3P2f1DnDh88wcDBvdE30KVW3RocDjgOQOjpi2hoatDQpD5JCc9wmbWQtLR0AG5E3KZylUoANDIx5sDuQwCkp2Xw+29/0L2PhWK/CKFYEetkfsMiIyNxcXEhJSUFDQ0NnJyc0NDQwN/fHwBDQ0MMDQ3ly3SUK1cOHx8fMjIyGD16NGfOnGHs2LE8f/4cyGq8mZqa8vPPPxMYGMi2bduQyWQ0bNgQV1fXrOFPc+dy//59AEaMGMGQIUN4+vQps2fPJj09nRYtWnD06FEuXbqUbYkR+G8JEW1tbebOnUt8fDwJCQm0adMGT09PwsPD8fb2RiaTUbduXVxcXHB3d+f+/ftIpVImTJhA3759+eGHH0hJScHKyoqAgAA2bNjA0aNHkUqltG/fnlmzZiGRSHItw7Zt20hISGDixIns3LkTHR2dgv6xCflkUFmf2Og4+XZcTAJlymqhpaUpvyHmFWNQ2YC4D4/FJlC/QdZT2Pfv31NBtzyBp3dSvrw2dhMc5XE//ZC1pt+k6eMUWj5BKMoqVdYnNjpevv1v3dLU0iT9n/qXV0yOY7HxGDWoA0BK8guCA45zPPg0zc1MWbvdh/6dR3Dtyk3GThrBjk17SHmeysAhfdDTr1hAJRaEwlPJMLe6VCbb/e5zcTnrYzz1G9SlUmUDEuISsz1cj4uJx8BQj9PHzsn3qampMttlGkcPnQTg2p83GTRiACu81lO+gjadurbjSliEIoovCEWGaGT+Y9asWUycOJHu3bsTERGBnZ0dx48fZ9iwYQBYW1tjY2ODm5sbJiYmbNy4kdu3b1OjRg15Glu3bgXgyZMnfPfddzg6OnL//n327t2Lv78/6urq+Pj4sGnTJlq0aMGLFy8IDAwkPj4eHx8fhgwZgru7OwMGDGDYsGEcPHiQnTvznno4JCQEY2NjVq5cydu3b+nTpw+3bt0C4NGjR5w9e5YyZcqwdOlSGjZsiJeXF2lpaQwbNgxTU1PWrVvH6NGjCQgI4Ny5c9y8eZP9+/cjkUiYNWsWhw4dokGDBrmW4ccff8Tf358NGzaIBmYRp6QkIbcOZ6lMmq+Yj49JkCCV/fea+rPEZDqY9KKBSX22H1jHoB4PefTwydcsgiAUW0pKSrmO+JBlq3+fjpEoSbId+7D+2Y6bLd9/JewaVy9fp11nMwJ2B2NgqMe2gPW8ynjFHr+DvH0nlrERvn2fqksf3u8+F/fxMYlEglSa817437H/7oflK2izeos3L1PTWOqxGoBZU1yYu2AGv57bw9MnMZw9cZ7SpUv9f4opfGO+xTGBopEJpKenExUVRffu3QFo0qQJ5cqV4+HDh9niunTpgq2tLV27dqVLly60a9eOqKiobDFpaWlMmTKFefPmUaNGDXbs2MHjx48ZMmQIAO/evaNBgwYMHz6cyMhIxo8fT8eOHZk9O+sPhcuXL7N8+XIABg4ciIuLS55579u3L9evX2fr1q08fPiQlJQUMjIyAKhZsyZlypQB4OLFi7x+/ZoDBw4AkJGRwf3796lXr548rUuXLnH9+nWsrKwAeP36NYaGhrx8Vv53JwAAl3NJREFU+TLXMgjFR0xUHKbNGsm39SvpkvL8Ba8yXucrJiYqDj0DXfkxPYOKxMXEo1VGizYdWnLy17MA3L5+l7u3/sKoQR3RyBSEf8RExWGSj/r3qZjYqDj0s9U/XXlP58jvBrPe9795AyQSCe/fvaecdlmCDxzj5xVbgaxhtU8is9+vBOFbMd1hEl16dAJAq4wm9+48kB/Tr6SXo74BxETHYdq8Ua5xMbnVudiErHvhRyMCsupjVq+nUYO6bNixnBO/nmWRy3Jk/zwMKlVanTnT3OR58FzmzP27/8fencfVnP0PHH/ddlQkKpV9yZ5lEIqyb6Gyk2UsYwiDIiRZsoXsjHWslaUV2VLZkn0dfBlJe0KU3b3390c/lxQyo43z/D7u4+vez/uczzm3OX06n3M+5/zzHb8BQSh4xDOZkO2dLLlcjlSa+a7X4MGD2bZtG+XKlcPDw4M1a9ZkSePo6EinTp1o2TLjl51UKqVjx44EBAQQEBDA7t27cXV1RUdHh/379zNgwACioqKwsbHh2bNnqKurK8ojkUhQUVFR/Pvjcr79/zvS27ZtY+HChZQsWZIBAwZQuXJlRZyGxoe7ZDKZDA8PD0U5du3ahYWFRabyS6VSBg0alKmsI0eO/GwdhMLjZNgZ6jWsQ/lKZQHoO7gHIQfDcxwTcjCcHv26oqysjJa2Jl1s2nM0OAyZTMq8Za40aGwKQBWTSlSqWoErF8QzmILwXkbbqv1R27L7TPvLPibk4HHsPmp/nW3acTQ4jOfpL+j/a0/adcl4tqtGHRPq1q/FiWOnqV2vBqu2LEJFRRllZWVGjB1M4J7gPKy1IOSdpfPXYm3VF2urvvToMIj6DetQ4f/bUr/BdhwNDs+S5mRoxGfjjgaH0aN/t0zXvCMHQklMSCY6KoYuNhmDEhZWTZHJZNz++y4GZfTY7reWFYvW4+6yWNHBBBg3eST9h/QEoELlcrRu34JD+47l6nciFC6yXHrlJ9HJBDQ1NTE2Nubw4cMAXL58mZSUFKpWrYqysjLv3r0DoGfPnjx//pzBgwczePBg/v7770z5eHp6oqamxsiRIxWfNWnShCNHjvDo0SPkcjlubm5s2bKFkJAQnJycsLS0xMXFhaJFi5KQkIC5uTl+fn4AHD16VDEqqaOjw927GXfmrl69ysOHDwE4deoUvXv3pmvXrrx+/Zpbt25l+sX2npmZGV5eXgAkJyfTtWtXEhISssQEBATw/Plz3r17x+jRoxULAmVXBwBlZeUsnXGh4Hmc8gTncTNZsXEhB0/twaRGFea5elLbtAaBoTu/GAOwc/MeHtyPJSjMC98j29i9I4Czpy/y4vlLRg2ayLQ5EwkM3cn8ZTOY8JsLiQnJ+VldQShQHqc8Ycq4WazYuIDgU7upVqMK812XUtu0BgGhO74YAx/aX2DYTvYe2cruHQGcO30RmUzG7/YTGTpqAPuO+zB/uSt/DJ/Ck8dPORUWyfGQ0wSFe7P/5C7++V8Uf63dmY/fgiDkjUcpT5g81o2Vmzw4dHovJjWrMtd1CQB16tUgKNTrq3E7Nu/hQVQs+8K98T+6nd07/Dl7+iIAf4yYQt/BPQg+sYuJ00YzZuhk5HI5Do7DKVq0CIOG9yEo1IugUC/2Hsr4W2n+jKVYtjHnwHEflq+fj5PDDBLik7IpvfCz+hEX/pHIf/KlQSMjI1m5ciVubm64ubmRmpqKqqoqLi4uNGjQgHPnzjF58mSGDBlClSpVmDdvHioqKhQtWpQ5c+agoqLCwIED8fLyokWLFpiYmCjm8mtra7Nt2zZ2796tWDSnRo0azJ07N2MPJhcXrl27hrq6Om3atGH06NE8evSIadOmkZCQQM2aNfH19eX27ds8efKEcePGkZKSQq1atfjnn39Yvnw5MTExuLm5oaamhqamJhoaGnTq1Ily5cqxcuVKtm3bBmRM43Vzc+PWrVtIpVJGjBiBjY0NsbGxioWLAFavXs3+/fuRSqVYWFgwdepUJBJJtnVQV1fH3d2d48ePs2HDBsqWLZuj77xq6Ya59vMUBCF7EvJ5LXNB+ElJ5fk9niAIP69/Ui7mdxFyZHKFvrmS74L7XrmSb0789J3Mgs7ExITbt2/ndzG+K9HJFIS8JzqZgpA/RCdTEPJPYelkTsqlTubCfOxkioV/BEEQfgJKEtHJFIT88FYmHikRBOHnIzqZBdyPNoopCIIgCIIgCMIHP+J8B7HwjyD8JCzbmhMU5s2hiL0s37gATc1iOY5RUlJi2pyJHDy9l6Nn/ek7yC5L2h79uvLnds9Mn63cvJCjZ/0JDN1JYOhOps6ekDuVE4RCpGWb5gSE7ST49B6WbphHsWza4tdiDAz1Cb+ynxIliys+a9K8IXsOb8E/dAfeBzZRp77YakoQPmXV1oLg47sJiQxg1SYPNLWytr8vxSkpKTHd3YmjZ/wJPRdEv8E9FWnMzBsRdMyb4OO72em/gRq1qmWbtyB86kdc+Ed0MgWFKVOm0Lp1a0xMTPK7KMJ3VlK3BPOXzcDhVyfaN7Uj5n4sjtPH5DimzyA7KlQuR2eLXti2tWfQb/2oW78WAMVLaDPLYwrT3B2RfDIls94vdelnPYyuVv3oatWPudOX5E2FBaGA0tEtwdxlrowdMpmOzXoQEx3HxOkO3xTTrVcntgf8iX4ZPcVnqqoqLFk3l+kT3Olu1Z+1nptYuGpWntVLEAqDkro6LFwxi98HT6R1k248iI5jkuu4b4rrN7gHFSuXp31zO7q16cevI/tj2qA2WlqarN2yhHluS+jYoicuTnNYuckDNTXVvK6mIBQIopMpKPj5+REcHCym6P6AzC2bcu3y30TfiwFg51976NqjY45j2nWyZK9XEFKplGdP09jvd4huPTsB0KlbW5ITU1gwY2mm/IzLGVJMsyjuntPZF+7D/OUzKF5CO5drKggFW3NLs4x2FpXRzrz/2ou1XYccx+jpl6J1R0uG9R6bKc3bt+9oadqJm9f/B0DZ8kakPnma29URhELFwqopVy9d5/69BwBs37SLbj06fVNc+86t2LMzQHE9DPI9SPeenalQuRxpz9I4ffwsAPfu3CctLZ36jUzzqHZCYSbPpVd+Ep1MAYCRI0cil8tp1qwZpqYZvxCdnZ2ZMWMGtra2tG/fHn9/fwAiIiKwtbXF1taWIUOG8Pjx43wsuZATBkb6JMQlKt4nxiejpa2Zacrsl2IMjAxI/PhYQjIG/z+K4rVlLysXr+fNm7eZzqlbqiSnw88y3XEuXa368vz5C+Ytm5FbVRSEQqGMoT6JH+2P976dfTwd9ksxyUkpjB0ySfHH78fevZOiW7ok4Vf24zRjLBtWbs3dyghCIVPGyICEuI/bVhLa2lpZpsx+Ka6MkQHx8YmZjhkY6hP1TzRFihbBwrIpAHXr16KaSWX09Evlcq0EoWASnUwBgLVr1wLg7++Prq6u4vOYmBh8fHzYsmULCxcu5OHDh6xevRo3Nzd8fX1p1qwZf//9d34VW8ghJSUJ2W1WJP1o1cMvxXx6TIIEqezLj6lfuXid0YMdSYxPQiaTsWLhOizbmqOqKtYbE35eGW0pa0OTZWmLX475nEcPH9PStDN9Ov3K3GWuVKhU7r8VWBB+IEpKEuTZjO9IpbIcxylJlDINEUkkEmRSKelpz/nNfjyjxg/lQPgubHtbc/rEOd6+fZslH0H4lCyXXvlJ/LUnfJGtrS2qqqoYGBjQoEEDLly4QOvWrXFwcKBNmza0bt2a5s2b53cxha+Ij03EtEFtxXv9MqVJffKUly9e5SgmPjYRPYPSimN6BqUyjbRk5xezemgX1+bYoeNAxoVYLpNluZgLws8kPi6Jul9rizmI+ZSmVjHMLBpx9EAYAH9fu83tG3eoVrNytqOegvCzGO88ijYdWgKgqaXJ7Zt3FMcMyuj9f9t6mSlNfGwi9RrWyTYuLi7hk+thaRLik5BIJLx4/oK+3YYpjh2LDFQ8giIIX5LdTY28FhQUxJo1a3j37h2DBg2if//+mY4fPXqUFStWIJfLMTY2Zt68eRQvXvwzuYmRTOErlJWVFf+WyWSoqKgwePBgtm3bRrly5fDw8GDNmjX5WEIhJ06GnaFewzqUr1QWgL6DexByMDzHMSEHw+nRryvKyspoaWvSxaY9R4PDvnjOosWK4jpvkuI5zGGj7TkYFILsKyOggvAjOxV2BtNfalO+YkY76zPIjmMHj39zzKdkMhnuS6dTv3FdAKqYVKJi1QpcuXAjF2ohCIWH5/zVdLbsTWfL3ti2t6d+w7qKEf5+Q3pyJJtr2YnQiM/GHQ0Oo1e/7v9/PdTC2rYDRw6EIpfL2eSzijr1MlZ17ty9Pa9fv+bmjf/lST0F4b9ISkrC09OTnTt34u/vj4+PD3fv3lUcT09Px83NjXXr1hEYGIiJiQkrVqz4Yp5iJFP4ouDgYDp06EB8fDxXr17F3d2dnj17MnPmTAYPHkyJEiUICQnJ72IKX/E45QnO42ayYuNC1NRUeXA/FqfRrtQ2rcHcpdPpatXvszEAOzfvoVwFY4LCvFBVU8V7iy9nT1/84jmPh5xm63pvfPZvQqIk4X837zJt/Jy8qK4gFFiPU54wdewslm2aj6qqKjH3Y5ns4EZt0xrM9nTBplX/z8Z8yYvnL3EY5MTU2RNRUVXhzes3OI50ISkhOW8qJgiFwKOUxziNcWX15kWoqqkSHRXLxFHTAKhTrybzl86gs2XvL8Zt37SLchWMOXB8N6qqKnht2UPk6QsA/DHCmXmeM1BVUyU56SEj7P/Ir6oKhUxu3X5/9uwZz549y/K5trY22tofFmM8ffo0ZmZmlChRAoD27dtz8OBBHBwyVjZ/+/YtM2bMQF9fHwATExOCgoK+eG6JPLsHP4SfkomJCSEhIQwcOJBjx47h7OzM48ePSUlJ4c2bN0yYMIFWrVoRERHBvHnzUFFRoWjRosyZM4cKFSrk+DxVSzfMvUoIgpAtZYmYuCII+eG19E1+F0EQflpRj67kdxFyxKFC71zJ12SiOStXrsx6PgcHxoz5sJXdn3/+yYsXLxg/fjwAu3fv5urVq8yePTtL2levXtGvXz/s7e2xsbH57LnFSKag8H7rkmPHjik+69ChA7a2tpnimjZtSmBg4L8+j0zc1xCEPKcs+XqMIAiCIAh5T5ZLz2QOGjQo247gx6OYkPHIxcd7ncvl8ix7nwOkpaUxevRoqlev/sUOJohOpiAIgiAIgiAIwg/n02mxn2NgYMD58+cV7x8+fIienl6mmOTkZIYOHYqZmRlTp079ap5i/pTwWfPnz88yiikUbpZtzdkf7sORM76s2Lgg0z6ZOYlTUlLCZY4jhyP2cuxsAH0H22VJ26NfN9btWJrps6GjBhB8cjf7wrzZuncN5SoYf/e6CUJh0bJNcwLCdhJ8eg9LN8zLtEdmTmMMDPUJv7KfEiUzr+zX3LIJfsd25Gr5BaEws2prQfDx3YREBrBqk0eWPTJzGlfGUJ+I60fQKVlC8Vnd+rXYfeAv9of5EHxiD917ds7Nqgg/EHkuvXKqWbNmRERE8PjxY16+fMnhw4dp0aKF4rhUKmXkyJF07NiRadOmZTvK+SnRyfwOdu3axb59+/5zPu+HoL+HyMhI7O3tAbC3tycyMjJLzIoVK766MlR2fH19cXZ2/s9lFPJWSd0SLFzuxughjrQ1syUmOg4n1zHfFNd3kB0VK5ejo3kvurcdwJDf+lG3fi0AipfQZvaiqUyf65jpl0+zFo3p2b87PTsMpotlHw7tO8aCFW55UGNBKHh0dEswd5krY4dMpmOzHsRExzFxusM3xXTr1YntAX+iX+bDXWZ1DXXGOY9kybq5mVYFFwThg5K6OixcMYvfB0+kdZNuPIiOY5LruG+Os+3dBZ99mzEok3mkZ/Vfi1m6YA2dLXszpPcops12FHvVCjkiQ54rr5zS19dn/PjxDBw4kO7du9OlSxfq1q3L8OHDuXbtGseOHePvv//m0KFDdOvWjW7dujFt2rQv5ik6md/BxYsXefPmvz/Y//TpU27evPkdSiQIWZlbNeXq5Rvc//89u3Zs3k23Hh2/Ka5dZyv2eAUilUp59jSNfX6H6d6zEwCdurclKfEh82cszZRfSvIjXJ3mkp7+HIBrl//GyLhMblVTEAq05pZmXLv8N9FRGe3L+6+9WNt1yHGMnn4pWne0ZFjvsZnSmFuZUaRoEZzHzMyDWghC4WRh1ZSrl64r9o7dvmkX3Xp0+qY4PYPStO3UikE9f8+URk1djeUL/+RUeMZN/cT4ZB4/eoKBoX5uVkkQvhtra2v27dvHoUOHGD58OADr16+nTp06tG3bllu3bhEQEKB4ubu7fzG/fHkmMzIyEg8PD2QyGcWLF0dJSYm0tDSSk5OxsbFh9OjRWFhYcOTIETQ1NenTpw+tWrVixIgR7Nu3jwsXLuDs7MzMmTO5cOECqqqqjBo1ik6dOnH16lXmzZvHq1ev0NHRYebMmZQtWxZ7e3vq1KnDhQsXePz4MS4uLrRs2ZKgoCA2bNiAsrIyxsbGeHh4cPnyZVauXMm2bdsAcHZ2pnHjxrRr144JEyaQkpICwOjRoylSpAjHjh3jzJkzlC5dmv3795Oamkp0dDROTk68fv2azZs38+rVK968ecPcuXNp0KABN2/exNXVlVevXlG8eHEWLVrEnDlzSE5OZvTo0axatQpPT08iIiJ4+vQpenp6eHp6UqpUKczNzWnfvj0XLlxAWVmZpUuXUrZsWU6ePMm8efNQV1enYsWKmb7zXbt2MW/ePACmTJlCkyZNMh0PDQ1l6dKlyGQyypYty6xZsyhVqhSnT59m/vz5yOVyDA0NWbx4caZ07u7uPHr0CA8PD3H3vIArY6hPQlyS4n1ifDJa2lpoahZTdAC/FlfG6NNjSVSvWRUAr7/2AmDXxzrTef936x/Fv9XUVJnkOpbgwCPft3KCUEiUMdQnMf7T9qVJMc1iPP//dvilmOSkFMYOmZQl35DgcEKCw2ncrEHuV0IQCqkyRgZZrmHa2lpoahUjPe15juKSEx/y+6AJWfJ+8/oNu3b4Kd73HWhHMc2iXDp/NZdqI/xIfsQdxPNtJPP+/fts2bIFc3NzunTpwq5duwgKCmLLli08e/YMMzMzzp07x/Pnz4mPj+fcuXMAnDhxAktLS7Zt28aLFy8IDg5m8+bNrFq1ijdv3uDi4sLixYvx8/NjyJAhTJ8+XXHOt2/f4uPjw5QpU1i2bBkAS5cuZdOmTfj6+mJkZMS9e/c+W+YjR45gZGSEr68v7u7unD9/nmbNmtGqVSvGjh2LhYUFACVKlCA4OBhLS0u8vb1Zu3YtgYGBDBs2jHXr1gHg6OjIqFGjCAoKolOnTmzZsgUXFxf09PRYtWoV0dHR3Lt3D29vbw4dOkSZMmUUK7o+fPiQpk2b4u/vT6NGjdixYwdv3rzB2dmZ5cuX4+vri4aGRqayFy1aFH9/f+bPn4+Tk1OmkddHjx7h6urKqlWrCAoKokGDBsyaNYs3b97g6OjIggULCAoKolq1avj5ffgFumLFCpKSkli4cKHoYBYCSkpKZLdjkVQmzXHcp8ckEglSqTRLbHZK6pbgrz2ref78BYvmZF1OWxB+BkpKkmzbl+yjdpiTGEEQvp2SkgR5NlMIpVLZv4r7nJHjfuWPyb8zvN9YXr96/e8KKwiFXL6tLluxYkW0tLQYOnQoZ86cYePGjdy5c4e3b9/y8uVLWrZsSUREBEpKSlhbW3PgwAHevn3L+fPnmTVrFjt37qRXr14oKSkpRhD/97//ERMTw++/f5jCkJ6ervj3+05g1apVSU1NBcDKyoq+ffvSpk0b2rdvT40aNbJ9fhGgfv36LFmyhKSkJCwtLT/7/GTdunWBjD/WV61axbFjx4iKiuLs2bMoKSnx+PFjHj58iJWVFQD9+vUDIDY2VpFH+fLlmTx5Mrt37yYqKorLly9TrtyHef0f1+X8+fPcvn0bPT09KleuDICNjY2iIw3Qo0cPAKpXr46urm6mzvTVq1epW7cuxsYZi7H07t2bdevWcfv2bfT19alRowYAEydOBDKeyTx+/DiPHz9mz549qKiIRYoLqj+cR9K6fUsANLWKcfvmXcUx/TJ6pD55yssXrzKliY9LxLRh7Wzj4mMT0TcorTimZ1CaxBxs9m5Ssyrrtnty+EAo81w9kcl+xHt2gvB18XFJ1G3wcfsqnaUd5iRGEIScGe88ijYd3l8HNbl9847imIHi+vYyU5r42ETqNazz1bhPqamp4rFyNlVNKmHbYSBxMfHfsSbCjyy7mxqFXb6NZL4faZs/fz7btm3D0NCQ33//HR0dHeRyOS1atCAyMpIzZ87QpEkTqlevzp49e6hWrRrq6uqoqKhkWlwkOjoamUyGsbGxYq6wr68vO3fuVMSoq6sDZErn4uLC8uXLKV68OE5OTgQEBCCRZL6L/PbtWwAqVKhAcHAw1tbWnD9/nh49emT7x/L7uj1//pwePXoQGxtLo0aNFAvxqKqqZirD69eviYmJyZTH9evXGTp0KDKZjPbt29OmTZtMZfq4Lu/3svn4+Kcjix+/l8lkmTqGn9ZBLpfz7t27LOVMS0sjMTERACMjI2bPns2sWbNEh6EAWzp/LdZWfbG26kuPDoOo37AOFSqVBaDfYDuOBodnSXMyNOKzcUeDw+jRvxvKyspoaWvSxaY9Rw6EfrEMBmX02O63lhWL1uPuslj89yL81E6FncH0l9qUr5jRvvoMsuPYwePfHCMIQs54zl9NZ8vedLbsjW17e+o3rKtYjKffkJ4cCQ7LkuZEaESO4rKca+1cNLWKYddxkOhgCt9Elkuv/JTvC/+cOnWKoUOH0rFjR6KiokhKSkImk1GyZEk0NDQIDQ2lYcOGmJmZsXr1asXoX6NGjThw4AByuZxHjx4xYMAAjIyMePr0qWKfl7179+Lo6PjZc79794527dqho6PDb7/9Rrdu3bh58yY6OjrExMTw+vVrUlNTuXDhAgDbt29nxYoVdOzYkRkzZvD48WPS09NRVlbOdsrg/fv3kUgkjBw5kiZNmnDkyBGkUilaWlro6+tz8uRJAAICAli2bBkqKiq8e/cOgHPnztG4cWP69u1LhQoVCAsL++K0RBMTE1JSUrh16xYA+/fvz3Q8KCgIgGvXrvH8+XPKly+vOGZqasqVK1cUI6k+Pj40adKEihUr8ujRI+7ezRj92rBhA15eXgBUrlyZnj17UqRIEXbsEMvlFwaPUp4weawbKzd5cOj0XkxqVmWu6xIA6tSrQVCo11fjdmzew4OoWPaFe+N/dDu7d/hz9vTFL57XwXE4RYsWYdDwPgSFehEU6sXeQ1tyt7KCUEA9TnnC1LGzWLZpPvtP7qJajcosmLGU2qY1FFuPfC5GEIT/5lHKY5zGuLJ68yKORPhhUqMq7tMXAVCnXk32h/l8Ne5z6v9Sl07d2lGhUjn2/P82JvvDfGhh1SzX6yUIBVG+z3P87bffmDRpEhoaGhgYGFC7dm1iY2MpV64cLVq0IDw8nGLFimFmZsbcuXNp2TJjykO/fv2YM2cOXbt2BWD69OloaWmxbNky3N3def36NZqamixYsOCz51ZRUWHs2LH8+uuvqKuro6ury/z589HV1aVly5Z07twZIyMjGjZsCED37t2ZMGEC1tbWKCsr4+TkhLa2Ns2aNWPJkiVoaWllyr969erUqFGDjh07IpFIMDc3V3RYPTw8cHNzw8PDAx0dHRYuXIiOjg6GhobY29uzaNEiHBwcsLbOWETl/ffyOaqqqixZsgQnJydUVFSoWbNmpuMvXryge/fuKCkpsXjxYlRVVRXHSpUqxaxZs3BwcODt27cYGhri7u6Ouro6Hh4eTJo0ibdv31KuXDkWLlzIoUOHFGnd3Nzo27cvbdu2xcDA4Ks/byF/hR09RdjRU1k+v3b5JtZWfb8aJ5VKmePy5QvtXu8g9noHKd67THTHZeKXVyAThJ/J8ZDTHA85nemzp6nPsGnV/4sxn6qu1yjLZ2dPX6Rryz7fp6CC8AMKO3qSsKMns3x+7fLfdLbs/dW4j1XUNVX8+9L5q5neC8K3+BGny0rk2a0uIAi5qHIpsfqhIOQ1VSWxOJcg5IfX0v++xZkgCP9O1KMr+V2EHBlSwS5X8t18f2+u5JsT+T6SKfx8fsS7NYJQ0L2RvcvvIgjCT0lJIm7wCILwZfn9/GRuEJ1MQRAEQRAEQRCEfCL7ASeW5vvCP4Ig5B2rtuYcCPfh6Bk/Vm5ciKZmsW+KU1JSYvocR45E+HLsbAD9BvfIkta4nCEX74RRp17NLMeG/NaP4BO7v2+lBKGQyM32V7yENp5r3Qk65sWRCF+69+ycKU81NVW27llDR+s2uVdBQSigLNuasz/chyNnfFmxccFn297n4pSUlHCZ48jhiL0cOxtA38FZpzb26NeNdTuWZvqs7yA7gk/uZn+4D2u3LUGnZInvXTVBKLBEJzOXTZkyhdatW2NiYvJd8nN2dsbX1/e75AUZC7kMHTqU9u3bExkZqdhmRfjxlNTVYcHymYwa4kQbMxtiomOZ5Dr2m+L6DbKjYuXydDDvSfe2AxjyWz/q1q+lSKumrsaSNe6ZFpZ6r2FjU0aMGZxr9ROEgiy325/Hylkkxidh3aov9rYjmTFvEgZl9ICMVS/3HNxCw8ZiURLh51NStwQLl7sxeogjbc1siYmOw8l1zDfF9R1kR8XK5eho3itL2yteQpvZi6Yyfa5jpm3fjMsZMmHqKPpaD6Nzy97EPYhn3OSReVFloRCS59IrP4lOZi7z8/MjODiY27dv53dRspWUlMTt27czrRgr/JgsrMy4dvkG9+89AGD75t1069Hxm+LadW7Fbq8ApFIpz56msc/vUKYRk1kLprDXO5Anj1Mz5VmqdEnc5jsz380zl2onCAVbbra/4iW0MW/ZhGUe6wBITEjGtr09qanPABg8oi8LZy3nyqUbeVFVQShQzK2acvXyDe7fy9iPfMdn2t6X4tp1tmKPV+BHbe8w3Xt2AqBT97YkJT5k/ifbDCkrK6OqqkIxzaJIJBKKFNXg9evXuVhTQShYxDOZuWjkyJHI5XKaNWvG27dvuXLlCs7OzqSmphIdHY2TkxOlSpVi3rx5vHr1Ch0dHWbOnEnZsmWxt7enevXqnD9/ntevXzN16lTMzc0z5e/p6UlERARPnz5FT08PT09PSpUqRVBQEGvWrEEikVCnTh1mz57NmzdvmDVrFnfu3EEqlTJ8+HC6dOnCb7/9RmpqKra2tkyePFmR99mzZ/H09OTVq1c8e/aMKVOm0KZNGxITE3F0dOTp06dUq1aNc+fOcfy42CS8MChjaEBCXJLifWJ8MlraWmhqFiM9/XmO4soY6Wc6lhCfTPWaVQHoNcAGFVUVfLb5MXr8MEWMkpISS/+cy4KZS3n7Tiw+I/yccrP9la9YluSkFIb+PoCWrZujrq7G+lVbifono6M6bsQUAEaNH5rb1RSEAqeMoX4O297n4z5te4nxSYprn9dfGat32vWxznTe6KgY1q/cypEzfqQ9TSMtLZ0eHQbnRhWFH4As38cdvz8xkpmL1q5dC4C/vz+6urqKz0uUKEFwcDDm5ua4uLiwePFi/Pz8GDJkCNOnT1fEpaen4+fnx+LFi3F2dubNmw/LoEdHR3Pv3j28vb05dOgQZcqUITAwkKSkJObNm8emTZvYv38/UqmU8PBw1qxZQ61atfD19WXHjh2sXbuWmJgY1qxZg56eXpYpuNu3b2fOnDn4+fkxZ84cli1bBoC7uzsdO3YkKCiIDh06kJSUhFA4KClJyG7HIqlMmuM4JSWlTMckEpBKZdSqW53+g3vg4ph1P0yn6WM4G3GRk+GR36EWglA45Wb7U1VVoVwFY9LTntOr8xDGDnfGZc5EapvW+P4VEYRC5tN2817Wtvf5uKxtT4JUKs0S+zFzSzPad2mNhWlHzGq142hwOAtXzvyXtRCEwkeMZOaDunXrAnD//n1iYmL4/fffFcfS09MV/+7VqxcANWrUoHTp0pmm3JYvX57Jkyeze/duoqKiuHz5MuXKlePSpUs0aNAAAwMDADw8PABYvXo1r169Yu/ejDtuL1684M6dO1SrVi3bMnp4eBAaGsrBgwe5cuUKz59n3O07deoU8+bNA6Bt27Zoa2t/l+9EyB1/OP9Om/YtAdDUKsbtm3cVx/TL6JH65CkvX7zKlCYuLhHThnWyjYuPTUTfoPSHYwalSUxIwrZXFzS1irHnwF8A6BmUxnOtO/PclmLTqzOPHj6hXadWFC1WBIMyeuwL9aaLldgwXvix5VX7S0p8CMAerwAgYwTl/JnLmDaozfUrN3OtfoJQUP3hPJLW39j24uMSMW1YO9u4T9uenkFpEhOSv1iG1h1aEnLoOI9SngCwbaOPWPhO+KwfcXs/MZKZDzQ0NACQyWQYGxsTEBBAQEAAvr6+7Ny5UxGnrPxhby2ZTIaKyod7AtevX2fo0KHIZDLat29PmzZtkMvlqKioZHrw/PHjxzx+/BiZTIaHh4fiXLt27cLCwuKzZezXrx9Xr16ldu3ajBz54UF1ZWXlbO/0CQXT0vlr6GLVhy5WfbDrMJD6DetQoVI5APoP7sHR4LAsaU6GRnw27khwGD37d0NZWRktbU262LTn8IEwZrssonWT7opzJSc+ZPzIaYQcDMesVjs6W/ami1UfpoyfRfT9WNHBFH4KedX+Yh/Ec+3K39j2zpiuV6p0SRo0NuXaZfEMpvBzWjp/LdZWfbG26kuPDoP+v02VBaDfYDuOBodnSfOh7WWNOxocRo9P2t6RA6FfLMONq7ewamtO0WJFAOhg3ZrLF659z2oKPxBZLr3yk+hk5qNKlSrx9OlTzp8/D8DevXtxdHRUHD9w4AAA165d49mzZ5lGHc+dO0fjxo3p27cvFSpUICwsDKlUSp06dbh8+TIPH2bc2Z47dy4hISGYmZnh5eUFQHJyMl27diUhISHbcqWmpnL//n3GjRtHixYtCAkJUUwLadq0KUFBQQCEh4fz7Nmz7/ytCLnlUcoTJo11Y9UmDw6f3otJzSq4uy4BoE69muwL9f5q3I7Nu3kQFcv+cB8Cju5g1w5/zp6+kG91EoTCIrfb3+8DJ9KiVVMOntzDzoD1rFi0jquX/s6fygpCAfIo5QmTx7qxcpMHh07vxaRmVeYq2l4NgkK9vhq3Y/MeHkTFsi/cG/+j29m9w5+zpy9+8bx7dgYQdvQkASE72B/uQ5PmvzDJYUbuVlYQChCJXAxL5SoTExNCQkIYOHAgx44dw9nZmcaNG2NrawvApUuXcHd35/Xr12hqarJgwQLKlSuHvb092traxMfHAzBjxgzq1aunSN+8eXMcHBx49SpjukeNGjWQyWQsWrSIgwcPsnr1amQyGfXq1WPmzJm8fPkSNzc3bt26hVQqZcSIEdjY2BAbG6soW2RkJCtXrmTbtm3MmzePkJAQVFRUMDMzIzg4mNDQUNLS0pg8eTKpqalUr16dkJAQzp07903fSaVS9b/vlywIgiAIBZQEydeDBEHIFf+kfPlmQEHRs3y3XMl3d3RAruSbE6KTWUDZ29vj4OBAkyZN8rsomWzdupVmzZpRpUoVbty4wfTp0795307RyRQEQRB+FqKTKQj5R3Qy86+TKRb+Eb5J+fLlmTBhAkpKSqirqzN79uxvzkMuz+9Z4oLw81GSKH89SBCE7+6dXGzdJAjCl/2IC/+IkUwhz1XUNc3vIgjCT0d0MgUhf4hOpiDkn+hHV/O7CDliW75rruTrGx2YK/nmhFj4RxB+IlZtLQg+vpuQyABWbfJAU6vYN8UpKSkx3d2Jo2f8CT0XRL/BPRVpzMwbEXTMm+Dju9npv4EatT4sVDVx6miOnQ1kf5gPszymoqaulrsVFYQCyLKtOfvDfThyxpcVGxegqZl9+/tcnJKSEi5zHDkcsZdjZwPoO9guS9oe/bqxbsdSxfvfxg4mKNRL8Tp17SCXo47nSv0EoTBo1daCg8f3cCwykNWbFn32Ovi1uDKG+kReP4JOyRKKz1q3b8mVuyc4ELZL8SqmWTQ3qyMIBZboZP5EpkyZQlxcXH4XQ8gnJXV1WLhiFr8PnkjrJt14EB3HJNdx3xTXb3APKlYuT/vmdnRr049fR/bHtEFttLQ0WbtlCfPcltCxRU9cnOawcpMHamqq9OjXjVbtWtCtTX86W/bmYWIKjlMd8rr6gpCvSuqWYOFyN0YPcaStmS0x0XE4uY75pri+g+yoWLkcHc170b3tAIb81o+69WsBULyENrMXTWX6XMdM21j9ufwvxVYO/boN5+WLl4wb5pwXVRaEAqekrg4eK2YzcvAEWjXpyoPoWJxd//jmONve1uze9xcGZfQzpWvY2JR1q7bQybKX4vU8/UUu10r4Ecjl8lx55SfRyfyJREZG5vt/cEL+sbBqytVL17l/7wEA2zftoluPTt8U175zK/bsDEAqlfLsaRpBvgfp3rMzFSqXI+1ZGqePnwXg3p37pKWlU7+RKXVMa3L4QChpz9IAOLgvhI5d2+RFlQWhwDC3asrVyze4fy8GyNiOpFuPjt8U166zFXu8AhXtb5/fYbr3zGibnbq3JSnxIfNnLP1sGabMGk/40VOEh5z+zrUThMKhRQ6vg1+K0zMoTftOVtj3HJklXcNG9Whm0Zjg8N3s3vcXjZs2zMXaCELBJhb++cS7d+9wc3Pjzp07pKSkYGJiwoIFC5g4cSIpKSkAjB49mtatW7N582b8/PxQUlKibt26zJo1C6lUysKFCzl79ixSqRRbW1sGDx5MYmIijo6OvHjxImPKk4sL9erVY8GCBZw6dQolJSXatGmDg4MDK1asID4+nvv37/P48WN+//13IiIiuHLlCtWrV8fT0xOJRMK6desIDg5GKpVibm6Ok5MTcXFxODg4ULVqVW7evImuri7Lli1j165dJCcnM2LECMaNG8fmzZvx9s7Yl83X15crV67g6uqabdmz+06WLFlCSkoKw4YNQ0dHBw0NDTZv3pyfPzrhK8oYGZAQl6R4nxifhLa2FppaxUhPe56juDJGBsTHJ2Y6Vr1WNaL+iaZI0SJYWDblRFgEdevXoppJZfT0S3H5wjV+HTmArRu8SX3yFNs+1pTWL503lRaEAqKMof4n7SoZLW0tNDWLkZ7+PEdxZYw+PZZE9ZpVAfD6ay8Adn2ssz1/lWoVadvREqtfcmcFQ0EoDMoYGRAf9+EalvCF6+Dn4pITH/LboAnZ5p/6JJWAPcEcCDrCL03qs2H7Mjq07ElifFK28YLwnuwHXPhHdDI/cenSJVRVVfHx8UEmkzFo0CCOHj2KkZER69at4+bNmwQGBmJpacmff/7JiRMnUFZWZtq0aSQlJXHs2DEA/Pz8ePPmDUOHDqV27dqcOXMGS0tLhg0bxvHjx7lw4QKlS5fm+PHj7N+/n5cvXzJlyhRev34NwP/+9z98fHy4ePEigwYNIigoiAoVKtCpUydu375NcnIy169fZ8+ePUgkEpycnAgMDKRhw4bcunWLuXPnUrNmTcaMGUNQUBAjRozA29ubdevWYWRkxMKFC3nw4AHlypXD39+fiRMnsmvXrmzLLpfLs3wn4eHh1KpVi6ioKDZs2ICxsXG+/cyEnFFSkmS7eplUKstxnJJEiY8PSSQSZFIp6WnP+c1+PI7THJgyczxnIy5y+sQ53r59S5DvQQwM9djpv54XL17itXUvb9++/e71E4SCTElJKduZJFKZNMdxnx6TSCRIpdIssdkZMrI/2zbuIj0t/RtLLgg/DiUlpWz/lM96HcxZ3Kc+7nyej7zEhbNXsLA0Y/fO/NtGQigcfsR9F0Qn8xONGjWiRIkS7Nixg3v37nH//n3i4uI4evQoSUlJWFpaMnr0aJSVlalfvz49evSgdevWDBkyBH19fSIiIrh58yZnzpwB4MWLF9y+fZumTZsyZswYbt68ScuWLRkwYADKysqoq6vTp08frKyscHR0RF1dHYDmzZujoqKCoaEhpUuXpkqVKgDo6+vz9OlTIiIiuHr1Kra2tgC8evUKQ0NDGjZsiK6uLjVr1gSgatWqPH36NFMdJRIJNjY2BAYGYmtry6NHjzA1NWXjxo3Zlr1///5ZvpMXLzKeMdDV1RUdzAJsvPMo2nRoCYCmlia3b95RHDMoo0fqk6e8fPEyU5r42ETqNayTbVxcXAJ6Bh9GIfUMSpMQn4REIuHF8xf07TZMcexYZCDR92IoXkKbwD3BrFm6CYAGjUyJ/v+pgILwI/vDeSSt279vf8W4ffOu4pi+ol29ypQmPi4R04a1s42Lj01E/5P2l5iQ/NVyKCkp0b5LK7q17v9fqyQIhc4E51G06WAJgJaWJrdydB1M+Ox18HO0tbWwH9qbVZ4bFJ9JJPDurVhdWPg5iWcyPxESEoKjoyMaGhrY2trSqFEjDA0NCQ4OxtramvPnz9OjRw9kMhmrV6/Gzc0NuVzOsGHDFNNMnZycCAgIICAgAB8fH3r06EHDhg3Zv38/5ubmHDhwgJEjR6KiosLu3bsZN24cqamp9OnTh6ioKABUVVUVZVJRyXovQCqVMmjQIMV5du/ezciRGc8HvO+oQkaHMru74jY2Nuzfv599+/bRrVs3RZ7ZlT277+R9nhoaGt/vyxe+O8/5q+ls2ZvOlr2xbW9P/YZ1qVCpHAD9hvTkSHBYljQnQiM+G3c0OIxe/bqjrKyMlrYW1rYdOHIgFLlcziafVdSpl3Fzo3P39rx+/ZqbN/5H3Xq1WLvVExUVFZSVlfl93K8E7NmfJ/UXhPy0dP5axaI7PToMon7DOlSoVBaAfoPtOBocniXNydCIz8YdDQ6jR/9u/9/+NOli054jB0K/Wg6TmlV4lppGXEzCd6ydIBQOS+avVizC0739gEzXt/5DenI4OGsbOv7JdfBzcR9LT3/OwKG96WidseZArTrVMW1Qh7CQU9+5RsKPSJ5L/8tPYiTzExEREXTs2BE7OztiYmKIjIzE1NSUFStWMGXKFFq0aIGVlRWpqan079+fPXv2UL9+fRITE7l9+zZmZmbs2rULKysr3rx5Q79+/Zg5cybh4eHo6+szaNAgmjRpgo2NDX///TezZ89m27ZtNG3alL///lvRyfwaMzMzli9fTq9evVBXV2f06NHY2NjQuHHjz6ZRVlZWTK0yMjLCwMAAb29vvLy8FHlmV/bsvpOmTZv+9y9byFOPUh7jNMaV1ZsXoaqmSnRULBNHTQOgTr2azF86g86Wvb8Yt33TLspVMObA8d2oqqrgtWUPkacvAPDHCGfmec5AVU2V5KSHjLD/A4ATYRE0ad6Q4BO7UVJS4vCBUDau2Z4v34Eg5JdHKU+YPNaNlZs8UFVT5cH9WBxHTQegTr0azPV0xdqq7xfjdmzeQ7kKZdkX7o2amipeW/Zy9vTFr567QqVyxMbE52r9BKEwyLi+TWfN5sWoqakSHRXD+I+ugwuWutHJstcX4z5HJpMxbMA4Zs2fwvjJo3j37h0Ow5x48jg1D2omCAWPRC6WG83k9u3bODo6AhmjiUZGRhgaGhIVFUVCQgLKysr079+fnj178tdff+Hj40ORIkWoWLEis2fPRlVVlQULFnDmzBnevXuHra0tI0aMICEhgYkTJ/L8+XOUlZUZO3YslpaWLFiwgNDQUIoUKUKDBg2YMmUKa9asAWDMmDHExsYycOBAxbOe9vb2ODg40KRJE1avXs3+/fuRSqVYWFgwdepU4uLiMsWvWLFCkZe7uzvHjx9nw4YNlC1blt27d3P48GHWr18PwNu3b7Mte3bfSaVKlejZs2emc+VURV3T//6DEgThmyhJlPO7CILwU3onF9MlBSG/RD+6mt9FyJFO5bKucvw9HHhwIFfyzQnRyfxJvXv3jkmTJtGhQwfatWuXp+cWnUxByHuikykI+UN0MgUh/4hOZv51MsV02Z+QXC7HwsKCZs2a0aZN3u9X+CMu0ywIBd0b6ev8LoIg/JQ0lNW/HiQIwk/tRxzzE53Mn5BEIiEiIiK/iyEIgiAIgiAIP70fcQsTsbqsIPzEWrW14ODxPRyLDGT1pkVoahX7V3FlDPWJvH4EnZIlAKhqUokDYbsUr0Mn9hL96CodurTO7SoJQqHQul0Ljpz05fjZffy5ecln297n4jQ01Fm8YjYhp/05djqAxStmo6GRMWJW1aQyfsHbOHx8L4fC99CyVfM8q5cgFHSWbc3ZH+7DkTO+rNi4AE3N7Nve5+KUlJRwmePI4Yi9HDsbQN/Bdoo0Zua/4H90O/vCvNlzcAt169fKkzoJQkEkOpkFWGRkJPb29t8tv5CQEJYtWwbA8uXLOX/+/DelNzEx+W5lEfJfSV0dPFbMZuTgCbRq0pUH0bE4u/7xzXG2va3Zve8vDMroKz67c/ueYsn4Tpa9OBF6moA9Bzi4LyQPaiYIBVtJXR2WrJzDiIF/0KJxF6KjY5k6Y8I3xY2d+BsqKsq0aW5DG3MbNIpo4DB+OABzF7ngvd2Xdi3smODgwtrNi1FWFs/kCkJJ3RIsXO7G6CGOtDWzJSY6DifXMd8U13eQHRUrl6OjeS+6tx3AkN/6Ubd+LVRVVVi+fj5Tx8+mi2UfVi3ZwOI1s/O2gkKh9SNuYSI6mT+R1q1bM27cOADOnTun2M5E+Dm1sGrK1UvXuX/vAZCxPUm3HlkfPP9SnJ5Badp3ssK+58jPnqeRWQM6dm3LVEdxsRUEgJatmnHl0nWi/r9Nbd3ojU3Pzt8Ud+b0eZYt+hO5XI5MJuP61ZsYlzUEMrarKlFCGwBNrWK8fiWexxUEAHOrply9fIP792IA2LF5N916dPymuHadrdjjFYhUKuXZ0zT2+R2me89OvH37jmZ1OvD3tdsAlCtvROrjp3lUM0EoeMQzmYVAVFQUrq6upKamUrRoUaZNm0bdunVxdnZGU1OTGzdukJSUxOjRo7GzsyMtLY1Jkybx4MEDypYtS2JiIitXruTs2bOcPXsWMzMzrl+/jouLCytXrmTOnDmKbVE+3jIlNjYWJycnXrx4ganphxVhnz9/zqxZs7hz5w5SqZThw4fTpUuXfPyGhH+jjJEB8XGJivcJ8Uloa2uhqVWM9LTnOYpLTnzIb4OyjsB8bOrMCSxyX5EpT0H4mRkalclR2/tS3PHQ04rPjcqWYdhIeyaPdwNgmtMcdgVsYvjvA9EtrcuooY7ipqIgkPFoR0JckuJ9YnwyWtpaaGoWIz39eY7iyhh9eiyJ6jWrAhkr9+uWLkngsZ3olCzBuGHOeVAr4UfwIy6KKUYyCwEnJyfs7e0JCgpiypQpjBs3jjdv3gCQmJjIzp07WbNmDQsXLgRg1apVVKxYkf379zN69Gj+97//Zcqve/fu1K5dmzlz5nxxCuzs2bOxtbUlICCABg0aKD5fs2YNtWrVwtfXlx07drB27VpiYmJyoeZCblJSUsr2V5pUKvtXcdlp2MgUXV0d/Pfk3xLaglDQKClJsl1JMGvb+3pcHdOa+B3Yxl8bdnL0UDjq6mqs2biI8aOn8Uvt1th1HsgCzxkYGhl8/4oIQiGjpKSUfZuSSXMc9+kxiUSS6SbOo4ePaV6nAz07DmbBCjcqVC73HWsg/KjkcnmuvPKTGMks4J4/f05sbKxiL8t69epRvHhx7t27B0Dz5s2RSCRUq1aN1NRUAE6dOsWiRYsAqFOnDtWqVftX5z579iyLFy8GoGvXrri4uABw+vRpXr16xd69ewF48eIFd+7coWzZsv+6nkLemOA8ijYdLAHQ0tLk1s07imMGZfRIffKUly9eZkoTH5tAvYZ1vhqXnS42HdjrE5Tvv+gEIb85TnGgXUcrIGMK662/P2p7hno8yaZNxcUmUL9h3c/GdbXtyNxF03GZ5I7/nv0AmNSoSpGiRTh6KByAi+evcvvWXeo3rJtpVFQQfhZ/OI+kdfuWQEbbu33zruKYvuJ69ipTmvi4REwb1s42Lj42EX2D0opjegalSUxIRlNLk2YWjTh8IBSAG1dvcfPG/zCpUYX7/zzIzSoKQoEkRjILuOz+OJfL5Yq7ZurqGasJSiQSxXFlZeVv/qP+ffy7d++y/VwikaCklPGfi0wmw8PDg4CAAAICAti1axcWFhbfdD4hfyyZv1qxGE/39gOo37AuFSpl3GXtP6Qnh4NDs6Q5HhqRo7jsNGnWkFPHI79fBQShkFo0byXtWtjRroUd1m370eCXulT8/zZlP6Q3hw8cy5Im/Njpz8a17WDJ7PlT6Gc7XNHBBLh/7wFa2pr80rgeAOUrlKWaSWWuX72ZyzUUhIJp6fy1WFv1xdqqLz06DKJ+wzpUqJRxU7zfYDuOBodnSXMyNOKzcUeDw+jRvxvKyspoaWvSxaY9Rw6EIpNJmb98Bg0bZzxeVNWkEpWrVODKhet5VFOhMJMhz5VXfhIjmQWcpqYmxsbGHD58mHbt2nH58mVSUlKoWrXqZ9M0bdqUoKAgqlevzu3bt7lz506mTihkdETfd1R1dHS4e/cuZmZmHD16VBHTrFkzAgMD6d+/P4cPH+b164zFI8zMzPDy8mLOnDkkJyfTvXt3vL29KVdOTAkpTB6lPMZpzHTWbF6Mmpoq0VExjB81DYA69WqyYKkbnSx7fTHuaypWKk9sTHxuVkMQCp1HKY+Z4ODCui1LUVVVIfp+DONGTgWgbr1aLFo+i3Yt7L4YN32WIxKJhEXLZynyPRd5iWlOcxg2YBwz501BXUMN6Tspk/5wI/q+eKRBEB6lPGHyWDdWbvJAVU2VB/djcRw1HYA69Wow19MVa6u+X4zbsXkP5SqUZV+4N2pqqnht2cvZ0xcBGDlwAi7ujqioqPDmzRvGj5xGYkJyvtVXEPKTRC7msRVYkZGRrFy5Ejc3N9zc3EhNTUVVVRUXFxcaNGiAs7MzjRs3xtbWFsjYYuT27dukp6czZcoU7t+/T7ly5bh+/Tq+vr6Eh4dz9uxZ5s+fz8aNG/H29mbBggWoqKjg7OyMuro6rVu3xtfXl2PHjpGUlISTkxOpqanUrl2bgwcPcvHiRdLT03Fzc+PWrVtIpVJGjBiBjY1NjutVXrfu14MEQfiu3snEwi+CkB80lNXzuwiC8NP6J+VifhchRyyN2+RKvmGxR78elEtEJ/MHFBAQgLGxMQ0bNiQ+Pp4BAwZw9OhRxXTX/GZcsvbXgwRB+K7eSt99PUgQhO9OU7VofhdBEH5ahaWT2cKoda7kezwu//YnF9Nlf0CVKlVixowZyGQylJSUmDVrVoHpYAqCIAiCIAiC8GMTPY8fUJ06dfD19cXf3x9fX1/Mzc3zu0hCAdWqbQuOnPAlPDKItZsXo6lV7JviNDTUWbRiNkdP+RFy2p9FK2ajoZExNayZeSP2h/hw+PheAg/voF4DMYItCNlp064loacCOHU+mPVbln62HeYkbtP25cz1mJ7bRRaEQsWyrTn7w304csaXFRsXoKmZfRv7XJySkhIucxw5HLGXY2cD6DvYLkvaHv26sW7H0iyfe6ycybDR9t+1PsKPR55Lr/xU6DuZkZGR2NvnvPEOHz6cpKQkfH19cXbOuknux/lNmzaNa9eufbey/hvOzs74+vqSlJTE8OHDAQgNDWXz5s1fTDdlyhTi4uK+Wzk+Pv+nvrTXplBwldTVYcnK2YwY9Actm1jz4H4sU1zHf1PcmAkjUFFWpq25LW3NbdHQUMdh/DBUVVVYvXERk/6YQbsWdixbtI5la+bldRUFocDT1dVh2eq5/Go/lua/dCT6fgwubhP/VdzocUNp0vSXvCq6IBQKJXVLsHC5G6OHONLWzJaY6DicXMd8U1zfQXZUrFyOjua96N52AEN+60fd+rUAKF5Cm9mLpjJ9rmOmRRYrV63Idr8/6WCdO8/aCUJBV+g7md9q/fr16Ovr5yjW3d2dOnXqfD0wD+jr67N+/XoArl+/Tnp6+hfjIyMjv+vehB+fX/gxtLRqxpVLN4i6l7F/19ZNPtj07PxNcZERF1i2+E/kcjkymYwbV29iZGzI27fv+KVWa25cuwVA+QrGPHnyNI9qJgiFh2Wr5ly6eI2oe9EAbNnojV1P62+Oa2bemFZtLNi6yTtvCi4IhYS5VVOuXr7B/XsZKyzv2Lybbj06flNcu85W7PEKRCqV8uxpGvv8DtO9ZycAOnVvS1LiQ+bPWJopvwFDe+Gz3Y/gwCO5WDvhRyG2MCmgnjx5wtChQ0lOTqZu3brMmDGDOnXqcPv2bQB8fX0Vq6q2atWKrVu3Zkp/8uRJ5s2bh7q6OhUrVlR8bm9vj4ODAwB//vknGhoa/PPPP5iYmLBo0SLU1NTYunUr27dvR0tLi0qVKlGuXDnGjBmDv78/a9asQVNTk3r16vH8+fNM5zc2NlasHrtt2zbOnj2Lp6cnr1694tmzZ0yZMoU2bT7c/YqNjWXgwIGsW7cOb++MPyIMDAxYs2YNGzdupGLFirx48YKOHTvSv39/kpOTGTFiBOPGjWPz5s2KNL6+vly5cgVTU1PCwsJ49OgRDx8+xMrKCmdnZyQSCevWrSM4OBipVIq5uTlOTk7ExcUxcOBAjh07RmxsLE5OTrx48QJTU9Nc/dkKucfQyCDT5uwJ8Uloa2uhqVWM9LTnOYo7Hnpa8bmRcRmGjrRn8viZQMaeq6VK63IwdBc6ujqMGuqYB7UShMLF0LhMpvYVH5eIdvFs2uEX4ooVK8ac+VPpYzecgUN652n5BaGgK2OoT0JckuJ9YnwyWtpaaGoWIz39eY7iyhh9eiyJ6jUztpLz+msvAHZ9Mt8cmum8AAALS7PvXylBKAR+iJHM2NhYpk+fTmBgIM+fP8fLyyvHad+8eYOzszPLly/H19cXDQ2NbOMuXbqEq6srwcHBxMfHc/LkSW7dusWOHTvw9fVl586dREdn3GFOTExk4cKFbN++HR8fH6Kior5aju3btzNnzhz8/PyYM2cOy5YtyzauSpUq9OnThz59+tCzZ0+6d+9OYGAgAIcPH8bS0pIRI0agp6fHunXraNeuHQ8fPuTBg4xRKH9/f8WWJxcuXGDZsmXs27ePK1eucOTIEY4fP87169fZs2cP/v7+JCUlKfJ/b/bs2dja2hIQEECDBg1y9kULBY5ESSnb0W6pVPbNcXVMa+J7YCt/bfAi5PCHja1THj7il9qt6da+P4tXzqZi5fLfsQaCUPgpfaZ9yT5ph5+Lk0gkrN24GNep80hOephr5RSEwupzbUf6ybZOX4r79JhEIlHsNS4I34MYySygfvnlFypUqACAtbU1vr6+OU57+/Zt9PT0qFy5MgA2NjbZdvCqVq2KgYEBAJUrV+bp06dER0djZWWFpqYmAJ07d+bZs2dcunSJBg0aULp0aUWep06d+mI5PDw8CA0N5eDBg1y5coXnz59/Mf49W1tbhgwZwrhx4/Dz82PChAmZjkskEmxsbAgMDMTW1pZHjx5hamrKP//8Q+vWrSlVqhQAnTp14syZM6irq3P16lVFR/TVq1cYGhrSsGFDRZ5nz55l8eLFAHTt2hUXF5cclVXIf45TRtO2gxUAmlrFuPX3HcUxgzJ6pD55yssXLzOliY9NoH7DOp+N62rbkbkeLrhMcsd/7wEAtLQ0ad6iCQf3Zyydff3qTW5e/x81alYl6p/oXK2jIBR0k6aOoX3HVgBoaWty88b/FMfKGOrz5EkqLz5ph7Ex8TRoWDdLnIlJFcpXMGame8YaA3r6pVBSVkZDQ40JY8QCQMLP6Q/nkbRu3xLIuNbdvnlXcUxfcQ17lSlNfFwipg1rZxsXH5uIvkFpxTE9g9IkJiTnci2En8mPuKPkD9HJVFH5UA25XK54L5fLkUgkvHv3+f3hJBJJph+ssrJytnHq6h82U36fRklJCZlMliX209HQj8v3vlxApnL169ePJk2a0KRJE5o2bYqjY86mFhobG2NoaMjhw4cVHchP2djYMGzYMNTU1OjWrZvi84/rKpPJUFZWRiqVMmjQIIYMGQLAs2fPUFZW5smTJ9nWQSKRiO1RCpFF81axaN4qAHRLleToST8qVipH1L0H2A/pzaHgY1nShIeeZvpsp2zj2rRvyax5zvSzG8HVyzcUaaQyKYtWzCIl5THnIy9RrXplKletyKUL+buQliAUBAvnrmDh3BUAlCpVkrCIQCpWKk/UvWgG/dqHg/uzaYfHTjHTfXKWuPPnLtOglpUiztHZgZK6Okx1mp1n9RGEgmbp/LUsnb8WAN1SOhw4vosKlcpy/14M/QbbcTQ4PEuak6ERTJ05Ptu4o8Fh9OjfjZBDxylarAhdbNoz3dE9T+skCIXND9E7uHDhAvHx8chkMvz9/WnWrBk6OjrcuXMHuVzOsWNZL9jvmZiYkJKSwq1bGQuU7N+/P8fnbdq0KeHh4aSnp/PmzRsOHz6MRCLB1NSU69evk5CQgEwm48CBA4o0Ojo63L2bcUctJCRjlCc1NZX79+8zbtw4WrRoQUhIyBenYSgrK2fqoNrZ2TFnzhy6du2aKeZ9HkZGRhgYGODt7Z2pk3nixAnS0tJ4/fo1+/fvp0WLFpiZmREQEMDz58959+4do0eP5tChQ5nO36xZs0xTdF+/fp3j70woOB6lPGaigwt//uVJ6JlAqtesymwXDwDq1qvFofA9X42bPitjNT2PZTM5FL6HQ+F7mLNwGi+ev2TYgHG4uU/mUPgeFq+YjcOISSTEJ322PILwM0pJecy4UVPZuHUZJ87up0bNari5ZDzLZVq/NiEn/L4aJwjC5z1KecLksW6s3OTBodN7MalZlbmuSwCoU68GQaFeX43bsXkPD6Ji2Rfujf/R7eze4c/Z0xfzrU7Cj0dMly2gqlSpwtSpU3n48CFmZmb06NEDJSUlRo4cSalSpWjYsGGWkbj3VFVVWbJkCU5OTqioqFCzZs0cn7datWoMHDiQ3r17U7RoUXR0dFBXV6dkyZLMmjWL3377DRUVFfT09BRpxo4dy+zZs1m5cqVi/8oSJUrQo0cPOnfujIqKCmZmZrx69YoXL15ke95GjRoxefJkSpUqhb29Pe3atWP69OmZOpDvn83csGEDZcuWpVOnThw+fDjTyrolS5Zk+PDhPHnyhK5du2JhYQHArVu36NWrF1KpFAsLC2xsbDJth+Lq6oqTkxM+Pj7Url2bYsWy329KKPiOHT3BsaMnsnx+9fIN2rfs8dW4lk2yroL53pnT5+nSps/3Kagg/MBCjhwn5MjxLJ9fuXSd1hY2X4372KL5K797+QShsAs7eoqwo1kfW7p2+SbWVn2/GieVSpnjsuiL59jrHcRe76Asn08a4/btBRaEH4BE/iNOAs4jUVFRhIeHM3jwYAB+//13evbsSatWrTLFfby67fcml8s5fvw4Xl5erF27NtuYd+/eMWnSJDp06EC7du1yvUxfY1yy9teDBEH4rt5KP//YgCAIuUdTtWh+F0EQflr/pBSOEedGhi1yJd9z8V++MZmbfoiRzPxiZGTEtWvX6NKlCxKJBHNzc6ysrL6e8DuaO3cuoaGhn93DUi6XY2FhQbNmzTJtiZKfpNk8xyoIQu4qplokv4sgCD+lZ29ztpCfIAg/rx9xzE+MZAp5rkyJnE9JFgTh+yiiov71IEEQvru0t9k/+iIIQu57+PR2fhchR34pY5Er+Z5PyPqoU175IRb+EQTh32ndrgUhp/w4cW4/6/7yRFMr++drPxenpa3J+i2ehJ4OIPxMEKPHDc2Sts8AW7Z4r8rVeghCYWDV1pwD4T4cPePHyo0L0dTMvr19Lk5JSYnpcxw5EuHLsbMB9BvcI0ta43KGXLwTRp16H27m9R1kx8GTezgQ7sOf2zzRKVkiV+onCIVB23YtCTsVSMT5g2zcsuyz172cxG3evoL5Hh+2CiqhU5w16xdx7IQfp88F07N3tyxpBCE7P+LCP6KTmYeuXbvGtGnT/lVaLy8vvLy8snzu6+uLs7Pzfy1ajkyZMiXTAkBC4aarq8PSVe4Ms/8Di0adib4fw7QZE74pbtK0sSTEJ2HVrBsdWvVi0NA+NGyUsY1OiRLFWbBkBrPnTUGCJE/rJggFTUldHRYsn8moIU60MbMhJjqWSa5jvymu3yA7KlYuTwfznnRvO4Ahv/Wjbv1airRq6mosWeOOqqqq4jPjcoZMnDqaPtZD6dSyN7EP4vlj8sjcr7AgFEC6ujosWz2PX+3H0PSXDty/H8N0t6xbxuUkzmHcMMya/pLpsxWr5xMfn0grCxvsug1m7sJplDHURxB+RqKTmYfq1KmDu/u/21epb9++9O3b9+uBuSgyMvKHnDP+s2rZqjmXL14n6l40AFs2eWPbs8s3xU2fPJeZ/7+dib5+adTU1Eh7lg5AV5sOJCYmM2u6R15URxAKNAsrM65dvsH9ew8A2L55N916dPymuHadW7HbKwCpVMqzp2ns8ztE956dFWlnLZjCXu9AnjxOVXymrKyMqqoKxTSLIpFIKFJUg9ev3+RiTQWh4LJsZc7li9e49//Xs782etGjZ9ZV0r8W18y8Ma3aWLBlk7fisxI6xWlp1UyxwnNCfBLtW/Ui9cnT3KyS8IOQy+W58spPYuGfbxQZGYmHhwcymQwjIyOKFi3KnTt3kEqlDB8+nC5duvD27VtmzJjBhQsX0NfXRyKRMGrUKABWrlzJtm3biIqKwtXVldTUVIoWLcq0adOoW7cuzs7OaGpqcuPGDZKSkhg9ejR2dnasWJGxcfeYMWPw9/dnzZo1aGpqKsoAcPr0aebPn49cLsfQ0JDFixdTtGhR5s6dS0REBBKJhK5duzJixAgiIyMVZQFwdnamcePGNG7cGAcHB6pWrcrNmzfR1dVl2bJl7Nq1i+TkZEaMGMG4cePYvHkz3t4Zv1x9fX25cuUKM2fOzIefiPBvGRoZEB+XqHifEJeEdnEtNLWKkZ72PMdxUqmUlX8uoHO3dgTvO8rdO1EAbN3sA0Cvft3zpkKCUICVMTQgIe7DPrGJ8cloaWuhqVmM9PTnOYorY6Sf6VhCfDLVa1YFoNcAG1RUVfDZ5sfo8cMUMdFRMaxbuZWjZ/xJe5pGWlo6dh0G5WZVBaHAMjI2IO6j61l8XGK2170vxRUrVgz3+dPoYzeMgUN6K2IqVixHUtJDfh89hNZtW6CmrsaqFRu598/9PKmbULjl99TW3CBGMv+F+/fvs2XLFsqXL0+tWrXw9fVlx44drF27lpiYGLy9vXn58iUHDx5k3rx5XLt2LUseTk5O2NvbExQUxJQpUxg3bhxv3mTcXU5MTGTnzp2sWbOGhQsXZkqXlJTEokWL2LFjBz4+Pjx/nvFL8c2bNzg6OrJgwQKCgoKoVq0afn5+eHl5kZCQQGBgILt37+bw4cOEhYV9sX63bt1iyJAh7Nu3D21tbYKCghgxYgR6enqsW7eOdu3a8fDhQx48yLjT7u/vj62t7Xf4ZoW8pKSklO1dLqlU9s1xDr9Nplbl5ujoFGfC5FHfv7CCUMgpKUmyb0cyaY7jPm2LEklGO6xVtzr9B/fAxTHrTBlzSzM6dGmNuWkHmtRqy5HgMDxWihuCws/pc9czWQ6vexKJhHUbFzN96jySkh5mOqaqqkqFCmVJS0unc/u+jPh1PHPmTqFuvVpZ8hGEn4EYyfwXKlasiJaWFqdPn+bVq1fs3bsXgBcvXnDnzh1OnTpFr169kEgkGBkZ0bRp00zpnz9/zoMHDxR7VtarV4/ixYtz7949AJo3b45EIqFatWqkpqZmSnvp0iXq169PqVKlALC2tubMmTPcvn0bfX19atSoAcDEiRMBGDt2LDY2NigrK1OkSBGsra2JiIjIspfnx3R1dalZM2PRiKpVq/L0aeapHhKJBBsbGwIDA7G1teXRo0eYmpr+m69SyGNOUx1o1zHjZ6+lVYybf99RHCtjqM+TJ095+eJlpjRxsQnU/6VutnGWrZpz8+//kZT4kBfPX+C39wCdu7bNm8oIQgH3h/PvtGnfEgBNrWLcvnlXcUy/jB6pT57y8sWrTGni4hIxbVgn27j42ET0DUp/OGZQmsSEJGx7dUFTqxh7DvwFgJ5BaTzXujPPbSkWlmaEHArnUcoTALZt9OHgiT25VWVBKHAmTx1Lh/+/7mlqa3Lzxv8UxzKuZ6m8+OS6FxuTQIOGplniTEyqUL5CWWa7Z6yFoadfCiVlZdQ11PFclLFX+c4dvgBE3XtA5JmLNGhYl6uXb+RqHYXCTy5GMgUADQ0NAGQyGR4eHgQEBBAQEMCuXbuwsLBAWVkZ2Rf2gszu7phcLkcqzbijra6esdWARJJ1sRSJJPNdbhWVjPsEqqqqmeLT0tJITEzMUo735/k0n7dv3yr+/f782Z3vPRsbG/bv38++ffvo1k2snlZYeMxdSVsLW9pa2NK5TV8a/lKXipXKAzBwSG8OHTiWJU3YsVOfjbO26cDEyaMBUFNTpWv3Dpw6HplHtRGEgm3p/DV0sepDF6s+2HUYSP2GdahQqRwA/Qf34GhwWJY0J0MjPht3JDiMnv27oaysjJa2Jl1s2nP4QBizXRbRukl3xbmSEx8yfuQ0Qg6Gc+PqLazaWlC0WMY+qR2s23D5QtbZNYLwo1owdzlWFt2xsuhOx9a9aNjIlEr/fz0b/GsfDu4PyZIm7NjJbOPOn7tMvVqWivz+2uRNgO8Bxo9x4UF0LFcuX6dP3+4AlC6tS6PG9bl86Xqe1VUQChLRyfwPzMzMFCu+Jicn07VrVxISEmjWrBkHDhxALpeTlJTE2bNnM3UANTU1MTY25vDhwwBcvnyZlJQUqlat+tVzNmzYkMuXL5OUlIRMJuPAgQNAxujqo0ePuHs34075hg0b8PLywszMDH9/f6RSKS9fviQoKIgmTZqgo6NDTEwMr1+/JjU1lQsXLnz13MrKyoqOsJGREQYGBnh7e4tOZiH1KOUxf4x2Yf1WT45HBlG9ZlVmTsuYnm1arxZHTvh+NW6my0K0tDUJPR3AobA9XL1yg/VrtuVbnQShoHqU8oRJY91YtcmDw6f3YlKzCu6uSwCoU68m+0K9vxq3Y/NuHkTFsj/ch4CjO9i1w5+zp7/8u3v3zgBCj54kMGQnB8J9MGveECcH19ytrCAUUCkpjxk3agobty7n1NkD1KhZDVeXBQCY1q9N6An/r8Z9yaD+Dli1NufEmX3479/GooWruHxR3NQRvk4ml+fKKz9J5Pm99FAh8/GCOenp6bi5uXHr1i2kUikjRozAxsaGt2/fMmvWLC5dukTp0qV5/Pgxs2fP5uXLl4q0//zzD25ubqSmpqKqqoqLiwsNGjRQLMDz/hlHExMTbt++nWnhn4MHD7Js2TKKFClClSpVUFJSYv78+Zw9e5b58+fz9u1bypUrx8KFC1FTU2PBggWcOXOGt2/fYm1tjYODAwCurq6cPn0aIyMjSpUqRfPmzWncuDEDBw7k2LGMkaqPz+vu7s7x48fZsGEDZcuWVTzjuX79+m/6DsuUqPn1IEEQvqsiKupfDxIE4btLe/siv4sgCD+th09v53cRcqS2vlmu5Hs96Uyu5JsTopOZC8LCwpDL5VhZWZGWlkb37t3Zu3cvJUqUyO+ifTfv3r1j0qRJdOjQQfFsaU6JTqYg5D3RyRSE/CE6mYKQfwpLJ7OWfpNcyfdGUv49wiQW/skFlStXZtKkSSxduhTIWHznR+pgyuVyLCwsaNasGW3atMnv4giCkAPP3778epAgCN9dEWW1/C6CIAgFXH5Pbc0NYiRTyHNiJFMQ8p5M/vnFyARByD3qyqr5XQRB+Gk9eFw4nomtodc4V/K9mXw2V/LNCbHwjyD8xFq3a0HIKT9OnNvPur880dQq9k1xWtqarN/iSejpAMLPBDF63NAsacuWN+LvqAhMxV5hgqDQpl1LQk8FcOp8MOu3LP1s28tJ3Kbty5nrMT3L530H2LLNe813L7sgFGat2lpw6MReQiMDWbN58Wfb3tfiyhjpc/b6UXRKllB8VtWkEnsPbCE4fDcHwnbRolWz3KyK8AOR59L/8lOB7GRGRkZib2//n/NZvnw558+fB2DatGlcu1aw72Z8XN7/IiYmhqlTp36HEn3g5eWlWEn3Y76+vjg7O3/Xcwl5Q1dXh6Wr3Blm/wcWjToTfT+GaTMmfFPcpGljSYhPwqpZNzq06sWgoX1o2OjD3mLq6mqs/HMBaqriTr4gvKerq8Oy1XP51X4szX/pSPT9GFzcJv6ruNHjhtKk6S+ZPiuhU5yFnm7MmT8t262wBOFnVVJXh0UrZ/PboPFYNenKg/uxOLv+8c1xdr2t2b3vLwwM9TOlm+Phgs8OPzq27InTGFdWb1qEsrJyLtdKEL6PoKAgOnXqRLt27dixY0eW4zdv3sTW1pb27dszbdo03r1798X8CmQn83s5d+6cYssNd3d36tSp85UU+evj8v4X8fHxxMTEfIcSfdC3b1/69u37XfMU8lfLVs25fPE6UfeiAdiyyRvbnl2+KW765LnMdPEAQF+/NGpqaqQ9S1eknbtoOrt2+vP48ZPcro4gFBqWrZpz6eK1D21qozd2Pa2/Oa6ZeWNatbFg6ybvTOm62nQgMSEZt+kLc7EWglD4tLBqxpVLN7h/7wEA2zb50L1n52+K0zcoTbtOrbDvMTJLOmVlJYqX0AagmGYxXr96k1tVEX4w+b2FSVJSEp6enuzcuRN/f398fHwU2yK+5+TkhKurK4cOHUIul7Nr164v5llgF/55/Pgxw4cP58GDB1SsWJHly5dz4MABtmzZgkwmo1atWsyYMQN1dXW2b99OQEAAL1++RFVVlcWLF3P16lWuX7+Oi4sLK1euZM6cOYqtO/788080NDT4559/MDExYdGiRaipqbF161a2b9+OlpYWlSpVoly5cowZM4bQ0FCWLl2KTCajbNmyzJo1i1KlStGqVSvq1q3LzZs32bx5M25ubqSkpAAwevRoqlSpwqBBgzh27BhKSkpERkayfv165syZg6OjIy9evEBJSQkXFxfu37+fqbwaGhqKLU40NDSYPn06NWvWxNnZmSJFivD333/z7NkzJkyYQEBAALdu3aJNmzY4OzszZ84cYmNjmTlzJunp6TRq1IhevXoBYG9vj6OjI4sWLaJ69eqcP3+e169fM3XqVMzNzUlJScHV1ZXExEQkEgkTJ06kWbNmmbYy8ff3Z82aNWhqamJkZETRokXz5z8S4T8xNDIgPi5R8T4hLgnt4lpoahUjPe15juOkUikr/1xA527tCN53lLt3ogDoZ2+HqqoKO7buYZzjb3lXMUEo4AyNy2RqU/Fxidm3vS/EFStWjDnzp9LHbjgDh/TOlP/WTT4A9O5nk8s1EYTCxdDIgISPr2fxSWhrZ3/d+1xcUuJDfhs0Ptv8XZzm4h2wgWG/D0S3VEkchjl9l8ED4ceX31NbT58+jZmZmWKh0vbt23Pw4EFF3ykuLo5Xr15Rr149AGxtbVm+fDn9+vX7bJ4FdiQzPj4eV1dXgoODSUlJYffu3ezatQtvb28CAgLQ1dVl48aNpKenc/ToUbZt28a+ffuwtLRkx44ddO/endq1azNnzhxMTEwy5X3p0iVF3vHx8Zw8eZJbt26xY8cOfH192blzJ9HRGXeOHz16hKurK6tWrSIoKIgGDRowa9YsRV4tWrTg0KFDREZGYmRkhK+vL+7u7pw/f57y5ctjbGxMZGTG8sH+/v7Y2tqyZ88eLC0t8fX1ZezYsVy4cCFLeSdPnoyTkxN+fn7Mnj2b8eM//EJLTk7Gx8eHESNGMGXKFGbOnIm/vz+7du0iLS0NFxcXateuzYwZM7CzsyMgIADI+A/k8ePHmJpmTGdMT0/Hz8+PxYsX4+zszJs3b3B3d8fOzg5fX1/WrFmDq6sr6ekfRqaSkpJYtGgRO3bswMfHh+fPP/xSFgoXJSUlslv3SyqVfXOcw2+TqVW5OTo6xZkweRR1TGsw8NfeTB4/8/sXXBAKuc+1KVkO255EImHtxsW4Tp1HctLDXCunIPxoJEqSHF33chr3MXV1NVZt8mDiaBea1G5Dzy6DmbfElTJG+p9NIwi57dmzZ8TGxmZ5PXv2LFNccnIypUuXVrzX09MjKSnps8dLly6d6Xh2CuxIZvXq1SlbtiyQsSXIkydPiI6OVozIvX37lpo1a6KpqcnixYvZv38/9+/f58SJE9SoUeOLeVetWhUDAwNF3k+fPiU6OhorKys0NTUB6Ny5M8+ePePq1avUrVsXY2NjAHr37s26desUeb3vsNWvX58lS5aQlJSEpaUlo0ePBsDOzo7AwEDq1avHmTNncHNzQ19fnzFjxnDz5k1atmzJgAEDMpXv+fPnXL9+nSlTpig+e/HiBU+eZEw5bNGiBQCGhoZUrVoVXV1dAEqUKMHTp08z5dWkSROmT59ObGwsAQEBdOvWTXHs/XdZo0YNSpcuze3btzl9+jT37t1j+fLlQMZ+mB9Pvb106RL169enVKlSAFhbW3PmTP5t9Cp8G6epDrTr2AoALa1i3Pz7juJYGUN9njx5yssXmbe6iItNoP4vdbONs2zVnJt//4+kxIe8eP4Cv70H6Ny1LdrammhqaRJ4eCcA+gZ6rFy/kNmuizgcHJoHNRWEgmXS1DG0f9/2tDW5eeN/imMZbSqVF5+0vdiYeBo0/LTtpWJiUoXyFYyZ6Z7xPLyefimUlJXR0FBjwpisCwAJws9swpTRtO1gCYCWlia3PrruGZTRIzWb6158bCL1P2p7n4v7mEmNKhQpokHI4eMAXDp/lf/d+of6DeuSEHfkO9ZI+BHl1hYmW7ZsYeXKlVk+d3BwYMyYMR/OL5NleoZfLpdnev+149kpsJ1MFZUPRZNIJGhpadGxY0dcXFyAjI6YVColISEBe3t7BgwYQIsWLShVqhQ3b978Yt7q6h82JZdIMu5WKSkpIZNlvUP16WdyuTzTg67v86pQoQLBwcGcOHGC0NBQNm3axIEDB+jQoQOenp4cOnSIFi1aoK6uTsOGDdm/fz9hYWEcOHAAPz8/Nm/enOmcampqihFIgMTERMUQtupHi6h8/D1lRyKR0L17d/bv309wcDAbN25UHPv4YXSZTIaKigoymYwtW7YozpWcnIyuri5Hjx7N9H3l9PxCweIxdyUeczN+2eiWKknoaX8qVipP1L1oBg7pzaEDx7KkCTt2ihlznLKNs7bpQCfrtkwa74aamipdu3fgeNhp1q3eiuuU+Yo8zl49gsPwSVy5fCNvKioIBczCuStYODfjsYNSpUoSFhGoaFODfu3Dwf1Z2174sVPMdJ+cJe78ucs0qGWliHN0dqCkrg5TnWbnWX0EobBYMm8VS+atAjKue4dP+lKhUjnu33vAgCG9sr3xeTz0NC6zHb8a97H792LQ0takYWNTLpy9QvkKxlQ1qcSNq1/+m1QQctOgQYOwscn66IS2tnam9wYGBpkWH3348CF6enqZjj98+GHmTEpKSqbj2Smw02Wzc+TIER49eoRcLsfNzY0tW7Zw7do1ypcvz+DBg6lTpw5Hjx5VzH9XVlbO8Vz4pk2bEh4eTnp6Om/evOHw4cNIJBJMTU25cuUKsbGxAPj4+NCkSZMs6bdv386KFSvo2LEjM2bM4PHjx6Snp1OkSBFatGjBkiVLsLW1BWDhwoUEBgZiY2ODq6srf//9d6byamlpUaFCBUUn89SpU/Tv3z/H35OysnKmjrCtrS3e3t6UKVMGff0P0zYOHDgAwLVr13j27BnVqlXDzMyMnTszRp/u3r2LtbU1L19+uHPXsGFDLl++TFJSEjKZTJGHUPg8SnnMH6NdWL/Vk+ORQVSvWZWZ0zIWCjGtV4sjJ3y/GjfTZSFa2pqEng7gUNgerl65wfo12/KtToJQGKSkPGbcqKls3LqME2f3U6NmNdxcFgBgWr82ISf8vhonCMK3e5TyGEeH6az9awkhZwKoXrMqs/9/8bq69WoSHL77q3Gf8+xZGiPs/8BtrjOHT/qy5q8lOI+fRfT92Fyvl1D45dYWJtra2hgbG2d5fdrJbNasGRERETx+/JiXL19y+PBhxcxJACMjI9TV1blw4QIAAQEBmY5np9AMQ2lpaeHg4MCgQYOQyWTUqFGDESNG8O7dO7y8vOjUqRNyuZxGjRpx507GVAgLCwtmzJjBggVfvyhXq1aNgQMH0rt3b4oWLYqOjg7q6uqUKlWKWbNm4eDgwNu3bzE0NMTd3T1L+u7duzNhwgSsra1RVlbGyclJ8QPs3LkzFy9eVEyttbe3Z+LEifj6+qKsrKwo38fl9fDwwM3NjQ0bNqCqqoqnp2eOl6KvXLkyaWlpODk54eHhQZkyZShTpkyWOxkxMTGKzzw9PVFWVsbFxQVXV1esrTNWMFy4cKFiCjFAqVKlcHFxYfDgwRQpUoQqVarkqExCwXTsyHGOHTme5fMrl2/Q1sL2q3HPnqbx+1DHr56ncd22/62ggvCDCTlynJDs2t6l67S2sPlq3McWzc86FQrAZ6cfPjv9/ltBBeEHE3r0BKFHT2T5/Orlv+nYsudX4z5WrmTmXQsiTp7Duo1YiV/4dnL555/3zQv6+vqMHz+egQMH8vbtW3r06EHdunUZPnw4Y8eOpU6dOixatAgXFxfS09OpVasWAwcO/GKeEnl2Tzb/hKKioggPD2fw4MEA/P777/Ts2ZNWrVr9p3ylUimenp7o6uoyZMiQ71DSbyOXy0lOTsbe3p59+/ahpqYGZHR0HRwcsh2VzW1lStTM83MKws9Ols8XMEH4Wakri32CBSG/PHh8Lb+LkCMVdU2/HvQvRD26kiv55kShGcnMbUZGRly7do0uXbogkUgwNzfHysrq6wm/ws7ODh0dHdasWfMdSvntDh06hJubG25ubooOZn4Tf+wKQt57JX2b30UQhJ9SURX1rwcJgvBTk+XzFia5QYxkCnlOv3j1/C6CIPx0RCdTEPJHSXXNrwcJgpAr8nMk71uU16379aB/IfrR1VzJNycK1cI/giB8X23atST0VACnzgezfstSNLWK/eu4TduXM9fjw/YJ9RrUJujQTkJO+BF2OhC7Xta5Vg9BKGzatbfk1Jn9nL94hC3bVqCllX1H5Etxw4b35/jJAM5eOMS6DYsVs1U6dGzF/QcXOHE6SPHS1My+bQvCz8yqrQXBx3cTEhnAqk0en70Gfi2ujKE+EdePoFOyRB6UWvgRyeXyXHnlpx+uk5mUlMTw4cPz7HwrVqxgxYqMZenf70F59epVPDwyViELCQlh2bJl/yrvZcuWERIS8q/SLl++PNNSxN/Dx3tsfqxVq1aK1XeFwkNXV4dlq+fyq/1Ymv/Skej7Mbi4TfxXcaPHDaVJ018yfbZx63I85q2gtYUNfXsMZ9ZcZypWKp+rdRKEwkC3VElWr12Iff/R/NKgLfejYnCb5fRNcdZd2zFi5EC6WQ+kyS8dKFJEg9EOGc/9N2nSgBXLN2DRzFrxSk9/nqd1FISCrqSuDgtXzOL3wRNp3aQbD6LjmOQ67pvjbHt3wWffZgzKfHk7B0H42fxwnUx9fX3Wr1+fL+d+v+XI3bt3efToEQCtW7dm3Lisv7RyYty4cbRu3fpfpT137lyOt2/JqY/37RQKP8tWzbl08RpR96IB2LLRG7ueWUcbvxbXzLwxrdpYsHWTt+IzdXU1Fi9YxfGwCAAS4pNISXmMoZFBblZJEAqFVq3MuXjhKvf+uQ/Axg076Nkr6028L8X16WvLyuUbefLkKXK5nD/GTcfbyx+AxmYNaNGyKScj9hF82JtmzRvlRbUEoVCxsGrK1UvXuX/vAQDbN+2iW49O3xSnZ1Catp1aMajn73lXcOGHJEOeK6/8VKgX/nn37h1ubm7cuXOHlJQUTExMmDhxIsOHD+fYsWM4OzuTmppKdHQ0Tk5OaGhoMH/+fORyOYaGhixevJiiRYsyd+5cIiIikEgkdO3alREjRhAZGcmff/6JhoYG//zzDyYmJixatAg1NTU2bNjArl270NHRQVtbm7p1M+ZRm5iYcO7cOZYvX86LFy9Ys2YN+vr6nD17lvnz53P58mXc3d15/fo1Ojo6zJo1i/Lly2Nvb0+dOnW4cOECjx8/xsXFhZYtW+Ls7Ezjxo1p3LgxDg4OVK1alZs3b6Krq8uyZcsoUaIEBw4cYPny5RQtWpQaNWoglUoxMzPj+vXruLi4sHLlSn777TeOHTuGkpISkZGRrF+/nuHDh7N69WpUVFSIjY2lbt26uLu7o6amhr+/P1u2bEEmk1GrVi1mzJiBuro6JiYm3L59m9TUVJycnEhMTKRy5cq8fv06n/9LEP4NQ+MyxMclKt7HxyWiXVwLTa1ipKc9z1FcsWLFmDN/Kn3shjNwSG9FzOvXb9i5ba/ivf3gXmhqFuPCucu5WylBKASMjcsQF5egeB8Xl0jx4lpoaWmSlpaeo7gqVStw8YIue/0yRlAiTp/D9f/30HzyOJXduwIJ8D+IWdOGeHn/SfOmXYiP/9COBeFnV8bIgIS4JMX7xPgktLWzXgO/FJec+JDfB03I03ILP6b8ntqaGwr1SOalS5dQVVXFx8eHI0eOkJaWRnh4eKaYEiVKEBwcjLm5OY6OjixYsICgoCCqVauGn58fXl5eJCQkEBgYyO7duzl8+DBhYWGK/F1dXQkODiY+Pp6TJ09y7do19u7di5+fH5s3byYxMfNFW1tbm7Fjx9KqVSt+//3Dna03b94wYcIEpk+fTmBgIH369GHChA+/mN6+fYuPjw9TpkzJdnrtrVu3GDJkCPv27UNbW5ugoCAeP37M3Llz2bJlC3v27OHp06dAxp6dtWvXZs6cOZiYmGBsbExkZCQA/v7+2NraKuo3bdo0Dh48yOvXr9mxYwd37txh165deHt7ExAQgK6uLhs3bsxUluXLl1OzZk2CgoLo378/KSkp//InKOQnJSWlbH+pyaSyHMVJJBLWblyM69R5JCc9/Ox5xowfjtMUB+z7/M6rV+KGhCB8rk19OvvkS3GqKqpYtjJn8MAxWFp0R0enBNNnZExjH9BvFAH+BwE4E3GByMiLWLVqngs1EYTCS0lJgjybkR5plmtgzuIEQcisUI9kNmrUiBIlSrBjxw7u3bvH/fv3efHiRaaY96OMt2/fRl9fnxo1agAwcWLGxXjs2LHY2NigrKxMkSJFsLa2JiIiglatWlG1alUMDDKm91WuXJmnT58SFRVFy5YtKVYs46HvDh06IJN9/RfN/fv3M416duzYEVdXV9LS0gCwsLAAoGrVqqSmpmZJr6urS82aNRUxT58+5fz589SvXx99fX0go3N59OjRLGnt7OwIDAykXr16nDlzBjc3Ny5fvkyjRo2oVKkSkPG85a5du1BVVSU6OppevXoBGZ3f9+d97+zZsyxevFjxMyhbtuxX6y8UDJOmjqF9x4y9X7W0Nbl543+KY2UM9XnyJJUXL15mShMbE0+DhnWzxJmYVKF8BWNmujsDoKdfCiVlZTQ01JgwZjpqaqosXzOfaiaV6dy2LzEP4vKghoJQME11+YOOnTIef9DW0uTGjduKY4aG+jx5nLXtxcTE0/AX02zjEhKTCAo8pBj59PH2Z7LzGIoX12LY8AEsXvRh2yyJRMLbt+9ys3qCUCiMdx5Fmw4tAdDU0uT2zTuKYwZl9Eh98pSXn7TD+NhE6jWs89U4QfgvZGIks2AJCQnB0dERDQ0NbG1tadSoEYaGhpliNDQ0AFBVVUUikSg+T0tLIzExMUsHUS6XK+4mq6t/2NtKIpEgl8sV//+eikrO+unZdUSzO9fHZfxYdmVRUlLKUQe3Q4cOnDp1ikOHDtGiRQtFXsrKypnKoqysjFQqpWPHjgQEBBAQEMDu3btxdXXNlN+n38HH+QgF28K5GQvxtLawoVPr3jRsZKpYjGfQr304uP9YljThx05lG3f+3GUa1LJS5Ldlkw8BvsFMGJOxwuyq9Rkr8HVpJzqYgjB3zlLFIjytW/WgUeP6VKpcAYBfh/Zj//6sNwiPHTv52bgA/4PY2HZCQyPj93mXLu24ePEqaWnPGTZiAF27tQegbt2aNPzFlKNHj+d+JQWhgPOcv5rOlr3pbNkb2/b21G9YlwqVygHQb0hPjgSHZUlzIjQiR3GCIGRWqDuZERERdOzYETs7O7S1tYmMjPzsYjcVK1bk0aNH3L17F4ANGzbg5eWFmZkZ/v7+SKVSXr58SVBQEE2aNPnsOZs2bUpoaChpaWm8fv2aI0eOZIlRVlbm3bvMd40rVapEamoqV69m7Fdz4MABDA0NKVGixL+sPTRo0IBr166RnJyMXC7nwIEDik7q+w4jQJEiRWjRogVLlixRTJUFuHDhAklJSchkMvz9/WnRogVNmjThyJEjPHr0CLlcjpubG1u2bMnyHbxfBOjq1as8ePDgX9dByD8pKY8ZN2oqG7cu48TZ/dSoWQ23/3+my7R+bUJO+H017nN+aVSPrt07ULFSecU2JiEn/LBsbZ7r9RKEgi7l4SNGjZzM1u0rOXvhEDVrmeAydS4A9evX4cTpoK/GbVi3nbDQU4SfDOD8xSMU0yzKLLfFyGQy+vUeyZixw4g4G8yqtQsYMmgsjx89ybf6CkJB9CjlMU5jXFm9eRFHIvwwqVEV9+mLAKhTryb7w3y+GicI34s8l/6Xnwr1dNmePXvi6OjI/v37UVVVpUGDBopnDz+lrq6Oh4cHkyZN4u3bt5QrV46FCxeipqbG/fv36datG2/fvsXa2pq2bdt+Np8aNWowaNAgevTogba2dpaRU8iYorty5UoWLVqkmI6qpqaGp6cns2fP5uXLlxQvXhxPT8//VP+SJUvi4uLCr7/+ipqaGsbGxmhrawMZ029nzJjBggULaNCgAZ07d+bixYuYmn6YeqWnp8ekSZNISkqiefPm9OzZE2VlZRwcHBg0aBAymYwaNWowYsSITOcdO3Yszs7OdO7cmUqVKonpsoVYyJHjhBzJOsJx5dJ1WlvYfDXuY4vmr1T8+/y5y+gXr/79CioIP5gjh8M4cjgsy+eXLl3Dopn1V+NkMhkL5q1gwbwV2ebRtnXP71lcQfghhR09SdjRk1k+v3b5bzpb9v5q3Mcq6pp+8bggfMmPuPCPRP4j1uon8eTJE7Zt24aDgwNKSkrMmTNHsVrtx6RSKZ6enujq6jJkSMY+apGRkaxcuZJt27blebmLa1bO83MKws+ulEbx/C6CIPyUop8lfT1IEIRc8e5N4XhcJ7duzCc9vZUr+eZEoR7J/NmVKFGCZ8+e0aVLF5SVlalVq5ZiwZ6P2dnZoaOjw5o1a7LJRRAEQRAEQRCE/JLfe1rmBjGSKeQ5MZJZcLRrb8mMmU6oq6lx48YtHEZNybRPX07ihg3vz8BBvdAoosHlS9dxGDWFN2/eKNKWL29M+IkAbLoN5tKla3lWNyEzMZKZ/yzbmuPkMgY1dVVu3bjDlHGzSE9/nuM4JSUlps6aQIvWTVFRVmH96q14/bU3U9oe/brRrrMVI/r/ofis7yA7Bg7vg0wqI+ZBHFPGzeLJ49Rcrq3wnhjJLDw6dWzNnDnOqKurc+3aTYaPmJjtNfG9TRuXcv36TZZ4/pmHpRS+RWEZySxd3CRX8n349PbXg3JJoV74R8iY9vrp9NiPhYaGsnnzZgC8vLzw8vICYMqUKcTF5bzhxcbG0qpVq/9WWKFA0S1VktVrF2LffzS/NGjL/agY3GY5fVOcddd2jBg5kG7WA2nySweKFNFgtMMQRVp1dTXWbViCqppqntVLEAqikrolWLjcjdFDHGlrZktMdBxOrmO+Ka7vIDsqVi5HR/NedG87gCG/9aNu/VoAFC+hzexFU5k+1zHTKuXG5QyZMHUUfa2H0bllb+IexDNu8si8qLIgFCqlSpVkw/ol9Oo9glq1WxAVFc1c96nZxlavXoUjh3ZhZ9s5j0sp/KjkcnmuvPKT6GT+4K5fv056esZduL59+9K3b18go3Oa3//xCfmrVStzLl64yr1/7gOwccMOevbq9k1xffrasnL5Rp48eYpcLuePcdPx9vJXpF28ZCY7d+zlkVjZUvjJmVs15erlG9y/FwPAjs276daj4zfFtetsxR6vQKRSKc+eprHP7zDde3YCoFP3tiQlPmT+jKWZ8lNWVkZVVYVimkWRSCQUKarB69evc7GmglA4tW3bkvPnr3D3bhQAa//cSr++NtnG/j5yMBs372TP3n15WUThByaTy3PllZ/EM5k/iLNnz+Lp6cmrV6949uwZU6ZMoUKFCnh7ewNgaGhIfHw8kLHSbnJyMiNGjGDHjh3Y2dmxdetWjI2NMy0I9PfffzNt2jQAqlf/8EBySkoKrq6uJCYmIpFImDhxIs2aNcv7Sgv/ibFxGeLiEhTv4+ISKV5cCy0tzUzTg74UV6VqBS5e0GWv32YMyugRcfocrv+/vcnAQb1QUVVly18+THQalXcVE4QCqIyhPglxH6ZNJsYno6WthaZmsUxTZr8UV8bo02NJVK9ZFUAxbdauz4eVaQGio2JYv3IrR874kfY0jbS0dHp0GJwbVRSEQq2ssSExsfGK97GxCRQvrp3lmggw7g8XANq2aZmnZRSEwkSMZP4gtm/fzpw5c/Dz82POnDksW7aMKlWq0KdPH/r06YOdnZ0idsSIEejp6bFu3Tp0dHQ+m+fkyZNxdHTEz88PY2Njxefu7u7Y2dnh6+vLmjVrcHV1VYyWCoWHkpJStqPZn+41+6U4VRVVLFuZM3jgGCwtuqOjU4LpMyZialqLX4f2Y/w4l1wrvyAUJp9tR7IctjeZNMsxiUTy2b2h3zO3NKN9l9ZYmHbErFY7jgaHs3DlzH9ZC0H4ceX0migIueFHnC4rRjJ/EB4eHoSGhnLw4EGuXLnC8+dZF5P4Fo8fPyY5OZnmzZsDYGtry969GXfKT58+zb1791i+fDkA7969IyYmhho1avy3Sgi5bqrLH3Ts1BoAbS1Nbtz48EC4oaE+Tx6n8uLFy0xpYmLiafiLabZxCYlJBAUeUtzl9fH2Z7JzxvNjWlqaHA7ZDUCZMnqs37iE6S7zCT4Qkqt1FISC4g/nkbRunzHSoalVjNs37yqO6ZfRI/XJU16+eJUpTXxcIqYNa2cbFx+biL5BacUxPYPSJCYkf7EMrTu0JOTQcR6lZExZ37bRh+ATu/9z3QThR+A2w5EuXdoBGdfE6zc+bPdgZGTA48dPslwTBUHIGTGS+YPo168fV69epXbt2owc+e2LOry/2/Hu3Tsg4w75x3dAlJWVFf+WyWRs2bKFgIAAAgIC2LVrF9WqVfuPNRDywtw5S7FoZo1FM2tat+pBo8b1qVS5AgC/Du3H/v1Hs6Q5duzkZ+MC/A9iY9sJDQ11ALp0acfFi1eZMnkODeu3UZwrISGZ4UMniA6m8FNZOn8t1lZ9sbbqS48Og6jfsA4VKpUFoN9gO44Gh2dJczI04rNxR4PD6NG/G8rKymhpa9LFpj1HDoR+sQw3rt7Cqq05RYsVAaCDdWsuXxCrPAsCgNvMRfzSqB2/NGpHcwtrmjRuQJUqFQH4bYQ9gUGH87mEws9ChjxXXvlJjGT+AFJTU4mPj2fnzp2oqamxaNEixfQOZWXlbBd5UFZWVsTo6Ohw9+5dypYtS0hIiOIzQ0NDwsLCsLS0ZN++Dw+3m5mZsXPnTkaNGsXdu3fp378/ISEhaGpq5kFthe8l5eEjRo2czNbtK1FTUyXq3gNGjnAEoH79OixfNReLZtZfjNuwbjs6OsUJPxmAspIyV67cYNrYuflZLUEokB6lPGHyWDdWbvJAVU2VB/djcRw1HYA69Wow19MVa6u+X4zbsXkP5SqUZV+4N2pqqnht2cvZ0xe/eN49OwMwLleGgJAdvHn9lrjYBCY5zMj1+gpCYfPw4SOGDZ+Aj/c61NRUufdPNIN/HQdAwwZ1+fPPjA6pIAg5I/bJLOTeL9RTs2ZNQkJCUFFRwczMjODgYEJDQ7lx4waTJ09myJAhpKamAjBmzBjc3d05fvw4GzZs4N69e8yePZvixYtjbm7OxYsX2bZtG3fu3GHKlCm8e/eOevXqcfz4cY4dO0ZSUhKurq6KhYQcHR1p2TLnD7+LfTIFIe+JfTIFIX+IfTIFIf8Uln0ytYtVypV8nz2/lyv55oToZAp5TnQyBSHviU6mIOQP0ckUhPxTWDqZmkUr5kq+6S+iciXfnBDTZYU8J5XJ8rsIgvDTSXzxOL+LIAg/pRIaxfK7CIIgCHlOdDIFQRAEQRAEQRDyiTyfF+nJDWJ1WUH4ibXvYMWZyGAuXg5h2/ZVaGllv3jT5+K271jN6TP7Fa+4hCv47F6fKa39wJ7s2rMh1+siCIVJ+w5WREYGcykHbS+7uO07VhNx5oDiFZ9wlV2ftL3y5Y2Jib1M/QZ1cr0+glBYtG1vSfjpQM5cOMjGLcvQ1Mp+pDkncX9tX8n8Ra6K9+YWTTh23I/w04H479tKrdrVc60eglDQiU7mZ6SlpTF69Ogvxjg7O+Pr6/ufzxUZGYm9vf1/zudj3zu/a9euMW3atCyfx8bG0qpVq+96LiFvlCpVkrVrF9K/3+80qNeaqKgHzJo96ZviBvQfRTOzzjQz64zD6Ck8fZrGhD8yLrg6OsVZtnwOCz1ckUjytGqCUKCVKlWSP9d60K/f79Sv15r7UTHMmj35m+IG9B9FU7NONDXrxOjRzjx9+ozxf3z4Y1ddXZ2Nm5aipqaaZ/UShIJOV1eH5avnMcR+DGYNOxB9PwbXmY7/Km7MuGGYNftF8V5LW5O/tq9kxvSFtGzWFcfxbmzcItqgkDMyuTxXXvlJdDI/4+nTp9y8eTO/i/GvnT179rvmV6dOHdzd3b9rnkL+atXaggsXr/LPP/cB2LB+O716d/tXcaqqqqxbt4jJk2YRF5cAgK1dZxISkpg2VWxpIggfa/1Jm1q/fju9s2l7OYnLaHuLmTRptqLtAXguncX27Xt49OhJrtVDEAobq9bmXL54jXv/RAOweaMXPXp2/ea45uaNadXGgr82eSk+q1y5As+epXEiPAKAu3fukZb2nEaN6+dmlYQfhFwuz5VXfirQz2RGRkayevVqVFRUiI2NpW7duvz++++MGjUKHR0dNDQ02LhxI3PnziUiIgKJRELXrl0ZMWJEtmnd3d1RU1PD39+fLVu2IJPJqFWrFjNmzEBdXR0zMzNq167Nw4cPKV26NMnJyYwePZqqVasil8sZP348kDGC2aJFi0xl9fT0JCIigqdPn6Knp4enpyelSpXC3Nyc9u3bc+HCBZSVlVm6dClly5bl5MmTzJs3D3V1dSpW/LCilL29PQ4ODjRp0oTY2FgGDhzIsWPHCAoKYsOGDSgrK2NsbIyHhwfq6uqsW7eO4OBgpFIp5ubmODk5KTqDPXv2pFevXpw5c4bFixcDsGLFCtTV1Xn9+jXx8fH8888/PHnyhN69ezNs2DCkUikLFy7k7NmzSKVSbG1tGTx4sGKrlG3btvH3338rRjWrVxdTQQorY+MyxMV++KM0Li6R4sW10dLSJC0t/ZviBg3uRUJCEkGBHzau3rhhJwD9B9jldlUEoVAxNjYkNlObSvhM2/t63KDBvUlMSCIo8JAibtDg3qiqqPDXZm8mTXLIgxoJQuFgaJT5ehYfl4h2cS00tYqRnvY8R3GaxYrhvsCF3rZDGfRrH0XM3btRFC1WFMtWzQk7dor6DepgUr0K+gal86ZyglDAFPiRzEuXLjFt2jQOHjzI69evCQ8PJyoqCg8PDzZv3oyXlxcJCQkEBgaye/duDh8+TFhYWLZpd+zYwZ07d9i1axfe3t4EBASgq6vLxo0bAXjy5AnDhw8nICAANzc39PT0WLVqFXZ2dgQFBSGXy3n58iVnzpyhdevWijJGR0dz7949vL29OXToEGXKlCEwMBCAhw8f0rRpU/z9/WnUqBE7duzgzZs3ODs7s3z5cnx9fdHQ0Pjq97B06VI2bdqEr68vRkZG3Lt3j+PHj3P9+nX27NmDv78/SUlJBAYG4uLiAsDu3bvp1KkTERERpKdn/EGyb98+unXLuBN+/fp1Nm/ejK+vLz4+Pty4cYNdu3YB4Ofnx549ewgJCeH8+fOZyjJ58mQcHR3x8/PD2Nj4P/x0hfykpKSU7V0uqVT6zXGjHYaycMHK719IQfgBSZQkOWp7OYlzcPiVBR+1vXr1ajFsWH/Gjs36eIMg/Ow+dz2TSWU5ipNIJKzbtITpU+aSlPQw07H0tOcM7DuK8RNHEnYqkF59u3Py+BnevHn7fSsh/JDkufS//FSgRzIBGjVqRKVKGRuUduvWjV27dqGrq6vo3ERGRmJjY4OysjJFihTB2tqaiIgIWrVqlW1aVVVVoqOj6dWrFwBv376lZs2aivOZmppmKUPZsmUxMjLi3LlzxMfH07JlS9TV1RXHy5cvz+TJk9m9ezdRUVFcvnyZcuXKKY5bWFgAULVqVc6fP8/t27fR09OjcuWM/SJtbGxYtmzZF78HKysr+vbtS5s2bWjfvj01atQgMDCQq1evYmtrC8CrV68wNDTMlK5YsWK0bNmSI0eOULZsWcqWLft/7d15XFXV2sDxHwGSr8MVMUmy1OwjKUrOqDhiAYLgC6gEcVLT1BS49iIGSlmGs2ZO6bV7zZuZOQDKZFwGp26CaCiiiAOCqFcMFYVjMhzO+8d52ZdZ7hsK2vP9R+Gsvfbaw7P3efZaa4OpqSkAY8eOpUUL3UR2GxsbEhMTOX36NOnp6SQmJgLw4MEDMjIyeO211wC4c+cOt27dwtraGgBXV1dCQkLqbLtoOoI+/hAHxzcBaNWqJWfPZiifmZm9yJ07+Tx48FulZXJybtB/QO9ay1m+0QMDA32OHk16/BsgxFMq6OMPcXR8C6h/7F3LucGAAX1qLffGGxYYGBhw9GiiUsbT041WrVqScFD3voAOHdqzdeuXLFiwlOiouMe2fUI0VQELfLEbo+sYaNWqJenn/h17HcxMuXu3euxdv3aDfv0tq5Uzf/01OnV+mUVLAgFob9oOfX19njcy4kPfINTqB4xz/Pc7MZJ+ieFK5tXHuXlCNFlNPsnU19dX/q/VanXBXKHnr6zK31zUarXKU96altVoNIwZM0bp7VOr1ZWeCtfWq+jm5kZkZCQ3btzAx8en0mdpaWn4+fkxefJk7Ozsqj0BK09I9fR0T6XL/61pG8vbClBaWqr8LigoiPPnz3P48GH8/f3x9vZGo9EwadIkpkyZAsD9+/er1VXe9k2bNtGxY0clIa263rKyMmX/+Pv7Y2trC+iSyhYtWnDq1KlK21Bb20XTFvz5GoI/XwPACy+YkHT8R7p27czly1lMneZJVFRstWUS4o+ydOmCWssNHWrF4f+bgyKEqFnV2DteIfamTXunxtiLrxJ7VcvpYu/nSsvMm7eIefMWKT+fS/+J996bQ8ovZx7TlgnRtC1bvI5li9cBupdpHUmM5NWunci8nM3k9zw4EBVfbZmD8T/x2eKAauVOHD/FGz1GKOXmBfrQ1sSYgLm6mPth79eoPD7gVEoa/+3qwMOHRZxNO/9kNlQ81Rp7/uTj0OSHy548eZLc3FzKysrYt29ftbmQgwYNYt++fWg0Gn777TciIiKwsrKqdVkrKytiY2O5ffs2Wq2WTz/9lL///e/V1mtgYFApybO3t+fYsWPk5eVV6+1MTk5m4MCBeHh40LlzZw4dOlRt2FNF5ubm5OXlcf687sITFRWlfGZsbMylS5cAiIvTPXUuLS3F1tYWY2NjZsyYwbhx40hPT2fQoEHs378ftVpNaWkps2fPJiZGNy9HX19faX///v25efMmSUlJvPnmm8q64uLiKC4u5t69exw8eJChQ4cyaNAgdu/eTUlJCWq1Gk9PTyXBLG+fmZmZMiQ5MjKy1u0UTduvv95m5kx/vtvxFSd/icWipznzA3Tzefv07cXPiVGPLAfw2mtduJp9rVG2QYinUXlM7dixiZO/xGHR05zAgGBAF3vHEqMfWQ6g62udyZbYE6Le8vLu4DsrkK3frufn5AP0sOjGJwuWAdC7T08O/rT/keXqMmPq//DFumB+SopCNWki73rW/VcKhCj3LL74R0/b2C2oQ1JSkjI3Mjc3F2trayZNmsTkyZNJSEgAdMNdly9fTmJiIiUlJTg5OeHt7V3jsvPnz0dfX589e/YoL/7p3r07S5YswcjICHNzczIyMpR6VSoVhoaGbN++HQB/f3+6devG+++/D+heADRw4ECsra3x9vbm4cOHAHTv3p2ysjJWrVpVqc7Q0FCOHz/OsmXLSE5OZtGiRRgYGNCjRw+uXr3K9u3bSU1NJSAgACMjI0aPHk1oaCgJCQlERkayadMmjIyMMDExYdmyZZiYmPDVV18RFRWFRqNh2LBhzJ8/Hz09PXx8fMjMzCQ0NBQjIyO+/PJL8vPz+fTTTwHdC4BOnDiBWq2msLCQqVOnMmHChEr7s7S0FFdXV+VFSuUv/rl48SKBgYGUlpbSu3dvjhw5ohyP+mj5X10eXUgI0aAae26GEH9UzQ2aNXYThPjDyrt/obGbUC+GzV56LPWWFF9/LPXWR5NPMssTmye5bFVarRa1Wo27uzvbtm3jhReenjeFabVaSkpKmDJlCvPnz8fCwgLQJZlAtaG/T4IkmUI8eZJkCtE4JMkUovE8LUmmwWNKMksbMcls8nMym4IzZ84wbdo0Zs+e/VQlmKB7u62joyMTJkxQEszGVvjgSmM3QQghhBBCiCahMZPBx6VJ92QKIYQQQgghhHi6NPkX/wghhBBCCCGEeHpIkimEEEIIIYQQosFIkimEEEIIIYQQosFIkimEEEIIIYQQosFIkimEEEIIIYQQosFIkimEEEIIIYQQosFIkimEEEIIIYQQosFIkimEEEIIIYQQosFIkimEEEIIIYQQosFIkinEM6CgoIDZs2fXWSYwMJDr16/XWUalUpGUlNRg7Vq/fj3r169vsPqEqMm6des4ceLE764nJyeH+fPnN0CL6heTDcXGxoZr16797nrqE6/l6woNDSUgIOB3r7M2DXksxJOXlJSESqX63fWUn29P8nwwNzdvkHoCAgIIDQ2t17oe970yNTWVlStXPrb6haiJJJlCPAPu3btHenp6nWWSkpLQarVPqEVCPDnJycloNJrfXc+NGzfIyclpgBbVLyZF7RryWIinn5wPv8+lS5e4fft2YzdD/MFIkinEMyA4OJhbt24xe/ZsQkJCGDt2LE5OTgQEBKBWq9myZQu3bt1i+vTp3L17lwMHDjBx4kScnZ2xt7fnl19+qdd6li5dytatW5WffXx8iI2N5cKFC6hUKtzc3Bg1ahQ7d+6stmzFp8MVe0FSU1Px8PDAxcWF9957T75IiDrdvHkTLy8vXF1dGT9+PBs2bCAtLY2goCAyMjJQqVR4e3tjZ2dHenp6refdzz//jLOzM05OTsyYMYPCwkKCg4NJS0vjs88+q9YTU94rce3aNezt7fHw8GDKlCloNBqWLl2Ki4sLzs7ObNu2DagckwBr1qxh4sSJ2NnZoVKpyMvL4+zZs1hbW3Pnzh3y8/MZNWoUZ86cYejQoeTl5QGQn5/P0KFDKSkp4bvvvmPChAmMHTsWFxcXMjMzK+2bqr2LFUcmbNmyRWnjihUrlAdOf/3rX7G1tcXd3Z3U1FRl2YMHDzJu3DicnJyYNWuW0p6a1HY9qXosIiIicHBwwNHRkYCAAEpKSli/fj1BQUGoVCpsbGzYtGmTsv/Kj4V4eh0/fly5vo8ePZq4uDhAF0/BwcF4eHhgY2NDSEgIoDvf33//fZycnJgzZw5FRUVA5fOhtLSUoKAg3N3dGT16NLNmzeLhw4fEx8djZ2dHUVER2dnZDB8+nNzcXKytrSkpKQHgwoULODs7AzXHZEVVexfLe1Vri3mtVsvSpUuV+q5evaosW9N9uTa1xbmNjQ1z5szBzs6O27dvs23bNuzs7HBwcFB6KWvar/fv32fdunUkJCQo8SXEkyBJphDPgKCgINq3b4+vry+bN29m+/btRERE0Lx5czZs2MD06dNp3749W7Zs4U9/+hM//PADmzdvJjw8nGnTprFly5Z6rWfcuHFERkYCUFhYSEpKCiNGjGDPnj3MmjWLkJAQvv32W1asWFGv+oqLiwkKCmL16tWEhYUxZcoUPv744//3fhDPvr179zJy5EhCQ0Px9fWlefPm9OzZk+DgYCWhNDc3JyYmhu7du9dYR3FxMXPnzmX58uVERETQrVs3wsLCCAoKomfPnixcuLDONly5coWVK1fyzTffsHv3bgDCwsLYu3cv8fHxnDhxQonJjRs3kp2dTWZmJj/88AMxMTF06NCB8PBwLCwscHd3Z8WKFXz++ed4eHjQq1cv7O3t+fHHHwH4xz/+wVtvvUVRURFxcXFs376dyMhIRo4cyY4dO+q1z44cOUJaWhp79+5l37595ObmEh4ezpkzZwgJCSEsLIxvvvmGmzdvAnD79m0++eQTNm7cSEREBH379mXRokU11l1WVlbn9aT8WLRt21Z5SBUVFYVGo+Hw4cMAZGRk8Le//Y09e/awZcsW7t+/X+9jIZq27777juDgYMLCwggODmbt2rXKZzdv3uT7779n06ZNyj1j3bp19OjRg4iICN555x0l8at4PqSkpGBoaMiuXbuIjY2loKCAw4cPM3r0aHr37s3mzZsJDAzko48+wtTUFEtLS3766ScAoqKicHZ2rjUm66O2mI+JieHcuXNERkaydu1aJcnMyMio8b5ck8LCwjrjfPjw4cTExHD9+nW+//579u7dS3h4OGfPniUtLa3G/dq6dWt8fX2xsbHhgw8++E8OnxC/i0FjN0AI0XCSk5MZNWoUxsbGALi7uxMYGFipzHPPPcfGjRtJSEjgypUrHD9+nOeeq9/zph49elBcXEx2djYpKSnY2NjQrFkzAgICOHr0KH/5y1+4cOECDx48qFd9WVlZ5OTkVLrxFRYW1nNrxR/R4MGD8fHxIT09nREjRuDl5cWhQ4cqlbG0tKyzjoyMDExNTZUk1M/PD6De85FNTEzo2LEjAMeOHSM9PZ3ExEQAHjx4QEZGBi+++KJSvlOnTnz00Ufs2bOHK1eucOrUKV555RUAPvjgA9zc3Hj++eeV3ghnZ2eWLl2Kl5cXkZGRfPjhh7Rs2ZLVq1cTFRVFVlYWR48erTWJrurYsWOkpqbi6uoKwMOHDzEzMyMvL48RI0bQokULAOzt7SkrKyM1NRVLS0tlG93d3Wt9EPWo60n5sUhJSaFv377Kfinf1vT0dKysrGjWrBkmJia0adOGgoKCem2XaPpWrlzJwYMH+fHHHzl9+nSlHjxra2v09PTo1q0b+fn5gK7nc/Xq1QAMGDCAl19+uVqdAwYMoE2bNuzYsYPMzEyysrKUe86CBQtwcHCgb9++ODo6Arp4ioqKYtSoURw4cIDt27djampaa0w+Sm0xf/nyZWxtbTE0NKRt27YMHz4cqN99udyj4vyNN96oVGerVq0AlN7U2varEI1BkkwhniFlZWWVftZqtZSWllb6nVqtZvz48Tg7OzNgwADMzc3r3SMCuht2dHQ0KSkpTJ8+HYA5c+bQunVrRo0ahYODg9LbWZVWq0VPT09pU1lZGR07dmT//v0AaDSaOoflCdGvXz+ioqI4dOgQ0dHRhIWFVSvz/PPPV/q56nlnaGiInp6e8nlBQUG14Wt6enqV5jCXD7erWr9Go8Hf3x9bW1sA7ty5Q4sWLfj111+VMmlpafj5+TF58mTs7Ox47rnnlLrL161Wq8nPz6dt27ZYWlpy7949UlNTyc3NpU+fPvzrX/9CpVLh5eXF8OHDadeuXbU5n7W1WaPRMGnSJKZMmQLA/fv30dfXZ9euXZXKGxgYUFxcXK/rSLlHXU/K95WBgUGlfX7nzh3l/0ZGRrVug3i6eXp6YmVlhZWVFYMHD2bu3LnKZ+XHveJ5UfX46+vrV6szPj6edevW8e677+Lq6srdu3eVZfLy8tDX1yczM5OioiKMjIwYPXo0y5YtIzk5mQ4dOmBqalpnTFZsS8VYqBhPNcV8xWHooDvnoX735XKPivPyfVY1nnJzc2nevHmt+1WIxiDDZYV4BhgYGFBaWsrAgQNJSEhQnl7u3r0bKysrQHez1mg0ZGVloaenx8yZM7GysiI2NvY/emmKk5MT0dHRZGdn069fPwD++c9/4uvry5tvvsmRI0cAqtVpbGzMxYsX0Wq1JCQkAPDqq69y79495c2gISEhlb6ECFHVihUrCA8Px8XFhU8++YRz584p53ZNajrvunTpwu3bt7l06RKgm5e4c+dO9PX1lS9/xsbG5OTkUFRURH5+PidPnqyx/kGDBrF7925KSkpQq9V4enpy6tQpJSZB1+swcOBAPDw86Ny5M4cOHVLa+9lnn+Hl5YWnp2el+YdOTk4sXLhQ6Y05c+YMnTp1YvLkyfTq1Yu4uLgaY+zy5ctotVpycnLIyMhQ2rh//37UajWlpaXMnj2bmJgYBg8ezMGDBykoKKCoqIjY2FhA11ty+vRp5Y21u3btUq4jVdX3etKrVy9OnTqlJN9LliwhPj6+xjqBSsdCPJ3y8/PJysriz3/+M8OHDyc+Pv6R95rBgwcrDx1TU1OVIacVz4djx44xZswY3NzcaN26NUlJSWg0GjQaDYGBgSxYsICBAwcqQ3ObNWvGsGHDWLJkiTIfs66YLGdsbKxcI1JTU5Vzt7aYHzx4MAcOHKC4uJh79+5x9OhRgDrvy1XVJ84B+vfvz+HDh5WY9vPzU4bL1kTiSTQG6ckU4hlgYmKCmZkZixcvZsaMGahUKkpKSrCwsFC+uI4cOZLp06fz9ddf0717d8aMGYOenh5Dhw6t9Qt0TTp06ICxsTF9+vRRnpT6+Pjg6emJkZERr7/+Oi+99FK1P6ng5+fHzJkzadeuHf369ePu3bs0a9aMtWvXsnjxYoqKimjZsiXLly9vuB0jnjkqlQo/Pz9CQ0PR19dn+fLlXL58mYULF9Z47tR03hkZGbFy5UrmzZtHSUkJr7zyCitWrKC4uJiCggL8/f1ZuXIlI0aMwNHRkZdeekl5oFLV22+/TXZ2Ni4uLpSWluLq6oqVlRUlJSWYmZmhUqlYtWoV3t7eODk5AdCzZ0+uXbtGdHQ0OTk5fPHFF2i1Wtzc3IiOjsbBwQFnZ2fWrl3LmjVrAN0QuJ07d+Lg4IBWq2XAgAFcvHixUluGDBlCSEgI9vb2dOnSRWmzjY0N58+fZ+LEiWg0GoYNG4aLiwt6enpMmjSJ8ePH07p1a8zMzABo164dixYtwtvbW9mOxYsX17j9r7/+er2uJ6ampixYsICpU6dSVlZG7969cXV15auvvqqx3q5du1Y6FuLp06ZNG4YMGYKjoyMGBgYMGjSIhw8f1jmdwtfXl4CAABwdHXn11VeV4bIVz4dp06Yxd+5coqKiMDQ0pG/fvly7do2tW7diYmKCra0tQ4YMYezYsdja2tK7d2/GjRtHeHg4dnZ2ADg4ONQYkxU5ODgQExODg4MDFhYW9OjRA6g95kGXJI4dO5Z27drRtWtXQBcjtd2Xq6pPnANYWFjg5eXF22+/TVlZGW+99RZDhgypdV6ppaUlGzZsYNWqVfIgVzwxeloZlyKEEEIIIYQQooFIT6YQopKrV6/i4+NT42fBwcH06tXrCbdICCGEEEI8TaQnUwghhBBCCCFEg5EX/wghhBBCCCGEaDCSZAohhBBCCCGEaDCSZAohhBBCCCGEaDCSZAohhBBCCCGEaDCSZAohhBBCCCGEaDD/CxD4rGT6DRLoAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 1080x576 with 2 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Display which variables influence target variables the most for df\n",
    "correlation_matrix = df.corr()\n",
    "sns.set(rc = {'figure.figsize':(15,8)})\n",
    "sns.heatmap(correlation_matrix[['total_value', 'structuretaxvaluedollarcnt', 'landtaxvaluedollarcnt']].sort_values(by='total_value', ascending=False), annot=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 51,
   "id": "7a1175d6-4ead-4004-89b0-cc9e490ccc5d",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<AxesSubplot:>"
      ]
     },
     "execution_count": 51,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAA5kAAAHYCAYAAADHzSzGAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAEAAElEQVR4nOzdd1RUx9vA8e/Sm0iRIppg72DvJYIdFYO9EU00xm4SxWBv2DX2aPzFaGJHRUGxi5rYS1TsUREFCyhI7+y+f/Bm4wZUTIQVfD7n7Dm5d58795kl4+7cmTtXoVKpVAghhBBCCCGEEO+AjrYTEEIIIYQQQghReEgnUwghhBBCCCHEOyOdTCGEEEIIIYQQ74x0MoUQQgghhBBCvDPSyRRCCCGEEEII8c5IJ1MIIYQQQgghxDsjnUwhhBBCCCGE+IAlJCTQoUMHwsPDs7138+ZNOnfuTJs2bZgwYQIZGRlvLE86mUIIIYQQQgjxgbpy5Qq9evUiNDQ0x/e9vLyYPHkyBw4cQKVS4evr+8YypZMphBBCCCGEEIVMXFwc4eHh2V5xcXEacb6+vkyZMgVbW9tsZTx69IiUlBRq1KgBQOfOndm/f/8bz633TmogxFtIfx6i7RSE+OBknA3QdgpCfJhMzbWdgRAfLOPmX2g7hVzJq9/Gv2wOZPny5dn2Dx8+nBEjRqi3Z86c+coyIiMjsbGxUW/b2NgQERHxxnNLJ1MIIYQQQgghCpl+/frh4eGRbb+5ee4vfimVShQKhXpbpVJpbL+KdDKFEEIIIYQQopAxNzd/qw5lTuzt7Xn27Jl6+/nz5zlOq/0nuSdTCCGEEEIIIbRFmZk3r3egRIkSGBoacvHiRQD8/f1p1qzZG4+TTqYQQgghhBBCaItKmTev/+DLL7/k6tWrACxYsIDZs2fTtm1bkpKS+Oyzz954vEKlUqn+UwZCvCVZ+EeI/CcL/wihJbLwjxBaU2AW/om4nSfl6ttVzJNyc0PuyRRC5IpKpWKCz0LKly3F5727ajsdIQqF3248YFngGdIyMinvYM3UHi6YGRloxAQFh7DywHkUCgVFTQyZ3L05HxUrqhHz7dr92JibMq5L0/xMX4gC67er91i283hW2ythw9TP2mFmbKgRE3TpT1buPpHV9kyNmOzZlo9sLBnz404eRsao4x4/j6F2hY9ZMqxLPtdCFBrK/zbq+D6S6bJCiDe6F/qQASPHcejYCW2nIkShEZ2QzJQtQSzo3wb/cb0paWXOkj1nNGJS0jIYv+kIC/u3xXdMd5pVLcXcnZrtcG3QJS6FPMnP1IUo0KLjk5jyy14WfPUp/tO/pGQxC5bsPK4Rk5KWzvif97BwsAe+kz6nmXM55m45DMCCr7L2+U76nMmebSliYsS4Xq20URUh3lvSydSS+Ph4hg0b9tqYcePG8ejRo9fGeHp6cvbs2XeW17Jly1i2bNk7K08UDlt27KFLxza0dpFREiHeldO3w6j6kS2ONhYAdGtclX1/3OHlu1iUKhWoICElDYDk1HQM9XTV75+/+4hTtx7StVHVfM1diILs9I37VHW0x9HOCoBun9Rk39nrmm1PqQKVioTkVACSU9Mw1NecAJiekcnkdYF4dW+BvZVMixb/nkqlzJOXNsl0WS2JjY3l5s2br405e/bsGzuiQuSHCaOHAnDq3B9azkSIwiMiJgF7CzP1tl1RMxJS0khMTVdPmTUx1GdC12b0W+qHhakRmUoV60ZkPfMsMjaR+TtPsuKr9mw/dUMrdRCiIIp4Ea/RKbSzLJLV9lLS1FNmTYwMmNCnDf3mbcDC1JhMpZJ1Y/tqlLPzZDA2Rc1wrVkhX/MXhZBMlxXvio+PD5GRkQwbNowdO3bQoUMHOnbsiLe3N4mJiaxevZrIyEgGDRrEixcv2LdvH927d8fd3Z22bdvyxx+5+7E/e/Zsfv75Z/X2iBEjOHToEH/++Seenp506dIFFxcXNm/enO3YihX/vlnYz88Pb29vAIKDg+nVqxceHh588cUXhIWF/cdPQwghPjxKlYqcnmet+9LOO4+jWH3wAn7f9eTQ1H4MbFmLMesOkJ6Zybj1hxjzaSNszE3zMWshCj6lUkVOj5LX1Xmp7T16xurAk/hNHcChecMY6NaQMT/u1Bjt3HD4PAPdGuVDxkIUPNLJ1JKJEydia2vLyJEjWbVqFevXr2f37t0YGxuzfPlyBg0ahK2tLatXr6Zo0aJs2bKFVatWERAQwMCBA1m9enWuztOpUyf27NkDQEJCApcuXeKTTz5h27ZtDB06lB07dvDrr78yb968XJWXlpbGxIkTWbhwITt37uTzzz9n0qRJ//pzEEKID1VxiyI8i01Sb0fGJmJubIixob5636nbYVQvba9e6KdHk2rcfRrN1QcRhEfHscD/FN0X+LL99HUOXr7LtK1H870eQhQ0xa3MeRaboN6OjInH3MQIY8O/F906dT2E6mVL8pGNJQA9mtfi7qPnxCQmA3DrYQSZSiV1KnyUv8mLwuk9fITJfyXTZbXs/PnzuLi4YGn5//+I9ejBuHHjNGJ0dHRYsWIFQUFB3L9/n3PnzqGjk7vrA1WqVCEtLY0HDx5w6dIlXF1dMTAwwNvbm99//50ff/yRP//8k6SkpDcXBoSGhhIWFsaQIUPU+xISEl5zhBBCiJw0rFiShQGnePAsBkcbC7afuk7zaqU0YiqXLMaWE9eIik/CuogJR6/ep4RVEWqVceDA5L+fU7Zy/3liElNkdVkhcqFhlVIs3B7Eg4hoHO2s2P7bZZpXL6cRU/lje7Yc+4OouESszU05evkOJYoVxdLMBIALfz6kXkVHFDlNRxBCSCdT25T/mIOtUqnIyMjQ2JeYmEjXrl1xd3enbt26VKxYkY0bN+b6HO7u7uzdu5dLly4xaNAgAL7++mvMzc1xcXHBzc1NPdr5TyqVCoVCoc5JqVRSsmRJ/P39AcjMzOT58+e5zkUIIUQWqyImTOvpgte6g6RnZlKyWFF8erlyPSySaVuP4TumO/XKl6SfSw0GrvBHX1cXcxNDFg1op+3UhSjQrMxNmdbPDa/Vu0jPyKSkjSU+n7fneugTpq3fj++kz6lXyZF+reozcOEm9PV0MTcxZtHQzuoyHka+wMG66GvOIsRbUGZqO4N3TjqZWqKnp0dGRgb16tXj119/ZejQoVhYWODr60v9+vUB0NXVJTMzk9DQUBQKBYMHD0alUjF27FgyM3P/P2PHjh0ZNGgQaWlp1K5dG4CTJ0+yb98+7Ozs1B3Wf5ZpaWnJnTt3KF++PEFBQVhYWFCmTBliY2O5cOECderUYceOHezevZv169e/o09GvM9mThyt7RSEKFSaVnGkaRVHjX1FTY3wHdNdvd2zSTV6Nqn22nKGtK2bJ/kJUVg1dSpLU6eyGvuKmhrjO+lz9XZPl1r0dKmV4/Hje7fO0/yEKOikk6kl1tbWODg4MHPmTL766is8PT1JT0+natWqTJs2DYDmzZszaNAg/ve//1G5cmXatWuHQqGgSZMmXLx4MdfnKl68OJaWltSsWVM9rWPEiBH07t0bQ0NDKlWqRIkSJQgPD9c4bvTo0QwePJhixYpRu3ZtXrx4gYGBAUuWLGHmzJmkpqZiZmbG3Llz390HI4QQQgghxIdEy/dP5gWF6uVlsoTIB+nPQ7SdghAfnIyzAdpOQYgPk6k8P1EIbTFu/oW2U8iVtJBzeVKuQZl6eVJubshIZiHw8OFDRowYkeN7Pj4+ODk55XNGQgghhBBCiA+VdDILgY8//li9EI8QQgghhBCi4FAVwumy8pxMIYQQQgghhBDvjIxkCiGEEEIIIYS2KAvfSKZ0MoUQQgghhBBCW2S6rBBCCCGEEEII8WoykimEEEIIIYQQ2qLM1HYG75yMZAohhBBCCCGEeGdkJFMIIYQQQgghtKUQ3pMpnUwhRK6oVCom+CykfNlSfN67q7bTEaJQ+O3GA5YFniEtI5PyDtZM7eGCmZGBRkxQcAgrD5xHoVBQ1MSQyd2b81Gxohox367dj425KeO6NM3P9IUosH67eo9lO49ntb0SNkz9rB1mxoYaMUGX/mTl7hNZbc/UiMmebfnIxpIxP+7kYWSMOu7x8xhqV/iYJcO65HMtRKFRCFeX/eCny8bHxzNs2LA8P4+rqyvh4eH/uZxly5axbNmyXJ3Lz88Pb2/v/3zOVwkLC2P8+PF5Vr54f9wLfciAkeM4dOyEtlMRotCITkhmypYgFvRvg/+43pS0MmfJnjMaMSlpGYzfdISF/dviO6Y7zaqWYu5OzXa4NugSl0Ke5GfqQhRo0fFJTPllLwu++hT/6V9SspgFS3Ye14hJSUtn/M97WDjYA99Jn9PMuRxztxwGYMFXWft8J33OZM+2FDExYlyvVtqoihDvrQ++kxkbG8vNmze1nUaB9PjxY8LCwrSdhsgHW3bsoUvHNrR2kVESId6V07fDqPqRLY42FgB0a1yVfX/cQaVSqWOUKhWoICElDYDk1HQM9XTV75+/+4hTtx7StVHVfM1diILs9I37VHW0x9HOCoBun9Rk39nrmm1PqQKVioTkVACSU9Mw1NecAJiekcnkdYF4dW+BvZV5/lVAFD4qZd68tOiDny7r4+NDZGQkw4YNo1y5cpw+fZrY2FhsbW1ZtGgRERERDBo0iN27d6Ojo4OHhwdLly5lyJAh7Nq1i2LFihETE0OHDh04evQoW7duxd/fn+TkZPT19Vm4cCFlypRRn8/Pz49z584xZ84cADw9PRk+fDj169dn9erV7Nu3j8zMTJo0aYKXlxcKhYKffvoJX19fLC0tMTc3x9nZGYCjR4+yePFilEolH330EdOnT6dYsWI51nPfvn2sXbuWlJQU0tLSmDVrFrVq1cLT05OiRYty584dFi9ezN27d1m5ciUKhQInJydmzJjBqlWriIiI4MGDBzx69Ihu3boxZMgQfHx8CA8PZ9q0aUyZMiXv/1hCayaMHgrAqXN/aDkTIQqPiJgE7C3M1Nt2Rc1ISEkjMTVdPWXWxFCfCV2b0W+pHxamRmQqVawb4QFAZGwi83eeZMVX7dl+6oZW6iBEQRTxIl6jU2hnWSSr7aWkqafMmhgZMKFPG/rN24CFqTGZSiXrxvbVKGfnyWBsiprhWrNCvuYvREHwwY9kTpw4EVtbW8aOHUtISAhbtmzhwIEDFC9enICAAKpWrUqPHj2YN28eM2bMoFevXjg5OdG2bVv2798PwMGDB2nVqhWpqakcPnyY9evXs2fPHpo3b87GjRtzlcdvv/3GtWvX2L59O7t27SIiIoKAgACuXr3Kjh072LlzJ2vXruXp06cAREVFMXnyZFasWMHu3bupVasW06dPz7FspVLJli1bWLVqFQEBAQwcOJDVq1er369YsSIHDhzAysqK2bNn8/PPPxMYGEhmZibHj2dNH7l9+zZr1qxh27ZtrF69mri4OCZOnEi1atWkgymEEP+CUqVCoci+X/elnXceR7H64AX8vuvJoan9GNiyFmPWHSA9M5Nx6w8x5tNG2Jib5mPWQhR8SqWKHJoeujovtb1Hz1gdeBK/qQM4NG8YA90aMubHnRqjnRsOn2egW6N8yFgUekpl3ry06IMfyfyLo6Mj3333Hdu2beP+/ftcvnyZjz/+GIAhQ4bQpUsXjIyMmD9/PgDu7u7Mnj2bvn37smfPHr755hvMzMxYuHAhgYGBhIaG8vvvv1O5cuVcnf/06dMEBwfTuXNnAFJSUnBwcOD58+d88sknmJpm/Yho27YtSqWS4OBgnJ2dKVmyJAA9evTQ6Di+TEdHhxUrVhAUFMT9+/c5d+4cOjp/X1/4a2T00qVL1KpVC3t7ewB1XW/evEn9+vUxMDDA2toaCwsL4uPj3+rzFUIIoam4RRGuPYhUb0fGJmJubIixob5636nbYVQvba9e6KdHk2os8D/F1QcRhEfHscD/FABR8UkolSrSMjKY0sMlfysiRAFT3Mqca6GP1duRMfGYmxhhbPj3olunrodQvWxJPrKxBKBH81os8A0iJjEZSzMTbj2MIFOppE6Fj/I9f1H4qFTynMxC69q1awwYMAClUkmbNm1o2bKl+mpVfHw8iYmJREVFERMTA2R1zGJjYwkODiYiIoKaNWvy5MkTevToQXx8PM2aNcPDw0PjiheAQqHQ2Jeeng5AZmYm/fr1w9/fH39/f7Zt28bgwYOzxevpZV0XUP7j6oRKpSIjIyPHuiUmJtK1a1fCw8OpW7cunp6eGu8bGRmpy1a8dAU9Ojqa6OhoAAwN/15x7Z85CSGEeHsNK5Yk+EEED57FALD91HWaVyulEVO5ZDEu3ntCVHwSAEev3qeEVRFqlXHgwOTP8B3THd8x3enasCqta5STDqYQudCwSimCQx7zICLrN8723y7TvHo5jZjKH9tz8c5DouISATh6+Q4lihXF0swEgAt/PqReRUeN301CiL998J1MPT09MjIyOH/+PPXq1aNXr16UKlWKY8eOkZmZdVVh2rRp9O3bl969ezNt2jT1sR07dmTKlCm0b98egKtXr+Lo6Ej//v1xcnLi8OHD6jL+Ymlpyb1791CpVISFhXH79m0AGjRogL+/P4mJiWRkZDBs2DAOHDhAw4YNOXr0KPHx8aSmpnLo0CEAqlevzpUrV9Qr1m7dupX69evnWMfQ0FAUCgWDBw+mfv36HDp0KFteAE5OTly+fJlnz54BMGvWLI4cOfLKz05XV/eVHVshhBCvZ1XEhGk9XfBadxCPOZu58zSa0e6NuB4WSfcFvgDUK1+Sfi41GLjCn+7zfdly4hqLBrTTcuZCFGxW5qZM6+eG1+pdeEz5H3cePWN0N1euhz6h+4y1ANSr5Ei/VvUZuHAT3Wf8zJajf7BoaGd1GQ8jX+BgXfRVpxDi7cjCP4WPtbU1Dg4OBAUFkZKSQseOHQGoVq0a4eHh7N27l7CwML7//ntUKhVdunRh7969uLm54e7uzpIlS1i0aBEAjRs3ZvPmzbi5uaFSqahbty537tzROF+jRo3YsWMHbdu2pXTp0tSuXRvIeuzIrVu36N69O5mZmTRt2hQPDw8UCgX9+vWja9eumJub4+DgAECxYsWYPn06w4cPJz09HQcHB2bOnJljHStVqkTlypVp164dCoWCJk2acPHixWxxdnZ2TJgwQT2iW6NGDTp37swPP/yQY7lly5YlPj4eLy8v9dRaUbjNnDha2ykIUag0reJI0yqOGvuKmhrhO6a7ertnk2r0bFLtteUMaVs3T/ITorBq6lSWpk5lNfYVNTXGd9Ln6u2eLrXo6VIrx+PH926dp/kJUdApVDLvUeSz9Och2k5BiA9OxtkAbacgxIfJVB5tIYS2GDf/Qtsp5ErKH3nzHW1Uyz1Pys2ND366rBBCCCGEEEKId+eDny4rhBBCCCGEEFqj5fsn84J0MoUQQgghhBBCW5SF7xEm0skU+U759J62UxDiwxMVoe0MhPgwRUWAvsGb44QQohCRTqYQQgghRF6RDqYQ4k0K4XRZWfhHCCGEEEIIIcQ7IyOZQgghhBBCCKEtysI3kimdTCGEEEIIIYTQFpkuK4QQQgghhBBCvJqMZAohhBBCCCGEtsh0WSFEYfTbxWss2RBAWkYGFRxLMG1ob8xMjDViNu09xuZ9v2FkoE/pEvZM+LI7RYuYEp+YzJQfNnL/UQQqlQr35vX5wqOVlmoiRMHy250nLDt2nbQMJeVtizK1Qy3MDPXV7+8OfsD6s3fV2wmp6UTGJ3NgRDv0dHWYue8StyNiMdbXpVP1UvSqW1Yb1RCiwPvtz8csO3yFtEwl5e0smOpeDzMjfY2YoJvhrDx6FYVCQVFjAya71+UjqyJayliI95tMl9Wy+Ph4hg0bpu00/jVfX1/27Nmj7TTEfxAdG8+k5Rv43msgu5dNpqSdNYs3BGjEnLv6Jz/vPMz/po5g28JxNK1VlWmrNgOwYsse7Kwt2Ll4ApvmeuF74Heu3A7RRlWEKFCiE1OZsucPFnRpgP+Q1pS0NGVJ0DWNmI7Ojvh+2QLfL1uw8QsXipkZ4d2mOtZmRsw/FIyJgR5+X7Vi/ecunLj3lN/uPNFSbYQouKITU5iy6ywLejTBf0T7rLZ4+IpGTEp6BuP9TrOwRxN8h7SlWcUSzN33h5YyFoWOUpk3Ly2STqaWxcbGcvPmTW2n8a/98ccfpKWlaTsN8R+cvnKLauUccXSwBaB7m6bs/f08KpVKHXMj5CENnCtib20JQIsG1Tl+4Rrp6Rl890VXRvfzAOD5izjS0jOyjYIKIbI7fT+CqsUtcLQyA6BbrdLsux6m0fZetu70n1iZGNK1VhkAbj6Nob3Tx+jqKNDX1aFpOXsO3XqUb/kLUVicvveUqiWscLTOGpXsVqcc+64+0GiLSqUKVFmzCQCS09Ix1NPVSr6i8FGpMvPkpU0yXVbLfHx8iIyMZNiwYZQrV47Tp08TGxuLra0tixYtIiIigkGDBrF79250dHTw8PDghx9+wMrKivHjxxMfH09kZCQeHh6MGjUKPz8/jh07RkxMDJGRkfTs2ZNHjx5x5swZLCws+OmnnzA0NGTHjh2sXbsWhUJB1apVmTRpEqamplSsWJHbt28D4Ofnx7lz55gzZw6urq64u7tz4sQJkpOTmTt3LnFxcQQFBXHmzBlsbGxo2rSplj9N8W88ff4C+2IW6m07awsSklJITE5Rdxadypdi097jPI6MxsHWCv+gM6RnZBCTkIiNZVH0dHUZt+QXDp2+hGu96pRysNNSbYQoOCLikrE3N1Fv25kbk5CaQWJahsaUWYAXSan8evYOm79wVe9zcrAk8OpDapS0Jj1TyZFbj9HTVeRb/kIUFhGxSf9oiyYkpKaTmJqhnjJrYqjPhA516LfmMBbGBmSqVKz7oqW2UhbivScjmVo2ceJEbG1tGTt2LCEhIWzZsoUDBw5QvHhxAgICqFq1Kj169GDevHnMmDGDXr16UblyZfbs2UOHDh3w9fVl9+7d/PLLL0RHRwNw9epVfvjhB9asWcPs2bNp1qwZu3fvBuD333/n9u3brFq1ivXr17N7926MjY1Zvnz5G3O1sLBg+/bt9OzZkx9//JFGjRrh6urKyJEjpYNZgClVKiD7D1Mdnb//eahdpRyDu7Xj63mr6Tl2LgodBUXNTNB/6Sru7FH9+G3tXOISElm1bV9+pC5EgaZUqVDk0CfUzWHnjkv3aV6hOCUtTdX7vm3phALouSaIb7adpkFpW/R15GtdiLeV1Raztztdnb/33YmIYfXx6/gNa8ehMZ8ysGlVxviefOXMAyHeikyXFXnF0dGR7777jm3btjFnzhwuX75MUlISAEOGDOHGjRuEhYUxcOBAAAYMGEDx4sVZs2YNM2fOJD09neTkZABq1aqFmZkZJUqUAKBhw4YAlChRgri4OM6fP4+LiwuWlllTH3v06MGZM2femONfHcny5csTExPzTusvtKd4MUuevYhVb0dGxWJuZoKJkaF6X2JyCnWqlsd3gTdb5n2HS11nAIqamXLy0g0io2MAMDE2pF2TOtwMCcvXOghREBU3N+FZfIp6OzI+BXMjfYwNsk8yOnjjEZ2cHTX2JaZm8HULJ3YMasmPfZqiQsVH/z/1VgiRe8WLmvIsPlm9HRmfjLmRgUZbPHX3KdU/LqZe6KdHvXLcjYwlJkluGRIiJ9LJfE9cu3aNAQMGoFQqadOmDS1btlRfHYuPjycxMZGoqCh1527OnDmsX78eBwcHhgwZgqWlpTpeX19zmpWenuYPFuU/rmyoVCoyMjI0tgGNfQCGhlmdjpyu9omCq2GNygT/GcqDx5EAbDv4Oy51nTRiIqNj+WLyEhKSsr6E/7fjAO2a1EGhUHDw1CVW+e5DpVKRlp7OgVN/UN+pQr7XQ4iCpmEZW4IfR/MgOgGA7X+E0LxC8WxxcclpPHyRQPWS1hr7t/0Rwg/HbwAQlZDCzsuhtKtaMu8TF6KQaVjWnuDw5zyIigdg+4W7NK9UQiOmcnFLLoZGEpWQdWHo6K1HlLAwxdLUMFt5Qrw1lTJvXloknUwt09PTIyMjg/Pnz1OvXj169epFqVKlOHbsGJmZWTfsTps2jb59+9K7d2+mTZsGwMmTJxkwYADt2rXj/v37REREZOs8vkq9evUICgpSd1h9fX2pX78+AJaWlty5cweVSkVQUNAby9LV1VXnKQom66JFmDGsL6MXrKHTyBncefiYMf06c/3uA7qNng1A6RJ2DPBoRR/vBXQcMZ309Ay+/exTAEb39yAhKZnO38yih9c8qpT9mD7tm2uvQkIUEFamRkzrUBuvHWfxWHWIO5FxjG7pzPXHL+j+vyPquIcvErAxM0JfV/Mre0CjikTEJ9Nl9WG+3Pg7Q5tVoZqDVX5XQ4gCz8rMiGmd6uPlexKP5Xu5ExHL6NY1uP4omu4r9wNQr4wd/RpXZuC6ILqv3M+Wc3dY1EtuFRLiVWThHy2ztrbGwcGBoKAgUlJS6NixIwDVqlUjPDycvXv3EhYWxvfff49KpaJLly7s3buXr776irFjx2JkZIS9vb06PjcqVarEV199haenJ+np6VStWlXdeR09ejSDBw+mWLFi1K5dmxcvXry2rEaNGvH9999TpEgR2rZt+98+DKE1TWtXpWntqhr7ihYxZdvCcertXm6f0Mvtk2zHmpuaMO/bL/I8RyEKo6bl7Glazl5jX1FjA3y/bKHeruZgxe6hbbIda2qoz+JuDfM8RyE+BE0rONC0goPGvqImhvgO+fu3Tc965elZr3x+pyY+BFq+fzIvKFRyx7LIZ6nXDmk7BSE+OMo/3jwzQQiRB/QNtJ2BEB8s417TtJ1CriQfXpUn5Rq3HJwn5eaGTJcVQgghhBBCCPHOyHRZIYQQQgghhNCWQjhdVkYyhRBCCCGEEEK8MzKSKfKd8rk8Q1GI/HZv6lVtpyDEB6niuaXaTkEI8b7T8uNG8oJ0MoUQQgghhBBCW2S6rBBCCCGEEEII8WoykimEEEIIIYQQ2iIjmUIIIYQQQgghxKvJSKYQQgghhBBCaIss/COEKIx+u3qPZTuPk5aRSfkSNkz9rB1mxoYaMUGX/mTl7hMoFAqKmhox2bMtH9lYMubHnTyMjFHHPX4eQ+0KH7NkWJd8roUQBY+ZSx3svfqhMNAn5VYoj7yXoExIzjG2SKsGlFz4LTedu2ft0NGh+LTBmNarBkDCsQs8nf1zfqUuxAdBpVIxwWch5cuW4vPeXbWdjiisZLqs9p09exZPT8//XI6rqyvh4eGEhYUxfvz4d5DZm1WsWPGdlOPt7Y2fn1+uzrVs2TKWLVv2Ts6bk+DgYObPn59n5Yu8Fx2fxJRf9rLgq0/xn/4lJYtZsGTncY2YlLR0xv+8h4WDPfCd9DnNnMsxd8thABZ8lbXPd9LnTPZsSxETI8b1aqWNqghRoOhamVNy7tc8HDqbOy0Hkxb2FLux/XOMNSjlgP24L0ChUO+z8HDBsHQJ7rYbzt32IzCpXw3zdo3zKXshCr97oQ8ZMHIch46d0HYqQhQ4Ba6T+a49fvyYsDB5buO/dffuXaKiorSdhvgPTt+4T1VHexztrADo9klN9p29jkqlUscolSpQqUhITgUgOTUNQ33NiRDpGZlMXheIV/cW2FuZ518FhCigzJrWIvnqHdJCHwMQvWEvFp2aZ4tTGBlS8vvRPJ35k+Z+XR10TIxQGOijY6CPQl8fVWp6fqQuxAdhy449dOnYhtYuTbWdiijsVMq8eWlRgZ0ue+7cORYtWkRKSgpxcXGMGzeOli1b4u3tjZmZGdevXyciIoJhw4bRpUsXYmJi8PLy4unTp5QtW5bU1Kwfyz4+PoSHhzNt2jQmTJjA1KlTuXPnDs+fP6dixYp8//33nDx5knnz5hEQEMDTp0/x9PRk27ZtdO7cmWPHjqGvr8+ff/7JmDFjCAgIYNGiRZw+fZrY2FhsbW1ZtGgRxYoVU+f+18jiiBEjgKxR1V9//ZXixYszb948zp07R2ZmJp07d6Z///6oVCrmzJnDsWPHsLW1JTMzk3r16gGwY8cO1q5di0KhoGrVqkyaNAlTU9McP7MNGzbg7+9PcnIy+vr6LFy4kDJlyuDq6oqzszM3b95k06ZN7N69m82bN6Orq4uLiwteXl45fq6tWrVi6dKlJCUlsXLlSoYMGZKXf3KRRyJexGt0Cu0si5CQkkZiSpp6yqyJkQET+rSh37wNWJgak6lUsm5sX41ydp4MxqaoGa41K+Rr/kIUVPrFi5H+5Ll6O/3pc3SLmKJjZqwxZbbEzGFEb95Pyq1QjeNfbD+CebsmVDr9C+jqkHDiEvFB5/IrfSEKvQmjhwJw6twfWs5EiIKnwI5kbtiwAR8fH3bu3ImPjw9LlixRv/f06VM2bdrEypUrmTdvHgBLly6lSpUq7N69mz59+vD8edYX+8SJE6lWrRpTpkzh0qVL6Ovrs3XrVg4dOkR8fDzHjx+nRYsW1KhRg1WrVjFu3Di+++477OzscHZ25sSJrCkUgYGBuLu78+DBA0JCQtiyZQsHDhygePHiBAQE5KpOvr6+AOzcuZPt27dz5MgRLly4wIEDB7hx4wZ79uxhyZIlPHz4EIDbt2+zatUq1q9fz+7duzE2Nmb58uU5lp2QkMDhw4dZv349e/bsoXnz5mzcuFH9frNmzThw4ACPHj1i06ZNbN++nYCAAK5fv861a9dy/FzNzc0ZOXIkrq6u0sEswJRKFYoc9uvq/L33zqNnrA48id/UARyaN4yBbg0Z8+NOjdHODYfPM9CtUT5kLEThoNBRaLShv6gy/776bNXXDVVmJjHbDmWLsx3Zi8zoWG7V68vtxv3RLVoE6wEeeZqzEEKIPKBU5s1LiwrsSOb8+fM5evQo+/fv58qVKyQmJqrfa9y4MQqFggoVKhATEwNkjXwuXLgQgLp16/LRRx9lK7Nu3bpYWFiwceNGQkJCCA0NJSkpCYAJEybg5uZGrVq1aN++PQDu7u4EBgbi4uLCvn37WL9+PXZ2dnz33Xds27aN+/fvc/nyZT7++ONc1en06dPcvHmTM2fOAJCUlMTt27e5d+8erVu3Rl9fHysrK5o1awbA+fPncXFxwdLSEoAePXowbty4HMs2MzNj4cKFBAYGEhoayu+//07lypXV71evXl2jzCJFigCwbt26136uouArbmXOtf+frgcQGROPuYkRxoYG6n2nrodQvWxJPrL5///XmtdigW8QMYnJWJqZcOthBJlKJXUqZG9XQoicpT16hnH1v+/V17ezJiMmHtX/T0sHsOjSEh0jQ8ruWYpCXw8dIwPK7lnKgy+mYt6mEU+mrUKVnoEqPYMYvyOYt2tM1JqdWqiNEEII8bcCO5LZu3dvgoODqVatGoMHD9Z4z9Awa4qf4qUFEhQKzSvGurq62co8cuQIY8aMwcjIiM6dO1O3bl31Mc+fP0dXV5eQkBD1VNsWLVpw/vx5zp8/T/HixbGzs+PatWsMGDAApVJJmzZtaNmyZbYr1f/MJT096x6azMxMvLy88Pf3x9/fn61bt9K1a9ds8Xp6WdcGlP+4QqFSqcjIyMjx83ry5Ak9evQgPj6eZs2a4eHhoVHmX5+Znp6exucWERFBXFzcKz9XUfA1rFKK4JDHPIiIBmD7b5dpXr2cRkzlj+25eOchUXFZF3OOXr5DiWJFsTQzAeDCnw+pV9FR/t8Q4i0knLiESc2KGJRyAMCqjxvxh89oxIR4fMvddsO412EkD76YijIljXsdRpIRGU3y9XuYt///e8X0dCnSsj7Jl27ncy2EEEL8Z4XwnswC2cmMiYkhNDSUUaNG0axZM44cOUJmZuZrj2nYsCH+/v5A1oqof0051dXVVXfMTp8+Tbt27ejSpQvm5uacPXuWzMxMMjMzGTduHBMmTKBevXrqqbkGBgY0bdqUWbNm4e7uDmSNBNarV49evXpRqlQpjh07li03S0tL7t69q87l2bNnADRo0ABfX1/S09NJTEykd+/eXL58mYYNG7Jv3z7S0tKIjY3l999/B6BevXoEBQWpRxV9fX2pX79+jvW/evUqjo6O9O/fHycnJw4fPpzjZ1anTh2OHz9OYmIiGRkZjB49Wj1dNicvf36iYLIyN2VaPze8Vu/CY8r/uPPoGaO7uXI99AndZ6wFoF4lR/q1qs/AhZvoPuNnthz9g0VDO6vLeBj5AgfrotqqghAFUmZULOFjl/DRinGUO7gSw4qOPJ25BiOncpTds/SNxz/1+R+65qaUP7SScnuWkvHkOc9X78iHzIUQQrxTMl32/WBhYUGjRo1o3749enp6NGjQgJSUFPXU1pyMHDkSb29v2rdvT5kyZdTTZcuWLUt8fDxeXl4MHDiQMWPGEBgYiL6+PrVq1SI8PJyff/4Za2trWrduTaNGjejQoQOtW7emRo0adOrUiYCAANq0aQOAm5sbw4cPp2PHjgBUq1aN8PBwjVzc3Nw4cOAAbm5uVK1alSpVqgDQs2dPHjx4gIeHBxkZGXTu3Fndabx69SodOnSgWLFilC1bFoBKlSrx1Vdf4enpSXp6OlWrVmXatGk51r9x48Zs3rwZNzc3VCoVdevW5c6dO9niqlatSt++fenZsydKpZJWrVrRqFGjV95X6uzszPLly1mwYAFjxox55ecv3m9NncrS1Kmsxr6ipsb4Tvpcvd3TpRY9XWrlePz43q3zND8hCquEYxdIOHZBY1/m1bvc6zAyW2z6o0huOnX7Oy4mnvBR8ggpIfLazImjtZ2CEAWOQpXTqgNC5KHkY/KwcCHy270vXv9sXSFE3qh47s2j0kKIvKFfrIy2U8iVZN/peVKucffJeVJubhTI6bJCCCGEEEIIId5P0skUQgghhBBCCG1RqfLm9RZ2796Nm5sbrVu31njM4V+uX79Oly5dcHd356uvvlIvDPoq0skUQgghhBBCCG3R8sI/ERERLFq0iE2bNrFr1y62bt2qXqT0LzNnzmTkyJEEBARQunRp1qxZ89oyC+TCP0IIIYQQQgghXi0uLi7HEUdzc3PMzc3V26dOnaJBgwZYWFgA0KZNG/bv38/w4cPVMUqlksTErEfZJScnU7To658qIJ1Mkf8SXz+8LoR492TxESG043a97CsFCyHyR7WQPdpOIXfy6HEjv/zyC8uXL8+2f/jw4YwYMUK9HRkZiY2NjXrb1taW4OBgjWO8vb354osvmDVrFsbGxvj6+r723NLJFEIIIYQQQohCpl+/fnh4eGTb//IoJmSNUioUCvW2SqXS2E5JSWHChAmsW7cOZ2dn1q5dy3fffcfq1atfeW7pZAohhBBCCCGEtqjyZiTzn9NiX8Xe3p4LF/5+ZvOzZ8+wtbVVb//5558YGhri7OwMQI8ePViyZMlry5SFf4QQQgghhBBCW7S88E+jRo04ffo00dHRJCcnc/DgQZo1a6Z+39HRkadPnxISEgLAkSNHcHJyem2ZMpIphBBCCCGEEB8oOzs7vvnmGz777DPS09Pp2rUrzs7OfPnll4wcORInJydmz57N119/jUqlwtramlmzZr22TIVK9ZYPURHiP0oOXKztFIT44OjVd9d2CkJ8kGThHyG0p6As/JP8i3eelGvcb06elJsbMpIphOC3Gw9YFniGtIxMyjtYM7WHC2ZGBhoxQcEhrDxwHoVCQVETQyZ3b85HxTSXr/527X5szE0Z16VpfqYvRKGmUqmY4LOQ8mVL8XnvrtpOR4hCwcylDvZe/VAY6JNyK5RH3ktQJiTnGFukVQNKLvyWm87ds3bo6FB82mBM61UDIOHYBZ7O/jm/UheiQMi3ezKXLl2qcUPpvxUWFsb48ePfQUYQHx/PsGHD3klZb+Lq6kp4ePh/LmfZsmUsW7YsV+fy8/PD2ztvrozAu/1bCO2JTkhmypYgFvRvg/+43pS0MmfJnjMaMSlpGYzfdISF/dviO6Y7zaqWYu7OExoxa4MucSnkSX6mLkShdy/0IQNGjuPQsRNvDhZC5IqulTkl537Nw6GzudNyMGlhT7Eb2z/HWINSDtiP+wJeWmnTwsMFw9IluNtuOHfbj8CkfjXM2zXOp+xFoaTlezLzQr51Ms+fP09mZuZ/Lufx48eEhYW9g4wgNjaWmzdvvpOyPkTv8m8htOf07TCqfmSLo40FAN0aV2XfH3d4eSa9UqUCFSSkpAGQnJqOoZ6u+v3zdx9x6tZDujaqmq+5C1HYbdmxhy4d29DaRWYHCPGumDWtRfLVO6SFPgYgesNeLDo1zxanMDKk5PejeTrzJ839ujromBihMNBHx0Afhb4+qtT0/EhdiAIjTzqZT58+pW/fvnTu3JmuXbuyfPlyrl27xsSJE7l9+zaenp4MHz6cNm3acPPmTSpWrKg+9uXRt1OnTuHu7k7Hjh356quvSEhIwMfHh2vXrjFt2jTOnj2Lp6en+lhvb2/8/PwIDw+nbdu29OrVi88//5zMzExmz56Nh4cH7u7urFu3DgAfHx8iIyPVo5mLFi2ie/futGnTBk9PT54/f87169dp3Lgx0dHRxMTE4OLiwtWrV2nSpAnPnz8HICYmhiZNmpCens6GDRvo1q0bHTp0wMPDQ70KU071A/D09OTs2bMArF69Wp3jvHnz1D/yf/rpJ1q3bk2PHj00Hox69OhROnXqRMeOHRk6dKg6n5zs27eP7t274+7uTtu2bfnjjz/U53/5b7F7927c3Nxo37493t7epKens2zZMiZOnIinpyeurq6sXLlS/fn99bcQBVdETAL2FmbqbbuiZiSkpJH40hemiaE+E7o2o99SP1pN/YUtJ64xqkNDACJjE5m/8ySz+rZE56UrvUKI/27C6KG0b+2i7TSEKFT0ixcj/cnfv5nSnz5Ht4gpOmbGGnElZg4jevN+Um6Faux/sf0ImbEJVDr9CxXP/Erag8fEB53Lj9RFYSUjmbmzfft2mjdvjp+fHyNHjsTY2Jhq1arh4+Oj7lBWrFiRAwcOULly5RzLSEtLY8yYMcydO5fdu3dToUIFdu7cycSJE6lWrRpTpkx5bQ73799n/vz5rF27Fl9fXwB27tzJ9u3bOXLkCBcuXGDixInY2tqyYsUKHjx4QEhICFu2bOHAgQMUL16cgIAAqlatSo8ePZg3bx4zZsygV69eODk50bZtW/bv3w/AwYMHadWqFampqRw+fJj169ezZ88emjdvzsaNG3P1mf32229cu3aN7du3s2vXLiIiIggICODq1avs2LGDnTt3snbtWp4+fQpAVFQUkydPZsWKFezevZtatWoxffr0HMtWKpVs2bKFVatWERAQwMCBAzUenvrX38LKyorZs2fz888/ExgYSGZmJsePHwfg9u3brFmzhm3btrF69Wri4uJy/bcQ7zelSkVOfUPdl3beeRzF6oMX8PuuJ4em9mNgy1qMWXeA9MxMxq0/xJhPG2FjbpqPWQshhBD/jkJHQU7rXqoy//5RbtXXDVVmJjHbDmWLsx3Zi8zoWG7V68vtxv3RLVoE6wHZH3gvRK6plHnz0qI8WfinYcOGjBgxgps3b/LJJ5/Qt29fjh07phHz18M8X+X27dvY2dmpO6GjR48GUI/6vYm1tTUlS5YE4PTp09y8eZMzZ7LuM0tKSuL27dvY29ur4x0dHfnuu+/Ytm0b9+/f5/Lly3z88ccADBkyhC5dumBkZMT8+fMBcHd3Z/bs2fTt25c9e/bwzTffYGZmxsKFCwkMDCQ0NJTff//9lZ3ofzp9+jTBwcF07twZgJSUFBwcHHj+/DmffPIJpqZZP+Dbtm2LUqkkODgYZ2dndR179Oih0XF8mY6ODitWrCAoKIj79+9z7tw5dHT+vr7w19/i0qVL1KpVS/25/FXXmzdvUr9+fQwMDLC2tsbCwoL4+Phc1Uu8/4pbFOHag0j1dmRsIubGhhgb6qv3nbodRvXS9uqFfno0qcYC/1NcfRBBeHQcC/xPARAVn4RSqSItI4MpPWT0RQghxPsn7dEzjKv/PYtO386ajJh4VMmp6n0WXVqiY2RI2T1LUejroWNkQNk9S3nwxVTM2zTiybRVqNIzUKVnEON3BPN2jYlas1MLtRHi/ZQnnczatWsTGBjIsWPH2Lt3Lzt3Zm90RkZGGtsqlQqFQkFGRgYA+vr6KF4aSYmPjycxMVHjGIVC80pUevrf0/teLj8zMxMvLy9at24NQHR0NKampjx79kwdc+3aNUaPHk3//v1p06YNOjo66rL/OndiYiIxMTFYWVnh7OxMbGwswcHBREREULNmTZ48eYKnpyd9+/alWbNmFCtWLNs9n6/KOTMzk379+vH5558DEBcXh66uLlu3btWI19PTIy0tDeU/hsBVKpX6s/unxMREunbtiru7O3Xr1qVixYoaI6x/fVZ6enoan3l0dLT6vw0NDV9ZB1GwNaxYkoUBp3jwLAZHGwu2n7pO82qlNGIqlyzGlhPXiIpPwrqICUev3qeEVRFqlXHgwOTP1HEr958nJjFFVpcVQgjx3ko4cYniEwZgUMqBtNDHWPVxI/6w5oJ3IR7fqv9bv4Qt5fav4F6HrMfRJF+/h3n7piSeuQp6uhRpWZ/kS7fztQ6icFEpC9/v6jyZLjtv3jwCAgLw8PBg8uTJ3LhxA11d3Vcu/GNpacmdO1kLjQQFBQFQunRpoqKiuHv3LpB1X+LmzZvR1dVVd6YsLS0JCwsjNTWVmJgYLl68mGP5DRo0wNfXl/T0dBITE+nduzeXL19GT09PXdb58+epV68evXr1olSpUhw7dkyd77Rp0+jbty+9e/fWuP+wY8eOTJkyhfbt2wNw9epVHB0d6d+/P05OThw+fDhbnS0tLbl37x4qlYqwsDBu376tztHf35/ExEQyMjIYNmwYBw4coGHDhhw9epT4+HhSU1M5dChr2kb16tW5cuWKesXarVu3Ur9+/RzrHxoaikKhYPDgwdSvX59Dhw7l+LdwcnLi8uXL6s73rFmzOHLkSI5lAhp/C1FwWRUxYVpPF7zWHcRjzmbuPI1mtHsjrodF0n1B1lTzeuVL0s+lBgNX+NN9vi9bTlxj0YB2Ws5cCCGEeHuZUbGEj13CRyvGUe7gSgwrOvJ05hqMnMpRds/SNx7/1Od/6JqbUv7QSsrtWUrGk+c8X70jHzIXouDIk5FMT09PRo8ejZ+fH7q6usydO5d79+4xZcoU5s6dmy1+9OjRDB48mGLFilG7dm1evHiBoaEh8+fPZ+zYsaSnp/Pxxx8zb9480tLSiI+Px8vLi/nz5/PJJ5/Qvn17SpQoQe3atXPMp2fPnjx48AAPDw8yMjLo3Lkz9evXJz09HQcHBzw9PVmwYAHDhw+nY8eOAFSrVo3w8HD27t1LWFgY33//PSqVii5durB3717c3Nxwd3dnyZIlLFq0CIDGjRuzefNm3NzcUKlU1K1blzt37mjk0qhRI3bs2EHbtm0pXbq0OmdXV1du3bpF9+7dyczMpGnTpnh4eKBQKOjXrx9du3bF3NwcBwcHAIoVK8b06dMZPny4uh4zZ87Msf6VKlWicuXKtGvXDoVCQZMmTXLskNvZ2TFhwgQGDBiAUqmkRo0adO7cmR9++CHHcsuWLavxtxAFV9MqjjSt4qixr6ipEb5juqu3ezapRs8m1V5bzpC2dfMkPyE+dDMnjtZ2CkIUKgnHLpBwTPPReplX76pHK1+W/iiSm07d/o6LiSd8lPzuEe+QlhfpyQsKlcx7FPksOXCxtlMQ4oOjV99d2ykI8UG6XS97p0UIkT+qhezRdgq5krRyRJ6UazJkWZ6Umxv59pxMIYQQQgghhBCFX55MlxVCCCGEEEIIkQuy8I8QQgghhBBCCPFqMpIphBBCCCGEENpSCBf+kU6myHehXx/UdgpCfHCK2vhrOwUhPkjH0ktoOwUhPlivXxP/PVIIO5kyXVYIIYQQQgghxDsjI5lCCCGEEEIIoS2F8ImSMpIphBBCCCGEEOKdkZFMIYQQQgghhNAWuSdTCCGEEEIIIYR4NRnJFEJg1rwuNqP7ozDQJ/X2fZ6MX4wyITnn2JYNcZg/mj9rdgVAp6gZxacNx7ByGVTJKcTsOMSL9bvzM30hCizDRg0wHzwQhb4+6fdCiJk1H1VSkkaM+YghGLl8giouHoCMh2G8mDxdI8Zy1jSUz6OI/X5pvuUuRGHi6FqDht7d0TXQJ+rmQ454/UT6P74Hnfq1oppnC0BF7INIjo5dQ3JUnHYSFoWLUu7JfC1vb2/8/Pze+rhly5axbNmy18YsXbqUCxcuvFW5FStWBCAhIYHOnTvToUMHfv31V5YsWfLa4yZMmMDVq1df+b6rqyvh4eFvlUtOeb0vjh8/TtOmTRk9evRbHxscHMz8+fPzICuRX3StzCk+5xvCh88kpM0g0sKeYjvm8xxj9R0dsPMegEKhUO+zGz8IZVIyIe0Gc7/bt5g1q4OZS738Sl+IAkvHoigWE8YSPX4Kkb36kfH4CeZDB2WLM3CqyovJM3jW/0ue9f8yWwfTrE9PDKo751faQhQ6RlZFaLHwS/YNWsLG5l7EPoyk0bgeGjE2TqWo+ZUbOzymsbnlOGLvP6X+mK5aylgUOipl3ry0qMBMlz1//jyZmZn/6tibN29iYGDAnj17+Oyzzxg1atRr42fOnImTk9O/OldBtH//foYPH87ChQvf+ti7d+8SFRWVB1mJ/GLapBYpV/8k/cFjAGI2BWLu7pItTmFkSImFXkTM+p/GfqNq5YjdFZR1P0F6BgnHzlOkbeN8yV2IgsywXl3Sb94mM/wRAEl+/hi3bqEZpK+PfvnymPXtic36NVjOnIauna36bYOa1TFsUJekXQH5mboQhcrHzZyIvHKf2NAIAK6tP0KFTxtpxDy7GsqGZmNIi09G11AfU3srUmIStJGuEAXCG6fLqlQqFixYwOHDh9HV1aVHjx5UrlyZRYsWkZKSQlxcHOPGjaNly5Yax61bt47Nmzejq6uLi4sLXl5eeHt7U69ePTp37gxkjejdvn1b47gNGzbg7+9PcnIy+vr6LFy4kODgYK5du8bEiRNZvnw5RkZGTJ06lZiYGIyMjJg0aRJVqlQhPDwcLy8vkpKSqF69OgBRUVGMHz+e58+fM3jwYFq3bs25c+eYM2cOrq6uuLu7c+LECZKTk5k7dy7VqlXD09OT4cOH4+joyJgxY0hKSkJHR4eJEydSo0YNAFasWMHNmzdJTk5m3rx5VK9enQcPHuQ6L4DTp0+rRwGLFi3KwoULsbKy4n//+x/btm3D0tKSsmXLUrx4cUaMGKHxefn5+anrsW/fPtauXUtKSgppaWnMmjWLWrVq4enpSdGiRblz5w6LFy/m2bNnLF26lIyMDEqWLMmMGTM4fPgwR44c4fTp0+jo6FCvXr0c6/D8+XMmT57M06dPUSgUjB49mmrVqrF06VKSkpJYuXIlQ4YM+Zf/Gwpt0re3If3Jc/V2+tPn6BYxRcfMWGPKbPEZI3ixZS+pt+9rHJ9y5TZFP3Ul6Y8bKAz0KdKmMaqMjHzLX4iCStfOhsyISPV25rNn6JiZoTAxUU+Z1S1mTerFP4hfvYaMkFBMe/fAaq4Pz/oPQqeYNUW/HkHUt2Mx/bSjtqohRIFn5mBNwuO/L5gnPInG0NwEfTNjjSmzyoxMSrepjeu8gWSmpXN2wXZtpCsKow9xuuz+/fv5448/2L17N9u2bcPPz48ffvgBHx8fdu7ciY+PT7bpp8HBwWzatInt27cTEBDA9evXuXbt2huTSUhI4PDhw6xfv549e/bQvHlzNm7cyKeffkq1atXw8fGhYsWKfPfdd3h5ebFz505mzJjBN998A8CMGTPo3Lkz/v7+1KpVCwBra2t8fHyoVq0aq1atynZOCwsLtm/fTs+ePfnxxx813tu+fTvNmzfHz8+PkSNHcvHiRfV75cqVY9euXXh6erJmzRqAt8oL4IcffmDq1Kn4+fnRqFEjbty4QXBwMNu3b8fPz4+ff/75tdN2AZRKJVu2bGHVqlUEBAQwcOBAVq9erX6/YsWKHDhwADs7OxYuXMiaNWvYtWsXTZo0YcGCBXTr1g1XV1dGjhxJt27dXlmHmTNn0qVLF/z8/Fi5ciWTJ09GR0eHkSNH4urqKh3MgkxHkePzmVSZf0+zsOzdHlVmJrHbD2WLi5j9E6igtP8yPvphEoknL6FKl06mEG+k0Mn52WgvrTKY+eQp0WPGkRESCkDipq3olnBAt2QJLKdNInbpCpRR0fmUsBCFk0JHgYrXfw/+5f6Bi6ypPoRz3/vhvuE7eOn2ESHE3944knn+/HnatWuHgYEBBgYG+Pv7k5qaytGjR9m/fz9XrlwhMTEx2zEuLi4UKVIEyBrVzA0zMzMWLlxIYGAgoaGh/P7771SuXFkjJjExkWvXrjFu3Dj1vqSkJF68eMG5c+fUUz7d3d2ZOHHiG8/ZtGlTAMqXL8/Bgwc13mvYsCEjRozg5s2bfPLJJ/Tt21f93l8jt+XKlePAgQP/Kq8WLVowfPhwWrZsSYsWLWjcuDE//fQTzZs3x8zMDID27duTnp7+yvx1dHRYsWIFQUFB3L9/n3PnzqGj8/e1A2fnrPt0rly5wpMnT/jss8+ArM5p0aJFc/3Znjp1ipCQEJYuzVpUIiMjg7CwsDd+vuL9l/74GcbV/75PWM+uGJkx8aiSU9X7inZuicLYkNIBy1Do66MwMqB0wDLCBk4BXV0i5q1BGZs1bch6cHfS/n/qrRDi1TIjItCv+vd3nK6NDcq4OFQpKep9emXLoF++LMn7X77Ao0DX2go9h+IUHTEUAB1rK9DRAQMDYucsyK8qCFEoxD+Kwq5mWfW2mb0lKTEJZLz8PVjKDhObojw5/ycAN7cep/nsLzAqairTZsV/piqEjzB5YydTT09PY5GP8PBwRo0aRf369alfvz4NGzZkzJgxrz0mIiICY2NjFAoFqv+/aptTx+nJkyd4enrSt29fmjVrRrFixbh586ZGjFKpVHd2//L06VMsLCwA1OUrFAqNztarGBoaquP/qXbt2gQGBnLs2DH27t3Lzp07Wbt2LQC6uroax/2bvPr374+LiwtHjx5l/vz5BAcHqzvmf9HT09P4rFQqFQqFgoz/n46YmJhI165dcXd3p27dulSsWJGNGzeq442MjADIzMykVq1a6tHc1NTUbBcHXlcHpVLJL7/8oq5PZGQk1tbW2f4+ouBJPPEHduMGou/oQPqDx1j2ciP+yBmNmNCu36j/W7+ELWUCV3LffQQANt/2Q8fMhIjpK9G1tsCiexsejZqTr3UQoiBKPXcB8xFD0C1ZgszwR5h82pGU309qBqmUFP16BGlXrpL55CkmnTuRcS+EtCtXifD4e2GSIgP6oVO0qKwuK8S/EPbbVZpM6k3RUnbEhkZQrW8L7h/8QyPGxNaCNsuHsaXNeFJeJFDBozHRt8OkgynejQ9xumzdunU5ePAg6enpJCcnM2DAAO7cucOoUaNo1qwZR44cybYgT506dTh+/DiJiYlkZGQwevRorl27hoWFBXfv3gXg8OHD2c519epVHB0d6d+/P05OThw+fFhdtq6uLpmZmRQpUoRSpUqpO0InT56kT58+ADRq1IiAgKzFDw4ePEhqamq2c7yNefPmERAQgIeHB5MnT+bGjRuvjP03eXXr1o3ExET69+9P//79uXHjBg0bNuTYsWPExcWRlpamMbpqaWnJnTt3UKlUBAUFARAaGopCoWDw4MHUr1+fQ4cO5bhAUvXq1bl8+TL372fdT/fDDz8wb968XNehQYMGbNq0Ccha7Kdjx44kJyejq6ur7vCKgikzOpbH3osouWw8ZfavwrBiKSJm/w+jauUpHfD6VZ8Bon70Rd++GKUDf8Bx/WyeLd5AytU7+ZC5EAWb8kUMMTPnYTVzGjab1qFftgyxS1eiX6kCNuuyFtjKCAkldtFSrObPwmbTOoybNeHFlBlazlyIwiU5Ko4jo1fT7seR9A6ai3WljzgxYyO2zqXpsX8mAE/O3ebCMn88tk2gx/6ZlHdvQODAxdpNXIj32BtHMlu1asW1a9fo3LkzSqWSfv368eDBA9q3b4+enh4NGjQgJSWFpJee61W1alX69u1Lz549USqVtGrVikaNGlGyZEm+/vprOnbsSIMGDbCxsdE4V+PGjdm8eTNubm6oVCrq1q3LnTtZP1abNm3KlClTmDt3LvPnz2fq1Kn89NNP6Ovrs2jRIhQKBZMnT8bLy4utW7dSrVo1TE1N/9OH4+npyejRo/Hz80NXV5e5c+e+Nv5t8/r222/x9vZGT08PExMTfHx8KFWqFIMHD6Z3794YGxurp80CjB49msGDB1OsWDFq167NixcvqFSpEpUrV6Zdu3YoFAqaNGmice/oX2xsbJg1axZff/01SqUSOzu7HB898qo6TJw4kcmTJ9OxY9biEvPmzcPMzAxnZ2eWL1/OggULso1oi4Ij8fgF7h/XfERQSuwd9Wjly9IfRXK7Rhf1tjIxmfCh8qNXiH8j9fRZnp0+q7Ev/VY8z/p/qd5OPnCY5APZL8y+LH7NL3mSnxAfigdHr/Dg6BWNfZEx99nadoJ6+9r6I1xbfyS/UxMfAi0/biQvKFSqnFYdEO+Lv54fOmJE9h/7BdXN8m7aTkGID05Rm+Q3Bwkh3jm/sBLaTkGID9bwsA3aTiFXEn36vjnoXzCdqL36v3EkUwghhBBCCCFEHimE92RKJ/M9V5hGMIUQQgghhBD/UAhXl33z8qtCCCGEEEIIIUQuyUimEEIIIYQQQmiLTJcV4r87k2Sl7RSE+OA8e6TtDIT4MOlnfwy3EEIUetLJFEIIIYQQQghtKYSPMJF7MoUQQgghhBBCvDMykimEEEIIIYQQ2iL3ZAohhBBCCCGEeFdU8ggTIYQQQgghhBDi1WQkUwhByRY1qO3dHV1DfaJvPuTk6J9IT0jWiKncvxUVP2sBKhXxDyI56bWGlKg49IsY02ThlxQtWxyFjg53t/3O1R/2aKkmQhQsZVxr0Gxsd/QM9Im89ZD9Y38i7R9tr4pHY+oNckOlgoyUVI5MWc/Tq/dR6ChoOb0fHzWoDEDI0cscm7lZG9UQosAp7VqDxt91R9dAn+e3HnLIK3vbq+TRmDpf/X/bS07l2NT1RATfx7CoKS1mfY5NFUfSk1K5se04l9cd0lJNRKFQCKfLykhmHjl79iyenp65jl+6dCkXLlwAwNPTk7Nnz+ZVau+Ur68ve/ZIh6IgM7QqQpPvv+TooCX4NfMi4UEktcf30IixdipF1cFuBHaaxq4W44i7/5RaY7sCUMurK4lPotnVYhy73SZT8bMW2NQup42qCFGgGFsVod38L/EfvISfXL2IfRjJJ96abc+qTHGaj+/Ftn7z+cVtAqeX+fPpj6MAqNq5CVZli7O2tTfr2o7no/qVqehWTxtVEaJAMbYqQusFX7LnqyX84pLV9pr8o+1ZlilOswm92PnZfDa2m8DZZf50+P+213xKX9ITU/i1xVi2fDqFUs2rU7pFDS3URIj3l3Qy3xPnz58nMzNT22m8tT/++IO0tDRtpyH+gxKfOPH8yn3i7kcAcOvXI5T1aKQRE3U1lB1NxpAen4yuoT4m9lakvEgA4Ozk9ZyfvgkAYzsLdA30SYtLyt9KCFEAlW7mxNPg+7wIzWp7lzYcoUonzbaXkZbO/u9+IjEyBoCnwfcxtbFAR18Xha4O+saG6Broo2ugh66+Lhmp6fldDSEKHMdmTjy9cp+Y/297weuPUOlTzbaXmZbOobF/t72Il9qerVMpbvqdRKVUoUzP5H7QZcrLBR7xXyhVefPSIpkum4devHjBgAEDiIyMxNnZmSlTpuDr64u/vz/Jycno6+uzcOFCgoODuXbtGhMnTmT58uUAbN++nTlz5hAXF8eECRNwdXXF29ubmJgYHjx4gJeXF1ZWVsycOZPU1FQsLS2ZPn06jo6O3L9/n8mTJxMTE4OJiQkTJkzA2dkZb29vjI2NuXHjBnFxcXz77bf4+/tz69YtWrZsibe3N6mpqUybNo2LFy+ir6/P0KFDcXNzw9XVFXd3d06cOEFycjJz584lLi6OoKAgzpw5g42NDU2bNtXyJy7+DVMHaxIfR6m3E59EY2Bugr6ZscaUWVVGJh+3qU3jBQPJTE3n0oLtf7+XqaTZ0iE4tq/Lw/0Xibv3JF/rIERBVKS4NfEvtb34J9EYmptgYGasnrYXF/6cuPDn6hiXSX24e/gPlOmZXNv2GxXd6jP03DJ0dHW4//tV7h25lO/1EKKgKeJgTcKTt2t7n0zqQ8j/t72nl+5RuXNjHl/4E10DPcq1q4syo+ANFIj3iDwnU7yN8PBwJk2aREBAAImJiWzevJnDhw+zfv169uzZQ/Pmzdm4cSOffvop1apVw8fHh4oVKwJQpEgRdu7cycSJE1mxYoW6TAsLC/bt20eTJk349ttv1eX37NmTb7/9FgAvLy88PT3ZvXs348aNY9SoUerRxsjISLZu3cqgQYMYN24c06ZNY9euXfj6+hIfH8/69etJSkpi3759rF27lhUrVqiPtbCwYPv27fTs2ZMff/yRRo0a4erqysiRI6WDWYApdBSgyn61S5WZ/R+8hwcustlpCJe/96P1xu9AoVC/99vIlWx2GoKhhSnVv/HI05yFKAwUOgpUuWx7+saGuP8wAktHO/Z/9xMAjb/uTHJ0HMtrD+WHBiMxtjCj7pft8jxvIQo8Rc5tT5lD29MzNqT9yhFYlLLj0NistvebzyZUKuizzwf3n77h4YlrKNMz8jxtIQoS6WTmoTp16lCqVCkUCgUdO3bk3LlzLFy4kMDAQBYuXMjRo0dJSsp5WmHLli0BKFeuHC9evFDvd3Z2BiA0NBRzc3P1drt27Xj48CHx8fE8fPiQ1q1bA1CjRg2KFi1KSEgIAM2aNQPAwcGB8uXLY21tjZmZGRYWFsTGxnL+/Hk6duyIjo4ONjY2BAYGYmBgAKDuSJYvX56YmJh3/GkJbUl4FIWJnaV628TektQXCWQkp6r3FSllh23dCurtO1uOY1qyGIYWpjh84oSxnQUAGUmphPifxtqpVH6lL0SBFfc4CrOX2l4Re0uSYxJIf6ntQdaoSx+/yagylWzpOZPU/5+OXr5tHa76HkeZnklafDLXtv/Oxw2r5GsdhCiI4v/R9szsLUmJ0fzeg6y213NnVtvb1uPvtmdgZszvszazvtU4dvSeAyjUU2+F+FcK4XRZ6WTmIT29v2cjq1Qq4uLi6NGjB/Hx8TRr1gwPD48cr6QB6OrqAqB4aaQIwMjICABlDs/TUalUxMfH57j/r/s99fX1c8zv5X0vn/PBgwfqkUxDQ8MccxIF2+PjV7GpVQ7z0nYAVPJswcODf2jEmNha0HzlcAwtzQAo07kxMbfDSH2RQOmO9an5bWcAdAz0KN2hPk9O3sjfSghRAIX+dhWHmuWwLJXV9mr0acHdf7Q9A1Mjem2dwJ/7L7B7xAqNey4jroVSsX19AHT0dCnXqhaPL93NvwoIUUA9+O0q9jXLYfH/bc+5bwvu/aPt6Zsa0c13Anf3X2Dv8BVkvtT2nPu2oNHoLgCYFDPHqVdzbu06nX8VEKIAkHsy89DFixd5/Pgx9vb27Nq1i2bNmnHmzBn69+9PSkoKS5cuxd7eHsjqVL7Nwj9lypQhJiaG4OBgnJ2d2bt3Lw4ODjg4OFCyZEkOHjxI69atuXz5Ms+fP6d8+fK5Krdu3brs3bsXFxcXoqOj6du3L/v27Xtl/NvmLd4/KVFxnPh2NS6rR6Kjr0f8g0h+G7UKa+fSNF4wkIDWE4g4d5srS/1pt30CykwlyU9fcOSLxQCcn76JhnM+59MjswF4sP8iN346oMUaCVEwJEXFsc9rNZ1WjkTXQI+YB5EEfrMKe6fStJk7kF/cJlCzXyvMSxSjfJs6lG9TR33s1t6zCZq+kVbT+zHgyDxUSiUPTl7n7CpZ7VuIN0mOiuPgmNV0WJX1vRf7MJL9X6/Czrk0LecOZGO7CdTo34oiJYpRtk0dyr7U9nb0ms25FQG0XTwYz0OzUSgUnFq4g4jgEC3WSBR0qkL4CBPpZOahcuXKMX78eJ49e0aDBg3o2bMnp06dws3NDZVKRd26dblz5w6QNRV1ypQpzJ07N1dlGxgYsGjRImbMmEFycjJFixZl0aJFAMyfP5+pU6eybNky9PX1WbZsmXrK65v07t0bHx8f3N3dAZg0aRJmZmavjG/UqBHff/89RYoUoW3btrk6h3j/hAddITzoisa+qJj7BLSeoN6+/esRbv96JNuxaXFJHB+6Itt+IcSbhRy9QshRzbb39Op9fnHLantnf9jN2R92v/L43SOl7Qnxb4QevULoP9peRPB9NrbLanvnV+zm/IrXtL0vF+dleuJDUwg7mQrVq+ZrCpFH1pboq+0UhPjgPJNLikJohb78yhJCa755uEHbKeRK/MgOeVJukaXam90iPzuEEEIIIYQQQltyWGuloJOFf4QQQgghhBBCvDMykimEEEIIIYQQ2lII78mUTqbId0f0cn42qBAi76QgDwoXQivkqV9CaM032k7gAyadTCGEEEIIIYTQFhnJFEIIIYQQQgjxrhTGh33Iwj9CCCGEEEIIId4ZGckUQgghhBBCCG0phNNlZSRTCCGEEEIIIcQ7IyOZQghquNam+9g+6Bvo8/DWA34au4LkhOQcY79aOIKw2w/Zu9pfva+lZ1ua92yJvpEBoVfv8b+xK8hIk9VMhXiTWq516DP2M/QM9Hh46wE/jF36yrY3fOHXPLwdSsDqXdne8/pxHNER0ayZ/GMeZyxE4fBf2p5JEROGzBtBibIlUegoOL49iF2r/PIxe1HoyEim+DfOnj2Lp6dnvh+bH5YuXcqFCxe0nYb4D4pYmfPl/OEsGTwfL9cRRD6MoId39v/nHMqVYNzmadR1a6ixv07b+rTq78bs3lPxbjkKfSMD2g3omF/pC1FgmVuZM2z+SOYPns0o16FEPHxKH+9+2eJKlCvJlM0+NHBrlGM5nb7qTKW6VfI6XSEKjf/a9nqO7kPUkyi+bT0C746jad23HRVqVcyv9EUhpFKq8uSlTdLJFP/J+fPnyczM1HYa4j9walaD+8F3iQh9AsCRDftp1KlptriWn7Xj2JbDnAs8pbG/Sefm7PtfAImxCahUKtaO/5ETfsfzJXchCrLqzWpyN/gOT/+/7R3YsI+mnT7JFtf2s/Yc2XKQ04Ens71XtUE1ajSvxaGN+/M8XyEKi//a9n6e+j9+nfkzAJa2Vugb6pMUL88AF+JlMl02n7x48YIBAwYQGRmJs7MzU6ZM4cyZMyxdupSMjAxKlizJjBkzsLS05MSJE8yePRtDQ0NKly6tLsPT05OiRYty584dFi9ezNOnT1m8eDFKpZKPPvqI6dOnU6xYMS5fvszMmTNJTU3F0tKS6dOn4+joiKenJ1WqVOHixYukpqYyZswYfv31V+7du0f//v3p378/MTExTJgwgZCQEAwMDPD29qZhw4Y0adKENm3acPHiRXR1dVm8eDEXL17k2rVrTJw4keXLl1OxolzFK4isi1sT9fi5ejv6SRQm5qYYmxlrTB36dfJPADg1ra5xfPEyDoRcucvYXyZhYWfJ7fM32TLr1/xJXogCzLp4MY22F/XkOaY5tL2/psBWb1pT43hLWys+n/IlPv2m0rp32/xJWohC4L+2PQBlppKRi7+lQbtGnDtwhsf3HuV94qLwkumy4t8KDw9n0qRJBAQEkJiYyOrVq1m4cCFr1qxh165dNGnShAULFpCWloa3tzdLly7Fz88PIyMjjXIqVqzIgQMHsLW1ZfLkyaxYsYLdu3dTq1Ytpk+fTlpaGt9++636XD179uTbb79VH69Sqdi+fTtt2rTBx8eH5cuXs3HjRlasWAHAkiVL+Pjjj9m3bx/z5s1j8eLFADx79oyGDRuya9cu6taty8aNG/n000+pVq0aPj4+0sEswBQ6Ojk+n0mZqczV8bp6ulRr6syyYQuY1HEsZkXN6ObV512nKUSho6OjQ06PRstN29PV0+XrZWNYN2MNMZEv8iA7IQqv/9L2Xrb06+/5omZfzCzM6DqqxzvKTojCQTqZ+aROnTqUKlUKhUJBx44d+eWXX3jy5AmfffYZnTp1YuPGjTx48IDbt29ja2tL2bJlAfDw8NAox9nZGYDg4GCcnZ0pWbIkAD169ODMmTOEhoZibm6ujmvXrh0PHz4kPj4egGbNmgHg4OBA9erVMTY2pkSJEsTFxQFZ0187deoEZHVot27dqj5306ZZUyjLly9PbGxsnnxOIv9FPX6GpZ2VetvS3pqEmHhSk1NzdfyLiBec33+W5IRkMtMzOLnrN8rVqpBX6QpRaDx7/Ayrl9qelb018blse2Wdy2H3sR39Jn7B/L2LadWnLY06NGHw3OF5mbIQhcJ/aXuQNd3W0jbr+JSkFE4E/EaZamXzJFfxgVDm0UuLZLpsPtHT+/uj/mvUqFatWqxatQqA1NRUEhMTefz4scaokq6urkY5f41sKpWa/+eoVCoyMjKy7f/rvb/um9TX188xp5f3KRQK9fa9e/fUU3YNDQ0BUCgUOY58iYLp6m9X6D2hP3alihMR+oQWfVrzx8HzuT7+3L7TNGjfiGObD5Oemkbt1vUICb6bhxkLUThc+e0S/SZ8gX2p4jwNfULrPu04f/Bsro7984/bDG44QL3d/eteFLEyl9VlhciF/9L2ABp1aEL9tg1ZPf4H9Az0aNShCcG/X867hEWhp+1FevKCjGTmk4sXL/L48WOUSiW7du2iX79+XL58mfv37wPwww8/MG/ePCpWrMjz58+5desWAIGBgTmWV716da5cuUJ4eDgAW7dupX79+pQpU4aYmBiCg4MB2Lt3Lw4ODlhYWOQqzzp16qjPee/ePb788kuNTuc/6erqysI/BVxcVCyrvZYzcqUXc48s5aNKjmz0WUdpp7LM3Lvwjccf/nU/104E4xM4n/lByzAyMWLbvI35kLkQBVtcVCwrvJYwZqU3i4+swLGSI7/6/ExZp3LM37tY2+kJUWj917b3i8/PmBYx4fuDy5i3ZxEhV+8R+PPuvE9ciAJEoZIhqTx39uxZFi9ejKGhIc+ePaNBgwaMHz+e48ePs2TJEpRKJXZ2dsyfPx9LS0vOnz/P9OnT0dPTo0qVKjx8+JD169fj6enJ8OHDqV+/PgBBQUEsXbqU9PR0HBwcmDlzJra2tly6dIlZs2aRnJxM0aJFmT59OmXLltU43s/Pj3PnzjFnzhwga2rs7du3iYuLY+LEiYSGhqKnp8f48eOpU6eO+n1A49g1a9awZcsW5s6dS61atXL1efR17Jw3H7QQ4pVSkOeWCiGE+LBsfxCg7RRyJaaXS56Ua7H5aJ6UmxvSyRT5TjqZQuQ/6WQKIYT40EgnU3udTLknUwghhBBCCCG0RcuL9OQFuSdTCCGEEEIIIT5gu3fvxs3NjdatW7NxY/a1NUJCQvD09MTd3Z0BAwa88UkT0skUQgghhBBCCC1RKVV58sqtiIgIFi1axKZNm9i1axdbt27l7t2/nxSgUqkYMmQIX375JQEBAVSuXJnVq1e/tkyZLiuEEEIIIYQQ2pJH02Xj4uKIi4vLtt/c3Bxzc3P19qlTp2jQoIH6aRRt2rRh//79DB+e9ezl69evY2JiQrNmzQAYPHhwjuW+TDqZIt8lyQIkQuQ7e4WRtlMQ4oN0OuWRtlMQQnygfvnlF5YvX55t//DhwxkxYoR6OzIyEhsbG/W2ra2t+nGIAA8fPqRYsWKMHz+emzdvUqZMGSZNmvTac0snUwghhBBCCCG05G2mtr6Nfv364eHhkW3/y6OYAEqlEoVC8Xc+KpXGdkZGBufOnWPDhg04OTmxePFi5syZo34UYk6kkymEEEIIIYQQhcw/p8W+ir29PRcuXFBvP3v2DFtbW/W2jY0Njo6OODk5AdChQwdGjhz52jJl4R8hhBBCCCGE0BZlHr1yqVGjRpw+fZro6GiSk5M5ePCg+v5LgJo1axIdHc2tW7cACAoKomrVqq8tU0YyhRBCCCGEEEJLVFp+TqadnR3ffPMNn332Genp6XTt2hVnZ2e+/PJLRo4ciZOTEytWrGDixIkkJydjb2/PvHnzXlumQqVS5c0kYCFeobOju7ZTEOKDIwv/CKEdsvCPENpz6elJbaeQK1EdP8mTcq13H8+TcnNDRjKFENR2rUOfsZ+hb6DHg1sPWDF2KckJyTnGjlj4NQ9vh+K/ehcAJkVMGDZvBCXKlkSho+DY9iB2rvLLx+yFKDyqudSk09je6Bno8+jWAzZ8t4qUf7TFep82peVXHVGpID05Fd+pa3l4NURLGQtRcDRp2ZAR4wdjYGDAnZt3mfbNbBITknIVY1bElCmLxlGqnCM6Ogp2++5j3fKNlKlQilk/TFUfr6OrQ/nKZRn9xXiC9mrvB74oYLQ8kpkX5J7MD1inTp0ACA4OZv78+f+qDF9fX/bs2fMu0xL5zNzKnOHzRzJ/8GxGuA4l4uFTPL37ZYsrUa4k0zb70NCtkcb+XqP7EPUkiq9bj2Bsx9G06duOCrUq5lf6QhQaZlZF8Jw/lNVDFjKtxdc8D4vk0+96a8TYlimOx/i+LP9sFrPdxrJvmR+DVo3RUsZCFByW1hZMWzwBrwET8GjSi/AHjxk5cUiuY4Z+9yURj5/RrbknfdoOpFs/D5xrVyXkz1B6tuyvfp05fo59fgelgyk+eNLJ/ID5+/sDcPfuXaKiov5VGX/88QdpaWnvMi2Rz2o0q8nd4Ds8CX0CwP4N+2jaKfu0jXaftefwloOcCtScerJm6v9YN/NnACxtrdA31CcpPinb8UKI16vctDoPgu/xLPQpAL9tOEjdTk01YjLSMtj43SrinsUA8ODqPcxtLNDV183vdIUoUBp8Uo/rl2/y8H44ANt+2Um7zq1zHTNv4mIWTct63qCNrTX6BvokxCdqHF+zfnVadnBh5th/d+FefLhUyrx5aZNMly1Ezp49y/Lly1m/fj0A3t7e1KtXj19//ZXy5ctz8+ZNrK2tWbJkCRYWFlSsWJHz58+zdOlSkpKSWLlyJV988QXTpk3j4sWL6OvrM3ToUNzc3HB1dcXd3Z0TJ06QnJzM3LlziYuLIygoiDNnzmBjY0PTpk3fkKF4H1kXL8bzx8/V21FPnmNqboqxmbHGlNmfJv8IQPWmNbOVocxUMmrxtzRs14izB87w+J7cgyTE27J0sObFk78v+MU8icLY3AQjM2P1lNno8GdEhz9Tx3Sd2I/gwxfITM/M93yFKEjsHWyJeBSp3o58/Iwi5maYmpmop8y+KSYzMxOf5ZNp2aE5R/f9Rujdhxrn+HryMJbP/jHbFFwh3kimy4qC6NatW3z++efs2bMHc3Nzdu/erX7P3NyckSNH4urqypAhQ1i/fj1JSUns27ePtWvXsmLFCvVIpYWFBdu3b6dnz578+OOPNGrUCFdXV0aOHCkdzAJMR0eHnJb/Uma+3b94S77+nv41+2JmYUa3UT3eUXZCfDgUCh1yWosvp7ZoYGzIwBXfYFPKjo3eq/IjPSEKNIWODiqyt69MpfKtYiYOn45LlfaYW5gzaPTn6v3V61TD0tqCfX6H3nHmQhRM0sn8AFhbW1OlShUAypcvT2xs7Ctjz58/T8eOHdHR0cHGxobAwEAMDAwA1B3J8uXLExMTk+d5i/zx7PEzrOys1NvW9tbEx8STmpyaq+NrNKuJpW3W8SlJKZwI+I0y1crmSa5CFGYvHj+nqJ2letvC3orEmATS/tEWLR2sGeM3A6VSyeKe00iOk1ETId7k6aOn2NgVU2/bFi9G7Is4UpJSchXTsHk99XvJScns33WYSk4V1LGtO7Vgz7Z9OV4oEuJNCuN0WelkFiIKhULjH7f09HQADA0NXxnzT3p6eigUCvX2gwcP1COZf5Xz8vui4Lvy2yUq1KxI8VLFAWjdpx3nD57N9fGNOjShx9c9AdAz0KNRhyZcPRWcJ7kKUZjd+P0KpWuUx6aUPQBN+7Qi+NB5jRhDUyO+2TKVy/vP8fOIJaSnpmsjVSEKnNPHz+FUuyofly4JQNfPPDh24Pdcx7R2d1WPXOob6NPa3ZXzJ/5QH1u7YU3O/X4xP6oiRIEgncxCxNLSkrCwMFJTU4mJieHixdz9Y6erq0tGRgYAdevWZe/evahUKqKioujbt+9rF/bR1dUlM1PuBSrIYqNiWe61BK+V3iw9sgLHSo6s8/mZsk7lWLh38RuPX+fzMyZFTFh8cBkL9iwi5Oo9An/e/cbjhBCaEqLiWO+1ki9Xfsvkw9/jUPFjdvj8ysdOZRi3N+uh1837tcWqhA3V29Rj3N556pephZmWsxfi/fbieQxTv57F/J982PHbRspVLsP3U5dRpXolthxe99oYgIVTl1PE3Ixtx9az6eDP3Lxym03/81WX/3GZkjwOe6KNqolCoDCOZCpUMq5fqEyePJlTp05RokQJihUrRuPGjVm+fDlBQUEALFuW9Y/liBEjqFixIrdv3+b+/fsMGjSINm3aMHLkSHx8fLh06ZI6rnXr1ri6uvLrr79SsmRJjQWGAgMD+f777/Hy8qJt27a5yrGzo3veVF4I8Ur2CiNtpyDEB+l0iiyEJoS2XHp68s1B74EIl+yr+r8Ldke19ygd6WSKfCedTCHyn3QyhdAO6WQKoT3SydReJ1MeYSKEEEIIIYQQ2qIqfOudyD2ZQgghhBBCCCHeGRnJFEIIIYQQQggt0fYiPXlBRjKFEEIIIYQQQrwzMpIp8l16YbxcI8R7Lo4MbacgxAdJX0d+agkhXk+lLHz3ZMq/fEIIIYQQQgihJYVx/EWmywohhBBCCCGEeGdkJFMIIYQQQgghtEQljzARQgghhBBCCCFeTUYyhRBCCCGEEEJLCuM9mdLJFEJQx7UOn33XD30DfUJvhbLUawnJCck5xn79/Tc8uBXKztU7ATAwNGCwzxAq1KiAQgG3L/3JqokrSUtNy88qCFEg1XCtTfexfdA30OfhrQf8NHbFK9veVwtHEHb7IXtX+6v3tfRsS/OeLdE3MiD06j3+N3YFGWmykrAQOWncogFDxw3CwFCfuzdC8Bk9l8SEpFzHHLzmT+STZ+rY9T9s4cDOw5hbFGGMzyhKVyiFoZEBa5dsYN+Og/laN1GwFcbVZWW67AfK09OTs2fP5jr+yJEjLFmyBABXV1fCw8MJCwtj/PjxeZWiyCfmVuaMWvA1s7+azRCXwTx9+JT+3v2zxZUsVxKfzTNp7NZYY3/3Ed3R1dNlROvhjGg9AgMjA7oN65ZP2QtRcBWxMufL+cNZMng+Xq4jiHwYQQ9vz2xxDuVKMG7zNOq6NdTYX6dtfVr1d2N276l4txyFvpEB7QZ0zK/0hShQLKyKMmmRN95fTqJbU08ePXzMsPFf5Trm47IfEfcinr6tBqpfB3YeBmDy4nFEPnmGZ+uBDO8xmtEzRmJb3Cbf6yjE+0Q6mSJXWrRowahRozT2PX78mLCwMC1lJN6Vms1qcefKHZ6EPgZg3/q9fPJp82xx7T/rwKEtBzkZeEJj//Wz19m6dAsqlQqlUknI9RBsStrmR+pCFGhOzWpwP/guEaFPADiyYT+NOjXNFtfys3Yc23KYc4GnNPY36dycff8LIDE2AZVKxdrxP3LC73i+5C5EQVP/k7rcuHyLsPuPANjxiz9tO7fMdYxznWpkKpWs3rmMjYd/ZsA3/dDR0cHcogj1mtbhf9+vAyDyyTO+6DCY2Ji4/KucKPBUqrx5aZN0MgsALy8vfH191duenp5cuXKFzz//HA8PD3r16sWNGzcA+PPPP/H09KRLly64uLiwefNmAJYtW8aAAQNwc3Nj06ZNAPj6+vLpp5/y6aefqkc1ly1bxrJly9Tn+mvU0s/PD29vb428fHx8uHbtGtOmTcvT+ou8ZeNQjOdPnqu3nz95jqm5KcZmxhpxP05exXH/7D9gL/1+icf3szqoNiVscB/gnq0jKoTIzrq4NVGP/2570U+iMMmh7f06+SdO+/+e7fjiZRwwty7K2F8mMWv/93T+pgdJcYl5nrcQBZFdCVsiH0eqtyOfPMPM3AxTM5Ncxejq6nL+94uM7O3FV51H0qB5Xbp/0ZmSpUoQFRlFn0Hd+Z//cn7Z9yMVnSqQmpyar/UT4n0j92QWAF26dGHZsmV0796dR48eER0dzezZs5k8eTJVqlTh7t27DBs2jAMHDrBt2zaGDh1Kw4YNCQsLw93dnV69egGQlpbG3r17Adi3bx8mJibs2rWLW7duMWjQIA4fPvxWeU2cOJHly5czZcqUd15nkX8UCgWqHC53KTPf7i70sk5lmbB6AoHr9nD+yPl3lZ4QhZZCR+c/tT1dPV2qNXVm0cA5pKWmM3jhCLp59WHD9J/fdapCFHg6Cp0cR3YyX2pvr4vx37Tn7x3JsOlHX3oM6MKNK7co4ehAQkISX3YaTslSJVi9cxlhIeHcuvpnHtREFEZyT6bQivr16xMZGUl4eDi7du2iXbt2XLt2jXHjxtGpUydGjx5NUlISL168wNvbm9TUVH788UcWL15MUtLfN7Q7OztrlNu1a1cAKlWqhLW1NSEhIflaL/F+ePb4GVZ2Vupta3tr4mPi3+oqbNOOzZix0Ydf5vzCthXb8iJNIQqdqMfPsHyp7VnaW5PwFm3vRcQLzu8/S3JCMpnpGZzc9RvlalXIq3SFKNCePoqgmL21etvGvhixL+JISU7JVUy7Lq0pV7mM+j2FQkFGRgbPn2bNRtizJesifnjoI66cu0rVmpXzukqiEFEpFXny0ibpZBYACoWCTz/9lMDAQPbt24eHhwcGBgb4+/urX9u2bcPCwoKvv/6aQ4cOUbZsWb7++muNcoyMjDS2dXV11f+tVCrR09PLNqqVnp6ep3UT2nfpt0tUrFmR4qUcAGjX142zB8/k+vi6LesxaNogJvedlON0WiFEzq7+doVyNStgV6o4AC36tOaPg7mfBXBu32katG+EvqEBALVb1yMk+G6e5CpEQXf2+Hmq1arCR6VLAND5M3d+O3gy1zFlK5XmK68v0NHRwdDIgG6fe3DI/yiPw55yM/g27bu3BcCqmCVOdapy48rtfKydEO8fmS5bQHTu3JnevXtTrlw5SpQoQalSpfD396dTp06cPHmSyZMnc/jwYU6ePMm+ffuws7Nj48aNAGRmZuZY5u7du6lWrRpXr14lMTERR0dHLC0t1fdnBgcH8+zZsxyPhaxOakaGLJVf0MVGxbJkzBLGrRqHnr4eTx8+4fuvv6ecczlGzB3JqHYjX3v8FxO+QKFQMGLu33E3L9xg1aRVeZ26EAVaXFQsq72WM3KlF3oGekQ+eMqqb5ZS2qksA+cOZYLb6Ncef/jX/ZgVNcMncD46OjqEXgthk8+6/EleiALmRVQMM76Zw5zV09Ez0OdR6COmjppFZeeKTFjoRd9WA18ZA/C/79fhNfNrNgWtRU9PjyN7jqmn0I4dMJGxs76hy2edUOjosGbRL9y8ckub1RUFjLYX6ckLClVON4SI91Lv3r3p27cvbm5u3Lt3j6lTpxITE4O+vj5Tp07F2dmZtWvXsmHDBgwNDalUqRKXL19m7dq1BAQEADBixAgga/GgUqVKcfXqVXR0dJgyZQrVq1fnxYsXjBo1iufPn1O1alXu3bvH0qVLOXfuHOfOnWPOnDm4urry66+/YmpqiqenJ5UrV2b+/Pm5rkfHjzvkyecjhHi1ogoDbacgxAfpz/QobacgxAfr3OOCMcPqfvVWeVJu6SuH8qTc3JBOZgGgUqmIjIzE09OTPXv2YGBQsH8sSidTiPwnnUwhtEM6mUJoT0HpZIY4tc6TcstcPZgn5eaG3JNZABw4cIBOnTrx7bffFvgOphBCCCGEEKJwk3syC4C2bdvStm1bbachhBBCCCGEeMdUqsL3CBPpZAohhBBCCCGElqje7tHkBYJMlxVCCCGEEEII8c7ISKbId3dTIrWdghAfHBWyxpsQ2qCg8E2DE0K8W8pCOF1WRjKFEEIIIYQQQrwzMpIphBBCCCGEEFoiC/8IIYQQQgghhHhnVMrC18mU6bJCCCGEEEIIId4ZGckUQgghhBBCCC1RFcK1+WQkU4gP1CctG7Pr2Eb2ntrGop9mY2pm+tYx9g62HLuyBwurotmOLfGxA6dvH6Jq9cp5VgchCqJPWjbG/9gm9p3azuLXtL3Xxdg72HH8SmC2tte4eX12Bm3M0/yFKKjy6nuvXuPabDv4CzuPbmTL3jU41ayS53UR4n0nncwC6OrVq0yYMOFfHbt582Y2b96cbb+fnx/e3t6vPO7IkSMsWbLkX51TvH8srS2YuWQSoz73xq1RN8IfPGL0pGFvFdOpuxvr/VdjV9w2W/kGhgbM+2Ea+gb6eV4XIQoSS2sLZi2ZzMjPv6Ndo66EPXjE6EnD3yqmU3c3Nvj/qNH2DI0MGeU9mO9Xz0JXVzff6iNEQZFX33v6+np8v3omk7+diYdLH1YtWsvcFdPyrV6icFApFXny0ibpZBZATk5OzJw5818d26tXL3r16vXWx7Vo0YJRo0b9q3OK90/j5vW5dvkGD+6HAbB53Q46dGmb6xgbu2K0aPcJX/YYmWP5k+eMZdeWPcRExeRdJYQogBo3b8DVl9rVlnU76Jit7b06xtauGC3aNWfgP9peE5cGGJsY4z1CftwKkZO8+t5LT8+gefX23Lz2JwAlHR2IeRGb19URhYxSpciTlzbJPZlacvbsWebPn49SqaREiRKYmJhw584dMjMz+fLLL+nQoQPp6elMmTKFixcvYmdnh0KhYOjQoQAsX76c9evXc//+fSZPnkxMTAwmJiZMmDABZ2dnvL29MTMz4/r160RERDBs2DC6dOnCsmXLABgxYgS7du1i5cqVmJmZqXMIDw9n2LC/r9rdv3+fUaNGYWlpyblz55gzZw6urq60bduWU6dOATBr1iyqVJGpIQWJvYMdTx5HqrcjHkdSxNwMUzNTEhMS3xjzLOI5Iz//Lseyu/bphJ6+Hts2+PPV15/nbUWEKGCKO9jx9HGEevtpDm3vdTGREc8Z+fnYbOUe2XecI/uOU69RrbyvhBAFUF5+72VkZGJtY8WOw79iaWXBt4P+3WwzIQoT6WRqUWhoKEePHuXHH3/E1taWuXPnkpCQQM+ePalevTrHjh0jOTmZ/fv38/jxYzp27JitDC8vLwYNGkTr1q25fPkyo0aN4sCBAwA8ffqUTZs28eeff/LZZ5/RpUsX9XEREREsWLCAXbt2YWFhwVdffYWJiQklS5bE398fgEOHDrFq1Sr69u1LYGCgxnlNTEzYtWsXQUFBfPfdd+zevTsPPynxruno6OR4l7lSmflWMf9UxakiPfp1xrPToHeTqBCFjI6OAtUb296bY4QQbyevvvf+EvUsmubVO1DFqSI/71jBvdv3CQ15+N+SFh+MwvicTJkuq0WlS5emSJEinDp1ii1bttCpUyf69OlDUlISd+7c4eTJk3Ts2BGFQkGJEiVo2LChxvGJiYk8fPiQ1q1bA1CjRg2KFi1KSEgIAI0bN0ahUFChQgViYmI0jr106RI1a9akWLFi6OnpZevA3rp1izlz5rBs2TIMDQ2z5d69e3cAXF1diYiIIDo6+l19LCIfPHn0FBu7Yuptu+I2xLyIJTkp5a1i/qlTdzfMipiyKXANfkEbsLG3Yf7K6bi0aZo3FRGigHn8KAJbOxv1dk7tKjcxQoi3k1ffe2ZFTGnp1ly9fePqbW5fv0P5KmXfbQWEKGCkk6lFRkZGACiVSubPn4+/vz/+/v74+vrStGlTdHV1USqVrzw+pyvdKpWKzMysK25/dQ4ViuxXRxQKzSvlenp/D2pHR0czcuRIZs2ahYODQ47nfjleqVTKQhMFzMljZ6lepxqOpT8CoEe/zgTt/+2tY/5p9qRFtGvYlc6ufens2pdnT5/hNWQyRw/8njcVEaKAOXnsjEa76tmvSw5t780xQoi3k1ffe0qlEp/FE6lZzxmAchXLULp8KYIvXs+DWojCSqXKm5c2SSfzPdCgQQP1iq+RkZG4u7vz5MkTGjVqxN69e1GpVERERHDu3DmNDqOZmRklS5bk4MGDAFy+fJnnz59Tvnz5N56zdu3aXL58mYiICJRKJXv37gUgPT2dUaNG4enpSf369V95/F/TZw8dOkTZsmUpWjT7IyzE+yv6+QsmjJzB4p/nsOfEVipULse8KUuoWr0yfkEbXhsjhPj3op+/YPzI6Sz5eQ6BJ3ypULksc6csplr1yupHj7wqRgjx7+XV915SYjIj+nkxbsa3+AVtwGfxRLwGTyLiSeRrjxOisFOochoOE3nu7Nmz6sV7EhISmDp1Krdu3SIzM5NBgwbh4eFBeno606dP59KlS9jY2BAdHc2MGTNITk5WH3vv3j2mTp1KTEwM+vr6TJw4kVq1auHt7U29evXo3LkzABUrVuT27dsaC//s37+fJUuWYGxsTLly5dDR0aFx48aMGzeOSpUqkZGRgUqlolGjRpQvX15j4Z/q1asTEhKCsbExs2fPpnTp0rmue2XbennymQohXk2F/FMvhDYoKHz3WglRUNyMPKftFHLlsqN7npRb40FAnpSbG9LJfI8dO3YMlUqFi4sL8fHxfPrpp+zYsQMLCwut5uXq6sqvv/5KyZIl/9Xx0skUIv9JJ1MI7ZBOphDaU1A6mZc+7pQn5dZ86J8n5eaGrC77Hitbtixjx45l8eLFAIwcOVLrHUwhhBBCCCGEeB3pZL7HPvroI/W9mu+ToKAgbacghBBCCCFEoVAY55XKwj9CCCGEEEIIId4ZGckU+S5T9erHsggh8ka6Ml3bKQjxwTLVM9Z2CkKI95hSVfju3ZZOphBCCCFEHpEOphDiTVSFsJMp02WFEEIIIYQQQrwzMpIphBBCCCGEEFpSGKfLykimEEIIIYQQQoh3RkYyhRBCCCGEEEJLCuETTKSTKYQQQgghhBDaItNlhRCFRvNWjQk4tpn9p3ewZM0cTM1Mcx2jo6PDeJ9v2X9qO4fO7aRnvy4AlK1QGv+jG9Wv3ce38OezC7Ru75KvdRPifefSqin7ftvGkbP+rPh5PmZFsre/3MQVd7Dj9LVDWFpZqPc516zKtr3rCDy2lX2/b+fTbu3zsipCFBjNWjbC7+gGdp/cysL/zcTUzCTXMYZGhsxYPIGdxzey6/gmZiyegKGRIQDValRm/e7VbD/yK37HNtChS9t8rZcQ7yPpZIr/JCwsjPHjx2s7DfGWLK0tmL1kCiO+GEvbhl0IC33EmEnDcx3Ts19nSpd1pH3THnRp9Rn9v+qFc82q3PvzPp1c+qhfJ4+dYfeO/RwMPKqNagrxXrKytmTesukM6T+aFvU78fDBI8ZOHvXWcZ17dGDrnrXYF7fVOO6HdQtZPHcl7Zv34PMeQ5kwYwylynyc5/US4n1maW3BjCUT+fqLcXRs3IPwB4/5ZuKwXMcM+ro/urq6dG7el84ufTE0MmTgyM8AWLRmNivm/4+uLT5jSK9v8Jo+ko9Lf5TvdRQFl0qlyJOXNkknU/wnjx8/JiwsTNtpiLfUpHkDrl6+wYOQrL/d5nXbce/aLtcxrdxc2LE5gMzMTOJi4wnceRD3bprH12lQgzYdWzB5zOx8qJEQBUdTl4YEX7pGaMhDADb87Eunrm5vFWdrb0MrN1f6dRuicYyBoQFL5/3IyeNnAXj6OJLoqBfYO9jlZZWEeO81al6f65du8vB+1nfa1l/8aN+lTa5jLp6+xI+L1qJSqVAqldy8+icOJe0xMDRg5cI1nPntPAART57x4nkMdg42+Vg7Id4/ck9mAXP27Fnmz5+PUqmkZMmS6Ovrc/v2bRQKBQMGDODTTz9FqVQya9YsTp8+jUKhwN3dnUGDBnH27FlWrVqFvr4+4eHhuLq6YmJiwuHDhwFYvXo1xYoVY/fu3axcuRKFQoGTkxMzZsxg1apVRERE8ODBAx49ekS3bt0YMmQIPj4+hIeHM23aNKZMmaLlT0fkVvESdjx5FKHefvo4kiLmZpiamZKYkPjGmGzvPYmgYpVyGucYO2UUi2b9oC5PCJGleAn7f7StCMzNi2BWxJSE+MRcxUU+fcaQft9mKzstNQ3fjTvV270+64KpmQmXLgTnUW2EKBjsHWx5+vjv9hSh/k4zITEh6Y0xp46fU+8vXtIez0E9mDZmDmmpafht2q1+r6tnJ0zNTAi+eD0faiUKC6W2E8gD0sksgEJDQzl69CgrV64kLS2NPXv2EB0dTbdu3ahUqRIXL17kyZMnBAQEkJaWhqenJxUqVMDY2JgrV64QGBiIhYUFjRo14rvvvsPPz49x48YRGBhI27ZtmT17Nn5+ftjb2+Pl5cXx48cBuH37Nhs3biQ+Pp6WLVvSp08fJk6cyPLly6WDWcDo6OigUmVfy0ypzMxVjEJHofGeAgWZyr//iaxZ1xkra0t279j/jjMXouDT0VGgymEtwcxM5b+Ke5XBo77g80G96d99KKkpqf8uWSEKiazvtOz7lS99d+UmpopzRZasncvmn7dz/NBJjbgBIzzp+2UPBvf8Wtqc+OBJJ7MAKl26NEWKFOHMmTPMmjULACsrK1q0aMG5c+e4cOECHh4e6OrqYmxsTMeOHTl9+jSurq5UqFCB4sWLA2BpaUnDhg0BcHBwIC4ujkuXLlGrVi3s7e0BmD9/PgA3b96kfv36GBgYYG1tjYWFBfHx8VqovXgXHoc/xblWNfW2XXEbYl7EkpyUkquYJ+FPsbP/eyqQrb0NTx9HqrfdPm3FLt/AHDupQnyIvvEeSsu2nwBgVsSM2zfvqN+zL277/20rWeOYx+FPqVHb6Y1x/2RgoM/85TMoX7EMndt+xqOwx++wJkIUTE/CI3CqVVW9bVvchth/fO+9Kabdpy2ZOMeLmeMXstfvoDpO30CfmUsnUbZCafq0/5LHYU/yoUaiMFEhq8uK94CRkRFAth/wKpWKzMxMjStuL+8H0NfX13hPV1dXY1tPTw+F4u//0aOjo4mOjgbA0NBQvV+hUEgHogA7cewMNWpXw7FM1sIEvfp34cj+47mOObL/N7r0dkdXV5ci5ma092jN4X3H1MfWa1SL07+fQwiRZdGcH2jfvAftm/egcxtPatZ2Vi/G0/vzbhx6qf385fejp3MVl+1cq2ZhVsSULu36SQdTiP936vhZqteupl6Qp0c/D4L2/57rmE9aN8F75rcM6jFKo4MJMGfFVMzMTOnbQTqY4t9RqvLmpU0yklmANWjQgO3btzNx4kSio6M5cuQIy5YtQ19fn127duHi4kJaWhq7d+9m8ODBuSrTycmJadOm8ezZM2xsbJg1axb169d/Zbyuri4ZGRnvqkoin0Q/f8G4UdNZtmYu+gb6PAwNZ+ywKVSrXpmZiyfSyaXPK2MANq3dzkelShBwbBP6Bvps+cWP86f+UJfvWPpjwh/KF60QOYl6Ho3XiMn8sHYB+gb/x95dx1WRtQEc/13SABVRQbC7sUVFBbswwFaMNdZV1DVQVETswO517cRCQsXGRuyOdddESmyw4N77/sG7d0VQcVe4gM93P/fzemeec+Y8+B6HM2fmjD4P7oUwfMBYAMpXLMP0eeNpYdvxi3GfU6lqBZq3bszdP++zfc8azfYZE+ZzLPBUSqYlRJr2LOo5bkMmMXflVPT19Xn0IITRzhMpa1WKCXPG0K5B98/GAIwYPwgFCibM+WdF/YtnrrBrx16atGrAvT8fsN5/uWbfnEmLOXUkONXzFCKtUKhlOipdCQ4OZtGiRaxfv57o6Gg8PDy4ffs2SqWSnj170qFDB2JjY5kxYwanT58mNjYWe3t7nJ2dE5QFqF+/PuvWrSNfvnwsXLgQgEGDBrF3716WLFmCSqWiYsWKTJgwgSVLlmj2f1w2a9asODk5Ubp0ac2ttV9TInfVFPjJCCG+JFYVq+0mCPFDyqqXWdtNEOKHdS3itLabkCyHzTqkSL31I7amSL3JIYNMkepkkClE6pNBphDaIYNMIbRHBpnaG2TKM5lCCCGEEEIIoSVqFCny+Rb+/v40b96cxo0bs3Hjxs/GHTlyhPr163+1PnkmUwghhBBCCCG0RNvvyYyIiGDu3Ll4e3tjYGBAp06dqFGjBsWKJXwHelRUFDNmzEhWnTKTKYQQQgghhBAZzKtXrwgJCUn0efXqVYK4U6dOYW1tTY4cOciSJQtNmjRh797E7zp3c3PD2dk5WceWmUyR6pJ6ubgQImXp6+h/PUgI8d29VX7QdhOEEGlcSr0nc+3atSxatCjRdmdnZ81ingCRkZHkzv3R+8/z5OHKlSsJyqxbt44yZcpgZWWVrGPLIFMIIYQQQgghMpgePXrQtm3bRNuzZcuW4LtKpUKh+Gegq1arE3z/448/2L9/P2vWrCE8PDxZx5ZBphBCCCGEEEJoSUo9k5ktW7ZEA8qkmJubc+7cOc33J0+ekCdPHs33vXv38uTJExwdHYmNjSUyMpIuXbqwadOmz9Ypz2QKIYQQQgghhJaoUuiTXLVq1SIoKIhnz57x9u1b9u/fT926dTX7Bw8ezL59+/D19WX58uXkyZPniwNMkEGmEEIIIYQQQvywzMzMGDp0KN27d6dNmza0bNmSChUq0LdvX65evfqv6lSo1WpZhUWkquK5q2i7CUL8cBQptKiAEOLLlGptv5xAiB/XX1EXtN2EZNlt1jlF6m0RsTlF6k0OmckU4gdh28gG/yNe7AvawYKVMzAyyprsGB0dHcZOHs7eUzs4eMaHzj0cNWVq1K6K94H1+AVuZlvAGipUKpugTgMDfdZsW0xT+wYpm6AQaZhto9r4HdnM3qAdzF85naxJ9r+kY3R0dBgzeRh7T23nwJmddPqo/9k1rsOZPw7hG7hR88maNQsAv47+hf3B3vgGbmT8jFEYGBqkTrJCaJltIxt2H93CgdPeLPzM+e5LcTo6OrhNHsH+oB0cPuNL557/9LlCRfKz2X8Fe09ux3v/OooUK6TZ13tANwJObGPXES/W7VhKgUL5ALDMn5fVWxax79QOdh5YT/PWjVIueSHSiB9+kBkcHIyTk9Nn9wcGBrJ69erP7o+IiKBv374p0bRUoVQq6d27N02aNCE4OPiby48ePZrHjx+nQMvE95TTNAfT54/H+ScXmtR05NH9EEaMG5TsmE49HClUtAAt6nTAoZETPX7uQoVKZdHX12P+79NwGzaZVnadWTJnJZ5LJmrqrFi1PFsD1lC5esXUTFeINMXENAfT5o9n0E8jaVrTkUf3HzNinHOyYzr1cKBw0YK0qNMRx0bd6flzZ83FnMrVK7BqyQZa23XVfGJi3uDQ2R67xjY4NupOa7uuPImIYujoX1I9dyFSW07THMxc4MHAXiNoZO3AowePcXEf9E1xnXs4UrhoAZrZdKBNo270+v85D2DOsilsWrOdprXbMX/GMhavnglArbrVad+1De2b9qSlbSf27TrMjIUeAHgunsjF81dpUsuRbm1/pt+gHpQqWzw1fhwinVApUuajTT/8IPNrrl27RnR09Gf3m5mZ8fvvv6dii76viIgIbt++zb59+6hRo8Y3lw8ODkbuuE77bGxrcvXSDR7cfQTApjXbadWuWbJjGje3Zcdmf5RKJa9evmb3zn20bt+c2Ng4bCo048bV2wDkL2TJi2cvNXX26NuJWZMWcuXitdRIU4g0ycbWOkHf2pxk//t8TKPmduzY7PdR/9tPq/bx+ypVq4C1TVX8Ajexyf93qtasBEA5q9Ic3HOU16/iz1/7dwfSRO4mED8AG7uaXLl0nfv/70sbV2+j9Sf97WtxjVvYsf2jPrdr537atG+OmXluihQvxC7vfQAcPXSKLFmzULZCKaIin+LuMpXo6BgArl66gWW+vACUq1CaHZv9AIiJfsPpE+do3KJ+yv4ghNAyeYXJ/927dw93d3devHhBlixZGDt2LFmyZMHLywsACwsLLCws8PT0BCB79uzMnj2bN2/e0L17dw4fPkzPnj15/vw5ED94s7Ky4rfffsPHx4e1a9eiUqkoW7Ys48ePj7/9acwY7ty5A0CXLl3o0KEDjx49YuTIkcTExFC1alUCAgIICgpi4cKFAJoXp9avX59169aRI0cOxowZQ0REBJGRkdSsWZMpU6Zw5swZPD09UalUFC9eHHd3dyZOnMidO3dQKpX07duXli1b8vPPP/PixQscHBzw9vZm+fLlBAQEoFQqsbGxwcXFBYVCkWQOa9euJTIykn79+rFx40ZMTExS+69NJJO5pRlhj/95r1F4aCTG2YwwMsqqOSF+Kcbc0pzwj/eFRVKqTPxV2Li4OExz58Tn0EZy5szBkL6jNXFDfx4LQP9fe6VofkKkZXktzQh7HKH5/nffymqUlZj/978vxSTaFxZByTLFAHjx7CX+3vvY53+IKjWsWLJuNq1su3D5/DV69u/ChpVbePH8FW06tCCPWa5UylgI7clrkVRfMk5wvvtaXOL+GEGpMsXJa2lOZPiTBBfXw0MjMLfIw6G9xzTbDAz0Gek+mAC/AwBcvnCNdl1aM3/GMnKa5qBew9qcD76UEumLdEqVAddNkEHm/7m4uNCvXz8aN27MpUuXGDJkCPv27aNTp04AODo64uTkhIeHBxUqVOD333/nxo0bFCpUSFPHmjVrAHj48CE//fQTo0eP5s6dO2zduhUvLy8MDQ2ZPXs2K1eupGrVqrx8+RIfHx8iIiKYPXs2HTp0YOLEibRu3ZpOnTqxc+dONm7c+MV2HzlyhNKlS7NgwQI+fPhAixYtuH79OgD3798nMDAQY2NjZs2aRdmyZZkxYwbR0dF06tQJKysrli5dSvfu3fH29ubYsWNcu3aN7du3o1AocHFxwc/PjzJlyiSZw4ABA/Dy8mL58uUywEzjdHQUJDXhrFQpkxXz6T4FCpSqfxazePrkGXUqNKNMhVKs27GUdk3ucv/uw++ZghDplo6OTpJ3fKgS9L/Pxyh0FAn2fdz/nHuN1Gw/H3yZi2evUNu2Bt6b/TG3yMNa72W8ffOWLet38iE29numJUSa9Lm+9PH57mtxn+5TKBQolYnPhf/s++d8mNM0B4tWe/L6VTSzJi8CwGWgO2MmDWPPsS08ehhK4P7jZM6c6b+kKTKYjHhPoAwygZiYGEJCQmjcuDEAFStWJHv27Ny9ezdBXIMGDXB2dqZhw4Y0aNCA2rVrExISkiAmOjqagQMHMm7cOAoVKsSGDRt48OABHTp0ACA2NpYyZcrQuXNn7t27R+/evalbty4jR8b/onD27Fnmzp0LQJs2bXB3d/9i21u2bMmVK1dYs2YNd+/e5cWLF7x58waAwoULY2xsDMCpU6d49+4dO3bsAODNmzfcuXOHEiVKaOoKCgriypUrODg4APDu3TssLCx4/fp1kjmI9CM0JByryuU0383y5ubF85e8ffMuWTGhIeHkMc+t2ZfHPBfhoREYGRtRs041DuwJBODGlVvcuv4HJcsUk0GmEP8XGhJOhWT0v8/FhIWEY5ag/+XWzHR2/ak9y+b9s26AQqEgLjaO7Dmy4b9jL7/NXwPE31b78F7C85UQGcWvrv1p0KQeAEbGWbl980/NPrO8eRL1N4DQx+FYVSmXZFxoUn0uLDL+XPjJHQHx/TF+1rNkmeIs3zCX/XsCmeY+F9X/LwZlymzIqMEemjZMmePGnVt/fcefgBBpjzyTCUleyVKr1SiVCa969ezZk/Xr11OgQAE8PT1ZunRpojIjRoygefPm1KsX/4+dUqmkWbNm+Pr64uvry7Zt23B3d8fExITdu3fTrVs37t27R9u2bXn16hWGhoaa9igUCvT09DR//ridsf+/Ir1+/XpmzpxJzpw56datG0WLFtXEZcr0z1UylUqFp6enph1bt26lTp06CdqvVCrp0aNHgrb279//szmI9OPEkdNUrFKegkXyA9C5ZzsO7T2a7JhDe4/SrksrdHV1Mc5mRMu2TTgYcASVSsm0+e5Urm4FQLGSRShSvBCXz8szmEL8Lb5vlfuobzl+pv8lHXNo7zEcP+p/Ldo25mDAEWKi39D1p/Y0bhn/bFfp8iWpUKksxw+folzF0ixeOws9PV10dXXpN7gnftsDUjFrIVLPvOnLsLfrjL1dZ9o17UGlKuUp9P++1KWnIwcDjiYqcyIw6LNxBwOO0K5r6wTnvAN7AgkPi+TBvUe0bBs/KVHHriYqlYrbN/7EPG8eNuxcxsJZvzPFbbZmgAkwZFR/uvZqD0ChogVo0KQu+3YdTtGfiUhfVCn00SYZZAJGRkbky5eP/fv3A3Dp0iWioqIoXrw4urq6xMXFAdC+fXtiYmLo2bMnPXv25MaNGwnqmTt3LgYGBvTv31+zrUaNGhw4cICnT5+iVqvx8PBg7dq1HDp0CBcXF2xtbXFzcyNLliyEhYVhY2PDzp07ATh48KBmVtLExIQ//4y/MnflyhWePHkCwMmTJ+nYsSOtWrXi/fv33Lp1K8E/bH+ztrZm8+b4d+VERkbSqlUrwsLCEsX4+voSExNDXFwcAwcO1CwIlFQOALq6uokG4yLteRb1HNchE1i4ciZ7T26nZOliTHOfSzmr0vgFbvpiDMCm1dt5eD8E/yOb8T6wnm0bfTlz6gJvYt4yoMdwxk4ejl/gJqbPH8+wn90ID4vUZrpCpCnPop4zeshEFq6cQcDJbZQoXYzp7vMoZ1Ua38CNX4yBf/qf35FN7Diwjm0bfTl76gIqlYpfnIbTe0A3dh3bwvQF7vzadzTPn73k5JFgjh06hf9RL3af2Mpff9xjzbJNWvwpCJE6nkY9Z9RgDxat8mTfqR2ULFOcqe5zAChfsTT+gZu/Grdx9XYe3gth11EvfA5uYNtGH86cin/f4q/9RtO5ZzsCjm9l+NiBDOo9CrVajfOIvmTJkpkefTvhH7gZ/8DN7NgX/7vS9PHzsG1ow55jW1jw+3RcnMcTFhqRROuFyDgU6h98adDg4GAWLVqEh4cHHh4evHjxAn19fdzc3KhcuTJnz55l1KhR9OrVi2LFijFt2jT09PTIkiULkydPRk9Pj+7du7N582bq1q1LyZIlNffyZ8uWjfXr17Nt2zbNojmlS5dm6tSp8e9gcnPj6tWrGBoa0rBhQwYOHMjTp08ZO3YsYWFhlClTBm9vb27fvs3z588ZMmQIUVFRlC1blr/++osFCxbw6NEjPDw8MDAwwMjIiEyZMtG8eXMKFCjAokWLWL9+PRB/G6+Hhwe3bt1CqVTSr18/2rZtS0hIiGbhIoAlS5awe/dulEolderUYcyYMSgUiiRzMDQ0ZMqUKRw7dowVK1aQP3/+ZP3Mi+eukmJ/n0KIpCky4KICQqQHSrW25xOE+HH9FXVB201Ilu15u6ZIve3Cvry2S0r64QeZaV3JkiW5ffu2tpvxXckgU4jUJ4NMIbRDBplCaE96GWRuS6FBZnstDjLldlkhhBBCCCGEEN+NrC6bxmW0WUwhhBBCCCHEPzLi/Q4ykymEEEIIIYQQ4ruRmUwhhBBCCCGE0BJVBlw2QQaZQgjxA9BRZMAzmBDpQKxKXvMlhPgyVQZcnE9ulxVCCCGEEEII8d3ITKYQQgghhBBCaElGfJ+kzGQKIYQQQgghhPhuZCZTCCGEEEIIIbQkIy78IzOZQgghhBBCCCG+GxlkCvGDsG1kg/8RL/YF7WDByhkYGWVNdoyOjg5jJw9n76kdHDzjQ+cejonKtuvSit82zE2wbdHqmRw844Nf4Cb8AjcxZtKwlElOiHSkXsPa+B7ZRMCp7cxbMY2sSfTFr8WYW5hx9PJucuTMrtlWo3YVtu9fi0/gRrz2rKJ8pTIpnosQ6Y1dozoEHNvGoWBfFq/yxMg4cf/7UpyOjg7jprhw8LQPgWf96dKzvaaMtU01/A97EXBsG5t8VlC6bIlUyUmkf6oU+miTDDKFxujRo2nQoAElS5bUdlPEd5bTNAfT54/H+ScXmtR05NH9EEaMG5TsmE49HClUtAAt6nTAoZETPX7uQoVKZQHIniMbEz1HM3bKCBSfvCajYtUKdLHvQyu7LrSy68LUcXNSJ2Eh0igT0xxMne/O4F6jaFarHY8ePGb4OOdvimndoTkbfH/DLG8ezTZ9fT3mLJ/KuGFTaGPXlWVzVzFz8cRUy0uI9CCnqQkzF07kl57DaVCjNQ8fPGak+5BviuvSsx2FixakSW1HWjfswk/9u2JVuRzGxkYsWzuHaR5zaFa3PW4uk1m0yhMDA/3UTlOkQ+oU+miTDDKFxs6dOwkICOD27dvabor4zmxsa3L10g0e3H0EwKY122nVrlmyYxo3t2XHZn+USiWvXr5m9859tG7fHIDmrRsRGR7FjPHzEtSXr4AFWY2yMGXuOHYd3cL0BePJniNbCmcqRNpW29Y6vp/di+9nXmt2YO/YNNkxecxy0aCZLX06Dk5QJjY2jnpWzbl57Q8A8he05MXzlymdjhDpSh27mly5eI37dx8CsGHVVlq3a/5NcU1a1Gf7Jl/N+dDfey9t2regUNECvH71mlPHzgBw9859Xr+OplI1q1TKToi0RQaZAoD+/fujVqupVasWVlbx/yC6uroyfvx4HBwcaNKkCT4+PgAEBQXh4OCAg4MDvXr14tmzZ1psuUgOc0szwh6Ha76Hh0ZinM0owS2zX4oxtzQn/ON9YZGY/38WZfPaHSya/TsfPsQmOKZprpycOnqGcSOm0squMzExb5g2f3xKpShEupDXwozw0AjN97/72ce3w34pJjIiisG9Rmp++f1YXJwS09w5OXp5Ny7jB7Ni0bqUTUaIdCavpTlhjz/uWxFky2ac6JbZL8XltTQnNDQ8wT5zCzPu/fWAzFkyU8e2JgAVKpWlRMmi5DHLlcJZiYxApUiZjzbJIFMAsGzZMgB8fHwwNTXVbH/06BFbtmxh7dq1zJw5kydPnrBkyRI8PDzw9vamVq1a3LhxQ1vNFsmko6NAncR9E0qVMlkxn+5ToECp+vLd/pcvXGNgzxGEh0agUqlYOHM5to1s0NeXRa3Fjyu+LyXuaKpEffHLMZ/z9Mkz6lm1oFPzn5g6351CRQr8twYLkYHo6ChQJ3EToVKpSnacjkInwX2ICoUClVJJ9OsYfnYayoChvdlzdCsOHe05dfwssbGxieoR4kcgv+2JL3JwcEBfXx9zc3MqV67M+fPnadCgAc7OzjRs2JAGDRpQu3ZtbTdTfEVoSDhWlctpvpvlzc2L5y95++ZdsmJCQ8LJY55bsy+Pea4EMy1JqWpdkWzZs3F43zEg/kSsVqkSncyF+JGEPo6gwtf6YjJiPmVknBXrOtU4uOcIADeu3ub29TuUKFM0yVlPIX4UQ10H0LBpPQCMjI24ffOOZp953jz/71tvE5QJDQmnYpXyScY9fhz2yfkwN2GhESgUCt7EvKFz6z6afYeD/TSPoAjxJRnxNyOZyRRfpKurq/mzSqVCT0+Pnj17sn79egoUKICnpydLly7VYgtFcpw4cpqKVcpTsEh+ADr3bMehvUeTHXNo71HadWmFrq4uxtmMaNm2CQcDjnzxmFmyZsF92kjNc5h9Bjqx1/8Qqq/MgAqRkZ08chqrquUoWDi+n3Xq4cjhvce+OeZTKpWKKfPGUal6BQCKlSxC4eKFuHz+egpkIUT6MXf6ElrYdqSFbUccmjhRqUoFzQx/l17tOZDEuex4YNBn4w4GHKFDlzb/Px8aY+/QlAN7AlGr1azaspjyFeNXdW7Rpgnv37/n5vU/UiVPkb5lxNVlZSZTfFFAQABNmzYlNDSUK1euMGXKFNq3b8+ECRPo2bMnOXLk4NChQ9pupviKZ1HPcR0ygYUrZ2JgoM/D+yG4DHSnnFVpps4bRyu7Lp+NAdi0ejsFCuXD/8hm9A308VrrzZlTF754zGOHTrHudy+27F6FQkfBHzf/ZOzQyamRrhBp1rOo54wZPJH5q6ajr6/Po/shjHL2oJxVaSbNdaNt/a6fjfmSNzFvce7hwphJw9HT1+PD+w+M6O9GRFhk6iQmRDrwNOoZLoPcWbJ6FvoG+jy4F8LwAWMBKF+xDNPnjaeFbccvxm1YtZUChfKx59g29PX12Lx2O8GnzgPwaz9Xps0dj76BPpERT+jn9Ku2UhVC6xTqpB78ED+kkiVLcujQIbp3787hw4dxdXXl2bNnREVF8eHDB4YNG0b9+vUJCgpi2rRp6OnpkSVLFiZPnkyhQoWSfZziuaukXBJCiCTpKuTGFSG04b3yg7abIMQP697Ty9puQrIsy98tRert/2hDitSbHDKTKTT+fnXJ4cOHNduaNm2Kg4NDgriaNWvi5+eXqm0TQgghhBBCpA8yyBRCCCGEEEIILdH285MpQQaZ4rOmT5+u7SYIIYQQQgiRoWXEQaY8pCOEEEIIIYQQ4ruRmUwhhBBCCCGE0JKMuAqrDDJFqlPJgsZCpDpdhbZbIIQQQogfhQwyhRBCCCGEEEJLVBnwQrA8kymEEEIIIYQQ4ruRmUwhhBBCCCGE0JKMuLqsDDKFEEIIIYQQQksy4iBTbpcVQgghhBBCCPHdyCBTiB+IbSMbdh/dwoHT3ixcOQMjo6zfFKejo4Pb5BHsD9rB4TO+dO7pmKhsuy6tWb5xXoJtvQd0I+DENnYd8WLdjqUUKJTvu+cmRHpRr2FtfI9sIuDUduatmEbWJPrh12LMLcw4enk3OXJmT7C9tm0Ndh7emKLtFyI9s2tUh4Bj2zgU7MviVZ4YGSd9HvxaXF4LM4KuHcAkZw7NtgqVyrJtzxp2H9lCwPHttGnfIiVTERmIOoU+2iSDzO9g69at7Nq16z/X8/r1awYOHPgdWgTBwcE4OTkB4OTkRHBwcKKYhQsXsnDhwm+u29vbG1dX1//cRpG6cprmYOYCDwb2GkEjawcePXiMi/ugb4rr3MORwkUL0MymA20adaPXz12oUKksANlzZGPSrDGMmzoCheKfZdJq1a1O+65taN+0Jy1tO7Fv12FmLPRIhYyFSHtMTHMwdb47g3uNolmtdjx68Jjh45y/KaZ1h+Zs8P0Ns7x5NNsMMxkyxLU/c5ZPRVdXN9XyESI9yWlqwsyFE/ml53Aa1GjNwwePGek+5JvjHDq2ZMuu1Zh/1AcBlqyZzbwZS2lh25FeHQcwdtIIChUpkOJ5CZEWySDzO7hw4QIfPnz4z/W8fPmSmzdvfocWCZGYjV1Nrly6zv27jwDYuHobrds1+6a4xi3s2L7ZD6VSyauXr9m1cz9t2jcHoHmbRkSEP2H6+HkJ6ouKfIq7y1Sio2MAuHrpBpb58qZUmkKkabVtrbl66QYP7sX3L681O7B3bJrsmDxmuWjQzJY+HQcnKGNjZ03mLJlxHTQhFbIQIn2qY1eTKxevcf/uQwA2rNpK63bNvykuj3luGjWvT4/2vyQoY2BowIKZv3HyaPxF/fDQSJ49fY65hVlKpiQyCJUiZT7apJWFf4KDg/H09ESlUpE9e3Z0dHR4/fo1kZGRtG3bloEDB1KnTh0OHDiAkZERnTp1on79+vTr149du3Zx/vx5XF1dmTBhAufPn0dfX58BAwbQvHlzrly5wrRp03j37h0mJiZMmDCB/Pnz4+TkRPny5Tl//jzPnj3Dzc2NevXq4e/vz4oVK9DV1SVfvnx4enpy6dIlFi1axPr16wFwdXWlevXqNG7cmGHDhhEVFQXAwIEDyZw5M4cPH+b06dPkzp2b3bt38+LFCx48eICLiwvv379n9erVvHv3jg8fPjB16lQqV67MzZs3cXd35927d2TPnp1Zs2YxefJkIiMjGThwIIsXL2bu3LkEBQXx8uVL8uTJw9y5c8mVKxc2NjY0adKE8+fPo6ury7x588ifPz8nTpxg2rRpGBoaUrhw4QQ/861btzJt2jQARo8eTY0aNRLsDwwMZN68eahUKvLnz8/EiRPJlSsXp06dYvr06ajVaiwsLJg9e3aCclOmTOHp06d4enrK1fM0Lq+FGWGPIzTfw0MjMc5mjJFRVs0A8GtxeS0/3RdBqTLFAdi8ZgcAjp3sExz3j1t/af5sYKDPSPfBBPgd+L7JCZFO5LUwIzz00/5lRFajrMT8vx9+KSYyIorBvUYmqvdQwFEOBRyleq3KKZ+EEOlUXkvzROewbNmMMTLOSvTrmGTFRYY/4ZcewxLV/eH9B7Zu3Kn53rm7I1mNsnDx3JUUykZkJLLwz3d0//591q5di42NDS1btmTr1q34+/uzdu1aXr16hbW1NWfPniUmJobQ0FDOnj0LwPHjx7G1tWX9+vW8efOGgIAAVq9ezeLFi/nw4QNubm7Mnj2bnTt30qtXL8aNG6c5ZmxsLFu2bGH06NHMnz8fgHnz5rFq1Sq8vb2xtLTk7t27n23zgQMHsLS0xNvbmylTpnDu3Dlq1apF/fr1GTx4MHXq1AEgR44cBAQEYGtri5eXF8uWLcPPz48+ffqwfPlyAEaMGMGAAQPw9/enefPmrF27Fjc3N/LkycPixYt58OABd+/excvLi3379pE3b178/PwAePLkCTVr1sTHx4dq1aqxceNGPnz4gKurKwsWLMDb25tMmTIlaHuWLFnw8fFh+vTpuLi4JJh5ffr0Ke7u7ixevBh/f38qV67MxIkT+fDhAyNGjGDGjBn4+/tTokQJdu785x/QhQsXEhERwcyZM2WAmQ7o6OigVie+Q1+pUiY77tN9CoUCpVKZKDYpOU1zsGb7EmJi3jBr8qJvbL0QGYOOjiLJ/qX6qB8mJ0YI8e10dBSok3hSTalU/au4z+k/5Cd+HfULfbsM5v279/+usUKkc1p7hUnhwoUxNjamd+/enD59mpUrV3Lnzh1iY2N5+/Yt9erVIygoCB0dHezt7dmzZw+xsbGcO3eOiRMnsmnTJjp06ICOjo5mBvGPP/7g0aNH/PLLP7cwREdHa/789yCwePHivHjxAgA7Ozs6d+5Mw4YNadKkCaVLl07y+UWASpUqMWfOHCIiIrC1tf3s85MVKlQA4n9ZX7x4MYcPH+bevXucOXMGHR0dnj17xpMnT7CzswOgS5cuAISEhGjqKFiwIKNGjWLbtm3cu3ePS5cuUaDAP/f1f5zLuXPnuH37Nnny5KFo0aIAtG3bVjOQBmjXrh0ApUqVwtTUNMFg+sqVK1SoUIF8+eIXY+nYsSPLly/n9u3bmJmZUbp0aQCGDx8OxD+TeezYMZ49e8b27dvR05M34aRVv7r2p0GTegAYGWfl9s0/NfvM8ubhxfOXvH3zLkGZ0MfhWFUpl2RcaEg4Zua5NfvymOcmPCzyq+0oWaY4yzfMZf+eQKa5z0WlyojX7IT4utDHEVSo/HH/yp2oHyYnRgiRPENdB9Cw6d/nQSNu37yj2WeuOb+9TVAmNCScilXKfzXuUwYG+ngumkTxkkVwaNqdx49Cv2MmIiPT9iI9KUFrM5l/z7RNnz6d9evXY2FhwS+//IKJiQlqtZq6desSHBzM6dOnqVGjBqVKlWL79u2UKFECQ0ND9PT0Eiwu8uDBA1QqFfny5cPX1xdfX1+8vb3ZtGmTJsbQ0BAgQTk3NzcWLFhA9uzZcXFxwdfXF4Ui4VXk2NhYAAoVKkRAQAD29vacO3eOdu3aJfnL8t+5xcTE0K5dO0JCQqhWrZpmIR59ff0EbXj//j2PHj1KUMe1a9fo3bs3KpWKJk2a0LBhwwRt+jgXtVqdqM2fzix+/F2lUiUYGH6ag1qtJi4uLlE7X79+TXh4OACWlpZMmjSJiRMnyoAhDZs3fRn2dp2xt+tMu6Y9qFSlPIWK5AegS09HDgYcTVTmRGDQZ+MOBhyhXdfW6OrqYpzNiJZtm3BgT+AX22CeNw8bdi5j4azfmeI2W/7/In5oJ4+cxqpqOQoWju9fnXo4cnjvsW+OEUIkz9zpS2hh25EWth1xaOJEpSoVNIvxdOnVngMBRxKVOR4YlKy4RMdaNhUj46w4NushA0zxw9P6wj8nT56kd+/eNGvWjHv37hEREYFKpSJnzpxkypSJwMBAqlSpgrW1NUuWLNHM/lWrVo09e/agVqt5+vQp3bp1w9LSkpcvX3Lu3DkAduzYwYgRIz577Li4OBo3boyJiQk///wzrVu35ubNm5iYmPDo0SPev3/PixcvOH/+PAAbNmxg4cKFNGvWjPHjx/Ps2TOio6PR1dVN8pbB+/fvo1Ao6N+/PzVq1ODAgQMolUqMjY0xMzPjxIkTAPj6+jJ//nz09PSIi4sD4OzZs1SvXp3OnTtTqFAhjhw58sXbEkuWLElUVBS3bt0CYPfu3Qn2+/v7A3D16lViYmIoWLCgZp+VlRWXL1/WzKRu2bKFGjVqULhwYZ4+fcqff8bPfq1YsYLNmzcDULRoUdq3b0/mzJnZuFGWy08PnkY9Z9RgDxat8mTfqR2ULFOcqe5zAChfsTT+gZu/Grdx9XYe3gth11EvfA5uYNtGH86cuvDF4zqP6EuWLJnp0bcT/oGb8Q/czI59a1M2WSHSqGdRzxkzeCLzV01n94mtlChdlBnj51HOqrTm1SOfixFC/DdPo57hMsidJatncSBoJyVLF2fKuFkAlK9Yht1Htnw17nMqVa1A89aNKVSkANv//xqT3Ue2UNeuVornJdI/FeoU+WiT1u9z/Pnnnxk5ciSZMmXC3NyccuXKERISQoECBahbty5Hjx4la9asWFtbM3XqVOrVi7/loUuXLkyePJlWrVoBMG7cOIyNjZk/fz5Tpkzh/fv3GBkZMWPGjM8eW09Pj8GDB/PTTz9haGiIqakp06dPx9TUlHr16tGiRQssLS2pUqUKAG3atGHYsGHY29ujq6uLi4sL2bJlo1atWsyZMwdjY+ME9ZcqVYrSpUvTrFkzFAoFNjY2mgGrp6cnHh4eeHp6YmJiwsyZMzExMcHCwgInJydmzZqFs7Mz9vbxi6j8/XP5HH19febMmYOLiwt6enqUKVMmwf43b97Qpk0bdHR0mD17Nvr6+pp9uXLlYuLEiTg7OxMbG4uFhQVTpkzB0NAQT09PRo4cSWxsLAUKFGDmzJns27dPU9bDw4POnTvTqFEjzM3Nv/r3LbTryMGTHDl4MtH2q5duYm/X+atxSqWSyW5fPtHu8PJnh5e/5rvb8Cm4DZ/yH1otRMZy7NApjh06lWDbyxevaFu/6xdjPlUqT7VE286cukCrep2+T0OFyICOHDzBkYMnEm2/eukGLWw7fjXuY4VNrTR/vnjuSoLvQnyLjHiPl0Kd1OoCQqSgorlk9UMhUpu+jizOJYQ2vFf+91ecCSH+nXtPL2u7CckyqWDXrwf9C+MeaO9uQ63PZAohhBBCCCHEjyojzvhp/ZlMIYQQQgghhBAZh8xkCiGEEEIIIYSWZMRnMmWQKVKdSi0vFBcitcVmxDOYEOmAjkKXOHWctpshhBCpSgaZQgghhBApRAaYQoivUSm+HpPeyCBTCCGEEEIIIbRE2++0TAmy8I8QQgghhBBCiO9GZjKFEEIIIYQQQksy3jymzGQKIYQQQgghhPiOZCZTCCGEEEIIIbQkIy4ALzOZQvxA7BrVIeDYNg4F+7J4lSdGxlm/KU5HR4dxU1w4eNqHwLP+dOnZXlPG2qYa/oe9CDi2jU0+KyhdtoRm3/AxAzl8xo/dR7Yw0XMMBoYGKZuoEGmQbSMbdh/dwoHT3ixcOQMjo6T73+fidHR0cJs8gv1BOzh8xpfOPR0TlW3XpTXLN87TfP95cE/8AzdrPiev7uXSvWMpkp8Q6UH9RnXYe2w7h4P9WLJq1mfPg1+Ly2thRvC1A5jkzKHZ1qBJPS7/eZw9R7ZqPlmNsqRkOiKDUKFOkY82ySDzBzJ69GgeP36s7WYILclpasLMhRP5pedwGtRozcMHjxnpPuSb4rr0bEfhogVpUtuR1g278FP/rlhVLoexsRHL1s5hmsccmtVtj5vLZBat8sTAQJ92XVpTv3FdWjfsSgvbjjwJj2LEGOfUTl8IrcppmoOZCzwY2GsEjawdePTgMS7ug74prnMPRwoXLUAzmw60adSNXj93oUKlsgBkz5GNSbPGMG7qCBSKf9bC/23BGuztOmNv15kurfvy9s1bhvRxTY2UhUhzcpqa4LlwEv17DqN+jVY8fBCCq/uv3xzn0NGebbvWYJ7XLEG5KtWtWL54Lc1tO2g+MdFvUjgrIdImGWT+QIKDg1GrM+KjxSI56tjV5MrFa9y/+xCADau20rpd82+Ka9KiPts3+aJUKnn18jX+3ntp074FhYoW4PWr15w6dgaAu3fu8/p1NJWqWVHeqgz79wTy+tVrAPbuOkSzVg1TI2Uh0gwbu5pcuXSd+3cfAbBx9TZat2v2TXGNW9ixfbOfpv/t2rmfNu3j+2bzNo2ICH/C9PHzPtuG0ROHcvTgSY4eOvWdsxMifaibzPPgl+LymOemSXM7nNr3T1SuSrWK1KpTnYCj29i2aw3Va1ZJwWxERqJOoY82yTOZn4iLi8PDw4M7d+4QFRVFyZIlmTFjBsOHDycqKgqAgQMH0qBBA1avXs3OnTvR0dGhQoUKTJw4EaVSycyZMzlz5gxKpRIHBwd69uxJeHg4I0aM4M2bN/G3PLm5UbFiRWbMmMHJkyfR0dGhYcOGODs7s3DhQkJDQ7l//z7Pnj3jl19+ISgoiMuXL1OqVCnmzp2LQqFg+fLlBAQEoFQqsbGxwcXFhcePH+Ps7Ezx4sW5efMmpqamzJ8/n61btxIZGUm/fv0YMmQIq1evxsvLCwBvb28uX76Mu7t7km1P6mcyZ84coqKi6NOnDyYmJmTKlInVq1dr869OfEVeS3PCHkdovoeHRpAtmzFGxlmJfh2TrLi8luaEhoYn2FeqbAnu/fWAzFkyU8e2JsePBFGhUllKlCxKHrNcXDp/lZ/6d2PdCi9ePH+JQyd7cpvlTp2khUgj8lqYfdKvIjHOZoyRUVaio2OSFZfX8tN9EZQqUxyAzWt2AODYyT7J4xcrUZhGzWyxq9r6u+YlRHqS19Kc0Mf/nMPCvnAe/FxcZPgTfu4xLMn6Xzx/ge/2APb4H6BqjUqs2DCfpvXaEx4akWS8EBmZDDI/cfHiRfT19dmyZQsqlYoePXpw8OBBLC0tWb58OTdv3sTPzw9bW1t+++03jh8/jq6uLmPHjiUiIoLDhw8DsHPnTj58+EDv3r0pV64cp0+fxtbWlj59+nDs2DHOnz9P7ty5OXbsGLt37+bt27eMHj2a9+/fA/DHH3+wZcsWLly4QI8ePfD396dQoUI0b96c27dvExkZybVr19i+fTsKhQIXFxf8/PyoUqUKt27dYurUqZQpU4ZBgwbh7+9Pv3798PLyYvny5VhaWjJz5kwePnxIgQIF8PHxYfjw4WzdujXJtqvV6kQ/k6NHj1K2bFnu3bvHihUryJcvn9b+zkTy6OgoUCdxXUupVCU7Tkehk+DSmEKhQKVUEv06hp+dhjJirDOjJwzlTNAFTh0/S2xsLP7eezG3yMMmn9958+Ytm9ftIDY29rvnJ0RapqOjk+SdJEqVMtlxn+5TKBQolcpEsUnp1b8r61duJfp19De2XIiMQ0dHJ8nZncTnweTFferjwee54IucP3OZOrbWbNvk+y9aK34kGXHhHxlkfqJatWrkyJGDjRs3cvfuXe7fv8/jx485ePAgERER2NraMnDgQHR1dalUqRLt2rWjQYMG9OrVCzMzM4KCgrh58yanT58G4M2bN9y+fZuaNWsyaNAgbt68Sb169ejWrRu6uroYGhrSqVMn7OzsGDFiBIaGhgDUrl0bPT09LCwsyJ07N8WKFQPAzMyMly9fEhQUxJUrV3BwcADg3bt3WFhYUKVKFUxNTSlTpgwAxYsX5+XLlwlyVCgUtG3bFj8/PxwcHHj69ClWVlasXLkyybZ37do10c/kzZv4ZwxMTU1lgJmGDXUdQMOm9QAwMjbi9s07mn3mefPw4vlL3r55m6BMaEg4FauUTzLu8eMw8pj/MwuZxzw3YaERKBQK3sS8oXPrPpp9h4P9eHD3EdlzZMNvewBL560CoHI1Kx78/1ZAITKyX13706DJ3/0vK7dv/qnZZ6bpV+8SlAl9HI5VlXJJxoWGhGP2Sf8LD4v8ajt0dHRo0rI+rRt0/a8pCZHuDHMdQMOmtgAYGxtxK1nnwbDPngc/J1s2Y5x6d2Tx3BWabQoFxMXGfadMREam7UV6UoI8k/mJQ4cOMWLECDJlyoSDgwPVqlXDwsKCgIAA7O3tOXfuHO3atUOlUrFkyRI8PDxQq9X06dNHc5upi4sLvr6++Pr6smXLFtq1a0eVKlXYvXs3NjY27Nmzh/79+6Onp8e2bdsYMmQIL168oFOnTty7dw8AfX19TZv09BJfC1AqlfTo0UNznG3bttG/f/zzAX8PVCF+QJnUVfG2bduye/dudu3aRevWrTV1JtX2pH4mf9eZKVOm7/fDF9/d3OlLaGHbkRa2HXFo4kSlKhUoVKQAAF16tedAwJFEZY4HBn027mDAETp0aYOuri7G2Yyxd2jKgT2BqNVqVm1ZTPmK8Rc3WrRpwvv377l5/Q8qVCzLsnVz0dPTQ1dXl1+G/ITv9t2pkr8Q2jRv+jLNojvtmvagUpXyFCqSH4AuPR05GHA0UZkTgUGfjTsYcIR2XVv/v/8Z0bJtEw7sCfxqO0qWKcarF695/CjsO2YnRPowZ/oSzSI8bZp0S3B+69qrPfsDEvehY5+cBz8X97Ho6Bi69+5IM/v4NQfKli+FVeXyHDl08jtnJET6IDOZnwgKCqJZs2Y4Ojry6NEjgoODsbKyYuHChYwePZq6detiZ2fHixcv6Nq1K9u3b6dSpUqEh4dz+/ZtrK2t2bp1K3Z2dnz48IEuXbowYcIEjh49ipmZGT169KBGjRq0bduWGzduMGnSJNavX0/NmjW5ceOGZpD5NdbW1ixYsIAOHTpgaGjIwIEDadu2LdWrV/9sGV1dXc2tVZaWlpibm+Pl5cXmzZs1dSbV9qR+JjVr1vzvP2yRqp5GPcNlkDtLVs9C30CfB/dCGD5gLADlK5Zh+rzxtLDt+MW4Dau2UqBQPvYc24a+vh6b124n+NR5AH7t58q0uePRN9AnMuIJ/Zx+BeD4kSBq1K5CwPFt6OjosH9PICuXbtDKz0AIbXka9ZxRgz1YtMoTfQN9Ht4PYcSAcQCUr1iaqXPdsbfr/MW4jau3U6BQfnYd9cLAQJ/Na3dw5tSFrx67UJEChDwKTdH8hEgP4s9v41i6ejYGBvo8uPeIoR+dB2fM86C5bYcvxn2OSqWiT7chTJw+mqGjBhAXF4dzHxeeP3uRCpmJ9C7jzWOCQi3LjSZw+/ZtRowYAcTPJlpaWmJhYcG9e/cICwtDV1eXrl270r59e9asWcOWLVvInDkzhQsXZtKkSejr6zNjxgxOnz5NXFwcDg4O9OvXj7CwMIYPH05MTAy6uroMHjwYW1tbZsyYQWBgIJkzZ6Zy5cqMHj2apUuXAjBo0CBCQkLo3r275llPJycnnJ2dqVGjBkuWLGH37t0olUrq1KnDmDFjePz4cYL4hQsXauqaMmUKx44dY8WKFeTPn59t27axf/9+fv/9dwBiY2OTbHtSP5MiRYrQvn37BMdKrsKmVv/9L0oI8U10FLraboIQP6Q4tdwuKYS2PHh6RdtNSJahhTqlSL1z73ulSL3JIYPMH1RcXBwjR46kadOmNG7cOFWPLYNMIVKfDDKF0A4ZZAqhPellkDkkhQaZ87U4yJRnMn9AarWaOnXqoFAoaNhQ3lcohBBCCCGEtqhT6L9v4e/vT/PmzWncuDEbN25MtP/gwYO0bt2aVq1aMWDAgEQLi35Knsn8ASkUCoKCgrTdDCGEEEIIIYSWRUREMHfuXLy9vTEwMKBTp07UqFFD83aL6OhoPDw82LFjB2ZmZsyfP5+FCxfi5ub22TplJlMIIYQQQgghtESVQp9Xr14REhKS6PPq1asExz916hTW1tbkyJGDLFmy0KRJE/bu3avZHxsby/jx4zEzMwOgZMmShIV9ecVymckUqS4jvgtIiLTug/K9tpsgxA8pk67h14OEECIFrF27lkWLFiXa7uzszKBBgzTfIyMjyZ37o/cw58nDlSv/PM9qYmJCo0aNAHj37h3Lly/Hycnpi8eWQaYQQgghhBBCaElKTcD06NGDtm3bJtqeLVu2hMdXqVAoFJrvarU6wfe/vX79moEDB1KqVKkk6/2YDDKFEEIIIYQQIoPJli1bogFlUszNzTl37pzm+5MnT8iTJ0+CmMjISHr37o21tTVjxoz5ap3yTKYQQgghhBBCaIk6hT7JVatWLYKCgnj27Blv375l//791K1bV7NfqVTSv39/mjVrxtixY5Oc5fyUzGQKIYQQQgghhJZoe70SMzMzhg4dSvfu3YmNjaVdu3ZUqFCBvn37MnjwYMLDw7lx4wZKpZJ9+/YBUK5cOaZMmfLZOhVqtVpWYRGpqqBpBW03QYgfTpxKqe0mCPFDkoV/hNCev6IuaLsJyfJzofYpUu9v97elSL3JIbfLCvEDq9+oDnuPbedwsB9LVs3CyDjrv4rLa2FG8LUDmOTMAUDxkkXYc2Sr5rPv+A4ePL1C05YNUjolIdKFBo3rcuCEN8fO7OK31XM+2/c+F5cpkyGzF07i0CkfDp/yZfbCSWTKFD+YKV6yKDsD1rP/2A72Hd1Ovfq1Uy0vIdI620Y27D66hQOnvVm4cgZGRkn3vc/F6ejo4DZ5BPuDdnD4jC+dezpqyljbVMXn4AZ2HfFi+961VKhUNlVyEulfSr3CRJtkkJmGBQcHf3V54G9x6NAh5s+fD8CCBQsSPOCbHCVLlvxubRHal9PUBM+Fk+jfcxj1a7Ti4YMQXN1//eY4h472bNu1BvO8Zpptd27fpbltB83neOApfLfvYe+uQ6mQmRBpW05TE+Ysmky/7r9St3pLHjwIYcz4Yd8UN3j4z+jp6dKwdlsa2rQlU+ZMOA/tC8DUWW54bfCmcV1Hhjm7sWz1bHR1dVM1RyHSopymOZi5wIOBvUbQyNqBRw8e4+I+6JviOvdwpHDRAjSz6UCbRt3o9XMXKlQqi76+Hgt+n86YoZNoaduJxXNWMHvppNRNUIg0RAaZP5AGDRowZMgQAM6ePYtSKbfP/cjq2tXkysVr3L/7EIANq7bSul3zb4rLY56bJs3tcGrf/7PHqWZdmWatGjFmhJxshQCoV78Wly9e497/+9S6lV60bd/im+JOnzrH/Fm/oVarUalUXLtyk3z5LQDQ1dUlR4741QSNjLPy/p28I1UIABu7mly5dJ37dx8BsHH1Nlq3a/ZNcY1b2LF9sx9KpZJXL1+za+d+2rRvTmxsHLXKN+XG1dsAFChoyYtnL1MpM5HeqVPoP22ShX/SgXv37uHu7s6LFy/IkiULY8eOpUKFCri6umJkZMT169eJiIhg4MCBODo68vr1a0aOHMnDhw/Jnz8/4eHhLFq0iDNnznDmzBmsra25du0abm5uLFq0iMmTJ+Ps7EyNGjUICQmhe/fuHD58mJCQEFxcXHjz5g1WVlaa9sTExDBx4kTu3LmDUqmkb9++tGzZUos/IfFv5LU0J/RxuOZ7WGgE2bIZY2SclejXMcmKiwx/ws89Es/AfGzMhGHMmrIwQZ1C/MgsLPMmq+99Ke5Y4CnNdsv8eenT34lRQz0AGOsyma2+q+j7S3dMc5syoPcIuagoBPGPdoQ9jtB8Dw+NxDibMUZGWYmOjklWXF7LT/dFUKpMcQDi4uIwzZ0Tv8ObMMmZgyF9XFMhK5ERaPvW1pQgM5npgIuLC05OTvj7+zN69GiGDBnChw8fAAgPD2fTpk0sXbqUmTNnArB48WIKFy7M7t27GThwIH/88UeC+tq0aUO5cuWYPHnyF2+BnTRpEg4ODvj6+lK5cmXN9qVLl1K2bFm8vb3ZuHEjy5Yt49GjRymQuUhJOjo6SV7jUipV/youKVWqWWFqaoLP9j3/rpFCZEA6OgqSWnMvcd/7elx5qzLs3LOeNSs2cXDfUQwNDVi6chZDB46larkGOLbozoy547GwNP/+iQiRzujo6CTdpz5ZGO1LcZ/uUygUCS7iPH3yjNrlm9K+WU9mLPSgUNEC3zEDIdIPmclM42JiYggJCaFx48YAVKxYkezZs3P37l0AateujUKhoESJErx48QKAkydPMmvWLADKly9PiRIl/tWxz5w5w+zZswFo1aoVbm5uAJw6dYp3796xY8cOAN68ecOdO3fInz//v85TpI5hrgNo2NQWAGNjI27dvKPZZ543Dy+ev+Ttm7cJyoSGhFGxSvmvxiWlZdum7Njin+TJWogfyYjRzjRuZgfE38J668ZHfc8iD8+T6FOPQ8KoVKXCZ+NaOTRj6qxxuI2cgs/23QCULF2czFkyc3DfUQAunLvC7Vt/UqlKhQSzokL8KH517U+DJvWA+L53++afmn1mmvPZuwRlQh+HY1WlXJJxoSHhmJnn1uzLY56b8LBIjIyNqFWnGvv3BAJw/cotbl7/g5Kli3H/r4cpmaLIALR9a2tKkJnMNC6pX87VarXmqpmhYfxqgh+/FFVXV/ebf6n/Oz4uLi7J7QqFAh2d+P+7qFQqPD098fX1xdfXl61bt1KnTp1vOp7QjjnTl2gW42nTpBuVqlSgUJH4q6xde7Vnf0BgojLHAoOSFZeUGrWqcPJY8PdLQIh0ata0RTSu60jjuo7YN+pC5aoVKPz/PuXUqyP79xxOVObo4VOfjWvU1JZJ00fTxaGvZoAJcP/uQ4yzGVG1ekUAChbKT4mSRbl25WYKZyhE2jRv+jLs7Tpjb9eZdk17UKlKeQoVib8o3qWnIwcDjiYqcyIw6LNxBwOO0K5ra3R1dTHOZkTLtk04sCcQlUrJ9AXjqVI9/vGi4iWLULRYIS6fv5ZKmQqRtshMZhpnZGREvnz52L9/P40bN+bSpUtERUVRvHjxz5apWbMm/v7+lCpVitu3b3Pnzp0Eg1CIH4j+PVA1MTHhzz//xNramoMHD2piatWqhZ+fH127dmX//v28fx+/eIS1tTWbN29m8uTJREZG0qZNG7y8vChQQG4JSU+eRj3DZdA4lq6ejYGBPg/uPWLogLEAlK9YhhnzPGhu2+GLcV9TuEhBQh6FpmQaQqQ7T6OeMczZjeVr56Gvr8eD+48Y0n8MABUqlmXWgok0ruv4xbhxE0egUCiYtWCipt6zwRcZ6zKZPt2GMGHaaAwzGaCMUzLyVw8e3JdHGoR4GvWcUYM9WLTKE30DfR7eD2HEgHEAlK9Ymqlz3bG36/zFuI2rt1OgUH52HfXCwECfzWt3cOZU/LsY+3cfhtuUEejp6fHhwweG9h9LeFik1vIV6UdGfCZToZb72NKs4OBgFi1ahIeHBx4eHrx48QJ9fX3c3NyoXLkyrq6uVK9eHQcHByD+FSO3b98mOjqa0aNHc//+fQoUKMC1a9fw9vbm6NGjnDlzhunTp7Ny5Uq8vLyYMWMGenp6uLq6YmhoSIMGDfD29ubw4cNERETg4uLCixcvKFeuHHv37uXChQtER0fj4eHBrVu3UCqV9OvXj7Zt2yY7r4KmFb4eJIT4ruJUsvCLENqQSddQ200Q4of1V9QFbTchWZwKOqRIvesfeKdIvckhg8wMyNfXl3z58lGlShVCQ0Pp1q0bBw8e1Nzuqm0yyBQi9ckgUwjtkEGmENojg0ztDTLldtkMqEiRIowfPx6VSoWOjg4TJ05MMwNMIYQQQgghxD8y4oyfDDIzoPLly+Ptrb0rF0IIIYQQQogflwwyhRBCCCGEEEJLVBlwLlPuoRRCCCGEEEII8d3ITKZIdUp1RlyoWYi0TRb+EUJLdLXdACFEWqfOgDOZMsgUQgghhBBCCC3JiNMvcrusEEIIIYQQQojvRmYyhRBCCCGEEEJLZOEfIYQQQgghhBDiC2QmUwghhBBCCCG0JCMu/CMzmUL8wOo3qsuB494cDfZn2erZGBln/aa4TJkMmbVwEgdP7uTQKR9mLZxEpkyGANSyqcbuQ1vYf2wHfvs3UrFyuVTLS4j0pGHjegSe9OXkuQB+Xzvvs/0wOXGrNixgque4lG6yEOmKbSMbdh/dwoHT3ixcOQMjo6T72OfidHR0cJs8gv1BOzh8xpfOPR0TlW3XpTXLN85LtN1z0QT6DHT6rvmIjEeVQh9tSveDzODgYJyckt95+/btS0REBN7e3ri6un6xvrFjx3L16tXv1tZ/w9XVFW9vbyIiIujbty8AgYGBrF69+ovlRo8ezePHj79bOz4+/qdKliz53Y4jUk9OUxPmLJpEvx6/Uq+GPQ/vhzDafeg3xQ0a1g89XV0a2TjQyMaBTJkMcR7aB319PZasnMXIX8fTuK4j82ctZ/7SaamdohBpnqmpCfOXTOUnp8HUrtqMB/cf4eYx/F/FDRzSmxo1q6ZW04VIF3Ka5mDmAg8G9hpBI2sHHj14jIv7oG+K69zDkcJFC9DMpgNtGnWj189dqFCpLADZc2Rj0qwxjJs6AoVCoamvaPHCbNj5G03tG6ZGmkKkOel+kPmtfv/9d8zMzJIVO2XKFMqXL5/CLUoeMzMzfv/9dwCuXbtGdHT0F+ODg4NRq7/f1PvHxxcZQz27Wly+eJ17dx8CsG7VFtq2b/FNccFB55k/+zfUajUqlYrrV25imc+C2Ng4qpZtwPWrtwAoWCgfz5+/TKXMhEg/bOvX5uKFq9y7+wCAtSu9cGxv/81xtWyqU79hHdat8kqdhguRTtjY1eTKpevcv/sIgI2rt9G6XbNvimvcwo7tm/1QKpW8evmaXTv306Z9cwCat2lERPgTpo+fl6C+br07sGXDTgL8DqRgdiKjUKvVKfLRpgzxTObz58/p3bs3kZGRVKhQgfHjx1O+fHlu374NgLe3N2fOnGH69OnUr1+fdevWJSh/4sQJpk2bhqGhIYULF9Zsd3JywtnZGYDffvuNTJky8ddff1GyZElmzZqFgYEB69atY8OGDRgbG1OkSBEKFCjAoEGD8PHxYenSpRgZGVGxYkViYmISHD9fvnwEBwezaNEi1q9fz5kzZ5g7dy7v3r3j1atXjB49moYN/7n6FRISQvfu3Vm+fDleXvG/RJibm7N06VJWrlxJ4cKFefPmDc2aNaNr165ERkbSr18/hgwZwurVqzVlvL29uXz5MlZWVhw5coSnT5/y5MkT7OzscHV1RaFQsHz5cgICAlAqldjY2ODi4sLjx4/p3r07hw8fJiQkBBcXF968eYOVlVWK/t2KlGNhaU7o43DN97DQCLJlM8bIOCvRr2OSFXcs8JRmu2W+vPTu78SooRMAiIuLI1duU/YGbsXE1IQBvUekQlZCpC8W+fIm6F+hj8PJlj2JfviFuKxZszJ5+hg6Ofale6+Oqdp+IdK6vBZmhD2O0HwPD43EOJsxRkZZiY6OSVZcXstP90VQqkxxADav2QGAY6eEF4cmuM4AoI6t9fdPSoh0IEPMZIaEhDBu3Dj8/PyIiYlh8+bNyS774cMHXF1dWbBgAd7e3mTKlCnJuIsXL+Lu7k5AQAChoaGcOHGCW7dusXHjRry9vdm0aRMPHsRfYQ4PD2fmzJls2LCBLVu2cO/eva+2Y8OGDUyePJmdO3cyefJk5s+fn2RcsWLF6NSpE506daJ9+/a0adMGPz8/APbv34+trS39+vUjT548LF++nMaNG/PkyRMePoyfhfLx8cHBwQGA8+fPM3/+fHbt2sXly5c5cOAAx44d49q1a2zfvh0fHx8iIiI09f9t0qRJODg44OvrS+XKlZP3gxZpjkJHJ8mrXEql6pvjyluVwXvPOtas2Myh/Uc126OePKVquQa0btKV2YsmUbhowe+YgRDpn85n+pfqk374uTiFQsGylbNxHzONyIgnKdZOIdKrz/UdpUqZ7LhP9ykUCpRKZaJYIf4tFeoU+WhThpjJrFq1KoUKFQLA3t4eb2/vZJe9ffs2efLkoWjRogC0bds2yQFe8eLFMTc3B6Bo0aK8fPmSBw8eYGdnh5GREQAtWrTg1atXXLx4kcqVK5M7d25NnSdPnvxiOzw9PQkMDGTv3r1cvnyZmJiYL8b/zcHBgV69ejFkyBB27tzJsGHDEuxXKBS0bdsWPz8/HBwcePr0KVZWVvz11180aNCAXLlyAdC8eXNOnz6NoaEhV65c0QxE3717h4WFBVWqVNHUeebMGWbPng1Aq1atcHNzS1ZbhfaNGD2QRk3tADAyzsqtG3c0+8zz5uHF85e8ffM2QZnQkDAqVSn/2bhWDs2Y6umG28gp+OzYA4CxsRG169Zg7+5DAFy7cpOb1/6gdJni3PvrQYrmKERaN3LMIJo0qw+AcTYjbl7/Q7Mvr4UZz5+/4M0n/TDkUSiVq1RIFFeyZDEKFsrHhCnxawzkMcuFjq4umTIZMGyQLAAkfky/uvanQZN6QPy57vbNPzX7zDTnsHcJyoQ+DseqSrkk40JDwjEzz63Zl8c8N+FhkSmchfiRaHuRnpSQIQaZenr/pKFWqzXf1Wo1CoWCuLi4z5ZVKBQJrk7p6uomGWdoaJiojI6ODipV4v9bfDob+nH7/m4XkKBdXbp0oUaNGtSoUYOaNWsyYkTybi3Mly8fFhYW7N+/XzOA/FTbtm3p06cPBgYGtG7dWrP941xVKhW6uroolUp69OhBr169AHj16hW6uro8f/48yRwUCgU6OhliQvyHMGvaYmZNWwyAaa6cHDyxk8JFCnDv7kOcenVkX8DhRGWOBp5i3CSXJOMaNqnHxGmudHHsx5VL1zVllColsxZOJCrqGeeCL1KiVFGKFi/MxfPaXUhLiLRg5tSFzJy6EIBcuXJyJMiPwkUKcu/uA3r81Im9u5Poh4dPMmHKqERx585eonJZO03cCFdncpqaMMZlUqrlI0RaM2/6MuZNXwaAaS4T9hzbSqEi+bl/9xFdejpyMOBoojInAoMYM2FoknEHA47QrmtrDu07RpasmWnZtgnjRkxJ1ZyESG8yxOjg/PnzhIaGolKp8PHxoVatWpiYmHDnzh3UajWHDyc+Yf+tZMmSREVFcetW/AIlu3fvTvZxa9asydGjR4mOjubDhw/s378fhUKBlZUV165dIywsDJVKxZ49ezRlTExM+PPP+Ctqhw7Fz/K8ePGC+/fvM2TIEOrWrcuhQ4e+eBuGrq5uggGqo6MjkydPplWrVgli/q7D0tISc3NzvLy8Egwyjx8/zuvXr3n//j27d++mbt26WFtb4+vrS0xMDHFxcQwcOJB9+/YlOH6tWrUS3KL7/v37ZP/MRNrxNOoZw53d+G3NXAJP+1GqTHEmuXkCUKFiWfYd3f7VuHET41fT85w/gX1Ht7Pv6HYmzxzLm5i39Ok2BI8po9h3dDuzF07Cud9IwkIjPtseIX5EUVHPGDJgDCvXzef4md2ULlMCD7f4Z7msKpXj0PGdX40TQnze06jnjBrswaJVnuw7tYOSZYoz1X0OAOUrlsY/cPNX4zau3s7DeyHsOuqFz8ENbNvow5lTF7SWk8h41Cn0nzZliJnMYsWKMWbMGJ48eYK1tTXt2rVDR0eH/v37kytXLqpUqZJoJu5v+vr6zJkzBxcXF/T09ChTpkyyj1uiRAm6d+9Ox44dyZIlCyYmJhgaGpIzZ04mTpzIzz//jJ6eHnny5NGUGTx4MJMmTWLRokXY2NgAkCNHDtq1a0eLFi3Q09PD2tqad+/e8ebNmySPW61aNUaNGkWuXLlwcnKicePGjBs3LsEA8u9nM1esWEH+/Plp3rw5+/fvT7Cybs6cOenbty/Pnz+nVatW1KlTB4Bbt27RoUMHlEolderUoW3btgleh+Lu7o6LiwtbtmyhXLlyZM2a9PumRNp3+OBxDh88nmj7lUvXaVKv3Vfj6tVIvArm306fOkfLhp2+T0OFyMAOHTjGoQPHEm2/fPEaDeq0/Wrcx2ZNX/Td2ydEenfk4EmOHEz82NLVSzext+v81TilUslkt1lfPMYOL392ePkn2j5ykMe3N1iIDECh1vb6tunYvXv3OHr0KD179gTgl19+oX379tSvXz9B3Mer235varWaY8eOsXnzZpYtW5ZkTFxcHCNHjqRp06Y0btw4xdv0Nflylvt6kBDiu4pVfv6xASFEyjHSz6LtJgjxw/orKn3MODcv0DxF6t3zcM/Xg1JIhpjJ1BZLS0uuXr1Ky5YtUSgU2NjYYGdn9/WC39HUqVMJDAz87Dss1Wo1derUoVatWgleiSKEEEIIIYQQKUFmMkWqk5lMIVKfzGQKoR0ykymE9qSXmcxm+ZulSL0BjwJSpN7kkJlMIYQQQgghhNCSjPgKkwyxuqwQQgghhBBCiLRBZjJFqlMm8W5RIUTKyqqfWdtNEOKH9Co2RttNEEKkcdp+3UhKkJlMIYQQQgghhBDfjcxkCiGEEEIIIYSWqDLgTKYMMoUQQgghhBBCSzLiyz7kdlkhhBBCCCGEEN+NzGQKIYQQQgghhJZkxNtlZSZTiB9Yg8Z1OXRyJ8fP7mb5mrkYGWf9pjjjbEb8vnYugad8OXran4FDeicq26mbA2u9FqdoHkKkB3aNbNhzdAsHT+9k0cqZGBkl3d8+F6ejo8O4ySM4EOTN4TO+dOnZLlHZfAUsuHDnCOUrltFs69zDkb0ntrPn6BZ+Wz8Xk5w5UiQ/IdKDRo3rceSkH0Hn9rJy7fzPnveSE7d6w0Kme47TfM9hkp2lv8/i8PGdnDobQPuOrVMsDyHSOhlkpqKrV68yduzYf1V28+bNbN68OdF2b29vXF1d/2vTkmX06NE8fvw4VY4lUp6pqQnzFk+hj9Ov1KnWggf3HzF2/LBvihs5djBhoRHY1WpN0/od6NG7E1WqWQGQI0d2ZswZz6Rpo1GgSNXchEhrcpqaMGPBBAb0cqGhdVsePQhhpPvgb4rr0sORwkUL0tSmPW0adaPXz12oUKmspqyBoQFzlk5BX19fsy1fAQuGjxlIJ/veNK/XkZCHofw6qn/KJyxEGmRqasL8JdP4yWkQNas25f79R4zzGPGv4pyH9MG6ZtUE2xYumU5oaDj167TFsXVPps4cS14LsxTNSWQM6hT6T5tkkJmKypcvz5QpU/5V2c6dO9O5c+fv3KJvExwcnCEfTP5R1atfm0sXrnHv7gMA1q7ywqF9y2+KGzdqKhPcPAEwM8uNgYEBr19FA9CqbVPCwyOZOM4zNdIRIk2rY2fN1UvXuX/3IQAbVm+jdbtm3xTXuEV9tm32RalU8urla3bt3Eeb9i00ZSfOGM0OLz+eP3uh2aarq4u+vh5ZjbKgUCjInCUT799/SMFMhUi7bOvbcOnCVe7+/3y2ZuVm2rW3/+a4WjbVqd+wDmtXeWm25TDJTj27WsyavgiAsNAImtTvwIvnL1MyJZFBqNTqFPlokzyT+Y2Cg4Px9PREpVJhaWlJlixZuHPnDkqlkr59+9KyZUtiY2MZP34858+fx8zMDIVCwYABAwBYtGgR69ev5969e7i7u/PixQuyZMnC2LFjqVChAq6urhgZGXH9+nUiIiIYOHAgjo6OLFy4EIBBgwbh4+PD0qVLMTIy0rQB4NSpU0yfPh21Wo2FhQWzZ88mS5YsTJ06laCgIBQKBa1ataJfv34EBwdr2gLg6upK9erVqV69Os7OzhQvXpybN29iamrK/Pnz2bp1K5GRkfTr148hQ4awevVqvLzi/3H19vbm8uXLTJgwQQt/I+LfsrA0J/RxuOZ72OMIsmU3xsg4K9GvY5Idp1QqWfTbDFq0bkzAroP8eeceAOtWbwGgQ5c2qZOQEGlYXgtzwh5HaL6Hh0ZinM0YI6OsREfHJCsur6VZgn1hoZGUKlMcgA7d2qKnr8eW9TsZOLSPJubBvUcsX7SOg6d9eP3yNa9fR+PYtEdKpipEmmWZz5zHH53PQh+HJ3ne+1Jc1qxZmTJ9LJ0c+9C9V0dNTOHCBYiIeMIvA3vRoFFdDAwNWLxwJXf/up8quQmR1shM5r9w//591q5dS8GCBSlbtize3t5s3LiRZcuW8ejRI7y8vHj79i179+5l2rRpXL16NVEdLi4uODk54e/vz+jRoxkyZAgfPsRfXQ4PD2fTpk0sXbqUmTNnJigXERHBrFmz2LhxI1u2bCEmJv4fxQ8fPjBixAhmzJiBv78/JUqUYOfOnWzevJmwsDD8/PzYtm0b+/fv58iRI1/M79atW/Tq1Ytdu3aRLVs2/P396devH3ny5GH58uU0btyYJ0+e8PBh/JV2Hx8fHBwcvsNPVqQmHR2dJGemlUrVN8c5/zyKskVrY2KSnWGjBnz/xgqRzunoKJLuRyplsuM+7YsKRXw/LFuhFF17tsNtROI7ZWxsrWnasgE2Vk2pUbYRBwKO4LlILgiKH9PnzmeqZJ73FAoFy1fOZtyYaUREPEmwT19fn0KF8vP6dTQtmnSm309DmTx1NBUqlk1UjxCfUqfQR5tkJvNfKFy4MMbGxpw6dYp3796xY8cOAN68ecOdO3c4efIkHTp0QKFQYGlpSc2aNROUj4mJ4eHDhzRu3BiAihUrkj17du7evQtA7dq1USgUlChRghcvXiQoe/HiRSpVqkSuXLkAsLe35/Tp09y+fRszMzNKly4NwPDhwwEYPHgwbdu2RVdXl8yZM2Nvb09QUBD169f/bH6mpqaUKRO/aETx4sV5+TLhrR4KhYK2bdvi5+eHg4MDT58+xcrK6t/8KEUqcxnjTONm8X/3xsZZuXnjjmZfXgsznj9/yds3bxOUeRwSRqWqFZKMs61fm5s3/iAi/AlvYt6wc8ceWrRqlDrJCJHG/er6Cw2b1APAyDgrt2/+qdlnljcPL56/5O2bdwnKPH4cjlWV8knGhYaEY2ae+5995rkJD4vAoUNLjIyzsn3PGgDymOdm7rIpTPOYRx1baw7tO8rTqOcArF+5hb3Ht6dUykKkOaPGDKbp/897RtmMuHn9D82++PPZC958ct4LeRRG5SpWieJKlixGwUL5mTQlfi2MPGa50NHVxTCTIXNnLQNg00ZvAO7dfUjw6QtUrlKBK5eup2iOQqRFMpP5L2TKlAkAlUqFp6cnvr6++Pr6snXrVurUqYOuri4qleqz5ZO6OqZWq1Eq469oGxoaAvGDuU8pFAmvcuvpxV8n0NfXTxD/+vVrwsPDE7Xj7+N8Wk9sbKzmz38fP6nj/a1t27bs3r2bXbt20bq1rJ6WXnhOXUSjOg40quNAi4adqVK1AoWLFASge6+O7NtzOFGZI4dPfjbOvm1Tho8aCICBgT6t2jTl5LHgVMpGiLRt3vSltLTrREu7Tjg27U6lKuUpVKQAAF17tuNgwJFEZU4EBn027kDAEdp3bY2uri7G2Yxo2bYJ+/ccYZLbLBrUaKM5VmT4E4b2H8uhvUe5fuUWdo3qkCVrZgCa2jfk0vnEd9cIkVHNmLoAuzptsKvThmYNOlClmhVF/n8+6/lTJ/buPpSozJHDJ5KMO3f2EhXL2mrqW7PKC1/vPQwd5MbDByFcvnSNTp3bAJA7tynVqlfi0sVrqZarSL9UqFPko00yyPwPrK2tNSu+RkZG0qpVK8LCwqhVqxZ79uxBrVYTERHBmTNnEgwAjYyMyJcvH/v37wfg0qVLREVFUbx48a8es0qVKly6dImIiAhUKhV79uwB4mdXnz59yp9/xl8pX7FiBZs3b8ba2hofHx+USiVv377F39+fGjVqYGJiwqNHj3j//j0vXrzg/PnzXz22rq6uZiBsaWmJubk5Xl5eMshMp55GPePXgW78vm4ux4L9KVWmOBPGxt+ebVWxLAeOe381boLbTIyzGRF4ypd9R7Zz5fJ1fl+6Xms5CZFWPY16zsjBHixe5cn+UzsoWaYYU9znAFC+Yhl2BXp9NW7j6m08vBfC7qNb8D24ka0bfThz6sv/dm/b5EvgwRP4HdrEnqNbsK5dBRdn95RNVog0KirqGUMGjGblugWcPLOH0mVK4O42AwCrSuUIPO7z1bgv6dHVGbsGNhw/vQuf3euZNXMxly7IRR3xY1KoZbnQb/LxgjnR0dF4eHhw69YtlEol/fr1o23btsTGxjJx4kQuXrxI7ty5efbsGZMmTeLt27easn/99RceHh68ePECfX193NzcqFy5smYBnr+fcSxZsiS3b99OsPDP3r17mT9/PpkzZ6ZYsWLo6Ogwffp0zpw5w/Tp04mNjaVAgQLMnDkTAwMDZsyYwenTp4mNjcXe3h5nZ2cA3N3dOXXqFJaWluTKlYvatWtTvXp1unfvzuHD8TNVHx93ypQpHDt2jBUrVpA/f37NM56///77N/0M8+Yo8/UgIcR3lVnP8OtBQojv7nXsG203QYgf1pOXt7XdhGSpaWmXIvUGPQ5MkXqTQwaZKeDIkSOo1Wrs7Ox4/fo1bdq0YceOHeTIkUPbTftu4uLiGDlyJE2bNtU8W5pcMsgUIvXJIFMI7ZBBphDak14GmdYWtilS7+nQIylSb3LI7bIpoGjRoixfvpzWrVvTrVs3Bg8enKEGmGq1mjp16qBQKGjYsKG2myOEEEIIIYRIQ2QmU6Q6mckUIvXJTKYQ2iEzmUJoT3qZyaxuUS9F6j0TejRF6k0OmckUQgghhBBCCPHdyHsyhRBCCCGEEEJL1Fp+3UhKkEGmEEL8AGJi3349SAjx3WXWNdB2E4QQaVxGfHpRbpcVQgghhBBCCPHdyEymEEIIIYQQQmiJKgPeLiszmUIIIYQQQgghvhuZyRRCCCGEEEIILcmIz2TKIFMIIYQQQgghtERulxVCZCgNGtfl0MmdHD+7m+Vr5mJknPWb4oyzGfH72rkEnvLl6Gl/Bg7pnahs/oKW3LgXhFXFsimaixDpScPG9Qg86cvJcwH8vnbeZ/tecuJWbVjAVM9xibZ37ubAeq+l373tQqRn9RvVYd/xHQQG+7F09ezP9r2vxeW1NOPMtYOY5Myh2Va8ZBF27FlLwNFt7Dmylbr1a6VkKkKkaWlykBkcHIyTk9N/rmfBggWcO3cOgLFjx3L16tX/XGdK+ri9/8WjR48YM2bMd2jRPzZv3szmzZsTbff29sbV1fW7HkukDlNTE+YtnkIfp1+pU60FD+4/Yuz4Yd8UN3LsYMJCI7Cr1Zqm9TvQo3cnqlSz0pQ1NDRg0W8zMNDXT7W8hEjrTE1NmL9kKj85DaZ21WY8uP8IN4/h/ypu4JDe1KhZNcG2HCbZmTnXg8nTx6JQKFI0FyHSk5ymJsxaNImfewzFrkYrHt4PwdX912+Oc+xoz7ZdazC3MEtQbrKnG1s27qRZvfa4DHJnyapZ6OrqpnBWIiNQp9B/2pQmB5nfy9mzZ1EqlQBMmTKF8uXLa7lFX/Zxe/+L0NBQHj169B1a9I/OnTvTuXPn71qn0K569Wtz6cI17t19AMDaVV44tG/5TXHjRk1lgpsnAGZmuTEwMOD1q2hN2amzxrF1kw/Pnj1P6XSESDds69fm4oWr//SplV44trf/5rhaNtWp37AO61Z5JSjXqm1TwsMi8Rg3MwWzECL9qWtXi8sXr3P/7kMA1q/aQpv2Lb4pzsw8N42b18epXf9E5XR1dcieIxsAWY2y8v7dh5RKRYg0L80+k/ns2TP69u3Lw4cPKVy4MAsWLGDPnj2sXbsWlUpF2bJlGT9+PIaGhmzYsAFfX1/evn2Lvr4+s2fP5sqVK1y7dg03NzcWLVrE5MmTcXZ2BuC3334jU6ZM/PXXX5QsWZJZs2ZhYGDAunXr2LBhA8bGxhQpUoQCBQowaNAgAgMDmTdvHiqVivz58zNx4kRy5cpF/fr1qVChAjdv3mT16tV4eHgQFRUFwMCBAylWrBg9evTg8OHD6OjoEBwczO+//87kyZMZMWIEb968QUdHBzc3N+7fv5+gvZkyZcLDw4MXL16QKVMmxo0bR5kyZXB1dSVz5szcuHGDV69eMWzYMHx9fbl16xYNGzbE1dWVyZMnExISwoQJE4iOjqZatWp06NABACcnJ0aMGMGsWbMoVaoU586d4/3794wZMwYbGxuioqJwd3cnPDwchULB8OHDqVWrFgsXLgRg0KBB+Pj4sHTpUoyMjLC0tCRLliza+T+J+E8sLM0JfRyu+R72OIJs2Y0xMs5K9OuYZMcplUoW/TaDFq0bE7DrIH/euQdAFydH9PX12LhuO0NG/Jx6iQmRxlnky5ugT4U+Dk+6730hLmvWrEyePoZOjn3p3qtjgvrXrdoCQMcubVM4EyHSFwtLc8I+Pp+FRpAtW9Lnvc/FRYQ/4eceQ5Os381lKl6+K+jzS3dMc+XEuY/Ld5k8EBmfKgMu/JNmZzJDQ0Nxd3cnICCAqKgotm3bxtatW/Hy8sLX1xdTU1NWrlxJdHQ0Bw8eZP369ezatQtbW1s2btxImzZtKFeuHJMnT6ZkyZIJ6r548aKm7tDQUE6cOMGtW7fYuHEj3t7ebNq0iQcP4q8cP336FHd3dxYvXoy/vz+VK1dm4sSJmrrq1q3Lvn37CA4OxtLSEm9vb6ZMmcK5c+coWLAg+fLlIzg4GAAfHx8cHBzYvn07tra2eHt7M3jwYM6fP5+ovaNGjcLFxYWdO3cyadIkhg795x+0yMhItmzZQr9+/Rg9ejQTJkzAx8eHrVu38vr1a9zc3ChXrhzjx4/H0dERX19fAB4/fsyzZ8+wsoq/nTE6OpqdO3cye/ZsXF1d+fDhA1OmTMHR0RFvb2+WLl2Ku7s70dH/zExFREQwa9YsNm7cyJYtW4iJ+ecfZZG+6OjoJLmamVKp+uY4559HUbZobUxMsjNs1ADKW5Wm+08dGTV0wvdvuBDp3Of6lCqZfU+hULBs5Wzcx0wjMuJJirVTiIxGoaNI1nkvuXEfMzQ0YPEqT4YPdKNGuYa0b9mTaXPcyWtp9tkyQmRkaXYms1SpUuTPnx+AokWL8vz5cx48eKCZkYuNjaVMmTIYGRkxe/Zsdu/ezf379zl+/DilS5f+Yt3FixfH3NxcU/fLly958OABdnZ2GBkZAdCiRQtevXrFlStXqFChAvny5QOgY8eOLF++XFPX3wO2SpUqMWfOHCIiIrC1tWXgwIEAODo64ufnR8WKFTl9+jQeHh6YmZkxaNAgbt68Sb169ejWrVuC9sXExHDt2jVGjx6t2fbmzRueP4+/5bBu3boAWFhYULx4cUxNTQHIkSMHL1++TFBXjRo1GDduHCEhIfj6+tK6dWvNvr9/lqVLlyZ37tzcvn2bU6dOcffuXRYsWABAXFxcgltvL168SKVKlciVKxcA9vb2nD59+os/b5F2uIxxpnGz+gAYG2fl5o07mn15Lcx4/vwlb9+8TVDmcUgYlapWSDLOtn5tbt74g4jwJ7yJecPOHXto0aoR2bIZYWRshN/+TQCYmedh0e8zmeQ+i/0BgamQqRBpy8gxg2jyd9/LZsTN639o9sX3qRe8+aTvhTwKpXKVT/veC0qWLEbBQvmYMCX+efg8ZrnQ0dUlUyYDhg1KvACQED+yYaMH0qipLQDGxkbc+ui8Z543Dy+SOO+FhoRT6aO+97m4j5UsXYzMmTNxaP8xAC6eu8Ift/6iUpUKhD0+8B0zEhmRtp+fTAlpdpCpp/dP0xQKBcbGxjRr1gw3NzcgfiCmVCoJCwvDycmJbt26UbduXXLlysXNmze/WLehoWGCutVqNTo6OqhUia9QfbpNrVYTFxeXqK5ChQoREBDA8ePHCQwMZNWqVezZs4emTZsyd+5c9u3bR926dTE0NKRKlSrs3r2bI0eOsGfPHnbu3Mnq1asTHNPAwEAzAwkQHh5Ojhw5AND/aBGVj39OSVEoFLRp04bdu3cTEBDAypUrNfs+fhhdpVKhp6eHSqVi7dq1mmNFRkZiamrKwYMHE/y8knt8kbZ4Tl2E59RFAJjmykngKR8KFynIvbsP6N6rI/v2HE5U5sjhk4yf7JJknH3bpjS3b8TIoR4YGOjTqk1Tjh05xfIl63AfPV1Tx5krB3DuO5LLl66nTqJCpDEzpy5k5tT4xw5y5crJkSA/TZ/q8VMn9u5O3PeOHj7JhCmjEsWdO3uJymXtNHEjXJ3JaWrCGJdJqZaPEOnFnGmLmTNtMRB/3tt/wptCRQpw/+5DuvXqkOSFz2OBp3CbNOKrcR+7f/cRxtmMqFLdivNnLlOwUD6KlyzC9Stf/p1UCJDbZbXuwIEDPH36FLVajYeHB2vXruXq1asULFiQnj17Ur58eQ4ePKi5/11XVzfZ98LXrFmTo0ePEh0dzYcPH9i/fz8KhQIrKysuX75MSEgIAFu2bKFGjRqJym/YsIGFCxfSrFkzxo8fz7Nnz4iOjiZz5szUrVuXOXPm4ODgAMDMmTPx8/Ojbdu2uLu7c+PGjQTtNTY2plChQppB5smTJ+natWuyf066uroJBsIODg54eXmRN29ezMz+uW1jz549AFy9epVXr15RokQJrK2t2bQpfvbpzz//xN7enrdv/7lyV6VKFS5dukRERAQqlUpTh0h/nkY949eBbvy+bi7Hgv0pVaY4E8bGLxRiVbEsB457fzVugttMjLMZEXjKl31HtnPl8nV+X7peazkJkR5ERT1jyIAxrFw3n+NndlO6TAk83GYAYFWpHIeO7/xqnBDi2z2NesYI53EsWzOHQ6d9KVWmOJP+v3hdhYplCDi67atxn/Pq1Wv6Of2Kx1RX9p/wZumaObgOnciD+yEpnpcQaVG6mYYyNjbG2dmZHj16oFKpKF26NP369SMuLo7NmzfTvHlz1Go11apV486d+Fsh6tSpw/jx45kx4+sn5RIlStC9e3c6duxIlixZMDExwdDQkFy5cjFx4kScnZ2JjY3FwsKCKVOmJCrfpk0bhg0bhr29Pbq6uri4uJAtW/wKYy1atODChQuaW2udnJwYPnw43t7e6Orqatr3cXs9PT3x8PBgxYoV6OvrM3fu3GQvRV+0aFFev36Ni4sLnp6e5M2bl7x589K2bcJFIB49eqTZNnfuXHR1dXFzc8Pd3R17+/gVDGfOnKm5hRggV65cuLm50bNnTzJnzkyxYsWS1SaRNh0+cIzDB44l2n750nUa1XH4atyrl6/5pfeIrx6neoVG/62hQmQwhw4c41BSfe/iNRrUafvVuI/Nmr4oye1bNu1ky6ad/62hQmQwgQePE3jweKLtVy7doFm99l+N+1iBnAnfWhB04iz2DWUlfvHtMuLtsgp1Uk82/4Du3bvH0aNH6dmzJwC//PIL7du3p379+v+pXqVSydy5czE1NaVXr17foaXfRq1WExkZiZOTE7t27cLAwACIH+g6OzsnOSub0vLmKJPqxxTiR6dSf37BCiFEyjHUlfcEC6EtD59d1XYTkqVUnmopUu+tyLPJjvX392fp0qXExcXRo0ePRHdR3rx5k7FjxxITE0PVqlWZMGHCFx+bS1e3y6YkS0tLrl69SsuWLbG3t6dQoULY2dl9veBXODo6cv36da29Y3Lfvn20bt2aYcOGaQaYQgghhBBCiLRBpVanyCe5IiIimDt3Lps2bcLHx4ctW7bw559/JohxcXHB3d2dffv2oVar2bp16xfrlJlMkepkJlOI1CczmUJoh8xkCqE96WUms3juKilS750n55MVt3PnTs6ePcvUqVMBWLx4MWq1GmdnZyD+NYg9evTQLAR67tw5FixYwLp16z5bZ7p5JlMIIYQQQgghRPK8evWKV69eJdqeLVs2zdoxEP82idy5c2u+58mThytXrnx2f+7cuYmIiPjisWWQKVLdm7j32m6CED+cXJmya7sJQvyQHrz68i9iQgiRUq8wWbt2LYsWJV4cztnZmUGDBv1zfJUqwQKjarU6wfev7U+KDDKFEEIIIYQQIoPp0aNHordLAAlmMQHMzc05d+6c5vuTJ0/IkydPgv1PnjzRfI+KikqwPymy8I8QQgghhBBCaIk6hf7Lli0b+fLlS/T5dJBZq1YtgoKCePbsGW/fvmX//v3UrVtXs9/S0hJDQ0POn49/xtPX1zfB/qTITKYQQgghhBBCaIlay4vzmZmZMXToULp3705sbCzt2rWjQoUK9O3bl8GDB1O+fHlmzZqFm5sb0dHRlC1blu7du3+xTlldVqS67EZFtd0EIX448kymENohz2QKoT1xHx5ruwnJUtjUKkXqvff0corUmxwykymEEEIIIYQQWqIi4835yTOZQvzAGjex5eTp3Zy7cIC16xdibGz0zXF9+nbl2Alfzpzfx/IVszEwMEhQtmDBfNx/eJ5KlcqnaC5CpHW2jWzYfXQLB057s3DlDIyMsn5TnI6ODm6TR7A/aAeHz/jSuadjorLturRm+cZ5CbZ17uFIwIlt7D66hWXr52CSM8f3Tk2IDKF5swZcOH+A69eO4bX5t8+eE/+2auU8hg39OZVaJ0T6IoPMdC44OBgnJ6fP7g8MDGT16tUAbN68mc2bNwMwevRoHj9O/i0EISEh1K9f/781VqQpprlysmTZTJy6DqRq5Ubcv/cIj4ku3xRn36ox/fp3p7V9d2pUbUrmzJkY6NxLU9bQ0IDlK+agbyAvIxc/tpymOZi5wIOBvUbQyNqBRw8e4+I+6JviOvdwpHDRAjSz6UCbRt3o9XMXKlQqC0D2HNmYNGsM46aOSLCsfL4CFgwbM4DO9n1oUa8jjx+GMmRU/9RIWYh0JVeunKz4fQ4dOvajbLm63Lv3gKlTxiQZW6pUMQ7s24qjQ4tUbqXIqNRqdYp8tEkGmRnctWvXiI6OBqBz58507twZiB+cavv/fEK76te34cL5K9z96z4AK1dspH2H1t8U16mzA4sWrOT585eo1Wp+HTIOr80+mrKz50xg08YdPH36PKXTESJNs7GryZVL17l/9xEAG1dvo3W7Zt8U17iFHds3+6FUKnn18jW7du6nTfvmADRv04iI8CdMHz8vQX26urro6+uR1SgLCoWCzFky8f69vKtYiE81alSPc+cu8+ef9wBY9ts6unRO/OoHgF/692Tl6k1s37ErNZsoRLoiz2RmEGfOnGHu3Lm8e/eOV69eMXr0aAoVKoSXlxcAFhYWhIaGAmBoaEhkZCT9+vVj48aNODo6sm7dOvLly0dwcDCLFi1i/fr13Lhxg7FjxwJQqlQpzbGioqJwd3cnPDwchULB8OHDqVWrVuonLf6TfPny8vhxmOb748fhZM9ujLGxEa9fRycrrljxQlw4b8qOnasxz5uHoFNncXebAUD3Hh3Q09dn7ZotDHcZkHqJCZEG5bUwI+zxPwvAhIdGYpzNGCOjrERHxyQrLq/lp/siKFWmOACb1+wAwLGTfYLjPrj3iN8XrePA6Z28fvma16+jade0Z0qkKES6lj+fBY9CQjXfQ0LCyJ49W6JzIsCQX90AaNSwXqq2UWRc8kymSLM2bNjA5MmT2blzJ5MnT2b+/PkUK1aMTp060alTJxwd/3l2p1+/fuTJk4fly5djYmLy2TpHjRrFiBEj2LlzJ/ny5dNsnzJlCo6Ojnh7e7N06VLc3d01s6Ui/dDR0UlyNlupVCY7Tl9PH9v6NvTsPgjbOm0wMcnBuPHDsbIqy0+9uzB0iFuKtV+I9OSz/UiVzP6mUibap1AoEvXXT9nYWtOkZQPqWDXDumxjDgYcZeaiCf8yCyEyruSeE4VICRnxdlmZycwgPD09CQwMZO/evVy+fJmYmJivF/qCZ8+eERkZSe3atQFwcHBgx474K+WnTp3i7t27LFiwAIC4uDgePXpE6dKl/1sSIsWNcfuVZs0bAJDN2Ijr129r9llYmPH82QvevHmboMyjR6FUqWqVZFxYeAT+fvs0V3m3ePkwyjX++TFjYyP2H9oGQN68efh95RzGuU0nYM+hFM1RiLTiV9f+NGgSP9NhZJyV2zf/1Owzy5uHF89f8vbNuwRlQh+HY1WlXJJxoSHhmJnn1uzLY56b8LDIL7ahQdN6HNp3jKdR8besr1+5hYDj2/5zbkJkBB7jR9CyZWMg/px47fotzT5LS3OePXue6JwohEgemcnMILp06cKVK1coV64c/ft/+6IOf1/tiIuLA+KvkH98BURXV1fzZ5VKxdq1a/H19cXX15etW7dSokSJ/5iBSA1TJ8+jTi176tSyp0H9dlSrXokiRQsB8FPvLuzefTBRmcOHT3w2ztdnL20dmpMpkyEALVs25sKFK4weNZkqlRpqjhUWFknf3sNkgCl+KPOmL8PerjP2dp1p17QHlaqUp1CR/AB06enIwYCjicqcCAz6bNzBgCO069oaXV1djLMZ0bJtEw7sCfxiG65fuYVdIxuyZM0MQFP7Blw6f/V7pilEuuUxYRZVqzWmarXG1K5jT43qlSlWrDAAP/dzws9/v5ZbKH4UKrU6RT7aJDOZGcCLFy8IDQ1l06ZNGBgYMGvWLM3tHbq6ukku8qCrq6uJMTEx4c8//yR//vwcOnRIs83CwoIjR45ga2vLrl3/PNxubW3Npk2bGDBgAH/++Sddu3bl0KFDGBl9ealvkbZEPXnKgP6jWLdhEQYG+ty7+5D+/UYAUKlSeRYsnkqdWvZfjFuxfAMmJtk5esIXXR1dLl++ztjBU7WZlhBp0tOo54wa7MGiVZ7oG+jz8H4IIwaMA6B8xdJMneuOvV3nL8ZtXL2dAoXys+uoFwYG+mxeu4Mzpy588bjbN/mSr0BefA9t5MP7WB6HhDHSeXyK5ytEevPkyVP69B3GFq/lGBjoc/evB/T8aQgAVSpX4Lff4gekQojkUai1fcOu+E/+XqinTJkyHDp0CD09PaytrQkICCAwMJDr168zatQoevXqxYsXLwAYNGgQU6ZM4dixY6xYsYK7d+8yadIksmfPjo2NDRcuXGD9+vXcuXOH0aNHExcXR8WKFTl27BiHDx8mIiICd3d3zUJCI0aMoF695D/8nt2oaEr8KIQQX5ArU3ZtN0GIH9KDVxFfDxJCpIi4D8l/XZ82medImUfOwl/cTJF6k0MGmSLVySBTiNQng0whtEMGmUJoT3oZZJplL/X1oH8h4uWtrwelEHkmUwghhBBCCCHEdyPPZAohhBBCCCGElsh7MoUQQgghhBBCiC+QmUwhhBBCCCGE0JKMuESODDJFqlOqVNpughA/nPA3z7TdBCF+SDkyZdV2E4QQaZy232mZEuR2WSGEEEIIIYQQ343MZAohhBBCCCGElmTE22VlJlMIIYQQQgghxHcjM5lCCCGEEEIIoSXyChMhhBBCCCGEEOILZJApxA+sSVM7TgcHcOHSIdZvWIyxsdE3xW3YuIRTp3drPo/DLrNl2+8Jyjp1b8/W7StSPBch0pMmTe0IDg7gYjL6XlJxGzYuIej0Hs0nNOwKWz/pewUL5uNRyCUqVS6f4vkIkV40amLL0VN+nD6/l5Vr52NknPTqv8mJW7NhEdNnuWu+29SpweFjOzl6yg+fXesoW65UiuUhMha1Wp0iH22SQeZnvH79moEDB34xxtXVFW9v7/98rODgYJycnP5zPR/73vVdvXqVsWPHJtoeEhJC/fr1v+uxROrIlSsny5bNpGuXX6hcsQH37j1k4qSR3xTXresAalm3oJZ1C5wHjubly9cM+zX+hGtikp35CyYz09MdhSJVUxMiTcuVKye/LfOkS5dfqFSxAffvPWLipFHfFNet6wBqWjenpnVzBg505eXLVwz99Z9fdg0NDVm5ah4GBvqplpcQaZ2pqQkLlkyjl9MgrKs05cH9R7hPGPGv4gYN6YN1raqa78bZjFizYRHjx82kXq1WjBjqwcq10gdF8qjU6hT5aJMMMj/j5cuX3Lx5U9vN+NfOnDnzXesrX748U6ZM+a51Cu2q36AO5y9c4a+/7gOw4vcNdOjY+l/F6evrs3z5LEaNnMjjx2EAODi2ICwsgrFjpqZoHkKkNw0+6VO//76Bjkn0veTExfe92YwcOUnT9wDmzpvIhg3befr0eYrlIUR6Y9fAhksXrnL3rwcArF65mXbtW31zXG2b6tRvWIc1qzZrthUtWohXr15z/GgQAH/eucvr1zFUq14pJVMSIs1K0wv/BAcHs2TJEvT09AgJCaFChQr88ssvDBgwABMTEzJlysTKlSuZOnUqQUFBKBQKWrVqRb9+/ZIsO2XKFAwMDPDx8WHt2rWoVCrKli3L+PHjMTQ0xNramnLlyvHkyRNy585NZGQkAwcOpHjx4qjVaoYOHQrEz2DWrVs3QVvnzp1LUFAQL1++JE+ePMydO5dcuXJhY2NDkyZNOH/+PLq6usybN4/8+fNz4sQJpk2bhqGhIYULF9bU4+TkhLOzMzVq1CAkJITu3btz+PBh/P39WbFiBbq6uuTLlw9PT08MDQ1Zvnw5AQEBKJVKbGxscHFx0QwG27dvT4cOHTh9+jSzZ88GYOHChRgaGvL+/XtCQ0P566+/eP78OR07dqRPnz4olUpmzpzJmTNnUCqVODg40LNnT4KDg1m0aBHr16/nxo0bmlnNUqXkVpD0Kl++vDwO+eeX0sePw8mePRvGxka8fh39TXE9enYgLCwCf7/9mriVKzYB0LWbY0qnIkS6ki+fBSEJ+lTYZ/re1+N69OxIeFgE/n77NHE9enZEX0+PNau9GDnSORUyEiJ9sLBMeD4LfRxOtuzGGBlnJfp1TLLijLJmZcoMNzo69KbHT500MX/+eY8sWbNgW782Rw6fpFLl8pQsVQwz89ypk5xI19Sy8E/qu3jxImPHjmXv3r28f/+eo0ePcu/ePTw9PVm9ejWbN28mLCwMPz8/tm3bxv79+zly5EiSZTdu3MidO3fYunUrXl5e+Pr6YmpqysqVKwF4/vw5ffv2xdfXFw8PD/LkycPixYtxdHTE398ftVrN27dvOX36NA0aNNC08cGDB9y9excvLy/27dtH3rx58fPzA+DJkyfUrFkTHx8fqlWrxsaNG/nw4QOurq4sWLAAb29vMmXK9NWfw7x581i1ahXe3t5YWlpy9+5djh07xrVr19i+fTs+Pj5ERETg5+eHm5sbANu2baN58+YEBQURHR3/C8muXbto3Tr+Svi1a9dYvXo13t7ebNmyhevXr7N161YAdu7cyfbt2zl06BDnzp1L0JZRo0YxYsQIdu7cSb58+f7D367QJh0dnSTv11cqld8cN9C5NzNnLPr+jRQiA1LoKJLV95IT5+z8EzM+6nsVK5alT5+uDB6c+PEGIX50nzufqZSqZMUpFAqWr5rDuNFTiYh4kmBf9OsYuncewNDh/Tly0o8Ondtw4thpPnyI/b5JCJFOpOmZTIBq1apRpEgRAFq3bs3WrVsxNTXVDG6Cg4Np27Yturq6ZM6cGXt7e4KCgqhfv36SZfX19Xnw4AEdOnQAIDY2ljJlymiOZ2VllagN+fPnx9LSkrNnzxIaGkq9evUwNDTU7C9YsCCjRo1i27Zt3Lt3j0uXLlGgQAHN/jp16gBQvHhxzp07x+3bt8mTJw9FixYFoG3btsyfP/+LPwc7Ozs6d+5Mw4YNadKkCaVLl8bPz48rV67g4OAAwLt377CwsEhQLmvWrNSrV48DBw6QP39+8ufPj5mZGQAtW7Yka9b4B9nr16/P6dOnuXz5Mjdv3uT06dMAvHnzhtu3b1OsWDEAnj17RmRkJLVr1wbAwcGBHTt2fLHtIu1wGzeU5i0aAmBsbMT167c1+ywszHn27AVv3rxNUObRo1CqVqv42bgKVmXQ09Pl+PHglE9AiHTKbdxQWrRoBCS/74U8CqVatUqfjbOyKouenh7Hj5/WxHTp4oixsRGHA+PXC8ibNw+rVs1j7Nhp7Nl9MMXyEyKtch07mCbN4icGjI2NuHnjn76X18KM588T973HIaFUqVohUVzJUsUoWCg/E6eOBiCPWS50dXXJZGjI0MFuxMS8oXWLf9bECL6wj3t3H6ZkeiKD0PbzkykhzQ8ydXV1NX9Wq9XxnfmjmT+VKuHVJ7VarbnKm1RZpVJJs2bNNLN9MTExCa4Kf25W0dHRkV27dhEaGsqgQYMS7Lt27RrDhw+nZ8+eNGnSJNEVsL8HpApF/FXpv/83qRz/bitAXFycZpubmxu3bt3i6NGjuLi44OzsjFKppEePHvTq1QuAV69eJarr77YvXbqUfPnyaQaknx5XpVJpfj4uLi40btwYiB9UZs2alUuXLiXI4XNtF2nb5ElzmTxpLgC5c5sSfGYvRYsW4q+/7tO7Txd27z6QqMzhQ8eZNm3sZ+NsbGpw9P/PoAghkvZp3zvzUd/r06drkn3v0Cd979O4+L53KkGZkSMnMnLkRM33GzdP8NNPv3LxwtUUykyItG36lAVMn7IAiF9M69jpXRQpWpC7fz2g50+dCdh9KFGZwEMnmDDFNVHcuTOXsCpTTxM3cvQgcpqa4Doivs95bf8dp86/cOniNdo4NOfdu/dcv3YrdRIV6Zq2V4JNCWn+dtnz588TERGBSqXCx8cn0bOQ1tbW+Pj4oFQqefv2Lf7+/tSoUeOzZWvUqMGBAwd4+vQparUaDw8P1q5dm+i4enp6CQZ5TZs2JSgoiKioqESznWfPnqV69ep07tyZQoUKceTIkUS3PX2sZMmSREVFcetW/D88u3fv1uwzMTHhzz//BODgwfirznFxcTRu3BgTExN+/vlnWrduzc2bN7G2tsbX15eYmBji4uIYOHAg+/bFP5ejq6uraX/VqlUJDw8nODiYhg0bao518OBBPnz4wMuXLwkMDMTGxgZra2u2bt1KbGwsMTExdOnSRTPA/Lt9FhYWmluSd+3a9dk8Rdr25MlT+vd3YcPGJZy/cICy5UoyxjX+ed5Klctz6vTur8YBFCtWmIcPQrSSgxDp0d99auPGpZy/cJCy5Uoy2nUyEN/3gk7v+WocQNFihXggfU+IZIuKesbgAaNZtW4hp84GUKZsCdzHTgegYqVyBJ7w/Wrcl/zcexhzFkzmRPBunHp0oHuXL7+lQIiMTKFOw0Pn4OBgzbORERER1K5dmx49etCzZ08OHz4MxN/uOmPGDE6fPk1sbCz29vY4OzsnWXbMmDHo6uqybds2zcI/pUuXZurUqRgaGlKyZElu376tqdfJyQl9fX3Wr18PgIuLCyVKlKBv375A/AJA1atXp3bt2jg7O/Pu3TsASpcujUqlYtasWQnq9Pb25syZM0yfPp2zZ88yceJE9PT0KFOmDA8fPmT9+vVcuXIFV1dXDA0NadCgAd7e3hw+fJhdu3axdOlSDA0NMTU1Zfr06ZiamrJkyRJ2796NUqmkTp06jBkzBoVCwaBBg/hfe3cfVVWV/3H8jTzpL8dEnRi1h7HWyhIl01FCDQUL6CK4QJMw7pRTY+UD4yxsgmRyajATsvKpjJ5cWZkPQIFgjOLjzDimMxhSDJoIoTPaoKJAS7hc7u8PFmcBXvDOeBWlz2st1xLuufvus8/+nn2/Z+9zKC0tJSMjA09PT9544w2qqqr4wx/+ADQ9AOjAgQPU1tZSU1PDE088wcMPP9yqPRsaGoiKijIepNT84J8jR46QmJhIQ0MDw4cPZ/fu3cbxcETP/xt06Y1ExKm64kMFRK4HPdw8OrsKIj9alecPd3YVHOLZ/ZYrUm7dhYorUq4jrvkkszmxuZrvbctms1FbW0t0dDRr1qzhpz+9fp4UZrPZsFgszJgxg+effx4fHx+gKckELlr6ezUoyRS5+pRkinQOJZkinUdJZuclmdf8ctlrwaFDhwgKCmLatGnXVYIJTU+3HTt2LPfcc4+RYIqIiIiIyLXBZrNdkX+d6ZqeyZSuPEqGWAAADv5JREFUSTOZIlefZjJFOodmMkU6z/Uyk+nuMfCKlGupP3FFynWEZjJFRERERETEaa75P2EiIiIiIiLSVXXFtUZaLisiIiIiIiJOo+WyIiIiIiIi4jRKMkVERERERMRplGSKiIiIiIiI0yjJFBEREREREadRkikiIiIiIiJOoyRTREREREREnEZJpoiIiIiIiDiNkkwRERERERFxGiWZIiIiIiIi4jRKMkVERERERMRplGSKdAHV1dXMnj27w20SExM5ceJEh9uYzWb27dvntHqtWLGCFStWOK08EXuWL1/OgQMHLruciooKnn/+eSfUyLGYdJagoCCOHz9+2eU4Eq/Nn5WRkUFCQsJlf2Z7nHks5Orbt28fZrP5sstp7m9Xsz8MHjzYKeUkJCSQkZHh0Gdd6bGysLCQ1NTUK1a+iD1KMkW6gHPnzlFcXNzhNvv27cNms12lGolcPfv378dqtV52Of/617+oqKhwQo0ci0lpnzOPhVz/1B8uz7fffsvp06c7uxryI6MkU6QLSE5O5vvvv2f27Nmkp6czadIkwsPDSUhIoLa2lrS0NL7//ntmzpzJ2bNn2bJlC9OmTSMiIoLQ0FD+8Y9/OPQ5ixcv5v333zd+njt3Llu3buXw4cOYzWamTJlCYGAg69atu+i9La8Ot5wFKSwsJCYmhsjISH71q1/pi4R06OTJk8TGxhIVFcXUqVNZuXIlRUVFJCUlUVJSgtlsZs6cOYSEhFBcXNxuv/vrX/9KREQE4eHhPPXUU9TU1JCcnExRUREvvvjiRTMxzbMSx48fJzQ0lJiYGGbMmIHVamXx4sVERkYSERHBmjVrgNYxCfD6668zbdo0QkJCMJvNVFZW8vXXXzN27FjOnDlDVVUVgYGBHDp0iHHjxlFZWQlAVVUV48aNw2Kx8NFHH/Hwww8zadIkIiMjKS0tbdU2bWcXW65MSEtLM+qYkpJiXHB69913CQ4OJjo6msLCQuO9O3bsYPLkyYSHhzNr1iyjPva0dz5peyyys7MxmUyEhYWRkJCAxWJhxYoVJCUlYTabCQoK4q233jLar/lYyPXryy+/NM7vEydOZNu2bUBTPCUnJxMTE0NQUBDp6elAU3//9a9/TXh4OPPmzaOurg5o3R8aGhpISkoiOjqaiRMnMmvWLC5cuEB+fj4hISHU1dVRXl5OQEAAp06dYuzYsVgsFgAOHz5MREQEYD8mW2o7u9g8q9pezNtsNhYvXmyU99133xnvtTcut6e9OA8KCmLevHmEhIRw+vRp1qxZQ0hICCaTyZiltNeu58+fZ/ny5Wzfvt2IL5GrQUmmSBeQlJTETTfdRFxcHKtXr2bt2rVkZ2fTo0cPVq5cycyZM7nppptIS0vjxhtv5NNPP2X16tVkZWXx5JNPkpaW5tDnTJ48mc2bNwNQU1NDQUEB48ePZ+PGjcyaNYv09HQ+/PBDUlJSHCqvvr6epKQkli5dSmZmJjNmzOD3v//9/9wO0vVt2rSJCRMmkJGRQVxcHD169GDo0KEkJycbCeXgwYPJy8vj7rvvtltGfX098+fPZ8mSJWRnZ3PnnXeSmZlJUlISQ4cOZeHChR3W4dixY6SmpvLBBx+wYcMGADIzM9m0aRP5+fkcOHDAiMlVq1ZRXl5OaWkpn376KXl5efTv35+srCx8fHyIjo4mJSWFP/7xj8TExDBs2DBCQ0P54osvAPjTn/7Egw8+SF1dHdu2bWPt2rVs3ryZCRMm8PHHHzvUZrt376aoqIhNmzbx2WefcerUKbKysjh06BDp6elkZmbywQcfcPLkSQBOnz7NCy+8wKpVq8jOzmbEiBG89NJLdstubGzs8HzSfCz69OljXKTKycnBarWya9cuAEpKSnjvvffYuHEjaWlpnD9/3uFjIde2jz76iOTkZDIzM0lOTmbZsmXGaydPnuSTTz7hrbfeMsaM5cuXM2TIELKzs3n00UeNxK9lfygoKMDd3Z3169ezdetWqqur2bVrFxMnTmT48OGsXr2axMREnnvuOby9vfH19eXPf/4zADk5OURERLQbk45oL+bz8vL45ptv2Lx5M8uWLTOSzJKSErvjsj01NTUdxnlAQAB5eXmcOHGCTz75hE2bNpGVlcXXX39NUVGR3Xbt1asXcXFxBAUF8cwzz/w3h0/ksrh1dgVExHn2799PYGAgXl5eAERHR5OYmNhqm27durFq1Sq2b9/OsWPH+PLLL+nWzbHrTUOGDKG+vp7y8nIKCgoICgrCw8ODhIQE9uzZw9tvv83hw4f54YcfHCqvrKyMioqKVgNfTU2Ng3srP0b+/v7MnTuX4uJixo8fT2xsLDt37my1ja+vb4dllJSU4O3tbSSh8fHxAA7fj9y3b19uvvlmAPbu3UtxcTF/+9vfAPjhhx8oKSnhZz/7mbH9bbfdxnPPPcfGjRs5duwYBw8e5NZbbwXgmWeeYcqUKXTv3t2YjYiIiGDx4sXExsayefNmfvvb39KzZ0+WLl1KTk4OZWVl7Nmzp90kuq29e/dSWFhIVFQUABcuXGDAgAFUVlYyfvx4brjhBgBCQ0NpbGyksLAQX19fYx+jo6PbvRB1qfNJ87EoKChgxIgRRrs072txcTF+fn54eHjQt29fevfuTXV1tUP7Jde+1NRUduzYwRdffMFXX33VagZv7NixuLi4cOedd1JVVQU0zXwuXboUgFGjRnHLLbdcVOaoUaPo3bs3H3/8MaWlpZSVlRljzoIFCzCZTIwYMYKwsDCgKZ5ycnIIDAxky5YtrF27Fm9v73Zj8lLai/mjR48SHByMu7s7ffr0ISAgAHBsXG52qTi/5557WpX5k5/8BMCYTW2vXUU6g5JMkS6ksbGx1c82m42GhoZWv6utrWXq1KlEREQwatQoBg8e7PCMCDQN2Lm5uRQUFDBz5kwA5s2bR69evQgMDMRkMhmznW3ZbDZcXFyMOjU2NnLzzTfz+eefA2C1WjtclicycuRIcnJy2LlzJ7m5uWRmZl60Tffu3Vv93Lbfubu74+LiYrxeXV190fI1FxeXVvcwNy+3a1u+1Wrl2WefJTg4GIAzZ85www038J///MfYpqioiPj4eB5//HFCQkLo1q2bUXbzZ9fW1lJVVUWfPn3w9fXl3LlzFBYWcurUKe69917+/e9/YzabiY2NJSAggH79+l10z2d7dbZarTz22GPMmDEDgPPnz+Pq6sr69etbbe/m5kZ9fb1D55FmlzqfNLeVm5tbqzY/c+aM8X9PT89290Gub9OnT8fPzw8/Pz/8/f2ZP3++8VrzcW/ZL9oef1dX14vKzM/PZ/ny5fzyl78kKiqKs2fPGu+prKzE1dWV0tJS6urq8PT0ZOLEibzyyivs37+f/v374+3t3WFMtqxLy1hoGU/2Yr7lMnRo6vPg2Ljc7FJx3txmbePp1KlT9OjRo912FekMWi4r0gW4ubnR0NDA6NGj2b59u3H1csOGDfj5+QFNg7XVaqWsrAwXFxeefvpp/Pz82Lp163/10JTw8HByc3MpLy9n5MiRAPzlL38hLi6OBx54gN27dwNcVKaXlxdHjhzBZrOxfft2AG6//XbOnTtnPBk0PT291ZcQkbZSUlLIysoiMjKSF154gW+++cbo2/bY63eDBg3i9OnTfPvtt0DTfYnr1q3D1dXV+PLn5eVFRUUFdXV1VFVV8fe//91u+ffddx8bNmzAYrFQW1vL9OnTOXjwoBGT0DTrMHr0aGJiYvj5z3/Ozp07jfq++OKLxMbGMn369Fb3H4aHh7Nw4UJjNubQoUPcdtttPP744wwbNoxt27bZjbGjR49is9moqKigpKTEqOPnn39ObW0tDQ0NzJ49m7y8PPz9/dmxYwfV1dXU1dWxdetWoGm25KuvvjKeWLt+/XrjPNKWo+eTYcOGcfDgQSP5fvnll8nPz7dbJtDqWMj1qaqqirKyMn7zm98QEBBAfn7+Jccaf39/46JjYWGhseS0ZX/Yu3cvDz30EFOmTKFXr17s27cPq9WK1WolMTGRBQsWMHr0aGNproeHB/fffz8vv/yycT9mRzHZzMvLyzhHFBYWGn23vZj39/dny5Yt1NfXc+7cOfbs2QPQ4bjcliNxDvCLX/yCXbt2GTEdHx9vLJe1R/EknUEzmSJdQN++fRkwYACLFi3iqaeewmw2Y7FY8PHxMb64TpgwgZkzZ/LOO+9w991389BDD+Hi4sK4cePa/QJtT//+/fHy8uLee+81rpTOnTuX6dOn4+npyV133cXAgQMv+pMK8fHxPP300/Tr14+RI0dy9uxZPDw8WLZsGYsWLaKuro6ePXuyZMkS5zWMdDlms5n4+HgyMjJwdXVlyZIlHD16lIULF9rtO/b6naenJ6mpqfzud7/DYrFw6623kpKSQn19PdXV1Tz77LOkpqYyfvx4wsLCGDhwoHFBpa1HHnmE8vJyIiMjaWhoICoqCj8/PywWCwMGDMBsNvPqq68yZ84cwsPDARg6dCjHjx8nNzeXiooKXnvtNWw2G1OmTCE3NxeTyURERATLli3j9ddfB5qWwK1btw6TyYTNZmPUqFEcOXKkVV3GjBlDeno6oaGhDBo0yKhzUFAQ//znP5k2bRpWq5X777+fyMhIXFxceOyxx5g6dSq9evViwIABAPTr14+XXnqJOXPmGPuxaNEiu/t/1113OXQ+8fb2ZsGCBTzxxBM0NjYyfPhwoqKiePPNN+2We8cdd7Q6FnL96d27N2PGjCEsLAw3Nzfuu+8+Lly40OHtFHFxcSQkJBAWFsbtt99uLJdt2R+efPJJ5s+fT05ODu7u7owYMYLjx4/z/vvv07dvX4KDgxkzZgyTJk0iODiY4cOHM3nyZLKysggJCQHAZDLZjcmWTCYTeXl5mEwmfHx8GDJkCNB+zENTkjhp0iT69evHHXfcATTFSHvjcluOxDmAj48PsbGxPPLIIzQ2NvLggw8yZsyYdu8r9fX1ZeXKlbz66qu6kCtXjYtN61JERERERETESTSTKSKtfPfdd8ydO9fua8nJyQwbNuwq10hERERErieayRQRERERERGn0YN/RERERERExGmUZIqIiIiIiIjTKMkUERERERERp1GSKSIiIiIiIk6jJFNERERERESc5v8BI/tP+J5vD+8AAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 1080x576 with 2 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Display which variables influence target variables the most for df2\n",
    "correlation_matrix = df2.corr()\n",
    "sns.set(rc = {'figure.figsize':(15,8)})\n",
    "sns.heatmap(correlation_matrix[['total_value', 'structuretaxvaluedollarcnt', 'landtaxvaluedollarcnt']].sort_values(by='total_value', ascending=False), annot=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7111f46f-88ad-4ce4-a2e9-833ba20259e1",
   "metadata": {},
   "source": [
    "## 3. Machine learning models and Evaluation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "a04f0eac-2f65-4c8c-8e6a-09330cc57201",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>bathroomcnt</th>\n",
       "      <th>bedroomcnt</th>\n",
       "      <th>calculatedbathnbr</th>\n",
       "      <th>calculatedfinishedsquarefeet</th>\n",
       "      <th>finishedsquarefeet12</th>\n",
       "      <th>fips</th>\n",
       "      <th>fullbathcnt</th>\n",
       "      <th>latitude</th>\n",
       "      <th>longitude</th>\n",
       "      <th>lotsizesquarefeet</th>\n",
       "      <th>...</th>\n",
       "      <th>regionidzip</th>\n",
       "      <th>roomcnt</th>\n",
       "      <th>yearbuilt</th>\n",
       "      <th>structuretaxvaluedollarcnt</th>\n",
       "      <th>taxvaluedollarcnt</th>\n",
       "      <th>assessmentyear</th>\n",
       "      <th>landtaxvaluedollarcnt</th>\n",
       "      <th>taxamount</th>\n",
       "      <th>censustractandblock</th>\n",
       "      <th>total_value</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>parcelid</th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>10843547</th>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>73026.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>6037.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>33989359.0</td>\n",
       "      <td>-118394633.0</td>\n",
       "      <td>63085.0</td>\n",
       "      <td>...</td>\n",
       "      <td>96095.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>650756.0</td>\n",
       "      <td>1413387.0</td>\n",
       "      <td>2015.0</td>\n",
       "      <td>762631.0</td>\n",
       "      <td>20800.37</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1413387.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10859147</th>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>5068.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>6037.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>34148863.0</td>\n",
       "      <td>-118437206.0</td>\n",
       "      <td>7521.0</td>\n",
       "      <td>...</td>\n",
       "      <td>96424.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>1948.0</td>\n",
       "      <td>571346.0</td>\n",
       "      <td>1156834.0</td>\n",
       "      <td>2015.0</td>\n",
       "      <td>585488.0</td>\n",
       "      <td>14557.57</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1156834.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10879947</th>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1776.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>6037.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>34194168.0</td>\n",
       "      <td>-118385816.0</td>\n",
       "      <td>8512.0</td>\n",
       "      <td>...</td>\n",
       "      <td>96450.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>1947.0</td>\n",
       "      <td>193796.0</td>\n",
       "      <td>433491.0</td>\n",
       "      <td>2015.0</td>\n",
       "      <td>239695.0</td>\n",
       "      <td>5725.17</td>\n",
       "      <td>NaN</td>\n",
       "      <td>433491.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10898347</th>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>2400.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>6037.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>34171873.0</td>\n",
       "      <td>-118380906.0</td>\n",
       "      <td>2500.0</td>\n",
       "      <td>...</td>\n",
       "      <td>96446.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>1943.0</td>\n",
       "      <td>176383.0</td>\n",
       "      <td>283315.0</td>\n",
       "      <td>2015.0</td>\n",
       "      <td>106932.0</td>\n",
       "      <td>3661.28</td>\n",
       "      <td>NaN</td>\n",
       "      <td>283315.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10933547</th>\n",
       "      <td>0.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>6037.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>34131929.0</td>\n",
       "      <td>-118351474.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>...</td>\n",
       "      <td>96049.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>397945.0</td>\n",
       "      <td>554573.0</td>\n",
       "      <td>2015.0</td>\n",
       "      <td>156628.0</td>\n",
       "      <td>6773.34</td>\n",
       "      <td>NaN</td>\n",
       "      <td>554573.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>...</th>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>12246525</th>\n",
       "      <td>6.0</td>\n",
       "      <td>8.0</td>\n",
       "      <td>6.0</td>\n",
       "      <td>4582.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>6037.0</td>\n",
       "      <td>6.0</td>\n",
       "      <td>33879428.0</td>\n",
       "      <td>-118302589.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>...</td>\n",
       "      <td>96104.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>2012.0</td>\n",
       "      <td>450000.0</td>\n",
       "      <td>800000.0</td>\n",
       "      <td>2015.0</td>\n",
       "      <td>350000.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>800000.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>11366340</th>\n",
       "      <td>1.0</td>\n",
       "      <td>2.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>798.0</td>\n",
       "      <td>798.0</td>\n",
       "      <td>6037.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>34769356.0</td>\n",
       "      <td>-118553521.0</td>\n",
       "      <td>7034153.0</td>\n",
       "      <td>...</td>\n",
       "      <td>97319.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>2006.0</td>\n",
       "      <td>55200.0</td>\n",
       "      <td>469300.0</td>\n",
       "      <td>2016.0</td>\n",
       "      <td>414100.0</td>\n",
       "      <td>5764.45</td>\n",
       "      <td>NaN</td>\n",
       "      <td>469300.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>13010327</th>\n",
       "      <td>3.0</td>\n",
       "      <td>3.0</td>\n",
       "      <td>3.0</td>\n",
       "      <td>1526.0</td>\n",
       "      <td>1526.0</td>\n",
       "      <td>6037.0</td>\n",
       "      <td>3.0</td>\n",
       "      <td>34087735.0</td>\n",
       "      <td>-118063008.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>...</td>\n",
       "      <td>96517.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>2014.0</td>\n",
       "      <td>356454.0</td>\n",
       "      <td>594022.0</td>\n",
       "      <td>2016.0</td>\n",
       "      <td>237568.0</td>\n",
       "      <td>7343.47</td>\n",
       "      <td>NaN</td>\n",
       "      <td>594022.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>12385768</th>\n",
       "      <td>4.0</td>\n",
       "      <td>4.0</td>\n",
       "      <td>4.0</td>\n",
       "      <td>2110.0</td>\n",
       "      <td>2110.0</td>\n",
       "      <td>6037.0</td>\n",
       "      <td>4.0</td>\n",
       "      <td>33951985.0</td>\n",
       "      <td>-118124097.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>...</td>\n",
       "      <td>96100.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>2014.0</td>\n",
       "      <td>336548.0</td>\n",
       "      <td>554009.0</td>\n",
       "      <td>2016.0</td>\n",
       "      <td>217461.0</td>\n",
       "      <td>6761.20</td>\n",
       "      <td>NaN</td>\n",
       "      <td>554009.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>11795063</th>\n",
       "      <td>1.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>1040.0</td>\n",
       "      <td>1040.0</td>\n",
       "      <td>6037.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>34041100.0</td>\n",
       "      <td>-118234000.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>...</td>\n",
       "      <td>95994.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>1910.0</td>\n",
       "      <td>371756.0</td>\n",
       "      <td>620284.0</td>\n",
       "      <td>2016.0</td>\n",
       "      <td>248528.0</td>\n",
       "      <td>7611.91</td>\n",
       "      <td>NaN</td>\n",
       "      <td>620284.0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "<p>965533 rows × 25 columns</p>\n",
       "</div>"
      ],
      "text/plain": [
       "          bathroomcnt  bedroomcnt  calculatedbathnbr  \\\n",
       "parcelid                                               \n",
       "10843547          0.0         0.0                NaN   \n",
       "10859147          0.0         0.0                NaN   \n",
       "10879947          0.0         0.0                NaN   \n",
       "10898347          0.0         0.0                NaN   \n",
       "10933547          0.0         0.0                NaN   \n",
       "...               ...         ...                ...   \n",
       "12246525          6.0         8.0                6.0   \n",
       "11366340          1.0         2.0                1.0   \n",
       "13010327          3.0         3.0                3.0   \n",
       "12385768          4.0         4.0                4.0   \n",
       "11795063          1.0         1.0                1.0   \n",
       "\n",
       "          calculatedfinishedsquarefeet  finishedsquarefeet12    fips  \\\n",
       "parcelid                                                               \n",
       "10843547                       73026.0                   NaN  6037.0   \n",
       "10859147                        5068.0                   NaN  6037.0   \n",
       "10879947                        1776.0                   NaN  6037.0   \n",
       "10898347                        2400.0                   NaN  6037.0   \n",
       "10933547                           NaN                   NaN  6037.0   \n",
       "...                                ...                   ...     ...   \n",
       "12246525                        4582.0                   NaN  6037.0   \n",
       "11366340                         798.0                 798.0  6037.0   \n",
       "13010327                        1526.0                1526.0  6037.0   \n",
       "12385768                        2110.0                2110.0  6037.0   \n",
       "11795063                        1040.0                1040.0  6037.0   \n",
       "\n",
       "          fullbathcnt    latitude    longitude  lotsizesquarefeet  ...  \\\n",
       "parcelid                                                           ...   \n",
       "10843547          NaN  33989359.0 -118394633.0            63085.0  ...   \n",
       "10859147          NaN  34148863.0 -118437206.0             7521.0  ...   \n",
       "10879947          NaN  34194168.0 -118385816.0             8512.0  ...   \n",
       "10898347          NaN  34171873.0 -118380906.0             2500.0  ...   \n",
       "10933547          NaN  34131929.0 -118351474.0                NaN  ...   \n",
       "...               ...         ...          ...                ...  ...   \n",
       "12246525          6.0  33879428.0 -118302589.0                NaN  ...   \n",
       "11366340          1.0  34769356.0 -118553521.0          7034153.0  ...   \n",
       "13010327          3.0  34087735.0 -118063008.0                NaN  ...   \n",
       "12385768          4.0  33951985.0 -118124097.0                NaN  ...   \n",
       "11795063          1.0  34041100.0 -118234000.0                NaN  ...   \n",
       "\n",
       "         regionidzip  roomcnt  yearbuilt  structuretaxvaluedollarcnt  \\\n",
       "parcelid                                                               \n",
       "10843547     96095.0      0.0        NaN                    650756.0   \n",
       "10859147     96424.0      0.0     1948.0                    571346.0   \n",
       "10879947     96450.0      0.0     1947.0                    193796.0   \n",
       "10898347     96446.0      0.0     1943.0                    176383.0   \n",
       "10933547     96049.0      0.0        NaN                    397945.0   \n",
       "...              ...      ...        ...                         ...   \n",
       "12246525     96104.0      0.0     2012.0                    450000.0   \n",
       "11366340     97319.0      0.0     2006.0                     55200.0   \n",
       "13010327     96517.0      0.0     2014.0                    356454.0   \n",
       "12385768     96100.0      0.0     2014.0                    336548.0   \n",
       "11795063     95994.0      0.0     1910.0                    371756.0   \n",
       "\n",
       "          taxvaluedollarcnt  assessmentyear  landtaxvaluedollarcnt  taxamount  \\\n",
       "parcelid                                                                        \n",
       "10843547          1413387.0          2015.0               762631.0   20800.37   \n",
       "10859147          1156834.0          2015.0               585488.0   14557.57   \n",
       "10879947           433491.0          2015.0               239695.0    5725.17   \n",
       "10898347           283315.0          2015.0               106932.0    3661.28   \n",
       "10933547           554573.0          2015.0               156628.0    6773.34   \n",
       "...                     ...             ...                    ...        ...   \n",
       "12246525           800000.0          2015.0               350000.0        NaN   \n",
       "11366340           469300.0          2016.0               414100.0    5764.45   \n",
       "13010327           594022.0          2016.0               237568.0    7343.47   \n",
       "12385768           554009.0          2016.0               217461.0    6761.20   \n",
       "11795063           620284.0          2016.0               248528.0    7611.91   \n",
       "\n",
       "          censustractandblock  total_value  \n",
       "parcelid                                    \n",
       "10843547                  NaN    1413387.0  \n",
       "10859147                  NaN    1156834.0  \n",
       "10879947                  NaN     433491.0  \n",
       "10898347                  NaN     283315.0  \n",
       "10933547                  NaN     554573.0  \n",
       "...                       ...          ...  \n",
       "12246525                  NaN     800000.0  \n",
       "11366340                  NaN     469300.0  \n",
       "13010327                  NaN     594022.0  \n",
       "12385768                  NaN     554009.0  \n",
       "11795063                  NaN     620284.0  \n",
       "\n",
       "[965533 rows x 25 columns]"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df_zillow[df_zillow.isna().any(axis=1)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "id": "eb802d77",
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.impute import SimpleImputer\n",
    "from sklearn.linear_model import LinearRegression, Lasso\n",
    "from sklearn.neighbors import KNeighborsClassifier\n",
    "from sklearn.pipeline import Pipeline\n",
    "from sklearn.model_selection import train_test_split, cross_val_score, GridSearchCV\n",
    "from sklearn.metrics import classification_report, mean_absolute_error, mean_squared_error, max_error, r2_score"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "ada1ecf2",
   "metadata": {},
   "outputs": [],
   "source": [
    "df2 = df_zillow[['taxvaluedollarcnt','taxamount','finishedsquarefeet12','calculatedfinishedsquarefeet','calculatedbathnbr',\n",
    "                 'fullbathcnt','bathroomcnt','yearbuilt','bedroomcnt','regionidcity','propertylandusetypeid',\n",
    "                 'total_value']]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "910c8419",
   "metadata": {},
   "outputs": [],
   "source": [
    "imp = SimpleImputer(missing_values=np.nan,strategy='mean')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "9d4bc20b",
   "metadata": {},
   "outputs": [],
   "source": [
    "#DataFrame = imp.transform(df2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "be1120c7",
   "metadata": {},
   "outputs": [],
   "source": [
    "X = df2.drop(['total_value'], axis=1).values\n",
    "y = df2[['total_value']].values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "f0eca76c",
   "metadata": {},
   "outputs": [],
   "source": [
    "reg = LinearRegression()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "68b3726a",
   "metadata": {},
   "outputs": [],
   "source": [
    "param_grid_lasso = {'alpha':[0.001,0.01,0.1,1]}\n",
    "lasso = Lasso()\n",
    "lasso_cv = GridSearchCV(lasso, param_grid=param_grid_lasso, cv = 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "ec38e9cd",
   "metadata": {},
   "outputs": [],
   "source": [
    "param_grid_knn = {'n_neighbors':[5,50,100]}\n",
    "knn = KNeighborsClassifier()\n",
    "knn_cv = GridSearchCV(knn, param_grid_knn, cv = 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "d7f0359a",
   "metadata": {},
   "outputs": [],
   "source": [
    "pipeline_reg = Pipeline([\n",
    "    ('Imputation', imp),\n",
    "    ('LinearRegression', reg)\n",
    "])\n",
    "pipeline_lasso = Pipeline([\n",
    "    ('imputation', imp),\n",
    "    ('Lasso', lasso_cv)\n",
    "])\n",
    "pipeline_knn = Pipeline([\n",
    "    ('imputation', imp),\n",
    "    ('Knn', knn_cv)\n",
    "])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "1006acb7",
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.33, random_state = 42)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "f56a3a07",
   "metadata": {},
   "outputs": [],
   "source": [
    "pipeline_reg.fit(X_train, y_train)\n",
    "y_reg_pred = pipeline_reg.predict(X_test)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 68,
   "id": "98835df7",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "MAE: 3.2659809408066396e-09\n",
      "MSE: 6.48652439051223e-17\n",
      "RMSE :8.05389619408658e-09\n",
      "R2: 1.0\n"
     ]
    }
   ],
   "source": [
    "from decimal import *\n",
    "\n",
    "\n",
    "mae = Decimal(mean_absolute_error(y_test, y_reg_pred))\n",
    "mse = Decimal(mean_squared_error(y_test, y_reg_pred))\n",
    "rmse = Decimal(np.sqrt(mean_squared_error(y_test, y_reg_pred)))\n",
    "Adj_r2 = Decimal(1 - (1-r2_score(y_test, y_reg_pred)) * (len(y)-1)/(len(y)-X.shape[1]-1))\n",
    "\n",
    "\n",
    "\n",
    "print('MAE: '+str(mean_absolute_error(y_test, y_reg_pred)))\n",
    "print('MSE: '+str(mean_squared_error(y_test, y_reg_pred)))\n",
    "print('RMSE :'+str(np.sqrt(mean_squared_error(y_test, y_reg_pred))))\n",
    "print('R2: '+str(1 - (1-r2_score(y_test, y_reg_pred)) * (len(y)-1)/(len(y)-X.shape[1]-1)))\n",
    "\n",
    "df_reg = pd.DataFrame({'MAE' :mae, 'MSE': mse, 'RMSE' : rmse, 'Adj_R2' : Adj_r2}, index=[0])\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "id": "8ad53f85",
   "metadata": {},
   "outputs": [],
   "source": [
    "pipeline_lasso.fit(X_train, y_train)\n",
    "y_lasso_pred = pipeline_lasso.predict(X_test)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 74,
   "id": "bd446fc6",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "MAE: 6.712365717431368e-11\n",
      "MSE: 2.7208837822085635e-20\n",
      "RMSE:1.6495101643241133e-10\n",
      "Adj_r2:1.0\n"
     ]
    }
   ],
   "source": [
    "mae_lasso = Decimal(mean_absolute_error(y_test, y_lasso_pred))\n",
    "mse_lasso = Decimal(mean_squared_error(y_test, y_lasso_pred))\n",
    "rmse_lasso = Decimal(np.sqrt(mean_squared_error(y_test, y_lasso_pred)))\n",
    "Adj_r2_lasso = Decimal(1 - (1-r2_score(y_test, y_lasso_pred)) * (len(y)-1)/(len(y)-X.shape[1]-1))\n",
    "\n",
    "print('MAE: '+str(mean_absolute_error(y_test, y_lasso_pred)))\n",
    "print('MSE: '+str(mean_squared_error(y_test, y_lasso_pred)))\n",
    "print('RMSE:'+str(np.sqrt(mean_squared_error(y_test, y_lasso_pred))))\n",
    "print('Adj_r2:'+str(1 - (1-r2_score(y_test, y_lasso_pred)) * (len(y)-1)/(len(y)-X.shape[1]-1)))\n",
    "df_lasso = pd.DataFrame({'MAE' :mae_lasso, 'MSE': mse_lasso, 'RMSE' : rmse_lasso, 'Adj_R2' : Adj_r2_lasso}, index=[0])\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 75,
   "id": "a0f2be6e-39b5-48f2-b1dd-d94d4f32a866",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "MAE: Lasso reg fits better\n",
      "MSE: Lasso reg fits better\n",
      "RMSE: Lasso reg fits better\n",
      "Adj_R2: Both models have the same adjusted R2 Score\n"
     ]
    }
   ],
   "source": [
    "if mae_lasso < mae:\n",
    "    print('MAE: Lasso reg fits better')\n",
    "else:\n",
    "    print('MAE: Normal lin reg fits better')\n",
    "    \n",
    "if mse_lasso < mse:\n",
    "    print('MSE: Lasso reg fits better')\n",
    "else:\n",
    "    print('MSE: Normal lin reg fits better')\n",
    "    \n",
    "if rmse_lasso < rmse:\n",
    "    print('RMSE: Lasso reg fits better')\n",
    "else:\n",
    "    print('RMSE: Normal lin reg fits better')\n",
    "    \n",
    "if Adj_r2_lasso > Adj_r2:\n",
    "    print('Adj_R2: Lasso reg fits better')\n",
    "elif Adj_r2_lasso == Adj_r2:\n",
    "    print('Adj_R2: Both models have the same adjusted R2 Score')\n",
    "else:\n",
    "    print('Adj_R2: Normal lin reg fits better')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "07aed1b0-0153-4f1b-8423-789a1d8b6312",
   "metadata": {},
   "source": [
    "**Since we are dealing with a regression model, the performance of our model must be reported as an error. Error metrics summarize on average how close the predictions were to their expected values.\n",
    "In order to evaluate our models, we computed the Mean Squared Error, the mean absolute error, the Root Mean Square Error and the Adjusted R2 Score. We compared these metrics from both models and based on the results decided which model fit better.\n",
    "It can be recognized that the lasso regression model has better (lower) values for the MAE, MSE and RMSE than the normal linear regression. Therefore, the lasso regression model currently seems to be the model that fits better. \n",
    "Furthermore, both models have an adjusted R2 Score of 1. A value of 1 indicates a model that perfectly predicts values in the target field. A value that is less than or equal to 0 indicates a model that has no predictive value. Normally, the value lies between these values. Therefore, it should be further investigated why our models get a perfect score for both models.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "38bef65e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# pipeline_knn.fit(X_train, y_train)\n",
    "# y_knn_pred = pipeline_knn.predict(X_test)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "741a45a6",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(mean_absolute_error(y_test, y_knn_pred))\n",
    "print(mean_squared_error(y_test, y_knn_pred))\n",
    "print(np.sqrt(mean_squared_error(y_test, y_knn_pred)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dd309323-4105-483b-92ce-328173487336",
   "metadata": {},
   "source": [
    "## 4. Machine learning pipelines"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "97e307a2-93fa-4b86-8aa0-b8292438ea23",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "93c169a3-ddd6-4b0d-a140-3e57ee4b5345",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
